﻿Public Class CambioTienda
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Session("TipoUsuario").ToString.ToUpper = "U" Then
			CMensajes.Show("Sólo el administrador tiene permiso de acceso a esta sección.")
			Response.Redirect("Inicio.aspx" & Request.QueryString("SID"))
		End If

		Me.wucMessageCambioTienda.Visible = False

		If Not Page.IsPostBack Then
			Tiendas_Buscar()
		End If

		Dim scriptManager2 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
		scriptManager2.RegisterPostBackControl(Me.btnCambiar)
	End Sub

	Protected Sub btnCambiar_Click(sender As Object, e As EventArgs) Handles btnCambiar.Click
		Try
			If Me.cboTienda.SelectedItem.Text = "[Seleccione]" Then
				CMensajes.MostrarMensaje("Seleccione la tienda con la cual desea trabajar.", CMensajes.Tipo.GetError, wucMessageCambioTienda)
			Else
				Session("IdTienda") = Me.cboTienda.SelectedItem.Text
				CMensajes.Show("Se realizó el cambio de tienda correctamente.")
				Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
			End If
			Session("Cambio") = "S"
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Tiendas_Buscar()
		Try
			Dim ds As New DataTable
			ds = DatosSQL.FuncionSPDataTable("Tiendas_Buscar")
			If Not ds Is Nothing Then
				Dim dr As DataRow
				dr = ds.NewRow
				dr("IdTienda") = "[Seleccione]"
				ds.Rows.InsertAt(dr, 0)

				Me.cboTienda.DataSource = ds
				Me.cboTienda.DataTextField = "IdTienda"
				Me.cboTienda.DataBind()

			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class