﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CapturaRangosMarbetes.aspx.vb" Inherits="Saic.CapturaRangosMarbetes" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Captura rangos marbetes" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCapRangMarbetes" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" id="TblCamposRotativo" width="85%" cellpadding="7" visible="false">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Fecha inventario: " CssClass="control-label" Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFechaInventario" runat="server" Width="110px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Tipo inventario: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtTipoInventario" runat="server" Width="90px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Nombre del gerente: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtGerente" runat="server" Width="220px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblSubGerenteAdmvo" Text="Nombre del sub-gerente administrativo: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtSubGerenteAdmvo" runat="server" Width="220px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblSubGerenteComercial" Text="Nombre del sub-gerente comercial: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtSubGerenteComercial" runat="server" Width="220px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" ID="lblJefeDepto" Text="Nombre del jefe de departamento: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtJefeDepto" runat="server" Width="220px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Nombre del jefe de control de inventarios: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtJefeControlInventarios" runat="server" Width="220px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Nombre del responsable de administración de inventarios: " Font-Size="XX-Small"></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtReponsableAdmonInventario" runat="server" Width="220px" Font-Size="XX-Small" EnableViewState="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br /><br />
                <table runat="server" cellpadding="7" width="100%">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Rango incial: " Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtInicioRango" runat="server" Width="110px" MaxLength="11"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Rango final: " Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFinalRango" runat="server" Width="110px" MaxLength="11"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Ubicación: " Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtUbicacionFisica" runat="server" Width="200px" MaxLength="80"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Área: " Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboArea" runat="server" Width="140px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                        <td>
                            <span><asp:Button runat="server" Text="Agregar" ID="btnAgregar" CssClass="btn" CausesValidation="false" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server">
                    <tr>
                        <td><br />
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="IdRango" DataField="IdRango" ItemStyle-Font-Size="XX-Small" HeaderStyle-Width="70px" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
                                        <asp:TemplateField HeaderText="Rango Inicial" ItemStyle-Font-Size="XX-Small" ItemStyle-Width="130px" ControlStyle-Width="130px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:TextBox id="txtRangoInicialGrid" runat="server" Width="100px" MaxLength="11" Text='<%# DataBinder.Eval(Container, "DataItem.RangoInicial") %>'></asp:TextBox>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rango Final" ItemStyle-Font-Size="XX-Small" ItemStyle-Width="130px" ControlStyle-Width="130px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:TextBox id="txtRangoFinalGrid" runat="server" Width="97px" MaxLength="11" Text='<%# DataBinder.Eval(Container, "DataItem.RangoFinal") %>'></asp:TextBox>
                                            </ItemTemplate>                                  
                                        </asp:TemplateField>                          
                                        <asp:TemplateField HeaderText="Ubicaci&#243;n F&#237;sica" ItemStyle-Font-Size="XX-Small" ItemStyle-Width="200px" ControlStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:TextBox id="txtUbicacionFisicaGrid" runat="server" Width="200px" MaxLength="80" Text='<%# DataBinder.Eval(Container, "DataItem.UbicacionFisica") %>'></asp:TextBox>
                                            </ItemTemplate>                            
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Area" ItemStyle-Font-Size="XX-Small" ItemStyle-Width="165px" ControlStyle-Width="165px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:DropDownList id="cboAreaGrid" runat="server" Width="150px" CssClass="ComboBlue"></asp:DropDownList>
                                            </ItemTemplate>                           
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="IdArea" DataField="IdArea" ItemStyle-Font-Size="XX-Small" ItemStyle-Width="115px" ControlStyle-Width="115px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
                                        <asp:TemplateField HeaderText="Acci&#243;n" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkmodificar" runat="server" CommandName="Modificar" Text="Modificar" CommandArgument="<%#Container.DataItemIndex%>"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Acci&#243;n" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEliminar" runat="server" CommandName="Eliminar" Text="Eliminar" CommandArgument="<%#Container.DataItemIndex%>"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
<%--                    <tr>
                        <td align="center"><br />
                            <span><asp:Button runat="server" Text="Guardar" ID="btnGuardar" CssClass="btn" CausesValidation="false" /></span>
                        </td>
                    </tr>--%>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
