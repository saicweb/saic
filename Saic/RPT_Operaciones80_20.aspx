﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_Operaciones80_20.aspx.vb" Inherits="Saic.RPT_Operaciones80_20" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                    Reporte operaciones 80-20
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageRPTOperaciones80_20" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="25%" id="TblGeneral">
                    <tr>
                        <td>
                            <asp:label id="Label1" runat="server" Text="Reporte: "></asp:label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboReporte" tabIndex="1" runat="server" AutoPostBack="true" Width="160px" CssClass="ComboBlue">
                                <asp:ListItem Value="0">-- Seleccione --</asp:ListItem>
                                <asp:ListItem Value="1" Text="General"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Por Departamento"></asp:ListItem>
                                <asp:ListItem Value="3" Text="Por Almacen"></asp:ListItem>
                                <asp:ListItem Value="4" Text="Por Rango"></asp:ListItem>
                            </asp:DropDownList> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" align="center" width="65%" id="TblDepartamento">
                    <tr>
                        <td>
                            <table runat="server" align="center" cellpadding="10">
                                <tr>
                                    <td align="center">
                                        <asp:label id="Label2" runat="server" Text="Departamentos tiendas"></asp:label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td  align="center">
                                        <asp:ListBox ID="lstTiendas1" runat="server" Height="300px" Width="350px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table runat="server" align="center" cellpadding="10">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnAgregar1" runat="server" Font-Size="Small" Height="50px" Text="Agregar todos" Width="130px" CssClass="btn" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnAgregar2" runat="server" Font-Size="Small" Height="50px" Text="Agregar" Width="130px" CssClass="btn" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnQuitar1" runat="server" Font-Size="Small" Height="50px" Text="Quitar" Width="130px" CssClass="btn" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnQuitar2" runat="server"  Font-Size="Small" Height="50px" Text="Quitar todos" Width="130px" CssClass="btn" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table runat="server" align="center" cellpadding="10">
                                <tr>
                                    <td align="center">
                                        <asp:label id="Label4" runat="server" Text="Departamentos seleccionados"></asp:label><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:ListBox ID="lstSeleccionados1" runat="server" Height="300px" Width="350px" SelectionMode="Multiple"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" align="center" width="25%" id="TblAlmacen">
                    <tr>
                        <td>
                            <asp:label id="lblalmacen" runat="server" visible="True" Text="Almacen:"></asp:label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboAlmacen" runat="server" Width="140px" CssClass="ComboBlue" visible="True"></asp:DropDownList> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" align="center" cellpadding="5" id="TblRango">
                    <tr>
                        <td>
                            <table runat="server" width="45%">
                                <tr>
                                    <td>
                                        <asp:label id="lblRango" runat="server" visible="True" Text="Colocar rango de precio >" ></asp:label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRango" runat="server" Text="0" visible="True"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table runat="server" align="center" width="65%">
                                <tr>
                                    <td>
                                        <table runat="server" align="center" cellpadding="10">
                                            <tr>
                                                <td align="center">
                                                    <asp:label id="Label5" runat="server" Text="Departamentos tiendas"></asp:label><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td  align="center">
                                                    <asp:ListBox ID="lstTiendas2" runat="server" Height="300px" Width="350px" SelectionMode="Multiple"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><br />
                                        <table runat="server" align="center" cellpadding="10">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnAgregar3" runat="server" Font-Size="Small" Height="50px" Text="Agregar todos" Width="130px" CssClass="btn" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnAgregar4" runat="server" Font-Size="Small" Height="50px" Text="Agregar" Width="130px" CssClass="btn" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnQuitar3" runat="server" Font-Size="Small" Height="50px" Text="Quitar" Width="130px" CssClass="btn" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnQuitar4" runat="server"  Font-Size="Small" Height="50px" Text="Quitar todos" Width="130px" CssClass="btn" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table runat="server" align="center" cellpadding="10">
                                            <tr>
                                                <td align="center">
                                                    <asp:label id="Label6" runat="server" Text="Departamentos seleccionados"></asp:label><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:ListBox ID="lstSeleccionados2" runat="server" Height="300px" Width="350px" SelectionMode="Multiple"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <table runat="server" align="center" id="TblGeneralDescarga">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:RadioButtonList runat="server" ID="rbtnRPTFormatos" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
                                <asp:ListItem Value="PDF" Selected="True">PDF</asp:ListItem>
								<asp:ListItem Value="EXCEL">EXCEL</asp:ListItem>
                            </asp:RadioButtonList><br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:button id="btnGenerarReporte" runat="server" Text="Generar reporte" CssClass="btn"></asp:button>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:label id="lblConteoConsolidado" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:label id="lblTiempoProceso" runat="server" Text="Tiempo Proceso:"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
         <tr>
             <td>
                 <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
             </td>
         </tr>
    </table> 
</asp:Content>
