﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_ArticulosNoCatalogados
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject
	Dim txtAlmacen As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageRPT_ArticulosNoCatalogados.Visible = False

		If Session("Sociedad").ToString = "3" Then
			Me.lblAlmacen.Visible = False
			Me.cboAlmacen.Visible = False
		End If

		If Not Page.IsPostBack Then
			If Session("Sociedad").ToString <> "3" Then Almacen_Buscar()
			Session.Remove("RPT_ArtNoCatalogados")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			If Session("Sociedad").ToString <> "3" AndAlso Me.cboAlmacen.SelectedIndex = 0 Then
				CMensajes.MostrarMensaje("Seleccione un almacén.", CMensajes.Tipo.GetError, wucMessageRPT_ArticulosNoCatalogados)
			Else
				Dim almacen As String

				If Session("Sociedad").ToString = "3" Then
					almacen = "A001"
				Else
					almacen = Me.cboAlmacen.SelectedValue
				End If

				CatalogoReportes_CRV13(almacen)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_ArticulosNo_Catalogados.DataBinding
		If Session("RPT_ArtNoCatalogados") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_ArticulosNo_Catalogados.ReportSource = rpt
				Session("RPT_ArtNoCatalogados") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_ArticulosNo_Catalogados.ReportSource = Session("RPT_ArtNoCatalogados")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = ArticulosNoCatalogados(Almacen)

		Dim nomReporte As String = IIf(Session("Sociedad").ToString = "3", NombreReportes.RPT_ArticulosNoCatalogados_RST, NombreReportes.RPT_ArticulosNoCatalogados)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & nomReporte))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			If Session("Sociedad").ToString <> "3" Then txtAlmacen = rpt.ReportDefinition.Sections("Section1").ReportObjects("TxtAlmacen")
			txtNombreReporte.Text = "ARTICULOS NO CATALOGADOS"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			If Session("Sociedad").ToString <> "3" Then txtAlmacen.Text = IIf(Almacen = "0", "Todos", Almacen)
			rpt.SetDataSource(ds.Tables(0))
			RPT_ArticulosNo_Catalogados.ReportSource = rpt
			RPT_ArticulosNo_Catalogados.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_ArticulosNoCatalogados)
		End If
	End Sub

	Private Function ArticulosNoCatalogados(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@Almacen", Almacen)
			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados", par)
			End If
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(2) As SqlParameter
				par2(0) = New SqlParameter("@Tipo", Session("Perfil"))
				par2(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par2(2) = New SqlParameter("@Almacen", Almacen)

				If Session("Sociedad").ToString = "3" Then
					dsExcel = DatosSQL.funcioncp("RPT_ArticulosNoCatalogadosExcel_RST", par2)
				Else
					dsExcel = DatosSQL.funcioncp("RPT_ArticulosNoCatalogadosExcel", par2)
				End If
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					If Session("Sociedad").ToString = "3" Then
						Reportes.REPEXCEL(dsExcel, "Reporte Articulos No Catalogados", "ArtNoCatalogados", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), False)
					Else
						Reportes.REPEXCEL(dsExcel, "Reporte Articulos No Catalogados", "ArtNoCatalogados", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), True)
					End If
				End If
			End If
			Return ds
		Catch ex As Exception
			Return Nothing
		End Try
	End Function

	Sub Almacen_Buscar()
		Try
			Dim par(1) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class