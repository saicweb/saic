﻿Imports System.Data.SqlClient
Imports System.IO

Public Class SurtirArticuloMarbete
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("SurtirMarbete_Marbete") Is Nothing OrElse Session("SurtirMarbete_Almacen") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageSurtirArtMarbete.Visible = False

		If Not IsPostBack Then
			lblMarbete.Text = Session("SurtirMarbete_Marbete")
			lblAlmacen.Text = Session("SurtirMarbete_Almacen")
			LlenarGrid()
		End If
	End Sub

	Sub LlenarGrid()
		Try
			Dim Tabla As New DataTable
			If Tabla.Columns.Count = 0 Then
				Tabla.Columns.Add("ID")
			End If
			For x As Integer = 0 To 19
				Dim Ren As DataRow
				Ren = Tabla.NewRow
				Ren.Item(0) = x + 1
				Tabla.Rows.InsertAt(Ren, x)
			Next
			Me.RadGrid1.DataSource = Tabla
			RadGrid1.DataBind()
			'Me.RadGrid1.Rebind()
			For x As Integer = 0 To Me.RadGrid1.Rows.Count - 1
				Me.RadGrid1.Rows(x).Cells(0).ForeColor = Drawing.Color.Black
			Next
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function Validar() As Boolean
		Dim ContadorTotal As Integer = 0
		Dim ContadorOK As Integer = 0
		Dim linea As New List(Of String)
		Try
			'Se recorre el datagrid en busca de errores
			For x As Integer = 1 To 20
				Dim EAN As TextBox
				Dim Descripcion As TextBox
				Dim Cantidad As TextBox
				EAN = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox1"), TextBox)
				Descripcion = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox2"), TextBox)
				Cantidad = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox3"), TextBox)
				'Si la caja de EAN contiene datos procedemos a las validaciones
				If EAN.Text.Length > 0 Then
					ContadorTotal = ContadorTotal + 1
					'Validamos que el campo cantidad sea proporcionado
					If Cantidad.Text.Length > 0 Then
						'Validamos que el campo cantidad sea un valor numérico no negativo
						If IsNumeric(Cantidad.Text.Trim) = False OrElse CDbl(Cantidad.Text.Trim) < 0 Then
							CMensajes.Show("Favor de verificar el campo cantidad de la línea " & CStr(x))
							Me.RadGrid1.Rows(x - 1).Cells(0).ForeColor = Drawing.Color.Red
							Return False
							Exit Function
						End If
						'El resultado de las validaciones fue correcto
						Me.RadGrid1.Rows(x - 1).Cells(0).ForeColor = Drawing.Color.Black
						ContadorOK = ContadorOK + 1
					Else
						'CMensajes.Show("Favor de proporcionar la cantidad en la línea: " & CStr(x))
						linea.Add(x)
						Me.RadGrid1.Rows(x - 1).Cells(0).ForeColor = Drawing.Color.Red
					End If
				End If
			Next

			If linea IsNot Nothing AndAlso linea.Count > 0 Then
				Dim lineas As String = Nothing
				For Each item In linea
					If lineas IsNot Nothing Then
						lineas = ", " & item
					Else
						lineas = item
					End If
				Next
				CMensajes.MostrarMensaje("Favor de proporcionar la cantidad en la(s) línea(s): " & lineas, CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
			End If

			If ContadorTotal > ContadorOK Then
				Return False
			ElseIf ContadorTotal = ContadorOK Then
				Return True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
		If Validar() = True Then
			insertar()
		End If
	End Sub

	Sub insertar()
		Try
			'Se procede a la inserción de datos en BD
			For x As Integer = 1 To 20
				Dim EAN As TextBox
				Dim Cantidad As TextBox
				EAN = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox1"), TextBox)
				Cantidad = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox3"), TextBox)
				If EAN.Text.Length > 0 Then
					Registrar(lblMarbete.Text, EAN.Text, Cantidad.Text, lblAlmacen.Text)

				End If
			Next

			LlenarGrid()
			Session("Msg") = "Los artículos fueron agregados correctamente."
			Response.Redirect("SurtirpisoVenta.aspx?SID=" & Request.QueryString("SID"), False)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function Registrar(Marbete As String, EAN As String, Cant_Surtir As String, Almacen As String) As DataSet
		Dim ds As New DataSet
		Dim par(7) As SqlParameter
		par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
		par(1) = New SqlParameter("@Terminal", Session("IdUsuario"))
		par(2) = New SqlParameter("@Marbete", Marbete)
		par(3) = New SqlParameter("@EAN", EAN)
		par(4) = New SqlParameter("@Cant_Surtir", Cant_Surtir)
		par(5) = New SqlParameter("@Almacen", Almacen)
		par(6) = New SqlParameter("@Perfil", Session("Perfil"))
		par(7) = New SqlParameter("@Accion", 1)

		ds = DatosSQL.funcioncp("ConteosDispositivo_Surtir", par)
		Return ds

	End Function

	Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
		Try

			Dim ArchivoSeleccionado As Boolean = RadUpload1.HasFile 'Me.RadUpload1.UploadedFiles.Count > 0
			If ArchivoSeleccionado = False Then
				CMensajes.MostrarMensaje("Debe seleccionar el archivo a cargar antes de continuar.", CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
			Else
				Me.btnAgregar.Enabled = False
				Me.btnAgregar.Text = "Subiendo archivo..."

				Dim Extension As String = ""
				Dim Archivo As String = RadUpload1.FileName 'Me.RadUpload1.UploadedFiles(0).GetName
				Dim IdTienda As String = Session("IdTienda")
				Dim dt As New DataTable

				Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

				If Not Directory.Exists(DirectorioArchivos) Then
					Directory.CreateDirectory(DirectorioArchivos)
				End If

				If Me.RadioButtonList1.SelectedIndex = 0 Then
					If Archivo.EndsWith(".xls") = False Then
						CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .xls.", CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
					ElseIf Archivo.StartsWith(IdTienda) = False Then
						CMensajes.MostrarMensaje("El archivo seleccionado no corresponde al de la tienda, favor de verificar.", CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
					Else
						DirectorioArchivos = DirectorioArchivos & Archivo
						Me.RadUpload1.SaveAs(DirectorioArchivos)

						If ExcelClass.ValidarExcel(DirectorioArchivos) Then
							dt = ExcelClass.LeerExcel(DirectorioArchivos)
							Dim EAN, Cantidad As String
							For x As Integer = 0 To dt.Rows.Count - 1
								If IsDBNull(dt.Rows(x).Item(0)) Or IsDBNull(dt.Rows(x).Item(1)) Then
									Continue For
								End If
								EAN = dt.Rows(x).Item(0)
								Cantidad = dt.Rows(x).Item(1)

                                If EAN.Trim <> "EAN" AndAlso Cantidad.Trim <> "Cantidad" AndAlso EAN <> "" AndAlso Cantidad <> "" Then
                                    Registrar(lblMarbete.Text, EAN.Trim, Cantidad.Trim, lblAlmacen.Text)
                                End If
                            Next
							Session("Msg") = "Los artículos fueron agregados correctamente."
							Response.Redirect("SurtirpisoVenta.aspx?SID=" & Request.QueryString("SID"), False)
						Else
							CMensajes.MostrarMensaje("El archivo no es válido.", CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
						End If
					End If
				ElseIf Me.RadioButtonList1.SelectedIndex = 1 Then
					If Archivo.EndsWith(".txt") = False AndAlso Archivo.EndsWith(".TXT") = False Then
						CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .txt.", CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
					ElseIf Archivo.StartsWith(IdTienda) = False Then
						CMensajes.MostrarMensaje("El archivo seleccionado no corresponde al de la tienda, favor de verificar.", CMensajes.Tipo.GetError, wucMessageSurtirArtMarbete)
					Else
						DirectorioArchivos = DirectorioArchivos & Archivo
						Me.RadUpload1.SaveAs(DirectorioArchivos)

						Dim SR As StreamReader
						SR = New StreamReader(DirectorioArchivos)
						Dim Linea As String
						Dim Registro() As String
						Dim EAN As String
						Dim Cantidad As String
						Dim x As Integer = -2
						Linea = SR.ReadLine()

						Do
							x = x + 1
							If Linea Is Nothing Then
								Exit Do
							End If
							Registro = Linea.Split("|")
							EAN = Registro(0)
							Cantidad = Registro(1)
                            If EAN.Trim <> "EAN" AndAlso Cantidad.Trim <> "Cantidad" AndAlso EAN <> "" AndAlso Cantidad <> "" Then
                                Registrar(lblMarbete.Text, EAN.Trim, Cantidad.Trim, lblAlmacen.Text)
                            End If
                            Linea = SR.ReadLine()
						Loop Until Linea Is Nothing

						LlenarGrid()
						SR.Close()
						Session("Msg") = "Los artículos fueron agregados correctamente."
						Response.Redirect("SurtirpisoVenta.aspx?SID=" & Request.QueryString("SID"), False)
					End If
				End If
				Me.btnAgregar.Enabled = True
				Me.btnAgregar.Text = "Cargar archivo"
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class