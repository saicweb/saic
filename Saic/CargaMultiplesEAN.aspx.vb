﻿Imports System.IO

Public Class CargaMultiplesEAN
	Inherits System.Web.UI.Page

	Dim dtErrores As DataTable

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Not Page.IsPostBack Then
			Me.btnCargar.Attributes.Add("onclick", "this.disabled=true;" + GetPostBackEventReference(btnCargar).ToString())
		End If
	End Sub

	Sub dtErrores_Configurar()
		Try
			dtErrores = New DataTable

			' Estructura de la tabla
			dtErrores.Columns.Add("Renglon", GetType(String))
			dtErrores.Columns.Add("Mensaje", GetType(String))

			Session("dtErrores") = dtErrores
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub dtErrores_Agregar(ByVal Renglon As String, ByVal Mensaje As String)
        Try
            dtErrores = CType(Session("dterrores"), DataTable)
            Dim drErrores As DataRow
            drErrores = dtErrores.NewRow

            drErrores("Renglon") = Renglon
            drErrores("Mensaje") = Mensaje

            dtErrores.Rows.Add(drErrores)
            Session("dtErrores") = dtErrores
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

	Private Sub btnCargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCargar.Click
		Me.lblTiempoProceso.Visible = False
		Me.lblErroresPresentados.Visible = False
		Me.dgErrores.Visible = False

		Dim singleResultExist As Boolean = singleUpload.HasFile
		Dim strRuta As String = ""
		Me.btnCargar.Enabled = False

		Try
			Me.btnCargar.Text = "Subiendo archivo..."
			Me.btnCargar.Enabled = False

			Me.lblTiempoProceso.Text = "Hora de Inicio: " & Now.ToShortTimeString
			CMensajes.MostrarMensaje("Procesando...", CMensajes.Tipo.GetInformativo, wucMessageMensaje)

			Dim nomArchivo As String

			If singleResultExist Then
				nomArchivo = singleUpload.FileName
				If singleUpload.FileName.ToUpper.EndsWith(".TXT") Then
					Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

					If Not Directory.Exists(DirectorioArchivos) Then
						Directory.CreateDirectory(DirectorioArchivos)
					End If

					strRuta = DirectorioArchivos
					nomArchivo = "MultiplesEAN.TXT"
					strRuta &= nomArchivo
					Session("nomArchivo") = nomArchivo
					singleUpload.SaveAs(strRuta)
					Dim Archivo As New FileInfo(strRuta)
					Dim status As Boolean = False
					status = inicializa()
					If status = False Then
						CMensajes.MostrarMensaje("No fue posible inicializar el catálogo de múltiples EAN.", CMensajes.Tipo.GetError, wucMessageMensaje)
					Else
						Me.GuardarArchivo(Archivo, 5)
					End If
				Else
					CMensajes.MostrarMensaje("El archivo a cargar debe ser un archivo de texto con extension .txt.", CMensajes.Tipo.GetError, wucMessageCargaMultiplesEAN)
				End If
			Else
				CMensajes.MostrarMensaje("No se ha especificado el nombre del archivo a cargar.", CMensajes.Tipo.GetError, wucMessageCargaMultiplesEAN)
			End If
			Me.lblTiempoProceso.Text &= "  Hora de Termino: " & Now.ToShortTimeString
			Me.wucMessageMensaje.Visible = False
			Me.btnCargar.Enabled = True
			Me.btnCargar.Text = "Subir catálogo"
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		Finally
			If strRuta <> "" And File.Exists(strRuta) = True Then
				File.Delete(strRuta)
			End If
		End Try
	End Sub

	''' <summary>
	''' Cargar archivo
	''' </summary>
	''' <param name="columnas"></param>
	''' <returns></returns>
	Private Function CargarArchivo(ByVal columnas As Integer)
		Dim strRuta As String = ""
		Dim regreso As Boolean = False
		Try
			Dim nomArchivo As String
			Dim singleResultExist As Boolean = singleUpload.HasFile
			If singleResultExist Then
				nomArchivo = singleUpload.FileName
				If singleUpload.FileName.ToUpper.EndsWith(".TXT") Then
					Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

					If Not Directory.Exists(DirectorioArchivos) Then
						Directory.CreateDirectory(DirectorioArchivos)
					End If

					strRuta = DirectorioArchivos
					nomArchivo = "MultiplesEAN.TXT"
					strRuta &= nomArchivo
					Session("nomArchivo") = nomArchivo
					singleUpload.SaveAs(strRuta)
					Dim Archivo As New FileInfo(strRuta)
					Me.GuardarArchivo(Archivo, columnas)
				Else
					CMensajes.MostrarMensaje("El archivo a cargar debe ser un archivo de texto con extension .txt.", CMensajes.Tipo.GetError, wucMessageCargaMultiplesEAN)
				End If
			Else
				CMensajes.MostrarMensaje("No se ha especificado el nombre del archivo a cargar.", CMensajes.Tipo.GetError, wucMessageCargaMultiplesEAN)
			End If
			Return regreso
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		Finally
			If strRuta <> "" And File.Exists(strRuta) = True Then
				File.Delete(strRuta)
			End If
		End Try
	End Function

	Private Sub GuardarArchivo(ByVal Archivo As FileInfo, ByVal Columnas As Integer)
		Dim LECTOR As New StreamReader(Archivo.FullName)
		Try
			Me.dtErrores_Configurar()

			Dim MATERIAL As String, UM As String, EAN As String, TP As String, J As String

			Dim LINEA As String
			Dim RenCorrectos As Integer = 0
			Dim i As Integer = 0

			Do
				LINEA = LECTOR.ReadLine()
				'si ya llego al fin de los rows
				If LINEA Is Nothing Then
					Exit Do
				End If
				i = i + 1
				'leer datos 
				Dim datos() As String
				datos = LINEA.Split("|")
				If datos.Length = 0 Then
					Exit Do
				Else
					MATERIAL = ""
					UM = ""
					EAN = ""
					TP = ""
					J = ""

					MATERIAL = Replace(datos(0), Chr(34), "").TrimEnd 'TextoQuitarComillas(datos(0))
					UM = Replace(datos(1), Chr(34), "").TrimEnd  'TextoQuitarComillas(datos(1))
					EAN = Replace(datos(2), Chr(34), "").TrimEnd  'TextoQuitarComillas(datos(2))
					TP = Replace(datos(3), Chr(34), "").TrimEnd  'TextoQuitarComillas(datos(3))
					J = Replace(datos(4), Chr(34), "").TrimEnd  'TextoQuitarComillas(datos(4))


					If MATERIAL = "" Then 'Material
						Me.dtErrores_Agregar(i, "No se encontró el campo MATERIAL.")
					ElseIf UM = "" Then 'UM
						Me.dtErrores_Agregar(i, "No se encontro el campo UM.")
					ElseIf EAN = "" Then
						Me.dtErrores_Agregar(i, "No se encontro el campo EAN.")
					ElseIf TP = "" Then 'TP
						Me.dtErrores_Agregar(i, "No se encontro el campo TP.")
					ElseIf J = "" Then
						Me.dtErrores_Agregar(i, "No se encontro el campo J.")
					ElseIf MATERIAL.Trim.Length > 18 Then
						Me.dtErrores_Agregar(i, "Campo MATERIAL excede la longitud permitida.")
					ElseIf UM.Trim.Length > 2 Then
						Me.dtErrores_Agregar(i, "Campo UM excede la longitud permitida.")
					ElseIf EAN.Trim.Length > 18 Then
						Me.dtErrores_Agregar(i, "Campo EAN excede la longitud permitida.")
					ElseIf TP.Trim.Length > 2 Then
						Me.dtErrores_Agregar(i, "Campo TP excede la longitud permitida.")
					ElseIf J.Trim.Length > 1 Then
						Me.dtErrores_Agregar(i, "Campo J excede la longitud permitida.")
					Else
						RenCorrectos += 1
					End If
				End If
			Loop
			LECTOR.Close()

			dtErrores = CType(Session("dterrores"), DataTable)
			If dtErrores.Rows.Count > 0 Then
				Me.lblErroresPresentados.Visible = True
				Me.dgErrores.Visible = True
				Me.dgErrores.DataSource = dtErrores
				Me.dgErrores.DataBind()
				CMensajes.MostrarMensaje("Favor de corregir archivo.", CMensajes.Tipo.GetError, wucMessageCargaMultiplesEAN)
			Else
				Me.dgErrores.Visible = False
				Me.lblErroresPresentados.Visible = False

				If Convert.ToInt64(RenCorrectos) > 0 Then
					Dim insert As Boolean = False
					insert = BulkCopy.Insertar(Archivo.FullName, "|", CReglasNegocio.MEAN)
					CMensajes.MostrarMensaje("Se insertaron los valores correctamente.", CMensajes.Tipo.GetExito, wucMessageCargaMultiplesEAN)
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		Finally
			Archivo.Delete()
			Session("dterrores") = Nothing
		End Try
	End Sub

	Function inicializa() As Boolean
        Try
            inicializa = False
            DatosSQL.procedimientosp("MultiplesEAN_Borrar")
            Return True

        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Function

End Class