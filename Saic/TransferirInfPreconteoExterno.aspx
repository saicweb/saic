﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TransferirInfPreconteoExterno.aspx.vb" Inherits="Saic.TransferirInfPreconteoExterno" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%"  >
        <tr>
            <td class="titulos">
                Transferir información preconteo externo al SAIC
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageTransferirInfPreconteoExterno" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="45%" align="center">
                    <tr align="center">
                        <td colspan="2">
                            <span><asp:Button runat="server" Text="Transferir información" ID="btnTransferir" CssClass="btn" Visible="false" /></span>
                        </td>
                    </tr>
                    <tr align="center">
                        <td><br />
                            <asp:Label runat="server" Text="" ID="lblTiempoProceso"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>  
    
    <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
</asp:Content>