﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DescargaArchivoCierre.aspx.vb" Inherits="Saic.DescargaArchivoCierre" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Cierre de inventario" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Descarga archivo cierre" ID="lblTitulo" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageDecargaArchivoCierre" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" cellpadding="10" width="90%">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="Fecha inicio: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfechainicio" runat="server" Width="120px"></asp:TextBox>
                            <asp:CalendarExtender ID="txtfechainicio_CalendarExtender" runat="server" BehaviorID="txtfechainicio_CalendarExtender"
                                Format="yyyy-MM-dd" TargetControlID="txtfechainicio" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtfechainicio" ErrorMessage="yyyy-MM-dd" ValidationExpression="^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$" CssClass="alert-error"></asp:RegularExpressionValidator>
                        </td>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" Text="Fecha fin: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtfechafin" runat="server" Width="120px"></asp:TextBox>
                            <asp:CalendarExtender ID="txtfechafin_CalendarExtender1" runat="server" BehaviorID="txtfechafin_CalendarExtender"
                                Format="yyyy-MM-dd" TargetControlID="txtfechafin" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtfechafin" ErrorMessage="yyyy-MM-dd" ValidationExpression="^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$" CssClass="alert-error"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><br />
                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" CssClass="btn"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table runat="server" align="center"> 
                                <tr>
                                    <td align="center"><br />
                                        <asp:Panel runat="server" Height="350px" ScrollBars="Auto" ID="gridSubfamilia">
                                            <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" SkinID="Grid3">
                                                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                                <Columns>
                                                    <asp:BoundField HeaderText="Id" DataField="IdInventario" HeaderStyle-Width="80px" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
                                                    <asp:BoundField HeaderText="Tienda" DataField="IdTienda" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField HeaderText="Fecha inventario" DataField="FechaInventario" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField HeaderText="Tipo inventario" DataField="TipoInventario" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
                                                    <asp:BoundField HeaderText="Archivo" DataField="NombreArchivo" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField HeaderText="Ruta" DataField="Ruta" HeaderStyle-Width="300px" ItemStyle-Width="300px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField HeaderText="Usuario" DataField="NombreUsuario" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField HeaderText="Fecha incio" DataField="FechaInicio" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField HeaderText="Fecha término" DataField="FechaTermino" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
					                            </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
