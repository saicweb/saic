﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Corregir_NoCatalogados.aspx.vb" Inherits="Saic.Corregir_NoCatalogados" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Corregir no catalogados" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCorregir_NoCatalogados" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="75%" align="center">
                    <tr>
                        <td align="center">
                            <asp:Panel runat="server" Height="350px" Width="655px" ScrollBars="Auto">
                                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="false" SkinID="DGrid3">
                                    <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:BoundColumn DataField="IdMarbete" ReadOnly="true" HeaderText="Marbete" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
									    <asp:TemplateColumn HeaderText="EAN" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px">
										    <ItemTemplate>
											    <asp:TextBox id="txtean" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EAN") %>'></asp:TextBox>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:BoundColumn HeaderText="Descripci&#243;n" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="200px" ItemStyle-Width="200px"></asp:BoundColumn>
									    <asp:BoundColumn DataField="conteo" HeaderText="Cantidad" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" DataFormatString="{0:###,##0.00}"></asp:BoundColumn>
									    <asp:TemplateColumn HeaderText="Acci&#243;n" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
										    <ItemTemplate>
											    <asp:LinkButton id="lnkmodificar" runat="server" Text="Modificar" CausesValidation="false" CommandName="Edit">Modificar</asp:LinkButton>
										    </ItemTemplate>
									        <EditItemTemplate>
										        <asp:LinkButton id="LinkButton3" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
										        <asp:LinkButton id="LinkButton2" runat="server" Text="Cancel" CausesValidation="false" CommandName="Cancel"></asp:LinkButton>
									        </EditItemTemplate>
									    </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
