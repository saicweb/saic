﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_ResumenDifDepto_PrevioCierre.aspx.vb" Inherits="Saic.RPT_ResumenDifDepto_PrevioCierre" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                Resumen por departamento
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageRPT_ResumenDifDepto_PrevioCierre" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="45%" align="center" id="TblCabecero">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacen: "></asp:Label>
                        </td>
                        <td><br />
                            <asp:DropDownList runat="server" id="cboAlmacen" Width="150px" CssClass="dropdown" ToolTip="Seleccionar Tienda si es un Usuario Administrador"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="90%">
                    <tr>
                        <td align="center" width="700px"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="false">Descargar Excel</asp:hyperlink></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <div style="z-index:-1">
                                <cr:crystalreportviewer Id="RPT_ResumenDifDepto_Previo_Cierre" runat="server"  HasCrystalLogo="False"
                                AutoDataBind="True" OnDataBinding="_reportViewer_DataBinding" Height="50px"  EnableParameterPrompt="false" EnableDatabaseLogonPrompt="false" ToolPanelWidth="200px" 
                                Width="500px" ToolPanelView="None" HasRefreshButton="True" ShowAllPageIds="True" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" HasDrilldownTabs="False"></cr:crystalreportviewer>  
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
