﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="InicializarInventario.aspx.vb" Inherits="Saic.InicializarInventario" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Inicializar rotativo" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Inicializar inventario" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageInicializarInventario" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="52%" align="center">
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label1" runat="server" Width="250px" Text="Fecha inventario: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="dateFechaInventario" runat="server" Width="132px"></asp:TextBox>
                            <asp:CalendarExtender ID="dateFechaInventario_CalendarExtender" runat="server" BehaviorID="dateFechaInventario_CalendarExtender"
                                Format="yyyy-MM-dd" TargetControlID="dateFechaInventario" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="dateFechaInventario" ErrorMessage="yyyy-MM-dd" ValidationExpression="^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$" CssClass="alert-error"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label4" runat="server" Width="250px" Text="Tipo de inventario: "></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="cboTipoInventario" runat="server" Width="235px" AutoPostBack="true" CssClass="dropdown">
                                <asp:ListItem Selected="true" Value="X" Text="[Seleccione]"></asp:ListItem>
                                <asp:ListItem Value="G" Text="General"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Inventario Parcial o Ajustes"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label5" runat="server" Width="250px" Text="Nombre del gerente: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtGerente" runat="server" Width="270px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtGerente" Display="Dynamic"
                             ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSubGerenteAdmon" runat="server" Width="250px" Text="Nombre del sub-gerente administrativo:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubGerenteAdmvo" runat="server" Width="270px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSubGerenteAdmvo"
                                Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblSubGerenteComercial" runat="server" Width="250px" Text="Nombre del sub-gerente comercial: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubGerenteComercial" runat="server" Width="270px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSubGerenteComercial"
                                Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblJefeDepto" runat="server" Enabled="False" Width="250px" Text="Nombre del jefe de departamento: "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtJefeDepto" runat="server" Width="270px" MaxLength="50" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblNomJefeControlInv" runat="server" Width="250px" Text="Nombre del jefe de control de inventarios:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtJefeControlInventarios" runat="server" Width="270px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtJefeControlInventarios"
                                Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="lblNomResAdmonInv" runat="server" Width="250px" Enabled="False" Text="Nombre del responsable de administración de inventarios:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtreponsableAdmonInventario" runat="server" Width="270px" MaxLength="50" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnInicializar" runat="server" Text="Iniciar inventario" CssClass="btn"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
