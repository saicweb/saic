﻿Public Class VisorCierresEnEspera
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageVisorCierres.Visible = False

		If Not Page.IsPostBack Then
			LlenarGrid()
			Dim Intervalo As Integer = ConfigurationManager.AppSettings("TiempoVisorCierre").ToString
			UpdateTimer.Interval = Intervalo
		End If
	End Sub

	Sub LlenarGrid()
		Try
			Dim ds As New DataSet
			Dim sentencia As String = "VisorCierresEspera"

			ds = DatosSQL.FuncionSP(sentencia)

			'Armar la tabla
			Dim dt As New DataTable

			dt.Columns.Add("ID")
			dt.Columns.Add("Tienda")
			dt.Columns.Add("Estatus")
			dt.Columns.Add("Hora")

			Dim dr As DataRow
			Dim Estatus, CierreInv, hora As String
			For i As Short = 0 To ds.Tables(0).Rows.Count - 1
				CierreInv = ds.Tables(0).Rows(i).Item("CierreInventario")
				Estatus = ds.Tables(0).Rows(i).Item("Estatus")
				hora = ds.Tables(0).Rows(i).Item("Hora")

				dr = dt.NewRow
				dr("ID") = i + 1
				dr("Tienda") = ds.Tables(0).Rows(i).Item("IdTienda")
				If Estatus = "C" And CierreInv.ToUpper = "PENDIENTE" Then 'Si se está ejecutando
					dr("Estatus") = "En proceso"
					dr("Hora") = hora
				ElseIf Estatus = "P" And CierreInv.ToUpper = "PENDIENTE" Then
					dr("Estatus") = "En espera"
					dr("Hora") = "..."
				End If

				dt.Rows.Add(dr)
			Next

			If dt.Rows.Count = 0 Then
				dt.Rows.Add("No records to display.")
			End If

			Me.dg.DataSource = dt
			Me.dg.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub UpdateTimer_Tick(sender As Object, e As System.EventArgs) Handles UpdateTimer.Tick
		LlenarGrid()
		lblhora.Text = DateTime.Now.ToString()
	End Sub
End Class