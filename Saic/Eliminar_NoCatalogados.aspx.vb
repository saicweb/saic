﻿Imports System.Data.SqlClient

Public Class Eliminar_NoCatalogados
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageEliminar_NoCatalogados.Visible = False

		If Not Page.IsPostBack Then
			Try
				NoCatalogados_Buscar()
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			For i As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				Dim marbete As String = ""
				Dim EAN As String = ""
				chkRow = CType(Me.DataGrid1.Items(i).FindControl("CheckBox2"), CheckBox)
				If chkRow.Checked = True Then
					marbete = Me.DataGrid1.Items(i).Cells(0).Text
					EAN = Me.DataGrid1.Items(i).Cells(1).Text
					Dim PAR(3) As SqlParameter
					PAR(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
					PAR(1) = New SqlParameter("@Perfil", Left(Session("Perfil"), 1))
					PAR(2) = New SqlParameter("@IdMarbete", marbete)
					PAR(3) = New SqlParameter("@EAN", EAN)
					DatosSQL.procedimientocp("Eliminar_NoCatalogados", PAR)
				End If
			Next
			NoCatalogados_Buscar()
			CMensajes.MostrarMensaje("Se eliminaron los registros seleccionados.", CMensajes.Tipo.GetExito, wucMessageEliminar_NoCatalogados)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageEliminar_NoCatalogados.Visible = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Sub NoCatalogados_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@Almacen", "0")

			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados", par)
			End If
			If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.DataGrid1.DataSource = ds.Tables(0)
				Me.DataGrid1.DataBind()
				If Session("Sociedad").ToString = "3" Then
					Me.DataGrid1.Columns(2).Visible = False
				End If
				Me.btnEliminar.Enabled = True
			Else
				Me.DataGrid1.DataSource = ds.Tables(0)
				Me.DataGrid1.DataBind()
				Me.btnEliminar.Enabled = False
				CMensajes.MostrarMensaje("No se encntraron artículos no catalogados.", CMensajes.Tipo.GetError, wucMessageEliminar_NoCatalogados)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chkHeader As CheckBox = CType(Me.DataGrid1.Controls(0).Controls(0).FindControl("CheckBox1"), CheckBox)
		If chkHeader.Checked = True Then
			For x As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DataGrid1.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = True
			Next
		Else
			For x As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DataGrid1.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = False
			Next
		End If
	End Sub

	Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
		Try
			Dim seleccionados As Integer = 0
			For i As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DataGrid1.Items(i).FindControl("CheckBox2"), CheckBox)
				If chkRow.Checked = True Then
					seleccionados += 1
				End If
			Next
			If seleccionados = 0 Then
				CMensajes.MostrarMensaje("Ejecución no iniciada, no ha seleccionado ningún elemento.", CMensajes.Tipo.GetError, wucMessageEliminar_NoCatalogados)
			Else
				Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
				lblTexto.Text = "<div align=""center"">Ésta acción eliminará los EAN no catalogados. ¿Está seguro de que seleccionó los ya resueltos?</div>"
				winConfirm1.MuestraDialogo()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class