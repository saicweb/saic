﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CargaMateriales_0304.aspx.vb" Inherits="Saic.CargaMateriales_0304" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server"  width="100%">
        <tr>
            <td class="titulos">
                Carga material 03 04
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCambiarMateriales_0304" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="40%" align="center">
                    <tr align="center">
                        <td>
                            <asp:UpdatePanel ID="upl" runat="server">
                                <ContentTemplate>
                                     <asp:FileUpload ID="RadUpload1" runat="server" CssClass="upload" /><br /><br />
                                    <span><asp:Button runat="server" Text="Subir catálogo" ID="btnCambiar" CssClass="btn" /></span>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnCambiar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
