﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CorreccionesFinalesPorHoja.aspx.vb" Inherits="Saic.CorreccionesFinalesPorHoja" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Corecciones finales 80/20 por hoja" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucmessage ID="wucMessageCorreccionesFinalesPorHoja" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" cellpadding="10">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Departamento: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboDepto" runat="server" Width="300px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Almacén: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboAlmacenes" runat="server" Width="100px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="No. Hoja: " ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtHoja" runat="server" MaxLength="9" Width="80px"></asp:TextBox> 
                        </td>
                        <td align="center">
                            <span><asp:Button runat="server" Text="Consultar" ID="btnConsultar" CssClass="btn"/></span> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" align="center">
                    <tr>
                        <td>
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                     <Columns>
                                        <asp:BoundField HeaderText="No." DataField="IdRow" HeaderStyle-Width="80px" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="EAN" DataField="EAN" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="Descripci&#243;n" DataField="Descripcion" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"/>
                                        <asp:BoundField HeaderText="Cantidad actual" DataField="CantidadActual" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle"/>
                                        <asp:TemplateField HeaderText="Cantidad final" HeaderStyle-Width="95px" ItemStyle-Width="95px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:TextBox id="txtCantidadFinal" runat="server" Width="80px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <span><asp:Button runat="server" Text="Guardar" ID="btnGuardar" CssClass="btn" Visible="false" /></span> 
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
