﻿Imports System.Data.SqlClient
Imports System.IO
Imports dbAutoTrack.DataReports

Public Class AvanceConteosCedis
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageAvanceConteos.Visible = False
	End Sub

	Protected Sub btnEjecutar_Click(sender As Object, e As EventArgs) Handles btnEjecutar.Click
		If Me.cmbAvance.SelectedValue.ToString = "-1" Then
			CMensajes.MostrarMensaje("Seleccione una opción para mostrar.", CMensajes.Tipo.GetError, wucMessageAvanceConteos)
		ElseIf Me.cmbAvance.SelectedValue.ToString = "1" Then
			RackNacional()
		ElseIf Me.cmbAvance.SelectedValue.ToString = "2" Then
			CrossDock()
		End If
	End Sub

	Protected Sub GrdRackNacional_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrdRackNacional.RowCommand
		Dim iRow As Integer = Integer.Parse(IIf(e.CommandArgument.ToString = "", 0, e.CommandArgument.ToString))

		Dim rows As GridViewRow = Me.GrdRackNacional.Rows(iRow)
		Dim nivel As String = e.CommandName
		Dim pasillo As String = rows.Cells(0).Text

		LlenaGridModal(1, "Pasillo " & pasillo & " - Nivel " & nivel, nivel, pasillo)
		ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "pro();", True)
		Me.mppDetalle.Show()
	End Sub

	Protected Sub GrdCrossDock_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrdCrossDock.RowCommand
		Dim iRow As Integer = Integer.Parse(IIf(e.CommandArgument.ToString = "", 0, e.CommandArgument.ToString))

		Dim rows As GridViewRow = Me.GrdRackNacional.Rows(iRow)
		Dim almacen As String = rows.Cells(0).Text

		LlenaGridModal(2, "Almacén " & almacen,,, almacen)
		ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "pro();", True)
		Me.mppDetalle.Show()
	End Sub

	Protected Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
		ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "pro();", True)
		Me.mppDetalle.Hide()
	End Sub

	''' <summary>
	''' Reporte de Rack Nacional
	''' </summary>
	Private Sub RackNacional()
		RPT_MarbetesFaltantes("1")
		RPT_MarbeteMasUbicacion("1")
		LlenaGridRackNacional()
		Me.TblGrafica.Visible = True
		Me.GrdCrossDock.Visible = False
		Me.GrdRackNacional.Visible = True
		Me.lblTituloGrafica.Text = "Almacen: 0010 Racks Almacenamiento"
		Me.lblTituloGrafica.Visible = True
	End Sub

	''' <summary>
	''' Reporte Cross Dock
	''' </summary>
	Private Sub CrossDock()
		RPT_MarbetesFaltantes("2")
		RPT_MarbeteMasUbicacion("2")
		LlenaGridCrossDock()
		Me.TblGrafica.Visible = True
		Me.GrdRackNacional.Visible = False
		Me.GrdCrossDock.Visible = True
		Me.lblTituloGrafica.Text = "Conteos X Almacén"
		Me.lblTituloGrafica.Visible = True
	End Sub

	''' <summary>
	''' Llena resumen y genera reporte de los marbetes faltantes
	''' </summary>
	Private Sub RPT_MarbetesFaltantes(ByVal TipoReporte As String)
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))

			If TipoReporte = "1" Then
				ds = DatosSQL.funcioncp("RPT_MarbetesFaltantes_CDS", par)
			Else
				ds = DatosSQL.funcioncp("RPT_MarbetesFaltantesCD_CDS", par)
			End If

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.lblTotalMarbetes.Text = IIf(ds.Tables(1).Rows.Count > 0, ds.Tables(1).Rows(0)("TotalMarbetes").ToString, "0")
				Me.lblMarbetesFaltantes.Text = IIf(ds.Tables(2).Rows.Count > 0, ds.Tables(2).Rows(0)("MarbetesFaltantes").ToString, "0")
				REPORTE(ds, 1)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena Grid del Modal
	''' </summary>
	''' <param name="TipoReporte"></param>
	''' <param name="Titulo"></param>
	Private Sub LlenaGridModal(ByVal TipoReporte As String, ByVal Titulo As String, Optional ByVal Nivel As String = "", Optional ByVal Pasillo As String = "", Optional ByVal Almacen As String = "")
		Try
			Dim ds As New DataSet

			If TipoReporte = "1" Then
				Dim par(2) As SqlParameter
				par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(1) = New SqlParameter("@Nivel", Nivel)
				par(2) = New SqlParameter("@Pasillo", Pasillo)

				ds = DatosSQL.funcioncp("RPT_RackNacionalDetalle", par)
			Else
				Dim par(1) As SqlParameter
				par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(1) = New SqlParameter("@Almacen", Almacen)

				ds = DatosSQL.funcioncp("RPT_CrossDockDetalle", par)
			End If

			Me.lblTitleModal.Text = Titulo
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.GrdModal.DataSource = ds.Tables(0)
			Else
				Me.GrdModal.DataSource = Nothing
			End If
			Me.GrdModal.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena resumen y genera reporte de los marbetes con mas de una ubicación
	''' </summary>
	Private Sub RPT_MarbeteMasUbicacion(ByVal TipoReporte As String)
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))

			If TipoReporte = "1" Then
				ds = DatosSQL.funcioncp("RPT_MarbeteMasUbicacion_CDS", par)
			Else
				ds = DatosSQL.funcioncp("RPT_MarbeteMasUbicacionCD_CDS", par)
			End If

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(1).Rows.Count > 0 Then
				Me.lblMarbeteMasUbicacion.Text = IIf(ds.Tables(1).Rows.Count > 0, ds.Tables(1).Rows(0)("TotalMarbetes").ToString, "0")
				REPORTE(ds, 2)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena el grid de Rack Nacional
	''' </summary>
	Private Sub LlenaGridRackNacional()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_AvanceConteos", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				GrdRackNacional.DataSource = ds.Tables(0)
                GrdRackNacional.DataBind()
                ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "pro();", True)
            End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena el grid de Cross Dock
	''' </summary>
	Private Sub LlenaGridCrossDock()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_AvanceConteosCD", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				GrdCrossDock.DataSource = ds.Tables(0)
				GrdCrossDock.DataBind()
				ScriptManager.RegisterStartupScript(Me, Page.GetType, "Script", "pro();", True)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Crea reportes en pdf
	''' </summary>
	''' <param name="DATOS"></param>
	''' <param name="Reporte"></param>
	Sub REPORTE(ByVal DATOS As DataSet, ByVal Reporte As Integer)
		Try
			'//////////////////////////////////////////////////////////////////////
			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim settings As New dbAutoTrack.DataReports.PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Portrait
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			If Reporte = 1 Then
				report1 = New RPT_MarbetesFaltantes_CDS
			Else
				report1 = New RPT_MarbetesMasUbicacion_CDS
			End If

			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("AvanceConteosCedis")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim pathPdf As String = ""

			If Reporte = 1 Then
				pathPdf = DirectorioArchivos & "MarbetesFaltantes_" & Session("Perfil") & "_" & Session("IdUsuario") & ".pdf"
			Else
				pathPdf = DirectorioArchivos & "MarbetesMasUbicacion_" & Session("Perfil") & "_" & Session("IdUsuario") & ".pdf"
			End If

			report1.DataSource = DATOS

			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = Today.ToShortDateString
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = Today.ToShortTimeString
			CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = Session("IdTienda")

			document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)
			Dim encripta As New Encryption

			If DATOS IsNot Nothing AndAlso DATOS.Tables(0).Rows.Count > 0 Then
				If Reporte = 1 Then
					Me.lnkRptMarbetesFaltantes.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
					Me.lnkRptMarbetesFaltantes.Enabled = True
				Else
					Me.lnkRptMarbeteMasUbicacion.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
					Me.lnkRptMarbeteMasUbicacion.Enabled = True
				End If
			Else
				If Reporte = 1 Then
					Me.lnkRptMarbetesFaltantes.Enabled = False
				Else
					Me.lnkRptMarbeteMasUbicacion.Enabled = False
				End If
			End If
			'//////////////////////////////////////////////////
			'termina el reporte
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Sub


End Class