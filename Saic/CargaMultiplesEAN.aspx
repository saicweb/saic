﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CargaMultiplesEAN.aspx.vb" Inherits="Saic.CargaMultiplesEAN" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Carga múltiples EAN"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCargaMultiplesEAN" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="80%" align="center">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="uplXML" runat="server">
                                <ContentTemplate>
                                    <table runat="server" width="100%" align="center">
                                        <tr>
                                            <td align="center">
                                                <asp:FileUpload ID="singleUpload" runat="server" CssClass="upload"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2"><br />
                                                <asp:Button ID="btnCargar" runat="server" Text="Subir catálogo" CssClass="btn"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnCargar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"><br />
                            <asp:Label ID="lblTiempoProceso" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <wum:wucMessage ID="wucMessageMensaje" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"><br />
                            <asp:Label ID="lblErroresPresentados" runat="server" Text="Errores presentados:" Font-Bold="True" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Panel runat="server" ScrollBars="Auto" Width="515px" Height="250px">
                                <asp:DataGrid ID="dgErrores" runat="server" SkinID="DGrid3" AutoGenerateColumns="False" HorizontalAlign="Center" Visible="false">
                                    <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:BoundColumn DataField="Renglon" HeaderText="Renglón" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Mensaje" HeaderText="Mensaje" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="400px" ItemStyle-Width="400px">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
