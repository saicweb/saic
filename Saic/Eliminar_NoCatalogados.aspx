﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Eliminar_NoCatalogados.aspx.vb" Inherits="Saic.Eliminar_NoCatalogados" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Eliminar no catalogados" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageEliminar_NoCatalogados" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server">
                    <tr>
                        <td>
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="false" SkinID="DGrid3">
                                    <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:BoundColumn DataField="IdMarbete" ReadOnly="true" HeaderText="Marbete" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
										<asp:BoundColumn DataField="EAN" ReadOnly="true" HeaderText="C&#243;digo de barras" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
										<asp:BoundColumn DataField="estatus" HeaderText="Estatus" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="95px" ItemStyle-Width="95px"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="Seleccionar" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
											<HeaderTemplate>
												<asp:CheckBox id="CheckBox1" runat="server" Text="Todos" AutoPostBack="true" TextAlign="Left"
													OnCheckedChanged="SelectAll"></asp:CheckBox>
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox id="CheckBox2" runat="server"></asp:CheckBox>
											</ItemTemplate>
										</asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel> 
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <span><asp:Button ID="btnEliminar" runat="server" Text="Eliminar" CssClass="btn"></asp:Button></span>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
