﻿Imports System.Data.SqlClient
Public Class AdmonAreas
    Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As New Button
    Protected WithEvents btnConfirmNO As New Button

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If Not Page.IsPostBack Then
            Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If
				CosultarArea()
            Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
        End If
    End Sub

	Sub Area_Datos()
		Try
			Dim ds As New DataSet
			Dim PAR(0) As SqlParameter
			PAR(0) = New SqlParameter("@IdArea", Me.txtIdArea.Text.Trim)
			ds = DatosSQL.funcioncp("Areas_Buscar", PAR)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				'IdArea,NombreArea
				If IsDBNull(ds.Tables(0).Rows(0).Item("IdArea")) = False Then
					Me.txtIdArea.Text = ds.Tables(0).Rows(0).Item("IdArea")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreArea")) = False Then
					Me.txtNombreArea.Text = ds.Tables(0).Rows(0).Item("NombreArea")
				End If
			Else
				Me.btnModificar.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Areas_Administracion(1)
    End Sub
    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Areas_Administracion(2)
    End Sub


#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

    Sub cargaWinConfirm()
        winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
        Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
        txtTitutlo.Text = "Confirmar"

        btnConfirmNO = winConfirm1.FindControl("BtnNO")
        btnConfirmNO.Text = "No"
        AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

        btnConfirmSI = winConfirm1.FindControl("btnSI")
        btnConfirmSI.Text = "Sí"
        AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

        phConfirm.Controls.Add(winConfirm1)
    End Sub
#End Region

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Areas_Administracion(3)
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.winConfirm1.OcultaDialogo()
        Response.Redirect("AdmonAreas.aspx?SID=" & Session("SID"), False)
    End Sub

	Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click

	End Sub

	Sub Areas_Administracion(ByVal Accion As Integer)
		Try
			Dim ds As New DataSet

			'IdArea,NombreArea
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Accion", Accion)
			par(1) = New SqlParameter("@IdArea", Me.txtIdArea.Text.ToUpper.Trim)
			par(2) = New SqlParameter("@NombreArea", Me.txtNombreArea.Text.Trim)

			ds = DatosSQL.funcioncp("Areas_Administracion", par)

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0), CMensajes.Tipo.GetError, wucMessageAdmonAreas)
			Else
				CosultarArea()
				btnGuardar.Visible = True
				btnModificar.Visible = False
				btnEliminar.Visible = False
				txtIdArea.Enabled = True
				LimpiarCampos()

				Select Case Accion
					Case 1 '--NUEVO
						'CMensajes.Show("Area Agregada exitosamente.")
						CMensajes.MostrarMensaje("Area Agregada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonAreas)
					Case 2 '--MODIFICAR
						'CMensajes.Show("Area Modificada exitosamente.")
						CMensajes.MostrarMensaje("Area Modificada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonAreas)
					Case 3 '--ELIMINAR
						'CMensajes.Show("Area Eliminada exitosamente.")
						CMensajes.MostrarMensaje("Area Eliminada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonAreas)
				End Select
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Sub CosultarArea()
        Try
            'IdArea,NombreArea
            Dim ds As New DataSet
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@IdArea", -1)

            ds = DatosSQL.funcioncp("Areas_Buscar", par)
            gvArea.DataSource = ds
            gvArea.DataBind()
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Protected Sub gvArea_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvArea.RowCommand
        Try
            If e.CommandName = "Modificar" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim id, descripcion As String
                id = gvArea.Rows(index).Cells(0).Text
                descripcion = gvArea.Rows(index).Cells(1).Text

                txtIdArea.Text = id
                txtNombreArea.Text = descripcion
                txtIdArea.Enabled = False
                btnModificar.Visible = True
                btnGuardar.Visible = False
            ElseIf (e.CommandName = "Eliminar") Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim id, descripcion As String
                id = gvArea.Rows(index).Cells(0).Text
                descripcion = gvArea.Rows(index).Cells(1).Text

                txtIdArea.Text = id
                txtNombreArea.Text = descripcion
                txtIdArea.Enabled = False
                btnModificar.Visible = False
                btnGuardar.Visible = False
                btnEliminar.Visible = False
                Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
                lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar el área?</div>"
                winConfirm1.MuestraDialogo()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Sub LimpiarCampos()
        txtIdArea.Text = ""
        txtNombreArea.Text = ""
    End Sub

End Class