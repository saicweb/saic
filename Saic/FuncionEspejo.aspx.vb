﻿Imports System.Data.SqlClient
Imports System.IO

Public Class FuncionEspejo
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If
				If Session("Perfil") = "P" Then
					CMensajes.Show("Sólo el perfil 'Rotativo' tiene permiso de acceso a esta sección.")
					Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
				End If

				Dim scriptManager2 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
				scriptManager2.RegisterPostBackControl(Me.btnCambiar)
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Private Sub btnCambiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambiar.Click
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_FuncionEspejo", par)

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				REPORTE_InvSAP(ds)
			Else
				Me.btnCambiar.Visible = False
				CMensajes.MostrarMensaje("No existen datos para generar el archivo.", CMensajes.Tipo.GetError, wucMessageFuncionEspejo)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPORTE_InvSAP(ByVal DATOS As DataSet)
		Try
			'Crear el directorio si no existe
			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos") & ConfigurationManager.AppSettings("INV")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim archivo As String = DirectorioArchivos & "INV" & Session("IdTienda") & Now.Year.ToString & Format(Now.Month, "00") & Format(Now.Day, "00") & Format(Now.Hour, "00") & Format(Now.Minute, "00") & Format(Now.Second, "00") & ".txt"
			Dim sw As StreamWriter = New StreamWriter(archivo)
			Dim cadena As String
			'Guardar la información en el archivo txt
			For x As Integer = 0 To DATOS.Tables(0).Rows.Count - 1
				'Comienza proceso de creación del txt
				'cadena = DocumentoInventario & "|" & Consecutivo & "|" & Ejercicio & "|" & CampoFijo1 & "|" & CampoFijo2 & "|" & InventarioTeorico & "|" & CodigoMaterial
				cadena = DATOS.Tables(0).Rows(x).Item("LINEA")
				sw.WriteLine(cadena)
			Next
			sw.Flush()
			sw.Close()

			Dim encripta As New Encryption
			Response.Redirect("FileDownload.aspx?file=" & encripta.encryptString(archivo.Trim), False)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class