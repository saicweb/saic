﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="InicializarPreconteo.aspx.vb" Inherits="Saic.InicializarPreconteo" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%" >
        <tr>
            <td class="titulos">
                Inicializar preconteo
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageInicializarPreconteo" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="45%" align="center">
                    <tr align="center">
                        <td>
                            <span><asp:Button runat="server" Text="Inicializar" ID="btnInicializar" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>