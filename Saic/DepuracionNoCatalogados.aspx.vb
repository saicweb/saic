﻿Imports System.Data
Imports System.Data.SqlClient
Public Class DepuracionNoCatalogados
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Me.lbltiempo.Text = "Hora de Inicio: " & Now.ToShortTimeString

			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			'ROTATIVO
			If Session("Perfil").ToString.ToUpper = "R" Then
				par(0) = New SqlParameter("@Tipo", "R")
				'PRECONTEO
			ElseIf Session("Perfil").ToString.ToUpper = "P" Then
				par(0) = New SqlParameter("@Tipo", "P")
			End If
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados_ConsultaDepurar", par)
			If Not ds Is Nothing Then
				If ds.Tables(0).Rows.Count > 0 Then
					Depurar(ds)
					CMensajes.MostrarMensaje("Depuración realizada con éxito.", CMensajes.Tipo.GetExito, wucMessageDepuracionNoCatalogados)
				Else
					CMensajes.MostrarMensaje("No existen artículos no catalogados.", CMensajes.Tipo.GetError, wucMessageDepuracionNoCatalogados)
				End If
			Else
				CMensajes.MostrarMensaje("No existen artículos no catalogados.", CMensajes.Tipo.GetError, wucMessageDepuracionNoCatalogados)
			End If
			Me.lbltiempo.Text &= "  Hora de Termino: " & Now.ToShortTimeString
		Catch ex As Exception
			Me.btnGenerarReporte.Visible = False
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Depurar(ByVal ds As DataSet)
        Try
            For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim par(2) As SqlParameter
                'ROTATIVO
                If Session("Perfil").ToString.ToUpper = "R" Then
                    par(0) = New SqlParameter("@Tipo", "R")
                    'PRECONTEO
                ElseIf Session("Perfil").ToString.ToUpper = "P" Then
                    par(0) = New SqlParameter("@Tipo", "P")
                End If
                par(1) = New SqlParameter("@CBARRAS", ds.Tables(0).Rows(x).Item("CBARRAS"))
                par(2) = New SqlParameter("@IdTienda", ds.Tables(0).Rows(x).Item("IDTIENDA"))
                DatosSQL.procedimientocp("RPT_ArticulosNoCatalogados_Depuracion", par)
            Next
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
End Class
