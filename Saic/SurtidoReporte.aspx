﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SurtidoReporte.aspx.vb" Inherits="Saic.SurtidoReporte" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="" ID="lblTitulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageSurtidoReporte" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="45%" align="center">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacen: "></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" id="cboAlmacen" Width="150px" CssClass="dropdown" ToolTip="Seleccionar Tienda si es un Usuario Administrador"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:RadioButtonList runat="server" ID="rbtnRPTFormatos" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
                                <asp:ListItem Value="PDF" Selected="True">PDF</asp:ListItem>
								<asp:ListItem Value="EXCEL">EXCEL</asp:ListItem>
                            </asp:RadioButtonList><br />
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <span><asp:Button runat="server" Text="Descargar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

