﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonSociedades.aspx.vb" Inherits="Saic.AdmonSociedades" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table runat="server" width="100%">
                <tr>
                    <td class="titulos">
                        <asp:Label runat="server" Text="Sociedades" ID="lblTitulo"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <wum:wucMessage ID="wucMessageAdmonSociedades" runat="server" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <table runat="server" width="45%" align="center">
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Id sociedad: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtIdSociedad" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtIdSociedad" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Descripción: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtdescripcion" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdescripcion" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Descripción corta: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescripcionCorta" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDescripcionCorta" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Dirección: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDireccion" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDireccion" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Municipio: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMunicipio" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtMunicipio" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Código postal: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCodigoPostal" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCodigoPostal" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtCodigoPostal" ErrorMessage="Ingresar solo números." Display="Dynamic" Type="Integer" MinimumValue="0" MaximumValue="999999999">
                                    </asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="RFC: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRFC" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtRFC" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr align="center">
                                <td colspan="2">
                                    <span><asp:Button runat="server" Text="Agregar" ID="btnGuardar" CssClass="btn" Visible="true" /></span>
                                    <span><asp:Button runat="server" Text="Actualizar" ID="btnModificar" CausesValidation="false"  CssClass="btn" Visible="false" /></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <table runat="server">
                            <tr>
                                <td align="center"><br />
                                    <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                        <asp:GridView ID="gvsociedad" runat="server" AutoGenerateColumns="false" SkinID="Grid3">
                                        <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                        <Columns>
                                            <asp:BoundField DataField="IdSociedad" HeaderText="IdSociedad" HeaderStyle-Width="90px" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                            <asp:BoundField DataField="DescripcionSociedad" HeaderText="Sociedad" HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                            <asp:BoundField DataField="DescripcionCorta" HeaderText="Descripción corta" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
                                            <asp:BoundField DataField="Direccion" HeaderText="Dirección" HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                            <asp:BoundField DataField="Municipio" HeaderText="Municipio" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                            <asp:BoundField DataField="CodigoPostal" HeaderText="Código postal" HeaderStyle-Width="90px" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                            <asp:BoundField DataField="RFC" HeaderText="RFC" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                            <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkmodificar" CausesValidation="false" CommandName="Modificar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Modificar</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEliminar" CausesValidation="false" CommandName="Eliminar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Eliminar</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnGuardar" />
            <asp:PostBackTrigger ControlID="btnModificar" />
            <asp:PostBackTrigger ControlID="gvsociedad" />
            <asp:PostBackTrigger ControlID="wucMessageAdmonSociedades" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
