﻿Imports dbAutoTrack.DataReports
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class RPT_Operaciones80_20
    Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As New Button
    Dim ds As New DataSet


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If
				If Session("Perfil") = "P" Then
					Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
				End If

				Dim Tipo As String = Request.QueryString("Tipo")

				Threading.Thread.Sleep(5000)

				TblGeneral.Visible = True
				TblDepartamento.Visible = False
				TblAlmacen.Visible = False
				TblRango.Visible = False
				TblGeneralDescarga.Visible = False
				Departamentos_Buscar(Tipo)
				Almacen_Buscar(Tipo)
				ConteoConsolidado()

			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
        End If
    End Sub


#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Aceptar"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click
		winConfirm1.FindControl("btnNo").Visible = False

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.winConfirm1.OcultaDialogo()
	End Sub
#End Region

	''' <summary>
	''' Valida que el consolidado ya se haya realizado
	''' </summary>
	Sub ConteoConsolidado()
		Try
			Dim par(1) As SqlParameter
			Dim ds As New DataSet
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Tipo", 2)
			ds = DatosSQL.funcioncp("ConteoConsolidadoBuscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.lblConteoConsolidado.Text = ds.Tables(0).Rows(0).Item(0)
			Else
				Me.lblConteoConsolidado.Text = "0"
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

	''' <summary>
	''' Inserta el consolidado
	''' </summary>
	''' <param name="departamentos"></param>
	Public Sub GuardarConsolidado(ByVal departamentos)
		Try
			If Me.lblConteoConsolidado.Text.Trim.Length > 0 Then
				Me.lblTiempoProceso.Text &= "Inicio proceso Guardar Consolidado: " & Now.ToLongTimeString

				Dim parConsolidado(0) As SqlParameter
				If (Me.cboReporte.SelectedValue = 1 Or Me.cboReporte.SelectedValue = 3) Then
					ReDim parConsolidado(2)
					parConsolidado(0) = New SqlParameter("@Accion", 1)
					parConsolidado(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
					parConsolidado(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
					DatosSQL.procedimientocp("ConteoConsolidado_Administracion", parConsolidado)
				ElseIf (Me.cboReporte.SelectedValue = 2 Or Me.cboReporte.SelectedValue = 4) Then
					ReDim parConsolidado(3)
					parConsolidado(0) = New SqlParameter("@Accion", 1)
					parConsolidado(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
					parConsolidado(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
					parConsolidado(3) = New SqlParameter("@CODE_DEPTO", departamentos)
					DatosSQL.procedimientocp("ConteoConsolidado_AdministracionDeptsR", parConsolidado)
				End If

				Me.lblTiempoProceso.Text &= " --> Fin proceso Guardar Consolidado: " & Now.ToLongTimeString & "  /  "
				Me.ConteoConsolidado()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.lblTiempoProceso.Text = ""

		If Session("Cedis") = "1" Then
			CMensajes.MostrarMensaje("No se encontraron datos.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
		Else
			Dim dt As New DataTable
			Try
				If Me.cboReporte.SelectedValue = "-1" Then
					CMensajes.MostrarMensaje("Seleccione el reporte a generar.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
				Else
					Dim separator As String = ","
					Dim arregloDepartamentos As New ArrayList()
					Dim departamentos As String = ""
					Dim departamentos2 As String = ""
					Dim depListos As String = ""

					Select Case cboReporte.SelectedValue
						Case 2, 4
							If Me.cboReporte.SelectedValue = 2 Then
								If lstSeleccionados1.Items.Count > 0 Then
									For Each objItem As ListItem In lstSeleccionados1.Items
										arregloDepartamentos.Add(objItem.Value.ToString)
									Next
									GoTo continuar
								Else
									CMensajes.MostrarMensaje("Seleccione por lo menos un elemento de la lista.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
									GoTo terminar
								End If

							ElseIf Me.cboReporte.SelectedValue = 4 Then
								If String.IsNullOrEmpty(Me.txtRango.Text) OrElse String.IsNullOrWhiteSpace(Me.txtRango.Text) Then
									CMensajes.MostrarMensaje("Favor de colocar un valor en rango de precio.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
									GoTo terminar
								ElseIf Not lstSeleccionados2.Items.Count > 0 Then
									CMensajes.MostrarMensaje("Seleccione por lo menos un elemento de la lista.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
									GoTo terminar
								Else
									For Each objItem As ListItem In lstSeleccionados2.Items
										arregloDepartamentos.Add(objItem.Value.ToString)
									Next
									GoTo continuar
								End If
							End If
continuar:
							departamentos = String.Join(",", arregloDepartamentos.ToArray())

							Dim v(2) As SqlParameter
							Dim ds As New DataSet
							v(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
							v(1) = New SqlParameter("@Tipo", 2)
							v(2) = New SqlParameter("@CODE_DEPTO", departamentos)
							ds = DatosSQL.funcioncp("ConteoConsolidadoBuscarXDepto", v)

							If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
								Dim arreglo3 As New ArrayList()
								Dim arregloA As New ArrayList()

								If ds.Tables(1).Rows.Count > 0 Then
									For i = 0 To ds.Tables(1).Rows.Count - 1
										arregloA.Add(ds.Tables(1).Rows(i).Item("codigosDepartamentos").ToString())
									Next

									For j = 0 To arregloDepartamentos.Count - 1
										For x = 0 To arregloA.Count - 1
											If (arregloDepartamentos.Contains(arregloA(x))) Then
												arregloDepartamentos.Remove(arregloA(x))
												arreglo3.Add(arregloA(x))
											End If
										Next
									Next
								End If

								departamentos2 = String.Join(",", arregloDepartamentos.ToArray())
								depListos = String.Join(",", arreglo3.ToArray())

								If (departamentos2 = Nothing) Then
									departamentos2 = "0"
								End If

								If depListos <> "" Then
									ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Información", "alert('Los departamentos: " & depListos & " ya tienen información consolidada.');", True)
								End If

								GuardarConsolidado(departamentos2)
							End If
						Case 1, 3
							If Me.cboReporte.SelectedValue = 3 Then
								If Me.cboAlmacen.SelectedValue = "-1" Then
									CMensajes.MostrarMensaje("Seleccione un Almacen.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
									GoTo terminar
								End If
							End If

							If Me.lblConteoConsolidado.Text = "0" Then
								GuardarConsolidado(departamentos)
							End If
					End Select

					'=====REPORTE====
					Me.lblTiempoProceso.Text &= "Inicio proceso Consulta Datos Reporte: " & Now.ToLongTimeString
					Dim par(1) As SqlParameter

					If Me.cboReporte.SelectedValue = 1 Then '--GENERAL
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
						If Me.rbtnRPTFormatos.Items(0).Selected = True Then
							ds = DatosSQL.funcioncp("RPT_Diferencias80_20GeneralN", par)
							Dim totalabs As Decimal = ds.Tables(2).Rows(0)(0)
							If totalabs > 0 Then
								REPORTE(ds)
							Else
								CMensajes.MostrarMensaje("No existe información para generar el reporte, las cantidades cuadran.", CMensajes.Tipo.GetInformativo, wucMessageRPTOperaciones80_20)
							End If
						Else
							ds = DatosSQL.funcioncp("RPT_Diferencias80_20GeneralExcelN", par)
							Dim totalabs1 As Decimal = ds.Tables(2).Rows(0)(0)
							If totalabs1 > 0 Then
								Dim almacen As String
								almacen = Me.cboAlmacen.SelectedValue
								REPEXCEL(ds, almacen)
							Else
								CMensajes.MostrarMensaje("No existe información para generar el reporte, las cantidades cuadran.", CMensajes.Tipo.GetInformativo, wucMessageRPTOperaciones80_20)
							End If
						End If
						Me.lblTiempoProceso.Text &= "Termina de ejecutar SP " & Now.ToLongTimeString

					ElseIf Me.cboReporte.SelectedValue = 2 Then '--DEPARTAMENTO
						Try
							ReDim par(2)
							par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
							par(1) = New SqlParameter("@CODE_DEPTO", departamentos)
							par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))

							If Me.rbtnRPTFormatos.Items(0).Selected = True Then
								If Me.lstSeleccionados1.SelectedValue = "0" Then
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoInvGral", par)
								Else
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoNuevo", par)
								End If
							Else
								If Me.lstSeleccionados1.SelectedValue = "0" Then
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoInvGralExcel", par)
								Else
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoExcelNuevo", par)
								End If
							End If
							ValidaReportes(ds, departamentos2)
							'CMensajes.MostrarMensaje("Se realizó el consolidado de los siguientes departamentos: " & departamentos2, CMensajes.Tipo.GetExito, wucMessageRPTOperaciones80_20)
						Catch ex As Exception
							Throw New Exception(ex.Message, ex.InnerException)
						End Try

					ElseIf Me.cboReporte.SelectedValue = 3 Then '--ALMACEN
						ReDim par(2)
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@IdALMACEN", Me.cboAlmacen.SelectedValue)
						par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))

						If Me.rbtnRPTFormatos.Items(0).Selected = True Then
							If Me.cboAlmacen.SelectedValue = "0" Then
								ds = DatosSQL.funcioncp("RPT_Diferencias80_20AlmacenInvGral", par)
							Else
								ds = DatosSQL.funcioncp("RPT_Diferencias80_20AlmacenInvGralNuevo", par)
							End If
						Else
							If Me.cboAlmacen.SelectedValue = "0" Then
								ds = DatosSQL.funcioncp("RPT_Diferencias80_20AlmacenInvGralExcel", par)
							Else
								ds = DatosSQL.funcioncp("RPT_Diferencias80_20AlmacenInvGralExcelNuevo", par)
							End If
						End If
						If Me.rbtnRPTFormatos.Items(0).Selected = True Then
							REPORTE(ds)
						Else
							Dim almacen As String
							almacen = Me.cboAlmacen.SelectedValue
							REPEXCEL(ds, almacen)
						End If

					ElseIf Me.cboReporte.SelectedValue = 4 Then '--DEPARTAMENTO POR RANGO
						Try
							ReDim par(3)
							par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
							par(1) = New SqlParameter("@CODE_DEPTO", departamentos)
							par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
							par(3) = New SqlParameter("@Rango", txtRango.Text)

							If Me.rbtnRPTFormatos.Items(0).Selected = True Then
								If Me.lstSeleccionados2.SelectedValue = "0" Then
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoInvGralRango", par)
								Else
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoRangoNuevo", par)
								End If
							Else
								If Me.lstSeleccionados2.SelectedValue = "0" Then
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoInvGralExcelRango", par)
								Else
									ds = DatosSQL.funcioncp("RPT_Diferencias80_20DeptoExcelRangoNuevo", par)
								End If
							End If
							ValidaReportes(ds, departamentos2)
						Catch ex As Exception
							Throw New Exception(ex.Message, ex.InnerException)
						End Try
					End If
				End If
terminar:
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If

	End Sub

	''' <summary>
	''' Sólo para reporte por Rango y por Departamento
	''' </summary>
	''' <param name="ds"></param>
	Public Sub ValidaReportes(ByVal ds As DataSet, ByVal departamentos2 As String)
		Me.lblTiempoProceso.Text &= " --> Fin proceso Consulta Datos Reporte: " & Now.ToLongTimeString & "  /  "

		If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
			Me.HyperLink1.Visible = True
			Me.lblTiempoProceso.Text &= "Inicio proceso Generación de Reporte: " & Now.ToLongTimeString

			If Me.rbtnRPTFormatos.Items(0).Selected = True Then
				REPORTE(ds)
			Else
				Dim almacen As String
				almacen = Me.cboAlmacen.SelectedValue
				REPEXCEL(ds)
			End If
			CMensajes.MostrarMensaje("Se realizó el consolidado de los siguientes departamentos: " & departamentos2, CMensajes.Tipo.GetExito, wucMessageRPTOperaciones80_20)

			Me.lblTiempoProceso.Text &= " --> Fin proceso Generación de Reporte: " & Now.ToLongTimeString
		Else
			CMensajes.MostrarMensaje("No existen datos para generar el reporte.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
		End If
	End Sub

	Sub REPEXCEL(ByVal DATOS As DataSet, Optional ByVal Almacen As String = "")
		Try
			'Crear el directorio si no existe
			Dim TitulosArchivo As String
			Dim Archivo As String
			Dim Subtitulo As String
			Dim path As String = ConfigurationManager.AppSettings("SAIC").ToString() & ConfigurationManager.AppSettings("Reportes80_20")

			If (Not Directory.Exists(path)) Then
				Directory.CreateDirectory(path)
			End If

			If Me.cboReporte.SelectedValue = 1 Then
				TitulosArchivo = "Diferencias 80_20 General"
				Subtitulo = "Fecha: " & ds.Tables(1).Rows(0)(0) & "		Hora: " & ds.Tables(1).Rows(0)(1) & "		Tienda: " & ds.Tables(1).Rows(0)(2)
				Archivo = path & TitulosArchivo.Replace(" ", "") & "_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"

			ElseIf Me.cboReporte.SelectedValue = 2 Then
				TitulosArchivo = "Diferencias 80_20 Por Departamento"
				Subtitulo = "Fecha: " & ds.Tables(2).Rows(0)(0) & "	Hora: " & ds.Tables(2).Rows(0)(1) & "	Tienda: " & ds.Tables(2).Rows(0)(2)
				Archivo = path & TitulosArchivo.Replace(" ", "") & "_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"

			ElseIf Me.cboReporte.SelectedValue = 3 Then
				TitulosArchivo = "Diferencias 80_20 Por Almacen"
				Subtitulo = "Fecha: " & ds.Tables(1).Rows(0)(0) & "	Hora: " & ds.Tables(1).Rows(0)(1) & "	Tienda: " & ds.Tables(1).Rows(0)(2) & "		Almacén: " & Almacen
				Archivo = path & TitulosArchivo.Replace(" ", "") & "_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"

			Else
				TitulosArchivo = "Diferencias 80_20 Por Departamento por Rango"
				Subtitulo = "Fecha: " & ds.Tables(2).Rows(0)(0) & "	Hora: " & ds.Tables(2).Rows(0)(1) & "	Tienda: " & ds.Tables(2).Rows(0)(2)
				Archivo = path & TitulosArchivo.Replace(" ", "") & "_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"
			End If

			Reportes.Exportar_ExcelDinamico(DATOS.Tables(0), Archivo, TitulosArchivo, Subtitulo)

			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo)
			Me.HyperLink1.Visible = True
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPORTE(ByVal DATOS As DataSet)
        Try
            '//////////////////////////////////////////////////////////////////////
            'generacion de  reporte en pdf
            Dim report1 As dbATReport
            Dim dt As New DataTable 'Datatable definitivo
            Dim settings As New dbAutoTrack.DataReports.PageSettings
            settings.PaperKind = Drawing.Printing.PaperKind.Letter
            settings.Orientation = PageOrientation.Landscape
            settings.Margins.MarginLeft = 0
            settings.Margins.MarginRight = 0
            settings.Margins.MarginTop = 0
            settings.Margins.MarginBottom = 0

            Dim NombreArchivo As String
            If Me.cboReporte.SelectedValue = 1 Then
                NombreArchivo = "Diferencias80_20General"
                report1 = New RPT_Diferencias80_20General 'General 
                CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
                CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
                CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")
            ElseIf Me.cboReporte.SelectedValue = 2 Then
                'REPORTE PARA INV GRAL
                If Me.lstSeleccionados1.Items.Count = "76" Then 'Todos los departamentos
                    NombreArchivo = "RPT_Diferencias80_20DeptoInvGral"
                    report1 = New RPT_Diferencias80_20DeptoInvGral
                    CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Fecha")
                    CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Hora")
                    CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Tienda")

                Else ' Por un Depto en particular
                    NombreArchivo = "RPT_Diferencias80_20PorDepto"
                    report1 = New RPT_Diferencias80_20Depto
                    CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Fecha")
                    CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Hora")
                    CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Tienda")
                End If

            ElseIf Me.cboReporte.SelectedValue = 3 Then
                'REPORTE PARA INV GRAL DE ALMACEN
                If Me.cboAlmacen.SelectedValue = "0" Then 'Todos los almacenes
                    NombreArchivo = "RPT_Diferencias80_20AlmacenInvGral"
                    report1 = New RPT_Diferencias80_20AlmacenInvGral
                    CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
                    CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
                    CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")
                Else
                    NombreArchivo = "RPT_Diferencias80_20PorAlmacen"
                    report1 = New RPT_Diferencias80_20Almacen
                    CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
                    CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
                    CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")
                End If

            Else '--DEPARTAMENTO POR RANGO 
                'REPORTE PARA INV GRAL
                If Me.lstSeleccionados2.Items.Count = "76" Then 'Todos los departamentos
                    NombreArchivo = "RPT_Diferencias80_20DeptoInvGralRango"
                    report1 = New RPT_Diferencias80_20DeptoInvGral
                    CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Fecha")
                    CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Hora")
                    CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Tienda")

                Else ' Por un Depto en particular
                    NombreArchivo = "RPT_Diferencias80_20PorDeptoRango"
                    report1 = New RPT_Diferencias80_20Depto
                    CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Fecha")
                    CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Hora")
                    CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(2).Rows(0).Item("Tienda")
                End If
            End If
            report1.PageSetting = settings
            Dim document1 As New PDFExport.PDFDocument

			Dim path As String = ConfigurationManager.AppSettings("SAIC").ToString() & ConfigurationManager.AppSettings("Reportes80_20")
			If (Not Directory.Exists(path)) Then
                Directory.CreateDirectory(path)
            End If

            Dim pathPdf As String = path & NombreArchivo & "_" & Session("IdUsuario") & ".pdf"

            report1.DataSource = DATOS.Tables(0)

            document1.EmbedFont = True
            document1.Compress = True
            report1.Generate()
            document1.Export(report1.Document, pathPdf)

            Dim encripta As New Encryption
            Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf)
			Me.HyperLink1.Visible = True
			'//////////////////////////////////////////////////
			'termina el reporte
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

	Sub Departamentos_Buscar(ByVal Tipo As Integer)
        Try
            Dim ds As New DataSet
            Dim par(1) As SqlParameter
            'par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
            'par(1) = New SqlParameter("@Tipo", Tipo)
            ds = DatosSQL.FuncionSP("CatalogoGeneral_Deptos_Buscar2")
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                Me.lstTiendas1.DataSource = ds.Tables(0)
                Me.lstTiendas1.DataValueField = "CODE_DEPTO"
                Me.lstTiendas1.DataTextField = "NOM_DEPTO"
                Me.lstTiendas1.DataBind()

                Me.lstTiendas2.DataSource = ds.Tables(0)
                Me.lstTiendas2.DataValueField = "CODE_DEPTO"
                Me.lstTiendas2.DataTextField = "NOM_DEPTO"
                Me.lstTiendas2.DataBind()

            Else
				CMensajes.MostrarMensaje("No existen datos para generar el reporte.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
			End If
        Catch ex As Exception
			Me.btnGenerarReporte.Visible = False
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Sub Almacen_Buscar(ByVal Tipo As Integer)
        Try
            Dim ds As New DataSet
            Dim par(1) As SqlParameter
            Dim perfil As String

            If Tipo = 1 Then
                perfil = "R"
            ElseIf Tipo = 0 Then
                perfil = "P"
            End If
            par(0) = New SqlParameter("@Tipo", perfil)
            par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
            ds = DatosSQL.funcioncp("CatalogoGeneral_Almacen_Buscar", par)
            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then

                Me.cboAlmacen.DataSource = ds.Tables(0)
                Me.cboAlmacen.DataValueField = "idalmacen"
                Me.cboAlmacen.DataTextField = "almacen"
                Me.cboAlmacen.DataBind()
            Else
				CMensajes.MostrarMensaje("No existen datos para generar el reporte.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
			End If
        Catch ex As Exception
			Me.btnGenerarReporte.Visible = False
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Protected Sub btnAgregar2_Click(sender As Object, e As EventArgs) Handles btnAgregar2.Click
		If lstTiendas1.SelectedIndex > -1 Then
			Dim val As Integer = -1
			Dim CONT As Integer = 0
			Dim lista() As Integer = lstTiendas1.GetSelectedIndices

			Dim modulospadres As New List(Of String)

			For Each valor As Integer In lista
				lstTiendas1.SelectedIndex = valor
				lstSeleccionados1.Items.Add(lstTiendas1.SelectedItem)
			Next

			Dim listamodulospadres As IEnumerable(Of String) = modulospadres.Distinct


			For Each valor As Integer In lista
				If val = -1 Then
					val = valor
				Else
					CONT = CONT + 1
					val = valor - CONT
				End If
				lstTiendas1.SelectedIndex = val
				lstTiendas1.Items.Remove(lstTiendas1.SelectedItem)
			Next

			For Each row As String In listamodulospadres
				For i As Integer = 0 To lstTiendas1.Items.Count - 1
					lstTiendas1.SelectedIndex = i
					If lstTiendas1.SelectedValue = row Then
						lstSeleccionados1.Items.Add(lstTiendas1.SelectedItem)
						lstTiendas1.Items.Remove(lstTiendas1.SelectedItem)
						Exit For
					End If

				Next
			Next
		Else
			CMensajes.MostrarMensaje("Debe de seleccionar un elemento de la lista de departamentos tienda.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
		End If
    End Sub

    Protected Sub btnQuitar1_Click(sender As Object, e As EventArgs) Handles btnQuitar1.Click

        If lstSeleccionados1.SelectedIndex > -1 Then
            Dim val As Integer = -1
            Dim cont As Integer = 0
            Dim lista() As Integer = lstSeleccionados1.GetSelectedIndices
            For Each valor As Integer In lista
                lstSeleccionados1.SelectedIndex = valor
                lstTiendas1.Items.Add(lstSeleccionados1.SelectedItem)
            Next

            For Each valor As Integer In lista
                If val = -1 Then
                    val = valor
                Else
                    cont = cont + 1
                    val = valor - cont
                End If

                lstSeleccionados1.SelectedIndex = val
                lstSeleccionados1.Items.Remove(lstSeleccionados1.SelectedItem)
            Next
        Else
			CMensajes.MostrarMensaje("Debe de seleccionar un elemento de la lista de departamentos seleccionados.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
		End If
    End Sub

    Protected Sub btnAgregar1_Click(sender As Object, e As EventArgs) Handles btnAgregar1.Click
        Try
            For Each item As Object In lstTiendas1.Items
                lstSeleccionados1.Items.Add(item)
            Next

            lstTiendas1.Items.Clear()

        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Protected Sub btnQuitar2_Click(sender As Object, e As EventArgs) Handles btnQuitar2.Click
		Try

			For Each item As Object In lstSeleccionados1.Items
				lstTiendas1.Items.Add(item)
			Next

			lstSeleccionados1.Items.Clear()

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

    Protected Sub btnAgregar3_Click(sender As Object, e As EventArgs) Handles btnAgregar3.Click
        Try
            For Each item As Object In lstTiendas2.Items
                lstSeleccionados2.Items.Add(item)
            Next

            lstTiendas2.Items.Clear()

        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Protected Sub btnAgregar4_Click(sender As Object, e As EventArgs) Handles btnAgregar4.Click

        If lstTiendas2.SelectedIndex > -1 Then
            Dim val As Integer = -1
            Dim CONT As Integer = 0
            Dim lista() As Integer = lstTiendas2.GetSelectedIndices

            Dim modulospadres As New List(Of String)

            For Each valor As Integer In lista
                lstTiendas2.SelectedIndex = valor
                lstSeleccionados2.Items.Add(lstTiendas2.SelectedItem)
            Next

            Dim listamodulospadres As IEnumerable(Of String) = modulospadres.Distinct


            For Each valor As Integer In lista
                If val = -1 Then
                    val = valor
                Else
                    CONT = CONT + 1
                    val = valor - CONT
                End If
                lstTiendas2.SelectedIndex = val
                lstTiendas2.Items.Remove(lstTiendas2.SelectedItem)
            Next

            For Each row As String In listamodulospadres
                For i As Integer = 0 To lstTiendas2.Items.Count - 1
                    lstTiendas2.SelectedIndex = i
                    If lstTiendas2.SelectedValue = row Then
                        lstSeleccionados2.Items.Add(lstTiendas2.SelectedItem)
                        lstTiendas2.Items.Remove(lstTiendas2.SelectedItem)
                        Exit For
                    End If

                Next
            Next
        Else
			CMensajes.MostrarMensaje("Debe de seleccionar un elemento de la lista de departamentos tienda.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
		End If
    End Sub

    Protected Sub btnQuitar3_Click(sender As Object, e As EventArgs) Handles btnQuitar3.Click
        If lstSeleccionados2.SelectedIndex > -1 Then
            Dim val As Integer = -1
            Dim cont As Integer = 0
            Dim lista() As Integer = lstSeleccionados2.GetSelectedIndices
            For Each valor As Integer In lista
                lstSeleccionados2.SelectedIndex = valor
                lstTiendas2.Items.Add(lstSeleccionados2.SelectedItem)
            Next

            For Each valor As Integer In lista
                If val = -1 Then
                    val = valor
                Else
                    cont = cont + 1
                    val = valor - cont
                End If

                lstSeleccionados2.SelectedIndex = val
                lstSeleccionados2.Items.Remove(lstSeleccionados2.SelectedItem)
            Next
        Else
			CMensajes.MostrarMensaje("Debe de seleccionar un elemento de la lista de departamentos seleccionados.", CMensajes.Tipo.GetError, wucMessageRPTOperaciones80_20)
		End If
    End Sub

    Protected Sub btnQuitar4_Click(sender As Object, e As EventArgs) Handles btnQuitar4.Click
        Try

            For Each item As Object In lstSeleccionados2.Items
                lstTiendas2.Items.Add(item)
            Next

            lstSeleccionados2.Items.Clear()

        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

	Private Sub cboReporte_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboReporte.SelectedIndexChanged
		'Me.lblConteoConsolidado.Text = ""
		Me.lblTiempoProceso.Text = ""
		Me.HyperLink1.Visible = False
		If Me.cboReporte.SelectedValue = "-1" Or Me.cboReporte.SelectedValue = "0" Then 'Seleccione
			TblGeneral.Visible = True
			TblDepartamento.Visible = False
			TblAlmacen.Visible = False
			TblRango.Visible = False
			TblGeneralDescarga.Visible = False
			'lblMensaje.Text = ""
			'lblTiempoProceso.Text = ""
			'HyperLink1.Text = ""
		ElseIf Me.cboReporte.SelectedValue = "1" Then 'Por Inv Gral
			TblGeneral.Visible = True
			TblDepartamento.Visible = False
			TblAlmacen.Visible = False
			TblRango.Visible = False
			TblGeneralDescarga.Visible = True
			'lblMensaje.Text = ""
			'lblTiempoProceso.Text = ""
			'HyperLink1.Text = ""
		ElseIf Me.cboReporte.SelectedValue = "2" Then 'Por Depto  
			TblGeneral.Visible = True
			TblDepartamento.Visible = True
			TblAlmacen.Visible = False
			TblRango.Visible = False
			TblGeneralDescarga.Visible = True
			'lblMensaje.Text = ""
			'lblTiempoProceso.Text = ""
			'HyperLink1.Text = ""
		ElseIf Me.cboReporte.SelectedValue = "3" Then 'Por Almacen
			TblGeneral.Visible = True
			TblDepartamento.Visible = False
			TblAlmacen.Visible = True
			TblRango.Visible = False
			TblGeneralDescarga.Visible = True
			'lblMensaje.Text = ""
			'lblTiempoProceso.Text = ""
			'HyperLink1.Text = ""
		ElseIf Me.cboReporte.SelectedValue = "4" Then 'Por Inv Gral y Rango
			TblGeneral.Visible = True
			TblDepartamento.Visible = False
			TblAlmacen.Visible = False
			TblRango.Visible = True
			TblGeneralDescarga.Visible = True
			'lblMensaje.Text = ""
			'lblTiempoProceso.Text = ""
			'HyperLink1.Text = ""
		End If
	End Sub
End Class