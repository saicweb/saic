﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_DiferenciasPorDepartamento.aspx.vb" Inherits="Saic.RPT_DiferenciasPorDepartamento" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                Reporte de diferencias por departamento
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageDiferenciasPorDepartamento" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="40%" align="center">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Rango de departamentos: " ID="lblRango" ></asp:Label>
                        </td>
                        <td align="center"><br />
                            <asp:TextBox ID="txtDeptoInicial" runat="server" MaxLength="3" Width="50px" CssClass="inline" ></asp:TextBox> 
                            <asp:Label runat="server" Text=" - " CssClass="inline" ID="lbl1"></asp:Label>
                            <asp:TextBox ID="txtDeptoFinal" runat="server" MaxLength="3" Width="50px" CssClass="inline" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacen: " ID="lblAlmacen"></asp:Label>
                        </td>
                        <td align="center"><br />
                            <asp:DropDownList runat="server" id="cboAlmacen" Width="150px" CssClass="dropdown" ToolTip="Seleccionar Tienda si es un Usuario Administrador"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:RadioButtonList runat="server" ID="rbtnRPTFormatos" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
                                <asp:ListItem Value="PDF" Selected="True">PDF</asp:ListItem>
								<asp:ListItem Value="EXCEL">EXCEL</asp:ListItem>
                            </asp:RadioButtonList><br />
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2">
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
