﻿Imports System.Data.SqlClient

Public Class ReversaCierreInv
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		'	Mantener la contraseña
		txtPassword.Attributes.Add("value", txtPassword.Text)

		If Not Page.IsPostBack Then
			Validacion()
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageReversaCierreInv.Visible = False
		'Validacion()
		If Me.HddnAcceso.Value <> 1 Then
			Me.txtPassword.Text = ""
			Exit Sub
		End If
		Dim Pass, CPass As String
		If Me.txtPassword.Text.Trim.Length = 0 Then
			CMensajes.MostrarMensaje("Ingrese la contraseña de permiso de reversa.", CMensajes.Tipo.GetError, wucMessageReversaCierreInv)
		Else
			Pass = Security.getMD5Hash(Me.txtPassword.Text.Trim)
			Try
				Dim ds As New DataSet
				Dim PAR(1) As SqlParameter
				PAR(0) = New SqlParameter("@idAccion", 3)
				PAR(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
				ds = DatosSQL.funcioncpSeguridad("Usuarios_Buscar", PAR)
				If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					If IsDBNull(ds.Tables(0).Rows(0).Item("Password2")) = False Then
						CPass = ds.Tables(0).Rows(0).Item("Password2")
						If CPass <> Pass Then
							Me.txtPassword.Text = ""
							CMensajes.MostrarMensaje("La contraseña de permiso de reversa es incorrecta.", CMensajes.Tipo.GetError, wucMessageReversaCierreInv)
						Else
							Reversa()
							Me.txtPassword.Text = ""
						End If
					End If
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageReversaCierreInv.Visible = False
		Me.txtTienda.Text = ""
		Me.txtPassword.Text = ""
		Me.btnEjecutar.Enabled = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Sub Validacion()
		Try
			If Session("Perfil") = "P" Then
				Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
			End If

			If Session("PermisoReversa") = 0 Then
				Me.txtPassword.Text = ""
				CMensajes.MostrarMensaje("No cuenta con permisos para utilizar esta opción.", CMensajes.Tipo.GetError, wucMessageReversaCierreInv)
			Else
				Me.btnEjecutar.Enabled = True
				Me.txtTienda.Text = Session("IdTienda")
				Me.HddnAcceso.Value = 1
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">Ésta acción dará reversa al cierre de inventario de la tienda indicada, ¿Está seguro de continuar?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Sub Reversa()
		Try
			'Reversa a tabla de conteoConsolidado_sociedad
			Dim para(2) As SqlParameter
			para(0) = New SqlParameter("@Tienda", Session("IdTienda"))
			para(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			para(2) = New SqlParameter("@Tipo", "REVERSA_TABLAS")
			DatosSQL.procedimientocp("CI_ActualizaEstatus", para)

			'Actualizar el estatus
			ReDim para(2)
			para(0) = New SqlParameter("@Tienda", Session("IdTienda"))
			para(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			para(2) = New SqlParameter("@Tipo", "REVERSA")
			DatosSQL.procedimientocp("CI_ActualizaEstatus", para)

			CMensajes.MostrarMensaje("La reversa al cierre de inventario se ha completado con éxito.", CMensajes.Tipo.GetExito, wucMessageReversaCierreInv)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
		Me.txtTienda.Text = ""
		Me.txtPassword.Text = ""
		Me.btnEjecutar.Enabled = False
	End Sub
End Class