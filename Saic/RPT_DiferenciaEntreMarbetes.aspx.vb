﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_DiferenciaEntreMarbetes
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageRPT_DiferenciaEntreMarbetes.Visible = False

		If Not Page.IsPostBack Then
			Session.Remove("RPT_DiferenciaEntreMarbetes")
			If Session("Cedis") = 1 Then
				Me.TblFiltrosCedis.Visible = True
			End If
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim err As Integer = 0

			If Not Session("Cedis") = 1 Then
				For i As Integer = 1 To 2
					Dim ds2 As New DataSet
					Dim par2(2) As SqlParameter
					par2(0) = New SqlParameter("@Tipo", i)
					par2(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
					par2(2) = New SqlParameter("@Perfil", Session("Perfil"))
					ds2 = DatosSQL.funcioncp("RPT_MarbetesConciliacion", par2)
					If ds2.Tables(0).Rows.Count > 0 Then
						err += 1
						CMensajes.MostrarMensaje("Existen marbetes por conciliar.", CMensajes.Tipo.GetError, wucMessageRPT_DiferenciaEntreMarbetes)
						Exit For
					End If
				Next
			End If

			If err = 0 Then
				Dim pasillo As String = "0"
				Dim nivel As String = "0"
				Dim almacen As String = "0"
				Dim reporte As Integer

				If Session("Cedis") = 1 Then
					If Me.cmbAlmacen.SelectedValue.ToString = "-1" Then
						CMensajes.MostrarMensaje("Seleccione una opción para mostrar.", CMensajes.Tipo.GetError, wucMessageRPT_DiferenciaEntreMarbetes)
						Exit Sub
					ElseIf Me.cmbAlmacen.SelectedValue.ToString = "1" Then
						reporte = 1
						If Me.cmbPasillo.SelectedValue.ToString = "[Seleccione]" Then
							CMensajes.MostrarMensaje("Seleccione una opción para pasillo.", CMensajes.Tipo.GetError, wucMessageRPT_DiferenciaEntreMarbetes)
							Exit Sub
						ElseIf Me.cmbPasillo.SelectedValue.ToString = "[Todos]" Then
							pasillo = "0"
						Else
							pasillo = Me.cmbPasillo.SelectedValue.ToString
						End If

						Dim niveles As New ArrayList()
						For x As Integer = 0 To Me.DgNivel.Items.Count - 1
							Dim chkRow As New CheckBox
							chkRow = CType(Me.DgNivel.Items(x).FindControl("CheckBox2"), CheckBox)

							If chkRow.Checked Then
								Dim lblNivel As String = Me.DgNivel.Items(x).Cells(1).Text.ToString.Trim
								niveles.Add(lblNivel)
							End If
						Next

						If niveles.Count > 0 Then
							nivel = String.Join(",", niveles.ToArray)
						Else
							CMensajes.MostrarMensaje("Seleccione por lo menos una opción para nivel.", CMensajes.Tipo.GetError, wucMessageRPT_DiferenciaEntreMarbetes)
							Exit Sub
						End If
					ElseIf Me.cmbAlmacen.SelectedValue.ToString = "2" Then
						reporte = 2
						Dim almacenes As New ArrayList()
						For x As Integer = 0 To Me.DGAlmacen.Items.Count - 1
							Dim chkRow As New CheckBox
							chkRow = CType(Me.DGAlmacen.Items(x).FindControl("CheckBox2"), CheckBox)

							If chkRow.Checked Then
								Dim lblalmacen As String = Me.DGAlmacen.Items(x).Cells(1).Text.ToString.Trim
								almacenes.Add(lblalmacen)
							End If
						Next

						If almacenes.Count > 0 Then
							almacen = String.Join(",", almacenes.ToArray)
						Else
							CMensajes.MostrarMensaje("Seleccione por lo menos una opción para almacén.", CMensajes.Tipo.GetError, wucMessageRPT_DiferenciaEntreMarbetes)
							Exit Sub
						End If
					End If
				End If
				CatalogoReportes_CRV13(pasillo, nivel, almacen, reporte)
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_DiferenciaEntre_Marbetes.DataBinding
		If Session("RPT_DiferenciaEntreMarbetes") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_DiferenciaEntre_Marbetes.ReportSource = rpt
				Session("RPT_DiferenciaEntreMarbetes") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_DiferenciaEntre_Marbetes.ReportSource = Session("RPT_DiferenciaEntreMarbetes")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal pasillo As String, ByVal nivel As String, ByVal almacen As String, ByVal reporte As Integer)
		ds = RPT_DiferenciaEntreMarbetes(pasillo, nivel, almacen, reporte)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			Dim nombrereporte As String
			If Session("Cedis") = 1 Then
				If reporte = 1 Then
					nombrereporte = NombreReportes.RPT_DiferenciaEntreMarbetes_CDS
				Else
					nombrereporte = NombreReportes.RPT_DiferenciaEntreMarbetesCD_CDS
				End If
			Else
				nombrereporte = NombreReportes.RPT_DiferenciaEntreMarbetes
			End If

			rpt.Load(Server.MapPath(RutaCR & nombrereporte))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "DIFERENCIA ENTRE MARBETES"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_DiferenciaEntre_Marbetes.ReportSource = rpt
			RPT_DiferenciaEntre_Marbetes.DataBind()
		Else
			CMensajes.MostrarMensaje("No existe mercancía con diferencias en conteos.", CMensajes.Tipo.GetInformativo, wucMessageRPT_DiferenciaEntreMarbetes)
		End If
	End Sub

	Public Function RPT_DiferenciaEntreMarbetes(ByVal Pasillo As String, ByVal Nivel As String, ByVal Almacen As String, ByVal Reporte As Integer) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter

			If Session("Cedis") = 1 Then
				If Reporte = 1 Then
					ReDim par(3)
					par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
					par(1) = New SqlParameter("@Perfil", Session("Perfil"))
					par(2) = New SqlParameter("@Nivel", Nivel)
					par(3) = New SqlParameter("@Pasillo", Pasillo)

					ds = DatosSQL.funcioncp("RPT_DiferenciaEntreMarbetes_CDS", par)
				Else
					ReDim par(2)
					par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
					par(1) = New SqlParameter("@Perfil", Session("Perfil"))
					par(2) = New SqlParameter("@Almacen", Almacen)
					ds = DatosSQL.funcioncp("RPT_DiferenciaEntreMarbetesCD_CDS", par)
				End If

				If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					If Reporte = 1 Then
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@Perfil", Session("Perfil"))
						par(2) = New SqlParameter("@Nivel", Nivel)
						par(3) = New SqlParameter("@Pasillo", Pasillo)
						dsExcel = DatosSQL.funcioncp("RPT_DiferenciaEntreMarbetes_CDSExcel", par)
					Else
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@Perfil", Session("Perfil"))
						par(2) = New SqlParameter("@Almacen", Almacen)
						dsExcel = DatosSQL.funcioncp("RPT_DiferenciaEntreMarbetesCD_CDSExcel", par)
					End If
					If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
						Reportes.REPEXCEL(dsExcel, "Reporte Diferencia entre Marbetes", "DiferenciaMarbetes_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
					End If
				End If
			Else
				par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(1) = New SqlParameter("@Perfil", Session("Perfil"))
				ds = DatosSQL.funcioncp("RPT_DiferenciaEntreMarbetes", par)
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Protected Sub cmbAlmacen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAlmacen.SelectedIndexChanged
		Try
			If Me.cmbAlmacen.SelectedValue.ToString = "-1" Then
				CMensajes.MostrarMensaje("Seleccione una opción para mostrar.", CMensajes.Tipo.GetError, wucMessageRPT_DiferenciaEntreMarbetes)
			ElseIf Me.cmbAlmacen.SelectedValue.ToString = "1" Then
				LlenaNivel()
				LlenaPasillo()
			ElseIf Me.cmbAlmacen.SelectedValue.ToString = "2" Then
				LlenaAlmacenes()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub RackNacional()

	End Sub

	Private Sub CrossDock()

	End Sub

	Private Sub LlenaPasillo()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("Pasillo_Buscar", par)

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.cmbPasillo.DataSource = ds.Tables(0)
				Me.cmbPasillo.DataTextField = "Pasillo"
				Me.cmbPasillo.DataValueField = "Pasillo"
				Me.cmbPasillo.DataBind()
				Me.lblPasillo.Visible = True
				Me.cmbPasillo.Visible = True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub LlenaNivel()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("Nivel_Buscar", par)

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.DgNivel.DataSource = ds.Tables(0)
				Me.DgNivel.DataBind()
				Me.DgNivel.Visible = True
				Me.DGAlmacen.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub LlenaAlmacenes()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("AlmacenCDS_Buscar", par)

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.DGAlmacen.DataSource = ds.Tables(0)
				Me.DGAlmacen.DataBind()
				Me.DGAlmacen.Visible = True
				Me.DgNivel.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub SelectAllNivel(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chkHeader As CheckBox = CType(Me.DgNivel.Controls(0).Controls(0).FindControl("CheckBox1"), CheckBox)
		If chkHeader.Checked = True Then
			For x As Integer = 0 To Me.DgNivel.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DgNivel.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = True
			Next
		Else
			For x As Integer = 0 To Me.DgNivel.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DgNivel.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = False
			Next
		End If
	End Sub

	Protected Sub SelectAllAlmacen(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chkHeader As CheckBox = CType(Me.DGAlmacen.Controls(0).Controls(0).FindControl("CheckBox1"), CheckBox)
		If chkHeader.Checked = True Then
			For x As Integer = 0 To Me.DGAlmacen.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DGAlmacen.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = True
			Next
		Else
			For x As Integer = 0 To Me.DGAlmacen.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DGAlmacen.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = False
			Next
		End If
	End Sub
End Class