﻿Imports System.Data
Imports System.Data.SqlClient
Public Class CorreccionesFinalesPorEAN
    Inherits System.Web.UI.Page

	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageCorreccionesFinalesPorEAN.Visible = False

		If Not Page.IsPostBack Then
			Dim Tipo As String = Request.QueryString("Tipo")

			If Tipo = "2" Then
				Me.lblTitulo.Text = "Cambios a toma física por EAN"
				Me.lblTipo.Text = Tipo
			Else
				Me.lblTitulo.Text = "Correcciones finales 80/20 por EAN"
				Me.lblTipo.Text = Tipo
			End If
			CargarAlmacenes()
		End If
	End Sub

	Private Sub ImgBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgBuscar.Click
		Try
			Me.txtConteoFinal.Text = ""
			Me.txtCantidad.Text = ""
			Me.txtDescripcion.Text = ""
			Me.txtDepartamento.Text = ""
			If Me.txtEAN.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese el EAN a buscar.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorEAN)
			Else
				CatalogoGeneral_BuscarXEAN()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoGeneral_BuscarXEAN()
		Try
			Dim ds As New DataSet
			Dim Par(3) As SqlParameter
			Par(0) = New SqlParameter("@EAN", Me.txtEAN.Text.Trim)
			Par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			Par(2) = New SqlParameter("@Tipo", CInt(Me.lblTipo.Text) + 1)
			Par(3) = New SqlParameter("@IdAlmacen", Me.cboAlmacenes.SelectedValue)
			ds = DatosSQL.funcioncp("CatalogoGeneral_BuscarXEAN", Par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				If ds.Tables(0).Columns(0).ColumnName = "Error" Then
					CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0), CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorEAN)
				Else
					If IsDBNull(ds.Tables(0).Rows(0).Item("OBS")) = False Then
						Me.txtDescripcion.Text = ds.Tables(0).Rows(0).Item("OBS")
					Else
						Me.txtDescripcion.Text = ""
					End If
					If IsDBNull(ds.Tables(0).Rows(0).Item("NOM_DEPTO")) = False Then
						Me.txtDepartamento.Text = ds.Tables(0).Rows(0).Item("NOM_DEPTO")
					Else
						Me.txtDepartamento.Text = ""
					End If
					'ConteoFinal
					If IsDBNull(ds.Tables(0).Rows(0).Item("ConteoFinal")) = False Then
						Me.txtCantidad.Text = ds.Tables(0).Rows(0).Item("ConteoFinal")
					Else
						Me.txtCantidad.Text = ""
					End If
					Me.btnGuardar.Visible = True
					Me.btnCancelar.Visible = True
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
		Try
			If Me.txtEAN.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese el EAN.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorEAN)
			ElseIf Me.txtConteoFinal.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese la cantidad final.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorEAN)
			ElseIf IsNumeric(Me.txtConteoFinal.Text.Trim) = False Then
				CMensajes.MostrarMensaje("La cantidad final debe ser un dato númerico.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorEAN)
			ElseIf CDbl(Me.txtConteoFinal.Text.Trim) < 0 Then
				CMensajes.MostrarMensaje("La cantidad final debe ser un número positivo.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorEAN)
			Else
				CatalogoGeneral_BuscarXEAN()

				Dim par(5) As SqlParameter
				par(0) = New SqlParameter("@Accion", 2) 'Modificar
				par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
				par(3) = New SqlParameter("@EAN", Me.txtEAN.Text.Trim)
				par(4) = New SqlParameter("@ConteoFinal", CDbl(Me.txtConteoFinal.Text.Trim))
				par(5) = New SqlParameter("@Almacen", Me.cboAlmacenes.SelectedValue)
				DatosSQL.procedimientocp("ConteoConsolidado_Administracion", par)
				CMensajes.MostrarMensaje("Cantidad final modificada correctamente.", CMensajes.Tipo.GetExito, wucMessageCorreccionesFinalesPorEAN)
				Me.txtEAN.Text = ""
				Me.txtConteoFinal.Text = ""
				Me.txtCantidad.Text = ""
				Me.txtDescripcion.Text = ""
				Me.txtDepartamento.Text = ""
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        Me.txtEAN.Text = ""
        Me.txtConteoFinal.Text = ""
        Me.txtCantidad.Text = ""
        Me.txtDescripcion.Text = ""
        Me.txtDepartamento.Text = ""
    End Sub

    Sub CargarAlmacenes()
        Try
            Dim dtAlmacenes As New DataTable
            dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

            Me.cboAlmacenes.DataSource = dtAlmacenes
            Me.cboAlmacenes.DataValueField = "idAlmacen"
            Me.cboAlmacenes.DataTextField = "idAlmacen"
            Me.cboAlmacenes.DataBind()
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

End Class