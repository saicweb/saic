﻿Imports System.Data.SqlClient
Imports DTS
Imports DTSPump
Imports DTSCustTasks

Public Class TransferirInfPreconteoExterno
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
					Response.Redirect("Default.aspx")
				ElseIf Session("Perfil").ToString = "R" Then
					CMensajes.MostrarMensaje("El perfil de Inventario Rotativo no tiene permiso de acceso a esta sección.", CMensajes.Tipo.GetError, wucMessageTransferirInfPreconteoExterno)
				Else
					If (Session("TipoUsuario") = "A" OrElse (Session("TipoUsuario") = "U" AndAlso Session("TransferirInformacionPreconteoExternoAlSAIC") = "1")) Then
						Me.btnTransferir.Visible = True
					End If
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar transferencia de información"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageTransferirInfPreconteoExterno.Visible = False
		Try
			Me.lblTiempoProceso.Text = "Hora de Inicio: " & Now.ToLongTimeString
			DTSDatos()
			Me.lblTiempoProceso.Text &= "  Hora de Término: " & Now.ToLongTimeString
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageTransferirInfPreconteoExterno.Visible = False
	End Sub

#End Region

	Private Sub btnTransferir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTransferir.Click
		Try
			Dim DS As New DataSet
			Dim par(0) As SqlParameter

			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			DS = DatosSQL.funcioncp("Preconteos_Buscar", par)

			If Not DS Is Nothing AndAlso DS.Tables.Count > 0 AndAlso DS.Tables(0).Rows.Count > 0 AndAlso CInt(DS.Tables(0).Rows(0).Item(0)) > 0 Then
				CMensajes.MostrarMensaje("Existen articulos registrados en Preconteos, favor de inicializar antes de proceder.", CMensajes.Tipo.GetInformativo, wucMessageTransferirInfPreconteoExterno)
			Else '---------REALIZAR LA TRANSFERENCIA
				Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
				lblTexto.Text = "<div align=""center"">¿Desea continuar con la transferencia de información preconteos de externo a SAIC?</div>"
				winConfirm1.MuestraDialogo()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub DTSDatos()
		Dim goPackageOld As New DTS.Package
		Dim objPkg As New DTS.Package2

		Try
			Dim Continuar As Boolean = False
			Dim IPTienda As String = ""
			'**-----CONSULTAR IP DE TIENDA-----**
			Dim DS As New DataSet
			Dim parTienda(0) As SqlParameter
			parTienda(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			DS = DatosSQL.funcioncp("Tiendas_Buscar", parTienda)
			If Not DS Is Nothing AndAlso DS.Tables.Count > 0 AndAlso DS.Tables(0).Rows.Count > 0 AndAlso IsDBNull(DS.Tables(0).Rows(0).Item("IP")) = False AndAlso CStr(DS.Tables(0).Rows(0).Item("IP")).Trim.Length > 0 Then
				IPTienda = DS.Tables(0).Rows(0).Item("IP")
				Continuar = True
			Else '---------NO SE ENCONTRO LA IP
				Continuar = False
				CMensajes.MostrarMensaje("Favor de ingresar al catalogo de tiendas e ingrese la informacion requerida antes de proceder.", CMensajes.Tipo.GetInformativo, wucMessageTransferirInfPreconteoExterno)
			End If

			If Continuar = True Then

				'**-----PROCESO DTS-----**
				Dim ServidorBDSAIC As String = ConfigurationManager.AppSettings("ServidorBDSAIC")
				Dim UserBDSAIC As String = ConfigurationManager.AppSettings("UserBDSAIC")
				Dim PasswordBDSAIC As String = ConfigurationManager.AppSettings("PasswordBDSAIC")
				Dim NameDTS As String = ConfigurationManager.AppSettings("NameDTSTransferirInfPreconteoExterno")

				Dim strError, iCount

				'Remove all Global Variables
				'For Each gv As GlobalVariable In objPkg.GlobalVariables
				'    objPkg.GlobalVariables.Remove(gv.Name)
				'Next

				'-----Set the FileName Global Variable-----
				objPkg = goPackageOld

				'-----Create and Execute the package-----
				objPkg = Server.CreateObject("DTS.Package")
				'objPkg.LoadFromSQLServer("<SERVER NAME>", "<USER NAME>", "<PASSWORD>", _
				'DTSSQLStgFlag_Default, "", "", "", "<PACKAGE NAME>")
				' objPkg.LoadFromSQLServer(ServidorBDSAIC, UserBDSAIC, PasswordBDSAIC, _
				'DTSSQLServerStorageFlags.DTSSQLStgFlag_Default, "", "", "", NameDTS)

				objPkg.LoadFromSQLServer(ServerName:=ServidorBDSAIC.Trim, ServerUserName:=UserBDSAIC.Trim, ServerPassword:=PasswordBDSAIC.Trim,
				PackageName:=NameDTS.Trim)

				'-----VARIABLES GLOBALES-----
				'Remove all Global Variables
				For Each gv As GlobalVariable In objPkg.GlobalVariables
					objPkg.GlobalVariables.Remove(gv.Name)
				Next

				''Set the FileName Global Variable
				objPkg.GlobalVariables.AddGlobalVariable("IdTienda", CStr(Session("IdTienda")))
				objPkg.GlobalVariables.AddGlobalVariable("IdUsuario", CStr(Session("IdUsuario")))
				objPkg.GlobalVariables.AddGlobalVariable("ServerUsuario", UserBDSAIC)
				objPkg.GlobalVariables.AddGlobalVariable("ServerPassword", PasswordBDSAIC)
				objPkg.GlobalVariables.AddGlobalVariable("ServerSaicDataSource", ServidorBDSAIC)
				objPkg.GlobalVariables.AddGlobalVariable("ServerTiendaDataSource", IPTienda)

				objPkg.FailOnError = True
				Try
					objPkg.Execute()
					'-----Check For Errors-----
					For iCount = 1 To objPkg.Steps.Count
						If objPkg.Steps.Item(iCount).ExecutionResult = DTS.DTSStepExecResult.DTSStepExecResult_Failure Then
							Dim sMessage As String
							Dim lErrNum As Long
							Dim sDescr As String
							Dim sSource As String

							objPkg.Steps.Item(iCount).GetExecutionErrorInfo(lErrNum, sSource, sDescr)

							strError &= strError + objPkg.Steps.Item(iCount).Name + " failed. " + Chr(13) +
						  "Error: " + CStr(lErrNum) + " - " + sDescr + Chr(13)

						End If
					Next
				Catch ex As Exception
					'-----Check For Errors-----
					For iCount = 1 To objPkg.Steps.Count
						If objPkg.Steps.Item(iCount).ExecutionResult = DTS.DTSStepExecResult.DTSStepExecResult_Failure Then
							Dim sMessage As String
							Dim lErrNum As Long
							Dim sDescr As String
							Dim sSource As String

							objPkg.Steps.Item(iCount).GetExecutionErrorInfo(lErrNum, sSource, sDescr)

							strError &= strError + objPkg.Steps.Item(iCount).Name + " failed. " + Chr(13) +
						  "Error: " + CStr(lErrNum) + " - " + sDescr + Chr(13)

						End If
					Next
				End Try
				If strError = "" Then
					'Me.lblMensaje.Text &= "Data Imported Successfully."
				Else
					CMensajes.MostrarMensaje(strError, CMensajes.Tipo.GetInformativo, wucMessageTransferirInfPreconteoExterno)
				End If

				objPkg.UnInitialize()

			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

End Class