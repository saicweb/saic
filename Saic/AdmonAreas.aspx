﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonAreas.aspx.vb" Inherits="Saic.AdmonAreas" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td >
                <asp:Label runat="server" Text="Áreas" class="titulos"></asp:Label>
            </td>
        </tr>
         <tr>
            <td>
                <wum:wucMessage ID="wucMessageAdmonAreas" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="85%">
                    <tr>
                        <td align="center">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table runat="server" width="100%">
                                        <tr>
                                            <td colspan="3">
                                                <table runat="server" width="100%" cellpadding="10">
                                                    <tr>
                                                        <td>
                                                            <asp:Label runat="server">Id de Area:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtIdArea" runat="server" Width="176px" MaxLength="3"></asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" Font-Size="Small" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtIdArea"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label3" runat="server">Nombre de Area:</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNombreArea" TabIndex="2" runat="server" Width="176px" MaxLength="50"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Font-Size="Small" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtNombreArea"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td>
                                                <asp:Button ID="btnGuardar"  runat="server" CssClass="btn" Text="Agregar Area"></asp:Button>
                                            </td> 
                                            <td>
                                                <asp:Button ID="btnModificar" runat="server" CausesValidation="false" Visible="False" Text="Actualizar Datos" CssClass="btn"></asp:Button>
                                            </td> 
                                            <td>
                                                <asp:Button ID="btnEliminar" runat="server" CausesValidation="false" Visible="False" Text="Eliminar Datos" CssClass="btn"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center"> <br />
                                                <asp:Panel runat="server" Width="520px" Height="350px" ScrollBars="Auto">
                                                    <asp:GridView ID="gvArea" AutoGenerateColumns="false" runat="server" SkinID="Grid3" Width="500px">
                                                        <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle> 
                                                        <Columns>
                                                            <asp:BoundField DataField="IdArea" HeaderText="IdArea" HeaderStyle-Width="70px" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                            <asp:BoundField DataField="NombreArea" HeaderText="Descripción" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                                            <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkmodificar" CausesValidation="false" CommandName="Modificar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Modificar</asp:LinkButton>
                                                                </ItemTemplate> 
                                                            </asp:TemplateField>
                                                              <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                               <ItemTemplate>
                                                                   <asp:LinkButton ID="lnkEliminar" CausesValidation="false" CommandName="Eliminar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Eliminar</asp:LinkButton>
                                                                   </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="gvArea" />
                                    <asp:PostBackTrigger ControlID="btnGuardar" />
                                    <asp:PostBackTrigger ControlID="btnModificar" />
                                </Triggers>
                            </asp:UpdatePanel>
                            
                        </td>
                    </tr>
                </table>
                   <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
