﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TransferirInfDispositivo.aspx.vb" Inherits="Saic.TransferirInfDispositivo" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <script type="text/javascript" language="javascript">
        function deshabilitar(){
                document.TransferirInfDispositivo.btnCargar.disabled=true;
        }
    </script>
     <script type='text/javascript'>
        function Forzar() {
            location.reload(true);
        }
    </script>
    
    
    
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Transferir información a dispositivos" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageTransferirInfDispositivo" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="55%" align="center">
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="uplXML" runat="server" Visible="false" >
                                <ContentTemplate>
                                    <table runat="server" width="100%" align="center">
                                        <tr>
                                            <td align="center">
                                                <asp:FileUpload ID="singleUpload" runat="server" CssClass="upload"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2"><br />
                                                <asp:Button ID="btnCargar" runat="server" Text="Transferir información" CssClass="btn" OnClientClick="deshabilitar();"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnCargar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td><br /><br />
                            <table runat="server" id="TblDispositivosXAdjuntar">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Width="192px" Text=" Dispositivos por adjuntar: "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboTerminal" runat="server" Width="152px" CssClass="dropdown" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblAlmacen" runat="server" Width="184px" Text="Almacén: "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cboAlmacen" runat="server" Width="152px" CssClass="dropdown" AutoPostBack="true"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblConteos" runat="server" Width="200px" Text="Opción de múltiples conteos:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="rbtnNumConteo" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" CssClass="inline radio" CellPadding="15">
                                            <asp:ListItem Value="1" Selected="true">Conteo 1</asp:ListItem>
                                            <asp:ListItem Value="2">Conteo 2</asp:ListItem>
                                            <asp:ListItem Value="3">Conteo 3</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:checkbox id="chkImprimirMarbetes" runat="server" Text="Imprimir marbetes adjuntados" CssClass="inline checkbox" AutoPostBack="true" ></asp:checkbox>
                                    </td>
                                    <td align="center">
                                        <span><asp:hyperlink id="Link" runat="server" ForeColor="Blue" Visible="False">PDF</asp:hyperlink></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                        <span><asp:hyperlink id="Link2" runat="server" ForeColor="Blue" Visible="False">Excel</asp:hyperlink></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <asp:label id="lblTiempoProceso" runat="server" Width="330px"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <wum:wucMessage ID="wucMessageMensaje" runat="server" />
                        </td>
                    </tr>
                    <tr>
						<td colspan="2" align="center"><br />
							<asp:label id="lblCont" runat="server" ></asp:label>
						</td>
					</tr>
                    <tr>
						<td align="center">
							<p><asp:label id="Label7" runat="server" Text="Información reciente del dispositivo antes sincronizado y previa a adjuntar" Font-Bold="True"></asp:label></p>
						</td>
					</tr>
                    <tr>
						<td colspan="2" align="center"><br />
                            <asp:button id="btnEliminar" runat="server" Text="Baja" Enabled="False" CssClass="btn"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="btnGuardar" runat="server" Text="Grabar" Enabled="False" Visible="false" CssClass="btn"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;
							<asp:button id="btnAdjuntar" runat="server" Text="Adjuntar" Enabled="False" CssClass="btn"></asp:button>                   
                        </td>                      
					</tr>
                    <tr>
                        <td align="center"><br />
                            <asp:hyperlink id="linkArtSet" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink><br /><br />
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:GridView id="dg" runat="server" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="IdRow" HeaderStyle-width="60px" ItemStyle-Width="60"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="Marbete" DataField="IdMarbete" HeaderStyle-width="120px" ItemStyle-Width="120"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-width="90px" ItemStyle-Width="90px"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">		       
                                            <HeaderTemplate>
                                                <asp:Label ID="lbltitulo" runat="server" Text="Marcar p/eliminar&lt;br /&gt;"></asp:Label>
                                                <asp:CheckBox id="Checkbox3" CssClass="checkbox inline" runat="server" TextAlign="Left" AutoPostBack="true" OnCheckedChanged="Checkbox3_CheckedChanged"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox id="Checkbox4" CssClass="checkbox inline" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Fecha hora" DataField="FechaHora" HeaderStyle-width="130px" ItemStyle-Width="130"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />                  
                                        <asp:TemplateField HeaderText="Acci&#243;n" Visible="false" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkmodificar" runat="server" CommandName="Modificar" Text="Modificar" CommandArgument="<%#Container.DataItemIndex%>"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:BoundField HeaderText="Area" DataField="Area" Visible="false" HeaderStyle-width="150px" ItemStyle-Width="150"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />--%>
                                    </Columns>
						        </asp:GridView>
                                <asp:GridView id="dgArticulosSet" runat="server" AutoGenerateColumns="False" SkinID="Grid3" Visible="false">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="Marbete" DataField="IdMarbete" HeaderStyle-width="120" ItemStyle-Width="120"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="Código de barras" DataField="EAN" HeaderStyle-width="120px" ItemStyle-Width="120"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="Descripción" DataField="Descripcion" HeaderStyle-width="150px" ItemStyle-Width="150"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />                  
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-width="90px" ItemStyle-Width="90px"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">		       
                                            <HeaderTemplate>
                                                <asp:Label ID="lbltitulo" runat="server" Text="Marcar p/eliminar&lt;br /&gt;"></asp:Label>
                                                <asp:CheckBox id="chkETDgArt" CssClass="checkbox inline" runat="server" TextAlign="Left" AutoPostBack="true" OnCheckedChanged="chkETDgArt_CheckedChanged"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox id="chkEUnoDgArt" CssClass="checkbox inline" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Terminal" DataField="Terminal" HeaderStyle-width="150px" ItemStyle-Width="150"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />                  
                                        <asp:BoundField HeaderText="Almacen" DataField="Almacen" HeaderStyle-width="150px" ItemStyle-Width="150"  HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />                  
                                    </Columns>
						        </asp:GridView>
                            </asp:Panel>   
                            <asp:button id="btnEliminarArticulosSet" runat="server" Text="Eliminar" visible="false" CssClass="btn"></asp:button>
                        </td>
                    </tr>
                </table>
                <table align="center">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table align="center">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
