﻿Imports System.Data
Imports System.Data.SqlClient
Public Class AdmonMarbetes
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageAdmonMarbete.Visible = False

		If Not Page.IsPostBack Then
			Try
				If Session("Cedis") = 1 Then
					Me.txtIdMarbete.MaxLength = 17
				Else
					Me.txtIdMarbete.MaxLength = 11
				End If

				Dim Accion As String = Request.QueryString("Accion")
				If Accion = 1 Then 'NUEVO
					Me.lblTitulo.Text = "Agregar marbete"
					Me.lblMarbeteEliminar.Visible = False
					Me.PnlMarbeteEliminar.Visible = False
					Me.btnAgregar.Visible = True
					Me.cboMarbetes.Visible = False
					Me.lblAlmacen.Visible = False
					Me.lblAlmacen1.Visible = False
					Me.cmbAlmacen.Visible = False
					Me.cmbArea.Visible = False
					If Session("Cedis") = 1 Then
						Me.lblUbicacionFisica.Visible = True
						Me.txtUbicacionFisica.Visible = True
					End If
				ElseIf Accion = 2 Then 'MODIFICAR
					Me.lblTitulo.Text = "Cambiar nombre a marbete"
					Me.lblIdMarbete.Text = "Nombre Nuevo"
					Me.btnActualizar.Visible = True
					Me.lblMarbeteEliminar.Visible = False
					Me.PnlMarbeteEliminar.Visible = False
					Me.cboArea.Visible = False
					Me.lblArea.Visible = False
					Me.lblRangoInicial.Visible = True
					Me.cboRangoInicial.Visible = True
					Me.lblUbicacionFisica.Visible = False
					Me.txtUbicacionFisica.Visible = False
					Me.cboMarbetes.Visible = False
					Me.cmbAlmacen.Visible = False
					Me.cmbArea.Visible = False
					If Session("Sociedad").ToString <> "3" Then
						Me.lblAlmacen.Visible = True
						Me.lblAlmacen1.Visible = True
						Me.rbtnConteos.Visible = True
					End If
					CatalogoMarbetes_Buscar()
				ElseIf Accion = 3 Then 'ELIMINAR
					Me.lblTitulo.Text = "Eliminar marbete"
					Me.lblMarbeteEliminar.Visible = True
					Me.PnlMarbeteEliminar.Visible = True
					Me.btnEliminar.Visible = True
					Me.cboArea.Visible = False
					Me.lblArea.Visible = False
					Me.lblRangoInicial.Visible = False
					Me.cboRangoInicial.Visible = False
					Me.lblUbicacionFisica.Visible = False
					Me.txtUbicacionFisica.Visible = False
					Me.txtIdMarbete.Visible = False
					Me.cboMarbetes.Visible = False
					Me.lblIdMarbete.Visible = False
					Me.lblAlmacen.Visible = True
					Me.lblAlmacen1.Visible = False
					If Session("Sociedad").ToString <> "3" Then
						Me.rbtnConteos.Visible = True
						Me.cmbAlmacen.Visible = True
						CargarAlmacenEliminar()
					Else
						Me.lblAlmacen.Text = "Area: "
						Me.cmbArea.Visible = True
						CargarAreaEliminar()
					End If
					CargarMarbetesEliminar()
				End If
				Llenar_Areas()
				CargarAlmacenModificar()
				CargarAlmacenEliminar()
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			Select Case Me.HddnAccion.Value.ToString.Trim
				Case "Actualizar"
					Dim ds As New DataSet
					Dim par(3) As SqlParameter
					par(0) = New SqlParameter("@VPerfil", Session("Perfil").ToString.ToUpper)
					par(1) = New SqlParameter("@VMarbete", Me.txtIdMarbete.Text)
					par(2) = New SqlParameter("@VAlmacen", Me.lblAlmacen1.Text)
					par(3) = New SqlParameter("@IdTienda", Session("IdTienda"))
					ds = DatosSQL.funcioncp("ExistenciaMarbete2", par)
					Dim mensage As String = ds.Tables(0).Rows(0)(0)

					If mensage.StartsWith("ERROR:") Then
						CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0).ToString, CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
					Else
						Session("AccionMarbete") = 2
						CatalogoMarbetes_Administracion(2)
						Me.txtIdMarbete.Text = ""
						Me.CatalogoMarbetes_Buscar()
						CMensajes.MostrarMensaje("Nombre de marbete modificado correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonMarbete)
					End If

				Case "Eliminar"
					Session("AccionMarbete") = 3
					CatalogoMarbetes_Administracion(3)
					CargarMarbetesEliminar()
					CMensajes.MostrarMensaje("Marbete eliminado correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonMarbete)
			End Select

			winConfirm1.OcultaDialogo()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageAdmonMarbete.Visible = False
		winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Sub MarbeteTemporal()
		Try
			If Me.txtIdMarbete.Text.Trim.Length = 0 Then
				CMensajes.Show("Ingrese el Marbete.")
			End If

			Dim IdMarbete As String = ""
			IdMarbete = Me.txtIdMarbete.Text.Trim
			If IdMarbete.Length < 17 Then
				CMensajes.MostrarMensaje("El marbete debe tener 17 caracteres.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
			ElseIf IdMarbete.StartsWith(CStr(Session("idtienda"))) = False Then
				CMensajes.MostrarMensaje("El marbete debe iniciar con el código de tienda " & CStr(Session("IdTienda")) & ", con la cual se esta trabajando.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
			Else
				Session("IdMarbeteAgregado") = IdMarbete

				If Request.QueryString("Refresh") = "S" Then
					Response.Redirect("AgregarArticuloMarbete.aspx?SID=" & Request.QueryString("SID") & "&Origen=AM&Refresh=S&Ubicacion=" & Me.txtUbicacionFisica.Text.Trim, False)
				Else
					Response.Redirect("AgregarArticuloMarbete.aspx?SID=" & Request.QueryString("SID") & "&Origen=AM&Ubicacion=" & Me.txtUbicacionFisica.Text.Trim, False)
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoMarbetes_Administracion(ByVal Accion As Int16)
		Try
			'Accion 1=Agregar 2=Modificar 3=Eliminar
			Dim IdMarbete As String = ""

			If Accion = 1 Then
				IdMarbete = Me.txtIdMarbete.Text.Trim
				If Me.txtIdMarbete.Text.Trim.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese el marbete.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
					GoTo salir
				ElseIf Session("Cedis") = 0 Then
					If IdMarbete.Length < 11 Then
						CMensajes.MostrarMensaje("El marbete debe tener 11 dígitos.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					ElseIf IdMarbete.StartsWith(Right(CStr(Session("idtienda")), 3)) = False Then
						CMensajes.MostrarMensaje("El marbete debe iniciar con el número de tienda " & Right(CStr(Session("idtienda")), 3) & ", con la cual se esta trabajando.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					End If
				ElseIf Session("Cedis") = 1 Then
					If IdMarbete.Length < 17 Then
						CMensajes.MostrarMensaje("El marbete debe tener 17 caracteres.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					ElseIf IdMarbete.StartsWith(Session("idtienda")) = False Then
						CMensajes.MostrarMensaje("El marbete debe iniciar con el número de tienda " & CStr(Session("IdTienda")) & ", con la cual se esta trabajando.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					End If
				End If
			ElseIf Accion = 2 Then
				IdMarbete = Me.txtIdMarbete.Text.Trim
				If Session("Cedis") = 0 Then
					If IdMarbete.Length < 11 Then
						CMensajes.MostrarMensaje("El marbete debe tener 11 dígitos.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					ElseIf IdMarbete.StartsWith(Right(CStr(Session("idtienda")), 3)) = False Then
						CMensajes.MostrarMensaje("El marbete debe iniciar con el número de tienda " & Right(CStr(Session("idtienda")), 3) & ", con la cual se esta trabajando.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					End If
				ElseIf Session("Cedis") = 1 Then
					If IdMarbete.Length < 17 Then
						CMensajes.MostrarMensaje("El marbete debe tener 17 caracteres.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					ElseIf IdMarbete.StartsWith(Session("idtienda")) = False Then
						CMensajes.MostrarMensaje("El marbete debe iniciar con el número de tienda " & CStr(Session("IdTienda")) & ", con la cual se esta trabajando.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					End If
				ElseIf Me.cboRangoInicial.SelectedValue = "[Seleccione]" Then
					CMensajes.MostrarMensaje("Ingrese el rango inicial.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
					GoTo salir
				ElseIf Me.txtIdMarbete.Text.Trim.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese el nombre nuevo.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
					GoTo salir
				End If
			ElseIf Accion = 3 Then
				If Session("Sociedad").ToString <> "3" AndAlso Me.cmbAlmacen.SelectedValue = "[Selecciona]" Then
					CMensajes.MostrarMensaje("Seleccione un almacén.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
					GoTo salir
				Else
					Dim errEliminar As Integer = 0
					For i As Integer = 0 To Me.dg.Rows.Count - 1
						Dim ck As CheckBox = CType(dg.Rows(i).Cells(0).Controls(1), CheckBox)
						If ck.Checked = True Then
							errEliminar += 1
						End If
					Next
					If errEliminar = 0 Then
						CMensajes.MostrarMensaje("Seleccione por lo menos un marbete a eliminar.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
						GoTo salir
					End If
				End If
			End If

			If Accion <> 3 Then
				Dim par(8) As SqlParameter
				par(0) = New SqlParameter("@Accion", Accion)
				par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(2) = New SqlParameter("@IdArea", Me.cboArea.SelectedValue)
				par(3) = New SqlParameter("@IdMarbete", IdMarbete)
				par(4) = New SqlParameter("@RangoInicial", Me.cboRangoInicial.SelectedValue)
				par(5) = New SqlParameter("@RangoFinal", "")
				par(6) = New SqlParameter("@UbicacionFisica", Me.txtUbicacionFisica.Text.Trim)
				par(7) = New SqlParameter("@Perfil", Session("Perfil"))
				par(8) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)

				DatosSQL.procedimientocp("CatalogoMarbetes_Administracion", par)
			Else
				For i As Integer = 0 To Me.dg.Rows.Count - 1
					Dim ck As CheckBox = CType(dg.Rows(i).Cells(0).Controls(1), CheckBox)
					If ck.Checked = True Then
						Dim marbete As String = Nothing
						marbete = dg.Rows(i).Cells(1).Text

						Dim par(9) As SqlParameter
						par(0) = New SqlParameter("@Accion", Accion)
						par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(2) = New SqlParameter("@IdArea", Me.cboArea.SelectedValue)
						par(3) = New SqlParameter("@IdMarbete", marbete)
						par(4) = New SqlParameter("@RangoInicial", Me.cboRangoInicial.SelectedValue)
						par(5) = New SqlParameter("@RangoFinal", "")
						par(6) = New SqlParameter("@UbicacionFisica", Me.txtUbicacionFisica.Text.Trim)
						par(7) = New SqlParameter("@Perfil", Session("Perfil"))
						par(8) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)
						par(9) = New SqlParameter("@Almacen", IIf(Session("Sociedad").ToString = "3", "A001", Me.cmbAlmacen.SelectedValue))

						DatosSQL.procedimientocp("CatalogoMarbetes_Administracion", par)

						CatalogoMarbetes_Buscar()
					End If
				Next
			End If

			Select Case Accion
				Case 1
					Session("IdMarbeteAgregado") = Me.txtIdMarbete.Text.Trim

					If Request.QueryString("Refresh") = "S" Then
						Response.Redirect("AgregarArticuloMarbete.aspx?P=" & Session("Perfil") & "&SID=" & Request.QueryString("SID") & "&Origen=AM&Refresh=S", False)
					Else
						Response.Redirect("AgregarArticuloMarbete.aspx?P=" & Session("Perfil") & "&SID=" & Request.QueryString("SID") & "&Origen=AM", False)
					End If
			End Select
salir:
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub Llenar_Areas()
		Try
			Dim ds As New DataSet
			ds = DatosSQL.FuncionSP("Areas_Buscar")
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdArea") = -1
				dr("NombreArea") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Me.cboArea.DataSource = ds.Tables(0)
				Me.cboArea.DataTextField = "NombreArea"
				Me.cboArea.DataValueField = "IdArea"
				Me.cboArea.DataBind()
			Else
				CMensajes.MostrarMensaje("No existen áreas registradas en la base de datos.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Me.cboMarbetes.Items.Clear()
			Me.cboRangoInicial.Items.Clear()
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			par(2) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)

			ds = DatosSQL.funcioncp("Marbetes_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdMarbete") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Me.cboMarbetes.DataSource = ds.Tables(0)
				Me.cboMarbetes.DataValueField = "IdMarbete"
				Me.cboMarbetes.DataTextField = "IdMarbete"
				Me.cboMarbetes.DataBind()

				Me.cboRangoInicial.DataSource = ds.Tables(0)
				Me.cboRangoInicial.DataValueField = "IdMarbete"
				Me.cboRangoInicial.DataTextField = "IdMarbete"
				Me.cboRangoInicial.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub rbtnConteos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnConteos.SelectedIndexChanged
		Me.cmbAlmacen.SelectedIndex = 0

		Select Case Request.QueryString("Accion").ToString
			Case "3"
				CargarMarbetesEliminar()
			Case "2"
				Me.CatalogoMarbetes_Buscar()
		End Select
	End Sub

	Sub CargarAlmacenModificar()
		Try
			Dim dsAlmacenes As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Perfil", Session("Perfil"))
			par(1) = New SqlParameter("@idMarbete", Me.cboRangoInicial.SelectedValue)
			par(2) = New SqlParameter("@IdTienda", Session("IdTienda"))

			dsAlmacenes = DatosSQL.funcioncp("Buscar_Almacen_Marbete", par)

			If dsAlmacenes.Tables(0).Rows.Count = 0 Then
				Me.lblAlmacen1.Text = ""
			Else
				Me.lblAlmacen1.Text = dsAlmacenes.Tables(0).Rows(0).Item(0)
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
		Dim ds As New DataSet
		Dim par(2) As SqlParameter
		par(0) = New SqlParameter("@VPerfil", Session("Perfil").ToString.ToUpper)
		par(1) = New SqlParameter("@VMarbete", Me.txtIdMarbete.Text)
		par(2) = New SqlParameter("@IdTienda", Session("IdTienda"))
		ds = DatosSQL.funcioncp("[ExistenciaMarbete]", par)
		If ds.Tables(0).Rows(0)(0) > 0 Then
			CMensajes.MostrarMensaje("El marbete ya existe, favor de ingresar un marbete diferente.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
		Else
			If Session("Cedis") = 1 Then
				'EL ID DE TIENDA CON EL QUE SE ESTA TRABAJANDO ES CEDIS, POR LO TANTO NO GUARDA EL MARBETE EN CATALOGO
				MarbeteTemporal()
			Else
				'EL ID DE TIENDA CORRESPONDE A TIENDA NORMAL, CONTINUA EL PROCESO NORMALMENTE
				CatalogoMarbetes_Administracion(1)
			End If
		End If
	End Sub

	Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
		Me.HddnAccion.Value = Nothing
		Me.HddnAccion.Value = "Actualizar"
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">¿Está seguro que desea cambiar el nombre del marbete " & Me.cboRangoInicial.SelectedValue & " ?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
		Me.HddnAccion.Value = Nothing
		Me.HddnAccion.Value = "Eliminar"
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">¿Está seguro que desea eliminar el marbete ingresado?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Protected Sub cboRangoInicial_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboRangoInicial.SelectedIndexChanged
		CargarAlmacenModificar()
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(dg.HeaderRow.Cells(0).FindControl("Checkbox3"), CheckBox)
		For Each row As GridViewRow In dg.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(0).FindControl("Checkbox4"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub

	Protected Sub CargarAlmacenEliminar()
		Try
			Dim dtAlmacenes As New DataTable
			dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

			Dim Row As DataRow = dtAlmacenes.NewRow
			Row("idAlmacen") = "0"
			Row("idAlmacen") = "[Selecciona]"
			dtAlmacenes.Rows.InsertAt(Row, 0)
			dtAlmacenes.AcceptChanges()

			Me.cmbAlmacen.DataSource = dtAlmacenes
			Me.cmbAlmacen.DataValueField = "idAlmacen"
			Me.cmbAlmacen.DataTextField = "idAlmacen"
			Me.cmbAlmacen.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub CargarAreaEliminar()
		Try
			Dim dtArea As New DataTable
			dtArea = DatosSQL.FuncionSPDataTable("Buscar_AreaEliminar")

			Dim Row As DataRow = dtArea.NewRow
			Row("IdArea") = "0"
			Row("NombreArea") = "[Selecciona]"
			dtArea.Rows.InsertAt(Row, 0)
			dtArea.AcceptChanges()

			Me.cmbArea.DataSource = dtArea
			Me.cmbArea.DataValueField = "IdArea"
			Me.cmbArea.DataTextField = "NombreArea"
			Me.cmbArea.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub CargarMarbetesEliminar()
		Try
			Me.dg.DataSource = Nothing
			Dim dsMarbetes As New DataSet
			Dim par(4) As SqlParameter

			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			If Session("Sociedad").ToString = "3" Then
				par(2) = New SqlParameter("@Area", Me.cmbArea.SelectedValue)
			Else
				par(2) = New SqlParameter("@IdAlmacen", Me.cmbAlmacen.SelectedValue)
			End If
			par(3) = New SqlParameter("@Accion", 3)
			par(4) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)

			dsMarbetes = DatosSQL.funcioncp("Marbetes_Mostrar", par)

			Me.dg.DataSource = dsMarbetes
			Me.dg.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub cmbAlmacen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAlmacen.SelectedIndexChanged
		If Me.cmbAlmacen.SelectedValue <> "[Selecciona]" Then
			CargarMarbetesEliminar()
		Else
			CMensajes.MostrarMensaje("Selecciona un almacén.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
		End If
	End Sub

	Protected Sub cmbArea_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbArea.SelectedIndexChanged
		If Me.cmbArea.SelectedValue <> "[Selecciona]" Then
			CargarMarbetesEliminar()
		Else
			CMensajes.MostrarMensaje("Selecciona un área.", CMensajes.Tipo.GetError, wucMessageAdmonMarbete)
		End If
	End Sub
End Class


