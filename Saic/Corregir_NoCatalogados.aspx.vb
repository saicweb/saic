﻿Imports System.Data.SqlClient

Public Class Corregir_NoCatalogados
	Inherits System.Web.UI.Page

	Public Shared EANS() As String

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageCorregir_NoCatalogados.Visible = False

		If Not Page.IsPostBack Then
			Try
				NoCatalogados_Buscar()
				llenar_arreglo()
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Sub llenar_arreglo()
		Try
			'llena un arreglo con el valor de los EAN IRM
			Dim cantidad As Integer = Me.DataGrid1.Items.Count
			ReDim EANS(cantidad - 1)
			Dim m As String
			For i As Integer = 0 To cantidad - 1
				EANS(i) = CType(Me.DataGrid1.Items(i).FindControl("txtean"), TextBox).Text
				m = EANS(i)
			Next
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub NoCatalogados_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@Almacen", "0")

			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ArticulosNoCatalogados", par)
			End If

            If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                Me.DataGrid1.DataSource = ds.Tables(0)
                If Session("Sociedad").ToString = "3" Then
                    Me.DataGrid1.Columns(1).HeaderText = "CMaterial"
                End If
                Me.DataGrid1.DataBind()
            Else
                Me.DataGrid1.DataSource = ds.Tables(0)
                If Session("Sociedad").ToString = "3" Then
                    Me.DataGrid1.Columns(1).HeaderText = "CMaterial"
                End If
                Me.DataGrid1.DataBind()
                    CMensajes.MostrarMensaje("No se encontraron artículos no catalogados", CMensajes.Tipo.GetError, wucMessageCorregir_NoCatalogados)
                End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Try
			Dim chkHeader As CheckBox = CType(Me.DataGrid1.Controls(0).Controls(0).FindControl("CheckBox1"), CheckBox)
			If chkHeader.Checked = True Then
				For x As Integer = 0 To Me.DataGrid1.Items.Count - 1
					Dim chkRow As New CheckBox
					chkRow = CType(Me.DataGrid1.Items(x).FindControl("CheckBox2"), CheckBox)
					chkRow.Checked = True
				Next
			Else
				For x As Integer = 0 To Me.DataGrid1.Items.Count - 1
					Dim chkRow As New CheckBox
					chkRow = CType(Me.DataGrid1.Items(x).FindControl("CheckBox2"), CheckBox)
					chkRow.Checked = False
				Next
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub DataGrid1_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
		Try
			Dim mensaje As String = ""
			Dim meserror As String = ""
			Dim EANAnterior As String
			Dim EANActual As String
			Dim renglon As Integer
			Dim chkRow As New CheckBox

			renglon = e.Item.ItemIndex
			chkRow = CType(Me.DataGrid1.Items(renglon).FindControl("CheckBox2"), CheckBox)
			EANActual = CType(Me.DataGrid1.Items(renglon).FindControl("txtean"), TextBox).Text
			EANAnterior = EANS(renglon)

			If EANActual.Length < 13 AndAlso Session("Sociedad").ToString <> "3" Then
				EANActual = EANActual.PadLeft(13, "0")
			End If

			Dim ds As New DataSet
			Dim par(3) As SqlParameter
			If Session("Perfil") = "P" Then
				par(0) = New SqlParameter("@Accion", 6)
			Else
				par(0) = New SqlParameter("@Accion", 5)
			End If
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@EAN", EANActual)
			par(3) = New SqlParameter("@EANAnterior", EANAnterior)

			If Session("Perfil") = "P" Then
				ds = DatosSQL.funcioncp("Preconteos_Administracion", par)
			Else
				ds = DatosSQL.funcioncp("Conteos_Administracion", par)
			End If

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0).Item("STATUS") = 1 Then
				CMensajes.MostrarMensaje("Registro modificado.", CMensajes.Tipo.GetExito, wucMessageCorregir_NoCatalogados)
			Else
				CMensajes.MostrarMensaje("No se pudo modificar el registro.", CMensajes.Tipo.GetError, wucMessageCorregir_NoCatalogados)
			End If

			NoCatalogados_Buscar()
			llenar_arreglo()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class