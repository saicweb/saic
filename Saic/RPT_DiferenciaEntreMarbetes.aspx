﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_DiferenciaEntreMarbetes.aspx.vb" Inherits="Saic.RPT_DiferenciaEntreMarbetes" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Reporte de diferencia entre marbetes" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageRPT_DiferenciaEntreMarbetes" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="40%" align="center" id="TblCabecero">
                    <tr>
                        <td>
                            <table runat="server" id="TblFiltrosCedis" width="350px">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Almacén: " ID="lblAlmacen"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" id="cmbAlmacen" Width="180px" CssClass="dropdown inline" AutoPostBack="true">
                                            <asp:ListItem Selected="True" Value="-1" Text="[Seleccione]"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="0010 Rack Nacional"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Cross Dock"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Pasillo: " ID="lblPasillo" Visible="false"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" id="cmbPasillo" Width="180px" CssClass="dropdown inline" Visible="false"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                            <asp:DataGrid ID="DgNivel" runat="server" AutoGenerateColumns="false" SkinID="DGrid3" Visible="false">
                                                <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Seleccionar" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
											            <HeaderTemplate>
												            <asp:CheckBox id="CheckBox1" runat="server" Text="Todos" AutoPostBack="true" TextAlign="Left"
													            OnCheckedChanged="SelectAllNivel"></asp:CheckBox>
											            </HeaderTemplate>
											            <ItemTemplate>
												            <asp:CheckBox id="CheckBox2" runat="server"></asp:CheckBox>
											            </ItemTemplate>
										            </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Nivel" ReadOnly="true" HeaderText="Nivel" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                            <asp:DataGrid ID="DGAlmacen" runat="server" AutoGenerateColumns="false" SkinID="DGrid3" Visible="false">
                                                <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Seleccionar" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
											            <HeaderTemplate>
												            <asp:CheckBox id="CheckBox1" runat="server" Text="Todos" AutoPostBack="true" TextAlign="Left"
													            OnCheckedChanged="SelectAllAlmacen"></asp:CheckBox>
											            </HeaderTemplate>
											            <ItemTemplate>
												            <asp:CheckBox id="CheckBox2" runat="server"></asp:CheckBox>
											            </ItemTemplate>
										            </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="Ubicacion" ReadOnly="true" HeaderText="Almacen" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </asp:Panel><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="90%">
                    <tr>
                        <td align="center" ><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="false">Descargar Excel</asp:hyperlink></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <div style="z-index:-1">
                                <cr:crystalreportviewer Id="RPT_DiferenciaEntre_Marbetes" runat="server"  HasCrystalLogo="False"
                                AutoDataBind="True" OnDataBinding="_reportViewer_DataBinding" Height="50px"  EnableParameterPrompt="false" EnableDatabaseLogonPrompt="false" ToolPanelWidth="200px" 
                                Width="500px" ToolPanelView="None" HasRefreshButton="True" ShowAllPageIds="True" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" HasDrilldownTabs="False"></cr:crystalreportviewer>  
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
