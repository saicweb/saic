﻿Imports System.Data.SqlClient

Public Class AdmonSubFamilias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("IdUsuario") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        Me.wucMessageAdmonSubFamilias.Visible = False

        Try
            If Not Page.IsPostBack Then
                Departamentos_Buscar()
				'If Not Session("DepartamentoConsultaFamilia") Is Nothing AndAlso CStr(Session("DepartamentoConsultaFamilia")).Length > 0 Then
				'    Me.cboDepartamento.Text = Session("DepartamentoConsultaFamilia")
				'End If
				If Not Session("IdDepartamentoConsultaFamilia") Is Nothing AndAlso CStr(Session("IdDepartamentoConsultaFamilia")).Length > 0 Then
                    Me.cboDepartamento.SelectedValue = Session("IdDepartamentoConsultaFamilia")
                End If
                Llenar_Familias()
				'If Not Session("FamiliaConsultaFamilia") Is Nothing AndAlso CStr(Session("FamiliaConsultaFamilia")).Length > 0 Then
				'    Me.cboFamilia.Text = Session("FamiliaConsultaFamilia")
				'End If
				If Not Session("IdFamiliaConsultaFamilia") Is Nothing AndAlso CStr(Session("IdFamiliaConsultaFamilia")).Length > 0 Then
                    Me.cboFamilia.SelectedValue = Session("IdFamiliaConsultaFamilia")
                End If
				Llenar_SubFamilias("-1", "-1")
			End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

    Protected Sub lnkAgregar_Click(sender As Object, e As EventArgs) Handles lnkAgregar.Click
        Me.Departamentos_Buscar()
        Me.Llenar_Familias()

        Me.lblTitulo.Text = "Agregar subfamilia nueva"
        Me.TblAgregarSubFamilia.Visible = True
        Me.btnAgregar.Visible = True
        Me.btnCancelar.Visible = True
        Me.btnActualizar.Visible = False
        Me.btnBaja.Visible = False
        Me.txtIdSubFamilia.ReadOnly = False
		Me.TblGridSubfamilias.Visible = False
		Me.gridSubfamilia.Visible = False

		If Not Session("IdDepartamentoConsultaFamilia") Is Nothing AndAlso CStr(Session("IdDepartamentoConsultaFamilia")).Length > 0 Then
            Me.cboDepartamento.SelectedValue = Session("IdDepartamentoConsultaFamilia")
        End If
		'      If Not Session("DepartamentoConsultaFamilia") Is Nothing AndAlso CStr(Session("DepartamentoConsultaFamilia")).Length > 0 Then
		'	Me.cboDepartamento.SelectedItem.Text = Session("DepartamentoConsultaFamilia")
		'End If
		Me.Llenar_Familias()
		'      If Not Session("FamiliaConsultaFamilia") Is Nothing AndAlso CStr(Session("FamiliaConsultaFamilia")).Length > 0 Then
		'	Me.cboFamilia.SelectedItem.Text = Session("FamiliaConsultaFamilia")
		'End If
		If Not Session("IdFamiliaConsultaFamilia") Is Nothing AndAlso CStr(Session("IdFamiliaConsultaFamilia")).Length > 0 Then
            Me.cboFamilia.SelectedValue = Session("IdFamiliaConsultaFamilia")
        End If
    End Sub

    Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
		SubFamilias_Administracion(1)
	End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
		SubFamilias_Administracion(2)
	End Sub

    Protected Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
        BorrarSubFamilia(Me.txtIdSubFamilia.Text.Trim)
    End Sub

    Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.txtIdSubFamilia.Text = ""
        Me.txtdescripcion.Text = ""
        Me.cboFamilia.SelectedValue = -1
        Me.cboDepartamento.SelectedValue = -1
        Me.TblAgregarSubFamilia.Visible = False
		Me.TblGridSubfamilias.Visible = True
		Me.gridSubfamilia.Visible = True
		Llenar_SubFamilias("-1", "-1")
	End Sub

    ''' <summary>
    ''' Llena combo de departamentos
    ''' </summary>
    Private Sub Departamentos_Buscar()
        Try
            Me.cboDepartamento.Items.Clear()
            Me.cboDepartamentoGrid.Items.Clear()
            Dim ds As New DataSet
            ds = DatosSQL.FuncionSP("Departamentos_Buscar")
            If Not ds Is Nothing Then
                Dim dr As DataRow
                dr = ds.Tables(0).NewRow
                dr("IdDepartamento") = "-1"
                dr("DescDepto") = "[Seleccione]"
                ds.Tables(0).Rows.InsertAt(dr, 0)

                Me.cboDepartamentoGrid.DataSource = ds.Tables(0)
                Me.cboDepartamentoGrid.DataTextField = "DescDepto"
                Me.cboDepartamentoGrid.DataValueField = "IdDepartamento"
                Me.cboDepartamentoGrid.DataBind()

                Me.cboDepartamento.DataSource = ds.Tables(0)
                Me.cboDepartamento.DataTextField = "DescDepto"
                Me.cboDepartamento.DataValueField = "IdDepartamento"
                Me.cboDepartamento.DataBind()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

    ''' <summary>
    ''' Llena combos de familias
    ''' </summary>
    Private Sub Llenar_Familias()
        Try
            Me.cboFamilia.Items.Clear()
            Dim ds As New DataSet
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@IdDepartamento", Me.cboDepartamento.SelectedValue)
            ds = DatosSQL.funcioncp("Familias_Buscar", par)
            If Not ds Is Nothing Then
                Dim dr As DataRow
                dr = ds.Tables(0).NewRow
                dr("IdFamilia") = "-1"
                dr("DescripcionFamilia") = "[Seleccione]"
                ds.Tables(0).Rows.InsertAt(dr, 0)

                Me.cboFamiliaGrid.DataSource = ds.Tables(0)
                Me.cboFamiliaGrid.DataTextField = "DescripcionFamilia"
                Me.cboFamiliaGrid.DataValueField = "IdFamilia"
                Me.cboFamiliaGrid.DataBind()

                Me.cboFamilia.DataSource = ds.Tables(0)
                Me.cboFamilia.DataTextField = "DescripcionFamilia"
                Me.cboFamilia.DataValueField = "IdFamilia"
                Me.cboFamilia.DataBind()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

	''' <summary>
	''' Llena grid de subfamilias
	''' </summary>
	Private Sub Llenar_SubFamilias(ByVal departamento As String, ByVal familia As String)
		Try
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdDepartamento", departamento)
			par(1) = New SqlParameter("@IdFamilia", familia)
			ds = DatosSQL.funcioncp("SubFamilias_Buscar", par)
			If Not ds Is Nothing Then
				Me.GridSubfamilias.DataSource = ds.Tables(0)
				Me.GridSubfamilias.DataBind()
			Else
				Me.GridSubfamilias.DataSource = Nothing
				Me.GridSubfamilias.DataBind()
				CMensajes.MostrarMensaje("No se encontró ninguna subfamilia.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Elimina la subfamilia seleccionada
	''' </summary>
	''' <param name="id"></param>
	Sub BorrarSubFamilia(ByVal id As String)
        Try

            '@Accion as int,--1=Agregar 2=Modificar 3=Eliminar 
            Dim par(1) As SqlParameter
            par(0) = New SqlParameter("@Accion", 3)
            par(1) = New SqlParameter("@IdSubFamilia", id)

            DatosSQL.funcioncp("SubFamilias_Administracion", par)

            Me.TblAgregarSubFamilia.Visible = False
			Me.TblGridSubfamilias.Visible = True
			Me.gridSubfamilia.Visible = True
			Llenar_SubFamilias("-1", "-1")

			CMensajes.MostrarMensaje("Subfamilia " & id & " eliminada correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSubFamilias)
        Catch ex As SqlException
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

    ''' <summary>
    ''' Llena los controles con la información de la subfamilia seleccionada
    ''' </summary>
    Sub SubFamilia_Datos(ByVal idsubfamilia As String)
        Try
            Dim ds As New DataSet
            Dim PAR(0) As SqlParameter
            PAR(0) = New SqlParameter("@IdSubFamilia", idsubfamilia)
            ds = DatosSQL.funcioncp("SubFamilias_Buscar", PAR)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'SF.IdSubFamilia,SF.DescripcionSubFamilia,	F.IdFamilia,F.IdDepartamento
                If IsDBNull(ds.Tables(0).Rows(0).Item("IdSubFamilia")) = False Then
                    Me.txtIdSubFamilia.Text = ds.Tables(0).Rows(0).Item("IdSubFamilia")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("DescripcionSubFamilia")) = False Then
                    Me.txtdescripcion.Text = ds.Tables(0).Rows(0).Item("DescripcionSubFamilia")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("IdDepartamento")) = False Then
                    Me.cboDepartamento.SelectedValue = ds.Tables(0).Rows(0).Item("IdDepartamento")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("IdFamilia")) = False Then
                    Me.cboFamilia.SelectedValue = ds.Tables(0).Rows(0).Item("IdFamilia")
                End If
            Else
                Me.btnActualizar.Visible = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

    Private Sub SubFamilias_Administracion(ByVal Accion As Integer)
        Try
            If String.IsNullOrEmpty(Me.txtIdSubFamilia.Text) OrElse String.IsNullOrWhiteSpace(Me.txtIdSubFamilia.Text) Then
                CMensajes.MostrarMensaje("Ingrese el Id de la subfamilia.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
            ElseIf String.IsNullOrEmpty(Me.txtdescripcion.Text) OrElse String.IsNullOrWhiteSpace(Me.txtdescripcion.Text) Then
                CMensajes.MostrarMensaje("Ingrese la descripción de la subfamilia.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
            ElseIf Me.cboDepartamento.SelectedValue = "-1" Then
                CMensajes.MostrarMensaje("Seleccione el departamento.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
            ElseIf Me.cboFamilia.SelectedValue = "-1" Then
                CMensajes.MostrarMensaje("Seleccione la familia.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
            Else
                Dim par(3) As SqlParameter
                par(0) = New SqlParameter("@Accion", Accion)
                par(1) = New SqlParameter("@IdSubFamilia", Me.txtIdSubFamilia.Text.ToUpper.Trim)
                par(2) = New SqlParameter("@DescripcionSubFamilia", Me.txtdescripcion.Text.Trim)
                par(3) = New SqlParameter("@IdFamilia", Me.cboFamilia.SelectedValue)

                DatosSQL.funcioncp("SubFamilias_Administracion", par)
                Select Case Accion
                    Case 1 'Nuevo
                        Me.btnActualizar.Visible = False
                        Me.btnAgregar.Visible = True
                        Me.txtIdSubFamilia.ReadOnly = True
                        Me.lblTitulo.Text = "Agregar subfamilia nueva"
                        CMensajes.MostrarMensaje("Subfamilia agregada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSubFamilias)
                    Case 2 'Modificar
                        Me.btnActualizar.Visible = True
                        Me.btnAgregar.Visible = False
                        Me.txtIdSubFamilia.ReadOnly = True
                        Me.lblTitulo.Text = "Modificar datos de subfamilia"
                        CMensajes.MostrarMensaje("Subfamilia modificada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSubFamilias)
                End Select

                Me.TblAgregarSubFamilia.Visible = False
				Me.TblGridSubfamilias.Visible = True
				Me.gridSubfamilia.Visible = True

				Llenar_SubFamilias(Me.cboDepartamento.SelectedValue, Me.cboFamilia.SelectedValue)

			End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

    Private Sub GridSubfamilias_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridSubfamilias.RowCommand
        Dim iRow As Integer
        iRow = Integer.Parse(IIf(e.CommandArgument.ToString = "", 0, e.CommandArgument.ToString))

        If e.CommandName = "IdSubFamilia" Then
            Dim IdSubFamilia As String = Me.GridSubfamilias.DataKeys(iRow)("IdSubFamilia")

            Me.lblTitulo.Text = "Modificar datos de subfamilia"
            Me.TblAgregarSubFamilia.Visible = True
            Me.btnActualizar.Visible = True
            Me.btnBaja.Visible = True
            Me.btnCancelar.Visible = True
            Me.btnAgregar.Visible = False
            Me.txtIdSubFamilia.ReadOnly = True
            Me.txtIdSubFamilia.Text = IdSubFamilia
            Me.SubFamilia_Datos(IdSubFamilia)
			Me.TblGridSubfamilias.Visible = False
			Me.gridSubfamilia.Visible = False
		End If
    End Sub

    Private Sub cboDepartamento_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboDepartamento.SelectedIndexChanged
        If Me.cboDepartamento.Items.Count > 0 AndAlso Me.cboDepartamento.SelectedValue <> "-1" Then
            Me.Llenar_Familias()
        End If
    End Sub

    Private Sub cboDepartamentoGrid_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboDepartamentoGrid.SelectedIndexChanged
		If Me.cboDepartamentoGrid.Items.Count > 0 AndAlso Me.cboDepartamentoGrid.SelectedValue <> -1 Then
			Session("IdDepartamentoConsultaFamilia") = Me.cboDepartamentoGrid.SelectedValue
			Session("DepartamentoConsultaFamilia") = Me.cboDepartamentoGrid.SelectedItem.Text
			Me.Llenar_Familias()
		End If
	End Sub

    Private Sub cboFamiliaGrid_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboFamiliaGrid.SelectedIndexChanged
		If Me.cboFamiliaGrid.Items.Count > 0 AndAlso Me.cboFamiliaGrid.SelectedIndex <> -1 Then
			Session("IdFamiliaConsultaFamilia") = Me.cboFamiliaGrid.SelectedValue
			Session("FamiliaConsultaFamilia") = Me.cboFamiliaGrid.SelectedItem.Text
			Me.Llenar_SubFamilias(Me.cboDepartamentoGrid.SelectedValue, Me.cboFamiliaGrid.SelectedValue)
		End If
	End Sub
End Class