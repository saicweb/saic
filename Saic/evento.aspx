﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="evento.aspx.vb" Inherits="Saic.evento" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="~/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-image:none;">
    <form id="form1" runat="server">
    <div>
    
        <p align="center">
            &nbsp;</p>
        <p align="center">
            <font color="#ff6600" face="Verdana"><strong>EVENTO NO CONTROLADO</strong></font></p>
        <p align="center">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Img/error.png" />
        </p>
        <p align="center">
            <font color="#ff6600" face="Verdana" size="2"><strong>Disculpe las molestias, 
            hemos encontrado un error desconocido en la operación de la página Web. <br /> 
            Un mensaje con el detalle del error ha sido enviado al administrador del sistema 
            para su atención inmediata.</strong></font></p>
        <p align="center">
&nbsp;
            <asp:Button ID="cmdSalir" runat="server" CssClass="btn" style="Z-INDEX: 0" 
                Text="Aceptar" OnClick="cmdSalir_Click" />
        </p>
    
    </div>
    </form>
</body>
</html>