﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CambiarNumTienda_Marbete.aspx.vb" Inherits="Saic.CambiarNumTienda_Marbete" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Cambiar número de tienda a marbete" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCambiarNumTienda_Marbete" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="50%">
                    <tr>
                        <td align="center">
                            <asp:Panel runat="server" Height="200px" Width="195px" ScrollBars="Auto">
                                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="false" SkinID="DGrid3">
                                    <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
                                        <asp:BoundColumn DataField="IdMarbete" ReadOnly="true" HeaderText="Marbete" ItemStyle-Width="120px" HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="Seleccionar" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="CheckBox1" runat="server" CssClass="inline" OnCheckedChanged="SelectAll" Text="Todos" Font-Bold="true" TextAlign="Left"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox2" runat="server" CssClass="checkbox inline"></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>  
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:Button ID="btnCambiar" runat="server" Text="Ejecutar" CssClass="btn"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
