﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Configuration.ConfigurationManager

Public Class AgregarArticuloMarbete
	Inherits System.Web.UI.Page

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageAgregarArtMarbete.Visible = False

		Try
			If Not Page.IsPostBack Then
				If Request.RawUrl.ToString.Contains("Origen") Then
					Dim Origen As String = Request.QueryString("Origen")
					Me.lblOrigen.Text = Origen
				Else
					Me.lblOrigen.Text = ""
				End If

				CatalogoMarbetes_Buscar()

				'Me.txtUbicacion.MaxLength = Integer.Parse(AppSettings("CaracteresUbicacion").ToString)  ' GISP 2017-10-20
				'Session("Prefijoalmacen") = AppSettings("Prefijoalmacen").ToString  ' GISP 2017-10-26

				If Session("Cedis") = 1 Then
					Dim ubicacion As String = Request.QueryString("Ubicacion")
					Me.txtUbicacion.Text = ubicacion
				End If

				Try
					If Me.lblOrigen.Text = "AM" AndAlso Not Session("IdMarbeteAgregado") Is Nothing Then
						Me.cboMarbetes.SelectedValue = Session("IdMarbeteAgregado") 'Si le da click en Agregar Marbete
						'Te Muestra el almacen del marbete
						If Session("Sociedad").ToString = "3" Then
							Me.cboAlmacenes.Visible = False
						Else
							Me.cboAlmacenes.Visible = True
						End If
						Me.lblAlmacen.Visible = False
						Me.cboMarbetes.Enabled = False
						Session("Ubicacion") = ""
					ElseIf Me.lblOrigen.Text = "AAM" AndAlso Not Session("MarbeteAgregado") Is Nothing Then 'Si le da click en Administrar Marbetes con Radio Marbetes
						'Te Muestra el almacen del marbete
						Me.cboMarbetes.SelectedValue = Session("MarbeteAgregado")
						Me.cboAlmacenes.Visible = False
						If Session("Sociedad").ToString = "3" Then
							Me.lblAlmacen.Visible = False
						Else
							Me.lblAlmacen.Visible = True
						End If
						Me.txtUbicacion.Visible = False     ' GISP 2017.11.29
						Me.lblUbicacion.Visible = False     ' GISP 2017.11.29
						CargarAlmacen()
					ElseIf Me.lblOrigen.Text = "AAM" AndAlso Session("MarbeteAgregado") Is Nothing Then 'Si le da click en Administrar Marbetes con Radio EAN
						'Te Muestra el almacen del marbete
						Me.cboMarbetes.SelectedIndex = -1
						Me.cboAlmacenes.Visible = False
						If Session("Sociedad").ToString = "3" Then
							Me.lblAlmacen.Visible = False
						Else
							Me.lblAlmacen.Visible = True
						End If
						Me.txtUbicacion.Visible = False     ' GISP 2017.11.29
						Me.lblUbicacion.Visible = False     ' GISP 2017.11.29
						CargarAlmacen()
					ElseIf Me.lblOrigen.Text = "" Then 'Si le da click en Agregar articulo a Marbete
						'Te Muestra el almacen para que lo selecciones
						Me.cboAlmacenes.Visible = False
						If Session("Sociedad").ToString = "3" Then
							Me.lblAlmacen.Visible = False
						Else
							Me.lblAlmacen.Visible = True
						End If
						Me.txtUbicacion.Visible = False     ' GISP 2017.11.29
						Me.lblUbicacion.Visible = False     ' GISP 2017.11.29
						CargarAlmacen()
					End If
				Catch ex As Exception
					Session("MarbeteAgregado") = Nothing
					Session("IdMarbeteAgregado") = Nothing
				End Try
				LlenarGrid()
				CargarAlmacenes()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))

			ds = DatosSQL.funcioncp("Marbetes_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdMarbete") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				If Me.lblOrigen.Text = "AM" AndAlso Session("IdMarbeteAgregado") IsNot Nothing Then
					Dim DrDet() As DataRow
					DrDet = ds.Tables(0).Select("IdMarbete = '" & CStr(Session("IdMarbeteAgregado")) & "'")
					If DrDet Is Nothing OrElse DrDet.Length = 0 Then
						Dim dr2 As DataRow
						dr2 = ds.Tables(0).NewRow
						dr2("IdMarbete") = Session("IdMarbeteAgregado")
						ds.Tables(0).Rows.InsertAt(dr2, 1)
					End If
				End If

				Me.cboMarbetes.DataSource = ds.Tables(0)
				Me.cboMarbetes.DataValueField = "IdMarbete"
				Me.cboMarbetes.DataTextField = "IdMarbete"
				Me.cboMarbetes.DataBind()

			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub LlenarGrid()
		Try
			Dim Tabla As New DataTable
			If Tabla.Columns.Count = 0 Then
				Tabla.Columns.Add("ID")
			End If
			For x As Integer = 0 To 19
				Dim Ren As DataRow
				Ren = Tabla.NewRow
				Ren.Item(0) = x + 1
				Tabla.Rows.InsertAt(Ren, x)
			Next
			Me.RadGrid1.DataSource = Tabla
			If Session("Sociedad").ToString = "3" Then
				Me.RadGrid1.Columns(1).HeaderText = "Código material"
			Else
				Me.RadGrid1.Columns(1).HeaderText = "EAN"
			End If
			RadGrid1.DataBind()
			For x As Integer = 0 To Me.RadGrid1.Rows.Count - 1
				Me.RadGrid1.Rows(x).Cells(2).ForeColor = Drawing.Color.Black
			Next



		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
		If Validar() = True Then
			Insertar()
		End If
	End Sub

	Function Validar() As Boolean
		Dim ContadorTotal As Integer = 0
		Dim ContadorOK As Integer = 0
		Try
			'MARBETE
			If Me.cboMarbetes.SelectedValue = "[Seleccione]" Then
				CMensajes.MostrarMensaje("Seleccione el marbete.", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
			ElseIf Me.lblOrigen.Text = "AM" AndAlso Not Session("IdMarbeteAgregado") Is Nothing Then
				If Me.cboAlmacenes.SelectedValue = "Seleccionar Almacen" AndAlso Session("Sociedad").ToString <> "3" Then
					CMensajes.MostrarMensaje("Seleccione un Almacen.", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
				ElseIf Me.lblUbicacion.Visible = True And Me.txtUbicacion.Visible = True And Me.txtUbicacion.Text = "" Then
					CMensajes.MostrarMensaje(AppSettings("AdvertenciaUbicacion").ToString, CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
				Else
					GoTo continuar
				End If
			ElseIf Me.lblUbicacion.Visible = True And Me.txtUbicacion.Visible = True And Me.txtUbicacion.Text = "" Then
				CMensajes.MostrarMensaje(AppSettings("AdvertenciaUbicacion").ToString, CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
			Else
continuar:
				'Se recorre el datagrid en busca de errores
				Dim linea(20) As String
				For x As Integer = 1 To 20
					Dim EAN As TextBox
					Dim Descripcion As TextBox
					Dim Cantidad As TextBox

					EAN = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox1"), TextBox)
					Descripcion = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox2"), TextBox)
					Cantidad = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox3"), TextBox)
					'Si la caja de EAN contiene datos procedemos a las validaciones
					If EAN.Text.Length > 0 Then
						ContadorTotal = ContadorTotal + 1
						'Validamos que el campo cantidad sea proporcionado
						If Cantidad.Text.Length > 0 Then
							'Validamos que el campo cantidad sea un valor numérico no negativo
							If IsNumeric(Cantidad.Text.Trim) = False OrElse CDbl(Cantidad.Text.Trim) < 0 Then
								CMensajes.Show("Favor de verificar el campo cantidad de la línea " & CStr(x))
								Me.RadGrid1.Rows(x - 1).Cells(2).ForeColor = Drawing.Color.Red
								Return False
								Exit Function
							End If
							'El resultado de las validaciones fue correcto
							Me.RadGrid1.Rows(x - 1).Cells(2).ForeColor = Drawing.Color.Black
							ContadorOK = ContadorOK + 1
						Else
							linea(x) = x
							Me.RadGrid1.Rows(x - 1).Cells(2).ForeColor = Drawing.Color.Red
						End If
					End If
				Next

				If linea.Length > 0 Then
					Dim err As String = Nothing
					For j As Integer = 1 To 20
						If err = Nothing Then
							err = linea(j)
						Else
							err = err & ", " & linea(j)
						End If
					Next
					CMensajes.MostrarMensaje("Favor de proporcionar la cantidad en la(s) siguiente(s) línea(s): " & err, CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
				End If
			End If
			If ContadorTotal > ContadorOK OrElse ContadorTotal = 0 OrElse ContadorOK = 0 Then
				Return False
			ElseIf ContadorTotal = ContadorOK Then
				Return True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Insertar()
		Try
			Me.btnGuardar.Enabled = False
			Me.btnGuardar.Text = "Insertando artículos..."
			Dim ds As New DataSet
			Dim err As Integer = 0

			'Se procede a la inserción de datos en BD
			Dim par(7) As SqlParameter      'Línea actualizada GISP 2017.10.23
			For x As Integer = 1 To 20
				Dim EAN As TextBox
				Dim Descripcion As TextBox
				Dim Cantidad As TextBox
				EAN = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox1"), TextBox)
				Descripcion = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox2"), TextBox)
				Cantidad = CType(Me.RadGrid1.Rows(x - 1).FindControl("TextBox3"), TextBox)
				Descripcion.Text = ObtenerDescripcion(EAN)
				If EAN.Text.Length > 0 Then
					par(0) = New SqlParameter("@Accion", 1) 'Agregar
					par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
					par(2) = New SqlParameter("@IdMarbete", Me.cboMarbetes.SelectedValue)
					par(3) = New SqlParameter("@EAN", EAN.Text.Trim)
					par(4) = New SqlParameter("@Conteo", CDbl(Cantidad.Text.Trim))
					par(5) = New SqlParameter("@IdUsuario", Session("IdUsuario"))

					'Si lo trajo del combo, si lo trajo del label
					If Me.lblOrigen.Text = "AM" AndAlso Not Session("IdMarbeteAgregado") Is Nothing Then
						If Session("Sociedad").ToString = "3" Then
							par(6) = New SqlParameter("@idAlmacen", "A001")
						Else
							par(6) = New SqlParameter("@idAlmacen", Me.cboAlmacenes.SelectedValue)
						End If
					ElseIf Me.lblOrigen.Text = "AAM" Then
						If Session("Sociedad").ToString = "3" Then
							par(6) = New SqlParameter("@idAlmacen", "A001")
						Else
							par(6) = New SqlParameter("@idAlmacen", Right(Me.lblAlmacen.Text.ToString, 4))
						End If
					ElseIf Me.lblOrigen.Text = "" Then
						If Session("Sociedad").ToString = "3" Then
							par(6) = New SqlParameter("@idAlmacen", "A001")
						Else
							par(6) = New SqlParameter("@idAlmacen", Right(Me.lblAlmacen.Text.ToString, 4))
						End If
					End If

					'par(7) = New SqlParameter("@Ubicacion", txtUbicacion.Text.Trim)     ' Línea agregada GISP 2017.10.23
					If Session("Cedis") = 1 Then     ' GISP 2017.12.12
						par(7) = New SqlParameter("@Ubicacion", txtUbicacion.Text.Trim)
					Else
						par(7) = New SqlParameter("@Ubicacion", Session("Ubicacion"))
					End If

					If Session("Perfil") = "P" Then
						ds = DatosSQL.funcioncp("Preconteos_Administracion", par)
						If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
							Dim status As String = ds.Tables(0).Rows(0)(0).ToString
							If status <> "1" Then
								err += 1
								Me.ErrInsertar.Value += err
							End If
						End If
					Else
						ds = DatosSQL.funcioncp("Conteos_Administracion", par)
						If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
							Dim status As String = ds.Tables(0).Rows(0)(0).ToString
							If status <> "1" Then
								err += 1
								Me.ErrInsertar.Value += err
							End If
						End If
					End If
				End If
			Next
			CMensajes.MostrarMensaje("Los artículos fueron agregados correctamente.", CMensajes.Tipo.GetExito, wucMessageAgregarArtMarbete)
			Session("MarbeteAgregado") = Me.cboMarbetes.SelectedValue
			Me.cboAlmacenes.Enabled = False
			LlenarGrid()

			If Request.QueryString("Refresh") = "S" Then
				Session("msjAgregarArtMarbete") = "Los artículos fueron agregados correctamente."
				Response.Redirect("AdmonArticuloMarbete.aspx?SID=" & Request.QueryString("SID"), False)
			End If

			Me.btnGuardar.Enabled = True
			Me.btnGuardar.Text = "Agregar artículos"
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function ObtenerDescripcion(ByVal EAN As TextBox) As String
		Try
			Dim Descripcion As String
			Dim ds As New DataSet
			Dim Par(1) As SqlParameter
			Par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			Par(1) = New SqlParameter("@EAN", EAN.Text.Trim)
			ds = DatosSQL.funcioncp("CatalogoGeneral_BuscarXEAN", Par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				If IsDBNull(ds.Tables(0).Rows(0).Item("OBS")) = False Then
					Descripcion = ds.Tables(0).Rows(0).Item("OBS")
				Else
					Descripcion = ""
				End If
			End If
			Return Descripcion
		Catch ex As Exception
			Return ""
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
		Try
			If Me.cboMarbetes.SelectedValue = "[Seleccione]" Then
				CMensajes.MostrarMensaje("Seleccione el Marbete.", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
			ElseIf Request.QueryString("Origen") = "AM" AndAlso Me.cboAlmacenes.SelectedValue = "Seleccionar Almacen" AndAlso Session("Sociedad").ToString <> "3" Then
				CMensajes.MostrarMensaje("Seleccione un Almacen.", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
			ElseIf Me.lblUbicacion.Visible = True And Session("Cedis") = 1 And Me.txtUbicacion.Text = "" Then
				CMensajes.MostrarMensaje(AppSettings("AdvertenciaUbicacion").ToString, CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
			Else
				Dim ArchivoSeleccionado As Boolean = RadUpload1.HasFile 'Me.RadUpload1.UploadedFiles.Count > 0
				If ArchivoSeleccionado = False Then
					CMensajes.MostrarMensaje("Debe seleccionar el archivo a cargar antes de continuar", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
				Else
					Me.btnAgregar.Enabled = False
					Me.btnAgregar.Text = "Subiendo archivo..."
					Dim Extension As String = ""
					Dim Archivo As String = Me.RadUpload1.FileName
					Dim IdTienda As String = Session("IdTienda")
					Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

					If Not Directory.Exists(DirectorioArchivos) Then
						Directory.CreateDirectory(DirectorioArchivos)
					End If

					If Me.RadioButtonList1.SelectedIndex = 0 Then
						If Archivo.ToUpper.EndsWith(".XLS") = False Then
							CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .xls", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
						ElseIf Archivo.StartsWith(IdTienda) = False Then
							CMensajes.MostrarMensaje("El archivo seleccionado no corresponde al de la tienda, favor de verificar", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
						Else
							DirectorioArchivos = DirectorioArchivos & Archivo
							Me.RadUpload1.SaveAs(DirectorioArchivos)

							Try
								Dim dt As DataTable
								If ExcelClass.ValidarExcel(DirectorioArchivos) Then
									dt = ExcelClass.LeerExcel(DirectorioArchivos)

									Dim EAN, Cantidad As String
									For x As Integer = 0 To dt.Rows.Count - 1
										If IsDBNull(dt.Rows(x).Item(0)) Or IsDBNull(dt.Rows(x).Item(1)) Then
											Continue For
										End If
										EAN = dt.Rows(x).Item(0)
										Cantidad = dt.Rows(x).Item(1)

										If Session("Sociedad").ToString = "3" Then
											If EAN.Trim <> "CMaterial" AndAlso Cantidad.Trim <> "Cantidad" Then
												Insertar(EAN.Trim, Cantidad.Trim)
											End If
										Else
											If EAN.Trim <> "EAN" AndAlso Cantidad.Trim <> "Cantidad" Then
												Insertar(EAN.Trim, Cantidad.Trim)
											End If
										End If
									Next
									CMensajes.MostrarMensaje("Los artículos fueron grabados correctamente", CMensajes.Tipo.GetExito, wucMessageAgregarArtMarbete)
									Me.cboAlmacenes.Enabled = False
									Session("MarbeteAgregado") = Me.cboMarbetes.SelectedValue
									LlenarGrid()
								Else
									CMensajes.MostrarMensaje("No es un archivo válido.", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
								End If
							Catch ex As Exception
								Throw New Exception(ex.Message, ex.InnerException)
							End Try
						End If
					ElseIf Me.RadioButtonList1.SelectedIndex = 1 Then
						If Archivo.EndsWith(".txt") = False AndAlso Archivo.EndsWith(".TXT") = False Then
							CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .txt", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
						ElseIf Archivo.StartsWith(IdTienda) = False Then
							CMensajes.MostrarMensaje("El archivo seleccionado no corresponde al de la tienda, favor de verificar", CMensajes.Tipo.GetError, wucMessageAgregarArtMarbete)
						Else
							DirectorioArchivos = DirectorioArchivos & Archivo
							Me.RadUpload1.SaveAs(DirectorioArchivos)

							Dim dt As New DataTable
							dt = LeerTxt.LeerTxtADT(DirectorioArchivos, "|")
							dt = LeerTxt.ValidarDT(dt)

							For Each row In dt.Rows
								Insertar(row(0).Trim, row(1).Trim)
							Next

							If Me.ErrInsertar.Value > 0 Then
								CMensajes.MostrarMensaje("No se pudieron agregar los artículos.", CMensajes.Tipo.GetExito, wucMessageAgregarArtMarbete)
							Else
								CMensajes.MostrarMensaje("Los artículos fueron grabados correctamente.", CMensajes.Tipo.GetExito, wucMessageAgregarArtMarbete)
							End If
							Me.cboAlmacenes.Enabled = False
							Session("MarbeteAgregado") = Me.cboMarbetes.SelectedValue
							LlenarGrid()
						End If
					End If
					Me.btnAgregar.Enabled = True
					Me.btnAgregar.Text = "Cargar archivo"
				End If
			End If
		Catch ex As Exception
			Throw New Exception("Tienda: " & Session("IdTienda") & " - " & ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Insertar(ByVal Ean As String, ByVal Cantidad As String)
		Try
			Dim ds As New DataSet
			Dim err As Integer = 0

			'Se procede a la inserción de datos en BD
			'Dim par(6) As SqlParameter      'Línea original comentada por GISP 2017.12.06
			Dim par(7) As SqlParameter      'GISP 2017.12.06
			par(0) = New SqlParameter("@Accion", 1) 'Agregar
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@IdMarbete", Me.cboMarbetes.SelectedValue)
			par(3) = New SqlParameter("@EAN", Ean)
			par(4) = New SqlParameter("@Conteo", CDbl(Cantidad))
			par(5) = New SqlParameter("@IdUsuario", Session("IdUsuario"))

			'Si lo trajo del combo, si lo trajo del label
			If Me.lblOrigen.Text = "AM" AndAlso Not Session("IdMarbeteAgregado") Is Nothing Then
				If Session("Sociedad").ToString = "3" Then
					par(6) = New SqlParameter("@idAlmacen", "A001")
				Else
					par(6) = New SqlParameter("@idAlmacen", Me.cboAlmacenes.SelectedValue)
				End If
			ElseIf Me.lblOrigen.Text = "AAM" Then
				If Session("Sociedad").ToString = "3" Then
					par(6) = New SqlParameter("@idAlmacen", "A001")
				Else
					par(6) = New SqlParameter("@idAlmacen", Right(Me.lblAlmacen.Text.ToString, 4))
				End If
			ElseIf Me.lblOrigen.Text = "" Then
				If Session("Sociedad").ToString = "3" Then
					par(6) = New SqlParameter("@idAlmacen", "A001")
				Else
					par(6) = New SqlParameter("@idAlmacen", Right(Me.lblAlmacen.Text.ToString, 4))
				End If
			End If
			'par(7) = New SqlParameter("@Ubicacion", txtUbicacion.Text.Trim)     ' Línea agregada GISP 2017.12.06
			If Me.lblUbicacion.Visible = False And Me.txtUbicacion.Visible = False Then     ' GISP 2017.12.12
				par(7) = New SqlParameter("@Ubicacion", Session("Ubicacion"))
			Else
				par(7) = New SqlParameter("@Ubicacion", txtUbicacion.Text.Trim)
			End If

			If Session("Perfil") = "P" Then
				ds = DatosSQL.funcioncp("Preconteos_Administracion", par)
				If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					Dim status As String = ds.Tables(0).Rows(0)(0).ToString
					If status <> "1" Then
						err += 1
						Me.ErrInsertar.Value += err
					End If
				End If
			Else
				ds = DatosSQL.funcioncp("Conteos_Administracion", par)
				If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					Dim status As String = ds.Tables(0).Rows(0)(0).ToString
					If status <> "1" Then
						err += 1
						Me.ErrInsertar.Value += err
					End If
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarAlmacenes()
		Try
			Dim dtAlmacenes As New DataTable
			dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

			Dim Row As DataRow = dtAlmacenes.NewRow
			Row("idAlmacen") = "0"
			Row("idAlmacen") = "Seleccionar Almacen"
			dtAlmacenes.Rows.InsertAt(Row, 0)
			dtAlmacenes.AcceptChanges()

			Me.cboAlmacenes.DataSource = dtAlmacenes
			Me.cboAlmacenes.DataValueField = "idAlmacen"
			Me.cboAlmacenes.DataTextField = "idAlmacen"
			Me.cboAlmacenes.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarAlmacen()
		Try
			Dim dsAlmacenes As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Perfil", Left(Session("Perfil"), 1))
			par(1) = New SqlParameter("@idMarbete", Me.cboMarbetes.SelectedValue)
			par(2) = New SqlParameter("@IdTienda", Session("IdTienda"))

			dsAlmacenes = DatosSQL.funcioncp("Buscar_Almacen_Marbete", par)

			If dsAlmacenes.Tables(0).Rows.Count = 0 Then
				Me.lblAlmacen.Text = ""
				Session("Ubicacion") = ""   ' GISP 2017.12.12
			Else
				Me.lblAlmacen.Text = "Almacén: " & dsAlmacenes.Tables(0).Rows(0).Item(0)
				If Session("Sociedad").ToString <> "3" Then
					Session("Ubicacion") = dsAlmacenes.Tables(0).Rows(0).Item(1)    ' GISP 2017.12.12
				End If
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub cboMarbetes_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboMarbetes.SelectedIndexChanged
		If Me.lblOrigen.Text = "AAM" AndAlso Not Session("MarbeteAgregado") Is Nothing Then 'Si le da click en Administrar Marbetes
			CargarAlmacen()
		ElseIf Me.lblOrigen.Text = "" Then 'Si le da click en Agregar articulo a Marbete
			CargarAlmacen()
		ElseIf Me.lblOrigen.Text = "AAM" AndAlso Session("MarbeteAgregado") Is Nothing Then 'Si le da click en Administrar Marbetes con Radio EAN
			CargarAlmacen()
		End If
	End Sub
End Class

