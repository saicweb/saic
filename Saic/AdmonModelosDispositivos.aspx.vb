﻿Imports System.Data.SqlClient
Public Class AdmonModelosDispositivos
    Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As Button
    Protected WithEvents btnConfirmNO As Button

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If

				ConsultarModelo()
			Catch ex As Exception
				Me.btnGuardar.Visible = False
				Me.btnModificar.Visible = False
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
        End If
    End Sub

    Sub Modelo_Datos()
        Try
            Dim ds As New DataSet
            Dim PAR(0) As SqlParameter
            PAR(0) = New SqlParameter("@Modelo", Me.txtModelo.Text.Trim)
            ds = DatosSQL.funcioncp("ModelosDispositivo_Buscar", PAR)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'IdArea,NombreArea
                If IsDBNull(ds.Tables(0).Rows(0).Item("MODELO")) = False Then
                    Me.txtModelo.Text = ds.Tables(0).Rows(0).Item("MODELO")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("RUTA")) = False Then
                    Me.txtruta.Text = ds.Tables(0).Rows(0).Item("RUTA")
                End If
            Else
                Me.btnModificar.Visible = False
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Modelo_Administracion(1)
    End Sub
    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Modelo_Administracion(2)
    End Sub


#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

    Sub cargaWinConfirm()
        winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
        Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
        txtTitutlo.Text = "Confirmar"
        Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
        txtCuerpo.Text = ""


        btnConfirmNO = winConfirm1.FindControl("BtnNO")
        btnConfirmNO.Text = "No"
        AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

        btnConfirmSI = winConfirm1.FindControl("btnSI")
        btnConfirmSI.Text = "Sí"
        AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

        phConfirm.Controls.Add(winConfirm1)
    End Sub
#End Region

    Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
        Modelo_Administracion(3)
    End Sub
    Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.winConfirm1.OcultaDialogo()
        Response.Redirect("AdmonModelosDispositivos.aspx?SID=" & Session("SID"), False)
    End Sub

    Sub Modelo_Administracion(ByVal Accion As Integer)
		Try
			Dim ds As New DataSet
			'IdArea,NombreArea
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Accion", Accion)
			par(1) = New SqlParameter("@Modelo", Me.txtModelo.Text.ToUpper.Trim)
			par(2) = New SqlParameter("@Ruta", Me.txtruta.Text.Trim)

			ds = DatosSQL.funcioncp("ModelosDispositivo_Administracion", par)

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0), CMensajes.Tipo.GetError, wucMessageAdmonModelosDispositivos)
			Else
				ConsultarModelo()
				btnGuardar.Visible = True
				btnModificar.Visible = False
				txtModelo.Enabled = True
				limpiarDatos()

				Select Case Accion
					Case 1 '--NUEVO
						'CMensajes.Show("Modelo Agregado exitosamente.")
						CMensajes.MostrarMensaje("Modelo Agregado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonModelosDispositivos)
					Case 2 '--MODIFICAR
						'CMensajes.Show("Modelo Modificado exitosamente.")
						CMensajes.MostrarMensaje("Modelo Modificado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonModelosDispositivos)
					Case 3 '--ELIMINAR
						'CMensajes.Show("Modelo Eliminado exitosamente.")
						CMensajes.MostrarMensaje("Modelo Eliminado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonModelosDispositivos)
				End Select
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
    Sub ConsultarModelo()
        Try
            'IdArea,NombreArea
            Dim ds As New DataSet
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@Modelo", "")

            ds = DatosSQL.funcioncp("ModelosDispositivo_Buscar", par)
            gvdispositivo.DataSource = ds
            gvdispositivo.DataBind()
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
    Sub limpiarDatos()
        txtModelo.Text = ""
        txtruta.Text = ""
    End Sub

    Protected Sub gvdispositivo_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvdispositivo.RowCommand
        Try
            If e.CommandName = "Modificar" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim id, descripcion As String
                id = gvdispositivo.Rows(index).Cells(0).Text
                descripcion = gvdispositivo.Rows(index).Cells(1).Text

                txtModelo.Text = id
                txtruta.Text = descripcion
                txtModelo.Enabled = False
                btnModificar.Visible = True
                btnGuardar.Visible = False
            ElseIf e.CommandName = "Eliminar" Then
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim id, descripcion As String
                id = gvdispositivo.Rows(index).Cells(0).Text
                descripcion = gvdispositivo.Rows(index).Cells(1).Text

                txtModelo.Text = id
                txtruta.Text = descripcion
                txtModelo.Enabled = False
                btnModificar.Visible = False
                btnGuardar.Visible = False
                Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
                lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar el modelo dispositivo?</div>"
                winConfirm1.MuestraDialogo()
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
End Class