﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_MarbetesFaltantes.aspx.vb" Inherits="Saic.RPT_MarbetesFaltantes" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Reporte de marbetes faltantes" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageRPT_MarbetesFaltantes" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="40%" align="center" id="TblCabecero">
                    <tr>
                        <td>
                            <asp:Panel runat="server" Height="200px" Width="300px" ScrollBars="Auto" Visible="false" ID="PnlRotativo">
                                <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:TemplateField HeaderText="Todos" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:CheckBox id="Checkbox3" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" runat="server" Text="Todos&lt;br /&gt;" TextAlign="Left"></asp:CheckBox>
                                            </HeaderTemplate>
						                    <ItemTemplate>
                                                <asp:CheckBox id="Checkbox4" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
					                    </asp:TemplateField>
								        <asp:BoundField HeaderText="Ubicación" DataField="Ubicacion" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
                <tr>
            <td align="center">
                <table runat="server" width="90%">
                    <tr>
                        <td align="center"><br />
                            <div style="z-index:-1">
                                <cr:crystalreportviewer Id="RPT_Marbetes_Faltantes" runat="server"  HasCrystalLogo="False"
                                AutoDataBind="True" OnDataBinding="_reportViewer_DataBinding" Height="50px"  EnableParameterPrompt="false" EnableDatabaseLogonPrompt="false" ToolPanelWidth="200px" 
                                Width="500px" ToolPanelView="None" HasRefreshButton="True" ShowAllPageIds="True" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" HasDrilldownTabs="False"></cr:crystalreportviewer>  
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
