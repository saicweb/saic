﻿Imports System.Data.SqlClient

Public Class AdmonGrupos
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageAdmonSubFamilias.Visible = False

		If Session("Sociedad").ToString <> "3" Then
			CMensajes.Show("Sólo 'Restaurante' tiene permiso de acceso a esta sección.")
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
		End If

		Try
			Llenar_Grupo()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try

	End Sub

	Protected Sub lnkAgregar_Click(sender As Object, e As EventArgs) Handles lnkAgregar.Click
		Me.txtIdGrupo.Text = ""
		Me.txtNombreGrupo.Text = ""
		Me.lblTitulo.Text = "Agregar grupo nuevo"
		Me.TblAgregarGrupo.Visible = True
		Me.btnAgregar.Visible = True
		Me.btnCancelar.Visible = True
		Me.btnActualizar.Visible = False
		Me.btnBaja.Visible = False
		Me.txtIdGrupo.ReadOnly = False
		Me.TblGridAgregarGrupo.Visible = False
		Me.pnlGrupo.Visible = False
	End Sub

	Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
		Grupo_Administracion(1)
	End Sub

	Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
		Grupo_Administracion(2)
	End Sub

	Protected Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
		BorrarGrupo(Me.txtIdGrupo.Text.Trim)
	End Sub

	Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
		Me.txtIdGrupo.Text = ""
		Me.txtNombreGrupo.Text = ""
		Me.TblAgregarGrupo.Visible = False
		Me.TblGridAgregarGrupo.Visible = True
		Me.pnlGrupo.Visible = True
	End Sub

	''' <summary>
	''' Llena grid de grupo
	''' </summary>
	Private Sub Llenar_Grupo()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@Accion", 4)
			ds = DatosSQL.funcioncp("Grupo_Administracion", par)
			If Not ds Is Nothing Then
				Me.GridGrupos.DataSource = ds.Tables(0)
				Me.GridGrupos.DataBind()
			Else
				Me.GridGrupos.DataSource = Nothing
				Me.GridGrupos.DataBind()
				CMensajes.MostrarMensaje("No se encontró ningún grupo.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Elimina el grupo seleccionado
	''' </summary>
	''' <param name="id"></param>
	Sub BorrarGrupo(ByVal id As String)
		Try

			'@Accion as int,--1=Agregar 2=Modificar 3=Eliminar 
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@Accion", 3)
			par(1) = New SqlParameter("@IdGrupo", id)

			DatosSQL.funcioncp("Grupo_Administracion", par)

			Me.TblAgregarGrupo.Visible = False
			Me.TblGridAgregarGrupo.Visible = True
			Me.pnlGrupo.Visible = True

			Me.Llenar_Grupo()

			CMensajes.MostrarMensaje("Grupo " & id & " eliminado correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSubFamilias)
		Catch ex As SqlException
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena los controles con la información de grupo seleccionada
	''' </summary>
	''' <param name="idGrupo"></param>
	Sub Grupo_Datos(ByVal idGrupo As String)
		Try
			Dim ds As New DataSet
			Dim PAR(1) As SqlParameter
			PAR(0) = New SqlParameter("@Accion", 5)
			PAR(1) = New SqlParameter("@IdGrupo", idGrupo)
			ds = DatosSQL.funcioncp("Grupo_Administracion", PAR)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				If IsDBNull(ds.Tables(0).Rows(0).Item("IdGrupo")) = False Then
					Me.txtIdGrupo.Text = ds.Tables(0).Rows(0).Item("IdGrupo")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreGrupo")) = False Then
					Me.txtNombreGrupo.Text = ds.Tables(0).Rows(0).Item("NombreGrupo")
				End If
			Else
				Me.btnActualizar.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub Grupo_Administracion(ByVal Accion As Integer)
		Try
			If String.IsNullOrEmpty(Me.txtIdGrupo.Text) OrElse String.IsNullOrWhiteSpace(Me.txtIdGrupo.Text) Then
				CMensajes.MostrarMensaje("Ingrese el Id de grupo.", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
			ElseIf String.IsNullOrEmpty(Me.txtNombreGrupo.Text) OrElse String.IsNullOrWhiteSpace(Me.txtNombreGrupo.Text) Then
				CMensajes.MostrarMensaje("Ingrese el nombre de grupo", CMensajes.Tipo.GetError, wucMessageAdmonSubFamilias)
			Else
				Dim par(2) As SqlParameter
				par(0) = New SqlParameter("@Accion", Accion)
				par(1) = New SqlParameter("@IdGrupo", Me.txtIdGrupo.Text.ToUpper.Trim)
				par(2) = New SqlParameter("@NombreGrupo", Me.txtNombreGrupo.Text.ToUpper.Trim)

				DatosSQL.funcioncp("Grupo_Administracion", par)

				Select Case Accion
					Case 1 'Nuevo
						Me.btnActualizar.Visible = False
						Me.btnAgregar.Visible = True
						Me.txtIdGrupo.ReadOnly = True
						Me.lblTitulo.Text = "Agregar grupo nuevo"
						CMensajes.MostrarMensaje("Grupo agregado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSubFamilias)
					Case 2 'Modificar
						Me.btnActualizar.Visible = True
						Me.btnAgregar.Visible = False
						Me.txtIdGrupo.ReadOnly = True
						Me.lblTitulo.Text = "Modificar datos de grupo"
						CMensajes.MostrarMensaje("Grupo modificado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSubFamilias)
				End Select

				Llenar_Grupo()

				Me.TblAgregarGrupo.Visible = False
				Me.TblGridAgregarGrupo.Visible = True
				Me.pnlGrupo.Visible = True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub GridGrupos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridGrupos.RowCommand
		Dim iRow As Integer
		iRow = Integer.Parse(IIf(e.CommandArgument.ToString = "", 0, e.CommandArgument.ToString))

		If e.CommandName = "IdGrupo" Then
			Dim IdGrupo As String = Me.GridGrupos.DataKeys(iRow)("IdGrupo")

			Me.lblTitulo.Text = "Modificar datos de grupo"
			Me.TblAgregarGrupo.Visible = True
			Me.btnActualizar.Visible = True
			Me.btnBaja.Visible = True
			Me.btnCancelar.Visible = True
			Me.btnAgregar.Visible = False
			Me.txtIdGrupo.ReadOnly = True
			Me.txtIdGrupo.Text = IdGrupo
			Me.Grupo_Datos(IdGrupo)
			Me.TblGridAgregarGrupo.Visible = False
			Me.pnlGrupo.Visible = False
		End If
	End Sub
End Class