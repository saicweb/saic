﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonArticuloMarbete.aspx.vb" Inherits="Saic.AdmonArticuloMarbete" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes"  class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Modificar/Eliminar artículo marbete" ID="lblTitulo" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucmessage ID="wucMessageAdmonArticuloMarbete" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="100%">
                    <tr>
                        <td align="center">
                            <table runat="server">
                                <tr>
                                    <td align="center">
                                        <asp:Label runat="server" Text="Opción de búsqueda:" Width="150px" ID="Label1"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:radiobuttonlist id="rbtnOpcionBuscar" runat="server" Width="85px"  AutoPostBack="true" RepeatDirection="Vertical" CssClass="radio inline" TextAlign="Right">
							                <asp:ListItem Value="1" Selected="true">Marbete</asp:ListItem>
							                <asp:ListItem Value="2">EAN</asp:ListItem>
						                </asp:radiobuttonlist>
                                        <asp:radiobuttonlist id="rbtnOpcionBuscarCds"  Visible="false" runat="server" Width="85px"  AutoPostBack="true" RepeatDirection="Vertical" CssClass="radio inline" TextAlign="Right">
							                <asp:ListItem Value="1">Marbete</asp:ListItem>
							                <asp:ListItem Value="2">EAN</asp:ListItem>
                                            <asp:ListItem Value="3" Selected="true">Ubicación</asp:ListItem>
						                </asp:radiobuttonlist>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table runat="server" align="center">
                                <tr>
                                    <td align="center">
                                        <asp:Label runat="server" Text="Valor a buscar:" Width="150px" ID="Label2"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:TextBox ID="txtBuscar" Visible="false" runat="server" EnableViewState="False" Width="112px"></asp:TextBox> 
                                        <asp:DropDownList id="cboMarbetes" runat="server" AutoPostBack="true" Width="190px" MarkFirstMatch="true" AppendDataBoundItems="true" CssClass="dropdown"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:Label runat="server" Text=""  ID="lblAlmacen" font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center">
                            <asp:Button runat="server" Text="Buscar" ID="btnBuscar" CssClass="btn"  Visible="false"/>
                        </td>
                        <td align="center">
                            <asp:Button runat="server" Text="Agregar" ID="btnAgregar" CssClass="btn" />
                        </td>
                        <td>
                            <table runat="server" align="center">
                                <tr>
                                    <td align="center">
                                        <span><asp:Button runat="server" Text="Aplicar cambios" ID="btnModificar" CssClass="btn"/></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"><br />
                                        <span><asp:Button runat="server" Text="Agregar marbete" ID="btnagregar_marbete" CssClass="btn" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="center"><br />
                            <asp:Panel runat="server" Width="100%" Height="350px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" SkinID="Grid4">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="Usuario" DataField="IdUsuario" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundField HeaderText="Marbete" DataField="IdMarbete" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundField HeaderText="Ubicación" DataField="Ubicacion" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundField HeaderText="EAN" DataField="EAN" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="115px" ItemStyle-Width="115px" />
                                        <asp:BoundField HeaderText="Descripci&#243;n" DataField="OBS" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="230px" ItemStyle-Width="230px" />
                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" >
                                            <ItemTemplate>
                                                <asp:TextBox id="txtCantidad" runat="server" Width="89px" font-Size="X-Small" Text='<%# DataBinder.Eval(Container, "DataItem.Cantidad") %>' ></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Departamento" DataField="NOM_DEPTO" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="150px" ItemStyle-Width="150px" />
                                        <asp:BoundField HeaderText="Bultos" DataField="Bultos" ItemStyle-HorizontalAlign="center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="95px" ItemStyle-Width="95px" />
                                        <asp:TemplateField HeaderText="Acci&#243;n" HeaderStyle-Width="65px" ItemStyle-Width="65px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkmodificar" runat="server" CommandName="Modificar" Text="Modificar" CommandArgument="<%#Container.DataItemIndex%>"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Width="95px" ItemStyle-Width="95px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:Label ID="lbltitulochk" runat="server" Text="Marcar p/Eliminar&lt;br /&gt;"></asp:Label>
                                                <asp:CheckBox ID="chkheader"  runat="server" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" />
                                            </HeaderTemplate>                     
                                            <ItemTemplate>
                                                <asp:CheckBox id="CheckBox5" runat="server"  Checked='<%# DataBinder.Eval(Container, "DataItem.BultoV") %>'  onclick="javascript:CheckOne('grid',7);" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="IdPreconteos" DataField="IdPreconteos" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="95px" ItemStyle-Width="95px" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="BultoV" DataField="BultoV" HeaderStyle-Width="95px" ItemStyle-Width="95px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
