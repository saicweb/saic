﻿Public Class evento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then

			Try
				If Session("PopUpError") = True Then
					Session("PopUpError") = False
					Me.cmdSalir.Visible = False
				Else
					cmdSalir.Visible = True
				End If

				Dim mensajecorreo As String = ""
				Dim str() As String

				If Session("Err") IsNot Nothing AndAlso Session("Err") <> "" Then
					str = CStr(Session("Err")).Split("|")

					If str(0).Contains("NombreReporte") Then
						Dim var() As String = str(0).Split("&")
						Dim nomReporte As String = Nothing

						For i As Integer = 0 To var.Length - 1
							If var(i).Contains("NombreReporte") Then
								nomReporte = var(i).Substring(14, var(i).Length - 14)
								Dim encripta As New Encryption
								str(0).Replace(var(i), encripta.decryptString(nomReporte, False))
							End If
						Next
					End If

					mensajecorreo = String.Format("Page: {0}<br />Message: {1}<br />InnerException: {2}<br />StackTrace: {3}", str(0), str(1), str(2), str(3))

					Dim M As New Email
					M.enviaEmail(ConfigurationManager.AppSettings("CorreoError"), mensajecorreo, "Error SAIC")

				End If
			Catch ex As Exception

			Finally
				Session("Err") = ""
				Session("Value") = Nothing
			End Try
		End If
	End Sub

    Protected Sub cmdSalir_Click(sender As Object, e As EventArgs) Handles cmdSalir.Click
		Response.Redirect("~/Default.aspx")
	End Sub
End Class