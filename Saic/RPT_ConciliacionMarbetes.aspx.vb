﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_ConciliacionMarbetes
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageRPT_ConciliacionMarbetes.Visible = False

		If Not Page.IsPostBack Then
			Session.Remove("RPT_Conciliacion_Marbetes")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim tipo As String = Me.cboReporte.SelectedValue
			CatalogoReportes_CRV13(tipo)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Conciliacion_Marbetes.DataBinding
		If Session("RPT_Conciliacion_Marbetes") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Conciliacion_Marbetes.ReportSource = rpt
				Session("RPT_Conciliacion_Marbetes") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_Conciliacion_Marbetes.ReportSource = Session("RPT_Conciliacion_Marbetes")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Tipo As String)
		Dim nomReporte As String = Nothing
		If Tipo = "1" Then
			nomReporte = "MARBETES EN CONTEO 1 Y NO EN CONTEO 2"
		Else
			nomReporte = "MARBETES EN CONTEO 2 Y NO EN CONTEO 1"
		End If

		ds = RPT_ConciliacionMarbetes(Tipo)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_ConciliacionMarbetes))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = nomReporte
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Conciliacion_Marbetes.ReportSource = rpt
			RPT_Conciliacion_Marbetes.DataBind()
		Else
			CMensajes.MostrarMensaje("No existen marbetes por conciliar.", CMensajes.Tipo.GetInformativo, wucMessageRPT_ConciliacionMarbetes)
		End If
	End Sub

	Public Function RPT_ConciliacionMarbetes(ByVal Tipo As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Tipo", Tipo)
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@Perfil", Session("Perfil"))
			ds = DatosSQL.funcioncp("RPT_MarbetesConciliacion", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(2) As SqlParameter
				par2(0) = New SqlParameter("@Tipo", Tipo)
				par2(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par2(2) = New SqlParameter("@Perfil", Session("Perfil"))

				dsExcel = DatosSQL.funcioncp("RPT_MarbetesConciliacionExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					If Tipo = "1" Then
						Reportes.REPEXCEL(dsExcel, "Marbetes en conteo 1 y no en conteo 2", "MarbetesEnConteo1NoEnConteo2_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
					Else
						Reportes.REPEXCEL(dsExcel, "Marbetes en conteo 2 y no en  conteo 1", "MarbetesEnConteo2NoEnConteo1_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
					End If
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class