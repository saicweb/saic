﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CargaProvExterno.aspx.vb" Inherits="Saic.CargaProvExterno" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Procesos especiales" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Carga archivo de proveedor" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCargaProvExterno" runat="server" /><br />
            </td>
        </tr>
        <tr align="center">
            <td>
                <table runat="server" width="50%">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="uplXML" runat="server">
                                <ContentTemplate>
                                    <table runat="server" width="100%" align="center">
                                        <tr>
                                            <td align="center">
                                                <asp:FileUpload ID="RadUpload1" runat="server" CssClass="upload"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2"><br />
                                                <asp:Button ID="btncargar" runat="server" Text="Subir catálogo"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btncargar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
