﻿Imports System.Data.SqlClient

Public Class LocalizacionEAN
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageLocalizacionEAN.Visible = False

		If Session("Sociedad").ToString = "3" Then
			lblEAN_Material.Text = "Material: "
			lblAlmacen.Visible = False
			cboAlmacen.Visible = False
		End If

		If Not Page.IsPostBack Then
			If Session("Sociedad").ToString <> "3" Then Almacen_Buscar()
			Me.grid.DataSource = Nothing
			Me.grid.DataBind()
		End If
	End Sub

	Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
		Try
			Me.txtdescripcion.Text = ""

			If Me.txtBuscar.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese el EAN a buscar.", CMensajes.Tipo.GetError, wucMessageLocalizacionEAN)
			ElseIf Me.cboAlmacen.SelectedValue = "[Seleccione]" Then
				CMensajes.MostrarMensaje("Ingrese el almacén a buscar.", CMensajes.Tipo.GetError, wucMessageLocalizacionEAN)
			Else
				Dim par(3) As SqlParameter
				Dim DS As New DataSet
				par(0) = New SqlParameter("@Tipo", Session("Perfil"))
				par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(2) = New SqlParameter("@EAN", Me.txtBuscar.Text.Trim)
				par(3) = New SqlParameter("@Almacen", Me.cboAlmacen.SelectedValue)

				DS = DatosSQL.funcioncp("LocalizacionEAN", par)

				If Not DS Is Nothing AndAlso DS.Tables.Count > 0 Then
					Me.txtdescripcion.Text = DS.Tables(0).Rows(0).Item("OBS")

					If DS.Tables(1).Rows.Count = 0 Then
						DS.Tables(1).Rows.Add("Ningún artículo encontrado.")
					End If

					Me.grid.DataSource = DS.Tables(1)
					Me.grid.DataBind()

					If Me.txtdescripcion.Text.Trim.Length = 0 Then
						CMensajes.MostrarMensaje("Artículo no catalogado: " & Me.txtBuscar.Text, CMensajes.Tipo.GetInformativo, wucMessageLocalizacionEAN)
					End If
				Else
					Me.grid.DataSource = Nothing
					Me.grid.DataBind()
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@validar", 1)
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Dim Row As DataRow = DS.Tables(0).NewRow
				Row("idalmacen") = "1"
				Row("almacen") = "Todos los almacenes"
				DS.Tables(0).Rows.InsertAt(Row, DS.Tables(0).Rows.Count)
				DS.Tables(0).AcceptChanges()

				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub grid_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grid.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			grid.HeaderRow.Cells(3).Visible = False
			e.Row.Cells(3).Visible = False
		End If
	End Sub
End Class