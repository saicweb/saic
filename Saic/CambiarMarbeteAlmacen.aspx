﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CambiarMarbeteAlmacen.aspx.vb" Inherits="Saic.CambiarMarbeteAlmacen" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
    <script type="text/javascript">
    		function CopyCheckStateByColumn(HeaderCheckBox, gridViewName) {
    		    var columnIndex = HeaderCheckBox.parentElement.cellIndex;
    		    var newState = HeaderCheckBox.checked;
    		    ChangeChecksByColumn(gridViewName, newState, columnIndex);
    		}
    		function ChangeChecksByColumn(gridViewName, newState, columnIndex) {
    		    var tabla = document.getElementById(gridViewName);
    		    var columnas = tabla.cells.length / tabla.rows.length;
    		    celdas = tabla.cells;
    		    for (i = columnas + columnIndex; i < celdas.length; i += columnas) {
    		        if (celdas[i].firstChild.type == "checkbox"
				&& celdas[i].firstChild.checked != newState
    		        /* && agregar aquí otras condiciones */) {
    		            celdas[i].firstChild.click();
    		        }
    		    }
    		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Cambio marbete almacén" ID="lblTitulo" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucmessage ID="wucMessageCambiarMarbAlmacen" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="20%">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacén actual: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboAlmacenActual" runat="server" AutoPostBack="true"  Width="140px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Panel runat="server" Height="200px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:TemplateField HeaderText="Todos" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:CheckBox id="Checkbox3" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" runat="server" Text="Todos&lt;br /&gt;" TextAlign="Left"></asp:CheckBox>
                                            </HeaderTemplate>
						                    <ItemTemplate>
                                                <asp:CheckBox id="Checkbox4" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
					                    </asp:TemplateField>
								        <asp:BoundField HeaderText="Marbete" DataField="idMarbete" ItemStyle-Width="120px" ControlStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td><br />
                            <asp:Label runat="server" Text="Almacén nuevo: " ></asp:Label>
                        </td>
                        <td><br />
                            <asp:DropDownList id="cboAlmacenNuevo" runat="server" AutoPostBack="true"  Width="140px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:Button runat="server" Text="Actualizar" ID="btnActualizar" CssClass="btn"/></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
