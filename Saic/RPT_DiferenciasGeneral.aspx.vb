﻿Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class RPT_DiferenciasGeneral
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject
	Dim txtAlmacen As TextObject
	Dim txtUnidadesPiso As TextObject
	Dim txtUnidadesBodega As TextObject
	Dim txtUnidadesTotal As TextObject
	Dim txtCostoPiso As TextObject
	Dim txtCostoBodega As TextObject
	Dim txtCostoTotal As TextObject
	Dim txtVentasPiso As TextObject
	Dim txtVentasBodega As TextObject
	Dim txtVentasTotal As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If
				If Session("Perfil") = "P" Then
					Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
				End If
				Almacen_Buscar()

				Session.Remove("RPT_DiferenciasGeneral")
			Catch ex As Exception
				Me.btnGenerarReporte.Visible = False
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim almacen As String = Me.cboAlmacen.SelectedValue
			CatalogoReportes_CRV13(almacen)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Diferencias_General.DataBinding
		If Session("RPT_DiferenciasGeneral") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Diferencias_General.ReportSource = rpt
				Session("RPT_DiferenciasGeneral") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_Diferencias_General.ReportSource = Session("RPT_DiferenciasGeneral")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = RPT_DiferenciasGeneral(Almacen)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_DiferenciasGeneral))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtUnidadesPiso = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtUnidadesPiso")
			txtUnidadesBodega = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtUnidadesBodega")
			txtUnidadesTotal = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtUnidadesTotal")
			txtCostoPiso = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtCostoPiso")
			txtCostoBodega = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtCostoBodega")
			txtCostoTotal = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtCostoTotal")
			txtVentasPiso = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtVentasPiso")
			txtVentasBodega = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtVentasBodega")
			txtVentasTotal = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtVentasTotal")

			txtNombreReporte.Text = "REPORTE DE DIFERENCIAS GENERAL"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			txtUnidadesPiso.Text = IIf(ds.Tables(1).Rows(0)("UnidadesPiso") Is DBNull.Value, "", ds.Tables(1).Rows(0)("UnidadesPiso"))
			txtUnidadesBodega.Text = IIf(ds.Tables(1).Rows(0)("UnidadesBodega") Is DBNull.Value, "", ds.Tables(1).Rows(0)("UnidadesBodega"))
			txtUnidadesTotal.Text = IIf(ds.Tables(1).Rows(0)("UnidadesTotal") Is DBNull.Value, "", ds.Tables(1).Rows(0)("UnidadesTotal"))
			txtCostoPiso.Text = IIf(ds.Tables(1).Rows(0)("CostoPiso") Is DBNull.Value, "", ds.Tables(1).Rows(0)("CostoPiso"))
			txtCostoBodega.Text = IIf(ds.Tables(1).Rows(0)("CostoBodega") Is DBNull.Value, "", ds.Tables(1).Rows(0)("CostoBodega"))
			txtCostoTotal.Text = IIf(ds.Tables(1).Rows(0)("CostoTotal") Is DBNull.Value, "", ds.Tables(1).Rows(0)("CostoTotal"))
			txtVentasPiso.Text = IIf(ds.Tables(1).Rows(0)("VentaPiso") Is DBNull.Value, "", ds.Tables(1).Rows(0)("VentaPiso"))
			txtVentasBodega.Text = IIf(ds.Tables(1).Rows(0)("VentaBodega") Is DBNull.Value, "", ds.Tables(1).Rows(0)("VentaBodega"))
			txtVentasTotal.Text = IIf(ds.Tables(1).Rows(0)("VentaTotal") Is DBNull.Value, "", ds.Tables(1).Rows(0)("VentaTotal"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Diferencias_General.ReportSource = rpt
			RPT_Diferencias_General.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageDiferenciasGeneral)
		End If
	End Sub

	Public Function RPT_DiferenciasGeneral(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Almacen", Almacen)
			ds = DatosSQL.funcioncp("RPT_DiferenciasGeneral", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(1) As SqlParameter
				par2(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par2(1) = New SqlParameter("@Almacen", Almacen)

				dsExcel = DatosSQL.funcioncp("RPT_DiferenciasGeneralExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					Reportes.REPEXCEL(dsExcel, "Resumen diferencias general", "DiferenciasGeneral_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", "R")
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@validar", 2)
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

End Class