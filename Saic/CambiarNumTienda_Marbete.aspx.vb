﻿Imports System.Data.SqlClient

Public Class CambiarNumTienda_Marbete
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageCambiarNumTienda_Marbete.Visible = False

		If Not Page.IsPostBack Then
			Try
				If Session("TipoUsuario") = "A" Then 'ADMINISTRADOR
					Me.btnCambiar.Visible = True
					CatalogoMarbetes_Buscar()
				ElseIf Session("TipoUsuario") = "U" Then 'USUARIO
					CMensajes.Show("Sólo el administrador tiene permiso de acceso a esta sección.")
					Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	'''
	''' <summary>
	''' Metodo que llena el control para la confirmación de insertar una nueva diferencia
	''' </summary>
	''' <remarks></remarks>
	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			'Cambiar los marbetes seleccionados
			Dim Tienda As String = Session("IdTienda")
			For i As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				Dim marbete As String = ""
				chkRow = CType(Me.DataGrid1.Items(i).FindControl("CheckBox2"), CheckBox)
				If chkRow.Checked = True Then
					marbete = Me.DataGrid1.Items(i).Cells(0).Text
					Dim PAR(3) As SqlParameter
					PAR(0) = New SqlParameter("@IdTienda", Tienda)
					PAR(1) = New SqlParameter("@Perfil", Left(Session("Perfil"), 1))
					PAR(2) = New SqlParameter("@IdMarbete", marbete)
					If Session("Cedis") = 1 Then
						PAR(3) = New SqlParameter("@Marbete", Tienda.Substring(0) & marbete.Substring(4))
					Else
						PAR(3) = New SqlParameter("@Marbete", Tienda.Substring(1) & marbete.Substring(3))
					End If
					DatosSQL.procedimientocp("CambiarNumTienda_Marbete", PAR)
				End If
			Next
			CMensajes.MostrarMensaje("Ejecución exitosa. Proceso finalizado.", CMensajes.Tipo.GetExito, wucMessageCambiarNumTienda_Marbete)
			CatalogoMarbetes_Buscar()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageCambiarNumTienda_Marbete.Visible = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim IdTienda As String = Session("IdTienda")
			Dim NoTienda As String = IdTienda.Substring(1, 3)
			Dim IdMarbete As String = ""
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Left(Session("Perfil"), 1))
			ds = DatosSQL.funcioncp("Buscar_MarbetesTienda", par)

			If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim cont As Integer = 0
				Dim dsrows As Integer = ds.Tables(0).Rows.Count
				For x As Integer = 0 To dsrows - 1
					IdMarbete = ds.Tables(0).Rows(x).Item("IDMARBETE")
					IdMarbete = IdMarbete.Substring(0, 3)
					If IdMarbete = NoTienda.Trim Then
						ds.Tables(0).Rows(x).Delete()
						cont += 1
					End If
				Next
				ds.AcceptChanges()
				Me.DataGrid1.DataSource = ds.Tables(0)
				Me.DataGrid1.DataBind()
				Me.btnCambiar.Enabled = True

				If cont = dsrows Then
					btnCambiar.Enabled = False
					CMensajes.MostrarMensaje("Todos los marbetes coinciden con el código de la tienda.", CMensajes.Tipo.GetInformativo, wucMessageCambiarNumTienda_Marbete)
				End If
			Else
				Me.DataGrid1.DataSource = ds.Tables(0)
				Me.DataGrid1.DataBind()
				Me.btnCambiar.Enabled = False
				CMensajes.MostrarMensaje("No se encontraron marbetes.", CMensajes.Tipo.GetError, wucMessageCambiarNumTienda_Marbete)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chkHeader As CheckBox = CType(Me.DataGrid1.Controls(0).Controls(0).FindControl("CheckBox1"), CheckBox)
		If chkHeader.Checked = True Then
			For x As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DataGrid1.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = True
			Next
		Else
			For x As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DataGrid1.Items(x).FindControl("CheckBox2"), CheckBox)
				chkRow.Checked = False
			Next
		End If
	End Sub

	Private Sub btnCambiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCambiar.Click
		Try
			If Session("IdTienda") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If

			Dim seleccionados As Integer = 0
			For i As Integer = 0 To Me.DataGrid1.Items.Count - 1
				Dim chkRow As New CheckBox
				chkRow = CType(Me.DataGrid1.Items(i).FindControl("CheckBox2"), CheckBox)
				If chkRow.Checked = True Then
					seleccionados += 1
				End If
			Next
			If seleccionados = 0 Then
				CMensajes.MostrarMensaje("Ejecución no iniciada, no ha seleccionado ningún elemento.", CMensajes.Tipo.GetError, wucMessageCambiarNumTienda_Marbete)
			Else
				Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
				If Session("Cedis") = 1 Then
					lblTexto.Text = "<div align=""center"">Ésta acción modificará los primeros 4 dígitos del marbete. Favor de asegurarse que los artículos descargados correspondan a su sucursal. ¿Desea continuar?</div>"
				Else
					lblTexto.Text = "<div align=""center"">Ésta acción modificará los primeros 3 dígitos del marbete. Favor de asegurarse que los artículos descargados correspondan a su sucursal. ¿Desea continuar?</div>"
				End If
				winConfirm1.MuestraDialogo()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class