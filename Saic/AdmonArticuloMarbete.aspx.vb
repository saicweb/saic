﻿Imports System.Data
Imports System.Data.SqlClient
Public Class AdmonArticuloMarbete
	Inherits System.Web.UI.Page

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("Cedis") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If

			Me.wucMessageAdmonArticuloMarbete.Visible = False

			If Not Page.IsPostBack Then
				If Session("Cedis") = 1 Then
					Me.rbtnOpcionBuscarCds.Visible = True
					Me.rbtnOpcionBuscar.Visible = False
					Me.cboMarbetes.Visible = False
					Me.txtBuscar.Visible = True
					Me.btnBuscar.Visible = True
				End If

				Me.CatalogoMarbetes_Buscar()

				If Not Session("MarbeteAgregado") Is Nothing Then
					Try
						Me.cboMarbetes.SelectedValue = Session("MarbeteAgregado")
					Catch ex As Exception
						Session("MarbeteAgregado") = Nothing
					End Try
					Me.rbtnOpcionBuscar.SelectedValue = 1
					Preconteos_Conteos_Buscar()
				End If

				If Session("msjAgregarArtMarbete") IsNot Nothing OrElse Session("msjAgregarArtMarbete") <> "" Then
					CMensajes.MostrarMensaje(Session("msjAgregarArtMarbete"), CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
				End If

				CargarAlmacen()

				'Verifica que seleccione un almacen para poder agregarle articulos
				If Me.cboMarbetes.SelectedIndex = -1 Then
					Me.btnAgregar.Enabled = False
				Else
					Me.btnAgregar.Enabled = True
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
		Try
			Preconteos_Conteos_Buscar()
			If Session("Cedis") = 1 Then
				If Me.rbtnOpcionBuscarCds.SelectedValue = "1" Then 'MARBETE
					Me.txtBuscar.Visible = False
					Me.btnBuscar.Visible = False
					Me.cboMarbetes.Visible = True
					Me.lblAlmacen.Visible = True
				ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = "2" OrElse Me.rbtnOpcionBuscarCds.SelectedValue = "3" Then 'EAN
					Me.txtBuscar.Visible = True
					Me.txtBuscar.Visible = True
					Me.cboMarbetes.Visible = False
					Me.lblAlmacen.Visible = False
				End If
			Else
				If Me.rbtnOpcionBuscar.SelectedValue = "1" Then 'MARBETE
					Me.txtBuscar.Visible = False
					Me.btnBuscar.Visible = False
					Me.cboMarbetes.Visible = True
					Me.lblAlmacen.Visible = True
				ElseIf Me.rbtnOpcionBuscar.SelectedValue = "2" Then 'EAN
					Me.txtBuscar.Visible = True
					Me.btnBuscar.Visible = True
					Me.cboMarbetes.Visible = False
					Me.lblAlmacen.Visible = False
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Preconteos_Conteos_Buscar()
		Try
			Dim err As Integer = 0
			Dim err2 As Integer = 0

			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))


			If Session("Cedis") = 1 Then
				If Me.rbtnOpcionBuscarCds.SelectedValue = 1 Then
					If Me.cboMarbetes.SelectedValue = "[Seleccione]" Then
						err += 1
					End If
					par(1) = New SqlParameter("@IdMarbete", Me.cboMarbetes.SelectedValue)
					Session("MarbeteAgregado") = Me.cboMarbetes.SelectedValue
				ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = 2 Then
					If Me.txtBuscar.Text.Trim.Length = 0 Then
						err2 += 1
					End If
					par(1) = New SqlParameter("@EAN", Me.txtBuscar.Text.Trim)
					Session("MarbeteAgregado") = Nothing
				ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = 3 Then
					If Me.txtBuscar.Text.Trim.Length = 0 Then
						err2 += 1
					End If
					par(1) = New SqlParameter("@Ubicacion", Me.txtBuscar.Text.Trim)
					Session("MarbeteAgregado") = Nothing
				End If
			Else
				If Me.rbtnOpcionBuscar.SelectedValue = 1 Then
					If Me.cboMarbetes.SelectedValue = "[Seleccione]" Then
						CMensajes.Show("Seleccione el Marbete a buscar.")
						err += 1
					End If
					par(1) = New SqlParameter("@IdMarbete", Me.cboMarbetes.SelectedValue)
					Session("MarbeteAgregado") = Me.cboMarbetes.SelectedValue
				Else
					If Me.txtBuscar.Text.Trim.Length = 0 Then
						err2 += 1
					End If
					par(1) = New SqlParameter("@EAN", Me.txtBuscar.Text.Trim)
					Session("MarbeteAgregado") = Nothing
				End If
			End If

			If err > 0 OrElse err2 > 0 Then
				If err > 0 Then
					CMensajes.MostrarMensaje("Seleccione el Marbete a buscar.", CMensajes.Tipo.GetError, wucMessageAdmonArticuloMarbete)
				ElseIf err2 > 0 Then
					CMensajes.MostrarMensaje("Ingrese el valor a buscar.", CMensajes.Tipo.GetError, wucMessageAdmonArticuloMarbete)
				End If
			Else
				If Session("Perfil") = "P" Then
					ds = DatosSQL.funcioncp("Preconteos_Buscar", par)
				Else
					ds = DatosSQL.funcioncp("Conteos_Buscar", par)
				End If

				If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
					Me.grid.DataSource = ds.Tables(0)
					If Session("Sociedad").ToString = "3" Then
						Me.grid.Columns(3).HeaderText = "Código material"
					End If
					Me.grid.DataBind()
				End If

				If Session("Cedis") <> 1 Then
					Me.grid.Columns(2).Visible = False
					'Me.grid.Columns(7).Visible = False
					'Me.grid.Columns(8).Visible = False
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function Preconteos_Conteo_Administracion(ByVal Accion As Integer, ByVal IdMarbete As String, ByVal EAN As String, ByVal Conteo As Double, IdPreconteos As Integer) As Boolean
		Preconteos_Conteo_Administracion = False
		Try
			Dim ds As New DataSet
			Dim par(5) As SqlParameter
			par(0) = New SqlParameter("@Accion", Accion)
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@IdMarbete", IdMarbete)
			par(3) = New SqlParameter("@EAN", EAN)
			par(4) = New SqlParameter("@Conteo", Conteo)
			If Session("Perfil") = "P" Then
				par(5) = New SqlParameter("@IdPreconteos", IdPreconteos)
			Else
				par(5) = New SqlParameter("@IdConteos", IdPreconteos)
			End If

			If Session("Perfil") = "P" Then
				ds = DatosSQL.funcioncp("Preconteos_Administracion", par)
			Else
				ds = DatosSQL.funcioncp("Conteos_Administracion", par)
			End If

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Rows(0).Item("STATUS") = 1 Then
				Preconteos_Conteo_Administracion = True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
		Return Preconteos_Conteo_Administracion
	End Function

	Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
		Try
			Dim Eliminados As Integer = 0
			Dim Modificados As Integer = 0
			Dim cantidad As Integer = 0
			Dim nonum As Integer = 0
			Dim nonum2 As Integer=0

			For i As Integer = 0 To Me.grid.Rows.Count - 1
				Dim Cant As TextBox = CType(grid.Rows(i).Cells(5).Controls(1), TextBox)
				Dim CkEliminar As CheckBox = CType(grid.Rows(i).Cells(9).Controls(1), CheckBox)

				If CkEliminar.Checked = True Then
					Preconteos_Conteo_Administracion(3, grid.Rows(i).Cells(1).Text, grid.Rows(i).Cells(3).Text, 0, grid.Rows(i).Cells(10).Text)
					Eliminados = Eliminados + 1
				ElseIf Cant.Text.Trim = "" Then
					cantidad += 1
				ElseIf IsNumeric(Cant.Text.Trim) = False Then
					nonum += 1
				ElseIf CDbl(Cant.Text.Trim) < 0 Then
					nonum2 += 1
				ElseIf CDbl(Cant.Text.Trim) <> CDbl(grid.Rows(i).Cells(12).Text) Then
					Preconteos_Conteo_Administracion(2, grid.Rows(i).Cells(1).Text, grid.Rows(i).Cells(3).Text, Cant.Text, grid.Rows(i).Cells(10).Text)
					Modificados = Modificados + 1
				End If
			Next

			If cantidad > 0 OrElse nonum > 0 OrElse nonum2 > 0 Then
				If cantidad > 0 Then
					CMensajes.MostrarMensaje("Ingrese la cantidad antes de continuar.", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
				ElseIf nonum > 0 Then
					CMensajes.MostrarMensaje("La cantidad debe ser un dato númerico.", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
				ElseIf nonum2 > 0 Then
					CMensajes.MostrarMensaje("La cantidad debe ser mayor que cero.", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
				End If
			Else
				If Eliminados > 0 AndAlso Modificados > 0 Then
					CMensajes.MostrarMensaje(CStr(Eliminados) & " - Registro(s) Eliminado(s), " & CStr(Modificados) & " - Registro(s) Modificado(s).", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
					Preconteos_Conteos_Buscar()
				ElseIf Eliminados > 0 Then
					CMensajes.MostrarMensaje(CStr(Eliminados) & " - Registro(s) Eliminado(s).", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
					Preconteos_Conteos_Buscar()
				ElseIf Modificados > 0 Then
					CMensajes.MostrarMensaje(CStr(Modificados) & " - Registro(s) Modificado(s).", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
					Preconteos_Conteos_Buscar()
				End If

				If Session("Cedis") = 1 Then
					If Me.rbtnOpcionBuscarCds.SelectedValue = "1" Then 'MARBETE
						Me.txtBuscar.Visible = False
						Me.btnBuscar.Visible = False
						Me.cboMarbetes.Visible = True
					ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = "2" OrElse Me.rbtnOpcionBuscarCds.SelectedValue = "3" Then 'EAN
						Me.txtBuscar.Visible = True
						Me.txtBuscar.Visible = True
						Me.cboMarbetes.Visible = False
					End If
				Else
					If Me.rbtnOpcionBuscar.SelectedValue = "1" Then 'MARBETE
						Me.txtBuscar.Visible = False
						Me.btnBuscar.Visible = False
						Me.cboMarbetes.Visible = True
					ElseIf Me.rbtnOpcionBuscar.SelectedValue = "2" Then 'EAN
						Me.txtBuscar.Visible = True
						Me.btnBuscar.Visible = True
						Me.cboMarbetes.Visible = False
					End If
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub rbtnOpcionBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnOpcionBuscar.SelectedIndexChanged
		If Me.rbtnOpcionBuscar.SelectedValue = "1" Then 'MARBETE
			Me.txtBuscar.Visible = False
			Me.btnBuscar.Visible = False
			Me.cboMarbetes.Visible = True
			Me.lblAlmacen.Visible = True
			If Me.cboMarbetes.SelectedIndex = 0 Then
				Me.btnAgregar.Enabled = False
			Else
				Me.btnAgregar.Enabled = True
			End If
		ElseIf Me.rbtnOpcionBuscar.SelectedValue = "2" Then 'EAN
			Me.txtBuscar.Visible = True
			Me.btnBuscar.Visible = True
			Me.cboMarbetes.Visible = False
			Me.lblAlmacen.Visible = False
			Me.btnAgregar.Enabled = True
		End If
	End Sub

	Private Sub rbtnOpcionBuscarCds_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnOpcionBuscarCds.SelectedIndexChanged
		If Me.rbtnOpcionBuscarCds.SelectedValue = "1" Then 'MARBETE
			Me.txtBuscar.Visible = False
			Me.btnBuscar.Visible = False
			Me.cboMarbetes.Visible = True
			Me.lblAlmacen.Visible = True
			If Me.cboMarbetes.SelectedIndex = 0 Then
				Me.btnAgregar.Enabled = False
			Else
				Me.btnAgregar.Enabled = True
			End If
		ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = "2" OrElse Me.rbtnOpcionBuscarCds.SelectedValue = "3" Then 'EAN
			Me.txtBuscar.Visible = True
			Me.btnBuscar.Visible = True
			Me.cboMarbetes.Visible = False
			Me.lblAlmacen.Visible = False
			Me.btnAgregar.Enabled = True
		End If
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))

			ds = DatosSQL.funcioncp("Marbetes_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdMarbete") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Me.cboMarbetes.DataSource = ds.Tables(0)
				Me.cboMarbetes.DataValueField = "IdMarbete"
				Me.cboMarbetes.DataTextField = "IdMarbete"
				Me.cboMarbetes.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarAlmacen()
		Try
			Dim dsAlmacenes As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Perfil", Session("Perfil"))
			par(1) = New SqlParameter("@idMarbete", Me.cboMarbetes.SelectedValue)
			par(2) = New SqlParameter("@IdTienda", Session("IdTienda"))

			dsAlmacenes = DatosSQL.funcioncp("Buscar_Almacen_Marbete", par)

			If dsAlmacenes.Tables(0).Rows.Count = 0 Then
				Me.lblAlmacen.Text = ""
			Else
				Me.lblAlmacen.Visible = True
				Me.lblAlmacen.Text = "Almacén: " & dsAlmacenes.Tables(0).Rows(0).Item(0)
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
		Try
			Dim irow As Integer = Integer.Parse(e.CommandArgument.ToString)
			If e.CommandName = "Modificar" Then
				Dim Cant As TextBox = CType(grid.Rows(irow).Cells(5).Controls(1), TextBox)

				If Cant.Text.Trim = "" Then
					CMensajes.MostrarMensaje("Ingrese la cantidad antes de continuar.", CMensajes.Tipo.GetError, wucMessageAdmonArticuloMarbete)
				ElseIf IsNumeric(Cant.Text.Trim) = False Then
					CMensajes.MostrarMensaje("La cantidad debe ser un dato numerico.", CMensajes.Tipo.GetError, wucMessageAdmonArticuloMarbete)
				ElseIf CDbl(Cant.Text.Trim) < 0 Then
					CMensajes.MostrarMensaje("La cantidad debe ser un dato numerico mayor que cero.", CMensajes.Tipo.GetError, wucMessageAdmonArticuloMarbete)
				ElseIf CDbl(Cant.Text.Trim) = CDbl(grid.Rows(irow).Cells(12).Text) Then

				Else
					If Preconteos_Conteo_Administracion(2, grid.Rows(irow).Cells(1).Text, grid.Rows(irow).Cells(3).Text, Cant.Text, grid.Rows(irow).Cells(10).Text) = True Then
						Me.Preconteos_Conteos_Buscar()

						If Session("Cedis") = 1 Then
							If Me.rbtnOpcionBuscarCds.SelectedValue = "1" Then 'MARBETE
								Me.txtBuscar.Visible = False
								Me.btnBuscar.Visible = False
								Me.cboMarbetes.Visible = True
							ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = "2" OrElse Me.rbtnOpcionBuscarCds.SelectedValue = "3" Then 'EAN
								Me.txtBuscar.Visible = True
								Me.btnBuscar.Visible = True
								Me.cboMarbetes.Visible = False
							End If
						Else
							If Me.rbtnOpcionBuscar.SelectedValue = "1" Then 'MARBETE
								Me.txtBuscar.Visible = False
								Me.btnBuscar.Visible = False
								Me.cboMarbetes.Visible = True
							ElseIf Me.rbtnOpcionBuscar.SelectedValue = "2" Then 'EAN
								Me.txtBuscar.Visible = True
								Me.btnBuscar.Visible = True
								Me.cboMarbetes.Visible = False
							End If
						End If

						CMensajes.MostrarMensaje("Cantidad actualizada correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonArticuloMarbete)
					End If
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub grid_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grid.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			Dim Rep As String = HttpUtility.HtmlDecode(e.Row.Cells(6).Text)
			If String.IsNullOrEmpty(Rep) Or String.IsNullOrWhiteSpace(Rep) Or Rep.Trim = "&nbsp;" Then
				e.Row.BackColor = Drawing.Color.DarkOrange
			Else
				'e.Row.BackColor = Drawing.Color.White
			End If

			Dim lnkmodificar As LinkButton = CType(e.Row.Cells(8).Controls(1), LinkButton)

			grid.HeaderRow.Cells(7).Visible = False 'bultos
			e.Row.Cells(7).Visible = False
			grid.HeaderRow.Cells(10).Visible = False 'idPreconteo
			e.Row.Cells(10).Visible = False
			grid.HeaderRow.Cells(11).Visible = False 'bultov
			e.Row.Cells(11).Visible = False

			grid.HeaderRow.Cells(12).Visible = False 'cantidad
			e.Row.Cells(12).Visible = False
		End If
	End Sub

	Private Sub cboMarbetes_SelectedIndexChanged1(sender As Object, e As System.EventArgs) Handles cboMarbetes.SelectedIndexChanged
		Try
			If Me.cboMarbetes.SelectedIndex <> -1 Then
				Preconteos_Conteos_Buscar()
			End If
			CargarAlmacen()
			If Me.cboMarbetes.SelectedIndex = 0 Then
				Me.btnAgregar.Enabled = False
			Else
				Me.btnAgregar.Enabled = True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(grid.HeaderRow.Cells(8).FindControl("chkHeader"), CheckBox)
		For Each row As GridViewRow In grid.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(8).FindControl("CheckBox5"), CheckBox)
			chk.Checked = chk1.Checked
		Next

		If Session("Cedis") = 1 Then
			If Me.rbtnOpcionBuscarCds.SelectedValue = "1" Then 'MARBETE
				Me.txtBuscar.Visible = False
				Me.btnBuscar.Visible = False
				Me.cboMarbetes.Visible = True
				Me.lblAlmacen.Visible = True
			ElseIf Me.rbtnOpcionBuscarCds.SelectedValue = "2" OrElse Me.rbtnOpcionBuscarCds.SelectedValue = "3" Then 'EAN
				Me.txtBuscar.Visible = True
				Me.txtBuscar.Visible = True
				Me.cboMarbetes.Visible = False
				Me.lblAlmacen.Visible = False
			End If
		Else
			If Me.rbtnOpcionBuscar.SelectedValue = "1" Then 'MARBETE
				Me.txtBuscar.Visible = False
				Me.btnBuscar.Visible = False
				Me.cboMarbetes.Visible = True
				Me.lblAlmacen.Visible = True
			ElseIf Me.rbtnOpcionBuscar.SelectedValue = "2" Then 'EAN
				Me.txtBuscar.Visible = True
				Me.btnBuscar.Visible = True
				Me.cboMarbetes.Visible = False
				Me.lblAlmacen.Visible = False
			End If
		End If
	End Sub

	Protected Sub CheckVacio(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(grid.HeaderRow.Cells(8).FindControl("CheckBox5"), CheckBox)
		For Each row As GridViewRow In grid.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(8).FindControl("CheckBox5"), CheckBox)
			'If chk.Checked Then
			'	For i As Integer = 0 To grid.HeaderRow.Cells.Count - 1
			'		row.Cells(i).CssClass = "SelectedRow_Office2007"
			'	Next
			'Else
			'	For i As Integer = 0 To grid.HeaderRow.Cells.Count - 1
			'		row.Cells(i).CssClass = ""
			'	Next
			'End If
			'chk.Checked = chk1.Checked
		Next

	End Sub

	Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
		Response.Redirect("AgregarArticuloMarbete.aspx?P=" & Session("Perfil") & "&SID=" & Request.QueryString("SID") & "&Origen=AAM&Refresh=S", False)
	End Sub

	Protected Sub btnagregar_marbete_Click(sender As Object, e As EventArgs) Handles btnagregar_marbete.Click
		Response.Redirect("AdmonMarbetes.aspx?P=" & Session("Perfil") & "&Accion=1&SID=" & Request.QueryString("SID") & "&Origen=AAM&Refresh=S", False)
	End Sub
End Class

