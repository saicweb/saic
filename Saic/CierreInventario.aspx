﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CierreInventario.aspx.vb" Inherits="Saic.CierreInventario" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Cierre de inventario" class="titulos" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucmessage ID="wucMessageCierreInventario" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="100%">
                    <tr>
                       <td align="center">
                           <asp:button id="btnGenerarReporte" runat="server" text="Cerrar inventario"></asp:button>
                       </td>	   
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
			    </table>
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
