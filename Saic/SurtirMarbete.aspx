﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SurtirMarbete.aspx.vb" Inherits="Saic.SurtirMarbete" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                Resurtir artículos en marbete
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageSurtirMarbete" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="35%">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Marbete: " id="lblIdMarbete" ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboMarbetes" runat="server" AutoPostBack="true"  Width="150px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacén: " id="lblAlmacen" ></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="" id="lblAlmacen1" ></asp:Label>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><br />
                            <span><asp:Button runat="server" Text="Agregar" ID="btnGuardar" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
