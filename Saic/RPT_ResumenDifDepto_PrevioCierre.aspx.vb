﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_ResumenDifDepto_PrevioCierre
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject
	Dim txtAlmacen As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageRPT_ResumenDifDepto_PrevioCierre.Visible = False

		If Not Page.IsPostBack Then
			Almacen_Buscar()
			Session.Remove("RPT_ResumenDifDeptoCierre")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Dim almacen As String = Me.cboAlmacen.SelectedValue
		CatalogoReportes_CRV13(almacen)
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_ResumenDifDepto_Previo_Cierre.DataBinding
		If Session("RPT_ResumenDifDeptoCierre") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_ResumenDifDepto_Previo_Cierre.ReportSource = rpt
				Session("RPT_ResumenDifDeptoCierre") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_ResumenDifDepto_Previo_Cierre.ReportSource = Session("RPT_ResumenDifDeptoCierre")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = RPT_ResumenDifDepto_PrevioCierre(Almacen)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_ResumenDifDepto_PrevioCierre))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtAlmacen = rpt.ReportDefinition.Sections("Section1").ReportObjects("TxtAlmacen")
			txtNombreReporte.Text = "RESUMEN DE DIFERENCIAS POR DEPARTAMENTO ANTES DEL CIERRE DE INVENTARIO"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			txtAlmacen.Text = IIf(Almacen = "0", "Todos", Almacen)
			rpt.SetDataSource(ds.Tables(0))
			RPT_ResumenDifDepto_Previo_Cierre.ReportSource = rpt
			RPT_ResumenDifDepto_Previo_Cierre.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_ResumenDifDepto_PrevioCierre)
		End If
	End Sub

	Public Function RPT_ResumenDifDepto_PrevioCierre(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Almacen", Almacen)
			ds = DatosSQL.funcioncp("RPT_ResumenDifDeptoPrevCierre", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(1) As SqlParameter
				par2(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par2(1) = New SqlParameter("@Almacen", Almacen)

				dsExcel = DatosSQL.funcioncp("RPT_ResumenDifDeptoPrevCierreExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					Reportes.REPEXCEL(dsExcel, "Resumen Diferencias Por Departamento Antes Del Cierre De Inventario", "ResumenPorDepartamentoPrevioAlCierre_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), True)
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@validar", 2)
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class