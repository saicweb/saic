﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CorreccionesFinalesPorEAN.aspx.vb" Inherits="Saic.CorreccionesFinalesPorEAN" %>
 
<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Correcciones finales" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="" ID="lblTitulo" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCorreccionesFinalesPorEAN" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="65%">
                    <tr>
                        <td></td>
                        <td>
                            <asp:Label runat="server" Text="" ID="lblTipo" Visible="false" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacén: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboAlmacenes" runat="server" Width="140px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="EAN: " ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEAN" runat="server" MaxLength="50" Width="170px" CssClass="inline"></asp:TextBox> 
                            <asp:ImageButton runat="server" ID="ImgBuscar" ImageUrl="~/Img/seearch.png" Width="24px" Height="24px" CssClass="inline" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Cantidad actual: " ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCantidad" runat="server" MaxLength="50" Width="130px" ReadOnly="true"></asp:TextBox> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Cantidad final: " ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtConteoFinal" runat="server" MaxLength="50" Width="130px" ></asp:TextBox> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Descripción: " ></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtDescripcion" runat="server" MaxLength="100" Width="400px" ReadOnly="true"></asp:TextBox> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Departamento: " ></asp:Label>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtDepartamento" runat="server" MaxLength="100" Width="400px" ReadOnly="true" ></asp:TextBox> 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table runat="server" width="75%">
                                <tr>
                                    <td align="center">
                                        <span><asp:Button runat="server" Text="Guardar" ID="btnGuardar" CssClass="btn" Visible="false" /></span> 
                                    </td>
                                    <td align="center">
                                        <span><asp:Button runat="server" Text="Cancelar" ID="btnCancelar" CssClass="btn" Visible="false" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
