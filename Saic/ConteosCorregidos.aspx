﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ConteosCorregidos.aspx.vb" Inherits="Saic.ConteosCorregidos" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
     <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Reporte de correcciones a conteos" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageConteosCorregidos" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="45%" align="center">
                    <tr>
                        <td align="center">
                            <asp:RadioButtonList runat="server" ID="rbtnRPTFormatos" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
                                <asp:ListItem Value="PDF" Selected="True">PDF</asp:ListItem>
								<asp:ListItem Value="EXCEL">EXCEL</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
