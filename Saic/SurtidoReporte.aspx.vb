﻿Imports System.Data.SqlClient
Imports dbAutoTrack.DataReports
Imports System.IO

Public Class SurtidoReporte
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageSurtidoReporte.Visible = False

		If Request.QueryString("Tipo") = 1 Then
			lblTitulos.Text = "Resumen de mercancía escaneada"
		ElseIf Request.QueryString("Tipo") = 2 Then
			lblTitulos.Text = "Resumen de mercancía surtida"
		End If

		If Not IsPostBack Then
			Almacen_Buscar()
		End If
	End Sub

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Perfil", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@Accion", 8)
			DS = DatosSQL.funcioncp("ConteosDispositivo_Surtir", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPORTE(ByVal DATOS As DataSet)
		Try
			'//////////////////////////////////////////////////////////////////////
			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim dt As New DataTable 'Datatable definitivo
			Dim settings As New dbAutoTrack.DataReports.PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Landscape
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			Dim NombreArchivo As String
			If Request.QueryString("Tipo") = 1 Then
				NombreArchivo = "RPT_ResumenMercanciaEscaneada"
				report1 = New RPT_ResumenMercanciaEscaneadarpt

			ElseIf Request.QueryString("Tipo") = 2 Then
				'REPORTE PARA INV GRAL
				NombreArchivo = "RPT_ResumenMercanciaSurtida"
				report1 = New RPT_ResumenMercanciaSurtidarpt
			End If

			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim pathPdf As String = DirectorioArchivos & NombreArchivo & "_" & Session("IdUsuario") & ".pdf"
			'JLGH 27/SEP/2012
			'report1.DataSource = dt
			report1.DataSource = DATOS.Tables(0)

			Dim Almacen As String = cboAlmacen.SelectedValue
			If Almacen = "0" Then
				Almacen = "Todos"
			End If

			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
			CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")
			CType(report1.Sections.Item(0).Controls.Item("txtAlmacen"), dbATTextBox).Value = Almacen

			document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPEXCEL(ByVal DATOS As DataTable, ByVal Fecha As String, ByVal Hora As String, ByVal Tienda As String, ByVal Almacen As String)
		Try
			'Crear el directorio si no existe
			Dim TitulosArchivo As String
			Dim Archivo As String
			Dim Subtitulo As String

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			If Request.QueryString("Tipo") = 1 Then
				Archivo = DirectorioArchivos & "MercanciaEscaneada_" & Now.ToString("yyyyMMdd") & ".xls"
				TitulosArchivo = "Resumen de Mercancía Escaneada"
				Subtitulo = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda & "       Conteo1 " & "       Almacen: " & Almacen
			ElseIf Request.QueryString("Tipo") = 2 Then
				Archivo = DirectorioArchivos & "MercanciaSurtida_" & Now.ToString("yyyyMMdd") & ".xls"
				TitulosArchivo = "Resumen de Mercancía Surtida"
				Subtitulo = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda & "       Conteo1 " & "       Almacen: " & Almacen
			End If

			'Guardar la información en el archivo txt
			Reportes.Exportar_ExcelDinamico(DATOS, Archivo, TitulosArchivo, Subtitulo)
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGenerarReporte_Click(sender As Object, e As System.EventArgs) Handles btnGenerarReporte.Click
		Dim almacen As String
		almacen = Me.cboAlmacen.SelectedValue

		Dim ds As New DataSet
		Dim par(2) As SqlParameter
		par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
		par(1) = New SqlParameter("@Almacen", almacen)
		par(2) = New SqlParameter("@Perfil", Left(Session("Perfil"), 1))


		If Request.QueryString("Tipo") = 1 Then
			If Me.rbtnRPTFormatos.Items(0).Selected = True Then
				ds = DatosSQL.funcioncp("RPT_ResumenMercanciaEscaneada", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ResumenMercanciaEscaneadaExcel", par)
			End If
		ElseIf Request.QueryString("Tipo") = 2 Then
			If Me.rbtnRPTFormatos.Items(0).Selected = True Then
				ds = DatosSQL.funcioncp("RPT_ResumenMercanciaSurtida", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ResumenMercanciaSurtidaExcel", par)
			End If
		End If

		If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
			Me.HyperLink1.Visible = True

			If Me.rbtnRPTFormatos.Items(0).Selected = True Then
				REPORTE(ds)
			Else
				If almacen = "0" Then almacen = "Todos"
				If ds.Tables(0).Rows.Count > 1 Then
					REPEXCEL(ds.Tables(0), Now.ToShortDateString, Now.ToShortTimeString, Session("IdTienda"), almacen)
				Else
					CMensajes.MostrarMensaje("No existen datos para generar el reporte.", CMensajes.Tipo.GetError, wucMessageSurtidoReporte)
				End If
			End If

		Else
			CMensajes.MostrarMensaje("No existen datos para generar el reporte.", CMensajes.Tipo.GetError, wucMessageSurtidoReporte)
		End If
	End Sub
End Class