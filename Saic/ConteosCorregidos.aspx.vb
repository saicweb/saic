﻿Imports dbAutoTrack.DataReports
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class ConteosCorregidos
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            Try
                If Session("IdUsuario") Is Nothing Then
                    Me.btnGenerarReporte.Visible = False
                    'lblMensaje.Text = "Su sesion ha expirado, favor de volver a iniciar sesion."
                    CMensajes.MostrarMensaje("Su sesión ha expirado, favor de volver a iniciar sesión.", CMensajes.Tipo.GetError, wucMessageConteosCorregidos)
                    Exit Sub
                End If
                'If Session("Perfil") = "Rotativo" Then
                '    Me.btnGenerarReporte.Visible = False
                '    Me.MsgBox1.ShowMessage("El Perfil de Inventario Rotativo no tiene permiso de acceso a esta sección.")
                '    Exit Sub
                'End If

                'Me.lblTipo.Text = CStr(Session("Perfil")).Substring(0, 1)

            Catch ex As Exception
                'lblMensaje.Text = "Error P.RPT.ANC.1: " & ex.Message
                Throw New Exception("Error P.RPT.ANC.1: " & ex.Message)
                Me.btnGenerarReporte.Visible = False
            End Try
        End If
    End Sub


	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Me.HyperLink1.Visible = False
			'Me.lblMensaje.Text = ""
			If Session("IdUsuario") Is Nothing Then
				Me.btnGenerarReporte.Visible = False
				'lblMensaje.Text = "Su sesion ha expirado, favor de volver a iniciar sesion."
				CMensajes.MostrarMensaje("Su sesión ha expirado, favor de volver a iniciar sesión.", CMensajes.Tipo.GetError, wucMessageConteosCorregidos)
				Exit Sub
			End If
			Dim ds As New DataSet

			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))

			If Me.rbtnRPTFormatos.Items(0).Selected = True Then
				ds = DatosSQL.funcioncp("RPT_ConteosCorregidos", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ConteosCorregidosExcel", par)
			End If


			If Not ds Is Nothing OrElse ds.Tables.Count > 0 Then
				If ds.Tables(0).Rows.Count > 0 Then
					Me.HyperLink1.Visible = True
					If Me.rbtnRPTFormatos.Items(0).Selected = True Then
						REPORTE(ds)
					Else
						REPEXCEL(ds.Tables(0), Today.ToShortDateString, Today.ToShortTimeString, Session("IdTienda"))
					End If
					'cerrar ventana
					'Me.RadWindowManager1.Behavior = Telerik.WebControls.RadWindowBehaviorFlags.Close
					'Me.RadWindowManager1.Dispose()
				Else
					'Me.lblMensaje.Text = "No existen Marbetes."
					CMensajes.MostrarMensaje("No existen Marbetes.", CMensajes.Tipo.GetError, wucMessageConteosCorregidos)
					'Me.link.Visible = False
				End If
			Else
				'Me.lblMensaje.Text = "No existen Marbetes."
				CMensajes.MostrarMensaje("No existen Marbetes.", CMensajes.Tipo.GetError, wucMessageConteosCorregidos)
				'Me.link.Visible = False
			End If
		Catch ex As Exception
			'lblMensaje.Text = "Error P.RPT.ANC.2: " & ex.Message
			Throw New Exception("Error P.RPT.ANC.2: " & ex.Message)
			Me.btnGenerarReporte.Visible = False
			'Me.RadWindowManager1.Behavior = Telerik.WebControls.RadWindowBehaviorFlags.Close
			'Me.RadWindowManager1.Dispose()
			'Me.RadWindowManager1.Visible = False
		End Try
	End Sub

	Sub REPEXCEL(ByVal DATOS As DataTable, ByVal Fecha As String, ByVal Hora As String, ByVal Tienda As String)
        Try
            'Crear el directorio si no existe
            If DATOS.Rows.Count = 0 Then
                'Me.lblMensaje.Text = "Sin Datos."
                CMensajes.MostrarMensaje("Sin Datos.", CMensajes.Tipo.GetError, wucMessageConteosCorregidos)
            End If

            Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("ConteosCorregidos")

            If Not Directory.Exists(DirectorioArchivos) Then
                Directory.CreateDirectory(DirectorioArchivos)
            End If

            Dim Archivo As String = DirectorioArchivos & "ConteosCorregidos_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"
			Dim TitulosArchivo As String = "REPORTE DE CORRECCIONES A CONTEOS"
			'Guardar la información en el archivo txt
			Dim Subtitulo As String = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda
            Reportes.Exportar_ExcelDinamico(DATOS, Archivo, TitulosArchivo, Subtitulo)
            'Response.Redirect("FileDownload.aspx?file=" & Archivo)
            'Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & Archivo
            Dim encripta As New Encryption
            Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
        Catch ex As Exception
            Throw New Exception("D- " & ex.Message)
        End Try
    End Sub


    Sub REPORTE(ByVal DATOS As DataSet)
        Try
            '//////////////////////////////////////////////////////////////////////
            'generacion de  reporte en pdf
            Dim report1 As dbATReport
            Dim settings As New dbAutoTrack.DataReports.PageSettings
            settings.PaperKind = Drawing.Printing.PaperKind.Letter
            settings.Orientation = PageOrientation.Portrait
            settings.Margins.MarginLeft = 0
            settings.Margins.MarginRight = 0
            settings.Margins.MarginTop = 0
            settings.Margins.MarginBottom = 0

            report1 = New RPT_ConteosCorregidos
            report1.PageSetting = settings
            Dim document1 As New PDFExport.PDFDocument

            Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("ConteosCorregidos")

            If Not Directory.Exists(DirectorioArchivos) Then
                Directory.CreateDirectory(DirectorioArchivos)
            End If


            Dim pathPdf As String = DirectorioArchivos & "ConteosCorregidos_" & Session("Perfil") & "_" & Session("IdUsuario") & ".pdf"
            report1.DataSource = DATOS

            CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = Today.ToShortDateString
            CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = Today.ToShortTimeString
            CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = Session("IdTienda")
            'If Me.lblTipo.Text = "P" Then
            '    report1.Sections.Item(3).Visible = False
            'Else
            '    report1.Sections.Item(3).Visible = True
            '    CType(report1.Sections.Item(3).Controls.Item("txtTotal"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("txtTotal")
            'End If

            document1.EmbedFont = True
            document1.Compress = True
            report1.Generate()
            document1.Export(report1.Document, pathPdf)
            'Me.link.NavigateUrl = "FileDownload.aspx?file=" & pathPdf
            'Me.link.Visible = True
            'Response.Redirect("FileDownload.aspx?file=" & pathPdf)
            'Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & pathPdf
            Dim encripta As New Encryption
            Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
            '//////////////////////////////////////////////////
            'termina el reporte
        Catch ex As Exception
            Throw New Exception("A- " & ex.Message)
        End Try
    End Sub

End Class

