﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SubirArchivoProvExterno.aspx.vb" Inherits="Saic.SubirArchivoProvExterno" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Cargar archivo de proveedor externo" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageSubirArchivoProvExterno" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="65%" align="center">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="uplXML" runat="server">
                                <ContentTemplate>
                                    <table runat="server" width="100%" align="center">
                                        <tr>
                                            <td align="right">
                                                <asp:Label runat="server" Width="100px" Text="Almacén:"></asp:Label>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="cboAlmacenActual" TabIndex="1" runat="server" Width="100px" CssClass="dropdown"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:FileUpload ID="singleUpload" runat="server" CssClass="upload"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2"><br />
                                                <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" CssClass="btn"></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnCargar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"><br />
                            <asp:Label ID="lblTiempoProceso" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <wum:wucMessage ID="wucMessageMensaje" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"><br />
                            <asp:Label ID="lblErroresPresentados" runat="server" Text="Errores presentados:" Font-Bold="True" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DataGrid ID="dgErrores" runat="server" SkinID="DGrid3" AllowPaging="true" AutoGenerateColumns="False" HorizontalAlign="Center" Visible="false">
                                <PagerStyle CssClass="pagination-centered" />
                                <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                <Columns>
                                    <asp:BoundColumn DataField="EAN" HeaderText="EAN" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="115px" ItemStyle-Width="115px"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Mensaje" HeaderText="Mensaje" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="500px" ItemStyle-Width="500px"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
