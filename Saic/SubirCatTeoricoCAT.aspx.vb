﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Xml

Public Class SubirCatTeoricoCAT
    Inherits System.Web.UI.Page

    Dim dtDatos As DataTable
    Dim dtErrores As DataTable
    Dim arrayXml As String() = Nothing
    Dim XmlWrt As XmlWriter = Nothing
    Dim dt As DataTable
    Dim ArchivoDatos As String = ""
	Public Argumentos() As String

	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
			If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If

			If Session("Perfil") = "P" Then
				Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
			End If

			'TIPO INVENTARIO EN PROCESO

			If Session("TipoInventario") = "G" Then 'GENERAL
				If Session("Sociedad") = 3 Then
					Me.btnDescargar.Visible = False
					Me.btnCargar.Visible = True
					Me.singleUpload.Visible = True
				Else
					Me.btnDescargar.Visible = True
					Me.btnCargar.Visible = False
					Me.singleUpload.Visible = False
				End If
			ElseIf Session("TipoInventario") = "P" Then  'PARCIAL
				Me.btnDescargar.Visible = False
                Me.btnCargar.Visible = True
                Me.singleUpload.Visible = True
            End If
            Me.btnCargar.Attributes.Add("onclick", "this.disabled=true;" + GetPostBackEventReference(btnCargar).ToString())
            Me.btnDescargar.Attributes.Add("onclick", "this.disabled=true;" + GetPostBackEventReference(btnDescargar).ToString())
        End If
    End Sub

	Private Sub btnDescargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDescargar.Click
		Try
			Dim carpetasCAT As String = ConfigurationManager.AppSettings("DirectorioCAT")
			Dim archivoCAT As String = carpetasCAT & ConfigurationManager.AppSettings("PrefijoCAT") & Session("IdTienda") & ".txt"
			Dim cs As String = ConfigurationManager.AppSettings("cnn").ToString()

			If Not Directory.Exists(carpetasCAT) Then
				Directory.CreateDirectory(carpetasCAT)
			End If

			'Dim tabla As String = ""
			Dim line As String = Nothing
			Dim i As Integer = 0
			Dim a As Integer = 0
			Dim dt As New DataTable()

			If (File.Exists(archivoCAT) = True) Then
				BorrarTabla()

				dt = LeerTxt.LeerTxtADT(archivoCAT, "|c")

				If Session("Sociedad").ToString = "3" Then
					Dim fecha() As String = Session("lblFechaInv").ToString.Split("-")
					Dim fecha1 As String = fecha(1).Trim.Substring(6, 11)
					Dim fec As String = Convert.ToDateTime(fecha1).ToString("yyyy-MM-dd") & " 00:00:00"
					Dim newColumn As New DataColumn("FechaInventario", GetType(System.String))
					newColumn.DefaultValue = fec
					dt.Columns.Add(newColumn)
					newColumn.SetOrdinal(0)
					dt.AcceptChanges()

					newColumn = New DataColumn("IdAlmacen", GetType(System.String))
					newColumn.DefaultValue = "A001"
					dt.Columns.Add(newColumn)
					newColumn.SetOrdinal(5)
					dt.AcceptChanges()
				Else
					Dim newColumn As New DataColumn("Column11", GetType(System.Decimal))
					newColumn.DefaultValue = 0.0
					dt.Columns.Add(newColumn)
				End If

				Dim dsSufijo As DataSet = Nothing
				Dim tabla As String = Nothing
				Dim sufijo As String = Nothing
				Dim par(0) As SqlParameter
				par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				dsSufijo = DatosSQL.funcioncp(CReglasNegocio.Sufijo, par)

				If dsSufijo IsNot Nothing AndAlso dsSufijo.Tables.Count > 0 AndAlso dsSufijo.Tables(0).Rows.Count > 0 Then
					sufijo = dsSufijo.Tables(0).Rows(0)("Sufijo")
					tabla = CReglasNegocio.CatalogoInventarioTeorico & sufijo
					BulkCopy.Insertar(dt, tabla)
				End If

				If Session("Sociedad").ToString <> "3" Then
					CAT_DocumentoInventario_LOCAL()
				End If
				CMensajes.MostrarMensaje("Se insertaron " & dt.Rows.Count & " registros correctamente.", CMensajes.Tipo.GetExito, wucMessageFicheroCatalogoTeorico)
			Else
				CMensajes.MostrarMensaje("No existe el archivo: " & archivoCAT & ".", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnCargar_Click(sender As Object, e As EventArgs) Handles btnCargar.Click
		Me.btnCargar.Enabled = False
		Me.btnCargar.Text = "Subiendo archivo..."

		Dim TipoSAIC As String = ConfigurationManager.AppSettings("TipoSAIC")

		' rutina para SAIC LOCAL
		If TipoSAIC.ToUpper = "LOCAL" Then
			Try
				Me.lblTiempoProceso.Text = "Hora de Inicio: " & Now.ToShortTimeString
				Dim procede As Boolean
				procede = CargarArchivo(10, "LOCAL")
				'CMensajes.MostrarMensaje("Proceso terminado.", CMensajes.Tipo.GetExito, wucMessageFicheroCatalogoTeorico)
				Me.lblTiempoProceso.Text &= "  Hora de Término: " & Now.ToShortTimeString
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		Else
			' rutina original para ambiente Web centralizado
			Try
				Me.lblTiempoProceso.Text = "Hora de Inicio: " & Now.ToShortTimeString
				Dim procede As Boolean
				procede = CargarArchivo(10, "WEB")
				'CMensajes.MostrarMensaje("Proceso terminado.", CMensajes.Tipo.GetExito, wucMessageFicheroCatalogoTeorico)
				Me.lblTiempoProceso.Text &= "  Hora de Termino: " & Now.ToShortTimeString
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If

		Me.btnCargar.Enabled = True
		Me.btnCargar.Text = "Subir catálogo"
	End Sub

	'CARGAR ARCHIVO
	Private Function CargarArchivo(ByVal columnas As Integer, ByVal TipoSAIC As String)
		Dim strRuta As String = ConfigurationManager.AppSettings("SAIC")

		If Not Directory.Exists(strRuta) Then
            Directory.CreateDirectory(strRuta)
        End If

        Try
            Dim IdTienda As String
            Dim nomArchivo As String
            Dim singleResultExist As Boolean = Me.singleUpload.HasFile
            If singleResultExist Then
                nomArchivo = singleUpload.FileName
                IdTienda = nomArchivo.ToUpper.Substring(3, 4)
				If Session("Sociedad").ToString <> "3" AndAlso nomArchivo.ToUpper.Substring(0, 3) <> "CAT" Then
					CMensajes.MostrarMensaje("El nombre del archivo debe inciar con el prefijo 'CAT'", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
					Return False
				ElseIf Session("Sociedad").ToString = "3" AndAlso nomArchivo.ToUpper.Substring(0, 4) <> "CATR" Then
					CMensajes.MostrarMensaje("El nombre del archivo debe inciar con el prefijo 'CATR'", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
					Return False
				ElseIf IdTienda <> Session("IdTienda") Then
					CMensajes.MostrarMensaje("La tienda especificada en el nombre del archivo no corresponde a la tienda con la cual se esta trabajando: " & Session("IdTienda") & ".", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
					Return False
				ElseIf Me.Tienda_Existe(IdTienda) = False Then
					CMensajes.MostrarMensaje("La tienda especificada en el nombre del archivo no existe en la base de datos.", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
					Return False
				ElseIf singleUpload.FileName.ToUpper.EndsWith(".TXT") Then
					strRuta &= nomArchivo
					Session("nomArchivo") = nomArchivo
					singleUpload.SaveAs(strRuta)

					Dim Archivo As New FileInfo(strRuta)
					If TipoSAIC.ToUpper = "LOCAL" Then
						CargaTxt(strRuta)
					Else
						Dim archivoCAT As String = strRuta
						Dim dt As New DataTable()

						If (File.Exists(archivoCAT) = True) Then
							BorrarTabla()

							dt = LeerTxt.LeerTxtADT(archivoCAT, "|c")

							If Session("Sociedad").ToString = "3" Then
								Dim fecha() As String = Session("lblFechaInv").ToString.Split("-")
								Dim fecha1 As String = fecha(1).Trim.Substring(6, 11)
								Dim fec As String = Convert.ToDateTime(fecha1).ToString("yyyy-MM-dd") & " 00:00:00"
								Dim newColumn As New DataColumn("FechaInventario", GetType(System.String))
								newColumn.DefaultValue = fec
								dt.Columns.Add(newColumn)
								newColumn.SetOrdinal(0)
								dt.AcceptChanges()

								newColumn = New DataColumn("IdAlmacen", GetType(System.String))
								newColumn.DefaultValue = "A001"
								dt.Columns.Add(newColumn)
								newColumn.SetOrdinal(5)
								dt.AcceptChanges()
							Else
								Dim newColumn As New DataColumn("Column11", GetType(System.Decimal))
								newColumn.DefaultValue = 0.0
								dt.Columns.Add(newColumn)
							End If

							Dim dsSufijo As DataSet = Nothing
							Dim tabla As String = Nothing
							Dim sufijo As String = Nothing
							Dim par(0) As SqlParameter
							par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
							dsSufijo = DatosSQL.funcioncp(CReglasNegocio.Sufijo, par)

							If dsSufijo IsNot Nothing AndAlso dsSufijo.Tables.Count > 0 AndAlso dsSufijo.Tables(0).Rows.Count > 0 Then
								sufijo = dsSufijo.Tables(0).Rows(0)("Sufijo")
								tabla = CReglasNegocio.CatalogoInventarioTeorico & sufijo
								BulkCopy.Insertar(dt, tabla)
							End If

							If Session("Sociedad").ToString <> "3" Then
								CAT_DocumentoInventario_LOCAL()
							End If
							CMensajes.MostrarMensaje("Se insertaron " & dt.Rows.Count & " registros correctamente.", CMensajes.Tipo.GetExito, wucMessageFicheroCatalogoTeorico)
						Else
							CMensajes.MostrarMensaje("No existe el archivo: " & archivoCAT, CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
						End If
					End If
					Return True
				Else
					CMensajes.MostrarMensaje("El archivo a cargar debe ser un archivo de texto con extensión .txt.", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
					Return False
                End If
            Else
				CMensajes.MostrarMensaje("No se ha especificado el nombre del archivo a cargar.", CMensajes.Tipo.GetError, wucMessageFicheroCatalogoTeorico)
				Return False
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		Finally
            If strRuta <> "" And File.Exists(strRuta) = True Then
                File.Delete(strRuta)
            End If
        End Try
    End Function

    Private Function Tienda_Existe(ByVal IdTienda As String) As Boolean
        Tienda_Existe = False
        Try
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@IdTienda", IdTienda)
            Dim ds As New DataSet
            ds = DatosSQL.funcioncp("Tiendas_Buscar", par)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Return True
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Function

	Sub CargaTxt(ByVal Archivo As String)
        Dim Ruta As String
        Dim NombreArchivo As String
        Dim Status As Boolean = False

        NombreArchivo = IO.Path.GetFileName(Archivo)
        Ruta = IO.Path.GetFullPath(Archivo)
        Ruta = Ruta.Replace(NombreArchivo, "")

        Dim Registros As String = ""
        Dim ds As New DataSet
        Dim Param(1) As SqlParameter
        Param(0) = New SqlParameter("@Ruta", Ruta)
        Param(1) = New SqlParameter("@Archivo", NombreArchivo)
        Try
            ds = DatosSQL.funcioncp("CargaTxtInvTeoricoLocal", Param)
            Registros = ds.Tables(0).Rows(0).Item("REGISTROS")
            Status = True
        Catch ex As Exception
            Registros = ex.Message
        End Try

		CMensajes.MostrarMensaje("Registros cargados: " & Registros, CMensajes.Tipo.GetInformativo, wucMessageFicheroCatalogoTeorico)

		If Status Then
			'----Contar Doc Inventario (EN SAIC LOCAL LO TOMA DE LA TABLA CatalogoInventarioTeorico POR MEDIO DEL SP CAT_DocumentoInventario_LOCAL)
			CAT_DocumentoInventario_LOCAL()

            CAT_Errores()
        End If

    End Sub

	Private Sub CAT_DocumentoInventario_LOCAL()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("CAT_DocumentoInventario_LOCAL", par)
			Me.lblCantidad.Text &= ds.Tables(0).Rows.Count
			lblCantidad.Visible = True
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.DGDocumentoInventario.Visible = True
				Me.PnlDocInventario.Visible = True
				Me.DGDocumentoInventario.DataSource = ds.Tables(0)
				Me.DGDocumentoInventario.DataBind()
			Else
				Me.dgErrores.Visible = False
				Me.PnlErrores.Visible = False
				Me.lblErroresPresentados.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub CAT_Errores()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("CAT_Administracion", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				CMensajes.MostrarMensaje("Articulos cargados en Catalogo: " & ds.Tables(0).Rows(0).Item("Conteo"), CMensajes.Tipo.GetInformativo, wucMessageFicheroCatalogoTeorico)
				Me.lblErroresPresentados.Visible = True
				Me.dgErrores.Visible = True
				Me.PnlErrores.Visible = True
				Me.dgErrores.DataSource = ds.Tables(0)
				Me.dgErrores.DataBind()
			Else
				Me.dgErrores.Visible = False
				Me.PnlErrores.Visible = False
				Me.lblErroresPresentados.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Elimin tabla CatalogoInventarioTeorico_sociedad
	''' </summary>
	Private Sub BorrarTabla()
		Dim ds As New DataSet
		Dim par(0) As SqlParameter
		par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))

		ds = DatosSQL.funcioncp("CatalogoInventarioTeorico_Administracion", par)
	End Sub
End Class