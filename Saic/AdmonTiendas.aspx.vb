﻿Imports System.Data.SqlClient

Public Class AdmonTiendas
	Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As Button
    Protected WithEvents btnConfirmNO As Button

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			If Session("IdUsuario") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If
			Me.wucMessageAdmonTiendas.Visible = False

			If Not Page.IsPostBack Then
				Llenar_Tiendas()
				Sociedades_Buscar()
				UnidadNegocio_Buscar()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub lnkAgregar_Click(sender As Object, e As EventArgs) Handles lnkAgregar.Click
		Me.lblTitulo.Text = "Agregar tienda"
		Me.TblAgregar_Actualizar.Visible = True
		Me.btnAgregar.Visible = True
		Me.btnCancelar.Visible = True
		Me.btnActualizar.Visible = False
		Me.btnBaja.Visible = False
		Me.txtIdTienda.ReadOnly = False
		Me.TblGridTienda.Visible = False
	End Sub

	Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
		Tiendas_Administracion(1)
		Llenar_Tiendas()
	End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Tiendas_Administracion(2)
    End Sub

    Public Sub limpiarDatos(ByVal Accion As String)
        If (Accion = 1) Then
            Me.txtIdTienda.Text = ""
            Me.txtNombreTienda.Text = ""
            Me.cmbUnidadNegocio.SelectedValue = -1
            Me.txtUbicacion.Text = ""
            Me.cboSociedad.SelectedValue = -1
            Me.chkCedis.Checked = False
            Me.txtIP.Text = ""
            Me.TblAgregar_Actualizar.Visible = False
            Me.TblGridTienda.Visible = True
        ElseIf (Accion = 2) Then
            Me.txtIdTienda.Text = ""
            Me.txtNombreTienda.Text = ""
            Me.cmbUnidadNegocio.SelectedValue = -1
            Me.txtUbicacion.Text = ""
            Me.cboSociedad.SelectedValue = -1
            Me.chkCedis.Checked = False
            Me.txtIP.Text = ""
            Me.TblAgregar_Actualizar.Visible = False
            Me.TblGridTienda.Visible = True
        ElseIf (Accion = 3) Then
            Me.txtIdTienda.Text = ""
            Me.txtNombreTienda.Text = ""
            Me.cmbUnidadNegocio.SelectedValue = -1
            Me.txtUbicacion.Text = ""
            Me.cboSociedad.SelectedValue = -1
            Me.chkCedis.Checked = False
            Me.txtIP.Text = ""
            Me.TblAgregar_Actualizar.Visible = False
            Me.TblGridTienda.Visible = True
        End If

    End Sub

#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

    Sub cargaWinConfirm()
        winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
        Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
        txtTitutlo.Text = "Confirmar"
        Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
        txtCuerpo.Text = ""


        btnConfirmNO = winConfirm1.FindControl("BtnNO")
        btnConfirmNO.Text = "No"
        AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

        btnConfirmSI = winConfirm1.FindControl("btnSI")
        btnConfirmSI.Text = "Sí"
        AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

        phConfirm.Controls.Add(winConfirm1)
    End Sub
#End Region

    Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
        'BajaTienda(Me.txtIdTienda.Text.Trim)
        'Llenar_Tiendas()
        Tiendas_Administracion(3)
    End Sub
    Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.winConfirm1.OcultaDialogo()
        Response.Redirect("AdmonTiendas.aspx?SID=" & Session("SID"), False)
    End Sub


    Protected Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
        Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
        lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar la tienda?</div>"
        winConfirm1.MuestraDialogo()
    End Sub

	Protected Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.txtIdTienda.Text = ""
        Me.txtNombreTienda.Text = ""
		Me.cmbUnidadNegocio.SelectedValue = -1
		Me.txtUbicacion.Text = ""
		Me.cboSociedad.SelectedValue = -1
		Me.chkCedis.Checked = False
		Me.txtIP.Text = ""
		Me.TblAgregar_Actualizar.Visible = False
		Me.TblGridTienda.Visible = True
	End Sub

    Private Sub Llenar_Tiendas()
        Try
            Dim ds As New DataSet
            ds = DatosSQL.FuncionSP("Tiendas_Buscar")
            If Not ds Is Nothing Then
                Me.GridTiendas.DataSource = ds.Tables(0)
                Me.GridTiendas.DataBind()
                Me.TblGridTienda.Visible = True
            Else
                Me.GridTiendas.DataSource = Nothing
                Me.GridTiendas.DataBind()
                Me.TblGridTienda.Visible = True
                CMensajes.MostrarMensaje("No se encontró ninguna tienda.", CMensajes.Tipo.GetError, wucMessageAdmonTiendas)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Sub

    Private Sub GridTiendas_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridTiendas.RowCommand
		Dim iRow As Integer
		iRow = Integer.Parse(IIf(e.CommandArgument.ToString = "", 0, e.CommandArgument.ToString))

		If e.CommandName = "IdTienda" Then
			Dim idTienda As String = Me.GridTiendas.DataKeys(iRow)("IdTienda")

			Me.lblTitulo.Text = "Modificar datos de tienda"
			Me.TblAgregar_Actualizar.Visible = True
			Me.btnActualizar.Visible = True
			Me.btnBaja.Visible = True
			Me.btnCancelar.Visible = True
			Me.btnAgregar.Visible = False
			Me.txtIdTienda.ReadOnly = True
			Me.txtIdTienda.Text = idTienda
			Me.Tienda_Datos(idTienda)
			Me.TblGridTienda.Visible = False
		End If
	End Sub

	''' <summary>
	''' Llena combo de sociedades
	''' </summary>
	Sub Sociedades_Buscar()
		Try
			Dim ds As New DataTable
			ds = DatosSQL.FuncionSPDataTable("Sociedades_Buscar")

			Dim dr As DataRow
			dr = ds.NewRow
			dr("IdSociedad") = -1
			dr("DescripcionSociedad") = "[Seleccione]"
			ds.Rows.InsertAt(dr, 0)

			Me.cboSociedad.DataSource = ds
			Me.cboSociedad.DataTextField = "DescripcionSociedad"
			Me.cboSociedad.DataValueField = "IdSociedad"
			Me.cboSociedad.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena combo de unidad de negocio
	''' </summary>
	Sub UnidadNegocio_Buscar()
		Try
			Dim ds As New DataTable
			ds = DatosSQL.FuncionSPDataTable("UnidadNegocio_Buscar")

			Dim dr As DataRow
			dr = ds.NewRow
			dr("Id") = -1
			dr("UnidadNegocio") = "[Seleccione]"
			ds.Rows.InsertAt(dr, 0)

			Me.cmbUnidadNegocio.DataSource = ds
			Me.cmbUnidadNegocio.DataTextField = "UnidadNegocio"
			Me.cmbUnidadNegocio.DataValueField = "Id"
			Me.cmbUnidadNegocio.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Llena controles con la información de la tienda seleccionada
	''' </summary>
	''' <param name="idTienda"></param>
	Sub Tienda_Datos(ByVal idTienda As String)
		Try
			Dim ds As New DataSet
			Dim PAR(0) As SqlParameter
			PAR(0) = New SqlParameter("@IdTienda", idTienda)
			ds = DatosSQL.funcioncp("Tiendas_Buscar", PAR)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				'IdTienda,NombreTienda,Ubicacion,T.IdSociedad
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreTienda")) = False Then
					Me.txtNombreTienda.Text = ds.Tables(0).Rows(0).Item("NombreTienda")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("UnidadNegocio")) = False Then
					Me.cmbUnidadNegocio.SelectedValue = ds.Tables(0).Rows(0).Item("UnidadNegocio")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("Ubicacion")) = False Then
					Me.txtUbicacion.Text = ds.Tables(0).Rows(0).Item("Ubicacion")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("IdSociedad")) = False Then
					Me.cboSociedad.SelectedValue = ds.Tables(0).Rows(0).Item("IdSociedad")
				End If
				If ds.Tables(0).Rows(0).Item("Cedis") = 0 Then 'No
					Me.chkCedis.Checked = False
				ElseIf ds.Tables(0).Rows(0).Item("Cedis") = 1 Then 'SI
					Me.chkCedis.Checked = True
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("IP")) = False Then
					Me.txtIP.Text = ds.Tables(0).Rows(0).Item("IP")
				End If
			Else
				Me.btnActualizar.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Elimina la tienda seleccionada
	''' </summary>
	''' <param name="id"></param>
	Sub BajaTienda(ByVal id As String)
		Try

			'@Accion as int,--1=Agregar 2=Modificar 3=Eliminar 4=Cambiar Password
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@Accion", 3)
			par(1) = New SqlParameter("@IdTienda", id)

			DatosSQL.funcioncp("Tiendas_Administracion", par)

			Me.TblAgregar_Actualizar.Visible = False
			Me.TblGridTienda.Visible = True

			CMensajes.MostrarMensaje("Tienda " & id & " eliminada correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonTiendas)
		Catch ex As SqlException
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Tiendas_Administracion(ByVal Accion As Integer)
        Try
			If String.IsNullOrEmpty(Me.txtIdTienda.Text) OrElse String.IsNullOrWhiteSpace(Me.txtIdTienda.Text) Then
				CMensajes.MostrarMensaje("Ingrese el Id de tienda.", CMensajes.Tipo.GetError, wucMessageAdmonTiendas)
			ElseIf String.IsNullOrEmpty(Me.txtNombreTienda.Text) OrElse String.IsNullOrWhiteSpace(Me.txtNombreTienda.Text) Then
				CMensajes.MostrarMensaje("Ingrese el nombre de la tienda.", CMensajes.Tipo.GetError, wucMessageAdmonTiendas)
			ElseIf String.IsNullOrEmpty(Me.txtUbicacion.Text) OrElse String.IsNullOrWhiteSpace(Me.txtUbicacion.Text) Then
				CMensajes.MostrarMensaje("Ingrese la ubicación de la tienda.", CMensajes.Tipo.GetError, wucMessageAdmonTiendas)
			ElseIf Me.cmbUnidadNegocio.SelectedValue = "-1" Then
				CMensajes.MostrarMensaje("Selecione la unidad de negocio.", CMensajes.Tipo.GetError, wucMessageAdmonTiendas)
			ElseIf Me.cboSociedad.SelectedValue = "-1" Then
				CMensajes.MostrarMensaje("Selecione la sociedad.", CMensajes.Tipo.GetError, wucMessageAdmonTiendas)
			Else
				Dim cedis As Integer = 0
				Dim ds As New DataSet

				Dim par(7) As SqlParameter
				par(0) = New SqlParameter("@Accion", Accion)
				par(1) = New SqlParameter("@IdTienda", Me.txtIdTienda.Text.ToUpper.Trim)
				par(2) = New SqlParameter("@NombreTienda", Me.txtNombreTienda.Text.Trim)
				par(3) = New SqlParameter("@Ubicacion", Me.txtUbicacion.Text.Trim)
				par(4) = New SqlParameter("@IdSociedad", Me.cboSociedad.SelectedValue)

				If Me.chkCedis.Checked = True Then
					cedis = 1
				End If

				par(5) = New SqlParameter("@Cedis", cedis)
				par(6) = New SqlParameter("@IP", Me.txtIP.Text.Trim)
				par(7) = New SqlParameter("@UnidadNegocio", Me.cmbUnidadNegocio.SelectedValue.Trim)

				ds = DatosSQL.funcioncp("Tiendas_Administracion", par)

				If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Else
					limpiarDatos(Accion)
					Llenar_Tiendas()

					Select Case Accion
						Case 1 '--NUEVO
							Me.txtIdTienda.Text = ""
							Me.txtNombreTienda.Text = ""
							Me.txtUbicacion.Text = ""
							Me.cmbUnidadNegocio.SelectedValue = -1
							Me.cboSociedad.SelectedValue = -1
							Me.chkCedis.Checked = False
							Me.txtIP.Text = ""
							CMensajes.MostrarMensaje("Tienda agregada correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonTiendas)
						Case 2 '--MODIFICAR
							CMensajes.MostrarMensaje("Tienda modificada correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonTiendas)
						Case 3 '--ELIMINAR
							CMensajes.MostrarMensaje("Tienda Eliminada correctamente.", CMensajes.Tipo.GetExito, wucMessageAdmonTiendas)
					End Select
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

End Class