﻿Imports System.IO

Public Class RPT_CargaProvExt
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageRPT_CargaProvExt.Visible = False

		Try
			Select Case Request.QueryString("Tipo")
				Case 1
					lblTitulo.Text = "CheckList Locura"
				Case 2
					lblTitulo.Text = "Evaluación Locura"
				Case 3
					lblTitulo.Text = "Inv Prov Externo"
				Case 4
					lblTitulo.Text = "Inv Operaciones"
				Case 5
					lblTitulo.Text = "Incidencias"
			End Select
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub ConsultarInfo()
		Dim Archivo As String
		Dim TitulosArchivo As String

		Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes")

		If Not Directory.Exists(DirectorioArchivos) Then
			Directory.CreateDirectory(DirectorioArchivos)
		End If

		Select Case Request.QueryString("Tipo")
			Case 1 'CheckList
				Dim CHL As New CCheckList
				If CHL.CONSULTA Then
					Throw New Exception(CHL.ErrorMensaje)
				Else
					If CHL.ValidarDatos Then
						Archivo = DirectorioArchivos & "RPTCheckList_" & Now.ToString("yyyyMMdd") & ".xls"
						TitulosArchivo = "Reporte CheckList"
						REPEXCEL(CHL.dsRespuesta.Tables(0), Archivo, TitulosArchivo)
					Else
						CMensajes.MostrarMensaje("No se encntraron datos.", CMensajes.Tipo.GetError, wucMessageRPT_CargaProvExt)
					End If
				End If
			Case 2 'Evaluacion
				Dim EVL As New CEvaluacion
				If EVL.CONSULTA Then
					Throw New Exception(EVL.ErrorMensaje)
				Else
					If EVL.ValidarDatos Then
						Archivo = DirectorioArchivos & "RPTEvaluacion_" & Now.ToString("yyyyMMdd") & ".xls"
						TitulosArchivo = "Reporte Evaluacion"
						REPEXCEL(EVL.dsRespuesta.Tables(0), Archivo, TitulosArchivo)
					Else
						CMensajes.MostrarMensaje("No se encntraron datos.", CMensajes.Tipo.GetError, wucMessageRPT_CargaProvExt)
					End If
				End If
			Case 3 'Inv Prov Externo
				Dim IEXT As New CInv_externo
				If IEXT.CONSULTA Then
					Throw New Exception(IEXT.ErrorMensaje)
				Else
					If IEXT.ValidarDatos Then
						Archivo = DirectorioArchivos & "RPTInvProvExterno_" & Now.ToString("yyyyMMdd") & ".xls"
						TitulosArchivo = "Reporte Inv Prov Externo"
						REPEXCEL(IEXT.dsRespuesta.Tables(0), Archivo, TitulosArchivo)
					Else
						CMensajes.MostrarMensaje("No se encntraron datos.", CMensajes.Tipo.GetError, wucMessageRPT_CargaProvExt)
					End If
				End If
			Case 4 'Inv Operaciones
				Dim IOPER As New CInv_oper
				If IOPER.CONSULTA Then
					Throw New Exception(IOPER.ErrorMensaje)
				Else
					If IOPER.ValidarDatos Then
						Archivo = DirectorioArchivos & "RPTInvOperaciones_" & Now.ToString("yyyyMMdd") & ".xls"
						TitulosArchivo = "Reporte Inv Operaciones"
						REPEXCEL(IOPER.dsRespuesta.Tables(0), Archivo, TitulosArchivo)
					Else
						CMensajes.MostrarMensaje("No se encntraron datos.", CMensajes.Tipo.GetError, wucMessageRPT_CargaProvExt)
					End If
				End If
			Case 5 'Incidencias
				Dim INC As New CIncidencias
				If INC.CONSULTA Then
					Throw New Exception(INC.ErrorMensaje)
				Else
					If INC.ValidarDatos Then
						Archivo = DirectorioArchivos & "RPTIncidencias_" & Now.ToString("yyyyMMdd") & ".xls"
						TitulosArchivo = "Reporte Incidencias"
						REPEXCEL(INC.dsRespuesta.Tables(0), Archivo, TitulosArchivo)
					Else
						CMensajes.MostrarMensaje("No se encntraron datos.", CMensajes.Tipo.GetError, wucMessageRPT_CargaProvExt)
					End If
				End If
		End Select

	End Sub

	Sub REPEXCEL(ByVal DATOS As DataTable, Archivo As String, TitulosArchivo As String)
		Try
			Dim Subtitulo As String
			Subtitulo = "Fecha: " & Now.ToShortDateString & "    Hora: " & Now.ToShortTimeString

			'Guardar la información en el archivo txt
			'Reportes.Exportar_ExcelDinamico(DATOS, Archivo, TitulosArchivo, Subtitulo)
			Reportes.Exportar_Excel(DATOS, Archivo, TitulosArchivo, Subtitulo)
			HyperLink1.Visible = True
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
		Try
			ConsultarInfo()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class