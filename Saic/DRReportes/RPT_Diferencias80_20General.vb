Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_Diferencias80_20General
    Inherits dbAutoTrack.DataReports.Design.dbATReport
    Dim pageno As Integer = 1
    Dim conteo As Integer = 1
    Dim totalpaginas As Integer = 0
    Dim ContID As Integer = 0
#Region " Component Designer generated code "

    Public Sub New(Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the DataReport Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the DataReport Designer
    Private components As System.ComponentModel.IContainer
   
    'NOTE: The following procedure is required by the DataReport Designer
    'It can be modified using the DataReport Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
    Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox14 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox15 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox18 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox19 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
    Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
    Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtalmacen As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel38 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel40 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel41 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel42 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel45 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLine14 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLabel46 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATShape1 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox10 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATShape2 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATPageBreak1 As dbAutoTrack.DataReports.dbATPageBreak
    Private WithEvents txtcbarras As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
        Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel46 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLine14 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATLabel45 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel42 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel41 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel40 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel38 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
        Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
        Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel()
        Me.Detail = New dbAutoTrack.DataReports.Detail()
        Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox19 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox18 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox15 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox14 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.txtcbarras = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
        Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader()
        Me.DbATTextBox10 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATPageBreak1 = New dbAutoTrack.DataReports.dbATPageBreak()
        Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtalmacen = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATShape1 = New dbAutoTrack.DataReports.dbATShape()
        Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATShape2 = New dbAutoTrack.DataReports.dbATShape()
        Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel7, Me.DbATLabel6, Me.DbATLabel10, Me.DbATLabel5, Me.DbATLabel46, Me.DbATLine14, Me.DbATLabel45, Me.DbATLabel42, Me.DbATLabel41, Me.DbATLabel40, Me.DbATLabel38, Me.DbATLabel9, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLabel3})
        Me.PageHeader.Height = 108.0!
        Me.PageHeader.Name = "PageHeader"
        '
        'DbATLabel7
        '
        Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel7.Height = 18.0!
        Me.DbATLabel7.Left = 828.0!
        Me.DbATLabel7.Name = "DbATLabel7"
        Me.DbATLabel7.Parent = Me.PageHeader
        Me.DbATLabel7.Text = "PROVEEDOR"
        Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel7.Top = 72.0!
        Me.DbATLabel7.Width = 125.952!
        '
        'DbATLabel6
        '
        Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel6.Height = 18.0!
        Me.DbATLabel6.Left = 997.2!
        Me.DbATLabel6.Name = "DbATLabel6"
        Me.DbATLabel6.Parent = Me.PageHeader
        Me.DbATLabel6.Text = "ID"
        Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel6.Top = 72.0!
        Me.DbATLabel6.Width = 36.0!
        '
        'DbATLabel10
        '
        Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel10.Height = 18.0!
        Me.DbATLabel10.Left = 138.0!
        Me.DbATLabel10.Name = "DbATLabel10"
        Me.DbATLabel10.Parent = Me.PageHeader
        Me.DbATLabel10.Text = "DESCRIPCION"
        Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel10.Top = 72.0!
        Me.DbATLabel10.Width = 162.0!
        '
        'DbATLabel5
        '
        Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel5.Height = 18.0!
        Me.DbATLabel5.Left = 630.0!
        Me.DbATLabel5.Name = "DbATLabel5"
        Me.DbATLabel5.Parent = Me.PageHeader
        Me.DbATLabel5.Text = "DIF VENTA"
        Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel5.Top = 72.0!
        Me.DbATLabel5.Width = 71.952!
        '
        'DbATLabel46
        '
        Me.DbATLabel46.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel46.Height = 18.0!
        Me.DbATLabel46.Left = 714.0!
        Me.DbATLabel46.Name = "DbATLabel46"
        Me.DbATLabel46.Parent = Me.PageHeader
        Me.DbATLabel46.Text = "ACUMULADO"
        Me.DbATLabel46.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATLabel46.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel46.Top = 72.0!
        Me.DbATLabel46.Width = 71.904!
        '
        'DbATLine14
        '
        Me.DbATLine14.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine14.LineWeight = 1.0!
        Me.DbATLine14.Name = "DbATLine14"
        Me.DbATLine14.Parent = Me.PageHeader
        Me.DbATLine14.X1 = 1038.0!
        Me.DbATLine14.X2 = 6.0!
        Me.DbATLine14.Y1 = 90.0!
        Me.DbATLine14.Y2 = 90.0!
        '
        'DbATLabel45
        '
        Me.DbATLabel45.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel45.Height = 18.0!
        Me.DbATLabel45.Left = 546.0!
        Me.DbATLabel45.Name = "DbATLabel45"
        Me.DbATLabel45.Parent = Me.PageHeader
        Me.DbATLabel45.Text = "DIF COSTO"
        Me.DbATLabel45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel45.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel45.Top = 72.0!
        Me.DbATLabel45.Width = 71.952!
        '
        'DbATLabel42
        '
        Me.DbATLabel42.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel42.Height = 18.0!
        Me.DbATLabel42.Left = 468.0!
        Me.DbATLabel42.Name = "DbATLabel42"
        Me.DbATLabel42.Parent = Me.PageHeader
        Me.DbATLabel42.Text = "DIFERENCIA"
        Me.DbATLabel42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel42.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATLabel42.Top = 72.0!
        Me.DbATLabel42.Width = 59.904!
        '
        'DbATLabel41
        '
        Me.DbATLabel41.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel41.Height = 18.0!
        Me.DbATLabel41.Left = 390.0!
        Me.DbATLabel41.Name = "DbATLabel41"
        Me.DbATLabel41.Parent = Me.PageHeader
        Me.DbATLabel41.Text = "FISICO"
        Me.DbATLabel41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel41.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel41.Top = 72.0!
        Me.DbATLabel41.Width = 59.904!
        '
        'DbATLabel40
        '
        Me.DbATLabel40.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel40.Height = 18.0!
        Me.DbATLabel40.Left = 318.0!
        Me.DbATLabel40.Name = "DbATLabel40"
        Me.DbATLabel40.Parent = Me.PageHeader
        Me.DbATLabel40.Text = "TEORICO"
        Me.DbATLabel40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel40.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel40.Top = 72.0!
        Me.DbATLabel40.Width = 59.904!
        '
        'DbATLabel38
        '
        Me.DbATLabel38.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel38.Height = 18.0!
        Me.DbATLabel38.Left = 18.0!
        Me.DbATLabel38.Name = "DbATLabel38"
        Me.DbATLabel38.Parent = Me.PageHeader
        Me.DbATLabel38.Text = "CBARRAS"
        Me.DbATLabel38.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel38.Top = 72.0!
        Me.DbATLabel38.Width = 72.096!
        '
        'DbATLabel9
        '
        Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel9.Height = 18.0!
        Me.DbATLabel9.Left = 36.0!
        Me.DbATLabel9.Name = "DbATLabel9"
        Me.DbATLabel9.Parent = Me.PageHeader
        Me.DbATLabel9.Text = "FECHA:"
        Me.DbATLabel9.Top = 42.0!
        Me.DbATLabel9.Width = 48.0!
        '
        'DbATLabel2
        '
        Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel2.Height = 18.0!
        Me.DbATLabel2.Left = 318.0!
        Me.DbATLabel2.Name = "DbATLabel2"
        Me.DbATLabel2.Parent = Me.PageHeader
        Me.DbATLabel2.Text = "TIENDA:"
        Me.DbATLabel2.Top = 42.0!
        Me.DbATLabel2.Width = 54.0!
        '
        'DbATLabel1
        '
        Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel1.Height = 18.0!
        Me.DbATLabel1.Left = 30.0!
        Me.DbATLabel1.Name = "DbATLabel1"
        Me.DbATLabel1.Parent = Me.PageHeader
        Me.DbATLabel1.Text = "REPORTE DE DIFERENCIAS 80-20 GENERAL"
        Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel1.Top = 6.0!
        Me.DbATLabel1.Width = 1002.0!
        '
        'txtFecha
        '
        Me.txtFecha.DataField = "Fecha"
        Me.txtFecha.Height = 18.0!
        Me.txtFecha.Left = 90.0!
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Parent = Me.PageHeader
        Me.txtFecha.Top = 42.0!
        Me.txtFecha.Width = 72.0!
        '
        'txtTienda
        '
        Me.txtTienda.DataField = "Tienda"
        Me.txtTienda.Height = 18.0!
        Me.txtTienda.Left = 372.0!
        Me.txtTienda.Name = "txtTienda"
        Me.txtTienda.OutputFormat = "h:mm AM/PM"
        Me.txtTienda.Parent = Me.PageHeader
        Me.txtTienda.Top = 42.0!
        Me.txtTienda.Width = 270.0!
        '
        'DbATLabel4
        '
        Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel4.Height = 18.0!
        Me.DbATLabel4.Left = 186.0!
        Me.DbATLabel4.Name = "DbATLabel4"
        Me.DbATLabel4.Parent = Me.PageHeader
        Me.DbATLabel4.Text = "HORA:"
        Me.DbATLabel4.Top = 42.0!
        Me.DbATLabel4.Width = 48.0!
        '
        'txtHora
        '
        Me.txtHora.DataField = "Hora"
        Me.txtHora.Height = 18.0!
        Me.txtHora.Left = 234.0!
        Me.txtHora.Name = "txtHora"
        Me.txtHora.OutputFormat = "h:mm AM/PM"
        Me.txtHora.Parent = Me.PageHeader
        Me.txtHora.Top = 42.0!
        Me.txtHora.Width = 72.0!
        '
        'lblPage
        '
        Me.lblPage.ForeColor = System.Drawing.Color.Black
        Me.lblPage.Height = 18.0!
        Me.lblPage.Left = 936.0!
        Me.lblPage.Name = "lblPage"
        Me.lblPage.Parent = Me.PageHeader
        Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPage.Top = 36.0!
        Me.lblPage.Width = 96.0!
        '
        'DbATLabel3
        '
        Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel3.Height = 18.0!
        Me.DbATLabel3.Left = 360.0!
        Me.DbATLabel3.Name = "DbATLabel3"
        Me.DbATLabel3.Parent = Me.PageHeader
        Me.DbATLabel3.Text = "*PRECIOS VENTA VIGENTES EN SAP*"
        Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel3.Top = 2.879999!
        Me.DbATLabel3.Width = 1002.0!
        '
        'Detail
        '
        Me.Detail.CanGrow = False
        Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox5, Me.DbATTextBox2, Me.DbATTextBox1, Me.DbATTextBox19, Me.DbATTextBox18, Me.DbATTextBox15, Me.DbATTextBox14, Me.txtcbarras, Me.DbATTextBox3, Me.DbATTextBox4})
        Me.Detail.Height = 23.0!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'DbATTextBox5
        '
        Me.DbATTextBox5.CanGrow = False
        Me.DbATTextBox5.DataField = "OBS"
        Me.DbATTextBox5.Height = 18.0!
        Me.DbATTextBox5.Left = 90.0!
        Me.DbATTextBox5.Name = "DbATTextBox5"
        Me.DbATTextBox5.Parent = Me.Detail
        Me.DbATTextBox5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox5.Top = 0.0!
        Me.DbATTextBox5.Width = 222.0!
        '
        'DbATTextBox2
        '
        Me.DbATTextBox2.DataField = "PROVEEDOR"
        Me.DbATTextBox2.Height = 18.048!
        Me.DbATTextBox2.Left = 792.0!
        Me.DbATTextBox2.Name = "DbATTextBox2"
        Me.DbATTextBox2.OutputFormat = ""
        Me.DbATTextBox2.Parent = Me.Detail
        Me.DbATTextBox2.Text = ""
        Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox2.Top = 0.0!
        Me.DbATTextBox2.Width = 191.952!
        '
        'DbATTextBox1
        '
        Me.DbATTextBox1.DataField = "DIFERENCIA"
        Me.DbATTextBox1.Height = 18.0!
        Me.DbATTextBox1.Left = 468.048!
        Me.DbATTextBox1.Name = "DbATTextBox1"
        Me.DbATTextBox1.OutputFormat = "#,##0.00"
        Me.DbATTextBox1.Parent = Me.Detail
        Me.DbATTextBox1.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATTextBox1.Top = 0.0!
        Me.DbATTextBox1.Width = 59.904!
        '
        'DbATTextBox19
        '
        Me.DbATTextBox19.DataField = "DIFVENTA"
        Me.DbATTextBox19.Height = 18.0!
        Me.DbATTextBox19.Left = 630.0!
        Me.DbATTextBox19.Name = "DbATTextBox19"
        Me.DbATTextBox19.OutputFormat = "#,##0.00"
        Me.DbATTextBox19.Parent = Me.Detail
        Me.DbATTextBox19.Text = ""
        Me.DbATTextBox19.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox19.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox19.Top = 0.0!
        Me.DbATTextBox19.Width = 71.904!
        '
        'DbATTextBox18
        '
        Me.DbATTextBox18.DataField = "DIFCOSTO"
        Me.DbATTextBox18.Height = 18.0!
        Me.DbATTextBox18.Left = 546.048!
        Me.DbATTextBox18.Name = "DbATTextBox18"
        Me.DbATTextBox18.OutputFormat = "#,##0.00"
        Me.DbATTextBox18.Parent = Me.Detail
        Me.DbATTextBox18.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox18.Top = 0.0!
        Me.DbATTextBox18.Width = 71.904!
        '
        'DbATTextBox15
        '
        Me.DbATTextBox15.CanShrink = True
        Me.DbATTextBox15.DataField = "FISICO"
        Me.DbATTextBox15.Height = 18.0!
        Me.DbATTextBox15.Left = 396.048!
        Me.DbATTextBox15.Name = "DbATTextBox15"
        Me.DbATTextBox15.OutputFormat = "#,##0.00"
        Me.DbATTextBox15.Parent = Me.Detail
        Me.DbATTextBox15.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox15.Top = 0.0!
        Me.DbATTextBox15.Width = 59.904!
        '
        'DbATTextBox14
        '
        Me.DbATTextBox14.DataField = "ACUMULADO"
        Me.DbATTextBox14.Height = 18.0!
        Me.DbATTextBox14.Left = 714.0!
        Me.DbATTextBox14.Name = "DbATTextBox14"
        Me.DbATTextBox14.OutputFormat = "0.00%"
        Me.DbATTextBox14.Parent = Me.Detail
        Me.DbATTextBox14.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox14.Top = 0.0!
        Me.DbATTextBox14.Width = 71.904!
        '
        'txtcbarras
        '
        Me.txtcbarras.CanGrow = False
        Me.txtcbarras.DataField = "CBARRAS"
        Me.txtcbarras.Height = 18.0!
        Me.txtcbarras.Left = 12.048!
        Me.txtcbarras.Name = "txtcbarras"
        Me.txtcbarras.Parent = Me.Detail
        Me.txtcbarras.Text = ""
        Me.txtcbarras.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.txtcbarras.Top = 0.0!
        Me.txtcbarras.Width = 72.096!
        '
        'DbATTextBox3
        '
        Me.DbATTextBox3.DataField = ""
        Me.DbATTextBox3.Height = 18.0!
        Me.DbATTextBox3.Left = 984.0!
        Me.DbATTextBox3.Name = "DbATTextBox3"
        Me.DbATTextBox3.OutputFormat = ""
        Me.DbATTextBox3.Parent = Me.Detail
        Me.DbATTextBox3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATTextBox3.Top = 0.0!
        Me.DbATTextBox3.Width = 54.0!
        '
        'DbATTextBox4
        '
        Me.DbATTextBox4.CanGrow = False
        Me.DbATTextBox4.DataField = "Teorico"
        Me.DbATTextBox4.Height = 18.0!
        Me.DbATTextBox4.Left = 318.0!
        Me.DbATTextBox4.Name = "DbATTextBox4"
        Me.DbATTextBox4.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox4.Parent = Me.Detail
        Me.DbATTextBox4.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox4.Top = 0.0!
        Me.DbATTextBox4.Width = 60.0!
        '
        'PageFooter
        '
        Me.PageFooter.CanGrow = False
        Me.PageFooter.Height = 23.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.CanGrow = False
        Me.GroupHeader1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox10, Me.DbATPageBreak1, Me.DbATLabel8, Me.txtalmacen, Me.DbATShape1, Me.DbATLabel11, Me.DbATShape2})
        Me.GroupHeader1.Height = 41.0!
        Me.GroupHeader1.Index = 1
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.NewPage = dbAutoTrack.DataReports.NewPage.Before
        '
        'DbATTextBox10
        '
        Me.DbATTextBox10.CanGrow = False
        Me.DbATTextBox10.DataField = "CODE_DEPTO"
        Me.DbATTextBox10.Height = 18.0!
        Me.DbATTextBox10.Left = 221.952!
        Me.DbATTextBox10.Name = "DbATTextBox10"
        Me.DbATTextBox10.Parent = Me.GroupHeader1
        Me.DbATTextBox10.Text = ""
        Me.DbATTextBox10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATTextBox10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox10.Top = 12.0!
        Me.DbATTextBox10.Width = 38.4!
        '
        'DbATPageBreak1
        '
        Me.DbATPageBreak1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.Enabled = False
        Me.DbATPageBreak1.Name = "DbATPageBreak1"
        Me.DbATPageBreak1.Parent = Me.GroupHeader1
        Me.DbATPageBreak1.Top = 6.0!
        Me.DbATPageBreak1.Visible = False
        '
        'DbATLabel8
        '
        Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel8.Height = 18.0!
        Me.DbATLabel8.Left = 12.0!
        Me.DbATLabel8.Name = "DbATLabel8"
        Me.DbATLabel8.Parent = Me.GroupHeader1
        Me.DbATLabel8.Text = "ALMACEN:"
        Me.DbATLabel8.Top = 12.0!
        Me.DbATLabel8.Width = 66.0!
        '
        'txtalmacen
        '
        Me.txtalmacen.DataField = "Almacen"
        Me.txtalmacen.Height = 18.0!
        Me.txtalmacen.Left = 78.0!
        Me.txtalmacen.Name = "txtalmacen"
        Me.txtalmacen.Parent = Me.GroupHeader1
        Me.txtalmacen.Top = 12.0!
        Me.txtalmacen.Width = 96.0!
        '
        'DbATShape1
        '
        Me.DbATShape1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape1.Height = 18.0!
        Me.DbATShape1.Left = 12.0!
        Me.DbATShape1.LineWeight = 1.0!
        Me.DbATShape1.Name = "DbATShape1"
        Me.DbATShape1.Parent = Me.GroupHeader1
        Me.DbATShape1.Top = 12.0!
        Me.DbATShape1.Width = 144.0!
        '
        'DbATLabel11
        '
        Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel11.Height = 12.0!
        Me.DbATLabel11.Left = 174.0!
        Me.DbATLabel11.Name = "DbATLabel11"
        Me.DbATLabel11.Parent = Me.GroupHeader1
        Me.DbATLabel11.Text = "DEPTO:"
        Me.DbATLabel11.Top = 12.0!
        Me.DbATLabel11.Width = 48.0!
        '
        'DbATShape2
        '
        Me.DbATShape2.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape2.Height = 18.0!
        Me.DbATShape2.Left = 174.0!
        Me.DbATShape2.LineWeight = 1.0!
        Me.DbATShape2.Name = "DbATShape2"
        Me.DbATShape2.Parent = Me.GroupHeader1
        Me.DbATShape2.Top = 12.0!
        Me.DbATShape2.Width = 90.0!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.CanGrow = False
        Me.GroupFooter1.Height = 0.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.Visible = False
        '
        'RPT_Diferencias80_20General
        '
        Me.ReportWidth = 1056.0!
        Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
        Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
        Me.pageno = Me.pageno + 1
        totalpaginas = MyBase.MaxPages
    End Sub
    Private Sub Detail_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Initialize
        ContID += 1
        DbATTextBox3.Value = ContID
    End Sub

    Private Sub GroupHeader1_Initialize(sender As Object, e As System.EventArgs) Handles GroupHeader1.Initialize
        'Dim depto As String = DbATTextBox10.Value
        'Dim Alm As String = txtalmacen.Value
        ContID = 0
    End Sub
End Class


