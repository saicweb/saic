Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_ArticulosNoCatalogadosrptCDS
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Private WithEvents DbATLabel6 As dbATLabel
	Private WithEvents DbATLabel12 As dbATLabel
	Private WithEvents DbATTextBox6 As dbATTextBox
	Dim totalpaginas As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents DbATLine2 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
	Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
	Private WithEvents DbATLine3 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents txtTotal As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents lblTotal As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtAlmacen As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
		Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel12 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLine2 = New dbAutoTrack.DataReports.dbATLine()
		Me.txtAlmacen = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel()
		Me.Detail = New dbAutoTrack.DataReports.Detail()
		Me.DbATTextBox6 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine()
		Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
		Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader()
		Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter()
		Me.DbATLine3 = New dbAutoTrack.DataReports.dbATLine()
		Me.txtTotal = New dbAutoTrack.DataReports.dbATTextBox()
		Me.lblTotal = New dbAutoTrack.DataReports.dbATLabel()
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel6, Me.DbATLabel12, Me.DbATLabel3, Me.DbATLabel8, Me.DbATLabel7, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLabel5, Me.DbATLine2, Me.txtAlmacen, Me.DbATLabel10, Me.DbATLabel11})
		Me.PageHeader.Height = 83.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'DbATLabel6
		'
		Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel6.Height = 24.0!
		Me.DbATLabel6.Left = 174.0!
		Me.DbATLabel6.Name = "DbATLabel6"
		Me.DbATLabel6.Parent = Me.PageHeader
		Me.DbATLabel6.Text = "CODIGO DE BARRAS"
		Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel6.Top = 60.0!
		Me.DbATLabel6.Width = 168.0!
		'
		'DbATLabel12
		'
		Me.DbATLabel12.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel12.Height = 24.0!
		Me.DbATLabel12.Left = 318.0!
		Me.DbATLabel12.Name = "DbATLabel12"
		Me.DbATLabel12.Parent = Me.PageHeader
		Me.DbATLabel12.Text = "UBICACION"
		Me.DbATLabel12.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel12.Top = 60.0!
		Me.DbATLabel12.Width = 138.0!
		'
		'DbATLabel3
		'
		Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel3.Height = 24.0!
		Me.DbATLabel3.Left = 446.0!
		Me.DbATLabel3.Name = "DbATLabel3"
		Me.DbATLabel3.Parent = Me.PageHeader
		Me.DbATLabel3.Text = "FISICO"
		Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel3.Top = 60.0!
		Me.DbATLabel3.Width = 72.0!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 72.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.PageHeader
		Me.DbATLabel8.Text = "FECHA:"
		Me.DbATLabel8.Top = 36.0!
		Me.DbATLabel8.Width = 48.0!
		'
		'DbATLabel7
		'
		Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel7.Height = 24.0!
		Me.DbATLabel7.Left = 6.0!
		Me.DbATLabel7.Name = "DbATLabel7"
		Me.DbATLabel7.Parent = Me.PageHeader
		Me.DbATLabel7.Text = "MARBETE"
		Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel7.Top = 60.0!
		Me.DbATLabel7.Width = 168.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 336.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "TIENDA:"
		Me.DbATLabel2.Top = 36.0!
		Me.DbATLabel2.Width = 54.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 24.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "REPORTE DE ARTICULOS NO ENCONTRADOS EN CATALOGO"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel1.Top = 12.0!
		Me.DbATLabel1.Width = 768.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 120.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 36.0!
		Me.txtFecha.Width = 72.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 396.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 36.0!
		Me.txtTienda.Width = 168.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 204.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 36.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtHora
		'
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 252.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 36.0!
		Me.txtHora.Width = 72.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 702.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 36.0!
		Me.lblPage.Width = 72.0!
		'
		'DbATLabel5
		'
		Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel5.Height = 24.0!
		Me.DbATLabel5.Left = 522.0!
		Me.DbATLabel5.Name = "DbATLabel5"
		Me.DbATLabel5.Parent = Me.PageHeader
		Me.DbATLabel5.Text = "CODIGO CORRECTO"
		Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel5.Top = 60.0!
		Me.DbATLabel5.Width = 168.0!
		'
		'DbATLine2
		'
		Me.DbATLine2.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine2.LineWeight = 1.0!
		Me.DbATLine2.Name = "DbATLine2"
		Me.DbATLine2.Parent = Me.PageHeader
		Me.DbATLine2.X1 = 792.0!
		Me.DbATLine2.X2 = 24.0!
		Me.DbATLine2.Y1 = 78.0!
		Me.DbATLine2.Y2 = 78.0!
		'
		'txtAlmacen
		'
		Me.txtAlmacen.Height = 18.0!
		Me.txtAlmacen.Left = 630.0!
		Me.txtAlmacen.Name = "txtAlmacen"
		Me.txtAlmacen.Parent = Me.PageHeader
		Me.txtAlmacen.Top = 36.0!
		Me.txtAlmacen.Width = 66.0!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 18.0!
		Me.DbATLabel10.Left = 570.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.PageHeader
		Me.DbATLabel10.Text = "Almacen:"
		Me.DbATLabel10.Top = 36.0!
		Me.DbATLabel10.Width = 60.0!
		'
		'DbATLabel11
		'
		Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel11.Height = 18.0!
		Me.DbATLabel11.Left = 696.0!
		Me.DbATLabel11.Name = "DbATLabel11"
		Me.DbATLabel11.Parent = Me.PageHeader
		Me.DbATLabel11.Text = "ESTATUS"
		Me.DbATLabel11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel11.Top = 60.0!
		Me.DbATLabel11.Width = 78.0!
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox6, Me.DbATTextBox4, Me.DbATTextBox2, Me.DbATTextBox1, Me.DbATLine1, Me.DbATTextBox3})
		Me.Detail.Height = 24.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATTextBox6
		'
		Me.DbATTextBox6.DataField = "Ubicacion"
		Me.DbATTextBox6.Height = 18.0!
		Me.DbATTextBox6.Left = 320.0!
		Me.DbATTextBox6.Name = "DbATTextBox6"
		Me.DbATTextBox6.OutputFormat = ""
		Me.DbATTextBox6.Parent = Me.Detail
		Me.DbATTextBox6.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox6.Top = 0!
		Me.DbATTextBox6.Width = 132.0!
		'
		'DbATTextBox4
		'
		Me.DbATTextBox4.DataField = "Conteo"
		Me.DbATTextBox4.Height = 18.0!
		Me.DbATTextBox4.Left = 386.0!
		Me.DbATTextBox4.Name = "DbATTextBox4"
		Me.DbATTextBox4.OutputFormat = "#,##0.00"
		Me.DbATTextBox4.Parent = Me.Detail
		Me.DbATTextBox4.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox4.Top = 0!
		Me.DbATTextBox4.Width = 132.0!
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.DataField = "EAN"
		Me.DbATTextBox2.Height = 18.0!
		Me.DbATTextBox2.Left = 174.0!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.Parent = Me.Detail
		Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox2.Top = 0!
		Me.DbATTextBox2.Width = 168.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.DataField = "IdMarbete"
		Me.DbATTextBox1.Height = 18.0!
		Me.DbATTextBox1.Left = 6.0!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.Parent = Me.Detail
		Me.DbATTextBox1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox1.Top = 0!
		Me.DbATTextBox1.Width = 168.0!
		'
		'DbATLine1
		'
		Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine1.LineWeight = 1.0!
		Me.DbATLine1.Name = "DbATLine1"
		Me.DbATLine1.Parent = Me.Detail
		Me.DbATLine1.X1 = 528.0!
		Me.DbATLine1.X2 = 702.0!
		Me.DbATLine1.Y1 = 18.0!
		Me.DbATLine1.Y2 = 18.0!
		'
		'DbATTextBox3
		'
		Me.DbATTextBox3.DataField = "Estatus"
		Me.DbATTextBox3.Height = 18.0!
		Me.DbATTextBox3.Left = 702.0!
		Me.DbATTextBox3.Name = "DbATTextBox3"
		Me.DbATTextBox3.Parent = Me.Detail
		Me.DbATTextBox3.Top = 0!
		Me.DbATTextBox3.Width = 66.0!
		'
		'PageFooter
		'
		Me.PageFooter.Height = 19.0!
		Me.PageFooter.Name = "PageFooter"
		'
		'GroupHeader1
		'
		Me.GroupHeader1.Height = 0!
		Me.GroupHeader1.Index = 1
		Me.GroupHeader1.Name = "GroupHeader1"
		Me.GroupHeader1.Visible = False
		'
		'GroupFooter1
		'
		Me.GroupFooter1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLine3, Me.txtTotal, Me.lblTotal})
		Me.GroupFooter1.Height = 39.0!
		Me.GroupFooter1.KeepTogether = True
		Me.GroupFooter1.Name = "GroupFooter1"
		'
		'DbATLine3
		'
		Me.DbATLine3.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine3.LineWeight = 1.0!
		Me.DbATLine3.Name = "DbATLine3"
		Me.DbATLine3.Parent = Me.GroupFooter1
		Me.DbATLine3.X1 = 792.0!
		Me.DbATLine3.X2 = 24.0!
		Me.DbATLine3.Y1 = 6.0!
		Me.DbATLine3.Y2 = 6.0!
		'
		'txtTotal
		'
		Me.txtTotal.DataField = ""
		Me.txtTotal.Height = 18.0!
		Me.txtTotal.Left = 462.0!
		Me.txtTotal.Name = "txtTotal"
		Me.txtTotal.OutputFormat = "#,##0.00"
		Me.txtTotal.Parent = Me.GroupFooter1
		Me.txtTotal.Top = 6.0!
		Me.txtTotal.Width = 168.0!
		'
		'lblTotal
		'
		Me.lblTotal.ForeColor = System.Drawing.Color.Black
		Me.lblTotal.Height = 18.0!
		Me.lblTotal.Left = 72.0!
		Me.lblTotal.Name = "lblTotal"
		Me.lblTotal.Parent = Me.GroupFooter1
		Me.lblTotal.Text = "TOTAL"
		Me.lblTotal.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.lblTotal.Top = 6.0!
		Me.lblTotal.Width = 114.0!
		'
		'RPT_ArticulosNoCatalogadosrpt
		'
		Me.ReportWidth = 817.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region
	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub
End Class


