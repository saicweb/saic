Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_MarbetesFaltantesrpt
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
	Dim totalpaginas As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine2 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
		Me.DbATLine2 = New dbAutoTrack.DataReports.dbATLine()
		Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel()
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel()
		Me.Detail = New dbAutoTrack.DataReports.Detail()
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine()
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
		Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox()
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel3, Me.DbATLine2, Me.DbATLabel5, Me.lblPage, Me.txtHora, Me.DbATLabel4, Me.txtTienda, Me.txtFecha, Me.DbATLabel1, Me.DbATLabel2, Me.DbATLabel7, Me.DbATLabel8, Me.DbATLabel10})
		Me.PageHeader.Height = 82.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'DbATLine2
		'
		Me.DbATLine2.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine2.LineWeight = 1.0!
		Me.DbATLine2.Name = "DbATLine2"
		Me.DbATLine2.Parent = Me.PageHeader
		Me.DbATLine2.X1 = 792.0!
		Me.DbATLine2.X2 = 24.0!
		Me.DbATLine2.Y1 = 78.0!
		Me.DbATLine2.Y2 = 78.0!
		'
		'DbATLabel5
		'
		Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel5.Height = 24.0!
		Me.DbATLabel5.Left = 324.0!
		Me.DbATLabel5.Name = "DbATLabel5"
		Me.DbATLabel5.Parent = Me.PageHeader
		Me.DbATLabel5.Text = "OBSERVACIONES"
		Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel5.Top = 60.0!
		Me.DbATLabel5.Width = 486.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 696.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 36.0!
		Me.lblPage.Width = 96.0!
		'
		'txtHora
		'
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 300.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 36.0!
		Me.txtHora.Width = 72.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 252.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 36.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 438.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 36.0!
		Me.txtTienda.Width = 210.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 168.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 36.0!
		Me.txtFecha.Width = 72.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 24.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "REPORTE DE MARBETES FALTANTES Y FUERA DE RANGO (*)"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel1.Top = 12.0!
		Me.DbATLabel1.Width = 768.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 384.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "TIENDA:"
		Me.DbATLabel2.Top = 36.0!
		Me.DbATLabel2.Width = 54.0!
		'
		'DbATLabel7
		'
		Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel7.Height = 24.0!
		Me.DbATLabel7.Left = 24.0!
		Me.DbATLabel7.Name = "DbATLabel7"
		Me.DbATLabel7.Parent = Me.PageHeader
		Me.DbATLabel7.Text = "MARBETE"
		Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel7.Top = 60.0!
		Me.DbATLabel7.Width = 100.0!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 120.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.PageHeader
		Me.DbATLabel8.Text = "FECHA:"
		Me.DbATLabel8.Top = 36.0!
		Me.DbATLabel8.Width = 48.0!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 24.0!
		Me.DbATLabel10.Left = 198.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.PageHeader
		Me.DbATLabel10.Text = "UBICACION"
		Me.DbATLabel10.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel10.Top = 60.0!
		Me.DbATLabel10.Width = 122.0!
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox3, Me.DbATTextBox2, Me.DbATTextBox1, Me.DbATLine1})
		Me.Detail.Height = 29.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.DataField = "Ubicacion"
		Me.DbATTextBox2.Height = 18.0!
		Me.DbATTextBox2.Left = 210.0!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.Parent = Me.Detail
		Me.DbATTextBox2.Text = ""
		Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox2.Top = 6.0!
		Me.DbATTextBox2.Width = 106.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.DataField = "IdMarbete"
		Me.DbATTextBox1.Height = 18.0!
		Me.DbATTextBox1.Left = 30.0!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.Parent = Me.Detail
		Me.DbATTextBox1.Text = ""
		Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox1.Top = 6.0!
		Me.DbATTextBox1.Width = 114.0!
		'
		'DbATLine1
		'
		Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine1.LineWeight = 1.0!
		Me.DbATLine1.Name = "DbATLine1"
		Me.DbATLine1.Parent = Me.Detail
		Me.DbATLine1.X1 = 324.0!
		Me.DbATLine1.X2 = 792.0!
		Me.DbATLine1.Y1 = 24.0!
		Me.DbATLine1.Y2 = 24.0!
		'
		'PageFooter
		'
		Me.PageFooter.Height = 18.0!
		Me.PageFooter.Name = "PageFooter"
		'
		'DbATLabel3
		'
		Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel3.Height = 24.0!
		Me.DbATLabel3.Left = 96.0!
		Me.DbATLabel3.Name = "DbATLabel3"
		Me.DbATLabel3.Parent = Me.PageHeader
		Me.DbATLabel3.Text = "ALMACEN"
		Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel3.Top = 60.0!
		Me.DbATLabel3.Width = 122.0!
		'
		'DbATTextBox3
		'
		Me.DbATTextBox3.DataField = "Almacen"
		Me.DbATTextBox3.Height = 18.0!
		Me.DbATTextBox3.Left = 144.0!
		Me.DbATTextBox3.Name = "DbATTextBox3"
		Me.DbATTextBox3.Parent = Me.Detail
		Me.DbATTextBox3.Text = ""
		Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox3.Top = 6.0!
		Me.DbATTextBox3.Width = 46.0!
		'
		'RPT_MarbetesFaltantes
		'
		Me.ReportWidth = 816.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.Detail, Me.PageFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region
	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub
End Class


