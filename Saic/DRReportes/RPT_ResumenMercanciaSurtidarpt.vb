Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_ResumenMercanciaSurtidarpt
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Dim totalpaginas As Integer = 0
	Private WithEvents txtalmacen As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Dim ContID As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel38 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel40 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel41 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel42 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine14 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtcbarras As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
		Me.txtalmacen = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLine14 = New dbAutoTrack.DataReports.dbATLine()
		Me.DbATLabel42 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel41 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel40 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel38 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
		Me.Detail = New dbAutoTrack.DataReports.Detail()
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.txtcbarras = New dbAutoTrack.DataReports.dbATTextBox()
		Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox()
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.txtalmacen, Me.DbATLabel8, Me.DbATLabel10, Me.DbATLine14, Me.DbATLabel42, Me.DbATLabel41, Me.DbATLabel40, Me.DbATLabel38, Me.DbATLabel9, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage})
		Me.PageHeader.Height = 94.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'txtalmacen
		'
		Me.txtalmacen.DataField = "Almacen"
		Me.txtalmacen.Height = 18.0!
		Me.txtalmacen.Left = 528.0!
		Me.txtalmacen.Name = "txtalmacen"
		Me.txtalmacen.Parent = Me.PageHeader
		Me.txtalmacen.Top = 42.0!
		Me.txtalmacen.Width = 96.0!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 462.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.PageHeader
		Me.DbATLabel8.Text = "ALMACEN:"
		Me.DbATLabel8.Top = 42.0!
		Me.DbATLabel8.Width = 66.0!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 18.0!
		Me.DbATLabel10.Left = 156.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.PageHeader
		Me.DbATLabel10.Text = "CBARRAS"
		Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATLabel10.Top = 78.0!
		Me.DbATLabel10.Width = 66.0!
		'
		'DbATLine14
		'
		Me.DbATLine14.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine14.LineWeight = 1.0!
		Me.DbATLine14.Name = "DbATLine14"
		Me.DbATLine14.Parent = Me.PageHeader
		Me.DbATLine14.X1 = 1038.0!
		Me.DbATLine14.X2 = 6.0!
		Me.DbATLine14.Y1 = 90.0!
		Me.DbATLine14.Y2 = 90.0!
		'
		'DbATLabel42
		'
		Me.DbATLabel42.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel42.Height = 18.0!
		Me.DbATLabel42.Left = 636.0!
		Me.DbATLabel42.Name = "DbATLabel42"
		Me.DbATLabel42.Parent = Me.PageHeader
		Me.DbATLabel42.Text = "CANTIDAD SURTIDA"
		Me.DbATLabel42.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel42.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
		Me.DbATLabel42.Top = 72.0!
		Me.DbATLabel42.Width = 137.904!
		'
		'DbATLabel41
		'
		Me.DbATLabel41.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel41.Height = 18.0!
		Me.DbATLabel41.Left = 462.0!
		Me.DbATLabel41.Name = "DbATLabel41"
		Me.DbATLabel41.Parent = Me.PageHeader
		Me.DbATLabel41.Text = "CONTEO"
		Me.DbATLabel41.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel41.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATLabel41.Top = 72.0!
		Me.DbATLabel41.Width = 137.904!
		'
		'DbATLabel40
		'
		Me.DbATLabel40.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel40.Height = 18.0!
		Me.DbATLabel40.Left = 258.0!
		Me.DbATLabel40.Name = "DbATLabel40"
		Me.DbATLabel40.Parent = Me.PageHeader
		Me.DbATLabel40.Text = "DESCRIPCION"
		Me.DbATLabel40.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel40.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATLabel40.Top = 72.0!
		Me.DbATLabel40.Width = 179.904!
		'
		'DbATLabel38
		'
		Me.DbATLabel38.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel38.Height = 18.0!
		Me.DbATLabel38.Left = 36.0!
		Me.DbATLabel38.Name = "DbATLabel38"
		Me.DbATLabel38.Parent = Me.PageHeader
		Me.DbATLabel38.Text = "MARBETE"
		Me.DbATLabel38.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATLabel38.Top = 72.0!
		Me.DbATLabel38.Width = 96.096!
		'
		'DbATLabel9
		'
		Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel9.Height = 18.0!
		Me.DbATLabel9.Left = 36.0!
		Me.DbATLabel9.Name = "DbATLabel9"
		Me.DbATLabel9.Parent = Me.PageHeader
		Me.DbATLabel9.Text = "FECHA:"
		Me.DbATLabel9.Top = 42.0!
		Me.DbATLabel9.Width = 48.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 318.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "TIENDA:"
		Me.DbATLabel2.Top = 42.0!
		Me.DbATLabel2.Width = 54.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 36.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "Resumen de Mercanc�a Surtida"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel1.Top = 12.0!
		Me.DbATLabel1.Width = 1002.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 90.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 42.0!
		Me.txtFecha.Width = 72.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 372.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 42.0!
		Me.txtTienda.Width = 84.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 186.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 42.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtHora
		'
		Me.txtHora.DataField = "Hora"
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 234.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 42.0!
		Me.txtHora.Width = 72.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 846.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 42.0!
		Me.lblPage.Width = 96.0!
		'
		'Detail
		'
		Me.Detail.CanGrow = False
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox2, Me.DbATTextBox5, Me.DbATTextBox1, Me.txtcbarras, Me.DbATTextBox4})
		Me.Detail.Height = 25.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.CanShrink = True
		Me.DbATTextBox2.DataField = "Cant_Actual"
		Me.DbATTextBox2.Height = 18.0!
		Me.DbATTextBox2.Left = 492.048!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.OutputFormat = "#,##0.00"
		Me.DbATTextBox2.Parent = Me.Detail
		Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
		Me.DbATTextBox2.Top = 0.0!
		Me.DbATTextBox2.Width = 59.904!
		'
		'DbATTextBox5
		'
		Me.DbATTextBox5.CanGrow = False
		Me.DbATTextBox5.DataField = "CBarras"
		Me.DbATTextBox5.Height = 18.0!
		Me.DbATTextBox5.Left = 138.0!
		Me.DbATTextBox5.Name = "DbATTextBox5"
		Me.DbATTextBox5.Parent = Me.Detail
		Me.DbATTextBox5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
		Me.DbATTextBox5.Top = 0.0!
		Me.DbATTextBox5.Width = 90.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.DataField = "Cant_Surtir"
		Me.DbATTextBox1.Height = 18.0!
		Me.DbATTextBox1.Left = 600.048!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.OutputFormat = "#,##0.00"
		Me.DbATTextBox1.Parent = Me.Detail
		Me.DbATTextBox1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox1.Top = 0.0!
		Me.DbATTextBox1.Width = 131.904!
		'
		'txtcbarras
		'
		Me.txtcbarras.CanGrow = False
		Me.txtcbarras.DataField = "Marbete"
		Me.txtcbarras.Height = 18.0!
		Me.txtcbarras.Left = 48.048!
		Me.txtcbarras.Name = "txtcbarras"
		Me.txtcbarras.Parent = Me.Detail
		Me.txtcbarras.Text = ""
		Me.txtcbarras.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
		Me.txtcbarras.Top = 0.0!
		Me.txtcbarras.Width = 72.096!
		'
		'DbATTextBox4
		'
		Me.DbATTextBox4.CanGrow = False
		Me.DbATTextBox4.DataField = "Descripcion"
		Me.DbATTextBox4.Height = 18.0!
		Me.DbATTextBox4.Left = 246.0!
		Me.DbATTextBox4.Name = "DbATTextBox4"
		Me.DbATTextBox4.OutputFormat = "#,##0.00;(#,##0.00)"
		Me.DbATTextBox4.Parent = Me.Detail
		Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
		Me.DbATTextBox4.Top = 0.0!
		Me.DbATTextBox4.Width = 198.0!
		'
		'PageFooter
		'
		Me.PageFooter.CanGrow = False
		Me.PageFooter.Height = 21.0!
		Me.PageFooter.Name = "PageFooter"
		'
		'RPT_ResumenMercanciaSurtida
		'
		Me.ReportWidth = 950.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.Detail, Me.PageFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region

	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub
	Private Sub Detail_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Initialize
		'ContID += 1
		'DbATTextBox3.Value = ContID
	End Sub

	Private Sub GroupHeader1_Initialize(sender As Object, e As System.EventArgs)
		'Dim depto As String = DbATTextBox10.Value
		'Dim Alm As String = txtalmacen.Value
		ContID = 0
	End Sub
End Class


