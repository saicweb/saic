Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_Diferencias80_20AlmacenInvGral
    Inherits dbAutoTrack.DataReports.Design.dbATReport

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the DataReport Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the DataReport Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the DataReport Designer
    'It can be modified using the DataReport Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
    Private WithEvents Detail As dbAutoTrack.DataReports.Detail
    Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
    Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
    Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
    Private WithEvents DbATPageBreak1 As dbAutoTrack.DataReports.dbATPageBreak
    Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtIdAlmacen As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLine4 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATTextBox11 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox12 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox13 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox15 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox18 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel13 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel14 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel15 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel16 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel17 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel18 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel19 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATPageBreak2 As dbAutoTrack.DataReports.dbATPageBreak
    Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox6 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATShape1 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATShape2 As dbAutoTrack.DataReports.dbATShape
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
        Me.DbATLabel19 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel18 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel17 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel16 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel15 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel14 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel13 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
        Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
        Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
        Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel()
        Me.Detail = New dbAutoTrack.DataReports.Detail()
        Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox18 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox15 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox13 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox12 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox11 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLine4 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
        Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader()
        Me.DbATTextBox6 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtIdAlmacen = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATPageBreak2 = New dbAutoTrack.DataReports.dbATPageBreak()
        Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATShape1 = New dbAutoTrack.DataReports.dbATShape()
        Me.DbATShape2 = New dbAutoTrack.DataReports.dbATShape()
        Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter()
        Me.DbATPageBreak1 = New dbAutoTrack.DataReports.dbATPageBreak()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.CanGrow = False
        Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel19, Me.DbATLabel18, Me.DbATLabel17, Me.DbATLabel16, Me.DbATLabel15, Me.DbATLabel14, Me.DbATLabel13, Me.DbATLabel11, Me.DbATLabel4, Me.DbATLine1, Me.DbATLabel7, Me.DbATLabel8, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.txtHora, Me.lblPage, Me.DbATLabel3})
        Me.PageHeader.Height = 94.0!
        Me.PageHeader.Name = "PageHeader"
        '
        'DbATLabel19
        '
        Me.DbATLabel19.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel19.Height = 18.0!
        Me.DbATLabel19.Left = 994.56!
        Me.DbATLabel19.Name = "DbATLabel19"
        Me.DbATLabel19.Parent = Me.PageHeader
        Me.DbATLabel19.Text = "ID"
        Me.DbATLabel19.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel19.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel19.Top = 72.0!
        Me.DbATLabel19.Width = 42.0!
        '
        'DbATLabel18
        '
        Me.DbATLabel18.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel18.Height = 18.0!
        Me.DbATLabel18.Left = 378.0!
        Me.DbATLabel18.Name = "DbATLabel18"
        Me.DbATLabel18.Parent = Me.PageHeader
        Me.DbATLabel18.Text = "TEORICO"
        Me.DbATLabel18.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel18.Top = 72.0!
        Me.DbATLabel18.Width = 77.904!
        '
        'DbATLabel17
        '
        Me.DbATLabel17.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel17.Height = 18.0!
        Me.DbATLabel17.Left = 72.0!
        Me.DbATLabel17.Name = "DbATLabel17"
        Me.DbATLabel17.Parent = Me.PageHeader
        Me.DbATLabel17.Text = "DESCRIPCION"
        Me.DbATLabel17.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel17.Top = 72.0!
        Me.DbATLabel17.Width = 288.048!
        '
        'DbATLabel16
        '
        Me.DbATLabel16.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel16.Height = 18.0!
        Me.DbATLabel16.Left = 0.0!
        Me.DbATLabel16.Name = "DbATLabel16"
        Me.DbATLabel16.Parent = Me.PageHeader
        Me.DbATLabel16.Text = "CBARRAS"
        Me.DbATLabel16.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel16.Top = 72.0!
        Me.DbATLabel16.Width = 66.048!
        '
        'DbATLabel15
        '
        Me.DbATLabel15.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel15.Height = 18.0!
        Me.DbATLabel15.Left = 708.0!
        Me.DbATLabel15.Name = "DbATLabel15"
        Me.DbATLabel15.Parent = Me.PageHeader
        Me.DbATLabel15.Text = "DIF COSTO"
        Me.DbATLabel15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel15.Top = 72.0!
        Me.DbATLabel15.Width = 77.952!
        '
        'DbATLabel14
        '
        Me.DbATLabel14.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel14.Height = 18.0!
        Me.DbATLabel14.Left = 540.0!
        Me.DbATLabel14.Name = "DbATLabel14"
        Me.DbATLabel14.Parent = Me.PageHeader
        Me.DbATLabel14.Text = "DIFERENCIA"
        Me.DbATLabel14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel14.Top = 72.0!
        Me.DbATLabel14.Width = 77.904!
        '
        'DbATLabel13
        '
        Me.DbATLabel13.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel13.Height = 18.0!
        Me.DbATLabel13.Left = 456.0!
        Me.DbATLabel13.Name = "DbATLabel13"
        Me.DbATLabel13.Parent = Me.PageHeader
        Me.DbATLabel13.Text = "FISICO"
        Me.DbATLabel13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel13.Top = 72.0!
        Me.DbATLabel13.Width = 71.904!
        '
        'DbATLabel11
        '
        Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel11.Height = 18.0!
        Me.DbATLabel11.Left = 642.0!
        Me.DbATLabel11.Name = "DbATLabel11"
        Me.DbATLabel11.Parent = Me.PageHeader
        Me.DbATLabel11.Text = "TOTAL"
        Me.DbATLabel11.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel11.Top = 72.0!
        Me.DbATLabel11.Width = 47.952!
        '
        'DbATLabel4
        '
        Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel4.Height = 18.0!
        Me.DbATLabel4.Left = 804.0!
        Me.DbATLabel4.Name = "DbATLabel4"
        Me.DbATLabel4.Parent = Me.PageHeader
        Me.DbATLabel4.Text = "PROVEEDOR"
        Me.DbATLabel4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel4.Top = 72.0!
        Me.DbATLabel4.Width = 179.376!
        '
        'DbATLine1
        '
        Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine1.LineWeight = 1.0!
        Me.DbATLine1.Name = "DbATLine1"
        Me.DbATLine1.Parent = Me.PageHeader
        Me.DbATLine1.X1 = 1038.0!
        Me.DbATLine1.X2 = 6.0!
        Me.DbATLine1.Y1 = 90.0!
        Me.DbATLine1.Y2 = 90.0!
        '
        'DbATLabel7
        '
        Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel7.Height = 18.0!
        Me.DbATLabel7.Left = 426.0!
        Me.DbATLabel7.Name = "DbATLabel7"
        Me.DbATLabel7.Parent = Me.PageHeader
        Me.DbATLabel7.Text = "HORA:"
        Me.DbATLabel7.Top = 42.0!
        Me.DbATLabel7.Width = 48.0!
        '
        'DbATLabel8
        '
        Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel8.Height = 18.0!
        Me.DbATLabel8.Left = 264.0!
        Me.DbATLabel8.Name = "DbATLabel8"
        Me.DbATLabel8.Parent = Me.PageHeader
        Me.DbATLabel8.Text = "FECHA:"
        Me.DbATLabel8.Top = 42.0!
        Me.DbATLabel8.Width = 48.0!
        '
        'DbATLabel2
        '
        Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel2.Height = 18.0!
        Me.DbATLabel2.Left = 582.0!
        Me.DbATLabel2.Name = "DbATLabel2"
        Me.DbATLabel2.Parent = Me.PageHeader
        Me.DbATLabel2.Text = "TIENDA:"
        Me.DbATLabel2.Top = 42.0!
        Me.DbATLabel2.Width = 54.0!
        '
        'DbATLabel1
        '
        Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel1.Height = 18.0!
        Me.DbATLabel1.Left = 96.0!
        Me.DbATLabel1.Name = "DbATLabel1"
        Me.DbATLabel1.Parent = Me.PageHeader
        Me.DbATLabel1.Text = "REPORTE DE DIFERENCIAS 80-20 POR ALMACEN"
        Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel1.Top = 6.0!
        Me.DbATLabel1.Width = 846.0!
        '
        'txtFecha
        '
        Me.txtFecha.DataField = "Fecha"
        Me.txtFecha.Height = 18.0!
        Me.txtFecha.Left = 312.0!
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Parent = Me.PageHeader
        Me.txtFecha.Top = 42.0!
        Me.txtFecha.Width = 72.0!
        '
        'txtTienda
        '
        Me.txtTienda.DataField = "Tienda"
        Me.txtTienda.Height = 18.0!
        Me.txtTienda.Left = 636.0!
        Me.txtTienda.Name = "txtTienda"
        Me.txtTienda.OutputFormat = "h:mm AM/PM"
        Me.txtTienda.Parent = Me.PageHeader
        Me.txtTienda.Top = 42.0!
        Me.txtTienda.Width = 270.0!
        '
        'txtHora
        '
        Me.txtHora.Height = 18.0!
        Me.txtHora.Left = 474.0!
        Me.txtHora.Name = "txtHora"
        Me.txtHora.OutputFormat = "h:mm AM/PM"
        Me.txtHora.Parent = Me.PageHeader
        Me.txtHora.Top = 42.0!
        Me.txtHora.Width = 72.0!
        '
        'lblPage
        '
        Me.lblPage.ForeColor = System.Drawing.Color.Black
        Me.lblPage.Height = 18.0!
        Me.lblPage.Left = 924.0!
        Me.lblPage.Name = "lblPage"
        Me.lblPage.Parent = Me.PageHeader
        Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPage.Top = 42.0!
        Me.lblPage.Width = 96.0!
        '
        'DbATLabel3
        '
        Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel3.Height = 18.0!
        Me.DbATLabel3.Left = 24.0!
        Me.DbATLabel3.Name = "DbATLabel3"
        Me.DbATLabel3.Parent = Me.PageHeader
        Me.DbATLabel3.Text = "*PRECIOS VENTA VIGENTES EN SAP*"
        Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel3.Top = 20.88!
        Me.DbATLabel3.Width = 1002.0!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox3, Me.DbATTextBox18, Me.DbATTextBox15, Me.DbATTextBox13, Me.DbATTextBox12, Me.DbATTextBox11, Me.DbATLine4, Me.DbATTextBox1, Me.DbATTextBox4})
        Me.Detail.Height = 18.0!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'DbATTextBox3
        '
        Me.DbATTextBox3.CanGrow = False
        Me.DbATTextBox3.DataField = "DIFERENCIA"
        Me.DbATTextBox3.Height = 12.0!
        Me.DbATTextBox3.Left = 540.0!
        Me.DbATTextBox3.Name = "DbATTextBox3"
        Me.DbATTextBox3.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox3.Parent = Me.Detail
        Me.DbATTextBox3.Text = ""
        Me.DbATTextBox3.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox3.Top = 0.0!
        Me.DbATTextBox3.Width = 77.952!
        '
        'DbATTextBox18
        '
        Me.DbATTextBox18.CanGrow = False
        Me.DbATTextBox18.DataField = "DIFCOSTO"
        Me.DbATTextBox18.Height = 12.0!
        Me.DbATTextBox18.Left = 701.952!
        Me.DbATTextBox18.Name = "DbATTextBox18"
        Me.DbATTextBox18.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox18.Parent = Me.Detail
        Me.DbATTextBox18.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox18.Top = 0.0!
        Me.DbATTextBox18.Width = 82.8!
        '
        'DbATTextBox15
        '
        Me.DbATTextBox15.CanGrow = False
        Me.DbATTextBox15.DataField = "FISICO"
        Me.DbATTextBox15.Height = 12.0!
        Me.DbATTextBox15.Left = 456.0!
        Me.DbATTextBox15.Name = "DbATTextBox15"
        Me.DbATTextBox15.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox15.Parent = Me.Detail
        Me.DbATTextBox15.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox15.Top = 0.0!
        Me.DbATTextBox15.Width = 72.0!
        '
        'DbATTextBox13
        '
        Me.DbATTextBox13.CanGrow = False
        Me.DbATTextBox13.DataField = "Teorico"
        Me.DbATTextBox13.Height = 12.0!
        Me.DbATTextBox13.Left = 378.0!
        Me.DbATTextBox13.Name = "DbATTextBox13"
        Me.DbATTextBox13.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox13.Parent = Me.Detail
        Me.DbATTextBox13.Text = ""
        Me.DbATTextBox13.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox13.Top = 0.0!
        Me.DbATTextBox13.Width = 77.952!
        '
        'DbATTextBox12
        '
        Me.DbATTextBox12.CanGrow = False
        Me.DbATTextBox12.DataField = "OBS"
        Me.DbATTextBox12.Height = 12.48!
        Me.DbATTextBox12.Left = 78.0!
        Me.DbATTextBox12.Name = "DbATTextBox12"
        Me.DbATTextBox12.Parent = Me.Detail
        Me.DbATTextBox12.Text = ""
        Me.DbATTextBox12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox12.Top = 0.0!
        Me.DbATTextBox12.Width = 285.36!
        '
        'DbATTextBox11
        '
        Me.DbATTextBox11.CanGrow = False
        Me.DbATTextBox11.DataField = "CBARRAS"
        Me.DbATTextBox11.Height = 12.0!
        Me.DbATTextBox11.Left = 6.0!
        Me.DbATTextBox11.Name = "DbATTextBox11"
        Me.DbATTextBox11.Parent = Me.Detail
        Me.DbATTextBox11.Text = ""
        Me.DbATTextBox11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox11.Top = 0.0!
        Me.DbATTextBox11.Width = 67.2!
        '
        'DbATLine4
        '
        Me.DbATLine4.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine4.LineWeight = 1.0!
        Me.DbATLine4.Name = "DbATLine4"
        Me.DbATLine4.Parent = Me.Detail
        Me.DbATLine4.X1 = 624.0!
        Me.DbATLine4.X2 = 690.0!
        Me.DbATLine4.Y1 = 12.0!
        Me.DbATLine4.Y2 = 12.0!
        '
        'DbATTextBox1
        '
        Me.DbATTextBox1.CanGrow = False
        Me.DbATTextBox1.DataField = "PROVEEDOR"
        Me.DbATTextBox1.Height = 12.048!
        Me.DbATTextBox1.Left = 804.048!
        Me.DbATTextBox1.Name = "DbATTextBox1"
        Me.DbATTextBox1.OutputFormat = "#,##0.00"
        Me.DbATTextBox1.Parent = Me.Detail
        Me.DbATTextBox1.Text = ""
        Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox1.Top = 0.0!
        Me.DbATTextBox1.Width = 191.328!
        '
        'DbATTextBox4
        '
        Me.DbATTextBox4.CanGrow = False
        Me.DbATTextBox4.DataField = ""
        Me.DbATTextBox4.Height = 12.096!
        Me.DbATTextBox4.Left = 1012.56!
        Me.DbATTextBox4.Name = "DbATTextBox4"
        Me.DbATTextBox4.Parent = Me.Detail
        Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox4.Top = 0.0!
        Me.DbATTextBox4.Width = 30.048!
        '
        'PageFooter
        '
        Me.PageFooter.CanGrow = False
        Me.PageFooter.Height = 19.2!
        Me.PageFooter.Name = "PageFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox6, Me.DbATLabel6, Me.txtIdAlmacen, Me.DbATPageBreak2, Me.DbATLabel5, Me.DbATShape1, Me.DbATShape2})
        Me.GroupHeader1.Height = 43.0!
        Me.GroupHeader1.Index = 1
        Me.GroupHeader1.KeepTogether = True
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.NewPage = dbAutoTrack.DataReports.NewPage.Before
        '
        'DbATTextBox6
        '
        Me.DbATTextBox6.DataField = "Dept"
        Me.DbATTextBox6.Height = 18.0!
        Me.DbATTextBox6.Left = 198.0!
        Me.DbATTextBox6.Name = "DbATTextBox6"
        Me.DbATTextBox6.Parent = Me.GroupHeader1
        Me.DbATTextBox6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATTextBox6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATTextBox6.Top = 12.0!
        Me.DbATTextBox6.Width = 66.0!
        '
        'DbATLabel6
        '
        Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel6.Height = 18.0!
        Me.DbATLabel6.Left = 6.0!
        Me.DbATLabel6.Name = "DbATLabel6"
        Me.DbATLabel6.Parent = Me.GroupHeader1
        Me.DbATLabel6.Text = "ALMACEN:"
        Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATLabel6.Top = 12.0!
        Me.DbATLabel6.Width = 60.0!
        '
        'txtIdAlmacen
        '
        Me.txtIdAlmacen.CanGrow = False
        Me.txtIdAlmacen.DataField = "IdAlmacen"
        Me.txtIdAlmacen.DistinctField = "IdAlmacen"
        Me.txtIdAlmacen.Height = 18.0!
        Me.txtIdAlmacen.Left = 60.0!
        Me.txtIdAlmacen.Name = "txtIdAlmacen"
        Me.txtIdAlmacen.Parent = Me.GroupHeader1
        Me.txtIdAlmacen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.txtIdAlmacen.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.txtIdAlmacen.Top = 12.0!
        Me.txtIdAlmacen.Width = 42.048!
        '
        'DbATPageBreak2
        '
        Me.DbATPageBreak2.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak2.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak2.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak2.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak2.Enabled = False
        Me.DbATPageBreak2.Name = "DbATPageBreak2"
        Me.DbATPageBreak2.Parent = Me.GroupHeader1
        Me.DbATPageBreak2.Top = 6.0!
        Me.DbATPageBreak2.Visible = False
        '
        'DbATLabel5
        '
        Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel5.Height = 18.0!
        Me.DbATLabel5.Left = 150.0!
        Me.DbATLabel5.Name = "DbATLabel5"
        Me.DbATLabel5.Parent = Me.GroupHeader1
        Me.DbATLabel5.Text = "DEPT"
        Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATLabel5.Top = 12.0!
        Me.DbATLabel5.Width = 48.0!
        '
        'DbATShape1
        '
        Me.DbATShape1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape1.Height = 18.0!
        Me.DbATShape1.Left = 6.0!
        Me.DbATShape1.LineWeight = 1.0!
        Me.DbATShape1.Name = "DbATShape1"
        Me.DbATShape1.Parent = Me.GroupHeader1
        Me.DbATShape1.Top = 12.0!
        Me.DbATShape1.Width = 108.0!
        '
        'DbATShape2
        '
        Me.DbATShape2.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape2.Height = 18.0!
        Me.DbATShape2.Left = 144.0!
        Me.DbATShape2.LineWeight = 1.0!
        Me.DbATShape2.Name = "DbATShape2"
        Me.DbATShape2.Parent = Me.GroupHeader1
        Me.DbATShape2.Top = 12.0!
        Me.DbATShape2.Width = 120.0!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.0!
        Me.GroupFooter1.KeepTogether = True
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.Visible = False
        '
        'DbATPageBreak1
        '
        Me.DbATPageBreak1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.Enabled = False
        Me.DbATPageBreak1.Name = "DbATPageBreak1"
        Me.DbATPageBreak1.Parent = Me.GroupHeader1
        Me.DbATPageBreak1.Top = 16.8!
        Me.DbATPageBreak1.Visible = False
        '
        'RPT_Diferencias80_20AlmacenInvGral
        '
        Me.ReportWidth = 1056.0!
        Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region
    Dim pageno As Integer = 1
    Dim conteo As Integer = 1
    Dim totalpaginas As Integer = 0
    Dim Registros As Integer = 0
    Dim ContID As Integer = 0
    Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
        Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
        Me.pageno = Me.pageno + 1
        totalpaginas = MyBase.MaxPages
    End Sub

    Private Sub GroupFooter1_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.Initialize
        Me.DbATPageBreak1.Enabled = True
        Me.DbATPageBreak1.Visible = True
    End Sub

    Private Sub Detail_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Initialize
        ContID += 1
        DbATTextBox4.Value = ContID
    End Sub

    Private Sub GroupHeader1_Initialize(sender As Object, e As System.EventArgs) Handles GroupHeader1.Initialize
        ContID = 0
    End Sub
End Class


