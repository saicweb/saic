Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_DiferenciaEntreMarbetesC_rpt
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Dim totalpaginas As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents DbATLine7 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine28 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel23 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine11 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine10 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine9 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine8 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine6 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine5 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine3 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel22 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel21 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel20 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel19 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel18 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel17 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel16 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel15 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel14 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel13 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel12 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine4 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATShape1 As dbAutoTrack.DataReports.dbATShape
	Private WithEvents DbATShape2 As dbAutoTrack.DataReports.dbATShape
	Private WithEvents DbATLine25 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine24 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine23 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine22 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine21 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine20 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine19 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine18 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine17 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine16 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine15 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine14 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine13 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine2 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATTextBox15 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox14 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox13 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox12 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox11 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox10 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox9 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox8 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox7 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox6 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLine26 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine12 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine30 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents ReportHeader As dbAutoTrack.DataReports.ReportHeader
	Private WithEvents ReportFooter As dbAutoTrack.DataReports.ReportFooter
	Private WithEvents DbATLine27 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel24 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine29 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine31 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine32 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLine33 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents TotalConteo1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalConteo2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIF As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalCorrecto As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIFFisicaConteo1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIFFisicaConteo2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIFCostoConteo1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIFCostoConteo2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIFVentaConteo1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents TotalDIFVentaConteo2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLine34 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel25 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox16 As dbAutoTrack.DataReports.dbATTextBox
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader
		Me.DbATShape2 = New dbAutoTrack.DataReports.dbATShape
		Me.DbATShape1 = New dbAutoTrack.DataReports.dbATShape
		Me.DbATLine4 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel12 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel13 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel14 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel15 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel16 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel17 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel18 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel19 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel20 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel21 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel22 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLine3 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine5 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine6 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine8 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine9 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine10 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine11 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLabel23 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLine28 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLine7 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine29 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine31 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine32 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine33 = New dbAutoTrack.DataReports.dbATLine
		Me.Detail = New dbAutoTrack.DataReports.Detail
		Me.DbATLine30 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine12 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine26 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox6 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox7 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox8 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox9 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox10 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox11 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox12 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox13 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox14 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox15 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLine2 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine13 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine14 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine15 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine16 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine17 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine18 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine19 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine20 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine21 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine22 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine23 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine24 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLine25 = New dbAutoTrack.DataReports.dbATLine
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter
		Me.DbATLine34 = New dbAutoTrack.DataReports.dbATLine
		Me.ReportHeader = New dbAutoTrack.DataReports.ReportHeader
		Me.ReportFooter = New dbAutoTrack.DataReports.ReportFooter
		Me.TotalDIFVentaConteo2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalDIFVentaConteo1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalDIFCostoConteo2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalDIFCostoConteo1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalDIFFisicaConteo2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalDIFFisicaConteo1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalCorrecto = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalDIF = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalConteo2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.TotalConteo1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLine27 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLabel24 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel25 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATTextBox16 = New dbAutoTrack.DataReports.dbATTextBox
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATShape2, Me.DbATShape1, Me.DbATLine4, Me.DbATLine1, Me.DbATLabel8, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLabel12, Me.DbATLabel3, Me.DbATLabel6, Me.DbATLabel7, Me.DbATLabel9, Me.DbATLabel10, Me.DbATLabel13, Me.DbATLabel14, Me.DbATLabel15, Me.DbATLabel16, Me.DbATLabel17, Me.DbATLabel18, Me.DbATLabel19, Me.DbATLabel11, Me.DbATLabel20, Me.DbATLabel21, Me.DbATLabel22, Me.DbATLine3, Me.DbATLine5, Me.DbATLine6, Me.DbATLine8, Me.DbATLine9, Me.DbATLine10, Me.DbATLine11, Me.DbATLabel23, Me.DbATLine28, Me.DbATLabel5, Me.DbATLine7, Me.DbATLine29, Me.DbATLine31, Me.DbATLine32, Me.DbATLine33, Me.DbATLabel25})
		Me.PageHeader.Height = 96.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'DbATShape2
		'
		Me.DbATShape2.BackColor = System.Drawing.Color.Gainsboro
		Me.DbATShape2.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATShape2.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATShape2.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATShape2.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATShape2.Height = 18.0!
		Me.DbATShape2.Left = 6.0!
		Me.DbATShape2.LineWeight = 1.0!
		Me.DbATShape2.Name = "DbATShape2"
		Me.DbATShape2.Parent = Me.PageHeader
		Me.DbATShape2.Top = 78.0!
		Me.DbATShape2.Width = 1106.832!
		'
		'DbATShape1
		'
		Me.DbATShape1.BackColor = System.Drawing.Color.Gainsboro
		Me.DbATShape1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATShape1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATShape1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATShape1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATShape1.Height = 36.0!
		Me.DbATShape1.Left = 698.88!
		Me.DbATShape1.LineWeight = 1.0!
		Me.DbATShape1.Name = "DbATShape1"
		Me.DbATShape1.Parent = Me.PageHeader
		Me.DbATShape1.Top = 60.0!
		Me.DbATShape1.Width = 360.0!
		'
		'DbATLine4
		'
		Me.DbATLine4.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine4.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine4.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine4.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine4.LineWeight = 1.0!
		Me.DbATLine4.Name = "DbATLine4"
		Me.DbATLine4.Parent = Me.PageHeader
		Me.DbATLine4.X1 = 392.64!
		Me.DbATLine4.X2 = 392.64!
		Me.DbATLine4.Y1 = 78.0!
		Me.DbATLine4.Y2 = 96.0!
		'
		'DbATLine1
		'
		Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine1.LineWeight = 1.0!
		Me.DbATLine1.Name = "DbATLine1"
		Me.DbATLine1.Parent = Me.PageHeader
		Me.DbATLine1.X1 = 818.88!
		Me.DbATLine1.X2 = 818.88!
		Me.DbATLine1.Y1 = 59.52!
		Me.DbATLine1.Y2 = 96.0!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 168.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.PageHeader
		Me.DbATLabel8.Text = "FECHA:"
		Me.DbATLabel8.Top = 36.0!
		Me.DbATLabel8.Width = 48.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 540.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "TIENDA:"
		Me.DbATLabel2.Top = 36.0!
		Me.DbATLabel2.Width = 54.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 24.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "*PRECIOS VENTA VIGENTES EN SAP*"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATLabel1.Top = 21.12!
		Me.DbATLabel1.Width = 1008.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 216.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 36.0!
		Me.txtFecha.Width = 72.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 594.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 36.0!
		Me.txtTienda.Width = 270.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 360.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 36.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtHora
		'
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 408.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 36.0!
		Me.txtHora.Width = 72.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 936.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 36.0!
		Me.lblPage.Width = 96.0!
		'
		'DbATLabel12
		'
		Me.DbATLabel12.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel12.Height = 18.0!
		Me.DbATLabel12.Left = 140.88!
		Me.DbATLabel12.Name = "DbATLabel12"
		Me.DbATLabel12.Parent = Me.PageHeader
		Me.DbATLabel12.Text = "CBARRAS"
		Me.DbATLabel12.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel12.Top = 78.0!
		Me.DbATLabel12.Width = 85.44!
		'
		'DbATLabel3
		'
		Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel3.Height = 18.0!
		Me.DbATLabel3.Left = 228.0!
		Me.DbATLabel3.Name = "DbATLabel3"
		Me.DbATLabel3.Parent = Me.PageHeader
		Me.DbATLabel3.Text = "DESCRIPCION"
		Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel3.Top = 78.0!
		Me.DbATLabel3.Width = 107.952!
		'
		'DbATLabel6
		'
		Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel6.Height = 18.0!
		Me.DbATLabel6.Left = 702.0!
		Me.DbATLabel6.Name = "DbATLabel6"
		Me.DbATLabel6.Parent = Me.PageHeader
		Me.DbATLabel6.Text = "CONTEO 1"
		Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel6.Top = 78.0!
		Me.DbATLabel6.Width = 53.952!
		'
		'DbATLabel7
		'
		Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel7.Height = 18.0!
		Me.DbATLabel7.Left = 762.0!
		Me.DbATLabel7.Name = "DbATLabel7"
		Me.DbATLabel7.Parent = Me.PageHeader
		Me.DbATLabel7.Text = "CONTEO 2"
		Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel7.Top = 78.0!
		Me.DbATLabel7.Width = 53.952!
		'
		'DbATLabel9
		'
		Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel9.Height = 18.0!
		Me.DbATLabel9.Left = 516.0!
		Me.DbATLabel9.Name = "DbATLabel9"
		Me.DbATLabel9.Parent = Me.PageHeader
		Me.DbATLabel9.Text = "COSTO"
		Me.DbATLabel9.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel9.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel9.Top = 78.0!
		Me.DbATLabel9.Width = 53.952!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 18.0!
		Me.DbATLabel10.Left = 576.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.PageHeader
		Me.DbATLabel10.Text = "VENTA"
		Me.DbATLabel10.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel10.Top = 78.0!
		Me.DbATLabel10.Width = 53.952!
		'
		'DbATLabel13
		'
		Me.DbATLabel13.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel13.Height = 18.0!
		Me.DbATLabel13.Left = 456.0!
		Me.DbATLabel13.Name = "DbATLabel13"
		Me.DbATLabel13.Parent = Me.PageHeader
		Me.DbATLabel13.Text = "DIF"
		Me.DbATLabel13.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel13.Top = 78.0!
		Me.DbATLabel13.Width = 53.952!
		'
		'DbATLabel14
		'
		Me.DbATLabel14.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel14.Height = 18.0!
		Me.DbATLabel14.Left = 1002.0!
		Me.DbATLabel14.Name = "DbATLabel14"
		Me.DbATLabel14.Parent = Me.PageHeader
		Me.DbATLabel14.Text = "CONTEO 2"
		Me.DbATLabel14.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel14.Top = 78.0!
		Me.DbATLabel14.Width = 54.0!
		'
		'DbATLabel15
		'
		Me.DbATLabel15.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel15.Height = 18.0!
		Me.DbATLabel15.Left = 942.0!
		Me.DbATLabel15.Name = "DbATLabel15"
		Me.DbATLabel15.Parent = Me.PageHeader
		Me.DbATLabel15.Text = "CONTEO 1"
		Me.DbATLabel15.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel15.Top = 78.0!
		Me.DbATLabel15.Width = 53.952!
		'
		'DbATLabel16
		'
		Me.DbATLabel16.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel16.Height = 18.0!
		Me.DbATLabel16.Left = 396.0!
		Me.DbATLabel16.Name = "DbATLabel16"
		Me.DbATLabel16.Parent = Me.PageHeader
		Me.DbATLabel16.Text = "CONTEO 2"
		Me.DbATLabel16.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel16.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel16.Top = 78.0!
		Me.DbATLabel16.Width = 53.952!
		'
		'DbATLabel17
		'
		Me.DbATLabel17.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel17.Height = 18.0!
		Me.DbATLabel17.Left = 336.0!
		Me.DbATLabel17.Name = "DbATLabel17"
		Me.DbATLabel17.Parent = Me.PageHeader
		Me.DbATLabel17.Text = "CONTEO 1"
		Me.DbATLabel17.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel17.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel17.Top = 78.0!
		Me.DbATLabel17.Width = 53.952!
		'
		'DbATLabel18
		'
		Me.DbATLabel18.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel18.Height = 18.0!
		Me.DbATLabel18.Left = 942.0!
		Me.DbATLabel18.Name = "DbATLabel18"
		Me.DbATLabel18.Parent = Me.PageHeader
		Me.DbATLabel18.Text = "Diferencia Venta"
		Me.DbATLabel18.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel18.Top = 60.0!
		Me.DbATLabel18.Width = 114.048!
		'
		'DbATLabel19
		'
		Me.DbATLabel19.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel19.Height = 18.0!
		Me.DbATLabel19.Left = 630.0!
		Me.DbATLabel19.Name = "DbATLabel19"
		Me.DbATLabel19.Parent = Me.PageHeader
		Me.DbATLabel19.Text = "CORRECTO"
		Me.DbATLabel19.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel19.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel19.Top = 78.0!
		Me.DbATLabel19.Width = 66.048!
		'
		'DbATLabel11
		'
		Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel11.Height = 18.0!
		Me.DbATLabel11.Left = 702.0!
		Me.DbATLabel11.Name = "DbATLabel11"
		Me.DbATLabel11.Parent = Me.PageHeader
		Me.DbATLabel11.Text = "Diferencia Fisica"
		Me.DbATLabel11.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel11.Top = 60.0!
		Me.DbATLabel11.Width = 114.048!
		'
		'DbATLabel20
		'
		Me.DbATLabel20.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel20.Height = 18.0!
		Me.DbATLabel20.Left = 822.0!
		Me.DbATLabel20.Name = "DbATLabel20"
		Me.DbATLabel20.Parent = Me.PageHeader
		Me.DbATLabel20.Text = "Diferencia Costo"
		Me.DbATLabel20.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel20.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel20.Top = 60.0!
		Me.DbATLabel20.Width = 114.048!
		'
		'DbATLabel21
		'
		Me.DbATLabel21.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel21.Height = 18.0!
		Me.DbATLabel21.Left = 882.0!
		Me.DbATLabel21.Name = "DbATLabel21"
		Me.DbATLabel21.Parent = Me.PageHeader
		Me.DbATLabel21.Text = "CONTEO 2"
		Me.DbATLabel21.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel21.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel21.Top = 78.0!
		Me.DbATLabel21.Width = 53.952!
		'
		'DbATLabel22
		'
		Me.DbATLabel22.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel22.Height = 18.0!
		Me.DbATLabel22.Left = 822.0!
		Me.DbATLabel22.Name = "DbATLabel22"
		Me.DbATLabel22.Parent = Me.PageHeader
		Me.DbATLabel22.Text = "CONTEO 1"
		Me.DbATLabel22.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel22.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel22.Top = 78.0!
		Me.DbATLabel22.Width = 53.952!
		'
		'DbATLine3
		'
		Me.DbATLine3.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine3.LineWeight = 1.0!
		Me.DbATLine3.Name = "DbATLine3"
		Me.DbATLine3.Parent = Me.PageHeader
		Me.DbATLine3.X1 = 938.88!
		Me.DbATLine3.X2 = 938.88!
		Me.DbATLine3.Y1 = 59.52!
		Me.DbATLine3.Y2 = 96.0!
		'
		'DbATLine5
		'
		Me.DbATLine5.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine5.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine5.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine5.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine5.LineWeight = 1.0!
		Me.DbATLine5.Name = "DbATLine5"
		Me.DbATLine5.Parent = Me.PageHeader
		Me.DbATLine5.X1 = 338.64!
		Me.DbATLine5.X2 = 338.64!
		Me.DbATLine5.Y1 = 78.0!
		Me.DbATLine5.Y2 = 96.0!
		'
		'DbATLine6
		'
		Me.DbATLine6.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine6.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine6.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine6.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine6.LineWeight = 1.0!
		Me.DbATLine6.Name = "DbATLine6"
		Me.DbATLine6.Parent = Me.PageHeader
		Me.DbATLine6.X1 = 224.64!
		Me.DbATLine6.X2 = 224.64!
		Me.DbATLine6.Y1 = 78.0!
		Me.DbATLine6.Y2 = 96.0!
		'
		'DbATLine8
		'
		Me.DbATLine8.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine8.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine8.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine8.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine8.LineWeight = 1.0!
		Me.DbATLine8.Name = "DbATLine8"
		Me.DbATLine8.Parent = Me.PageHeader
		Me.DbATLine8.X1 = 452.64!
		Me.DbATLine8.X2 = 452.64!
		Me.DbATLine8.Y1 = 78.0!
		Me.DbATLine8.Y2 = 96.0!
		'
		'DbATLine9
		'
		Me.DbATLine9.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine9.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine9.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine9.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine9.LineWeight = 1.0!
		Me.DbATLine9.Name = "DbATLine9"
		Me.DbATLine9.Parent = Me.PageHeader
		Me.DbATLine9.X1 = 512.64!
		Me.DbATLine9.X2 = 512.64!
		Me.DbATLine9.Y1 = 78.0!
		Me.DbATLine9.Y2 = 96.0!
		'
		'DbATLine10
		'
		Me.DbATLine10.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine10.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine10.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine10.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine10.LineWeight = 1.0!
		Me.DbATLine10.Name = "DbATLine10"
		Me.DbATLine10.Parent = Me.PageHeader
		Me.DbATLine10.X1 = 572.64!
		Me.DbATLine10.X2 = 572.64!
		Me.DbATLine10.Y1 = 78.0!
		Me.DbATLine10.Y2 = 96.0!
		'
		'DbATLine11
		'
		Me.DbATLine11.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine11.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine11.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine11.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine11.LineWeight = 1.0!
		Me.DbATLine11.Name = "DbATLine11"
		Me.DbATLine11.Parent = Me.PageHeader
		Me.DbATLine11.X1 = 632.64!
		Me.DbATLine11.X2 = 632.64!
		Me.DbATLine11.Y1 = 78.0!
		Me.DbATLine11.Y2 = 96.0!
		'
		'DbATLabel23
		'
		Me.DbATLabel23.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel23.Height = 18.0!
		Me.DbATLabel23.Left = 24.0!
		Me.DbATLabel23.Name = "DbATLabel23"
		Me.DbATLabel23.Parent = Me.PageHeader
		Me.DbATLabel23.Text = "DIFERENCIA ENTRE MARBETES"
		Me.DbATLabel23.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel23.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel23.Top = 6.0!
		Me.DbATLabel23.Width = 1008.0!
		'
		'DbATLine28
		'
		Me.DbATLine28.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine28.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine28.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine28.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine28.LineWeight = 1.0!
		Me.DbATLine28.Name = "DbATLine28"
		Me.DbATLine28.Parent = Me.PageHeader
		Me.DbATLine28.X1 = 699.36!
		Me.DbATLine28.X2 = 699.36!
		Me.DbATLine28.Y1 = 78.0!
		Me.DbATLine28.Y2 = 96.0!
		'
		'DbATLabel5
		'
		Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel5.Height = 18.0!
		Me.DbATLabel5.Left = 6.0!
		Me.DbATLabel5.Name = "DbATLabel5"
		Me.DbATLabel5.Parent = Me.PageHeader
		Me.DbATLabel5.Text = "MARBETE"
		Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel5.Top = 78.0!
		Me.DbATLabel5.Width = 66.048!
		'
		'DbATLine7
		'
		Me.DbATLine7.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine7.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine7.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine7.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine7.LineWeight = 1.0!
		Me.DbATLine7.Name = "DbATLine7"
		Me.DbATLine7.Parent = Me.PageHeader
		Me.DbATLine7.X1 = 70.08!
		Me.DbATLine7.X2 = 70.08!
		Me.DbATLine7.Y1 = 78.0!
		Me.DbATLine7.Y2 = 96.0!
		'
		'DbATLine29
		'
		Me.DbATLine29.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine29.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine29.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine29.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine29.LineWeight = 1.0!
		Me.DbATLine29.Name = "DbATLine29"
		Me.DbATLine29.Parent = Me.PageHeader
		Me.DbATLine29.X1 = 702.0!
		Me.DbATLine29.X2 = 1058.88!
		Me.DbATLine29.Y1 = 78.0!
		Me.DbATLine29.Y2 = 78.0!
		'
		'DbATLine31
		'
		Me.DbATLine31.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine31.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine31.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine31.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine31.LineWeight = 1.0!
		Me.DbATLine31.Name = "DbATLine31"
		Me.DbATLine31.Parent = Me.PageHeader
		Me.DbATLine31.X1 = 761.472!
		Me.DbATLine31.X2 = 761.472!
		Me.DbATLine31.Y1 = 78.0!
		Me.DbATLine31.Y2 = 96.0!
		'
		'DbATLine32
		'
		Me.DbATLine32.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine32.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine32.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine32.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine32.LineWeight = 1.0!
		Me.DbATLine32.Name = "DbATLine32"
		Me.DbATLine32.Parent = Me.PageHeader
		Me.DbATLine32.X1 = 878.592!
		Me.DbATLine32.X2 = 878.592!
		Me.DbATLine32.Y1 = 78.0!
		Me.DbATLine32.Y2 = 96.0!
		'
		'DbATLine33
		'
		Me.DbATLine33.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine33.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine33.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine33.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine33.LineWeight = 1.0!
		Me.DbATLine33.Name = "DbATLine33"
		Me.DbATLine33.Parent = Me.PageHeader
		Me.DbATLine33.X1 = 998.592!
		Me.DbATLine33.X2 = 998.592!
		Me.DbATLine33.Y1 = 78.0!
		Me.DbATLine33.Y2 = 96.0!
		'
		'Detail
		'
		Me.Detail.CanGrow = False
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLine30, Me.DbATLine12, Me.DbATLine26, Me.DbATTextBox1, Me.DbATTextBox2, Me.DbATTextBox3, Me.DbATTextBox4, Me.DbATTextBox5, Me.DbATTextBox6, Me.DbATTextBox7, Me.DbATTextBox8, Me.DbATTextBox9, Me.DbATTextBox10, Me.DbATTextBox11, Me.DbATTextBox12, Me.DbATTextBox13, Me.DbATTextBox14, Me.DbATTextBox15, Me.DbATLine2, Me.DbATLine13, Me.DbATLine14, Me.DbATLine15, Me.DbATLine16, Me.DbATLine17, Me.DbATLine18, Me.DbATLine19, Me.DbATLine20, Me.DbATLine21, Me.DbATLine22, Me.DbATLine23, Me.DbATLine24, Me.DbATLine25, Me.DbATTextBox16})
		Me.Detail.Height = 38.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATLine30
		'
		Me.DbATLine30.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine30.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine30.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine30.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine30.LineWeight = 1.0!
		Me.DbATLine30.Name = "DbATLine30"
		Me.DbATLine30.Parent = Me.Detail
		Me.DbATLine30.X1 = 1034.88!
		Me.DbATLine30.X2 = 6.048!
		Me.DbATLine30.Y1 = 0.0!
		Me.DbATLine30.Y2 = 0.0!
		'
		'DbATLine12
		'
		Me.DbATLine12.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine12.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine12.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine12.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine12.LineWeight = 1.0!
		Me.DbATLine12.Name = "DbATLine12"
		Me.DbATLine12.Parent = Me.Detail
		Me.DbATLine12.X1 = 1034.64!
		Me.DbATLine12.X2 = 1034.64!
		Me.DbATLine12.Y1 = 0.0!
		Me.DbATLine12.Y2 = 18.0!
		'
		'DbATLine26
		'
		Me.DbATLine26.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine26.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine26.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine26.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine26.LineWeight = 1.0!
		Me.DbATLine26.Name = "DbATLine26"
		Me.DbATLine26.Parent = Me.Detail
		Me.DbATLine26.X1 = 6.24!
		Me.DbATLine26.X2 = 6.24!
		Me.DbATLine26.Y1 = 0.0!
		Me.DbATLine26.Y2 = 18.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.CanGrow = False
		Me.DbATTextBox1.DataField = "IdMarbete"
		Me.DbATTextBox1.Height = 16.32!
		Me.DbATTextBox1.Left = 0.0!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.OutputFormat = "#,##0.00"
		Me.DbATTextBox1.Parent = Me.Detail
		Me.DbATTextBox1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.0!)
		Me.DbATTextBox1.Top = 0.9599991!
		Me.DbATTextBox1.Width = 78.0!
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.CanGrow = False
		Me.DbATTextBox2.DataField = "EAN"
		Me.DbATTextBox2.Height = 16.32!
		Me.DbATTextBox2.Left = 128.88!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.OutputFormat = "#,##0.00"
		Me.DbATTextBox2.Parent = Me.Detail
		Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox2.Top = 0.96!
		Me.DbATTextBox2.Width = 85.44!
		'
		'DbATTextBox3
		'
		Me.DbATTextBox3.DataField = "OBS"
		Me.DbATTextBox3.Height = 16.32!
		Me.DbATTextBox3.Left = 216.0!
		Me.DbATTextBox3.Name = "DbATTextBox3"
		Me.DbATTextBox3.OutputFormat = ""
		Me.DbATTextBox3.Parent = Me.Detail
		Me.DbATTextBox3.Text = ""
		Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox3.Top = 0.96!
		Me.DbATTextBox3.Width = 108.0!
		'
		'DbATTextBox4
		'
		Me.DbATTextBox4.CanGrow = False
		Me.DbATTextBox4.DataField = "CONTEO1"
		Me.DbATTextBox4.Height = 16.32!
		Me.DbATTextBox4.Left = 330.0!
		Me.DbATTextBox4.Name = "DbATTextBox4"
		Me.DbATTextBox4.OutputFormat = "#,##0"
		Me.DbATTextBox4.Parent = Me.Detail
		Me.DbATTextBox4.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox4.Top = 0.96!
		Me.DbATTextBox4.Width = 54.0!
		'
		'DbATTextBox5
		'
		Me.DbATTextBox5.CanGrow = False
		Me.DbATTextBox5.DataField = "CONTEO2"
		Me.DbATTextBox5.Height = 16.32!
		Me.DbATTextBox5.Left = 390.0!
		Me.DbATTextBox5.Name = "DbATTextBox5"
		Me.DbATTextBox5.OutputFormat = "#,##0"
		Me.DbATTextBox5.Parent = Me.Detail
		Me.DbATTextBox5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox5.Top = 0.96!
		Me.DbATTextBox5.Width = 54.0!
		'
		'DbATTextBox6
		'
		Me.DbATTextBox6.CanGrow = False
		Me.DbATTextBox6.DataField = "DIF"
		Me.DbATTextBox6.Height = 16.32!
		Me.DbATTextBox6.Left = 450.0!
		Me.DbATTextBox6.Name = "DbATTextBox6"
		Me.DbATTextBox6.OutputFormat = "#,##0"
		Me.DbATTextBox6.Parent = Me.Detail
		Me.DbATTextBox6.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox6.Top = 0.96!
		Me.DbATTextBox6.Width = 54.0!
		'
		'DbATTextBox7
		'
		Me.DbATTextBox7.CanGrow = False
		Me.DbATTextBox7.DataField = "COSTO"
		Me.DbATTextBox7.Height = 16.32!
		Me.DbATTextBox7.Left = 510.0!
		Me.DbATTextBox7.Name = "DbATTextBox7"
		Me.DbATTextBox7.OutputFormat = "#,##0.00"
		Me.DbATTextBox7.Parent = Me.Detail
		Me.DbATTextBox7.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox7.Top = 0.96!
		Me.DbATTextBox7.Width = 54.0!
		'
		'DbATTextBox8
		'
		Me.DbATTextBox8.CanGrow = False
		Me.DbATTextBox8.DataField = "VENTA"
		Me.DbATTextBox8.Height = 16.32!
		Me.DbATTextBox8.Left = 570.0!
		Me.DbATTextBox8.Name = "DbATTextBox8"
		Me.DbATTextBox8.OutputFormat = "#,##0.00"
		Me.DbATTextBox8.Parent = Me.Detail
		Me.DbATTextBox8.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox8.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox8.Top = 0.96!
		Me.DbATTextBox8.Width = 54.0!
		'
		'DbATTextBox9
		'
		Me.DbATTextBox9.CanGrow = False
		Me.DbATTextBox9.DataField = "CORRECTO"
		Me.DbATTextBox9.Height = 16.32!
		Me.DbATTextBox9.Left = 630.0!
		Me.DbATTextBox9.Name = "DbATTextBox9"
		Me.DbATTextBox9.OutputFormat = "#,##0"
		Me.DbATTextBox9.Parent = Me.Detail
		Me.DbATTextBox9.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox9.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox9.Top = 0.96!
		Me.DbATTextBox9.Width = 66.0!
		'
		'DbATTextBox10
		'
		Me.DbATTextBox10.CanGrow = False
		Me.DbATTextBox10.DataField = "DIFFisicaConteo1"
		Me.DbATTextBox10.Height = 16.32!
		Me.DbATTextBox10.Left = 702.0!
		Me.DbATTextBox10.Name = "DbATTextBox10"
		Me.DbATTextBox10.OutputFormat = "#,##0"
		Me.DbATTextBox10.Parent = Me.Detail
		Me.DbATTextBox10.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox10.Top = 0.96!
		Me.DbATTextBox10.Width = 54.0!
		'
		'DbATTextBox11
		'
		Me.DbATTextBox11.CanGrow = False
		Me.DbATTextBox11.DataField = "DIFFisicaConteo2"
		Me.DbATTextBox11.Height = 16.32!
		Me.DbATTextBox11.Left = 762.0!
		Me.DbATTextBox11.Name = "DbATTextBox11"
		Me.DbATTextBox11.OutputFormat = "#,##0"
		Me.DbATTextBox11.Parent = Me.Detail
		Me.DbATTextBox11.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox11.Top = 0.96!
		Me.DbATTextBox11.Width = 54.0!
		'
		'DbATTextBox12
		'
		Me.DbATTextBox12.CanGrow = False
		Me.DbATTextBox12.DataField = "DIFCostoConteo1"
		Me.DbATTextBox12.Height = 16.32!
		Me.DbATTextBox12.Left = 822.0!
		Me.DbATTextBox12.Name = "DbATTextBox12"
		Me.DbATTextBox12.OutputFormat = "#,##0.00"
		Me.DbATTextBox12.Parent = Me.Detail
		Me.DbATTextBox12.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox12.Top = 0.96!
		Me.DbATTextBox12.Width = 54.0!
		'
		'DbATTextBox13
		'
		Me.DbATTextBox13.CanGrow = False
		Me.DbATTextBox13.DataField = "DIFCostoConteo2"
		Me.DbATTextBox13.Height = 16.32!
		Me.DbATTextBox13.Left = 882.0!
		Me.DbATTextBox13.Name = "DbATTextBox13"
		Me.DbATTextBox13.OutputFormat = "#,##0.00"
		Me.DbATTextBox13.Parent = Me.Detail
		Me.DbATTextBox13.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox13.Top = 0.96!
		Me.DbATTextBox13.Width = 54.0!
		'
		'DbATTextBox14
		'
		Me.DbATTextBox14.CanGrow = False
		Me.DbATTextBox14.DataField = "DIFVentaConteo1"
		Me.DbATTextBox14.Height = 16.32!
		Me.DbATTextBox14.Left = 942.0!
		Me.DbATTextBox14.Name = "DbATTextBox14"
		Me.DbATTextBox14.OutputFormat = "#,##0.00"
		Me.DbATTextBox14.Parent = Me.Detail
		Me.DbATTextBox14.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox14.Top = 0.96!
		Me.DbATTextBox14.Width = 54.0!
		'
		'DbATTextBox15
		'
		Me.DbATTextBox15.CanGrow = False
		Me.DbATTextBox15.DataField = "DIFVentaConteo2"
		Me.DbATTextBox15.Height = 16.32!
		Me.DbATTextBox15.Left = 1002.0!
		Me.DbATTextBox15.Name = "DbATTextBox15"
		Me.DbATTextBox15.OutputFormat = "#,##0.00"
		Me.DbATTextBox15.Parent = Me.Detail
		Me.DbATTextBox15.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox15.Top = 0.96!
		Me.DbATTextBox15.Width = 54.0!
		'
		'DbATLine2
		'
		Me.DbATLine2.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine2.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine2.LineWeight = 1.0!
		Me.DbATLine2.Name = "DbATLine2"
		Me.DbATLine2.Parent = Me.Detail
		Me.DbATLine2.X1 = 878.64!
		Me.DbATLine2.X2 = 878.64!
		Me.DbATLine2.Y1 = 0.0!
		Me.DbATLine2.Y2 = 18.0!
		'
		'DbATLine13
		'
		Me.DbATLine13.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine13.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine13.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine13.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine13.LineWeight = 1.0!
		Me.DbATLine13.Name = "DbATLine13"
		Me.DbATLine13.Parent = Me.Detail
		Me.DbATLine13.X1 = 938.64!
		Me.DbATLine13.X2 = 938.64!
		Me.DbATLine13.Y1 = 0.0!
		Me.DbATLine13.Y2 = 18.0!
		'
		'DbATLine14
		'
		Me.DbATLine14.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine14.LineWeight = 1.0!
		Me.DbATLine14.Name = "DbATLine14"
		Me.DbATLine14.Parent = Me.Detail
		Me.DbATLine14.X1 = 998.64!
		Me.DbATLine14.X2 = 998.64!
		Me.DbATLine14.Y1 = 0.0!
		Me.DbATLine14.Y2 = 18.0!
		'
		'DbATLine15
		'
		Me.DbATLine15.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine15.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine15.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine15.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine15.LineWeight = 1.0!
		Me.DbATLine15.Name = "DbATLine15"
		Me.DbATLine15.Parent = Me.Detail
		Me.DbATLine15.X1 = 818.64!
		Me.DbATLine15.X2 = 818.64!
		Me.DbATLine15.Y1 = 0.0!
		Me.DbATLine15.Y2 = 18.0!
		'
		'DbATLine16
		'
		Me.DbATLine16.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine16.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine16.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine16.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine16.LineWeight = 1.0!
		Me.DbATLine16.Name = "DbATLine16"
		Me.DbATLine16.Parent = Me.Detail
		Me.DbATLine16.X1 = 758.64!
		Me.DbATLine16.X2 = 758.64!
		Me.DbATLine16.Y1 = 0.0!
		Me.DbATLine16.Y2 = 18.0!
		'
		'DbATLine17
		'
		Me.DbATLine17.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine17.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine17.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine17.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine17.LineWeight = 1.0!
		Me.DbATLine17.Name = "DbATLine17"
		Me.DbATLine17.Parent = Me.Detail
		Me.DbATLine17.X1 = 699.36!
		Me.DbATLine17.X2 = 699.36!
		Me.DbATLine17.Y1 = 0.0!
		Me.DbATLine17.Y2 = 18.0!
		'
		'DbATLine18
		'
		Me.DbATLine18.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine18.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine18.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine18.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine18.LineWeight = 1.0!
		Me.DbATLine18.Name = "DbATLine18"
		Me.DbATLine18.Parent = Me.Detail
		Me.DbATLine18.X1 = 626.64!
		Me.DbATLine18.X2 = 626.64!
		Me.DbATLine18.Y1 = 0.0!
		Me.DbATLine18.Y2 = 18.0!
		'
		'DbATLine19
		'
		Me.DbATLine19.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine19.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine19.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine19.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine19.LineWeight = 1.0!
		Me.DbATLine19.Name = "DbATLine19"
		Me.DbATLine19.Parent = Me.Detail
		Me.DbATLine19.X1 = 566.64!
		Me.DbATLine19.X2 = 566.64!
		Me.DbATLine19.Y1 = 0.0!
		Me.DbATLine19.Y2 = 18.0!
		'
		'DbATLine20
		'
		Me.DbATLine20.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine20.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine20.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine20.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine20.LineWeight = 1.0!
		Me.DbATLine20.Name = "DbATLine20"
		Me.DbATLine20.Parent = Me.Detail
		Me.DbATLine20.X1 = 506.64!
		Me.DbATLine20.X2 = 506.64!
		Me.DbATLine20.Y1 = 0.0!
		Me.DbATLine20.Y2 = 18.0!
		'
		'DbATLine21
		'
		Me.DbATLine21.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine21.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine21.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine21.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine21.LineWeight = 1.0!
		Me.DbATLine21.Name = "DbATLine21"
		Me.DbATLine21.Parent = Me.Detail
		Me.DbATLine21.X1 = 446.64!
		Me.DbATLine21.X2 = 446.64!
		Me.DbATLine21.Y1 = 0.0!
		Me.DbATLine21.Y2 = 18.0!
		'
		'DbATLine22
		'
		Me.DbATLine22.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine22.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine22.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine22.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine22.LineWeight = 1.0!
		Me.DbATLine22.Name = "DbATLine22"
		Me.DbATLine22.Parent = Me.Detail
		Me.DbATLine22.X1 = 386.64!
		Me.DbATLine22.X2 = 386.64!
		Me.DbATLine22.Y1 = 0.0!
		Me.DbATLine22.Y2 = 18.0!
		'
		'DbATLine23
		'
		Me.DbATLine23.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine23.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine23.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine23.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine23.LineWeight = 1.0!
		Me.DbATLine23.Name = "DbATLine23"
		Me.DbATLine23.Parent = Me.Detail
		Me.DbATLine23.X1 = 326.64!
		Me.DbATLine23.X2 = 326.64!
		Me.DbATLine23.Y1 = 0.0!
		Me.DbATLine23.Y2 = 18.0!
		'
		'DbATLine24
		'
		Me.DbATLine24.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine24.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine24.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine24.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine24.LineWeight = 1.0!
		Me.DbATLine24.Name = "DbATLine24"
		Me.DbATLine24.Parent = Me.Detail
		Me.DbATLine24.X1 = 218.64!
		Me.DbATLine24.X2 = 218.64!
		Me.DbATLine24.Y1 = 0.0!
		Me.DbATLine24.Y2 = 18.0!
		'
		'DbATLine25
		'
		Me.DbATLine25.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine25.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine25.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine25.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine25.LineWeight = 1.0!
		Me.DbATLine25.Name = "DbATLine25"
		Me.DbATLine25.Parent = Me.Detail
		Me.DbATLine25.X1 = 136.08!
		Me.DbATLine25.X2 = 136.08!
		Me.DbATLine25.Y1 = 0.0!
		Me.DbATLine25.Y2 = 18.0!
		'
		'PageFooter
		'
		Me.PageFooter.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLine34})
		Me.PageFooter.Height = 20.0!
		Me.PageFooter.Name = "PageFooter"
		Me.PageFooter.Visible = False
		'
		'DbATLine34
		'
		Me.DbATLine34.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine34.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine34.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine34.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine34.LineWeight = 1.0!
		Me.DbATLine34.Name = "DbATLine34"
		Me.DbATLine34.Parent = Me.PageFooter
		Me.DbATLine34.X1 = 1034.88!
		Me.DbATLine34.X2 = 12.048!
		Me.DbATLine34.Y1 = 0.96!
		Me.DbATLine34.Y2 = 0.96!
		'
		'ReportHeader
		'
		Me.ReportHeader.Height = 0.0!
		Me.ReportHeader.Name = "ReportHeader"
		Me.ReportHeader.Visible = False
		'
		'ReportFooter
		'
		Me.ReportFooter.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.TotalDIFVentaConteo2, Me.TotalDIFVentaConteo1, Me.TotalDIFCostoConteo2, Me.TotalDIFCostoConteo1, Me.TotalDIFFisicaConteo2, Me.TotalDIFFisicaConteo1, Me.TotalCorrecto, Me.TotalDIF, Me.TotalConteo2, Me.TotalConteo1, Me.DbATLine27, Me.DbATLabel24})
		Me.ReportFooter.Height = 32.0!
		Me.ReportFooter.KeepTogether = True
		Me.ReportFooter.Name = "ReportFooter"
		'
		'TotalDIFVentaConteo2
		'
		Me.TotalDIFVentaConteo2.CanGrow = False
		Me.TotalDIFVentaConteo2.DataField = ""
		Me.TotalDIFVentaConteo2.Height = 16.32!
		Me.TotalDIFVentaConteo2.Left = 978.0!
		Me.TotalDIFVentaConteo2.Name = "TotalDIFVentaConteo2"
		Me.TotalDIFVentaConteo2.OutputFormat = "#,##0.00"
		Me.TotalDIFVentaConteo2.Parent = Me.ReportFooter
		Me.TotalDIFVentaConteo2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIFVentaConteo2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIFVentaConteo2.Top = 6.96!
		Me.TotalDIFVentaConteo2.Width = 54.0!
		'
		'TotalDIFVentaConteo1
		'
		Me.TotalDIFVentaConteo1.CanGrow = False
		Me.TotalDIFVentaConteo1.DataField = ""
		Me.TotalDIFVentaConteo1.Height = 16.32!
		Me.TotalDIFVentaConteo1.Left = 918.0!
		Me.TotalDIFVentaConteo1.Name = "TotalDIFVentaConteo1"
		Me.TotalDIFVentaConteo1.OutputFormat = "#,##0.00"
		Me.TotalDIFVentaConteo1.Parent = Me.ReportFooter
		Me.TotalDIFVentaConteo1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIFVentaConteo1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIFVentaConteo1.Top = 6.96!
		Me.TotalDIFVentaConteo1.Width = 54.0!
		'
		'TotalDIFCostoConteo2
		'
		Me.TotalDIFCostoConteo2.CanGrow = False
		Me.TotalDIFCostoConteo2.DataField = ""
		Me.TotalDIFCostoConteo2.Height = 16.32!
		Me.TotalDIFCostoConteo2.Left = 858.0!
		Me.TotalDIFCostoConteo2.Name = "TotalDIFCostoConteo2"
		Me.TotalDIFCostoConteo2.OutputFormat = "#,##0.00"
		Me.TotalDIFCostoConteo2.Parent = Me.ReportFooter
		Me.TotalDIFCostoConteo2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIFCostoConteo2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIFCostoConteo2.Top = 6.96!
		Me.TotalDIFCostoConteo2.Width = 54.0!
		'
		'TotalDIFCostoConteo1
		'
		Me.TotalDIFCostoConteo1.CanGrow = False
		Me.TotalDIFCostoConteo1.DataField = ""
		Me.TotalDIFCostoConteo1.Height = 16.32!
		Me.TotalDIFCostoConteo1.Left = 798.0!
		Me.TotalDIFCostoConteo1.Name = "TotalDIFCostoConteo1"
		Me.TotalDIFCostoConteo1.OutputFormat = "#,##0.00"
		Me.TotalDIFCostoConteo1.Parent = Me.ReportFooter
		Me.TotalDIFCostoConteo1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIFCostoConteo1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIFCostoConteo1.Top = 6.96!
		Me.TotalDIFCostoConteo1.Width = 54.0!
		'
		'TotalDIFFisicaConteo2
		'
		Me.TotalDIFFisicaConteo2.CanGrow = False
		Me.TotalDIFFisicaConteo2.DataField = ""
		Me.TotalDIFFisicaConteo2.Height = 16.32!
		Me.TotalDIFFisicaConteo2.Left = 738.0!
		Me.TotalDIFFisicaConteo2.Name = "TotalDIFFisicaConteo2"
		Me.TotalDIFFisicaConteo2.OutputFormat = "#,##0"
		Me.TotalDIFFisicaConteo2.Parent = Me.ReportFooter
		Me.TotalDIFFisicaConteo2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIFFisicaConteo2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIFFisicaConteo2.Top = 6.96!
		Me.TotalDIFFisicaConteo2.Width = 54.0!
		'
		'TotalDIFFisicaConteo1
		'
		Me.TotalDIFFisicaConteo1.CanGrow = False
		Me.TotalDIFFisicaConteo1.DataField = ""
		Me.TotalDIFFisicaConteo1.Height = 16.32!
		Me.TotalDIFFisicaConteo1.Left = 672.0!
		Me.TotalDIFFisicaConteo1.Name = "TotalDIFFisicaConteo1"
		Me.TotalDIFFisicaConteo1.OutputFormat = "#,##0"
		Me.TotalDIFFisicaConteo1.Parent = Me.ReportFooter
		Me.TotalDIFFisicaConteo1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIFFisicaConteo1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIFFisicaConteo1.Top = 6.96!
		Me.TotalDIFFisicaConteo1.Width = 54.0!
		'
		'TotalCorrecto
		'
		Me.TotalCorrecto.CanGrow = False
		Me.TotalCorrecto.DataField = ""
		Me.TotalCorrecto.Height = 16.32!
		Me.TotalCorrecto.Left = 606.0!
		Me.TotalCorrecto.Name = "TotalCorrecto"
		Me.TotalCorrecto.OutputFormat = "#,##0"
		Me.TotalCorrecto.Parent = Me.ReportFooter
		Me.TotalCorrecto.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.TotalCorrecto.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalCorrecto.Top = 6.96!
		Me.TotalCorrecto.Width = 66.0!
		'
		'TotalDIF
		'
		Me.TotalDIF.CanGrow = False
		Me.TotalDIF.DataField = ""
		Me.TotalDIF.Height = 16.32!
		Me.TotalDIF.Left = 426.0!
		Me.TotalDIF.Name = "TotalDIF"
		Me.TotalDIF.OutputFormat = "#,##0"
		Me.TotalDIF.Parent = Me.ReportFooter
		Me.TotalDIF.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalDIF.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalDIF.Top = 6.96!
		Me.TotalDIF.Width = 54.0!
		'
		'TotalConteo2
		'
		Me.TotalConteo2.CanGrow = False
		Me.TotalConteo2.DataField = ""
		Me.TotalConteo2.Height = 16.32!
		Me.TotalConteo2.Left = 366.0!
		Me.TotalConteo2.Name = "TotalConteo2"
		Me.TotalConteo2.OutputFormat = "#,##0"
		Me.TotalConteo2.Parent = Me.ReportFooter
		Me.TotalConteo2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalConteo2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalConteo2.Top = 6.96!
		Me.TotalConteo2.Width = 54.0!
		'
		'TotalConteo1
		'
		Me.TotalConteo1.CanGrow = False
		Me.TotalConteo1.DataField = ""
		Me.TotalConteo1.Height = 16.32!
		Me.TotalConteo1.Left = 306.0!
		Me.TotalConteo1.Name = "TotalConteo1"
		Me.TotalConteo1.OutputFormat = "#,##0"
		Me.TotalConteo1.Parent = Me.ReportFooter
		Me.TotalConteo1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.TotalConteo1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.TotalConteo1.Top = 6.96!
		Me.TotalConteo1.Width = 54.0!
		'
		'DbATLine27
		'
		Me.DbATLine27.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine27.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine27.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine27.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine27.LineWeight = 1.0!
		Me.DbATLine27.Name = "DbATLine27"
		Me.DbATLine27.Parent = Nothing
		Me.DbATLine27.X1 = 1034.88!
		Me.DbATLine27.X2 = 24.048!
		Me.DbATLine27.Y1 = 0.0!
		Me.DbATLine27.Y2 = 0.0!
		'
		'DbATLabel24
		'
		Me.DbATLabel24.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel24.Height = 18.0!
		Me.DbATLabel24.Left = 192.0!
		Me.DbATLabel24.Name = "DbATLabel24"
		Me.DbATLabel24.Parent = Nothing
		Me.DbATLabel24.Text = "TOTALES:"
		Me.DbATLabel24.Top = 6.0!
		Me.DbATLabel24.Width = 108.0!
		'
		'DbATLabel25
		'
		Me.DbATLabel25.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel25.Height = 18.0!
		Me.DbATLabel25.Left = 78.0!
		Me.DbATLabel25.Name = "DbATLabel25"
		Me.DbATLabel25.Parent = Me.PageHeader
		Me.DbATLabel25.Text = "UBICACI�N"
		Me.DbATLabel25.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel25.Top = 78.0!
		Me.DbATLabel25.Width = 66.0!
		'
		'DbATTextBox16
		'
		Me.DbATTextBox16.DataField = "Ubicacion"
		Me.DbATTextBox16.Height = 18.0!
		Me.DbATTextBox16.Left = 78.0!
		Me.DbATTextBox16.Name = "DbATTextBox16"
		Me.DbATTextBox16.OutputFormat = "#,##0.00"
		Me.DbATTextBox16.Parent = Me.Detail
		Me.DbATTextBox16.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!)
		Me.DbATTextBox16.Top = 0.0!
		Me.DbATTextBox16.Width = 54.0!
		'
		'RPT_DiferenciaEntreMarbetesC_
		'
		Me.ReportWidth = 1056.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.ReportHeader, Me.PageHeader, Me.Detail, Me.PageFooter, Me.ReportFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region
	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub


End Class


