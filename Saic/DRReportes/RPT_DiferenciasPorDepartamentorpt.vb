Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_DiferenciasPorDepartamentorpt
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Dim totalpaginas As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents DbATLabel41 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel42 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel45 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine14 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox13 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox14 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox15 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox18 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox19 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel39 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox11 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox12 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
	Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
	Private WithEvents DbATLabel37 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox10 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATPageBreak1 As dbAutoTrack.DataReports.dbATPageBreak
	Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtAlmacen As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox20 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel12 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox6 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATShape1 As dbAutoTrack.DataReports.dbATShape
	Private WithEvents DbATShape2 As dbAutoTrack.DataReports.dbATShape
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader
		Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtAlmacen = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel39 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLine14 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATLabel45 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel42 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel41 = New dbAutoTrack.DataReports.dbATLabel
		Me.Detail = New dbAutoTrack.DataReports.Detail
		Me.DbATTextBox20 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox12 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox11 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox19 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox18 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox15 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox14 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox13 = New dbAutoTrack.DataReports.dbATTextBox
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter
		Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader
		Me.DbATTextBox10 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel37 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATPageBreak1 = New dbAutoTrack.DataReports.dbATPageBreak
		Me.DbATLabel12 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATShape1 = New dbAutoTrack.DataReports.dbATShape
		Me.DbATShape2 = New dbAutoTrack.DataReports.dbATShape
		Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter
		Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox6 = New dbAutoTrack.DataReports.dbATTextBox
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel11, Me.DbATLabel10, Me.txtAlmacen, Me.DbATLabel9, Me.DbATLabel7, Me.DbATLabel5, Me.DbATLabel39, Me.DbATLabel8, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLabel3, Me.DbATLine14, Me.DbATLabel45, Me.DbATLabel42, Me.DbATLabel41})
		Me.PageHeader.Height = 87.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'DbATLabel11
		'
		Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel11.Height = 18.0!
		Me.DbATLabel11.Left = 6.0!
		Me.DbATLabel11.Name = "DbATLabel11"
		Me.DbATLabel11.Parent = Me.PageHeader
		Me.DbATLabel11.Text = "CBARRAS/MARBETE"
		Me.DbATLabel11.Top = 66.0!
		Me.DbATLabel11.Width = 120.048!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 18.0!
		Me.DbATLabel10.Left = 132.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.PageHeader
		Me.DbATLabel10.Text = "DESCRIPCION"
		Me.DbATLabel10.Top = 66.0!
		Me.DbATLabel10.Width = 246.0!
		'
		'txtAlmacen
		'
		Me.txtAlmacen.Height = 18.0!
		Me.txtAlmacen.Left = 840.0!
		Me.txtAlmacen.Name = "txtAlmacen"
		Me.txtAlmacen.OutputFormat = "h:mm AM/PM"
		Me.txtAlmacen.Parent = Me.PageHeader
		Me.txtAlmacen.Top = 42.0!
		Me.txtAlmacen.Width = 72.0!
		'
		'DbATLabel9
		'
		Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel9.Height = 18.0!
		Me.DbATLabel9.Left = 432.0!
		Me.DbATLabel9.Name = "DbATLabel9"
		Me.DbATLabel9.Parent = Me.PageHeader
		Me.DbATLabel9.Text = "TIENDA:"
		Me.DbATLabel9.Top = 42.0!
		Me.DbATLabel9.Width = 54.0!
		'
		'DbATLabel7
		'
		Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel7.Height = 18.0!
		Me.DbATLabel7.Left = 930.0!
		Me.DbATLabel7.Name = "DbATLabel7"
		Me.DbATLabel7.Parent = Me.PageHeader
		Me.DbATLabel7.Text = "DIF VENTA"
		Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel7.Top = 66.0!
		Me.DbATLabel7.Width = 77.952!
		'
		'DbATLabel5
		'
		Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel5.Height = 18.0!
		Me.DbATLabel5.Left = 600.0!
		Me.DbATLabel5.Name = "DbATLabel5"
		Me.DbATLabel5.Parent = Me.PageHeader
		Me.DbATLabel5.Text = "TEORICO"
		Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel5.Top = 66.0!
		Me.DbATLabel5.Width = 65.952!
		'
		'DbATLabel39
		'
		Me.DbATLabel39.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel39.Height = 18.0!
		Me.DbATLabel39.Left = 390.0!
		Me.DbATLabel39.Name = "DbATLabel39"
		Me.DbATLabel39.Parent = Me.PageHeader
		Me.DbATLabel39.Text = "PROVEEDOR"
		Me.DbATLabel39.Top = 66.0!
		Me.DbATLabel39.Width = 168.0!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 96.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.PageHeader
		Me.DbATLabel8.Text = "FECHA:"
		Me.DbATLabel8.Top = 42.0!
		Me.DbATLabel8.Width = 48.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 768.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "ALMACEN:"
		Me.DbATLabel2.Top = 42.0!
		Me.DbATLabel2.Width = 66.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 30.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "REPORTE DE DIFERENCIAS POR DEPARTAMENTO"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel1.Top = 6.0!
		Me.DbATLabel1.Width = 1002.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 144.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 42.0!
		Me.txtFecha.Width = 72.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 492.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 42.0!
		Me.txtTienda.Width = 270.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 270.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 42.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtHora
		'
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 318.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 42.0!
		Me.txtHora.Width = 72.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 936.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 42.0!
		Me.lblPage.Width = 96.0!
		'
		'DbATLabel3
		'
		Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel3.Height = 18.0!
		Me.DbATLabel3.Left = 30.0!
		Me.DbATLabel3.Name = "DbATLabel3"
		Me.DbATLabel3.Parent = Me.PageHeader
		Me.DbATLabel3.Text = "*PRECIOS VENTA VIGENTES EN SAP*"
		Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATLabel3.Top = 26.88!
		Me.DbATLabel3.Width = 1002.0!
		'
		'DbATLine14
		'
		Me.DbATLine14.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine14.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine14.LineWeight = 1.0!
		Me.DbATLine14.Name = "DbATLine14"
		Me.DbATLine14.Parent = Me.PageHeader
		Me.DbATLine14.X1 = 1044.0!
		Me.DbATLine14.X2 = 6.0!
		Me.DbATLine14.Y1 = 84.0!
		Me.DbATLine14.Y2 = 84.0!
		'
		'DbATLabel45
		'
		Me.DbATLabel45.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel45.Height = 18.0!
		Me.DbATLabel45.Left = 846.0!
		Me.DbATLabel45.Name = "DbATLabel45"
		Me.DbATLabel45.Parent = Me.PageHeader
		Me.DbATLabel45.Text = "DIF COSTO"
		Me.DbATLabel45.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel45.Top = 66.0!
		Me.DbATLabel45.Width = 77.952!
		'
		'DbATLabel42
		'
		Me.DbATLabel42.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel42.Height = 18.0!
		Me.DbATLabel42.Left = 762.0!
		Me.DbATLabel42.Name = "DbATLabel42"
		Me.DbATLabel42.Parent = Me.PageHeader
		Me.DbATLabel42.Text = "DIFERENCIA"
		Me.DbATLabel42.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel42.Top = 66.0!
		Me.DbATLabel42.Width = 77.952!
		'
		'DbATLabel41
		'
		Me.DbATLabel41.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel41.Height = 18.0!
		Me.DbATLabel41.Left = 690.0!
		Me.DbATLabel41.Name = "DbATLabel41"
		Me.DbATLabel41.Parent = Me.PageHeader
		Me.DbATLabel41.Text = "FISICO"
		Me.DbATLabel41.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel41.Top = 66.0!
		Me.DbATLabel41.Width = 65.952!
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox20, Me.DbATTextBox12, Me.DbATTextBox11, Me.DbATTextBox19, Me.DbATTextBox18, Me.DbATTextBox15, Me.DbATTextBox14, Me.DbATTextBox13})
		Me.Detail.Height = 17.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATTextBox20
		'
		Me.DbATTextBox20.DataField = "NOM_PROV"
		Me.DbATTextBox20.Height = 18.048!
		Me.DbATTextBox20.Left = 372.0!
		Me.DbATTextBox20.Name = "DbATTextBox20"
		Me.DbATTextBox20.Parent = Me.Detail
		Me.DbATTextBox20.Text = ""
		Me.DbATTextBox20.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox20.Top = 0.0!
		Me.DbATTextBox20.Width = 216.0!
		'
		'DbATTextBox12
		'
		Me.DbATTextBox12.DataField = "OBS"
		Me.DbATTextBox12.Height = 18.048!
		Me.DbATTextBox12.Left = 102.0!
		Me.DbATTextBox12.Name = "DbATTextBox12"
		Me.DbATTextBox12.Parent = Me.Detail
		Me.DbATTextBox12.Text = ""
		Me.DbATTextBox12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox12.Top = 0.0!
		Me.DbATTextBox12.Width = 245.952!
		'
		'DbATTextBox11
		'
		Me.DbATTextBox11.DataField = "EAN"
		Me.DbATTextBox11.Height = 18.0!
		Me.DbATTextBox11.Left = 5.952!
		Me.DbATTextBox11.Name = "DbATTextBox11"
		Me.DbATTextBox11.Parent = Me.Detail
		Me.DbATTextBox11.Text = ""
		Me.DbATTextBox11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox11.Top = 0.0!
		Me.DbATTextBox11.Width = 78.0!
		'
		'DbATTextBox19
		'
		Me.DbATTextBox19.DataField = "DIFVENTA"
		Me.DbATTextBox19.Height = 18.0!
		Me.DbATTextBox19.Left = 930.0!
		Me.DbATTextBox19.Name = "DbATTextBox19"
		Me.DbATTextBox19.OutputFormat = "#,##0.00"
		Me.DbATTextBox19.Parent = Me.Detail
		Me.DbATTextBox19.Text = ""
		Me.DbATTextBox19.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox19.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox19.Top = 0.0!
		Me.DbATTextBox19.Width = 78.0!
		'
		'DbATTextBox18
		'
		Me.DbATTextBox18.DataField = "DIFCOSTO"
		Me.DbATTextBox18.Height = 18.0!
		Me.DbATTextBox18.Left = 846.0!
		Me.DbATTextBox18.Name = "DbATTextBox18"
		Me.DbATTextBox18.OutputFormat = "#,##0.00"
		Me.DbATTextBox18.Parent = Me.Detail
		Me.DbATTextBox18.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox18.Top = 0.0!
		Me.DbATTextBox18.Width = 77.952!
		'
		'DbATTextBox15
		'
		Me.DbATTextBox15.DataField = "Conteo1"
		Me.DbATTextBox15.Height = 18.0!
		Me.DbATTextBox15.Left = 690.0!
		Me.DbATTextBox15.Name = "DbATTextBox15"
		Me.DbATTextBox15.OutputFormat = "#,##0"
		Me.DbATTextBox15.Parent = Me.Detail
		Me.DbATTextBox15.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox15.Top = 0.0!
		Me.DbATTextBox15.Width = 65.952!
		'
		'DbATTextBox14
		'
		Me.DbATTextBox14.DataField = "DIF"
		Me.DbATTextBox14.Height = 18.0!
		Me.DbATTextBox14.Left = 762.0!
		Me.DbATTextBox14.Name = "DbATTextBox14"
		Me.DbATTextBox14.OutputFormat = "#,##0.00"
		Me.DbATTextBox14.Parent = Me.Detail
		Me.DbATTextBox14.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox14.Top = 0.0!
		Me.DbATTextBox14.Width = 77.952!
		'
		'DbATTextBox13
		'
		Me.DbATTextBox13.DataField = "InventarioTeorico"
		Me.DbATTextBox13.Height = 18.0!
		Me.DbATTextBox13.Left = 606.0!
		Me.DbATTextBox13.Name = "DbATTextBox13"
		Me.DbATTextBox13.OutputFormat = "#,##0"
		Me.DbATTextBox13.Parent = Me.Detail
		Me.DbATTextBox13.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox13.Top = 0.0!
		Me.DbATTextBox13.Width = 65.952!
		'
		'PageFooter
		'
		Me.PageFooter.Height = 23.0!
		Me.PageFooter.Name = "PageFooter"
		'
		'GroupHeader1
		'
		Me.GroupHeader1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox10, Me.DbATLabel37, Me.DbATPageBreak1, Me.DbATLabel12, Me.DbATTextBox1, Me.DbATShape1, Me.DbATShape2})
		Me.GroupHeader1.Height = 29.0!
		Me.GroupHeader1.Index = 1
		Me.GroupHeader1.Name = "GroupHeader1"
		'
		'DbATTextBox10
		'
		Me.DbATTextBox10.DataField = "CODE_DEPTO"
		Me.DbATTextBox10.DistinctField = "CODE_DEPTO"
		Me.DbATTextBox10.Height = 18.0!
		Me.DbATTextBox10.Left = 210.0!
		Me.DbATTextBox10.Name = "DbATTextBox10"
		Me.DbATTextBox10.Parent = Me.GroupHeader1
		Me.DbATTextBox10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.DbATTextBox10.Top = 6.0!
		Me.DbATTextBox10.Width = 54.048!
		'
		'DbATLabel37
		'
		Me.DbATLabel37.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel37.Height = 18.0!
		Me.DbATLabel37.Left = 168.0!
		Me.DbATLabel37.Name = "DbATLabel37"
		Me.DbATLabel37.Parent = Me.GroupHeader1
		Me.DbATLabel37.Text = "DEPTO:"
		Me.DbATLabel37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.DbATLabel37.Top = 6.0!
		Me.DbATLabel37.Width = 42.0!
		'
		'DbATPageBreak1
		'
		Me.DbATPageBreak1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.Enabled = False
		Me.DbATPageBreak1.Name = "DbATPageBreak1"
		Me.DbATPageBreak1.Parent = Me.GroupHeader1
		Me.DbATPageBreak1.Top = 0.0!
		Me.DbATPageBreak1.Visible = False
		'
		'DbATLabel12
		'
		Me.DbATLabel12.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel12.Height = 18.0!
		Me.DbATLabel12.Left = 6.0!
		Me.DbATLabel12.Name = "DbATLabel12"
		Me.DbATLabel12.Parent = Me.GroupHeader1
		Me.DbATLabel12.Text = "Almacen:"
		Me.DbATLabel12.Top = 6.0!
		Me.DbATLabel12.Width = 72.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.DataField = "Almacen"
		Me.DbATTextBox1.Height = 18.0!
		Me.DbATTextBox1.Left = 78.0!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.Parent = Me.GroupHeader1
		Me.DbATTextBox1.Top = 6.0!
		Me.DbATTextBox1.Width = 78.0!
		'
		'DbATShape1
		'
		Me.DbATShape1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATShape1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATShape1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATShape1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATShape1.Height = 18.0!
		Me.DbATShape1.Left = 168.0!
		Me.DbATShape1.LineWeight = 1.0!
		Me.DbATShape1.Name = "DbATShape1"
		Me.DbATShape1.Parent = Me.GroupHeader1
		Me.DbATShape1.Top = 6.0!
		Me.DbATShape1.Width = 108.0!
		'
		'DbATShape2
		'
		Me.DbATShape2.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATShape2.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATShape2.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATShape2.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATShape2.Height = 18.0!
		Me.DbATShape2.Left = 6.0!
		Me.DbATShape2.LineWeight = 1.0!
		Me.DbATShape2.Name = "DbATShape2"
		Me.DbATShape2.Parent = Me.GroupHeader1
		Me.DbATShape2.Top = 6.0!
		Me.DbATShape2.Width = 150.0!
		'
		'GroupFooter1
		'
		Me.GroupFooter1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel6, Me.DbATTextBox2, Me.DbATTextBox3, Me.DbATTextBox4, Me.DbATTextBox5, Me.DbATTextBox6})
		Me.GroupFooter1.Height = 62.0!
		Me.GroupFooter1.Name = "GroupFooter1"
		'
		'DbATLabel6
		'
		Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel6.Height = 18.0!
		Me.DbATLabel6.Left = 30.0!
		Me.DbATLabel6.Name = "DbATLabel6"
		Me.DbATLabel6.Parent = Me.GroupFooter1
		Me.DbATLabel6.Text = "TOTALES"
		Me.DbATLabel6.Top = 0.0!
		Me.DbATLabel6.Width = 77.952!
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.DataField = "DIFVENTA"
		Me.DbATTextBox2.Height = 18.0!
		Me.DbATTextBox2.Left = 936.0!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.Parent = Me.GroupFooter1
		Me.DbATTextBox2.SummaryFunc = dbAutoTrack.DataReports.AggregateType.Sum
		Me.DbATTextBox2.SummaryRunning = dbAutoTrack.DataReports.SummaryRunning.Group
		Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox2.Top = 0.0!
		Me.DbATTextBox2.Width = 72.0!
		'
		'DbATTextBox3
		'
		Me.DbATTextBox3.DataField = "totalfisico"
		Me.DbATTextBox3.Height = 18.0!
		Me.DbATTextBox3.Left = 696.0!
		Me.DbATTextBox3.Name = "DbATTextBox3"
		Me.DbATTextBox3.Parent = Me.GroupFooter1
		Me.DbATTextBox3.SummaryFunc = dbAutoTrack.DataReports.AggregateType.Sum
		Me.DbATTextBox3.SummaryRunning = dbAutoTrack.DataReports.SummaryRunning.Group
		Me.DbATTextBox3.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox3.Top = 0.0!
		Me.DbATTextBox3.Width = 66.0!
		'
		'DbATTextBox4
		'
		Me.DbATTextBox4.DataField = "InventarioTeorico"
		Me.DbATTextBox4.Height = 18.0!
		Me.DbATTextBox4.Left = 618.0!
		Me.DbATTextBox4.Name = "DbATTextBox4"
		Me.DbATTextBox4.Parent = Me.GroupFooter1
		Me.DbATTextBox4.SummaryFunc = dbAutoTrack.DataReports.AggregateType.Sum
		Me.DbATTextBox4.SummaryRunning = dbAutoTrack.DataReports.SummaryRunning.Group
		Me.DbATTextBox4.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox4.Top = 0.0!
		Me.DbATTextBox4.Width = 60.0!
		'
		'DbATTextBox5
		'
		Me.DbATTextBox5.DataField = "DIF"
		Me.DbATTextBox5.Height = 18.0!
		Me.DbATTextBox5.Left = 768.0!
		Me.DbATTextBox5.Name = "DbATTextBox5"
		Me.DbATTextBox5.Parent = Me.GroupFooter1
		Me.DbATTextBox5.SummaryFunc = dbAutoTrack.DataReports.AggregateType.Sum
		Me.DbATTextBox5.SummaryRunning = dbAutoTrack.DataReports.SummaryRunning.Group
		Me.DbATTextBox5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox5.Top = 0.0!
		Me.DbATTextBox5.Width = 72.0!
		'
		'DbATTextBox6
		'
		Me.DbATTextBox6.DataField = "DIFCOSTO"
		Me.DbATTextBox6.Height = 18.0!
		Me.DbATTextBox6.Left = 852.0!
		Me.DbATTextBox6.Name = "DbATTextBox6"
		Me.DbATTextBox6.Parent = Me.GroupFooter1
		Me.DbATTextBox6.SummaryFunc = dbAutoTrack.DataReports.AggregateType.Sum
		Me.DbATTextBox6.SummaryRunning = dbAutoTrack.DataReports.SummaryRunning.Group
		Me.DbATTextBox6.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox6.Top = 0.0!
		Me.DbATTextBox6.Width = 72.0!
		'
		'RPT_DiferenciasPorDepartamento
		'
		Me.ReportWidth = 1056.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region
	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub

	Private Sub GroupFooter1_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.Initialize
		DbATPageBreak1.Enabled = True
		DbATPageBreak1.Visible = True
		'Total
		'Dim TotalInventarioTeorico_ As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("TotalInventarioTeorico"))
		'Me.TotalInventarioTeorico.Value = TotalInventarioTeorico_
		'Dim TotalConteo1_ As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("TotalConteo1"))
		'Me.TotalConteo1.Value = TotalConteo1_
		'Dim TotalDIF_ As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("TotalDIF"))
		'Me.TotalDIF.Value = TotalDIF_
		'Dim TotalDIFCOSTO_ As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("TotalDIFCOSTO"))
		'Me.TotalDIFCOSTO.Value = TotalDIFCOSTO_
		'Dim TotalDIFVENTA_ As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("TotalDIFVENTA"))
		' Me.TotalDIFVENTA.Value = TotalDIFVENTA_
	End Sub
End Class


