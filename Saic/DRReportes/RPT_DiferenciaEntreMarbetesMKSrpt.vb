Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_DiferenciaEntreMarbetesMKSrpt
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Dim totalpaginas As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel12 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox6 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox7 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel13 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox8 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox9 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader
		Me.DbATLabel13 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel12 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine
		Me.Detail = New dbAutoTrack.DataReports.Detail
		Me.DbATTextBox8 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox7 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox6 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter
		Me.DbATTextBox9 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel13, Me.DbATLabel12, Me.DbATLabel10, Me.DbATLabel9, Me.DbATLabel6, Me.DbATLabel5, Me.DbATLabel3, Me.DbATLabel7, Me.DbATLabel8, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLine1, Me.DbATLabel11})
		Me.PageHeader.Height = 82.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'DbATLabel13
		'
		Me.DbATLabel13.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel13.Height = 18.0!
		Me.DbATLabel13.Left = 684.0!
		Me.DbATLabel13.Name = "DbATLabel13"
		Me.DbATLabel13.Parent = Me.PageHeader
		Me.DbATLabel13.Text = "ALMACEN"
		Me.DbATLabel13.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel13.Top = 60.0!
		Me.DbATLabel13.Width = 66.0!
		'
		'DbATLabel12
		'
		Me.DbATLabel12.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel12.Height = 18.0!
		Me.DbATLabel12.Left = 6.0!
		Me.DbATLabel12.Name = "DbATLabel12"
		Me.DbATLabel12.Parent = Me.PageHeader
		Me.DbATLabel12.Text = "MARBETE"
		Me.DbATLabel12.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel12.Top = 60.0!
		Me.DbATLabel12.Width = 75.6!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 18.0!
		Me.DbATLabel10.Left = 776.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.PageHeader
		Me.DbATLabel10.Text = "CONTEO 3"
		Me.DbATLabel10.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel10.Top = 60.0!
		Me.DbATLabel10.Visible = False
		Me.DbATLabel10.Width = 36.048!
		'
		'DbATLabel9
		'
		Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel9.Height = 18.0!
		Me.DbATLabel9.Left = 626.0!
		Me.DbATLabel9.Name = "DbATLabel9"
		Me.DbATLabel9.Parent = Me.PageHeader
		Me.DbATLabel9.Text = "DIF"
		Me.DbATLabel9.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel9.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel9.Top = 60.0!
		Me.DbATLabel9.Width = 42.048!
		'
		'DbATLabel6
		'
		Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel6.Height = 18.0!
		Me.DbATLabel6.Left = 512.0!
		Me.DbATLabel6.Name = "DbATLabel6"
		Me.DbATLabel6.Parent = Me.PageHeader
		Me.DbATLabel6.Text = "CONTEO 2"
		Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel6.Top = 60.0!
		Me.DbATLabel6.Width = 90.048!
		'
		'DbATLabel5
		'
		Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel5.Height = 18.0!
		Me.DbATLabel5.Left = 410.0!
		Me.DbATLabel5.Name = "DbATLabel5"
		Me.DbATLabel5.Parent = Me.PageHeader
		Me.DbATLabel5.Text = "CONTEO 1"
		Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel5.Top = 60.0!
		Me.DbATLabel5.Width = 90.048!
		'
		'DbATLabel3
		'
		Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel3.Height = 18.0!
		Me.DbATLabel3.Left = 252.0!
		Me.DbATLabel3.Name = "DbATLabel3"
		Me.DbATLabel3.Parent = Me.PageHeader
		Me.DbATLabel3.Text = "DESCRIPCION"
		Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel3.Top = 60.0!
		Me.DbATLabel3.Width = 156.0!
		'
		'DbATLabel7
		'
		Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel7.Height = 18.0!
		Me.DbATLabel7.Left = 174.0!
		Me.DbATLabel7.Name = "DbATLabel7"
		Me.DbATLabel7.Parent = Me.PageHeader
		Me.DbATLabel7.Text = "CBARRAS"
		Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel7.Top = 60.0!
		Me.DbATLabel7.Width = 77.952!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 120.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.PageHeader
		Me.DbATLabel8.Text = "FECHA:"
		Me.DbATLabel8.Top = 36.0!
		Me.DbATLabel8.Width = 48.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 384.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "TIENDA:"
		Me.DbATLabel2.Top = 36.0!
		Me.DbATLabel2.Width = 54.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 6.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "DIFERENCIA ENTRE MARBETES"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel1.Top = 12.0!
		Me.DbATLabel1.Width = 786.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 168.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 36.0!
		Me.txtFecha.Width = 72.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 438.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 36.0!
		Me.txtTienda.Width = 210.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 252.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 36.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtHora
		'
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 300.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 36.0!
		Me.txtHora.Width = 72.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 696.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 36.0!
		Me.lblPage.Width = 96.0!
		'
		'DbATLine1
		'
		Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine1.LineWeight = 1.0!
		Me.DbATLine1.Name = "DbATLine1"
		Me.DbATLine1.Parent = Me.PageHeader
		Me.DbATLine1.X1 = 792.0!
		Me.DbATLine1.X2 = 6.0!
		Me.DbATLine1.Y1 = 78.0!
		Me.DbATLine1.Y2 = 78.0!
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox8, Me.DbATTextBox7, Me.DbATTextBox6, Me.DbATTextBox5, Me.DbATTextBox4, Me.DbATTextBox3, Me.DbATTextBox2, Me.DbATTextBox1, Me.DbATTextBox9})
		Me.Detail.Height = 20.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATTextBox8
		'
		Me.DbATTextBox8.DataField = "Almacen"
		Me.DbATTextBox8.Height = 18.0!
		Me.DbATTextBox8.Left = 666.0!
		Me.DbATTextBox8.Name = "DbATTextBox8"
		Me.DbATTextBox8.OutputFormat = "#,##0"
		Me.DbATTextBox8.Parent = Me.Detail
		Me.DbATTextBox8.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox8.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox8.Top = 0.0!
		Me.DbATTextBox8.Width = 90.0!
		'
		'DbATTextBox7
		'
		Me.DbATTextBox7.DataField = "Conteo3"
		Me.DbATTextBox7.Height = 18.0!
		Me.DbATTextBox7.Left = 788.0!
		Me.DbATTextBox7.Name = "DbATTextBox7"
		Me.DbATTextBox7.OutputFormat = "#,##0"
		Me.DbATTextBox7.Parent = Me.Detail
		Me.DbATTextBox7.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox7.Top = 0.0!
		Me.DbATTextBox7.Visible = False
		Me.DbATTextBox7.Width = 30.0!
		'
		'DbATTextBox6
		'
		Me.DbATTextBox6.DataField = "DIF"
		Me.DbATTextBox6.Height = 18.0!
		Me.DbATTextBox6.Left = 590.0!
		Me.DbATTextBox6.Name = "DbATTextBox6"
		Me.DbATTextBox6.OutputFormat = "#,##0"
		Me.DbATTextBox6.Parent = Me.Detail
		Me.DbATTextBox6.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox6.Top = 0.0!
		Me.DbATTextBox6.Width = 90.0!
		'
		'DbATTextBox5
		'
		Me.DbATTextBox5.DataField = "Conteo2"
		Me.DbATTextBox5.Height = 18.0!
		Me.DbATTextBox5.Left = 524.0!
		Me.DbATTextBox5.Name = "DbATTextBox5"
		Me.DbATTextBox5.OutputFormat = "#,##0"
		Me.DbATTextBox5.Parent = Me.Detail
		Me.DbATTextBox5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox5.Top = 0.0!
		Me.DbATTextBox5.Width = 90.0!
		'
		'DbATTextBox4
		'
		Me.DbATTextBox4.DataField = "Conteo1"
		Me.DbATTextBox4.Height = 18.0!
		Me.DbATTextBox4.Left = 410.0!
		Me.DbATTextBox4.Name = "DbATTextBox4"
		Me.DbATTextBox4.OutputFormat = "#,##0"
		Me.DbATTextBox4.Parent = Me.Detail
		Me.DbATTextBox4.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox4.Top = 0.0!
		Me.DbATTextBox4.Width = 90.0!
		'
		'DbATTextBox3
		'
		Me.DbATTextBox3.DataField = "OBS"
		Me.DbATTextBox3.Height = 18.0!
		Me.DbATTextBox3.Left = 258.0!
		Me.DbATTextBox3.Name = "DbATTextBox3"
		Me.DbATTextBox3.Parent = Me.Detail
		Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox3.Top = 0.0!
		Me.DbATTextBox3.Width = 144.0!
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.DataField = "EAN"
		Me.DbATTextBox2.Height = 18.0!
		Me.DbATTextBox2.Left = 168.0!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.Parent = Me.Detail
		Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
		Me.DbATTextBox2.Top = 0.0!
		Me.DbATTextBox2.Width = 78.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.DataField = "IDMARBETE"
		Me.DbATTextBox1.Height = 18.0!
		Me.DbATTextBox1.Left = 12.0!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.Parent = Me.Detail
		Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.2!)
		Me.DbATTextBox1.Top = 0.0!
		Me.DbATTextBox1.Width = 105.6!
		'
		'PageFooter
		'
		Me.PageFooter.Height = 21.0!
		Me.PageFooter.Name = "PageFooter"
		'
		'DbATTextBox9
		'
		Me.DbATTextBox9.DataField = "Ubicacion"
		Me.DbATTextBox9.Height = 18.0!
		Me.DbATTextBox9.Left = 96.0!
		Me.DbATTextBox9.Name = "DbATTextBox9"
		Me.DbATTextBox9.Parent = Me.Detail
		Me.DbATTextBox9.Top = 0.0!
		Me.DbATTextBox9.Width = 72.0!
		'
		'DbATLabel11
		'
		Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel11.Height = 18.0!
		Me.DbATLabel11.Left = 96.0!
		Me.DbATLabel11.Name = "DbATLabel11"
		Me.DbATLabel11.Parent = Me.PageHeader
		Me.DbATLabel11.Text = "UBICACION"
		Me.DbATLabel11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATLabel11.Top = 60.0!
		Me.DbATLabel11.Width = 84.0!
		'
		'RPT_DiferenciaEntreMarbetesMKS
		'
		Me.ReportWidth = 816.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.Detail, Me.PageFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region
	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub
End Class


