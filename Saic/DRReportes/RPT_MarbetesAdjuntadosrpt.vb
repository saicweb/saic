Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_MarbetesAdjuntadosrpt
    Inherits dbAutoTrack.DataReports.Design.dbATReport

#Region " Component Designer generated code "

    Public Sub New(Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the DataReport Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the DataReport Designer
    Private components As System.ComponentModel.IContainer
   
    'NOTE: The following procedure is required by the DataReport Designer
    'It can be modified using the DataReport Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
    Private WithEvents DbATLabel14 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLine2 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLine16 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLabel37 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel43 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel16 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLine4 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents TxtIdUsuario As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents ReportHeader As dbAutoTrack.DataReports.ReportHeader
    Private WithEvents ReportFooter As dbAutoTrack.DataReports.ReportFooter
    Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
    Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
    Private WithEvents DbATLabel13 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel12 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtTotalMarbetes As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtTotalUnidades As dbAutoTrack.DataReports.dbATTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PageHeader = New dbAutoTrack.DataReports.PageHeader
        Me.TxtIdUsuario = New dbAutoTrack.DataReports.dbATTextBox
        Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel16 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel43 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel37 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLine16 = New dbAutoTrack.DataReports.dbATLine
        Me.DbATLine2 = New dbAutoTrack.DataReports.dbATLine
        Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel
        Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox
        Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox
        Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel
        Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox
        Me.lblPage = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel14 = New dbAutoTrack.DataReports.dbATLabel
        Me.Detail = New dbAutoTrack.DataReports.Detail
        Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox
        Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox
        Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox
        Me.PageFooter = New dbAutoTrack.DataReports.PageFooter
        Me.DbATLine4 = New dbAutoTrack.DataReports.dbATLine
        Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine
        Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel
        Me.ReportHeader = New dbAutoTrack.DataReports.ReportHeader
        Me.ReportFooter = New dbAutoTrack.DataReports.ReportFooter
        Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader
        Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter
        Me.txtTotalUnidades = New dbAutoTrack.DataReports.dbATTextBox
        Me.txtTotalMarbetes = New dbAutoTrack.DataReports.dbATTextBox
        Me.DbATLabel13 = New dbAutoTrack.DataReports.dbATLabel
        Me.DbATLabel12 = New dbAutoTrack.DataReports.dbATLabel
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.TxtIdUsuario, Me.DbATLabel3, Me.DbATLabel16, Me.DbATLabel43, Me.DbATLabel37, Me.DbATLine16, Me.DbATLine2, Me.DbATLabel8, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLabel14})
        Me.PageHeader.Height = 167.0!
        Me.PageHeader.Name = "PageHeader"
        '
        'TxtIdUsuario
        '
        Me.TxtIdUsuario.DataField = "IdUsuario"
        Me.TxtIdUsuario.Height = 18.0!
        Me.TxtIdUsuario.Left = 714.0!
        Me.TxtIdUsuario.Name = "TxtIdUsuario"
        Me.TxtIdUsuario.Parent = Me.PageHeader
        Me.TxtIdUsuario.Top = 144.0!
        Me.TxtIdUsuario.Width = 72.0!
        '
        'DbATLabel3
        '
        Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel3.Height = 18.0!
        Me.DbATLabel3.Left = 90.0!
        Me.DbATLabel3.Name = "DbATLabel3"
        Me.DbATLabel3.Parent = Me.PageHeader
        Me.DbATLabel3.Text = "FECHA:"
        Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel3.Top = 60.0!
        Me.DbATLabel3.Width = 48.0!
        '
        'DbATLabel16
        '
        Me.DbATLabel16.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel16.Height = 18.0!
        Me.DbATLabel16.Left = 0.0!
        Me.DbATLabel16.Name = "DbATLabel16"
        Me.DbATLabel16.Parent = Me.PageHeader
        Me.DbATLabel16.Text = "MARBETE"
        Me.DbATLabel16.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATLabel16.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel16.Top = 144.0!
        Me.DbATLabel16.Width = 162.0!
        '
        'DbATLabel43
        '
        Me.DbATLabel43.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel43.Height = 18.0!
        Me.DbATLabel43.Left = 192.0!
        Me.DbATLabel43.Name = "DbATLabel43"
        Me.DbATLabel43.Parent = Me.PageHeader
        Me.DbATLabel43.Text = "T. CODIGOS"
        Me.DbATLabel43.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel43.Top = 144.0!
        Me.DbATLabel43.Width = 126.0!
        '
        'DbATLabel37
        '
        Me.DbATLabel37.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel37.Height = 18.0!
        Me.DbATLabel37.Left = 402.0!
        Me.DbATLabel37.Name = "DbATLabel37"
        Me.DbATLabel37.Parent = Me.PageHeader
        Me.DbATLabel37.Text = "T. UNIDADES"
        Me.DbATLabel37.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel37.Top = 144.0!
        Me.DbATLabel37.Width = 102.0!
        '
        'DbATLine16
        '
        Me.DbATLine16.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine16.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine16.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine16.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine16.LineWeight = 1.0!
        Me.DbATLine16.Name = "DbATLine16"
        Me.DbATLine16.Parent = Me.PageHeader
        Me.DbATLine16.X1 = 792.72!
        Me.DbATLine16.X2 = 12.0!
        Me.DbATLine16.Y1 = 164.88!
        Me.DbATLine16.Y2 = 164.88!
        '
        'DbATLine2
        '
        Me.DbATLine2.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine2.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine2.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine2.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine2.LineWeight = 1.0!
        Me.DbATLine2.Name = "DbATLine2"
        Me.DbATLine2.Parent = Me.PageHeader
        Me.DbATLine2.X1 = 258.0!
        Me.DbATLine2.X2 = 30.0!
        Me.DbATLine2.Y1 = 126.0!
        Me.DbATLine2.Y2 = 126.0!
        '
        'DbATLabel8
        '
        Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel8.Height = 18.0!
        Me.DbATLabel8.Left = 642.0!
        Me.DbATLabel8.Name = "DbATLabel8"
        Me.DbATLabel8.Parent = Me.PageHeader
        Me.DbATLabel8.Text = "USUARIO:"
        Me.DbATLabel8.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel8.Top = 144.0!
        Me.DbATLabel8.Width = 72.0!
        '
        'DbATLabel2
        '
        Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel2.Height = 18.0!
        Me.DbATLabel2.Left = 384.0!
        Me.DbATLabel2.Name = "DbATLabel2"
        Me.DbATLabel2.Parent = Me.PageHeader
        Me.DbATLabel2.Text = "TIENDA:"
        Me.DbATLabel2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel2.Top = 60.0!
        Me.DbATLabel2.Width = 54.0!
        '
        'DbATLabel1
        '
        Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel1.Height = 18.0!
        Me.DbATLabel1.Left = 30.0!
        Me.DbATLabel1.Name = "DbATLabel1"
        Me.DbATLabel1.Parent = Me.PageHeader
        Me.DbATLabel1.Text = "MARBETES ADJUNTADOS"
        Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel1.Top = 18.0!
        Me.DbATLabel1.Width = 756.0!
        '
        'txtFecha
        '
        Me.txtFecha.DataField = "Fecha"
        Me.txtFecha.Height = 18.0!
        Me.txtFecha.Left = 138.0!
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Parent = Me.PageHeader
        Me.txtFecha.Top = 60.0!
        Me.txtFecha.Width = 72.0!
        '
        'txtTienda
        '
        Me.txtTienda.DataField = "Tienda"
        Me.txtTienda.Height = 18.0!
        Me.txtTienda.Left = 438.0!
        Me.txtTienda.Name = "txtTienda"
        Me.txtTienda.OutputFormat = "h:mm AM/PM"
        Me.txtTienda.Parent = Me.PageHeader
        Me.txtTienda.Top = 60.0!
        Me.txtTienda.Width = 246.0!
        '
        'DbATLabel4
        '
        Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel4.Height = 18.0!
        Me.DbATLabel4.Left = 228.0!
        Me.DbATLabel4.Name = "DbATLabel4"
        Me.DbATLabel4.Parent = Me.PageHeader
        Me.DbATLabel4.Text = "HORA:"
        Me.DbATLabel4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel4.Top = 60.0!
        Me.DbATLabel4.Width = 48.0!
        '
        'txtHora
        '
        Me.txtHora.Height = 18.0!
        Me.txtHora.Left = 276.0!
        Me.txtHora.Name = "txtHora"
        Me.txtHora.OutputFormat = "h:mm AM/PM"
        Me.txtHora.Parent = Me.PageHeader
        Me.txtHora.Top = 60.0!
        Me.txtHora.Width = 72.0!
        '
        'lblPage
        '
        Me.lblPage.ForeColor = System.Drawing.Color.Black
        Me.lblPage.Height = 18.0!
        Me.lblPage.Left = 690.0!
        Me.lblPage.Name = "lblPage"
        Me.lblPage.Parent = Me.PageHeader
        Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPage.Top = 60.0!
        Me.lblPage.Width = 96.0!
        '
        'DbATLabel14
        '
        Me.DbATLabel14.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel14.Height = 18.0!
        Me.DbATLabel14.Left = 30.0!
        Me.DbATLabel14.Name = "DbATLabel14"
        Me.DbATLabel14.Parent = Me.PageHeader
        Me.DbATLabel14.Text = "RESUMEN DE INVENTARIO"
        Me.DbATLabel14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel14.Top = 107.56!
        Me.DbATLabel14.Width = 228.0!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox3, Me.DbATTextBox2, Me.DbATTextBox1})
        Me.Detail.Height = 22.0!
        Me.Detail.Name = "Detail"
        '
        'DbATTextBox3
        '
        Me.DbATTextBox3.DataField = "Codigos"
        Me.DbATTextBox3.Height = 18.0!
        Me.DbATTextBox3.Left = 198.0!
        Me.DbATTextBox3.Name = "DbATTextBox3"
        Me.DbATTextBox3.Parent = Me.Detail
        Me.DbATTextBox3.Text = ""
        Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.DbATTextBox3.Top = 0.0!
        Me.DbATTextBox3.Width = 48.0!
        '
        'DbATTextBox2
        '
        Me.DbATTextBox2.DataField = "IdMarbete"
        Me.DbATTextBox2.Height = 18.0!
        Me.DbATTextBox2.Left = 36.0!
        Me.DbATTextBox2.Name = "DbATTextBox2"
        Me.DbATTextBox2.Parent = Me.Detail
        Me.DbATTextBox2.Text = ""
        Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.DbATTextBox2.Top = 0.0!
        Me.DbATTextBox2.Width = 126.0!
        '
        'DbATTextBox1
        '
        Me.DbATTextBox1.DataField = "Unidades"
        Me.DbATTextBox1.Height = 18.0!
        Me.DbATTextBox1.Left = 408.0!
        Me.DbATTextBox1.Name = "DbATTextBox1"
        Me.DbATTextBox1.Parent = Me.Detail
        Me.DbATTextBox1.Text = ""
        Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.DbATTextBox1.Top = 0.0!
        Me.DbATTextBox1.Width = 90.0!
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLine4, Me.DbATLabel6, Me.DbATLine1, Me.DbATLabel5})
        Me.PageFooter.Height = 61.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'DbATLine4
        '
        Me.DbATLine4.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine4.LineWeight = 1.0!
        Me.DbATLine4.Name = "DbATLine4"
        Me.DbATLine4.Parent = Me.PageFooter
        Me.DbATLine4.X1 = 336.048!
        Me.DbATLine4.X2 = 144.0!
        Me.DbATLine4.Y1 = 30.0!
        Me.DbATLine4.Y2 = 30.0!
        '
        'DbATLabel6
        '
        Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel6.Height = 18.0!
        Me.DbATLabel6.Left = 480.0!
        Me.DbATLabel6.Name = "DbATLabel6"
        Me.DbATLabel6.Parent = Me.PageFooter
        Me.DbATLabel6.Text = "FIRMA CONTADOR TIENDA"
        Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.DbATLabel6.Top = 31.2!
        Me.DbATLabel6.Width = 192.0!
        '
        'DbATLine1
        '
        Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine1.LineWeight = 1.0!
        Me.DbATLine1.Name = "DbATLine1"
        Me.DbATLine1.Parent = Me.PageFooter
        Me.DbATLine1.X1 = 672.048!
        Me.DbATLine1.X2 = 480.0!
        Me.DbATLine1.Y1 = 30.0!
        Me.DbATLine1.Y2 = 30.0!
        '
        'DbATLabel5
        '
        Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel5.Height = 18.0!
        Me.DbATLabel5.Left = 144.0!
        Me.DbATLabel5.Name = "DbATLabel5"
        Me.DbATLabel5.Parent = Me.PageFooter
        Me.DbATLabel5.Text = "FIRMA CONTADOR ADI"
        Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.DbATLabel5.Top = 31.2!
        Me.DbATLabel5.Width = 192.0!
        '
        'ReportHeader
        '
        Me.ReportHeader.Height = 0.0!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'ReportFooter
        '
        Me.ReportFooter.Height = 0.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Height = 0.0!
        Me.GroupHeader1.Index = 2
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel12, Me.DbATLabel13, Me.txtTotalMarbetes, Me.txtTotalUnidades})
        Me.GroupFooter1.Height = 22.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'txtTotalUnidades
        '
        Me.txtTotalUnidades.DataField = "TotalUnidades"
        Me.txtTotalUnidades.Height = 18.0!
        Me.txtTotalUnidades.Left = 408.0!
        Me.txtTotalUnidades.Name = "txtTotalUnidades"
        Me.txtTotalUnidades.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.txtTotalUnidades.Parent = Me.GroupFooter1
        Me.txtTotalUnidades.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.txtTotalUnidades.Top = 0.0!
        Me.txtTotalUnidades.Width = 84.0!
        '
        'txtTotalMarbetes
        '
        Me.txtTotalMarbetes.DataField = "TotalMarbetes"
        Me.txtTotalMarbetes.Height = 18.0!
        Me.txtTotalMarbetes.Left = 192.0!
        Me.txtTotalMarbetes.Name = "txtTotalMarbetes"
        Me.txtTotalMarbetes.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.txtTotalMarbetes.Parent = Me.GroupFooter1
        Me.txtTotalMarbetes.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.txtTotalMarbetes.Top = 0.0!
        Me.txtTotalMarbetes.Width = 48.0!
        '
        'DbATLabel13
        '
        Me.DbATLabel13.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel13.Height = 18.0!
        Me.DbATLabel13.Left = 246.0!
        Me.DbATLabel13.Name = "DbATLabel13"
        Me.DbATLabel13.Parent = Me.GroupFooter1
        Me.DbATLabel13.Text = "TOTAL DE UNIDADES:"
        Me.DbATLabel13.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATLabel13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel13.Top = 0.0!
        Me.DbATLabel13.Width = 156.0!
        '
        'DbATLabel12
        '
        Me.DbATLabel12.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel12.Height = 18.0!
        Me.DbATLabel12.Left = 6.0!
        Me.DbATLabel12.Name = "DbATLabel12"
        Me.DbATLabel12.Parent = Me.GroupFooter1
        Me.DbATLabel12.Text = "TOTAL DE MARBETES:"
        Me.DbATLabel12.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATLabel12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel12.Top = 0.0!
        Me.DbATLabel12.Width = 180.0!
        '
        'RPT_MarbetesAdjuntados
        '
        Me.ReportWidth = 817.0!
        Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.ReportHeader, Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter, Me.ReportFooter})
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

End Class


