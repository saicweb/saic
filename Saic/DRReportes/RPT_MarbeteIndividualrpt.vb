Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_MarbeteIndividualrpt
	Inherits dbAutoTrack.DataReports.Design.dbATReport
	Dim pageno As Integer = 1
	Dim conteo As Integer = 1
	Dim totalpaginas As Integer = 0
#Region " Component Designer generated code "

	Public Sub New(Container As System.ComponentModel.IContainer)
		MyClass.New()

		'Required for Windows.Forms Class Composition Designer support
		Container.Add(Me)
	End Sub

	Public Sub New()
		MyBase.New()

		'This call is required by the DataReport Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Component overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the DataReport Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the DataReport Designer
	'It can be modified using the DataReport Designer.
	'Do not modify it using the code editor.
	Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
	Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
	Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
	Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox6 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox7 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox8 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLine1 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATLabel12 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLabel13 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtTotalCantidad As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents txtConteoEAN As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATPageBreak1 As dbAutoTrack.DataReports.dbATPageBreak
	Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATLine3 As dbAutoTrack.DataReports.dbATLine
	Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel14 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel16 As dbAutoTrack.DataReports.dbATLabel
	Private WithEvents txtAlmacen As dbAutoTrack.DataReports.dbATTextBox
	Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.PageHeader = New dbAutoTrack.DataReports.PageHeader
		Me.DbATLabel16 = New dbAutoTrack.DataReports.dbATLabel
		Me.lblPage = New dbAutoTrack.DataReports.dbATLabel
		Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox
		Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel
		Me.Detail = New dbAutoTrack.DataReports.Detail
		Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox8 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox7 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATTextBox6 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLine1 = New dbAutoTrack.DataReports.dbATLine
		Me.PageFooter = New dbAutoTrack.DataReports.PageFooter
		Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader
		Me.DbATLabel14 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel
		Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATPageBreak1 = New dbAutoTrack.DataReports.dbATPageBreak
		Me.DbATLine3 = New dbAutoTrack.DataReports.dbATLine
		Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox
		Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter
		Me.txtTotalCantidad = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel13 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtConteoEAN = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel12 = New dbAutoTrack.DataReports.dbATLabel
		Me.txtAlmacen = New dbAutoTrack.DataReports.dbATTextBox
		Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel
		CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
		'
		'PageHeader
		'
		Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel16, Me.lblPage, Me.txtHora, Me.DbATLabel4, Me.txtTienda, Me.txtFecha, Me.DbATLabel1, Me.DbATLabel2})
		Me.PageHeader.Height = 63.0!
		Me.PageHeader.Name = "PageHeader"
		'
		'DbATLabel16
		'
		Me.DbATLabel16.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel16.Height = 18.0!
		Me.DbATLabel16.Left = 30.0!
		Me.DbATLabel16.Name = "DbATLabel16"
		Me.DbATLabel16.Parent = Me.PageHeader
		Me.DbATLabel16.Text = "FECHA:"
		Me.DbATLabel16.Top = 30.0!
		Me.DbATLabel16.Width = 48.0!
		'
		'lblPage
		'
		Me.lblPage.ForeColor = System.Drawing.Color.Black
		Me.lblPage.Height = 18.0!
		Me.lblPage.Left = 690.0!
		Me.lblPage.Name = "lblPage"
		Me.lblPage.Parent = Me.PageHeader
		Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.lblPage.Top = 30.0!
		Me.lblPage.Width = 96.0!
		'
		'txtHora
		'
		Me.txtHora.Height = 18.0!
		Me.txtHora.Left = 204.0!
		Me.txtHora.Name = "txtHora"
		Me.txtHora.OutputFormat = "h:mm AM/PM"
		Me.txtHora.Parent = Me.PageHeader
		Me.txtHora.Top = 30.0!
		Me.txtHora.Width = 72.0!
		'
		'DbATLabel4
		'
		Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel4.Height = 18.0!
		Me.DbATLabel4.Left = 156.0!
		Me.DbATLabel4.Name = "DbATLabel4"
		Me.DbATLabel4.Parent = Me.PageHeader
		Me.DbATLabel4.Text = "HORA:"
		Me.DbATLabel4.Top = 30.0!
		Me.DbATLabel4.Width = 48.0!
		'
		'txtTienda
		'
		Me.txtTienda.DataField = "Tienda"
		Me.txtTienda.Height = 18.0!
		Me.txtTienda.Left = 336.0!
		Me.txtTienda.Name = "txtTienda"
		Me.txtTienda.OutputFormat = "h:mm AM/PM"
		Me.txtTienda.Parent = Me.PageHeader
		Me.txtTienda.Top = 30.0!
		Me.txtTienda.Width = 210.0!
		'
		'txtFecha
		'
		Me.txtFecha.DataField = "Fecha"
		Me.txtFecha.Height = 18.0!
		Me.txtFecha.Left = 78.0!
		Me.txtFecha.Name = "txtFecha"
		Me.txtFecha.Parent = Me.PageHeader
		Me.txtFecha.Top = 30.0!
		Me.txtFecha.Width = 72.0!
		'
		'DbATLabel1
		'
		Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel1.Height = 18.0!
		Me.DbATLabel1.Left = 30.0!
		Me.DbATLabel1.Name = "DbATLabel1"
		Me.DbATLabel1.Parent = Me.PageHeader
		Me.DbATLabel1.Text = "REPORTE INDIVIDUAL DE MARBETES"
		Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel1.Top = 12.0!
		Me.DbATLabel1.Width = 756.0!
		'
		'DbATLabel2
		'
		Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel2.Height = 18.0!
		Me.DbATLabel2.Left = 282.0!
		Me.DbATLabel2.Name = "DbATLabel2"
		Me.DbATLabel2.Parent = Me.PageHeader
		Me.DbATLabel2.Text = "TIENDA:"
		Me.DbATLabel2.Top = 30.0!
		Me.DbATLabel2.Width = 54.0!
		'
		'Detail
		'
		Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox3, Me.DbATTextBox2, Me.DbATTextBox8, Me.DbATTextBox7, Me.DbATTextBox6, Me.DbATLine1})
		Me.Detail.Height = 24.0!
		Me.Detail.KeepTogether = True
		Me.Detail.Name = "Detail"
		'
		'DbATTextBox3
		'
		Me.DbATTextBox3.DataField = "code_depto"
		Me.DbATTextBox3.Height = 18.0!
		Me.DbATTextBox3.Left = 612.0!
		Me.DbATTextBox3.Name = "DbATTextBox3"
		Me.DbATTextBox3.OutputFormat = "#,##0.00;(#,##0.00)"
		Me.DbATTextBox3.Parent = Me.Detail
		Me.DbATTextBox3.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox3.Top = 0.0!
		Me.DbATTextBox3.Width = 48.0!
		'
		'DbATTextBox2
		'
		Me.DbATTextBox2.DataField = "CANTIDAD"
		Me.DbATTextBox2.Height = 18.0!
		Me.DbATTextBox2.Left = 438.0!
		Me.DbATTextBox2.Name = "DbATTextBox2"
		Me.DbATTextBox2.OutputFormat = "#,##0.00;(#,##0.00)"
		Me.DbATTextBox2.Parent = Me.Detail
		Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox2.Top = 0.0!
		Me.DbATTextBox2.Width = 72.0!
		'
		'DbATTextBox8
		'
		Me.DbATTextBox8.DataField = "EAN"
		Me.DbATTextBox8.Height = 18.0!
		Me.DbATTextBox8.Left = 6.0!
		Me.DbATTextBox8.Name = "DbATTextBox8"
		Me.DbATTextBox8.Parent = Me.Detail
		Me.DbATTextBox8.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATTextBox8.Top = 0.0!
		Me.DbATTextBox8.Width = 108.0!
		'
		'DbATTextBox7
		'
		Me.DbATTextBox7.DataField = "DESCRIPCION"
		Me.DbATTextBox7.Height = 18.0!
		Me.DbATTextBox7.Left = 120.0!
		Me.DbATTextBox7.Name = "DbATTextBox7"
		Me.DbATTextBox7.Parent = Me.Detail
		Me.DbATTextBox7.Top = 0.0!
		Me.DbATTextBox7.Width = 312.0!
		'
		'DbATTextBox6
		'
		Me.DbATTextBox6.DataField = "nom_depto"
		Me.DbATTextBox6.Height = 18.0!
		Me.DbATTextBox6.Left = 666.0!
		Me.DbATTextBox6.Name = "DbATTextBox6"
		Me.DbATTextBox6.OutputFormat = "#,##0.00;(#,##0.00)"
		Me.DbATTextBox6.Parent = Me.Detail
		Me.DbATTextBox6.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATTextBox6.Top = 0.0!
		Me.DbATTextBox6.Width = 86.0!
		'
		'DbATLine1
		'
		Me.DbATLine1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine1.LineWeight = 1.0!
		Me.DbATLine1.Name = "DbATLine1"
		Me.DbATLine1.Parent = Me.Detail
		Me.DbATLine1.X1 = 516.0!
		Me.DbATLine1.X2 = 606.0!
		Me.DbATLine1.Y1 = 18.0!
		Me.DbATLine1.Y2 = 18.0!
		'
		'PageFooter
		'
		Me.PageFooter.Height = 21.0!
		Me.PageFooter.Name = "PageFooter"
		'
		'GroupHeader1
		'
		Me.GroupHeader1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel3, Me.txtAlmacen, Me.DbATLabel14, Me.DbATLabel11, Me.DbATLabel10, Me.DbATLabel9, Me.DbATLabel8, Me.DbATLabel6, Me.DbATLabel5, Me.DbATLabel7, Me.DbATTextBox4, Me.DbATPageBreak1, Me.DbATLine3, Me.DbATTextBox1})
		Me.GroupHeader1.Height = 69.0!
		Me.GroupHeader1.Index = 1
		Me.GroupHeader1.KeepTogether = True
		Me.GroupHeader1.Name = "GroupHeader1"
		'
		'DbATLabel14
		'
		Me.DbATLabel14.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel14.Height = 18.0!
		Me.DbATLabel14.Left = 438.0!
		Me.DbATLabel14.Name = "DbATLabel14"
		Me.DbATLabel14.Parent = Me.GroupHeader1
		Me.DbATLabel14.Text = "CANTIDAD"
		Me.DbATLabel14.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel14.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel14.Top = 48.0!
		Me.DbATLabel14.Width = 72.0!
		'
		'DbATLabel11
		'
		Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel11.Height = 18.0!
		Me.DbATLabel11.Left = 324.0!
		Me.DbATLabel11.Name = "DbATLabel11"
		Me.DbATLabel11.Parent = Me.GroupHeader1
		Me.DbATLabel11.Text = "EXISTENCIA FISICA"
		Me.DbATLabel11.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel11.Top = 30.0!
		Me.DbATLabel11.Width = 174.0!
		'
		'DbATLabel10
		'
		Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel10.Height = 18.0!
		Me.DbATLabel10.Left = 516.0!
		Me.DbATLabel10.Name = "DbATLabel10"
		Me.DbATLabel10.Parent = Me.GroupHeader1
		Me.DbATLabel10.Text = "RECUENTO"
		Me.DbATLabel10.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel10.Top = 48.0!
		Me.DbATLabel10.Width = 90.0!
		'
		'DbATLabel9
		'
		Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel9.Height = 18.0!
		Me.DbATLabel9.Left = 630.0!
		Me.DbATLabel9.Name = "DbATLabel9"
		Me.DbATLabel9.Parent = Me.GroupHeader1
		Me.DbATLabel9.Text = "NOMBRE"
		Me.DbATLabel9.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel9.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel9.Top = 48.0!
		Me.DbATLabel9.Width = 132.0!
		'
		'DbATLabel8
		'
		Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel8.Height = 18.0!
		Me.DbATLabel8.Left = 612.0!
		Me.DbATLabel8.Name = "DbATLabel8"
		Me.DbATLabel8.Parent = Me.GroupHeader1
		Me.DbATLabel8.Text = "DEPTO"
		Me.DbATLabel8.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel8.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel8.Top = 48.0!
		Me.DbATLabel8.Width = 48.0!
		'
		'DbATLabel6
		'
		Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel6.Height = 18.0!
		Me.DbATLabel6.Left = 120.0!
		Me.DbATLabel6.Name = "DbATLabel6"
		Me.DbATLabel6.Parent = Me.GroupHeader1
		Me.DbATLabel6.Text = "DESCRIPCION"
		Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel6.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel6.Top = 48.0!
		Me.DbATLabel6.Width = 312.0!
		'
		'DbATLabel5
		'
		Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel5.Height = 18.0!
		Me.DbATLabel5.Left = 252.0!
		Me.DbATLabel5.Name = "DbATLabel5"
		Me.DbATLabel5.Parent = Me.GroupHeader1
		Me.DbATLabel5.Text = "MARBETE:"
		Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel5.Top = 6.0!
		Me.DbATLabel5.Width = 60.0!
		'
		'DbATLabel7
		'
		Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel7.Height = 18.0!
		Me.DbATLabel7.Left = 6.0!
		Me.DbATLabel7.Name = "DbATLabel7"
		Me.DbATLabel7.Parent = Me.GroupHeader1
		Me.DbATLabel7.Text = "CBARRAS"
		Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.DbATLabel7.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
		Me.DbATLabel7.Top = 48.0!
		Me.DbATLabel7.Width = 108.0!
		'
		'DbATTextBox4
		'
		Me.DbATTextBox4.DataField = "IdMarbete"
		Me.DbATTextBox4.DistinctField = "IdMarbete"
		Me.DbATTextBox4.Height = 18.0!
		Me.DbATTextBox4.Left = 312.0!
		Me.DbATTextBox4.Name = "DbATTextBox4"
		Me.DbATTextBox4.Parent = Me.GroupHeader1
		Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.DbATTextBox4.Top = 6.0!
		Me.DbATTextBox4.Width = 192.0!
		'
		'DbATPageBreak1
		'
		Me.DbATPageBreak1.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATPageBreak1.Enabled = False
		Me.DbATPageBreak1.Name = "DbATPageBreak1"
		Me.DbATPageBreak1.Parent = Me.GroupHeader1
		Me.DbATPageBreak1.Top = 0.0!
		Me.DbATPageBreak1.Visible = False
		'
		'DbATLine3
		'
		Me.DbATLine3.BorderBottomColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderLeftColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderRightColor = System.Drawing.Color.Transparent
		Me.DbATLine3.BorderTopColor = System.Drawing.Color.Transparent
		Me.DbATLine3.LineWeight = 1.0!
		Me.DbATLine3.Name = "DbATLine3"
		Me.DbATLine3.Parent = Me.GroupHeader1
		Me.DbATLine3.X1 = 804.0!
		Me.DbATLine3.X2 = 0.0!
		Me.DbATLine3.Y1 = 66.0!
		Me.DbATLine3.Y2 = 66.0!
		'
		'DbATTextBox1
		'
		Me.DbATTextBox1.DataField = "Conteo"
		Me.DbATTextBox1.DistinctField = "Conteo"
		Me.DbATTextBox1.Height = 18.0!
		Me.DbATTextBox1.Left = 510.0!
		Me.DbATTextBox1.Name = "DbATTextBox1"
		Me.DbATTextBox1.Parent = Me.GroupHeader1
		Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATTextBox1.Top = 6.0!
		Me.DbATTextBox1.Width = 114.0!
		'
		'GroupFooter1
		'
		Me.GroupFooter1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.txtTotalCantidad, Me.DbATLabel13, Me.txtConteoEAN, Me.DbATLabel12})
		Me.GroupFooter1.Height = 62.0!
		Me.GroupFooter1.KeepTogether = True
		Me.GroupFooter1.Name = "GroupFooter1"
		'
		'txtTotalCantidad
		'
		Me.txtTotalCantidad.DataField = "IdMarbete"
		Me.txtTotalCantidad.DistinctField = "IdMarbete"
		Me.txtTotalCantidad.Height = 18.0!
		Me.txtTotalCantidad.Left = 252.0!
		Me.txtTotalCantidad.Name = "txtTotalCantidad"
		Me.txtTotalCantidad.OutputFormat = "#,##0.00;(#,##0.00)"
		Me.txtTotalCantidad.Parent = Me.GroupFooter1
		Me.txtTotalCantidad.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.txtTotalCantidad.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.txtTotalCantidad.Top = 0.0!
		Me.txtTotalCantidad.Width = 150.0!
		'
		'DbATLabel13
		'
		Me.DbATLabel13.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel13.Height = 18.0!
		Me.DbATLabel13.Left = 12.0!
		Me.DbATLabel13.Name = "DbATLabel13"
		Me.DbATLabel13.Parent = Me.GroupFooter1
		Me.DbATLabel13.Text = "TOTAL MARBETE:"
		Me.DbATLabel13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel13.Top = 0.0!
		Me.DbATLabel13.Width = 126.0!
		'
		'txtConteoEAN
		'
		Me.txtConteoEAN.DataField = "IdMarbete"
		Me.txtConteoEAN.DistinctField = "IdMarbete"
		Me.txtConteoEAN.Height = 18.0!
		Me.txtConteoEAN.Left = 306.0!
		Me.txtConteoEAN.Name = "txtConteoEAN"
		Me.txtConteoEAN.Parent = Me.GroupFooter1
		Me.txtConteoEAN.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.txtConteoEAN.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.txtConteoEAN.Top = 36.0!
		Me.txtConteoEAN.Width = 96.0!
		'
		'DbATLabel12
		'
		Me.DbATLabel12.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel12.Height = 18.0!
		Me.DbATLabel12.Left = 84.0!
		Me.DbATLabel12.Name = "DbATLabel12"
		Me.DbATLabel12.Parent = Me.GroupFooter1
		Me.DbATLabel12.Text = "CANTIDAD DE CODIGOS:"
		Me.DbATLabel12.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.DbATLabel12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Me.DbATLabel12.Top = 36.0!
		Me.DbATLabel12.Width = 216.0!
		'
		'txtAlmacen
		'
		Me.txtAlmacen.DataField = "Almacen"
		Me.txtAlmacen.DistinctField = ""
		Me.txtAlmacen.Height = 18.0!
		Me.txtAlmacen.Left = 726.0!
		Me.txtAlmacen.Name = "txtAlmacen"
		Me.txtAlmacen.Parent = Me.GroupHeader1
		Me.txtAlmacen.Top = 6.0!
		Me.txtAlmacen.Width = 66.0!
		'
		'DbATLabel3
		'
		Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
		Me.DbATLabel3.Height = 18.0!
		Me.DbATLabel3.Left = 654.0!
		Me.DbATLabel3.Name = "DbATLabel3"
		Me.DbATLabel3.Parent = Me.GroupHeader1
		Me.DbATLabel3.Text = "ALMACEN:"
		Me.DbATLabel3.Top = 6.0!
		Me.DbATLabel3.Width = 60.0!
		'
		'RPT_MarbeteIndividual
		'
		Me.ReportWidth = 816.0!
		Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
		CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

	End Sub

#End Region

	'Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
	'    Me.txtPagina.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
	'    Me.pageno = Me.pageno + 1
	'    totalpaginas = MyBase.MaxPages
	'End Sub

	Private Sub GroupFooter1_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.Initialize
		Dim TotalCantidad As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("TotalCantidad"))
		Me.txtTotalCantidad.Value = TotalCantidad
		Dim ConteoEAN As Double = Convert.ToDouble(MyBase.GetCurrentDBValue("ConteoEAN"))
		Me.txtConteoEAN.Value = ConteoEAN
		DbATPageBreak1.Enabled = True
		DbATPageBreak1.Visible = True
	End Sub

	'Private Sub PageFooter_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageFooter.Initialize
	'    'Me.txtPag.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
	'    Me.lblPag.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
	'    Me.pageno = Me.pageno + 1
	'    totalpaginas = MyBase.MaxPages
	'End Sub

	Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
		Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
		Me.pageno = Me.pageno + 1
		totalpaginas = MyBase.MaxPages
	End Sub
End Class


