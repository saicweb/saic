Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_Diferencias80_20Almacen
    Inherits dbAutoTrack.DataReports.Design.dbATReport
    Dim pageno As Integer = 1
    Dim conteo As Integer = 1
    Dim totalpaginas As Integer = 0
    Dim ContID As Integer = 0
#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the DataReport Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the DataReport Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the DataReport Designer
    'It can be modified using the DataReport Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
    Private WithEvents Detail As dbAutoTrack.DataReports.Detail
    Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
    Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLine14 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLabel45 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel42 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel41 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel40 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel39 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel38 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox18 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox15 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox13 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox12 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox11 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLine4 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel46 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel9 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
    Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
    Private WithEvents txtAlmacen As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATShape1 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATLabel11 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATShape2 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
        Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel46 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel38 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel39 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel40 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel41 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel42 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel45 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLine14 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel()
        Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
        Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel9 = New dbAutoTrack.DataReports.dbATLabel()
        Me.Detail = New dbAutoTrack.DataReports.Detail()
        Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLine4 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATTextBox11 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox12 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox13 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox15 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox18 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
        Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader()
        Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtAlmacen = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATShape1 = New dbAutoTrack.DataReports.dbATShape()
        Me.DbATLabel11 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATShape2 = New dbAutoTrack.DataReports.dbATShape()
        Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel7, Me.DbATLabel46, Me.DbATLabel10, Me.DbATLabel38, Me.DbATLabel39, Me.DbATLabel40, Me.DbATLabel41, Me.DbATLabel42, Me.DbATLabel45, Me.DbATLine14, Me.DbATLabel3, Me.lblPage, Me.txtHora, Me.DbATLabel4, Me.txtTienda, Me.txtFecha, Me.DbATLabel1, Me.DbATLabel2, Me.DbATLabel9})
        Me.PageHeader.Height = 110.0!
        Me.PageHeader.Name = "PageHeader"
        '
        'DbATLabel7
        '
        Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel7.Height = 18.0!
        Me.DbATLabel7.Left = 54.0!
        Me.DbATLabel7.Name = "DbATLabel7"
        Me.DbATLabel7.Parent = Me.PageHeader
        Me.DbATLabel7.Text = "FECHA:"
        Me.DbATLabel7.Top = 42.0!
        Me.DbATLabel7.Width = 48.0!
        '
        'DbATLabel46
        '
        Me.DbATLabel46.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel46.Height = 18.0!
        Me.DbATLabel46.Left = 810.0!
        Me.DbATLabel46.Name = "DbATLabel46"
        Me.DbATLabel46.Parent = Me.PageHeader
        Me.DbATLabel46.Text = "PROVEEDOR"
        Me.DbATLabel46.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel46.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel46.Top = 84.0!
        Me.DbATLabel46.Width = 137.952!
        '
        'DbATLabel10
        '
        Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel10.Height = 18.0!
        Me.DbATLabel10.Left = 660.0!
        Me.DbATLabel10.Name = "DbATLabel10"
        Me.DbATLabel10.Parent = Me.PageHeader
        Me.DbATLabel10.Text = "TOTAL"
        Me.DbATLabel10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel10.Top = 84.0!
        Me.DbATLabel10.Width = 47.952!
        '
        'DbATLabel38
        '
        Me.DbATLabel38.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel38.Height = 18.0!
        Me.DbATLabel38.Left = 6.0!
        Me.DbATLabel38.Name = "DbATLabel38"
        Me.DbATLabel38.Parent = Me.PageHeader
        Me.DbATLabel38.Text = "CBARRAS"
        Me.DbATLabel38.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel38.Top = 84.0!
        Me.DbATLabel38.Width = 66.048!
        '
        'DbATLabel39
        '
        Me.DbATLabel39.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel39.Height = 18.0!
        Me.DbATLabel39.Left = 96.0!
        Me.DbATLabel39.Name = "DbATLabel39"
        Me.DbATLabel39.Parent = Me.PageHeader
        Me.DbATLabel39.Text = "DESCRIPCION"
        Me.DbATLabel39.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel39.Top = 84.0!
        Me.DbATLabel39.Width = 258.048!
        '
        'DbATLabel40
        '
        Me.DbATLabel40.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel40.Height = 18.0!
        Me.DbATLabel40.Left = 396.0!
        Me.DbATLabel40.Name = "DbATLabel40"
        Me.DbATLabel40.Parent = Me.PageHeader
        Me.DbATLabel40.Text = "TEORICO"
        Me.DbATLabel40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel40.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel40.Top = 84.0!
        Me.DbATLabel40.Width = 65.904!
        '
        'DbATLabel41
        '
        Me.DbATLabel41.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel41.Height = 18.0!
        Me.DbATLabel41.Left = 492.0!
        Me.DbATLabel41.Name = "DbATLabel41"
        Me.DbATLabel41.Parent = Me.PageHeader
        Me.DbATLabel41.Text = "FISICO"
        Me.DbATLabel41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel41.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel41.Top = 84.0!
        Me.DbATLabel41.Width = 65.904!
        '
        'DbATLabel42
        '
        Me.DbATLabel42.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel42.Height = 18.0!
        Me.DbATLabel42.Left = 570.0!
        Me.DbATLabel42.Name = "DbATLabel42"
        Me.DbATLabel42.Parent = Me.PageHeader
        Me.DbATLabel42.Text = "DIFERENCIA"
        Me.DbATLabel42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel42.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel42.Top = 84.0!
        Me.DbATLabel42.Width = 65.904!
        '
        'DbATLabel45
        '
        Me.DbATLabel45.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel45.Height = 18.0!
        Me.DbATLabel45.Left = 720.0!
        Me.DbATLabel45.Name = "DbATLabel45"
        Me.DbATLabel45.Parent = Me.PageHeader
        Me.DbATLabel45.Text = "DIF COSTO"
        Me.DbATLabel45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel45.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel45.Top = 84.0!
        Me.DbATLabel45.Width = 77.952!
        '
        'DbATLine14
        '
        Me.DbATLine14.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine14.LineWeight = 1.0!
        Me.DbATLine14.Name = "DbATLine14"
        Me.DbATLine14.Parent = Me.PageHeader
        Me.DbATLine14.X1 = 1044.0!
        Me.DbATLine14.X2 = 6.0!
        Me.DbATLine14.Y1 = 102.0!
        Me.DbATLine14.Y2 = 102.0!
        '
        'DbATLabel3
        '
        Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel3.Height = 18.0!
        Me.DbATLabel3.Left = 30.0!
        Me.DbATLabel3.Name = "DbATLabel3"
        Me.DbATLabel3.Parent = Me.PageHeader
        Me.DbATLabel3.Text = "*PRECIOS VENTA VIGENTES EN SAP*"
        Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel3.Top = 26.88!
        Me.DbATLabel3.Width = 1002.0!
        '
        'lblPage
        '
        Me.lblPage.ForeColor = System.Drawing.Color.Black
        Me.lblPage.Height = 18.0!
        Me.lblPage.Left = 936.0!
        Me.lblPage.Name = "lblPage"
        Me.lblPage.Parent = Me.PageHeader
        Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPage.Top = 42.0!
        Me.lblPage.Width = 96.0!
        '
        'txtHora
        '
        Me.txtHora.Height = 18.0!
        Me.txtHora.Left = 312.0!
        Me.txtHora.Name = "txtHora"
        Me.txtHora.OutputFormat = "h:mm AM/PM"
        Me.txtHora.Parent = Me.PageHeader
        Me.txtHora.Top = 42.0!
        Me.txtHora.Width = 72.0!
        '
        'DbATLabel4
        '
        Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel4.Height = 18.0!
        Me.DbATLabel4.Left = 264.0!
        Me.DbATLabel4.Name = "DbATLabel4"
        Me.DbATLabel4.Parent = Me.PageHeader
        Me.DbATLabel4.Text = "HORA:"
        Me.DbATLabel4.Top = 42.0!
        Me.DbATLabel4.Width = 48.0!
        '
        'txtTienda
        '
        Me.txtTienda.DataField = "Tienda"
        Me.txtTienda.Height = 18.0!
        Me.txtTienda.Left = 480.0!
        Me.txtTienda.Name = "txtTienda"
        Me.txtTienda.OutputFormat = "h:mm AM/PM"
        Me.txtTienda.Parent = Me.PageHeader
        Me.txtTienda.Top = 42.0!
        Me.txtTienda.Width = 270.0!
        '
        'txtFecha
        '
        Me.txtFecha.DataField = "Fecha"
        Me.txtFecha.Height = 18.0!
        Me.txtFecha.Left = 102.0!
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Parent = Me.PageHeader
        Me.txtFecha.Top = 42.0!
        Me.txtFecha.Width = 72.0!
        '
        'DbATLabel1
        '
        Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel1.Height = 18.0!
        Me.DbATLabel1.Left = 186.0!
        Me.DbATLabel1.Name = "DbATLabel1"
        Me.DbATLabel1.Parent = Me.PageHeader
        Me.DbATLabel1.Text = "REPORTE DE DIFERENCIAS 80-20 POR ALMACEN"
        Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel1.Top = 6.0!
        Me.DbATLabel1.Width = 846.0!
        '
        'DbATLabel2
        '
        Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel2.Height = 18.0!
        Me.DbATLabel2.Left = 426.0!
        Me.DbATLabel2.Name = "DbATLabel2"
        Me.DbATLabel2.Parent = Me.PageHeader
        Me.DbATLabel2.Text = "TIENDA:"
        Me.DbATLabel2.Top = 42.0!
        Me.DbATLabel2.Width = 54.0!
        '
        'DbATLabel9
        '
        Me.DbATLabel9.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel9.Height = 18.0!
        Me.DbATLabel9.Left = 1014.0!
        Me.DbATLabel9.Name = "DbATLabel9"
        Me.DbATLabel9.Parent = Me.PageHeader
        Me.DbATLabel9.Text = "ID"
        Me.DbATLabel9.Top = 84.0!
        Me.DbATLabel9.Width = 24.0!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox2, Me.DbATLine4, Me.DbATTextBox11, Me.DbATTextBox12, Me.DbATTextBox13, Me.DbATTextBox15, Me.DbATTextBox18, Me.DbATTextBox4, Me.DbATTextBox1})
        Me.Detail.Height = 18.0!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'DbATTextBox2
        '
        Me.DbATTextBox2.CanGrow = False
        Me.DbATTextBox2.DataField = "PROVEEDOR"
        Me.DbATTextBox2.Height = 12.048!
        Me.DbATTextBox2.Left = 809.952!
        Me.DbATTextBox2.Name = "DbATTextBox2"
        Me.DbATTextBox2.OutputFormat = ""
        Me.DbATTextBox2.Parent = Me.Detail
        Me.DbATTextBox2.Text = ""
        Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox2.Top = 0.0!
        Me.DbATTextBox2.Width = 191.76!
        '
        'DbATLine4
        '
        Me.DbATLine4.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine4.LineWeight = 1.0!
        Me.DbATLine4.Name = "DbATLine4"
        Me.DbATLine4.Parent = Me.Detail
        Me.DbATLine4.X1 = 666.0!
        Me.DbATLine4.X2 = 708.0!
        Me.DbATLine4.Y1 = 12.0!
        Me.DbATLine4.Y2 = 12.0!
        '
        'DbATTextBox11
        '
        Me.DbATTextBox11.CanGrow = False
        Me.DbATTextBox11.DataField = "CBARRAS"
        Me.DbATTextBox11.Height = 12.0!
        Me.DbATTextBox11.Left = 6.0!
        Me.DbATTextBox11.Name = "DbATTextBox11"
        Me.DbATTextBox11.Parent = Me.Detail
        Me.DbATTextBox11.Text = ""
        Me.DbATTextBox11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox11.Top = 0.0!
        Me.DbATTextBox11.Width = 67.2!
        '
        'DbATTextBox12
        '
        Me.DbATTextBox12.CanGrow = False
        Me.DbATTextBox12.DataField = "OBS"
        Me.DbATTextBox12.Height = 12.48!
        Me.DbATTextBox12.Left = 96.0!
        Me.DbATTextBox12.Name = "DbATTextBox12"
        Me.DbATTextBox12.Parent = Me.Detail
        Me.DbATTextBox12.Text = ""
        Me.DbATTextBox12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox12.Top = 0.0!
        Me.DbATTextBox12.Width = 268.8!
        '
        'DbATTextBox13
        '
        Me.DbATTextBox13.DataField = "Teorico"
        Me.DbATTextBox13.Height = 12.0!
        Me.DbATTextBox13.Left = 402.0!
        Me.DbATTextBox13.Name = "DbATTextBox13"
        Me.DbATTextBox13.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox13.Parent = Me.Detail
        Me.DbATTextBox13.Text = ""
        Me.DbATTextBox13.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox13.Top = 0.0!
        Me.DbATTextBox13.Width = 53.952!
        '
        'DbATTextBox15
        '
        Me.DbATTextBox15.DataField = "FISICO"
        Me.DbATTextBox15.Height = 12.0!
        Me.DbATTextBox15.Left = 492.0!
        Me.DbATTextBox15.Name = "DbATTextBox15"
        Me.DbATTextBox15.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox15.Parent = Me.Detail
        Me.DbATTextBox15.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox15.Top = 0.0!
        Me.DbATTextBox15.Width = 48.0!
        '
        'DbATTextBox18
        '
        Me.DbATTextBox18.DataField = "DIFCOSTO"
        Me.DbATTextBox18.Height = 12.0!
        Me.DbATTextBox18.Left = 720.0!
        Me.DbATTextBox18.Name = "DbATTextBox18"
        Me.DbATTextBox18.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox18.Parent = Me.Detail
        Me.DbATTextBox18.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox18.Top = 0.0!
        Me.DbATTextBox18.Width = 76.8!
        '
        'DbATTextBox4
        '
        Me.DbATTextBox4.DataField = ""
        Me.DbATTextBox4.Height = 18.0!
        Me.DbATTextBox4.Left = 1002.0!
        Me.DbATTextBox4.Name = "DbATTextBox4"
        Me.DbATTextBox4.Parent = Me.Detail
        Me.DbATTextBox4.Top = 0.0!
        Me.DbATTextBox4.Width = 48.0!
        '
        'DbATTextBox1
        '
        Me.DbATTextBox1.DataField = "DIFERENCIA"
        Me.DbATTextBox1.Height = 12.0!
        Me.DbATTextBox1.Left = 576.0!
        Me.DbATTextBox1.Name = "DbATTextBox1"
        Me.DbATTextBox1.OutputFormat = "#,##0.00"
        Me.DbATTextBox1.Parent = Me.Detail
        Me.DbATTextBox1.Top = 0.0!
        Me.DbATTextBox1.Width = 90.0!
        '
        'PageFooter
        '
        Me.PageFooter.Height = 23.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel8, Me.txtAlmacen, Me.DbATShape1, Me.DbATLabel11, Me.DbATTextBox5, Me.DbATShape2})
        Me.GroupHeader1.Height = 28.0!
        Me.GroupHeader1.Index = 1
        Me.GroupHeader1.Name = "GroupHeader1"
        Me.GroupHeader1.NewPage = dbAutoTrack.DataReports.NewPage.Before
        '
        'DbATLabel8
        '
        Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel8.Height = 18.0!
        Me.DbATLabel8.Left = 12.0!
        Me.DbATLabel8.Name = "DbATLabel8"
        Me.DbATLabel8.Parent = Me.GroupHeader1
        Me.DbATLabel8.Text = "ALMACEN:"
        Me.DbATLabel8.Top = 6.0!
        Me.DbATLabel8.Width = 60.0!
        '
        'txtAlmacen
        '
        Me.txtAlmacen.DataField = "idAlmacen"
        Me.txtAlmacen.Height = 18.0!
        Me.txtAlmacen.Left = 72.0!
        Me.txtAlmacen.Name = "txtAlmacen"
        Me.txtAlmacen.Parent = Me.GroupHeader1
        Me.txtAlmacen.Top = 6.0!
        Me.txtAlmacen.Width = 72.0!
        '
        'DbATShape1
        '
        Me.DbATShape1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape1.Height = 18.0!
        Me.DbATShape1.Left = 12.0!
        Me.DbATShape1.LineWeight = 1.0!
        Me.DbATShape1.Name = "DbATShape1"
        Me.DbATShape1.Parent = Me.GroupHeader1
        Me.DbATShape1.Top = 6.0!
        Me.DbATShape1.Width = 132.0!
        '
        'DbATLabel11
        '
        Me.DbATLabel11.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel11.Height = 18.0!
        Me.DbATLabel11.Left = 162.0!
        Me.DbATLabel11.Name = "DbATLabel11"
        Me.DbATLabel11.Parent = Me.GroupHeader1
        Me.DbATLabel11.Text = "DEPT"
        Me.DbATLabel11.Top = 6.0!
        Me.DbATLabel11.Width = 48.0!
        '
        'DbATTextBox5
        '
        Me.DbATTextBox5.DataField = "dept"
        Me.DbATTextBox5.Height = 18.0!
        Me.DbATTextBox5.Left = 210.0!
        Me.DbATTextBox5.Name = "DbATTextBox5"
        Me.DbATTextBox5.Parent = Me.GroupHeader1
        Me.DbATTextBox5.Top = 6.0!
        Me.DbATTextBox5.Width = 66.0!
        '
        'DbATShape2
        '
        Me.DbATShape2.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape2.Height = 18.0!
        Me.DbATShape2.Left = 162.0!
        Me.DbATShape2.LineWeight = 1.0!
        Me.DbATShape2.Name = "DbATShape2"
        Me.DbATShape2.Parent = Me.GroupHeader1
        Me.DbATShape2.Top = 6.0!
        Me.DbATShape2.Width = 126.0!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'RPT_Diferencias80_20Almacen
        '
        Me.ReportWidth = 1068.0!
        Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region
    Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
        Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
        Me.pageno = Me.pageno + 1
        totalpaginas = MyBase.MaxPages
    End Sub


    Private Sub Detail_Initialize(sender As Object, e As System.EventArgs) Handles Detail.Initialize
        ContID += 1
        DbATTextBox4.Value = ContID
    End Sub

    Private Sub GroupHeader1_Initialize(sender As Object, e As System.EventArgs) Handles GroupHeader1.Initialize
        ContID = 0
    End Sub
End Class


