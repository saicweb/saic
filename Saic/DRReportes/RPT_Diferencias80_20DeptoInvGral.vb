Imports System
Imports System.ComponentModel
Imports System.Collections
Imports System.Diagnostics
Imports dbAutoTrack.DataReports

Public Class RPT_Diferencias80_20DeptoInvGral
    Inherits dbAutoTrack.DataReports.Design.dbATReport

#Region " Component Designer generated code "

    Public Sub New(Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the DataReport Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the DataReport Designer
    Private components As System.ComponentModel.IContainer
   
    'NOTE: The following procedure is required by the DataReport Designer
    'It can be modified using the DataReport Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As dbAutoTrack.DataReports.PageHeader
	Private WithEvents Detail As dbAutoTrack.DataReports.Detail
	Private WithEvents PageFooter As dbAutoTrack.DataReports.PageFooter
    Private WithEvents DbATLabel3 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents lblPage As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtHora As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel4 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents txtTienda As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtFecha As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel1 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel2 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel8 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents GroupHeader1 As dbAutoTrack.DataReports.GroupHeader
    Private WithEvents GroupFooter1 As dbAutoTrack.DataReports.GroupFooter
    Private WithEvents DbATPageBreak1 As dbAutoTrack.DataReports.dbATPageBreak
    Private WithEvents DbATLine14 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATTextBox2 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents txtCODE_DEPTO As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel6 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATShape1 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATLabel46 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel10 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel40 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel41 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel42 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel45 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel38 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATLabel39 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox1 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLine4 As dbAutoTrack.DataReports.dbATLine
    Private WithEvents DbATTextBox11 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox12 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox13 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox15 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox18 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATTextBox3 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATLabel5 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox4 As dbAutoTrack.DataReports.dbATTextBox
    Private WithEvents DbATShape2 As dbAutoTrack.DataReports.dbATShape
    Private WithEvents DbATLabel7 As dbAutoTrack.DataReports.dbATLabel
    Private WithEvents DbATTextBox5 As dbAutoTrack.DataReports.dbATTextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PageHeader = New dbAutoTrack.DataReports.PageHeader()
        Me.DbATLabel39 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel38 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel45 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel42 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel41 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel40 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel10 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel46 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLine14 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATLabel8 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel2 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel1 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtFecha = New dbAutoTrack.DataReports.dbATTextBox()
        Me.txtTienda = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLabel4 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtHora = New dbAutoTrack.DataReports.dbATTextBox()
        Me.lblPage = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel3 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATLabel5 = New dbAutoTrack.DataReports.dbATLabel()
        Me.Detail = New dbAutoTrack.DataReports.Detail()
        Me.DbATTextBox3 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox18 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox15 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox13 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox12 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox11 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATLine4 = New dbAutoTrack.DataReports.dbATLine()
        Me.DbATTextBox1 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox4 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.PageFooter = New dbAutoTrack.DataReports.PageFooter()
        Me.GroupHeader1 = New dbAutoTrack.DataReports.GroupHeader()
        Me.DbATShape1 = New dbAutoTrack.DataReports.dbATShape()
        Me.DbATLabel6 = New dbAutoTrack.DataReports.dbATLabel()
        Me.txtCODE_DEPTO = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATTextBox2 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.DbATPageBreak1 = New dbAutoTrack.DataReports.dbATPageBreak()
        Me.DbATShape2 = New dbAutoTrack.DataReports.dbATShape()
        Me.DbATLabel7 = New dbAutoTrack.DataReports.dbATLabel()
        Me.DbATTextBox5 = New dbAutoTrack.DataReports.dbATTextBox()
        Me.GroupFooter1 = New dbAutoTrack.DataReports.GroupFooter()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PageHeader
        '
        Me.PageHeader.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATLabel39, Me.DbATLabel38, Me.DbATLabel45, Me.DbATLabel42, Me.DbATLabel41, Me.DbATLabel40, Me.DbATLabel10, Me.DbATLabel46, Me.DbATLine14, Me.DbATLabel8, Me.DbATLabel2, Me.DbATLabel1, Me.txtFecha, Me.txtTienda, Me.DbATLabel4, Me.txtHora, Me.lblPage, Me.DbATLabel3, Me.DbATLabel5})
        Me.PageHeader.Height = 105.024!
        Me.PageHeader.Name = "PageHeader"
        '
        'DbATLabel39
        '
        Me.DbATLabel39.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel39.Height = 18.0!
        Me.DbATLabel39.Left = 78.0!
        Me.DbATLabel39.Name = "DbATLabel39"
        Me.DbATLabel39.Parent = Me.PageHeader
        Me.DbATLabel39.Text = "DESCRIPCION"
        Me.DbATLabel39.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel39.Top = 84.0!
        Me.DbATLabel39.Width = 228.048!
        '
        'DbATLabel38
        '
        Me.DbATLabel38.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel38.Height = 18.0!
        Me.DbATLabel38.Left = 6.0!
        Me.DbATLabel38.Name = "DbATLabel38"
        Me.DbATLabel38.Parent = Me.PageHeader
        Me.DbATLabel38.Text = "CBARRAS"
        Me.DbATLabel38.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel38.Top = 84.0!
        Me.DbATLabel38.Width = 66.048!
        '
        'DbATLabel45
        '
        Me.DbATLabel45.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel45.Height = 18.0!
        Me.DbATLabel45.Left = 726.0!
        Me.DbATLabel45.Name = "DbATLabel45"
        Me.DbATLabel45.Parent = Me.PageHeader
        Me.DbATLabel45.Text = "DIF COSTO"
        Me.DbATLabel45.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel45.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel45.Top = 84.0!
        Me.DbATLabel45.Width = 77.952!
        '
        'DbATLabel42
        '
        Me.DbATLabel42.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel42.Height = 18.0!
        Me.DbATLabel42.Left = 474.0!
        Me.DbATLabel42.Name = "DbATLabel42"
        Me.DbATLabel42.Parent = Me.PageHeader
        Me.DbATLabel42.Text = "DIFERENCIA"
        Me.DbATLabel42.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel42.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel42.Top = 84.0!
        Me.DbATLabel42.Width = 77.904!
        '
        'DbATLabel41
        '
        Me.DbATLabel41.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel41.Height = 18.0!
        Me.DbATLabel41.Left = 396.0!
        Me.DbATLabel41.Name = "DbATLabel41"
        Me.DbATLabel41.Parent = Me.PageHeader
        Me.DbATLabel41.Text = "FISICO"
        Me.DbATLabel41.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel41.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel41.Top = 84.0!
        Me.DbATLabel41.Width = 71.904!
        '
        'DbATLabel40
        '
        Me.DbATLabel40.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel40.Height = 18.0!
        Me.DbATLabel40.Left = 312.0!
        Me.DbATLabel40.Name = "DbATLabel40"
        Me.DbATLabel40.Parent = Me.PageHeader
        Me.DbATLabel40.Text = "TEORICO"
        Me.DbATLabel40.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel40.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel40.Top = 84.0!
        Me.DbATLabel40.Width = 77.904!
        '
        'DbATLabel10
        '
        Me.DbATLabel10.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel10.Height = 18.0!
        Me.DbATLabel10.Left = 612.0!
        Me.DbATLabel10.Name = "DbATLabel10"
        Me.DbATLabel10.Parent = Me.PageHeader
        Me.DbATLabel10.Text = "TOTAL"
        Me.DbATLabel10.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel10.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel10.Top = 84.0!
        Me.DbATLabel10.Width = 47.952!
        '
        'DbATLabel46
        '
        Me.DbATLabel46.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel46.Height = 18.0!
        Me.DbATLabel46.Left = 810.0!
        Me.DbATLabel46.Name = "DbATLabel46"
        Me.DbATLabel46.Parent = Me.PageHeader
        Me.DbATLabel46.Text = "PROVEEDOR"
        Me.DbATLabel46.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel46.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel46.Top = 84.0!
        Me.DbATLabel46.Width = 179.376!
        '
        'DbATLine14
        '
        Me.DbATLine14.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine14.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine14.LineWeight = 1.0!
        Me.DbATLine14.Name = "DbATLine14"
        Me.DbATLine14.Parent = Me.PageHeader
        Me.DbATLine14.X1 = 1038.0!
        Me.DbATLine14.X2 = 6.0!
        Me.DbATLine14.Y1 = 102.0!
        Me.DbATLine14.Y2 = 102.0!
        '
        'DbATLabel8
        '
        Me.DbATLabel8.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel8.Height = 18.0!
        Me.DbATLabel8.Left = 174.0!
        Me.DbATLabel8.Name = "DbATLabel8"
        Me.DbATLabel8.Parent = Me.PageHeader
        Me.DbATLabel8.Text = "FECHA:"
        Me.DbATLabel8.Top = 42.0!
        Me.DbATLabel8.Width = 48.0!
        '
        'DbATLabel2
        '
        Me.DbATLabel2.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel2.Height = 18.0!
        Me.DbATLabel2.Left = 540.0!
        Me.DbATLabel2.Name = "DbATLabel2"
        Me.DbATLabel2.Parent = Me.PageHeader
        Me.DbATLabel2.Text = "TIENDA:"
        Me.DbATLabel2.Top = 42.0!
        Me.DbATLabel2.Width = 54.0!
        '
        'DbATLabel1
        '
        Me.DbATLabel1.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel1.Height = 18.0!
        Me.DbATLabel1.Left = 96.0!
        Me.DbATLabel1.Name = "DbATLabel1"
        Me.DbATLabel1.Parent = Me.PageHeader
        Me.DbATLabel1.Text = "REPORTE DE DIFERENCIAS 80-20 POR DEPARTAMENTO"
        Me.DbATLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DbATLabel1.Top = 6.0!
        Me.DbATLabel1.Width = 846.0!
        '
        'txtFecha
        '
        Me.txtFecha.DataField = "Fecha"
        Me.txtFecha.Height = 18.0!
        Me.txtFecha.Left = 222.0!
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Parent = Me.PageHeader
        Me.txtFecha.Top = 42.0!
        Me.txtFecha.Width = 72.0!
        '
        'txtTienda
        '
        Me.txtTienda.DataField = "Tienda"
        Me.txtTienda.Height = 18.0!
        Me.txtTienda.Left = 594.0!
        Me.txtTienda.Name = "txtTienda"
        Me.txtTienda.OutputFormat = "h:mm AM/PM"
        Me.txtTienda.Parent = Me.PageHeader
        Me.txtTienda.Top = 42.0!
        Me.txtTienda.Width = 270.0!
        '
        'DbATLabel4
        '
        Me.DbATLabel4.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel4.Height = 18.0!
        Me.DbATLabel4.Left = 366.0!
        Me.DbATLabel4.Name = "DbATLabel4"
        Me.DbATLabel4.Parent = Me.PageHeader
        Me.DbATLabel4.Text = "HORA:"
        Me.DbATLabel4.Top = 42.0!
        Me.DbATLabel4.Width = 48.0!
        '
        'txtHora
        '
        Me.txtHora.Height = 18.0!
        Me.txtHora.Left = 414.0!
        Me.txtHora.Name = "txtHora"
        Me.txtHora.OutputFormat = "h:mm AM/PM"
        Me.txtHora.Parent = Me.PageHeader
        Me.txtHora.Top = 42.0!
        Me.txtHora.Width = 72.0!
        '
        'lblPage
        '
        Me.lblPage.ForeColor = System.Drawing.Color.Black
        Me.lblPage.Height = 18.0!
        Me.lblPage.Left = 936.0!
        Me.lblPage.Name = "lblPage"
        Me.lblPage.Parent = Me.PageHeader
        Me.lblPage.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblPage.Top = 42.0!
        Me.lblPage.Width = 96.0!
        '
        'DbATLabel3
        '
        Me.DbATLabel3.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel3.Height = 18.0!
        Me.DbATLabel3.Left = 30.0!
        Me.DbATLabel3.Name = "DbATLabel3"
        Me.DbATLabel3.Parent = Me.PageHeader
        Me.DbATLabel3.Text = "*PRECIOS VENTA VIGENTES EN SAP*"
        Me.DbATLabel3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel3.Top = 26.88!
        Me.DbATLabel3.Width = 1002.0!
        '
        'DbATLabel5
        '
        Me.DbATLabel5.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel5.Height = 18.0!
        Me.DbATLabel5.Left = 994.5599!
        Me.DbATLabel5.Name = "DbATLabel5"
        Me.DbATLabel5.Parent = Me.PageHeader
        Me.DbATLabel5.Text = "ID"
        Me.DbATLabel5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.DbATLabel5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATLabel5.Top = 84.0!
        Me.DbATLabel5.Width = 42.0!
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATTextBox3, Me.DbATTextBox18, Me.DbATTextBox15, Me.DbATTextBox13, Me.DbATTextBox12, Me.DbATTextBox11, Me.DbATLine4, Me.DbATTextBox1, Me.DbATTextBox4})
        Me.Detail.Height = 18.24!
        Me.Detail.KeepTogether = True
        Me.Detail.Name = "Detail"
        '
        'DbATTextBox3
        '
        Me.DbATTextBox3.CanGrow = False
        Me.DbATTextBox3.DataField = "DIFERENCIA"
        Me.DbATTextBox3.Height = 12.0!
        Me.DbATTextBox3.Left = 474.0!
        Me.DbATTextBox3.Name = "DbATTextBox3"
        Me.DbATTextBox3.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox3.Parent = Me.Detail
        Me.DbATTextBox3.Text = ""
        Me.DbATTextBox3.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox3.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox3.Top = 0.0!
        Me.DbATTextBox3.Width = 77.952!
        '
        'DbATTextBox18
        '
        Me.DbATTextBox18.CanGrow = False
        Me.DbATTextBox18.DataField = "DIFCOSTO"
        Me.DbATTextBox18.Height = 12.0!
        Me.DbATTextBox18.Left = 720.0!
        Me.DbATTextBox18.Name = "DbATTextBox18"
        Me.DbATTextBox18.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox18.Parent = Me.Detail
        Me.DbATTextBox18.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox18.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox18.Top = 0.0!
        Me.DbATTextBox18.Width = 82.8!
        '
        'DbATTextBox15
        '
        Me.DbATTextBox15.CanGrow = False
        Me.DbATTextBox15.DataField = "FISICO"
        Me.DbATTextBox15.Height = 12.0!
        Me.DbATTextBox15.Left = 396.0!
        Me.DbATTextBox15.Name = "DbATTextBox15"
        Me.DbATTextBox15.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox15.Parent = Me.Detail
        Me.DbATTextBox15.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox15.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox15.Top = 0.0!
        Me.DbATTextBox15.Width = 72.0!
        '
        'DbATTextBox13
        '
        Me.DbATTextBox13.CanGrow = False
        Me.DbATTextBox13.DataField = "Teorico"
        Me.DbATTextBox13.Height = 12.0!
        Me.DbATTextBox13.Left = 312.0!
        Me.DbATTextBox13.Name = "DbATTextBox13"
        Me.DbATTextBox13.OutputFormat = "#,##0.00;(#,##0.00)"
        Me.DbATTextBox13.Parent = Me.Detail
        Me.DbATTextBox13.Text = ""
        Me.DbATTextBox13.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.DbATTextBox13.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox13.Top = 0.0!
        Me.DbATTextBox13.Width = 77.952!
        '
        'DbATTextBox12
        '
        Me.DbATTextBox12.CanGrow = False
        Me.DbATTextBox12.DataField = "OBS"
        Me.DbATTextBox12.Height = 12.48!
        Me.DbATTextBox12.Left = 78.0!
        Me.DbATTextBox12.Name = "DbATTextBox12"
        Me.DbATTextBox12.Parent = Me.Detail
        Me.DbATTextBox12.Text = ""
        Me.DbATTextBox12.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox12.Top = 0.0!
        Me.DbATTextBox12.Width = 225.36!
        '
        'DbATTextBox11
        '
        Me.DbATTextBox11.CanGrow = False
        Me.DbATTextBox11.DataField = "CBARRAS"
        Me.DbATTextBox11.Height = 12.0!
        Me.DbATTextBox11.Left = 6.0!
        Me.DbATTextBox11.Name = "DbATTextBox11"
        Me.DbATTextBox11.Parent = Me.Detail
        Me.DbATTextBox11.Text = ""
        Me.DbATTextBox11.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox11.Top = 0.0!
        Me.DbATTextBox11.Width = 67.2!
        '
        'DbATLine4
        '
        Me.DbATLine4.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATLine4.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATLine4.LineWeight = 1.0!
        Me.DbATLine4.Name = "DbATLine4"
        Me.DbATLine4.Parent = Me.Detail
        Me.DbATLine4.X1 = 558.0!
        Me.DbATLine4.X2 = 714.0!
        Me.DbATLine4.Y1 = 12.0!
        Me.DbATLine4.Y2 = 12.0!
        '
        'DbATTextBox1
        '
        Me.DbATTextBox1.CanGrow = False
        Me.DbATTextBox1.DataField = "PROVEEDOR"
        Me.DbATTextBox1.Height = 12.048!
        Me.DbATTextBox1.Left = 809.952!
        Me.DbATTextBox1.Name = "DbATTextBox1"
        Me.DbATTextBox1.OutputFormat = "#,##0.00"
        Me.DbATTextBox1.Parent = Me.Detail
        Me.DbATTextBox1.Text = ""
        Me.DbATTextBox1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox1.Top = 0.0!
        Me.DbATTextBox1.Width = 179.328!
        '
        'DbATTextBox4
        '
        Me.DbATTextBox4.CanGrow = False
        Me.DbATTextBox4.DataField = ""
        Me.DbATTextBox4.Height = 12.096!
        Me.DbATTextBox4.Left = 994.5599!
        Me.DbATTextBox4.Name = "DbATTextBox4"
        Me.DbATTextBox4.Parent = Me.Detail
        Me.DbATTextBox4.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.DbATTextBox4.Top = 0.0!
        Me.DbATTextBox4.Width = 42.048!
        '
        'PageFooter
        '
        Me.PageFooter.CanGrow = False
        Me.PageFooter.Height = 19.2!
        Me.PageFooter.Name = "PageFooter"
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New dbAutoTrack.DataReports.dbATControl() {Me.DbATShape1, Me.DbATLabel6, Me.txtCODE_DEPTO, Me.DbATTextBox2, Me.DbATPageBreak1, Me.DbATShape2, Me.DbATLabel7, Me.DbATTextBox5})
        Me.GroupHeader1.Height = 35.328!
        Me.GroupHeader1.Index = 1
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'DbATShape1
        '
        Me.DbATShape1.BackColor = System.Drawing.Color.Gainsboro
        Me.DbATShape1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape1.Height = 18.0!
        Me.DbATShape1.Left = 168.0!
        Me.DbATShape1.LineWeight = 0.0!
        Me.DbATShape1.Name = "DbATShape1"
        Me.DbATShape1.Parent = Me.GroupHeader1
        Me.DbATShape1.Top = 12.0!
        Me.DbATShape1.Width = 108.0!
        '
        'DbATLabel6
        '
        Me.DbATLabel6.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel6.Height = 18.0!
        Me.DbATLabel6.Left = 168.0!
        Me.DbATLabel6.Name = "DbATLabel6"
        Me.DbATLabel6.Parent = Me.GroupHeader1
        Me.DbATLabel6.Text = "DEPTO:"
        Me.DbATLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATLabel6.Top = 12.0!
        Me.DbATLabel6.Width = 42.0!
        '
        'txtCODE_DEPTO
        '
        Me.txtCODE_DEPTO.CanGrow = False
        Me.txtCODE_DEPTO.DataField = "CODE_DEPTO"
        Me.txtCODE_DEPTO.DistinctField = "CODE_DEPTO"
        Me.txtCODE_DEPTO.Height = 18.0!
        Me.txtCODE_DEPTO.Left = 228.0!
        Me.txtCODE_DEPTO.Name = "txtCODE_DEPTO"
        Me.txtCODE_DEPTO.Parent = Me.GroupHeader1
        Me.txtCODE_DEPTO.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.txtCODE_DEPTO.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.txtCODE_DEPTO.Top = 12.0!
        Me.txtCODE_DEPTO.Width = 42.048!
        '
        'DbATTextBox2
        '
        Me.DbATTextBox2.DataField = ""
        Me.DbATTextBox2.DistinctField = ""
        Me.DbATTextBox2.Height = 18.0!
        Me.DbATTextBox2.Left = 114.0!
        Me.DbATTextBox2.Name = "DbATTextBox2"
        Me.DbATTextBox2.Parent = Me.GroupHeader1
        Me.DbATTextBox2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATTextBox2.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATTextBox2.Top = 60.0!
        Me.DbATTextBox2.Width = 42.048!
        '
        'DbATPageBreak1
        '
        Me.DbATPageBreak1.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATPageBreak1.Enabled = False
        Me.DbATPageBreak1.Name = "DbATPageBreak1"
        Me.DbATPageBreak1.Parent = Me.GroupHeader1
        Me.DbATPageBreak1.Top = 4.8!
        Me.DbATPageBreak1.Visible = False
        '
        'DbATShape2
        '
        Me.DbATShape2.BackColor = System.Drawing.Color.Gainsboro
        Me.DbATShape2.BorderBottomColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderLeftColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderRightColor = System.Drawing.Color.Transparent
        Me.DbATShape2.BorderTopColor = System.Drawing.Color.Transparent
        Me.DbATShape2.Height = 18.0!
        Me.DbATShape2.Left = 12.0!
        Me.DbATShape2.LineWeight = 1.0!
        Me.DbATShape2.Name = "DbATShape2"
        Me.DbATShape2.Parent = Me.GroupHeader1
        Me.DbATShape2.Top = 12.0!
        Me.DbATShape2.Width = 138.0!
        '
        'DbATLabel7
        '
        Me.DbATLabel7.ForeColor = System.Drawing.Color.Black
        Me.DbATLabel7.Height = 18.048!
        Me.DbATLabel7.Left = 12.0!
        Me.DbATLabel7.Name = "DbATLabel7"
        Me.DbATLabel7.Parent = Me.GroupHeader1
        Me.DbATLabel7.Text = "ALMACEN:"
        Me.DbATLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATLabel7.Top = 12.0!
        Me.DbATLabel7.Width = 60.0!
        '
        'DbATTextBox5
        '
        Me.DbATTextBox5.DataField = "Almacen"
        Me.DbATTextBox5.DistinctField = "Almacen"
        Me.DbATTextBox5.Height = 18.0!
        Me.DbATTextBox5.Left = 78.0!
        Me.DbATTextBox5.Name = "DbATTextBox5"
        Me.DbATTextBox5.Parent = Me.GroupHeader1
        Me.DbATTextBox5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DbATTextBox5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.DbATTextBox5.Top = 12.0!
        Me.DbATTextBox5.Width = 66.048!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.0!
        Me.GroupFooter1.Name = "GroupFooter1"
        Me.GroupFooter1.Visible = False
        '
        'RPT_Diferencias80_20DeptoInvGral
        '
        Me.ReportWidth = 1056.0!
        Me.Sections.AddRange(New dbAutoTrack.DataReports.Section() {Me.PageHeader, Me.GroupHeader1, Me.Detail, Me.GroupFooter1, Me.PageFooter})
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region
    Dim pageno As Integer = 1
    Dim conteo As Integer = 1
    Dim totalpaginas As Integer = 0
    Dim Registros As Integer = 0
    Dim ContID As Integer = 0
    Private Sub PageHeader_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles PageHeader.Initialize
        Me.lblPage.Text = ("P�gina: " & Me.pageno.ToString & " De " & MyBase.MaxPages.ToString)
        Me.pageno = Me.pageno + 1
        totalpaginas = MyBase.MaxPages
    End Sub

    Private Sub GroupFooter1_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupFooter1.Initialize
        Me.DbATPageBreak1.Enabled = True
        Me.DbATPageBreak1.Visible = True
    End Sub
   
    Private Sub Detail_Initialize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Detail.Initialize
        ContID += 1
        DbATTextBox4.Value = ContID
    End Sub

    Private Sub GroupHeader1_Initialize(sender As Object, e As System.EventArgs) Handles GroupHeader1.Initialize
        ContID = 0
    End Sub
End Class


