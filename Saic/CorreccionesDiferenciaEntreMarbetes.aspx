﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CorreccionesDiferenciaEntreMarbetes.aspx.vb" Inherits="Saic.CorreccionesDiferenciaEntreMarbetes" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%"> 
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Correcciones finales" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Diferencia de marbete" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
		<tr>
            <td>
                <wum:wucMessage ID="wucMessageCorreccionesDiferenciasEntreMarbetes" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="30%">
                    <tr>
                        <td colspan="2" align="center"><br />
                            <asp:button id="btnConsultar" runat="server" Text="Consultar" CssClass="btn"></asp:button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <asp:button id="btnGuardar2" runat="server" Text="Guardar" CssClass="btn" Visible="false"></asp:button>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <table runat="server">
                    <tr>
                        <td align="right">
                            <asp:label runat="server" ID="lblaviso" Visible="false" Text="Debe elegir una de las opciones: 'Conteo 2 a Conteo 1' o 'Cantidad final'" CssClass="avisos" Font-Size="XX-Small" ></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td><br />
                            <asp:Panel runat="server" DefaultButton="btnGuardar2" >
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView id="dg" runat="server" AutoGenerateColumns="false" SkinID="Grid3">
                                            <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                            <Columns>
                                                <asp:BoundField HeaderText="No." DataField="IdRow" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                <asp:BoundField HeaderText="Marbete" DataField="IdMarbete" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                <asp:BoundField HeaderText="Ubicacion" DataField="Ubicacion" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                <asp:BoundField HeaderText="EAN" DataField="EAN" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                <asp:BoundField HeaderText="Descripci&#243;n" DataField="Descripcion" HeaderStyle-Width="500px" ItemStyle-Width="500px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle"  />
                                                <asp:BoundField HeaderText="Conteo1" DataField="Conteo1" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" DataFormatString="{0:###,##0.00}" />
                                                <asp:BoundField HeaderText="Conteo2" DataField="Conteo2" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" DataFormatString="{0:###,##0.00}" />
                                                <asp:BoundField HeaderText="Diferencia" DataField="Dif" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" DataFormatString="{0:###,##0.00}"/>
                                                <asp:TemplateField HeaderText="Conteo2 a Conteo1" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:CheckBox id="chkCambiar" runat="server"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cantidad Final" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:TextBox id="txtCantidadFinal" runat="server" Width="70px"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
								        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>     
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <asp:button id="btnGuardar" runat="server" Text="Guardar" CssClass="btn" visible="false"></asp:button>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>  
    
                <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
	</table>
</asp:Content>
