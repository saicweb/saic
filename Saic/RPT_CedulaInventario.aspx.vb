﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Public Class RPT_CedulaInventario
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("Sociedad") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageRPT_CedulaInventario.Visible = False

		If Not Page.IsPostBack Then
			Session.Remove("RPT_CedulaInventario")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			CatalogoReportes_CRV13()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Cedula_Inventario.DataBinding
		If Session("RPT_CedulaInventario") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Cedula_Inventario.ReportSource = rpt
				Session("RPT_CedulaInventario") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_Cedula_Inventario.ReportSource = Session("RPT_CedulaInventario")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13()
		Dim nombreReporte As String = Nothing
		If Session("Sociedad") = "1" Then
			nombreReporte = RutaCR & NombreReportes.RPT_CedulaInventario1
		Else
			nombreReporte = RutaCR & NombreReportes.RPT_CedulaInventario2
		End If

		ds = RPT_CedulaInventario()

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(nombreReporte))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "CEDULA DE INVENTARIO"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Cedula_Inventario.ReportSource = rpt
			RPT_Cedula_Inventario.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetInformativo, wucMessageRPT_CedulaInventario)
		End If
	End Sub

	Public Function RPT_CedulaInventario() As DataSet
		Try
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			If Session("Sociedad") = 1 Then
				ds = DatosSQL.funcioncp("RPT_CedulaInventario_C", par)
			ElseIf Session("Sociedad") = 2 Then
				ds = DatosSQL.funcioncp("RPT_CedulaInventario_S", par)
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class