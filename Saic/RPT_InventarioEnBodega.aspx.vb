﻿Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class RPT_InventarioEnBodega
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Not Page.IsPostBack Then
			Session.Remove("RPT_InventarioEnBodega")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		CatalogoReportes_CRV13()
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles Rpt_InventarioEn_Bodega.DataBinding
		If Session("RPT_InventarioEnBodega") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				Rpt_InventarioEn_Bodega.ReportSource = rpt
				Session("RPT_InventarioEnBodega") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			Rpt_InventarioEn_Bodega.ReportSource = Session("RPT_InventarioEnBodega")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13()
		ds = RPT_InventarioEnBodega()

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_InventarioEnBodega))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "REPORTE DE INVENTARIO EN BODEGA"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			Rpt_InventarioEn_Bodega.ReportSource = rpt
			Rpt_InventarioEn_Bodega.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_InventarioBodega)
		End If
	End Sub

	Public Function RPT_InventarioEnBodega() As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@idTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_InventarioEnBodega", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(0) As SqlParameter
				par2(0) = New SqlParameter("@idTienda", Session("IdTienda"))

				dsExcel = DatosSQL.funcioncp("RPT_InventarioEnBodegaExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					Reportes.REPEXCEL(dsExcel, "Reporte de inventario en bodega", "InventarioEnBodega_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class
