﻿Imports System.Data
Imports System.Data.SqlClient
Public Class CapturaRangosMarbetes
	Inherits System.Web.UI.Page

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		Me.wucMessageCapRangMarbetes.Visible = False

		If Session("Perfil").ToString.Trim = "R" Then
			Me.TblCamposRotativo.Visible = True

			If Session("Sociedad").ToString = "3" Then
				Me.lblSubGerenteAdmvo.Visible = False
				Me.txtSubGerenteAdmvo.Visible = False
				Me.lblSubGerenteComercial.Visible = False
				Me.txtSubGerenteComercial.Visible = False
				Me.lblJefeDepto.Visible = False
				Me.txtJefeDepto.Visible = False
			End If
		End If

		If Not Page.IsPostBack Then
			Llenar_Areas()
			CatalogoMarbetes_Buscar()
			If Session("Perfil") = "R" Then InformacionGeneralInventario()
		End If
	End Sub

	Private Sub Llenar_Areas()
		Try
			Dim ds As New DataSet
			ds = DatosSQL.FuncionSP("Areas_Buscar") 'Verificado su uso en Preconteo
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdArea") = "-1"
				dr("NombreArea") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Session("tblAreas") = ds.Tables(0)
				Me.cboArea.DataSource = ds.Tables(0)
				Me.cboArea.DataTextField = "NombreArea"
				Me.cboArea.DataValueField = "IdArea"
				Me.cboArea.DataBind()
			Else
				CMensajes.MostrarMensaje("No existen areas registradas en la base de datos.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				Me.btnAgregar.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))

			If Session("Perfil") = "P" Then
				ds = DatosSQL.funcioncp("CatalogoMarbetesRango_Buscar2", par)
			Else
				ds = DatosSQL.funcioncp("CatalogoMarbetesRango_Buscar", par)
			End If

			If ds.Tables(0).Rows.Count = 0 Then
				ds.Tables(0).Rows.Add(0)
			End If

			Me.dg.DataSource = ds.Tables(0)
			Me.dg.DataBind()
			'Me.btnGuardar.Visible = True

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub InformacionGeneralInventario()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("InformacionGeneralInventario_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				If IsDBNull(ds.Tables(0).Rows(0).Item("FechaInventario2")) = False Then
					Me.txtFechaInventario.Text = ds.Tables(0).Rows(0).Item("FechaInventario2")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("TipoInventarioDesc")) = False Then
					Me.txtTipoInventario.Text = ds.Tables(0).Rows(0).Item("TipoInventarioDesc")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreGerente")) = False Then
					Me.txtGerente.Text = ds.Tables(0).Rows(0).Item("NombreGerente")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreSubGerenteAdmvo")) = False Then
					Me.txtSubGerenteAdmvo.Text = ds.Tables(0).Rows(0).Item("NombreSubGerenteAdmvo")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreSubGerenteComercial")) = False Then
					Me.txtSubGerenteComercial.Text = ds.Tables(0).Rows(0).Item("NombreSubGerenteComercial")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreJefeDepto")) = False Then
					Me.txtJefeDepto.Text = ds.Tables(0).Rows(0).Item("NombreJefeDepto")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreJefeControlInventarios")) = False Then
					Me.txtJefeControlInventarios.Text = ds.Tables(0).Rows(0).Item("NombreJefeControlInventarios")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreRespAdmonInventarios")) = False Then
					Me.txtReponsableAdmonInventario.Text = ds.Tables(0).Rows(0).Item("NombreRespAdmonInventarios")
				End If
			Else
				Me.btnAgregar.Visible = False
				'Me.btnGuardar.Visible = False
				CMensajes.MostrarMensaje("No existe ningún inventario iniciado.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoMarbetes_Administracion(ByVal Accion As Int16, ByVal IdRango As Integer, ByVal IdArea As String, ByVal UbicacionFisica As String, ByVal RangoInicial As String, ByVal RangoFinal As String)
		Try
			'Accion 1=Agregar 2=Modificar 3=Eliminar
			If Accion = 1 Then
				If Me.txtInicioRango.Text.Trim.Length < 11 Then
					CMensajes.MostrarMensaje("El rango inicial debe contar con 11 digitos.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf IsNumeric(Me.txtInicioRango.Text.Trim) = False OrElse Me.txtInicioRango.Text.Trim.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese el rango inicial.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf Me.txtFinalRango.Text.Trim.Length < 11 Then
					CMensajes.MostrarMensaje("El rango final debe contar con 11 digitos.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf Left(Me.txtInicioRango.Text.Trim, 3) <> Right(Session("IdTienda"), 3) Then
					CMensajes.MostrarMensaje("El rango inicial debe tener como prefijo la tienda con la cual se esta trabajando. Ejemplo: " & Right(Session("IdTienda"), 3) & Right(Me.txtInicioRango.Text.Trim, 8), CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf IsNumeric(Me.txtFinalRango.Text.Trim) = False OrElse Me.txtFinalRango.Text.Trim.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese el rango final.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf CDbl(Me.txtInicioRango.Text.Trim) > CDbl(Me.txtFinalRango.Text.Trim) Then
					CMensajes.MostrarMensaje("El rango es incorrecto.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf Left(Me.txtFinalRango.Text.Trim, 3) <> Right(Session("IdTienda"), 3) Then
					CMensajes.MostrarMensaje("El rango final debe tener como prefijo la tienda con la cual se esta trabajando. Ejemplo: " & Right(Session("IdTienda"), 3) & Right(Me.txtFinalRango.Text.Trim, 8), CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf Me.txtUbicacionFisica.Text.Trim.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese ubicación física.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				ElseIf Me.cboArea.SelectedValue = "-1" Then
					CMensajes.MostrarMensaje("Seleccione el área.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
					GoTo salir
				End If
			Else
				GoTo continuar
			End If

continuar:
			Dim ds As New DataSet
			Dim par(6) As SqlParameter
			par(0) = New SqlParameter("@Accion", Accion)
			par(1) = New SqlParameter("@IdRango", IdRango)
			par(2) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(3) = New SqlParameter("@IdArea", IdArea)
			par(4) = New SqlParameter("@UbicacionFisica", UbicacionFisica)
			par(5) = New SqlParameter("@RangoInicial", RangoInicial)
			par(6) = New SqlParameter("@RangoFinal", RangoFinal)

			If Session("Perfil") = "P" Then
				ds = DatosSQL.funcioncp("CatalogoMarbetesRango_Administracion2", par)
			Else
				ds = DatosSQL.funcioncp("CatalogoMarbetesRango_Administracion", par)
			End If

			Me.CatalogoMarbetes_Buscar()
			Me.txtInicioRango.Text = ""
			Me.txtFinalRango.Text = ""
			Me.txtUbicacionFisica.Text = ""
			Me.cboArea.SelectedValue = -1

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0).ToString, CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
			Else
				Select Case Accion
					Case 1
						CMensajes.MostrarMensaje("Marbetes almacenados correctamente.", CMensajes.Tipo.GetExito, wucMessageCapRangMarbetes)
					Case 2
						CMensajes.MostrarMensaje("Rango de marbetes modificado correctamente.", CMensajes.Tipo.GetExito, wucMessageCapRangMarbetes)
					Case 3
						CMensajes.MostrarMensaje("Rango de marbetes eliminado correctamente.", CMensajes.Tipo.GetExito, wucMessageCapRangMarbetes)
				End Select
			End If
salir:
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
		Me.CatalogoMarbetes_Administracion(1, 0, Me.cboArea.SelectedValue, Me.txtUbicacionFisica.Text.Trim.ToUpper, Me.txtInicioRango.Text.Trim, Me.txtFinalRango.Text.Trim)
	End Sub

	Private Sub dg_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dg.RowCommand
		Try
			Dim irow As Integer = Integer.Parse(e.CommandArgument.ToString)
			If e.CommandName = "Eliminar" Then
				Me.CatalogoMarbetes_Administracion(3, Integer.Parse(dg.Rows(irow).Cells(0).Text), "", "", "", "")
			ElseIf e.CommandName = "Modificar" Then
				Dim RI As String = CType(dg.Rows(irow).Cells(1).Controls(1), TextBox).Text.Trim
				Dim RF As String = CType(dg.Rows(irow).Cells(2).Controls(1), TextBox).Text.Trim
				Dim Ubicacion As String = CType(dg.Rows(irow).Cells(3).Controls(1), TextBox).Text.Trim
				Dim comboArea As DropDownList = CType(dg.Rows(irow).Cells(4).Controls(1), DropDownList)

				If comboArea.SelectedValue = "-1" Then
					CMensajes.MostrarMensaje("Seleccione el área.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				ElseIf RI.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese el rango inicial.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				ElseIf RF.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese el rango final.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				ElseIf IsNumeric(RI) = False Then
					CMensajes.MostrarMensaje("Ingrese el rango inicial.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				ElseIf IsNumeric(RF) = False Then
					CMensajes.MostrarMensaje("Ingrese el rango final.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				ElseIf CDbl(RI) > CDbl(RF) Then
					CMensajes.MostrarMensaje("El rango es incorrecto.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				ElseIf Ubicacion.Length = 0 Then
					CMensajes.MostrarMensaje("Ingrese la ubicación física.", CMensajes.Tipo.GetError, wucMessageCapRangMarbetes)
				Else
					Me.CatalogoMarbetes_Administracion(2, dg.Rows(irow).Cells(0).Text, comboArea.SelectedValue, Ubicacion, RI, RF)
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub dg_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dg.RowDataBound
		Try
			If e.Row.RowType = DataControlRowType.DataRow Then
				If e.Row.Cells(0).Text = 0 Then
					'Dim prueba As String = e.Row.Cells(1).Text
					For i As Integer = 0 To e.Row.Cells.Count - 1
						e.Row.Cells(i).Controls.Clear()
					Next
					e.Row.Cells(1).Text = "No records to display."
				Else
					Dim IdArea As String = e.Row.Cells(5).Text
					Dim ComboAreas As DropDownList = CType(e.Row.Cells(4).Controls(1), DropDownList)
					ComboAreas.DataSource = CType(Session("tblAreas"), DataTable)
					ComboAreas.DataTextField = "NombreArea"
					ComboAreas.DataValueField = "IdArea"
					ComboAreas.DataBind()

					ComboAreas.SelectedValue = IdArea
					ComboAreas.Dispose()

					Dim link As LinkButton = CType(e.Row.Cells(6).Controls(1), LinkButton)
					Dim link2 As LinkButton = CType(e.Row.Cells(7).Controls(1), LinkButton)
					'CMensajes.AddConfirmLink(link, "¿Esta seguro que desea Modificar el Rango de Marbetes seleccionado?")
					'CMensajes.AddConfirmLink(link2, "¿Esta seguro que desea Eliminar el Rango de Marbetes seleccionado?")
				End If

				dg.HeaderRow.Cells(0).Visible = False
				e.Row.Cells(0).Visible = False
				dg.HeaderRow.Cells(5).Visible = False
				e.Row.Cells(5).Visible = False

			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class
