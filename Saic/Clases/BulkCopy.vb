﻿Imports System.Data.SqlClient
Imports System.IO

Public Class BulkCopy

	''' <summary>
	''' Inserta datos en tabla específica
	''' </summary>
	''' <param name="archivo"></param>
	''' <param name="split"></param>
	''' <param name="tabla"></param>
	''' <returns></returns>
	Public Shared Function Insertar(ByVal archivo As String, ByVal split As String, ByVal tabla As String) As Boolean
		Dim cnn As String = CReglasNegocio.CnnSAIC
		Dim regreso As Boolean = False
		Dim line As String = Nothing
		Dim dt As New DataTable()
		Dim Archivo2 As New FileInfo(archivo)
		Dim LECTOR As New StreamReader(Archivo2.FullName)
		Dim col As Integer = 0

		Try
			Do
				line = LECTOR.ReadLine()
				If line Is Nothing Then
					Exit Do
				End If

				If col = 0 Then
					Dim datas() As String = line.Split(split)
					col = datas.Length
					For Each item In datas
						dt.Columns.Add(New DataColumn())
					Next item
				End If

				Dim datos() As String
				datos = line.Split("|")
				If datos.Length = 0 Then
					Exit Do
				Else
					Dim row As DataRow = dt.NewRow()
					Dim datos2(datos.Length - 1) As String
					For i = 0 To datos.Length - 1
						datos2(i) = datos(i).Trim
					Next
					row.ItemArray = datos2
					dt.Rows.Add(row)
				End If
			Loop
			LECTOR.Close()

			If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
				Using cn As New SqlConnection(cnn)
					cn.Open()
					Using copy As New SqlBulkCopy(cnn)
						copy.BulkCopyTimeout = 0
						Dim columnas = dt.Columns.Count

						For j As Integer = 0 To columnas - 1
							copy.ColumnMappings.Add(j, j)
						Next
						copy.DestinationTableName = tabla
						copy.WriteToServer(dt)
						regreso = True
					End Using
				End Using
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
		Return regreso
	End Function

	''' <summary>
	''' Inserta datos en tabla específica
	''' </summary>
	''' <param name="dt"></param>
	''' <param name="tabla"></param>
	''' <returns></returns>
	Public Shared Function Insertar(ByVal dt As DataTable, ByVal tabla As String) As Boolean
		Dim cnn As String = CReglasNegocio.CnnSAIC
		Dim regreso As Boolean = False

		Try
			If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
				Using cn As New SqlConnection(cnn)
					cn.Open()
					Using copy As New SqlBulkCopy(cnn)
						copy.BulkCopyTimeout = 0
						Dim columnas = dt.Columns.Count

						For j As Integer = 0 To columnas - 1
							copy.ColumnMappings.Add(j, j)
						Next

						copy.DestinationTableName = tabla
						copy.WriteToServer(dt)
					End Using
				End Using
				regreso = True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
		Return regreso
	End Function
End Class
