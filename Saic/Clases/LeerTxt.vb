﻿Imports System.IO

Public Class LeerTxt
	''' <summary>
	''' Lee archivo txt y lo convierte en  data table
	''' </summary>
	''' <param name="archivo"></param>
	''' <param name="split"></param>
	''' <returns></returns>
	Public Shared Function LeerTxtADT(ByVal archivo As String, ByVal split As String) As DataTable
		Try
			Dim regreso As Boolean = False
			Dim line As String = Nothing
			Dim dt As New DataTable()
			Dim Archivo2 As New FileInfo(archivo)
			Dim LECTOR As New StreamReader(Archivo2.FullName)
			Dim col As Integer = 0

			Do
				line = LECTOR.ReadLine()
				If line Is Nothing Then
					Exit Do
				End If

				If col = 0 Then
					Dim datas() As String = line.Split(split)
					col = datas.Length
					For Each item In datas
						dt.Columns.Add(New DataColumn())
					Next item
				End If

				Dim datos() As String
				datos = line.Split(split)
				If datos.Length = 0 Then
					Exit Do
				Else
					Dim row As DataRow = dt.NewRow()
					Dim datos2(datos.Length - 1) As String
					For i = 0 To datos.Length - 1
						datos2(i) = datos(i).Trim
					Next
					row.ItemArray = datos2
					dt.Rows.Add(row)
				End If
			Loop
			LECTOR.Close()

			Return dt
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Function

    ''' <summary>
    ''' Lee archivo txt y lo convierte en  data table
    ''' </summary>
    ''' <param name="archivo"></param>
    ''' <param name="split"></param>
    ''' <returns></returns>
    Public Shared Function LeerTxtConteos(ByVal archivo As String, ByVal split As String) As DataTable
        Try
            Dim regreso As Boolean = False
            Dim line As String = Nothing
            Dim dt As New DataTable()
            Dim Archivo2 As New FileInfo(archivo)
            Dim LECTOR As New StreamReader(Archivo2.FullName)
            Dim col As Integer = 0

            Do
                line = LECTOR.ReadLine()
                If line Is Nothing Then
                    Exit Do
                End If

                If col = 0 Then
                    Dim datas() As String = line.Split(split)
                    col = datas.Length
                    For Each item In datas
                        dt.Columns.Add(New DataColumn())
                    Next item
                End If

                Dim datos() As String
                datos = line.Split(split)
                If datos.Length = 0 Then
                    Exit Do
                Else
                    Dim row As DataRow = dt.NewRow()
                    Dim datos2(datos.Length - 1) As String
                    For i = 0 To datos.Length - 1
                        datos2(i) = datos(i).Trim
                    Next
                    row.ItemArray = datos2

                    Dim valido As Boolean = True
                    For i As Integer = 0 To datos2.Count - 1
                        If datos2(i) = "" Then
                            valido = False
                            Exit For
                        End If
                    Next

                    If valido Then
                        dt.Rows.Add(row)
                    End If

                End If
            Loop
            LECTOR.Close()

            Return dt
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function


    ''' <summary>
    ''' Elimina celdas/registros vacíos del data table y los elimina
    ''' </summary>
    ''' <param name="dt"></param>
    ''' <returns></returns>
    Public Shared Function ValidarDT(ByRef dt As DataTable) As DataTable
		For i As Integer = dt.Rows.Count - 1 To 0 Step -1
			Dim vacio As Integer = 0
			For j As Integer = 0 To dt.Columns.Count - 1
				If dt.Rows(i)(j) Is DBNull.Value OrElse String.IsNullOrEmpty(dt.Rows(i)(j)) OrElse String.IsNullOrWhiteSpace(dt.Rows(i)(j)) Then
					vacio += 1
				Else
					dt.Rows(i)(j) = dt.Rows(i)(j).ToString.Trim
					dt.AcceptChanges()
				End If
			Next
			If vacio > 0 Then
				dt.Rows.RemoveAt(i)
				dt.AcceptChanges()
			End If
		Next

		Return dt
	End Function
End Class
