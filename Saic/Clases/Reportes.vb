Imports System
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Xml
Imports NativeExcel
Imports System.Data
Imports System.Drawing
Imports System.Data.SqlClient

Public Class Reportes
	Public Shared Sub Exportar_Excel(ByVal datos As DataTable, ByVal archivo As String, ByVal TituloPrincipal As String, Subtitulos As String)
		'Dim FechaFormato = Format(Now, "yyyyMMdd_hhmm")
		'Dim Fechaejecucion = Format(Now, "dd/MM/yyyy hh:mm:ss")
		'Dim NombreArchivo As String = "XLSReporteEmbarque" & FechaFormato
		'Path = Path & NombreArchivo & ".xls"
		'archivo = Path

		Dim d As Integer
		Dim r As Integer
		Dim i As Integer
		Dim j As Integer
		Dim w As Integer = 1
		Dim k As Integer
		Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
		Dim sheet As IWorksheet = book.Worksheets.Add()
		Dim pathExcel As String = archivo
		Dim col As Integer
		Dim ren As Integer
		Dim c As Integer
		Dim renglon As Integer

		col = datos.Columns.Count - 1
		ren = datos.Rows.Count - 1

		Dim arrEncabezados(col) As String

		For c = 0 To col
			arrEncabezados(c) = datos.Columns(c).ColumnName
		Next

		book.Worksheets(w).Cells(1, 1).Value = TituloPrincipal
		book.Worksheets(w).Cells(1, 1).Font.Size = 14
		book.Worksheets(w).Cells(1, 1).Font.Bold = True
		book.Worksheets(w).Cells(1, 1).Font.Color = Drawing.Color.White
		book.Worksheets(w).Cells(1, 1).Interior.Color = Drawing.Color.Navy
		'book.Worksheets(w).Cells(2, 1).Interior.Color = Drawing.Color.Navy
		book.Worksheets(w).Cells(1, 1).Borders.Weight = XlBorderWeight.xlThin
		book.Worksheets(w).Cells(1, 1).Borders.Color = Drawing.Color.White
		book.Worksheets(w).Range(1, 1, 2, col + 1).Merge()
		book.Worksheets(w).Range(1, 1, 2, col + 1).HorizontalAlignment = XlHAlign.xlHAlignCenter
		'book.Worksheets(w).Range("A2:" & LetraFin & "2").Merge()

		book.Worksheets(w).Cells(3, 1).Value = Subtitulos
		book.Worksheets(w).Cells(3, 1).Autofit()
		book.Worksheets(w).Cells(3, 1).Borders.Weight = XlBorderWeight.xlThin
		book.Worksheets(w).Cells(3, 1).Borders.Color = Drawing.Color.White
		book.Worksheets(w).Range(3, 1, 3, 4).Merge()
		book.Worksheets(w).Range(3, 1, 3, 4).HorizontalAlignment = XlHAlign.xlHAlignLeft






		For j = 0 To arrEncabezados.Length - 1
			book.Worksheets(w).Cells(4, j + 1).Value = arrEncabezados(j)
			book.Worksheets(w).Cells(4, j + 1).Font.Color = Drawing.Color.White
			book.Worksheets(w).Cells(4, j + 1).Interior.Color = Drawing.Color.Navy
			book.Worksheets(w).Cells(4, j + 1).Borders.Weight = XlBorderWeight.xlThin
			book.Worksheets(w).Cells(4, j + 1).Borders.Color = Drawing.Color.White
			book.Worksheets(w).Cells(4, j + 1).Font.Bold = True
		Next

		renglon = 5

		For r = 0 To ren
			For c = 0 To col
				'If r Mod 2 = 0 Then
				'    book.Worksheets(w).Cells(renglon, c + 1).Interior.Color = Drawing.Color.LightGray
				'End If
				book.Worksheets(w).Cells(renglon, c + 1).Value = datos.Rows(r).Item(c)

			Next
			renglon += 1

			If renglon > 65000 Then
				sheet = book.Worksheets.Add
				w += 1
				book.Worksheets(w).Cells(2, 1).Value = TituloPrincipal
				book.Worksheets(w).Cells(2, 1).Font.Size = 14
				book.Worksheets(w).Cells(2, 1).Font.Bold = True

				For j = 0 To arrEncabezados.Length - 1
					book.Worksheets(w).Cells(4, j + 1).Value = arrEncabezados(j)
				Next
				renglon = 4
			End If
		Next


		'For k = 1 To w
		'    For j = 0 To arrEncabezados.Length - 1
		'        book.Worksheets(k).Cells(4, j + 1).Font.Bold = True
		'        book.Worksheets(k).Cells(4, j + 1).Autofit()
		'    Next
		'Next

		'For k = 1 To w
		'    For j = 0 To arrEncabezados.Length - 1
		'        book.Worksheets(k).Cells.Columns.Autofit()
		'    Next
		'Next

		book.SaveAs(pathExcel)

	End Sub

	Public Shared Function Exportar_ExcelDinamico(ByVal datos As DataTable, ByVal archivo As String, ByVal TituloReporte As String, ByVal SubTituloReporte As String) As Boolean
		Try
			Dim r, j, k As Integer
			Dim w As Integer = 1
			' crea archivos de Excel para interfases sin errores
			Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
			' crea una nueva hoja de calculo en el archivo de Excel
			Dim sheet As IWorksheet = book.Worksheets.Add()

			Dim pathExcel As String = archivo
			Dim col, ren, c, renglon, UltimaCol As Integer
			Dim TipoColumna, Merge(), MergeStr(), Colores, Formatos() As String
			'TOTAL DE COLUMNAS Y RENGLONES A IMPRIMIR
			If TituloReporte = "Diferencias Por Departamento" Then
				col = datos.Columns.Count - (1 + 10)
			ElseIf TituloReporte = "Articulos No Contados Fisicamente" Then
				col = datos.Columns.Count - (1 + 8)
			Else
				col = datos.Columns.Count - (1 + 5)  'oncj
			End If
			ren = datos.Rows.Count - 1

			'IMPRIMIR TITULO DE REPORTE
			book.Worksheets(w).Cells(1, 1).Value = TituloReporte
			book.Worksheets(w).Range(1, 1).Interior.Color = Color.Navy
			book.Worksheets(w).Range(1, 1).Font.Color = Color.White
			book.Worksheets(w).Range(1, 1).Font.Bold = True
			book.Worksheets(w).Range(1, 1).Font.Size = 12
			book.Worksheets(w).Range(1, 1).HorizontalAlignment = XlHAlign.xlHAlignCenter
			book.Worksheets(w).Range(1, 1, 1, col + 1).Merge()
			'IMPRIMIR SUBTITULO DEL REPORTE
			book.Worksheets(w).Cells(2, 1).Value = SubTituloReporte
			book.Worksheets(w).Range(2, 1).Interior.Color = Color.Navy
			book.Worksheets(w).Range(2, 1).Font.Color = Color.White
			book.Worksheets(w).Range(2, 1).Font.Bold = True
			book.Worksheets(w).Range(1, 1).Font.Size = 10
			book.Worksheets(w).Range(2, 1).HorizontalAlignment = XlHAlign.xlHAlignCenter
			book.Worksheets(w).Range(2, 1, 2, col + 1).Merge()
			book.Worksheets(w).Cells(1, 1).Autofit()
			book.Worksheets(w).Cells(2, 1).Autofit()
			' escribe datos
			renglon = 3
			UltimaCol = 0

			For r = 0 To ren
				TipoColumna = datos.Rows(r).Item("Tipo")
				Merge = CStr(datos.Rows(r).Item("Merge")).Split("/")
				Colores = datos.Rows(r).Item("Color")
				Formatos = CStr(datos.Rows(r).Item("Formatos")).Split("/")
				UltimaCol = datos.Rows(r).Item("UltimaColumna")
				'SI ES ENCABEZADO PONER EN NEGRITAS
				For c = 0 To col
					book.Worksheets(w).Cells(renglon, c + 1).Value = datos.Rows(r).Item(c)
					If TipoColumna = "D" Then
						book.Worksheets(w).Range(renglon, UltimaCol).Borders.Item(XlBordersIndex.xlAround).Weight = XlBorderWeight.xlThin
					End If
					'BORDES, Si bortes para SAIC
					If TipoColumna.Length > 0 AndAlso c <= UltimaCol Then
						book.Worksheets(w).Range(renglon, 1, renglon, UltimaCol).Borders.Item(XlBordersIndex.xlAround).Weight = XlBorderWeight.xlThin
					End If
				Next
				If TipoColumna.Length > 0 Then
					'APLICAR COLOR DE FONDO
					If Colores.Trim.Length > 0 Then
						book.Worksheets(w).Range(renglon, 1, renglon, UltimaCol).Interior.Color = Reportes.ColorDeFondo(Colores)
					End If
					'EN NEGRITA SI ES ENCABEZADO
					If TipoColumna = "C" Then
						book.Worksheets(w).Range(renglon, 1, renglon, UltimaCol).Font.Bold = True
					End If
					'MERGE
					For iMerge As Integer = 0 To Merge.Length - 1 'Merge(iMerge)
						MergeStr = Merge(iMerge).Split(",")
						If MergeStr.Length >= 3 Then
							book.Worksheets(w).Range(renglon, CInt(MergeStr(0)), renglon + CInt(MergeStr(1)), CInt(MergeStr(2))).Merge()
							book.Worksheets(w).Range(renglon, CInt(MergeStr(0))).HorizontalAlignment = XlHAlign.xlHAlignCenter
						End If
					Next
					''   Formatos()
					If TituloReporte <> "REPORTE DE CORRECIONES A CONTEOS" Then
						For iFormatos As Integer = 0 To Formatos.Length - 1
							If Formatos(iFormatos).Length > 0 Then
								AplicarFormato(Formatos(iFormatos), book.Worksheets(w), renglon)
							End If
						Next
					End If
				End If
				renglon += 1
				' determina limite de Excel
				If renglon > 65000 Then
					' crea una nueva hoja para superar el limite
					sheet = book.Worksheets.Add
					w += 1
					renglon = 3
				End If
			Next

			'ajusta formato de encabezados de datos de todas las hojas creadas
			For k = 1 To w
				For j = 0 To col
					book.Worksheets(k).Cells(1, j + 1).Font.Bold = True
					book.Worksheets(k).Cells(1, j + 1).Autofit()
				Next
				book.Worksheets(k).UsedRange.Autofit()
			Next

			book.SaveAs(pathExcel)
			Return True
		Catch ex As Exception
			Throw New Exception("/R2-" & ex.Message)
			Return False
		End Try
	End Function
	Shared Sub AplicarFormato(ByVal InfoFormato As String, ByVal xlWorkSheet As NativeExcel.IWorksheet, ByVal Row As Integer)
		'--DEFINICION DE FORMATOS
		Dim FormatoRegistro() As String = InfoFormato.ToString.Split("-")
		'INDICE DE FORMATOS
		'1=Formato de numero entero
		'2=Formato Condicional numero entero
		'3=Formato de numero decimal
		'4=Formato Condicional decimal
		'5=Formato de Moneda
		'6=Formato Condicional Moneda
		'7=Formato %
		'8=formato porcentaje rojo si es negativo y azul si es positivo
		'9=borde superior grueso
		'10=Borde alrededor
		'11=Letra en negrita
		'12= Borde Inferior Grueso
		For r As Integer = 0 To (FormatoRegistro.Length - 1) Step 2
			Select Case FormatoRegistro(r)
				Case "1"  '1=Formato de numero entero
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Value = xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Value
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "###,##0"
				Case "2"  '2=Formato Condicional numero entero
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "[Black]###,##0;[Red]-###,##0"
				Case "3"  '3=Formato de numero decimal
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "###,##0.00"
				Case "4"  '4=Formato Condicional decimal
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "[Black]###,##0.00;[Red]-###,##0.00"
				Case "5"  '5=Formato de Moneda
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Value = CDbl(xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Value)
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "$#,##0.00;-$#,##0.00"
				Case "6"  '6=Formato Condicional Moneda
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "$#,##0.00;[Rojo]-$#,##0.00"
				Case "7" '7=Formato %
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "#,##0.00%;-#,##0.00%"
				Case "8" '8=formato porcentaje rojo si es negativo y azul si es positivo
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "[Blue]#,##0.00%;[Red]-#,##0.00%"
				Case "9" '9=borde Inferior grueso
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Borders.Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick
				Case "10" '10=Borde alrededor
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Borders.Weight = XlBorderWeight.xlThin
				Case "11" '11=Letra en negrita
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Font.Bold = True
				Case "12" '9=borde Superiro  grueso
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Borders.Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlThick
				Case "13" '9=borde inferior  delgado
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Borders.Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin
					'Case "12"
					'    '[$-C0A]dd-mmm-aa;@
					'    xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					'    xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "[Blue]#,##0.00%;[Red]-#,##0.00%"
					'Case "13"
					'    'dd/mm/aa;@
					'    xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					'    xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))). = "dd/mm/aa;@"
					'Case "14"
					'    'dd/mm/aaaa;@
					'    xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).Select()
					'    xlWorkSheet.Range(Row, CInt(FormatoRegistro(1))).NumberFormat = "dd/mm/aaaa;@"
			End Select
		Next
	End Sub
	Shared Function ColorDeFondo(ByVal Colores As String) As Color
		Select Case Colores
			Case "White"
				Return Color.White
			Case "Bisque"
				Return Color.Bisque
			Case "YellowGreen"
				Return Color.YellowGreen
			Case "LightBlue"
				Return Color.LightBlue
			Case "LightGoldenrodYellow"
				Return Color.LightGoldenrodYellow
		End Select
	End Function

	Shared Sub pintarceldarojo(ByVal libro As NativeExcel.IWorksheet, ByVal celda As String)
		libro.Cells(celda).Interior.Color = Color.Red
	End Sub
	Shared Sub pintarceldarojo(ByVal libro As NativeExcel.IWorksheet, ByVal renglon As Integer, ByVal columna As Integer)
		libro.Cells(renglon, columna).Interior.Color = Color.Red
	End Sub
	Shared Sub pintarceldaamarilla(ByVal libro As NativeExcel.IWorksheet, ByVal celda As String)
		libro.Cells(celda).Interior.Color = Color.LightYellow
	End Sub
	Shared Sub pintarceldaamarilla(ByVal libro As NativeExcel.IWorksheet, ByVal renglon As Integer, ByVal columna As Integer)
		libro.Cells(renglon, columna).Interior.Color = Color.LightYellow
	End Sub
	Shared Sub pintarceldagris(ByVal libro As NativeExcel.IWorksheet, ByVal celda As String)
		libro.Cells(celda).Interior.Color = Color.LightGray
	End Sub
	Shared Sub pintarceldagris(ByVal libro As NativeExcel.IWorksheet, ByVal renglon As Integer, ByVal columna As Integer)
		libro.Cells(renglon, columna).Interior.Color = Color.LightGray
	End Sub
	Shared Sub textoblanco(ByVal libro As NativeExcel.IWorksheet, ByVal celda As String)
		libro.Cells(celda).Font.Color = Color.White
	End Sub
	Shared Sub textoblanco(ByVal libro As NativeExcel.IWorksheet, ByVal renglon As Integer, ByVal columna As Integer)
		libro.Cells(renglon, columna).Font.Color = Color.White
	End Sub


	''' <summary>
	''' Crea reporte de excel para descarga de CR
	''' </summary>
	''' <param name="DATOS"></param>
	''' <param name="titulo"></param>
	''' <param name="nombreReporte"></param>
	''' <param name="link"></param>
	''' <param name="Almacen"></param>
	Public Shared Sub REPEXCEL(ByVal DATOS As DataSet, ByVal titulo As String, ByVal nombreReporte As String, ByVal link As HyperLink, ByVal Perfil As String, ByVal Usuario As String, Optional ByVal Almacen As Boolean = False)
		Try
			'Crear el directorio si no existe
			Try
				Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes")

				If Not Directory.Exists(DirectorioArchivos) Then
					Directory.CreateDirectory(DirectorioArchivos)
				End If

				Dim Archivo As String = DirectorioArchivos & nombreReporte & Perfil & "_" & Usuario & ".xls"
				'Guardar la información en el archivo txt
				Dim subtitulo As String = ""
				If Almacen Then
					subtitulo = "Fecha: " & DATOS.Tables(1).Rows(0)(0) & "    Hora: " & DATOS.Tables(1).Rows(0)(1) & "       Tienda: " & DATOS.Tables(1).Rows(0)(2) & "       Almacen: " & DATOS.Tables(1).Rows(0)(3)
				Else
					subtitulo = "Fecha: " & DATOS.Tables(1).Rows(0)(0) & "    Hora: " & DATOS.Tables(1).Rows(0)(1) & "       Tienda: " & DATOS.Tables(1).Rows(0)(2)
				End If
				Reportes.Exportar_ExcelDinamico(DATOS.Tables(0), Archivo, titulo, subtitulo)
				Dim encripta As New Encryption
				link.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
				link.Visible = True
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Obtiene la tienda con la descripción para el cabecero de los reportes
	''' </summary>
	''' <param name="idTienda"></param>
	''' <returns></returns>
	Public Shared Function GetTienda(ByVal idTienda As String) As String
		Try
			Dim dsTienda As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", idTienda)
			dsTienda = DatosSQL.funcioncp("Tiendas_Buscar", par)
			Return dsTienda.Tables(0).Rows(0)("NombreTienda")
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class

