﻿Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security

Public Class Email
	Public Sub enviaEmail(ByVal Destinatarios As String, ByVal Mensaje As String, ByVal Asunto As String)
		Dim _message As New System.Net.Mail.MailMessage
		Dim _SMTP As New System.Net.Mail.SmtpClient

		Dim mailTo As String = ConfigurationManager.AppSettings("mailTo").ToString()
		Dim mailPwd As String = ConfigurationManager.AppSettings("mailPwd").ToString()
		Dim mailHost As String = ConfigurationManager.AppSettings("mailHost").ToString()
		Dim mailPto As String = ConfigurationManager.AppSettings("mailPto").ToString()
		Dim userSMTP As String = ConfigurationManager.AppSettings("userSMTP").ToString()
		Dim pwdSMTP As String = ConfigurationManager.AppSettings("pwdSMTP").ToString()

		Dim mailAsunto As String = Asunto
		Dim mailPara As String = Destinatarios
		Dim mailMensaje As String = Mensaje

		Dim dest() As String = Destinatarios.Split(";")

		Try
			'Configuración del SMTP
			_SMTP.Credentials = New System.Net.NetworkCredential(userSMTP, pwdSMTP)
			_SMTP.Host = mailHost
			_SMTP.Port = Convert.ToInt32(mailPto)
			For i = 0 To dest.Count - 1
				_message.To.Add(dest(i))
			Next
			_message.From = New System.Net.Mail.MailAddress(mailTo, mailTo, System.Text.Encoding.UTF8)
			_message.Subject = mailAsunto
			_message.SubjectEncoding = System.Text.Encoding.UTF8
			_message.Body = mailMensaje
			_message.Priority = System.Net.Mail.MailPriority.Normal
			_message.IsBodyHtml = True
			System.Net.ServicePointManager.ServerCertificateValidationCallback = Function(s As Object, certificate As X509Certificate, chain As X509Chain, sslPolicyErrors As SslPolicyErrors) True

			_SMTP.Send(_message)

		Catch ex As System.Net.Mail.SmtpException
			Throw New Exception(ex.Message)
		End Try
	End Sub
End Class

