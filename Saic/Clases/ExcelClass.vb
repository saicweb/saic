﻿Imports NativeExcel

Public Class ExcelClass
	''' <summary>
	''' Valida que el archivo de excel no esté vacío
	''' </summary>
	''' <param name="ruta"></param>
	''' <returns></returns>
	Public Shared Function ValidarExcel(ByVal ruta As String) As Boolean
		Dim book As IWorkbook = NativeExcel.Factory.OpenWorkbook(ruta)
		If book Is Nothing Then
			Return False
		Else
			Return True
		End If
	End Function

	''' <summary>
	''' Regresa el contenido del archivo de excel
	''' </summary>
	''' <param name="ruta"></param>
	''' <returns></returns>
	Public Shared Function LeerExcel(ByVal ruta As String, Optional ByVal header As Boolean = True) As DataTable
		Dim book As IWorkbook = Factory.OpenWorkbook(ruta)
		If book IsNot Nothing Then
			If header Then
				Return book.Worksheets(1).UsedRange.GetDataTable(True, True)
			Else
				Return book.Worksheets(1).UsedRange.GetDataTable(False, True)
			End If
		End If
	End Function

	''' <summary>
	''' Regresa el contenido del archivo de excel
	''' </summary>
	''' <param name="ruta"></param>
	''' <param name="hoja"></param>
	''' <param name="rango"></param>
	''' <returns></returns>
	Public Shared Function LeerExcel(ByVal ruta As String, ByVal hoja As String, ByVal rango As String, Optional ByVal header As Boolean = True) As DataTable
		Dim book As IWorkbook = Factory.OpenWorkbook(ruta)
		Dim dt As DataTable = Nothing
		If book IsNot Nothing Then
			If header Then
				dt = book.Worksheets(hoja).Range(rango).GetDataTable(True, True)
			Else
				dt = book.Worksheets(hoja).Range(rango).GetDataTable(False, True)
			End If
		End If
		Return dt
	End Function
End Class
