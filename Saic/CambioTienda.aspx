﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CambioTienda.aspx.vb" Inherits="Saic.CambioTienda" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%"  >
        <tr>
            <td class="titulos">
                Cambio de tienda
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCambioTienda" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="30%" align="center">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Tienda: " ></asp:Label>
                        </td>
                        <td>         
                            <asp:DropDownList runat="server" id="cboTienda" Width="250px" CssClass="dropdown inline"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><br />
                            <span><asp:Button runat="server" Text="Cambiar" ID="btnCambiar" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
