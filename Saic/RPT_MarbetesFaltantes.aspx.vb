﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_MarbetesFaltantes
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageRPT_MarbetesFaltantes.Visible = False

		If Not Page.IsPostBack Then
			Try
				If Session("Perfil") = "R" Then
					CargarFiltro()
				End If

				Session.Remove("RPT_MarbetesFaltantes")
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Sub CargarFiltro()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("Filtro_CatMarbeteRango", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				dg.DataSource = ds
				dg.DataBind()
				Me.PnlRotativo.Visible = True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim Ubicaciones As String = ""
			For i As Integer = 0 To dg.Rows.Count - 1
				Dim chk As CheckBox = TryCast(dg.Rows(i).FindControl("Checkbox4"), CheckBox)
				If chk.Checked Then
					Ubicaciones &= "'" & dg.Rows(i).Cells(1).Text & "',"
				End If
			Next

			If Ubicaciones.Length > 0 Then
				Ubicaciones = Ubicaciones.Substring(0, Ubicaciones.Length - 1)
			Else
				Ubicaciones = "''"
			End If

			CatalogoReportes_CRV13(Ubicaciones)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Marbetes_Faltantes.DataBinding
		If Session("RPT_MarbetesFaltantes") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Marbetes_Faltantes.ReportSource = rpt
				Session("RPT_MarbetesFaltantes") = rpt
				Me.TblCabecero.Visible = False
				Me.PnlRotativo.Visible = False
			End If
		Else
			RPT_Marbetes_Faltantes.ReportSource = Session("RPT_MarbetesFaltantes")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Ubicaciones As String)
		ds = RPT_MarbetesFaltantes(Ubicaciones)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			Dim nomReporte As String = IIf(Session("Sociedad").ToString = "3", NombreReportes.RPT_MarbetesFaltantes_RST, NombreReportes.RPT_MarbetesFaltantes)
			rpt.Load(Server.MapPath(RutaCR & nomReporte))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "REPORTE DE MARBETES FALTANTES Y FUERA DE RANGO(*)"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Marbetes_Faltantes.ReportSource = rpt
			RPT_Marbetes_Faltantes.DataBind()
		Else
			CMensajes.MostrarMensaje("No existen marbetes faltantes.", CMensajes.Tipo.GetInformativo, wucMessageRPT_MarbetesFaltantes)
		End If
	End Sub

	Public Function RPT_MarbetesFaltantes(ByVal Ubicaciones As String) As DataSet
		Try
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@Ubicacion", Ubicaciones)
			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_MarbetesFaltantes_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_MarbetesFaltantes", par)
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(dg.HeaderRow.Cells(0).FindControl("Checkbox3"), CheckBox)
		For Each row As GridViewRow In dg.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(0).FindControl("Checkbox4"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub
End Class