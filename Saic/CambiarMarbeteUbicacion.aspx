﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CambiarMarbeteUbicacion.aspx.vb" Inherits="Saic.CambiarMarbeteUbicacion" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes" class="titulos" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Cambiar marbete ubicación" ID="lblTitulo" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucmessage ID="wucMessageCambiarMarbUbicacion" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="30%">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Panel runat="server" Width="385px" Height="200px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:TemplateField HeaderText="Todos" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:CheckBox id="Checkbox3" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" runat="server" Text="Todos&lt;br /&gt;" TextAlign="Left"></asp:CheckBox>
                                            </HeaderTemplate>
						                    <ItemTemplate>
                                                <asp:CheckBox id="Checkbox4" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
					                    </asp:TemplateField>
								        <asp:BoundField HeaderText="Marbete" DataField="idMarbete" ItemStyle-Width="150px" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
                                        <asp:BoundField HeaderText="Ubicación" DataField="Ubicacion" ItemStyle-Width="150px" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td><br />
                            <asp:Label runat="server" Text="Ubicación nueva: " ></asp:Label>
                        </td>
                        <td><br />
                            <asp:TextBox runat="server" ID="txtUbicacionNueva" MaxLength="10" Width="150px" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:Button runat="server" Text="Actualizar" ID="btnActualizar" CssClass="btn"/></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
