﻿Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data
Public Class DatosSQL



	Public Shared Function FuncionSP(ByVal sentencia As String) As Data.DataSet
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			Dim adap As New SqlDataAdapter
			Dim dset As New DataSet
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.StoredProcedure
			comando.CommandText = sentencia
			adap.SelectCommand = comando
			adap.Fill(dset)
			Return dset
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Function
	Public Shared Function FuncionSPtext(ByVal sentencia As String) As Data.DataSet
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			Dim adap As New SqlDataAdapter
			Dim dset As New DataSet
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.Text
			comando.CommandText = sentencia
			adap.SelectCommand = comando
			adap.Fill(dset)
			Return dset
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Function
	Public Shared Function FuncionSPDataTable(ByVal sentencia As String) As Data.DataTable
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			Dim adap As New SqlDataAdapter
			Dim dset As New DataSet
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.StoredProcedure
			comando.CommandText = sentencia
			adap.SelectCommand = comando
			adap.Fill(dset)
			Return dset.Tables(0)
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Function

	Public Shared Function funcioncp(ByVal sentencia As String, ByVal param() As SqlParameter) As Data.DataSet
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			Dim adap As New SqlDataAdapter
			Dim dset As New DataSet
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.StoredProcedure
			comando.CommandText = sentencia
			comando.Parameters.Clear()
			For i As Integer = 0 To param.Length - 1
				comando.Parameters.Add(param(i))
			Next
			adap.SelectCommand = comando
			adap.Fill(dset)
			Return dset
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Function


	Public Shared Function funcioncpSeguridad(ByVal sentencia As String, ByVal param() As SqlParameter) As Data.DataSet
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			Dim adap As New SqlDataAdapter
			Dim dset As New DataSet
			con.ConnectionString = ConfigurationManager.AppSettings("cnnSeguridad").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.StoredProcedure
			comando.CommandText = sentencia
			comando.Parameters.Clear()
			For i As Integer = 0 To param.Length - 1
				comando.Parameters.Add(param(i))
			Next
			adap.SelectCommand = comando
			adap.Fill(dset)
			Return dset
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Function


	Public Shared Sub procedimientocp(ByVal sentencia As String, ByVal param() As SqlParameter)
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandType = CommandType.StoredProcedure
			comando.CommandText = sentencia
			comando.CommandTimeout = 0
			comando.Parameters.Clear()
			If param.Length = 0 Then
				comando.Parameters.Add(param(0))
			Else
				For i As Integer = 0 To param.Length - 1
					comando.Parameters.Add(param(i))
				Next
			End If
			comando.ExecuteNonQuery()
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Sub
	Public Shared Sub procedimientosp(ByVal sentencia As String)
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.StoredProcedure
			comando.CommandText = sentencia
			comando.ExecuteNonQuery()
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Sub

	Public Shared Sub procedimientoText(ByVal sentencia As String)
		Dim con As New SqlConnection
		Try
			Dim comando As New SqlCommand
			con.ConnectionString = ConfigurationManager.AppSettings("cnn").ToString
			con.Open()
			comando.Connection = con
			comando.CommandTimeout = 0
			comando.CommandType = CommandType.Text
			comando.CommandText = sentencia
			comando.ExecuteNonQuery()
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Sub
End Class
Public Class DatosAccess
	Public Shared Function FuncionSP(ByVal sentencia As String, ByVal BDAccess As String) As Data.DataTable
		Dim con As New OleDbConnection 'Connection
		Try
			Dim ConString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & BDAccess & ";"
			Dim comando As OleDbCommandBuilder  'Command
			Dim adap As OleDbDataAdapter  'DataAdapter
			Dim dset As New DataSet
			con.ConnectionString = ConString 'ConfigurationSettings.AppSettings("cnnAccess")
			con.Open()

			adap = New OleDbDataAdapter(sentencia, con)
			comando = New OleDbCommandBuilder(adap)
			adap.Fill(dset)
			Return dset.Tables(0)
		Catch ex As Exception
			Throw New Exception(ex.Message)
		Finally
			con.Close()
		End Try
	End Function
End Class
'Public Class DatosOracle
'    Public Shared Function FuncionSP(ByVal sentencia As String) As Data.DataTable
'        Dim con As New OracleConnection 'Connection
'        Try
'            Dim comando As OracleCommandBuilder  'Command
'            Dim adap As OracleDataAdapter  'DataAdapter
'            Dim dset As New DataSet
'            con.ConnectionString = ConfigurationSettings.AppSettings("cnnOracle")
'            con.Open()

'            adap = New OracleDataAdapter(sentencia, con)
'            comando = New OracleCommandBuilder(adap)
'            adap.Fill(dset)
'            Return dset.Tables(0)
'        Catch ex As Exception
'            Throw New Exception(ex.Message)
'        Finally
'            con.Close()
'        End Try
'    End Function
'End Class
