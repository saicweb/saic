﻿Imports System.ComponentModel
Imports System.Web.UI
Imports System.Web.UI.WebControls

''' <summary>
''' Permite Visualizar Mensajes
''' </summary>
Public Class CMensajes
	Private Shared m_executingPages As New Hashtable()

	Private Sub New()
	End Sub

	Public Shared Sub Show(ByVal sMessage As String)
		'si no existe la page entra 
		If Not m_executingPages.Contains(HttpContext.Current.Handler) Then
			' obtiene la pagina que hace referencia
			Dim executingPage As Page = TryCast(HttpContext.Current.Handler, Page)

			If executingPage IsNot Nothing Then
				Dim messageQueue As New Queue()

				' Agrega el valor
				messageQueue.Enqueue(sMessage)

				'agrega el mensaje al Hashtable y la page de referencia  utilizando la page como key
				m_executingPages.Add(HttpContext.Current.Handler, messageQueue)

				'agregamos evento
				AddHandler executingPage.Unload, AddressOf ExecutingPage_Unload
			End If
		Else
			'si entra es por que ya existe 
			Dim queue As Queue = DirectCast(m_executingPages(HttpContext.Current.Handler), Queue)
			queue.Enqueue(sMessage)
		End If
	End Sub

	Private Shared Sub ExecutingPage_Unload(ByVal sender As Object, ByVal e As EventArgs)
		'obtiene los mensajes registrados
		Dim queue As Queue = DirectCast(m_executingPages(HttpContext.Current.Handler), Queue)

		If queue IsNot Nothing Then
			Dim sb As New StringBuilder()

			' cantidad de msg registrados
			Dim iMsgCount As Integer = queue.Count


			sb.Append("<script language='javascript'>")


			Dim sMsg As String
			While System.Math.Max(System.Threading.Interlocked.Decrement(iMsgCount), iMsgCount + 1) > 0
				sMsg = DirectCast(queue.Dequeue(), String)
				sMsg = sMsg.Replace(vbLf, "\n")
				sMsg = sMsg.Replace("""", "'")
				sb.Append("alert( """ & sMsg & """ );")
			End While

			'cierre de etiqueta
			sb.Append("</script>")

			' borra la referencia
			m_executingPages.Remove(HttpContext.Current.Handler)

			'escribe el script generado
			HttpContext.Current.Response.Write(sb.ToString())
		End If
	End Sub

	''' <summary>
	''' permite visualizar mensajes con el control wucMessage
	''' </summary>
	''' <param name="mensaje"></param>
	''' <param name="Tipo"></param>
	''' <param name="wucMessage"></param>
	''' <remarks></remarks>
	Public Shared Sub MostrarMensaje(ByVal mensaje As String, ByVal Tipo As Tipo, ByVal wucMessage As WucMessage)
		wucMessage.Visible = True
		Select Case Tipo
			Case 1 'Exito
				wucMessage.ShowMessage(mensaje, TipoMensaje.Exito)
			Case 2 'Error
				wucMessage.ShowMessage(mensaje, TipoMensaje.Error)
			Case 3 'Informacion
				wucMessage.ShowMessage(mensaje, TipoMensaje.Informativo)
		End Select
	End Sub

	''' <summary>
	''' permite ejecutar un script de tipo javascript
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="type"></param>
	''' <param name="key"></param>
	''' <param name="scritp"></param>
	''' <param name="addScriptTags"></param>
	''' <remarks></remarks>
	Public Shared Sub EjecutarScript(ByVal sender As Object, ByVal type As System.Type, ByVal key As String, ByVal scritp As String, ByVal addScriptTags As Boolean)
		ScriptManager.RegisterStartupScript(sender, type, key, scritp, addScriptTags)
	End Sub

	''' <summary>
	''' Tipo de mensajes
	''' </summary>
	''' <remarks></remarks>
	Public Enum Tipo
		GetExito = 1
		GetError = 2
		GetInformativo = 3
	End Enum

	''' <summary>
	''' Permite agregar mensaje de confirmacion a un Button
	''' </summary>
	''' <param name="boton">control al que se le agregara el confirm</param>
	''' <param name="msg">mensaje a mostrar</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function AddConfirm(ByVal boton As Button, ByVal msg As String) As String
		Try
			boton.Attributes.Add("OnClick", "return confirm('" & msg & "')")
			Return ""
		Catch ex As Exception
			Return "ERROR: " & ex.Message
		End Try
	End Function

	''' <summary>
	''' Permite agregar un mensaje de confirmacion a un LinkButton
	''' </summary>
	''' <param name="Link">control al que se le agregara el confirm</param>
	''' <param name="msg">mensaje que se mostrara</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function AddConfirm(ByVal Link As LinkButton, ByVal msg As String) As String
		Try
			Link.Attributes.Add("OnClick", "return confirm('" & msg & "')")
			Return ""
		Catch ex As Exception
			Return "ERROR: " & ex.Message
		End Try
	End Function

	''' <summary>
	''' Permite agregar mensaje de confirmacion a un DropDownList
	''' </summary>
	''' <param name="Cmb">control al que se le agregara el confirm</param>
	''' <param name="msg">mensaje a mostrar</param>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Shared Function AddConfirm(ByVal Cmb As DropDownList, ByVal msg As String) As String
		Try
			Cmb.Attributes.Add("onchange", "return confirm('" & msg & "')")
			Return ""
		Catch ex As Exception
			Return "ERROR: " & ex.Message
		End Try
	End Function

	Enum TipoMensaje
		Informativo = 1
		[Error] = 2
		Exito = 3
	End Enum
End Class
