﻿Imports System.Security.Cryptography
Imports System.Text
Imports System.IO
Imports System.Web

''' <summary>
''' Clase para encriptar y desencriptar
''' </summary>
''' <remarks></remarks>
Public Class Encryption
	' Variables
	Private oDESCryptoServiceProvider As DESCryptoServiceProvider
	Private oMemoryStream As MemoryStream
	Private oCryptoStream As CryptoStream

	Private byteBase As Byte() = New Byte() {1, 2, 3, 4, 5, 6, 7, 8}
	Private byteKey As Byte()


	' Constantes
	Private Const GENERAL_KEY As String = "GCCB2B13" ' Deben ser 8 caracteres



	'''<summary>Desencripta una cadena de texto</summary>
	'''<param name="stringToDecrypt">Cadena a desencriptar</param>
	'''<param name="Dynamic">Opcional. Si se establece en verdadero la semilla de conversión cambia cada día, utilizado para pasar parámetros entre páginas</param>
	'''<returns>Cadena desencriptada</returns>
	Public Function decryptString(ByVal stringToDecrypt As String, Optional ByVal Dynamic As Boolean = False) As String
		Dim inputByteArray As Byte()
		Dim oEncoding As Encoding

		Dim sKey As String = ""
		Dim sResponse As String = ""

		Try

			' Instanciar objetos
			oDESCryptoServiceProvider = New DESCryptoServiceProvider
			oMemoryStream = New MemoryStream()

			' Parametros y Bytes
			If Dynamic = True Then
				sKey = Now.ToString("yyyy") & Now.ToString("MM") & Now.ToString("dd")
			Else
				sKey = GENERAL_KEY
			End If

			stringToDecrypt = HttpContext.Current.Server.UrlDecode(stringToDecrypt).Replace(" ", "+") ' Decodificar para URL
			byteKey = Encoding.UTF8.GetBytes(sKey.Substring(0, 8))
			inputByteArray = Convert.FromBase64String(stringToDecrypt)

			' Configurar objeto de desencriptación
			oCryptoStream = New CryptoStream(oMemoryStream, oDESCryptoServiceProvider.CreateDecryptor(byteKey, byteBase), CryptoStreamMode.Write)
			oCryptoStream.Write(inputByteArray, 0, inputByteArray.Length)
			oCryptoStream.FlushFinalBlock()

			' Desencriptar
			oEncoding = Encoding.UTF8
			sResponse = oEncoding.GetString(oMemoryStream.ToArray())

		Catch ex As Exception
			Err.Raise(Err.Number, Err.Source, Err.Description)
		End Try
		Return sResponse
	End Function

	'''<summary>Encripta una cadena de texto</summary>
	'''<param name="stringToEncrypt">Cadena a encriptar</param>
	'''<param name="Dynamic">Opcional. Si se establece en verdadero la semilla de conversión cambia cada día, utilizado para pasar parámetros entre páginas</param>
	'''<returns>Cadena encriptada</returns>
	Public Function encryptString(ByVal stringToEncrypt As String, Optional ByVal Dynamic As Boolean = False) As String
		Dim inputByteArray As Byte()
		Dim sResponse As String = ""

		Dim sKey As String = ""

		Try

			' Instanciar objetos
			oDESCryptoServiceProvider = New DESCryptoServiceProvider
			oMemoryStream = New MemoryStream()

			' Parametros y Bytes
			If Dynamic = True Then
				sKey = Now.ToString("yyyy") & Now.ToString("MM") & Now.ToString("dd")
			Else
				sKey = GENERAL_KEY
			End If

			byteKey = Encoding.UTF8.GetBytes(sKey.Substring(0, 8))
			inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt)

			' Configurar objeto de encriptación
			oCryptoStream = New CryptoStream(oMemoryStream, oDESCryptoServiceProvider.CreateEncryptor(byteKey, byteBase), CryptoStreamMode.Write)
			oCryptoStream.Write(inputByteArray, 0, inputByteArray.Length)
			oCryptoStream.FlushFinalBlock()

			' Encriptar
			sResponse = Convert.ToBase64String(oMemoryStream.ToArray())

			' Codificar para URL
			sResponse = HttpContext.Current.Server.UrlEncode(sResponse).Replace("+", " ")

		Catch ex As Exception
			Err.Raise(Err.Number, Err.Source, Err.Description)
		End Try
		Return sResponse
	End Function


End Class

