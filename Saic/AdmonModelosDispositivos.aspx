﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonModelosDispositivos.aspx.vb" Inherits="Saic.AdmonModelosDispositivos" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Modelos dispositivos" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <wum:wucMessage ID="wucMessageAdmonModelosDispositivos" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="wucMessageAdmonModelosDispositivos" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="75%" align="center">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <table runat="server" align="center">
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" Text="Modelo: " Width="62px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtModelo" runat="server" MaxLength="10" Width="75px" CssClass="auto-style1"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtModelo" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" Text="Ruta: "></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtruta" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtruta" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                                </asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center">
                                                <span><asp:Button runat="server" Text="Agregar" ID="btnGuardar" CssClass="btn" Visible="true" /></span>
                                                <span><asp:Button runat="server" Text="Actualizar" ID="btnModificar" CausesValidation="false" CssClass="btn" Visible="false" /></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnGuardar" />
                                    <asp:PostBackTrigger ControlID="btnModificar" />
                                    <asp:PostBackTrigger ControlID="gvdispositivo" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:GridView ID="gvdispositivo" runat="server" AutoGenerateColumns="false" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField DataField="Modelo" HeaderText="Modelo" HeaderStyle-Width="120px" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField DataField="Ruta" HeaderText="Ruta" HeaderStyle-Width="250px" ItemStyle-Width="250px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField DataField="ModeloRuta" HeaderText="Modelo Ruta" HeaderStyle-Width="250px" ItemStyle-Width="250px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                                        <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkmodificar" CausesValidation="false" CommandName="Modificar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Modificar</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEliminar" CausesValidation="false" CommandName="Eliminar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Eliminar</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
</asp:Content>
