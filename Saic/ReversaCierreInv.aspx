﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="ReversaCierreInv.aspx.vb" Inherits="Saic.ReversaCierreInv" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%" >
        <tr>
            <td class="titulos">
                Reversa cierre
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageReversaCierreInv" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="30%" align="center">
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Tienda: "></asp:Label>
                    </td>
                    <td>          
                        <asp:TextBox ID="txtTienda" runat="server" MaxLength="4" Width="60px" ></asp:TextBox> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Password: "></asp:Label>
                    </td>
                    <td><br />           
                        <asp:TextBox ID="txtPassword" runat="server" MaxLength="10" Width="100px" TextMode="Password"></asp:TextBox> 
                    </td>
                </tr>
                <tr align="center">
                    <td align="center">
                        <span><asp:Button runat="server" Text="Ejecutar" ID="btnEjecutar" Enabled="false" CssClass="btn" /></span>
                    </td>
                    <td align="center">
                        <span><asp:Button runat="server" Text="Cancelar" ID="btnCancelar" CssClass="btn" /></span>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>

     <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>  
    
    <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="HddnAcceso" Value="0" />
</asp:Content>
