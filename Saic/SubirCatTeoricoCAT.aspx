﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SubirCatTeoricoCAT.aspx.vb" Inherits="Saic.SubirCatTeoricoCAT" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Subir catálogo para teórico"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageFicheroCatalogoTeorico" runat="server" /><br />
            </td>
        </tr>
        <tr> 
            <td>
                <table runat="server" width="85%" align="center"> 
                    <tr>
                        <td align="center">
                            <span><asp:Button runat="server" Text="Descargar" ID="btnDescargar" CssClass="btn" /></span> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="upl" runat="server">
                                <ContentTemplate>
                                    <table runat="server" cellpadding="12" width="100%">
                                        <tr>
                                            <td align="center">
                                                <asp:FileUpload ID="singleUpload" runat="server" CssClass="upload" />
                                            </td>
                                            <td align="center"> 
                                                <span><asp:Button id="btnCargar" runat="server" Visible="False" Text="Subir Catalogo" CssClass="btn"/></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnCargar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3"><br /><br />
                            <asp:Label ID="lblTiempoProceso" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="3"><br /><br />
                            <asp:Label ID="lblErroresPresentados" runat="server" Text="Errores presentados:" Font-Bold="True" Visible="False"></asp:Label>
                            <asp:Label ID="lblCantidad" runat="server" Text="Documento inventario: " Font-Bold="True" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Panel runat="server" ID="PnlErrores" Height="250px" Width="515px" ScrollBars="Auto" Visible="false">
                                <asp:DataGrid ID="dgErrores" runat="server" SkinID="DGrid3"  AutoGenerateColumns="False" HorizontalAlign="Center" Visible="false">
                                    <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
									    <asp:BoundColumn DataField="CodigoMaterial" HeaderText="Codigo Material" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px"></asp:BoundColumn>
									    <asp:BoundColumn DataField="Mensaje" HeaderText="Mensaje" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="400px" ItemStyle-Width="400px">
									    </asp:BoundColumn>
								    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="PnlDocInventario" Height="250px" Width="220px" ScrollBars="Auto" Visible="false">
                                <asp:DataGrid ID="DGDocumentoInventario" runat="server" SkinID="DGrid3" AutoGenerateColumns="False" HorizontalAlign="Center" Visible="false">
                                    <HeaderStyle Font-Size="Medium" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <Columns>
									    <asp:BoundColumn DataField="DocumentoInventario" HeaderText="Documento Inventario" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundColumn>
								    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
