﻿Public Class _Default
    Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		'	Mantener la contraseña
		Session.Timeout = 50
		TxtContrasena.Attributes.Add("value", TxtContrasena.Text)
		If Not IsPostBack Then
			LlenarCombos()
		End If
	End Sub

	Protected Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
		consultaLogin()
	End Sub

	''' <summary>
	''' Obtiene IdUsuario, IdTienda, TipoUsuario, UnidadNegocio SID y Perfil
	''' </summary>
	Sub consultaLogin()
		Try
			Dim Cusuarios As New CUsuarios() With {.IdUsuario = txtUsuario.Text.ToUpper,
													   .Password = TxtContrasena.Text}
			If Cusuarios.loginUsuario Then
				Throw New Exception(Cusuarios.ErrorMensaje)
			End If

			If Cusuarios.dsRespuesta.Tables(0).Rows(0).Item("autorizacion") = "AUTORIZADO" Then
				Session("IdUsuario") = Cusuarios.dsRespuesta.Tables(1).Rows(0)("IdUsuario").ToString.ToUpper
				Session("NombreUsuario") = Cusuarios.dsRespuesta.Tables(1).Rows(0)("NombreUsuario")
				Session("IdTienda") = IIf(Cusuarios.dsRespuesta.Tables(1).Rows(0)("IdTienda").ToString = "0", Me.cmbTienda.SelectedValue.ToString, Cusuarios.dsRespuesta.Tables(1).Rows(0)("IdTienda").ToString)
				Session("TransferirInformacionPreconteoExternoAlSaic") = Cusuarios.dsRespuesta.Tables(1).Rows(0)("TransferirInformacionPreconteoExternoAlSAIC")
				Session("SubirArchivoAccessControl") = Cusuarios.dsRespuesta.Tables(1).Rows(0)("SubirArchivoAccessControl")
				Session("PermisoReversa") = Cusuarios.dsRespuesta.Tables(1).Rows(0)("PermisoReversa")
				Session("TipoUsuario") = Cusuarios.dsRespuesta.Tables(1).Rows(0)("TipoUsuario")
				Session("UnidadNegocio") = Me.cmbUnidadNegocio.SelectedValue.ToString
				Session("SID") = Cusuarios.dsRespuesta.Tables(0).Rows(0).Item("SID")
				'Opción 1 - Preconteo
				'Opción 2 - Rotativo
				If Me.rbtnperfilInv.SelectedValue.ToString = "1" Then
					Session("Perfil") = "P"
				ElseIf Me.rbtnperfilInv.SelectedValue.ToString = "2" Then
					Session("Perfil") = "R"
				End If

				'Agregar validación de perfil a la pagina de inicio
				If Session("TipoUsuario") = "A" Then
					If Me.cmbTienda.SelectedValue.ToString = "--Tienda--" Then
						Mostrar_Mensaje("Ingrese tienda.")
						Exit Sub
					ElseIf Me.cmbUnidadNegocio.SelectedValue.ToString = "--Unidad de Negocio--" Then
						Mostrar_Mensaje("Ingrese unidad de negocio.")
						Exit Sub
					Else
						Response.Redirect("Inicio.aspx?SID=" & Session("SID"), False)
					End If
				Else
					Response.Redirect("Inicio.aspx?SID=" & Session("SID"), False)
				End If
			Else
				Mostrar_Mensaje(Cusuarios.dsRespuesta.Tables(0).Rows(0).Item("autorizacion"))
			End If
		Catch ex As Exception
			Mostrar_Mensaje(ex.Message)
		End Try
	End Sub

	''' <summary>
	''' Muestra mensaje de error
	''' </summary>
	''' <param name="mensaje"></param>
	Sub Mostrar_Mensaje(ByVal mensaje As String)
		mensaje = "alert('" & mensaje & "');"
		ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, Guid.NewGuid.ToString, mensaje, True)
	End Sub

	''' <summary>
	''' Llena lista desplegable de Unidad de Negocio
	''' </summary>
	Sub LlenarCombos()
		Try
			Dim CNegocioTienda As New CNegocioTienda() With {.Accion = CNegocioTienda.TipoAccion.GetUnidadNegocio}

			If CNegocioTienda.LlenaUnidadNegocio Then
				Throw New Exception(CNegocioTienda.ErrorMensaje)
			Else
				If CNegocioTienda.dsRespuesta.Tables.Count > 0 AndAlso CNegocioTienda.dsRespuesta.Tables(0).Rows.Count > 0 Then
					Me.cmbUnidadNegocio.Items.Add("--Unidad de Negocio--")
					Me.cmbUnidadNegocio.DataSource = CNegocioTienda.dsRespuesta.Tables(0)
					Me.cmbUnidadNegocio.DataTextField = "UnidadNegocio"
					Me.cmbUnidadNegocio.DataValueField = "UnidadNegocio"
					Me.cmbUnidadNegocio.DataBind()
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub cmbUnidadNegocio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbUnidadNegocio.SelectedIndexChanged
		Try
			If Me.cmbUnidadNegocio.SelectedValue = "--Unidad de Negocio--" Then
				Mostrar_Mensaje("Debe seleccionar una unidad de negocio.")
			Else
				If Me.cmbUnidadNegocio.SelectedValue = "Cedis" Then
					Me.rbtnperfilInv.Items(0).Enabled = False
				Else
					Me.rbtnperfilInv.Items(0).Enabled = True
				End If

				Dim CNegocioTienda As New CNegocioTienda() With {.Accion = CNegocioTienda.TipoAccion.GetTienda,
																 .UnidadNegocio = Me.cmbUnidadNegocio.SelectedValue.ToString}

				If CNegocioTienda.LlenaUnidadNegocio Then
					Throw New Exception(CNegocioTienda.ErrorMensaje)
				Else
					If CNegocioTienda.dsRespuesta.Tables.Count > 0 AndAlso CNegocioTienda.dsRespuesta.Tables(0).Rows.Count > 0 Then
						Me.cmbTienda.Items.Clear()
						Me.cmbTienda.Items.Add("--Tienda--")
						Me.cmbTienda.DataSource = CNegocioTienda.dsRespuesta.Tables(0)
						Me.cmbTienda.DataTextField = "IdTienda"
						Me.cmbTienda.DataValueField = "IdTienda"
						Me.cmbTienda.DataBind()
						Me.cmbTienda.Enabled = True
					End If
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class