﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="LocalizacionEAN.aspx.vb" Inherits="Saic.LocalizacionEAN" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Consulta de localización por EAN" ID="lblTitulo" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Administración de marbetes" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageLocalizacionEAN" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="100%" cellpadding="10">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="EAN: " ID="lblEAN_Material" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtBuscar" MaxLength="13" Width="100px" EnableViewState="false"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Almacén: " ID="lblAlmacen" ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboAlmacen" runat="server" ToolTip="Seleccionar tienda si es un administrador"  Width="140px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                        <td align="center"> 
                            <span><asp:Button runat="server" Text="Buscar" ID="btnBuscar" CssClass="btn" CausesValidation="false"/></span>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Descripción: " ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtdescripcion" Width="400px" EnableViewState="false" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" align="center"><br />
                            <table runat="server">
                                <tr>
                                    <td>
                                        <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                            <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" SkinID="Grid3">
                                                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                                <Columns>
                                                    <asp:BoundField HeaderText="Marbete" DataField="IdMarbete" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px" />
                                                    <asp:BoundField HeaderText="Cantidad" DataField="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="95px" ItemStyle-Width="95px" />
                                                    <asp:BoundField HeaderText="NombreArea" DataField="NombreArea" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px" />
                                                    <asp:BoundField HeaderText="IdArea" DataField="IdArea" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="95px" ItemStyle-Width="95px" />
                                                    <asp:BoundField HeaderText="Ubicaci&#243;n F&#237;sica" DataField="UbicacionFisica" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
							                    </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
