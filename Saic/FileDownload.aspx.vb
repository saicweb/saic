﻿Imports System.IO

Public Class FileDownload
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Request.QueryString("file").ToString.Trim = String.Empty Then
			ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), Guid.NewGuid.ToString, "alert('Descarga de archivo no especificado.');", True)
			ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), Guid.NewGuid.ToString, "history.back();", True)
		Else
			DownloadFile(Request.QueryString("file"))
		End If
	End Sub

	Sub DownloadFile(archivo As String)
		Dim encripta As New Encryption
		Dim filepath As String = encripta.decryptString(archivo, False)
		Dim filename As String = Path.GetFileName(filepath)


		If File.Exists(filepath) = False Then
			ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), Guid.NewGuid.ToString, "alert('El archivo no existe.');", True)
			ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType(), Guid.NewGuid.ToString, "history.back();", True)
			'Response.Redirect("accesorestringido.aspx")
		Else
			Response.Clear()
			Response.ContentType = "application/octet-stream"
			Response.AddHeader("Content-Disposition",
			  "attachment; filename=""" & filename & """")
			Response.Flush()
			Response.TransmitFile(filepath)
			Response.End()
		End If

	End Sub
End Class