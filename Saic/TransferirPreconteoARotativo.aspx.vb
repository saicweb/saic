﻿Imports System.Data.SqlClient

Public Class TransferirPreconteoARotativo
    Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As New Button
    Protected WithEvents btnConfirmNO As New Button
    Dim CTipo As String = ""
    Dim mensaje As String = ""
    Dim xmlAlmacen As String = ""

    Dim dsgvGrid As New DataSet
    Dim dtAlmacen As New DataTable
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing OrElse Session("Perfil") = "P" Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessage.Visible = False

		If Page.IsPostBack = False Then
			Me.hdnTienda.Value = Session("IdTienda")
			Deptos_Buscar()
		End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType, "script", "CargaListas();", True)

	End Sub


    Sub Deptos_Buscar()
        Try
            dsgvGrid = New DataSet
            'Dim ds As New DataSet
            Dim par(0) As SqlParameter
            'par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
            par(0) = New SqlParameter("@IdTienda", Me.hdnTienda.Value)

            dsgvGrid = DatosSQL.funcioncp("AlmacenTPR_Buscar", par)
			dtAlmacen = dsgvGrid.Tables(2)
			Me.gvGrid.DataSource = dsgvGrid.Tables(0)
			Me.gvGrid.DataBind()

			If dtAlmacen.Rows.Count > 0 Then
				Me.btnTransferir.Visible = True
			Else
				Me.btnTransferir.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
    End Sub

	Protected Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkHeader As CheckBox = CType(Me.gvGrid.Controls(0).Controls(0).FindControl("CheckBox1"), CheckBox)
        'If chkHeader.Checked = True Then
        For x As Integer = 0 To Me.gvGrid.Rows.Count - 1
            Dim chkRow As New CheckBox
            chkRow = CType(Me.gvGrid.Rows(x).FindControl("CheckBox2"), CheckBox)
            chkRow.Checked = chkHeader.Checked
        Next
        'End If
    End Sub



#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

    Sub cargaWinConfirm()
        winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
        Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
        txtTitutlo.Text = "Confirmar"

        btnConfirmNO = winConfirm1.FindControl("BtnNO")
        btnConfirmNO.Text = "No"
        AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

        btnConfirmSI = winConfirm1.FindControl("btnSI")
        btnConfirmSI.Text = "Sí"
        AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

        phConfirm.Controls.Add(winConfirm1)
    End Sub
#End Region

    Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			wucMessage.Visible = False
			Dim dt As New DataTable
			dt.Columns.Add("idTienda")
			dt.Columns.Add("Depto")
			dt.Columns.Add("Almacen")

			dt = Session("dtGuardar")
			If dt Is Nothing AndAlso dt.Rows.Count = 0 Then
				For i = 0 To Me.gvGrid.Rows.Count - 1
					Dim list As New ListBox
					list = gvGrid.Rows(i).FindControl("lstDepartamentos")
					For Each item As ListItem In list.Items
						If item.Selected = True Then
							dt.Rows.Add(Me.hdnTienda.Value, item.Text.Split("-")(0), Me.gvGrid.DataKeys(i)("Almacen").ToString)
						End If
					Next
				Next
			End If

			For i = 0 To dt.Rows.Count - 1
				Guardar(dt.Rows(i).Item("Almacen").ToString, dt.Rows(i).Item("Depto"))
			Next
			Deptos_Buscar()
			CMensajes.MostrarMensaje("Preconteo transferido a Rotativo correctamente!", CMensajes.Tipo.GetExito, wucMessage)
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Sub
    Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.winConfirm1.OcultaDialogo()
        Response.Redirect("TransferirPreconteoARotativo.aspx?SID=" & Session("SID"), False)
    End Sub


	'Protected Sub btnTransferir_Click(sender As Object, e As EventArgs) Handles btnTransferir.Click
	'    CTipo = "Validar"
	'    'Dim xml As String = ""

	'    For i = 0 To gvGrid.Rows.Count - 1
	'        If (gvGrid.Rows(i).Cells("CheckBox1").ToString().Equals("true")) Then
	'        End If
	'    Next

	'    Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
	'    If CTipo = "Guardar" Then
	'        GetXML(xmlAlmacen)
	'    ElseIf CTipo = "Validar" Then
	'        Dim mensaje As String = ObtenerMensaje()
	'        Response.Clear()
	'        Response.ContentType = "text/xml;"
	'        'Response.Write(mensaje)
	'        lblTexto.Text = mensaje
	'        winConfirm1.MuestraDialogo()
	'        'Response.Flush()
	'        'Response.End()
	'    End If

	'End Sub

	'Public Shared Function GetJSON(ByVal dt As DataTable) As Object
	'    Dim JItems As New JArray()
	'    For Each row As DataRow In dt.Rows
	'        Dim JItem As New JObject()
	'        For Each col As DataColumn In dt.Columns
	'            JItem.Add(New JProperty(col.ColumnName, row(col.ColumnName)))
	'        Next
	'        JItems.Add(JItem)
	'    Next
	'    Return JItems
	'End Function

	'Public Shared Function GetDataTableJSON(ByVal dt As DataTable) As String
	'    Dim oResponse As New JObject()
	'    Dim objs As New JArray()
	'    Try

	'        oResponse.Add(New JProperty("Error", "0"))
	'        oResponse.Add(New JProperty("Almacen", GetJSON(dt)))
	'    Catch ex As Exception
	'        oResponse.Add(New JProperty("Error", "1"))
	'        oResponse.Add(New JProperty("Description", "Error.-" & ex.Message))
	'    End Try
	'    Return oResponse.ToString()
	'End Function


	'Private Function ListBox_To_DataTable(ByVal listbox As ListBox) As DataTable
	'    Dim dt As DataTable = New DataTable

	'    For index As Integer = 0 To listbox.Items.Count - 1
	'        Dim row As DataRow = dt.NewRow
	'        Dim columns() As String = listbox.Items(index).ToString.Split({""}, StringSplitOptions.RemoveEmptyEntries)

	'        For column As Integer = 0 To columns.Count - 1
	'            If column > dt.Columns.Count - 1 Then
	'                dt.Columns.Add(New DataColumn)
	'            End If

	'            row(column) = columns(column)
	'        Next

	'        dt.Rows.Add(row)
	'    Next

	'    Return dt
	'End Function

	'Sub GetXML(ByVal Xmlin As String)
	'    Try

	'        'ListBox_To_DataTable(lstDepartamentos)

	'        'Page.Response.ContentType = "text/xml"

	'        'Dim textReader = New IO.StreamReader(Request.InputStream)
	'        'Request.InputStream.Seek(0, IO.SeekOrigin.Begin)
	'        'textReader.DiscardBufferedData()

	'        'Dim Xmlin = XDocument.Load(textReader)
	'        'Dim Xmlin As New XDocument
	'        'Dim xmlDoc As New XmlDocument
	'        'Dim productNodes As XmlNodeList
	'        'Dim productNode As XmlNode
	'        'Dim baseDataNodes As XmlNodeList
	'        'Dim cant As Integer = 0

	'        'Dim Almacen, Depto As String

	'        'xmlDoc.LoadXml(Xmlin.ToString)

	'        'productNodes = xmlDoc.GetElementsByTagName("Almacen")
	'        'For Each productNode In productNodes
	'        '    baseDataNodes = productNode.ChildNodes

	'        '    For Each baseDataNode As XmlNode In baseDataNodes
	'        '        If baseDataNode.Name = "name" Then
	'        '            Almacen = baseDataNode.InnerText
	'        '        ElseIf baseDataNode.Name = "Depto" Then
	'        '            Depto = baseDataNode.InnerText
	'        '        End If
	'        '        'prueba = baseDataNode.Name & ": " & baseDataNode.InnerText
	'        '    Next
	'        'Guardar(xmlAlmacen)
	'        'cant += 1
	'        'Next

	'        'If cant = 0 Then
	'        '    Session("VarXml") = "Transferencia no iniciada; No ha seleccionado ningún departamento."
	'        '    Exit Sub
	'        'End If

	'        'Session("VarXml") = "Preconteo transferido a Rotativo correctamente!"

	'    Catch ex As Exception
	'        Session("VarXml") = "ERROR: " & ex.Message
	'    End Try
	'End Sub

	''' <summary>
	''' Permite  mandar los parametros para ejecutar en la BD
	''' </summary>
	''' <param name="Almacen"></param>
	''' <param name="depto"></param>
	''' <remarks></remarks>
	Sub Guardar(ByVal Almacen As String, ByVal depto As String)

		Try
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Me.hdnTienda.Value)
			par(1) = New SqlParameter("@Depto", depto)
			par(2) = New SqlParameter("@Almacen", Almacen)
			If Session("Sociedad").ToString = "3" Then
				DatosSQL.procedimientocp("PreconteosAConteos_Transferir_RST", par)
			Else
				DatosSQL.procedimientocp("PreconteosAConteos_Transferir", par)
			End If
			Session("VarXml") = "Preconteo transferido a Rotativo correctamente!"
			'Me.btnGuardar.Enabled = True
			'Deptos_Buscar()
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Sub

    Private Function Conteos_Existe() As Boolean
        Try
            Dim par(1) As SqlParameter
            par(0) = New SqlParameter("@Tipo", 2)
            par(1) = New SqlParameter("@IdTienda", Me.hdnTienda.Value)
            Dim ds As New DataSet
			'ds = DatosSQL.funcioncp("Existe_AdministracionRespaldo", par)
			ds = DatosSQL.funcioncp("Existe_Administracion", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item("Existe") = "S" Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
    End Function


    Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim chbx As CheckBox = DirectCast(sender, CheckBox)

        For i = 0 To Me.gvGrid.Rows.Count - 1
            Dim list As New ListBox
            Dim checkBox As New CheckBox
            checkBox = gvGrid.Rows(i).FindControl("chbxSeleccionar")
            list = gvGrid.Rows(i).FindControl("lstDepartamentos")
            For Each item As ListItem In list.Items
                If chbx.Checked = True Then
                    item.Selected = True
                    checkBox.Checked = True
                Else
                    item.Selected = False
                    checkBox.Checked = False
                End If
            Next
        Next
    End Sub

    Sub chbxSeleccionar_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim chbx As CheckBox = DirectCast(sender, CheckBox)
        Dim row As GridViewRow = TryCast(chbx.NamingContainer, GridViewRow)
        If row IsNot Nothing Then
            Dim list As New ListBox
            list = gvGrid.Rows(row.RowIndex).FindControl("lstDepartamentos")
            For Each item As ListItem In list.Items
                If chbx.Checked = True Then
                    item.Selected = True
                Else
                    item.Selected = False
                End If
            Next
        End If
    End Sub


	'  Function ObtenerMensaje() As String
	'Try
	'	If Conteos_Existe() = True Then
	'		Return "Ya existe información en Rotativo, ¿Desea continuar?"
	'	Else
	'		Return "¿Desea transferir la información del PRECONTEO" & Chr(13) &
	'			" al inventario ROTATIVO?"
	'	End If
	'Catch ex As Exception
	'	Throw New Exception(ex.Message)
	'      End Try
	'  End Function

	'Sub cargarDepartamentos(ByVal LstBx As ListBox)
	'       Try
	'           Dim ds As New DataSet
	'           Dim PAR(0) As SqlParameter
	'           PAR(0) = New SqlParameter("@IdTienda", Me.hdnTienda.Value)
	'           ds = DatosSQL.funcioncp("AlmacenTPR_Buscar", PAR)
	'           LstBx.DataSource = ds.Tables(2)
	'           LstBx.DataTextField = "Depto"
	'           LstBx.DataValueField = "Almacen"
	'           LstBx.DataBind()
	'       Catch ex As Exception
	'           Throw New Exception(ex.Message)
	'       End Try
	'   End Sub

	Private Sub gvGrid_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvGrid.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lstView As New ListBox
            lstView = e.Row.FindControl("lstDepartamentos")


            Dim idAlmacen As String = Me.gvGrid.DataKeys(e.Row.RowIndex)("Almacen").ToString
            Dim dt As New DataTable
			Dim rows() = dtAlmacen.Select(String.Format("Almacen = '{0}'", idAlmacen))

			If rows.Length > 0 Then
				dt = rows.CopyToDataTable
				'Dim ds As New DataSet
				'Dim PAR(0) As SqlParameter
				'PAR(0) = New SqlParameter("@IdTienda", "2172")
				'ds = DatosSQL.funcioncp("AlmacenTPR_Buscar", PAR)
				lstView.DataSource = dt
				lstView.DataTextField = "Depto"
				lstView.DataValueField = "Depto"
				lstView.DataBind()
			End If
		End If
    End Sub

	Private Sub btnTransferir_Click(sender As Object, e As EventArgs) Handles btnTransferir.Click
		wucMessage.Visible = False
		Dim dt As New DataTable
		dt.Columns.Add("idTienda")
		dt.Columns.Add("Depto")
		dt.Columns.Add("Almacen")
		For i = 0 To Me.gvGrid.Rows.Count - 1
			Dim list As New ListBox
			list = gvGrid.Rows(i).FindControl("lstDepartamentos")
			For Each item As ListItem In list.Items
				If item.Selected = True Then
					dt.Rows.Add(Me.hdnTienda.Value, item.Text.Split("-")(0), Me.gvGrid.DataKeys(i)("Almacen").ToString)
				End If
			Next
		Next
		Session("dtGuardar") = dt
		If dt.Rows.Count = 0 Then
			CMensajes.MostrarMensaje("Transferencia no iniciada; No ha seleccionado ningún departamento.", CMensajes.Tipo.GetInformativo, wucMessage)
		ElseIf Me.hdnTienda.Value = "" Then
			CMensajes.MostrarMensaje("Su sesion ha caducado, favor ingresar de nuevo al sistema.", CMensajes.Tipo.GetInformativo, wucMessage)
		Else
			Try
				If Conteos_Existe() = True Then
					Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
					lblTexto.Text = "<div align=""center"">Ya existe información en Rotativo, ¿Desea continuar.</div>"
					winConfirm1.MuestraDialogo()
				Else
					Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
					lblTexto.Text = "<div align=""center"">¿Desea transferir la información del PRECONTEO al inventario ROTATIVO?.</div>"
					winConfirm1.MuestraDialogo()
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try
		End If
	End Sub
End Class