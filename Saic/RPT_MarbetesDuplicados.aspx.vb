﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Public Class RPT_MarbetesDuplicados
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageRPT_MarbetesDuplicados.Visible = False

		If Not Page.IsPostBack Then
			Session.Remove("RPT_MarbetesDuplicados")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		CatalogoReportes_CRV13()
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Marbetes_Duplicados.DataBinding
		If Session("RPT_MarbetesDuplicados") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Marbetes_Duplicados.ReportSource = rpt
				Session("RPT_MarbetesDuplicados") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_Marbetes_Duplicados.ReportSource = Session("RPT_MarbetesDuplicados")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13()
		ds = RPT_MarbetesDuplicados()

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			Dim nomReporte As String = IIf(Session("Sociedad").ToString = "3", NombreReportes.RPT_MarbetesDuplicados_RST, NombreReportes.RPT_MarbetesDuplicados)
			rpt.Load(Server.MapPath(RutaCR & nomReporte))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "REPORTE DE MARBETES DUPLICADOS"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Marbetes_Duplicados.ReportSource = rpt
			RPT_Marbetes_Duplicados.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_MarbetesDuplicados)
		End If

	End Sub

	Public Function RPT_MarbetesDuplicados() As DataSet
		Try
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))

			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_MarbetesDuplicados_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_MarbetesDuplicados", par)
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class