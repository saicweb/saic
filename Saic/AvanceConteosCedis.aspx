﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AvanceConteosCedis.aspx.vb" Inherits="Saic.AvanceConteosCedis" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajaxToolkit" %>
<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
     <link rel="stylesheet" href="Scripts/jquery-ui.css">
    <script language="javascript" type="text/javascript" src="Scripts/jquery-1.10.2.js"></script>
    <script language="javascript"  type="text/javascript" src="Scripts/jquery-ui.js"></script>
    
    <script type="text/javascript">
        function pro() {
            $(".progress").each(function () {
                $(this).progressbar({
                    value: parseInt($(this).find('.progress-label').text())
                });
            });
        }
    </script>
    <style type="text/css">
        .ui-progressbar
        {
            position: relative;
        }
        .progress-label
        {   position:absolute;
            left: 50%;
            top: 4px;
            font-weight: bold;
        }  

    </style>
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Avance de conteos CEDIS" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageAvanceConteos" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="350px" id="TblFiltro">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Visualizar avance: " ID="Label1"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" id="cmbAvance" Width="180px" CssClass="dropdown inline">
                                <asp:ListItem Selected="True" Value="-1" Text="[Seleccione]"></asp:ListItem>
                                <asp:ListItem Value="1" Text="0010 Rack Nacional"></asp:ListItem>
                                <asp:ListItem Value="2" Text="Cross Dock"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><br />
                            <span><asp:Button runat="server" Text="Ejecutar" ID="btnEjecutar" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <table runat="server" id="TblGrafica" visible="false">
                    <tr>
                        <td>
                            <table runat="server" width="150px">
                                <tr>
                                    <td align="center">
                                        <asp:Label runat="server" Text="Marbetes Faltantes" ID="Label2" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table runat="server" width="200px" style="border:solid; border-style:groove; border-color:lightslategray; background-color:lightgray;">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" Text="Total marbetes:" ID="Label3" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" Text="" ID="lblTotalMarbetes" Font-Size="Medium"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br />
                                                    <asp:Label runat="server" Text="Marbetes faltantes:" ID="Label4" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" Text="" ID="lblMarbetesFaltantes" Font-Size="Medium"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br />
                                                    <asp:HyperLink runat="server" ID="lnkRptMarbetesFaltantes" Text="Descargar reporte"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table runat="server" width="365px">
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table runat="server" width="150px">
                                <tr>
                                    <td align="center">
                                        <asp:Label runat="server" Text="Inf. Marbete - Ubicación" ID="Label5" Font-Bold="true" Font-Size="Medium"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table runat="server" width="200px" style="border:solid; border-style:groove; border-color:lightslategray; background-color:lightgray;">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" Text="oo" ForeColor="lightgray" ID="Label8" Font-Size="Medium"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" Text="Marbetes con más de una ubicación:" ID="Label6" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <asp:Label runat="server" Text="" ID="lblMarbeteMasUbicacion" Font-Size="Medium"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label runat="server" Text="oo" ForeColor="lightgray" ID="Label7" Font-Size="Medium"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"><br />
                                                    <asp:HyperLink runat="server" ID="lnkRptMarbeteMasUbicacion" Text="Descargar reporte"></asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center"><br />
                            <asp:Label runat="server" Text="" ID="lblTituloGrafica" Visible="false" Font-Bold="true"></asp:Label>
                            <asp:GridView runat="server" ID="GrdRackNacional" AutoGenerateColumns="false" SkinID="Grid6" Visible="false" Width="700px">
                                <Columns>
                                    <asp:BoundField DataField="Pasillo" HeaderText="Pasillo - Nivel" HeaderStyle-Width="100px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:TemplateField ItemStyle-Width="120" HeaderText="A" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <div class='progress'>
                                                <div class="progress-label"><asp:LinkButton CommandName="A" CommandArgument=<%#Container.DataItemIndex%> runat="server" ForeColor="Black"><%# Eval("A")%></asp:LinkButton></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="120" HeaderText="B" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <div class='progress'>
                                                <div class="progress-label"><asp:LinkButton CommandName="B" CommandArgument=<%#Container.DataItemIndex%> runat="server" ForeColor="Black"><%# Eval("B")%></asp:LinkButton></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="120" HeaderText="C" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <div class='progress'>
                                                <div class="progress-label"><asp:LinkButton CommandName="C" CommandArgument=<%#Container.DataItemIndex%> runat="server" ForeColor="Black"><%# Eval("C")%></asp:LinkButton></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="120" HeaderText="D" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <div class='progress'>
                                                <div class="progress-label"><asp:LinkButton CommandName="D" CommandArgument=<%#Container.DataItemIndex%> runat="server" ForeColor="Black"><%# Eval("D")%></asp:LinkButton></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="120" HeaderText="E" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <div class='progress'>
                                                <div class="progress-label"><asp:LinkButton CommandName="E" CommandArgument=<%#Container.DataItemIndex%> runat="server" ForeColor="Black"><%# Eval("E")%></asp:LinkButton></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView runat="server" ID="GrdCrossDock" AutoGenerateColumns="false" Visible="false" SkinID="Grid6" Width="700px">
                                <Columns>
                                    <asp:BoundField DataField="Ubicacion" HeaderText="Almacén" HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    <asp:TemplateField ItemStyle-Width="180" ShowHeader="false" HeaderStyle-Width="180px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                        <ItemTemplate>
                                            <div class='progress'>
                                                <div class="progress-label"><asp:LinkButton CommandName="CrossDock" CommandArgument=<%#Container.DataItemIndex%> runat="server" ForeColor="Black"><%# Eval("Porcentaje")%></asp:LinkButton></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upPopUp" runat ="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlgridModal" runat="server" Width="450px" Height="410px"  CssClass="CajaDialogo" style="display:none">
                            <table align="center">
                                <tr valign="baseline">
                                    <td align="center" valign="baseline"  class="titulosPopUp" style="width:450px; padding-top:7px;">
                                        <strong><asp:label ID="lblTitleModal" runat="server" /></strong> 
                                    </td>    
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel Height="300px" runat="server" ScrollBars="Vertical" Width="450px">
                                            <asp:GridView runat="server" ID="GrdModal" SkinID="Grid6" AutoGenerateColumns="false" Width="430px" EmptyDataText="Marbetes en orden">
                                                <Columns>
                                                    <asp:BoundField DataField="IdMarbete" HeaderText="Marbete" HeaderStyle-Width="10px" HeaderStyle-VerticalAlign="Middle" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                    <asp:BoundField DataField="Ubicacion" HeaderText="Ubicación" HeaderStyle-Width="10px" HeaderStyle-VerticalAlign="Middle" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="trAdbtn" runat="server">
                                    <td align="center"><br />
                                        <asp:Button ID="btnCerrar" runat="server" Text="Cerrar" />
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td>
                                        <asp:UpdateProgress ID="uprPopUp" runat="server" AssociatedUpdatePanelID="upPopUp" DisplayAfter="0">
                                            <ProgressTemplate>
                                                <img src="Img/MiddgetLoadingGif.gif" alt="Cargando..." />
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>                                                    
                                    </td>
                                </tr>--%>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <ajaxToolkit:ModalPopupExtender ID="mppDetalle" runat="server" BackgroundCssClass="PopUpBackground" DropShadow="false"
                PopupControlID="pnlgridModal" TargetControlID="hddnTarget"></ajaxToolkit:ModalPopupExtender>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="hddnTarget" />
</asp:Content>
