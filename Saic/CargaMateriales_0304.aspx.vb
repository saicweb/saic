﻿Imports System.IO

Public Class CargaMateriales_0304
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Protected Sub btnCambiar_Click(sender As Object, e As EventArgs) Handles btnCambiar.Click
		If Me.RadUpload1.HasFile = False Then
			CMensajes.MostrarMensaje("Debe seleccionar el archivo a cargar.", CMensajes.Tipo.GetError, wucMessageCambiarMateriales_0304)
		Else
			Me.btnCambiar.Enabled = False
			Me.btnCambiar.Text = "Subiendo archivo..."

			Dim Extension As String = ""
			Dim Archivo As String = Me.RadUpload1.FileName
			Dim IdTienda As String = Session("IdTienda")

			'Crear el directorio si no existe
			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim Ruta As String = DirectorioArchivos
			Dim err1 As Integer = 0
			Dim err2 As Integer = 0

			'si selecciona la opcion de excel

			If Archivo.ToUpper.EndsWith(".XLS") = False Then
				CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .xlsx.", CMensajes.Tipo.GetError, wucMessageCambiarMateriales_0304)
			ElseIf Archivo.StartsWith("MATERIALES") = False Then
				CMensajes.MostrarMensaje("El nombre del archivo seleccionado no es correcto, favor de verificar.", CMensajes.Tipo.GetError, wucMessageCambiarMateriales_0304)
			Else
				Ruta = Ruta & Archivo
				Me.RadUpload1.SaveAs(Ruta)

				Try
					Dim cantidad As Integer
					cantidad = CantidadMat()
					If cantidad > 0 Then
						EliminarMat()
					End If

					Dim material, ean, st As String
					Dim dt As DataTable

					If ExcelClass.ValidarExcel(Ruta) Then
						dt = ExcelClass.LeerExcel(Ruta)

						For x As Integer = 0 To dt.Rows.Count - 1
							If Not IsDBNull(dt.Rows(x).Item(0)) Then
								material = CType(dt.Rows(x).Item(0), String)
								ean = CType(dt.Rows(x).Item(1), String)
								st = CType(dt.Rows(x).Item(2), String)

								'valida que la longitud sea correcta
								material = validar_archivo(material, 18)
								ean = validar_archivo(ean, 13)
								st = validar_archivo(st, 2)

								If material.Trim <> "MATERIAL" AndAlso ean.Trim <> "EAN" AndAlso st.Trim <> "ST" Then
									If material = "0" Or ean = "0" Or st = "0" Then
										EliminarMat()
										err1 += 1
										GoTo Err
									Else
										'InsertaMat(material.Trim, ean.Trim, st.Trim)
									End If
								End If
							End If
						Next
						BulkCopy.Insertar(dt, CReglasNegocio.Materiales)
Err:
						If err1 > 0 Then
							CMensajes.MostrarMensaje("Un registro excede la longitud permitida, revise el archivo.", CMensajes.Tipo.GetError, wucMessageCambiarMateriales_0304)
						Else
							CMensajes.MostrarMensaje("Los materiales fueron grabados correctamente.", CMensajes.Tipo.GetExito, wucMessageCambiarMateriales_0304)
						End If
					Else
						CMensajes.MostrarMensaje("No es un archivo válido.", CMensajes.Tipo.GetExito, wucMessageCambiarMateriales_0304)
					End If
				Catch ex As Exception
					Throw New Exception(ex.Message, ex.InnerException)
				End Try
			End If
			Me.btnCambiar.Enabled = True
			Me.btnCambiar.Text = "Subir catálogo"
		End If
	End Sub

	Function validar_archivo(ByRef valor As String, ByVal longitud As Integer) As String
		Try
			Dim cant As Integer
			cant = valor.Length
			If cant < longitud Then
				For i As Integer = cant To longitud - 1
					valor = "0" & valor
				Next
			ElseIf cant > longitud Then
				valor = "0"
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
		Return valor
	End Function

	''' <summary>
	''' Obtiene la cantidad de materiales
	''' </summary>
	''' <returns></returns>
	Function CantidadMat() As Integer
		Try
			Dim CMateriales As New CMateriales() With {.Accion = 1}

			If CMateriales.CantidadMateriales Then
				Throw New Exception(CMateriales.ErrorMensaje)
			Else
				If CMateriales.dsRespuesta.Tables.Count > 0 AndAlso CMateriales.dsRespuesta.Tables(0).Rows.Count > 0 Then
					Return Convert.ToInt32(CMateriales.dsRespuesta.Tables(0).Rows(0)(0))
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	''' <summary>
	''' Elimina el contenido de la tabla Materiales
	''' </summary>
	Sub EliminarMat()
		Try
			Dim CMateriales As New CMateriales() With {.Accion = 2}

			If CMateriales.EliminarMateriales Then
				Throw New Exception(CMateriales.ErrorMensaje)
			Else

			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Inserta el material
	''' </summary>
	Sub InsertaMat(ByVal Material As String, ByVal EAN As String, ByVal ST As String)
		Try
			Dim CMateriales As New CMateriales() With {.Accion = 3,
													   .Material = Material,
													   .EAN = EAN,
													   .ST = ST}

			If CMateriales.InsertaMateriales Then
				Throw New Exception(CMateriales.ErrorMensaje)
			Else

			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class