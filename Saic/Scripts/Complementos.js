﻿//Ismael Rmz

//Funcion para seleccionar todas las casillas de un grid con header
function CheckAll(Checkbox, variable, celda, estilo) {
        var Grid1 = document.getElementById(variable);
    

        for (i = 0; i < Grid1.rows.length; i++) {
            try {
                
                if (Grid1.rows[i].cells[celda].getElementsByTagName("input")[0].disabled == false) {
                    Grid1.rows[i].cells[celda].getElementsByTagName("input")[0].checked = Checkbox.checked;
                }
                
    
                //Permite cambiar el color
                var cantcell = Grid1.rows[i].cells.length
    
                    for (j = 0; j < cantcell; j++) {
                        if (Checkbox.checked == true) {
                            if (estilo == "S") {
                                Grid1.rows[i].cells[j].className = "SelectedRow_Office2007";
                            }
                            else {
                                Grid1.rows[i].cells[j].className = "";
                            }
                        }
                        else {
                            if (estilo == "S") {
                                Grid1.rows[i].cells[j].className = "SelectedRow_Office20072";
                            }
                            else {
                                Grid1.rows[i].cells[j].className = "";
                            }
                            
                        }
                     }
                }
        catch (err) {
            //console.log("ERROR: " + err.Message);
        }
}
}


//Permite aplicar los estilos a los check seleccionados
function CheckOne(variable, celda, estilo) {
    if (variable == "grid") {
        var Grid1 = document.getElementById("grid");
    }
    
    for (i = 1; i < Grid1.rows.length; i++) {
        try {
            var cantcell = Grid1.rows[i].cells.length

            if (Grid1.rows[i].cells[celda].getElementsByTagName("input")[0].checked == true) {
                for (j = 0; j < cantcell; j++) {
                    Grid1.rows[i].cells[j].className = "SelectedRow_Office2007";
                }
            }
            else {
                for (j = 0; j < cantcell; j++) {
                    if (estilo == "S") {
                        Grid1.rows[i].cells[j].className = "SelectedRow_Office20072";
                    }
                    else {
                        Grid1.rows[i].cells[j].className = "";
                    }
                    
                }
            }
        }
        catch (err) {
            //console.log("ERROR: " + err.Message);
        }
    }
}