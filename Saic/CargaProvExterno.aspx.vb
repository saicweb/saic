﻿Imports System.IO

Public Class CargaProvExterno
	Inherits System.Web.UI.Page

	Dim dt As New DataTable
	Dim Reg() As String
	Dim CantColumnas As Integer = 0
	Dim TipoCarga As TipoArchivo

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Dim culture As New System.Globalization.CultureInfo("es-PE")
		System.Threading.Thread.CurrentThread.CurrentCulture = culture
	End Sub

	Protected Sub btncargar_Click(sender As Object, e As EventArgs) Handles btncargar.Click
		Try
			Me.btncargar.Enabled = False
			Me.btncargar.Text = "Subiendo archivo..."

			If Validar() = False Then Exit Sub
			CantColumnas = GetCantColum()
			LeerArchivo()
			LimpiarRegistrosDT()
			Procesarinfo()

			Me.btncargar.Enabled = False
			Me.btncargar.Text = "Subir catálogo"
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub LeerArchivo()
		Try
			Dim Archivo As String = Me.RadUpload1.FileName
			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")
			Dim Hoja As String = Nothing

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			DirectorioArchivos = DirectorioArchivos & Archivo
			Me.RadUpload1.SaveAs(DirectorioArchivos)

			If Archivo.ToUpper.EndsWith(".XLS") = False Then
				CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .xlsx.", CMensajes.Tipo.GetError, wucMessageCargaProvExterno)
			Else
				If ExcelClass.ValidarExcel(DirectorioArchivos) Then
					Select Case TipoCarga
						Case TipoArchivo.Checklist
							Hoja = "Check List LCR"
							Dim rango As String = "A5:CV65536"

							dt = ExcelClass.LeerExcel(DirectorioArchivos, Hoja, rango, False)
							For h = 0 To 90
								dt.Columns(h).ColumnName = dt.Rows(0)(h)
							Next
							dt.Rows(0).Delete()
							For i As Integer = dt.Rows.Count - 1 To 0 Step -1
								Dim row As DataRow = dt.Rows(i)
								If String.IsNullOrEmpty(row(0)) OrElse String.IsNullOrWhiteSpace(row(0)) Then
									dt.Rows.Remove(row)
								End If
							Next
							For j As Integer = 92 To dt.Columns.Count
								dt.Columns.Remove("Column" & j)
							Next
							dt.AcceptChanges()

						Case TipoArchivo.Evaluacion
							Hoja = "Datos-Evaluacion"
							Dim rango As String = "A5:AV65536"

							dt = ExcelClass.LeerExcel(DirectorioArchivos, Hoja, rango, False)
							For h = 0 To 47
								If dt.Rows(0)(h) = "2.9" Then
									If dt.Rows(0)(h + 1) = "2.1" Then
										dt.Columns(h + 1).ColumnName = "2.10"
									End If
									dt.Columns(h).ColumnName = dt.Rows(0)(h)
									h += 1
								ElseIf dt.Rows(0)(h) = "Obs 2.9" Then
									If dt.Rows(0)(h + 1) = "Obs 2.1" Then
										dt.Columns(h + 1).ColumnName = "Obs 2.10"
									End If
									dt.Columns(h).ColumnName = dt.Rows(0)(h)
									h += 1
								Else
									dt.Columns(h).ColumnName = dt.Rows(0)(h)
								End If
							Next
							dt.Rows(0).Delete()
							For i As Integer = dt.Rows.Count - 1 To 0 Step -1
								Dim row As DataRow = dt.Rows(i)
								If String.IsNullOrEmpty(row(0)) OrElse String.IsNullOrWhiteSpace(row(0)) Then
									dt.Rows.Remove(row)
								End If
							Next
							For j As Integer = 49 To dt.Columns.Count
								dt.Columns.Remove("Column" & j)
							Next
							dt.AcceptChanges()

						Case TipoArchivo.Incidencias
							Hoja = "Incidencias"
							Dim rango As String = "A5:AE65536"

							dt = ExcelClass.LeerExcel(DirectorioArchivos, Hoja, rango, False)
							For h = 0 To 30
								dt.Columns(h).ColumnName = dt.Rows(0)(h)
							Next
							dt.Rows(0).Delete()
							For i As Integer = dt.Rows.Count - 1 To 0 Step -1
								Dim row As DataRow = dt.Rows(i)
								If String.IsNullOrEmpty(row(0)) OrElse String.IsNullOrWhiteSpace(row(0)) Then
									dt.Rows.Remove(row)
								End If
							Next
							dt.AcceptChanges()

						Case TipoArchivo.Inv_externo
							Hoja = "INV DS-WW-EXTERNO"
							Dim rango As String = "A5:BI65536"

							dt = ExcelClass.LeerExcel(DirectorioArchivos, Hoja, rango, False)

							Dim PV As Integer = 0
							Dim Bodega As Integer = 0
							Dim IPV As Integer = 0
							Dim IB As Integer = 0
							For h = 0 To 60
								Select Case dt.Rows(0)(h)
									Case "Piso de Venta"
										PV += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & PV

									Case "Bodega"
										Bodega += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & Bodega

									Case "% inventario PV"
										IPV += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & IPV

									Case "% inventario B"
										IB += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & IB

									Case Else
										dt.Columns(h).ColumnName = dt.Rows(0)(h)
								End Select
							Next
							dt.Rows(0).Delete()
							For i As Integer = dt.Rows.Count - 1 To 0 Step -1
								Dim row As DataRow = dt.Rows(i)
								If String.IsNullOrEmpty(row(0)) OrElse String.IsNullOrWhiteSpace(row(0)) Then
									dt.Rows.Remove(row)
								End If
							Next
							dt.AcceptChanges()

						Case TipoArchivo.Inv_oper
							Hoja = "INV DS-WW-TIENDA"
							Dim rango As String = "A5:AW65536"

							dt = ExcelClass.LeerExcel(DirectorioArchivos, Hoja, rango, False)

							Dim PV As Integer = 0
							Dim Bodega As Integer = 0
							Dim IPV As Integer = 0
							Dim IB As Integer = 0
							For h = 0 To 48
								Select Case dt.Rows(0)(h)
									Case "Piso de Venta"
										PV += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & PV

									Case "Bodega"
										Bodega += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & Bodega

									Case "% inventario PV"
										IPV += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & IPV

									Case "% inventario B"
										IB += 1
										dt.Columns(h).ColumnName = dt.Rows(0)(h) & IB

									Case Else
										dt.Columns(h).ColumnName = dt.Rows(0)(h)
								End Select
							Next
							dt.Rows(0).Delete()
							For i As Integer = dt.Rows.Count - 1 To 0 Step -1
								Dim row As DataRow = dt.Rows(i)
								If String.IsNullOrEmpty(row(0)) OrElse String.IsNullOrWhiteSpace(row(0)) Then
									dt.Rows.Remove(row)
								End If
							Next
							dt.AcceptChanges()
					End Select
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Procesarinfo()
		Try
			Dim Inicio As Boolean = False
			If dt.Columns.Count < CantColumnas Then
				CMensajes.MostrarMensaje("La cantidad de columnas no son correctas.", CMensajes.Tipo.GetExito, wucMessageCargaProvExterno)
			Else
				For Renglon As Integer = 0 To dt.Rows.Count - 1
					If Inicio = False Then
						If Not IsDBNull(dt.Rows(Renglon)(0)) Then
							Inicio = ValidarCabecero(dt.Columns(0).ColumnName) 'Cabecero
							If Inicio = False Then
								Inicio = ValidarCabecero(dt.Rows(Renglon)(0)) 'Row
								If Inicio Then Continue For
							End If
						End If
					End If
					If Inicio = False Then Continue For
					ReDim Reg(CantColumnas)
					For Columna As Integer = 0 To CantColumnas - 1
						'If IsDBNull(Ds.Tables(0).Rows(Renglon)(Columna)) OrElse Ds.Tables(0).Rows(Renglon)(Columna).ToString = "" Then
						'    Throw New Exception("El archivo no debe contener registros Vacíos")
						'End If
						If IsDBNull(dt.Rows(Renglon)(Columna)) Then
							Reg(Columna) = ""
						Else
							Reg(Columna) = dt.Rows(Renglon)(Columna)
						End If
					Next
					CargarInfo()
				Next
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function ValidarCabecero(Name As String) As Boolean
		Try
			Dim ValorInicio As String
			Select Case TipoCarga
				Case TipoArchivo.Checklist
					ValorInicio = "Folio-00000"
				Case TipoArchivo.Evaluacion
					ValorInicio = "Folio"
				Case TipoArchivo.Incidencias
					ValorInicio = "Tienda"
				Case TipoArchivo.Inv_externo
					ValorInicio = "Tienda"
				Case TipoArchivo.Inv_oper
					ValorInicio = "Tienda"
			End Select

			If Name = ValorInicio Then
				Return True
			End If

			Return False
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub CargarInfo()
		Try
			Dim Obj As Object
			Dim Cont As Integer = 0

			Select Case TipoCarga
				Case TipoArchivo.Checklist
					Obj = New CCheckList
				Case TipoArchivo.Evaluacion
					Obj = New CEvaluacion
				Case TipoArchivo.Incidencias
					Obj = New CIncidencias
				Case TipoArchivo.Inv_externo
					Obj = New CInv_externo
				Case TipoArchivo.Inv_oper
					Obj = New CInv_oper
			End Select

			For Each Row As System.Reflection.PropertyInfo In Obj.GetType().GetProperties()
				If Row.CanRead Then
					If Row.PropertyType.Name = "String" And Row.DeclaringType.Name = Obj.GetType.Name Then
						Row.SetValue(Obj, Reg(Cont), Nothing)
						Cont += 1
					End If
				End If
			Next

			Select Case TipoCarga
				Case TipoArchivo.Checklist
					Dim CHL As CCheckList
					CHL = Obj
					If CHL.INSERTAR() Then Throw New Exception(CHL.ErrorMensaje)
				Case TipoArchivo.Evaluacion
					Dim EVL As CEvaluacion
					EVL = Obj
					If EVL.INSERTAR() Then Throw New Exception(EVL.ErrorMensaje)
				Case TipoArchivo.Incidencias
					Dim INC As CIncidencias
					INC = Obj
					If INC.INSERTAR() Then Throw New Exception(INC.ErrorMensaje)
				Case TipoArchivo.Inv_externo
					Dim IEXT As CInv_externo
					IEXT = Obj
					If IEXT.INSERTAR() Then Throw New Exception(IEXT.ErrorMensaje)
				Case TipoArchivo.Inv_oper
					Dim IOPER As CInv_oper
					IOPER = Obj
					If IOPER.INSERTAR() Then Throw New Exception(IOPER.ErrorMensaje)
			End Select
			CMensajes.MostrarMensaje("Guardado correctamente.", CMensajes.Tipo.GetExito, wucMessageCargaProvExterno)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function GetCantColum() As Integer
		Try
			Dim Obj As Object
			Dim Cont As Integer = 0

			Select Case IO.Path.GetFileNameWithoutExtension(RadUpload1.FileName)
				Case "Checklist"
					Obj = New CCheckList
				Case "Evaluacion"
					Obj = New CEvaluacion
				Case "Incidencias"
					Obj = New CIncidencias
				Case "Inv_externo"
					Obj = New CInv_externo
				Case "Inv_oper"
					Obj = New CInv_oper
			End Select

			For Each Row As System.Reflection.PropertyInfo In Obj.GetType().GetProperties()
				If Row.CanRead Then
					If Row.PropertyType.Name = "String" And Row.DeclaringType.Name = Obj.GetType.Name Then
						Cont += 1
					End If
				End If
			Next
			Return Cont
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Function Validar() As Boolean
		Try
			If RadUpload1.HasFile = False Then
				CMensajes.MostrarMensaje("Debe seleccionar el archivo a cargar antes de continuar.", CMensajes.Tipo.GetError, wucMessageCargaProvExterno)
				Return False
			ElseIf RadUpload1.FileName.ToUpper.EndsWith(".XLS") = False Then
				CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .xls.", CMensajes.Tipo.GetError, wucMessageCargaProvExterno)
				Return False
			Else
				Select Case IO.Path.GetFileNameWithoutExtension(RadUpload1.FileName)
					Case "Checklist"
						TipoCarga = TipoArchivo.Checklist
						Return True
					Case "Evaluacion"
						TipoCarga = TipoArchivo.Evaluacion
						Return True
					Case "Incidencias"
						TipoCarga = TipoArchivo.Incidencias
						Return True
					Case "Inv_externo"
						TipoCarga = TipoArchivo.Inv_externo
						Return True
					Case "Inv_oper"
						TipoCarga = TipoArchivo.Inv_oper
						Return True
					Case Else
						TipoCarga = Nothing
						CMensajes.MostrarMensaje("El nombre del archivo seleccionado no es correcto, favor de verificar.", CMensajes.Tipo.GetError, wucMessageCargaProvExterno)
						Return False
				End Select
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub LimpiarRegistrosDT()
		Try
			Dim CantReg As Integer = 0
			Dim Val As String = ""
			For Row As Integer = 0 To dt.Rows.Count - 1
				If Row = 130 Then
					Dim a As String = ""
				End If
				For Col As Integer = 0 To dt.Columns.Count - 1
					If IsDBNull(dt.Rows(Row)(Col)) Then
						Val = ""
					ElseIf dt.Rows(Row)(Col) Is Nothing Then
						Val = ""
					Else
						Val = dt.Rows(Row)(Col).ToString
					End If
					If Val.Length > 0 Then
						CantReg += 1
						Exit For
					End If
				Next
				If CantReg = 0 Then
					dt.Rows(Row).Delete()
				End If
				CantReg = 0
			Next
			dt.AcceptChanges()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Public Enum TipoArchivo
		Checklist = 1
		Evaluacion = 2
		Incidencias = 3
		Inv_externo = 4
		Inv_oper = 5
	End Enum

End Class