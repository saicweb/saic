﻿Imports System.Data.SqlClient
Imports System.IO

Public Class TransferirConteo_DLX
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing Then
            Response.Redirect("Default.aspx")
        End If
        If Session("Cedis") Is Nothing OrElse Session("Cedis").ToString.Trim = "0" Then
            'CMensajes.Show("Ésta opción sólo está habilitada para CEDIS")
            Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
        End If
    End Sub

    Private Sub btncargar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncargar.Click
        '        Dim ArchivoSeleccionado As Boolean = Me.RadUpload1.UploadedFiles.Count > 0
        Dim ArchivoSeleccionado As Boolean = Me.RadUpload1.HasFile

        If ArchivoSeleccionado = False Then
            CMensajes.MostrarMensaje("Debe seleccionar el archivo a cargar antes de continuar.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
        Else
            Me.btncargar.Enabled = False
            Me.btncargar.Text = "Subiendo archivo..."

            Dim Extension As String = ""
            'Dim Archivo As String = Me.RadUpload1.UploadedFiles(0).GetName
            Dim Archivo As String = Me.RadUpload1.FileName
            Dim IdTienda As String = Session("IdTienda")

            'Crear el directorio si no existe
            Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("ArchivosDLX")

            If Not Directory.Exists(DirectorioArchivos) Then
                Directory.CreateDirectory(DirectorioArchivos)
            End If

            Dim Ruta As String = DirectorioArchivos

            If Me.rdformato.SelectedIndex = 0 Then
                If Archivo.EndsWith(".xls") = False Then
                    CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .xls.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                ElseIf Archivo.StartsWith(IdTienda) = False Then
                    CMensajes.MostrarMensaje("El archivo seleccionado no corresponde al de la tienda, favor de verificar.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                Else
                    Ruta = Ruta & "ArchivoDlx.xls"
                    File.Delete(Ruta)
                    If IO.File.Exists(Ruta) Then
                        CMensajes.MostrarMensaje("No se pudo reemplazar el archivo.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                    End If

                    Me.RadUpload1.SaveAs(Ruta)

                    Try
                        Dim dt As DataTable

                        If ExcelClass.ValidarExcel(Ruta) Then
                            dt = ExcelClass.LeerExcel(Ruta, False)
                            'dt = LeerTxt.ValidarDT(dt)
                            InsertaCargaConteoDLX(dt)
                            'BulkCopy.Insertar(dt, CReglasNegocio.CargaConteosDLX)
                            Try
                                Dim msg As String = Insertar()
                                If msg = "OK" Then
                                    CMensajes.MostrarMensaje("Información agregada correctamente.", CMensajes.Tipo.GetExito, wucMessageTransferirConteo_DLX)
                                Else
                                    CMensajes.MostrarMensaje(msg, CMensajes.Tipo.GetInformativo, wucMessageTransferirConteo_DLX)
                                End If
                            Catch ex As Exception
                                Throw New Exception(ex.Message, ex.InnerException)
                            End Try
                        Else
                            CMensajes.MostrarMensaje("Archivo de Excel inválido.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                        End If
                    Catch ex As Exception
                        Throw New Exception(ex.Message, ex.InnerException)
                    End Try
                End If
                'si selecciona la opcio de txt
            ElseIf Me.rdformato.SelectedIndex = 1 Then
                Try
                    If Archivo.EndsWith(".txt") = False AndAlso Archivo.EndsWith(".TXT") = False Then
                        CMensajes.MostrarMensaje("Debe seleccionar un archivo con extensión .txt.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                    ElseIf Archivo.StartsWith(IdTienda) = False Then
                        CMensajes.MostrarMensaje("El archivo seleccionado no corresponde al de la tienda, favor de verificar.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                    Else
                        Ruta = Ruta & "ArchivoDlx.txt"
                        File.Delete(Ruta)
                        If IO.File.Exists(Ruta) Then
                            CMensajes.MostrarMensaje("No se pudo reemplazar el archivo.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                        End If

                        Me.RadUpload1.SaveAs(Ruta)

                        If Not IO.File.Exists(Ruta) Then
                            CMensajes.MostrarMensaje("El archivo no se guardó correctamente.", CMensajes.Tipo.GetError, wucMessageTransferirConteo_DLX)
                        Else
                            Try
                                Dim dt As New DataTable
                                dt = LeerTxt.LeerTxtConteos(Ruta, "|")
                                'dt = LeerTxt.ValidarDT(dt)
                                InsertaCargaConteoDLX(dt)
                                'BulkCopy.Intsertar(Ruta, "|", CReglasNegocio.CargaConteosDLX)

                                Dim msg As String = Insertar()
                                If msg = "OK" Then
                                    CMensajes.MostrarMensaje("Información agregada correctamente.", CMensajes.Tipo.GetExito, wucMessageTransferirConteo_DLX)
                                Else
                                    CMensajes.MostrarMensaje(msg, CMensajes.Tipo.GetInformativo, wucMessageTransferirConteo_DLX)
                                End If
                            Catch ex As Exception
                                Throw New Exception(ex.Message, ex.InnerException)
                            End Try
                        End If
                    End If
                Catch ex As Exception
                    Throw New Exception(ex.Message, ex.InnerException)
                End Try
            End If
            Me.btncargar.Enabled = True
            Me.btncargar.Text = "Cargar archivo"
        End If
    End Sub

    Function Insertar() As String
        Try
            Dim prueba As Integer
            prueba = Me.rbtnNumConteo.SelectedValue
            'Se procede a la inserción de datos en BD
            Dim par(3) As SqlParameter
            Dim ds As New DataSet
            par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
            par(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
            par(2) = New SqlParameter("@perfil", Session("Perfil").ToString)
            par(3) = New SqlParameter("@NumConteo", Me.rbtnNumConteo.SelectedValue)

            ds = DatosSQL.funcioncp("Transferir_ConteoDLX2", par)

            'ds = DatosSQL.funcioncp("Transferir_ConteoDLX_SSIS", par)
            Return ds.Tables(0).Rows(0)(0)
        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try
    End Function

    Sub InsertaCargaConteoDLX(ByVal dt As DataTable)
        Try
            ' Limpia tabla temporal CargaConteosDLX_TEMP
            Dim par(1) As SqlParameter
            Dim ds As New DataSet
            par(0) = New SqlParameter("@Accion", "1")
            par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))

            ds = DatosSQL.funcioncp("Inserta_CargaConteosDLX_nuevo", par)

            ' Llena tabla temporal CargaConteosDLX_TEMP
            BulkCopy.Insertar(dt, "CargaConteosDLX_TEMP")

            ' Llena tabla CargaConteosDLX
            Dim par1(1) As SqlParameter
            Dim ds1 As New DataSet
            par1(0) = New SqlParameter("@Accion", "2")
            par1(1) = New SqlParameter("@IdTienda", Session("IdTienda"))

            ds1 = DatosSQL.funcioncp("Inserta_CargaConteosDLX_nuevo", par1)

        Catch ex As Exception
            Throw New Exception(ex.Message, ex.InnerException)
        End Try

    End Sub

End Class