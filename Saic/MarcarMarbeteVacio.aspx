﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="MarcarMarbeteVacio.aspx.vb" Inherits="Saic.MarcarMarbeteVacio" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Marcar marbete vacío" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageMarcarMarbeteVacio" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" align="center">
                    <tr>
                        <td>
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="Marbete" DataField="IdMarbete" ItemStyle-Width="120px" ControlStyle-Width="120px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:TemplateField HeaderStyle-Width="80px" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>              
                                                <asp:Label ID="lbltitulochk" runat="server" Text="Marcar como vacío&lt;br /&gt;"></asp:Label>
                                                <asp:CheckBox ID="chkheader"  runat="server" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" />
                                            </HeaderTemplate>
                                            <ItemTemplate>                                                                                                                                                                                      
                                                <asp:CheckBox ID="chkvacio" runat="server" onclick="javascript:CheckOne('grid',1);" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Vacío" DataField="Vacio" ItemStyle-Width="85px" ControlStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <span><asp:Button runat="server" Text="Marcar marbetes seleccionados" ID="btnModificar" CssClass="btn" CausesValidation="false" /></span>
            </td>
        </tr>
    </table>
</asp:Content>
