﻿Imports System.Data
Imports System.Data.SqlClient
Public Class CambiarMarbeteAlmacen
	Inherits System.Web.UI.Page

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageCambiarMarbAlmacen.Visible = False

		Try
			If Not Page.IsPostBack Then
				CargarAlmacenActual()
				CargarAlmacenNuevo()
				CargarMarbetes()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub


#Region "CargarDatos"
	Sub CargarAlmacenActual()
		Try
			Dim dtAlmacenes As New DataTable
			dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

			Me.cboAlmacenActual.DataSource = dtAlmacenes
			Me.cboAlmacenActual.DataValueField = "idAlmacen"
			Me.cboAlmacenActual.DataTextField = "idAlmacen"
			Me.cboAlmacenActual.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarAlmacenNuevo()
		Try
			Dim dtAlmacenes As New DataTable
			dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

			Dim Row As DataRow = dtAlmacenes.NewRow
			Row("idAlmacen") = "0"
			Row("idAlmacen") = "Seleccionar Almacen"
			dtAlmacenes.Rows.InsertAt(Row, 0)
			dtAlmacenes.AcceptChanges()

			Me.cboAlmacenNuevo.DataSource = dtAlmacenes
			Me.cboAlmacenNuevo.DataValueField = "idAlmacen"
			Me.cboAlmacenNuevo.DataTextField = "idAlmacen"
			Me.cboAlmacenNuevo.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarMarbetes()
		Try
			Dim dsMarbetes As New DataSet
			Dim par(2) As SqlParameter

			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			If Me.cboAlmacenNuevo.SelectedIndex = -1 Then
				par(2) = New SqlParameter("@IdAlmacen", "A001")
			Else
				par(2) = New SqlParameter("@IdAlmacen", Me.cboAlmacenActual.SelectedValue)
			End If

			dsMarbetes = DatosSQL.funcioncp("Marbetes_Mostrar", par)

			Me.dg.DataSource = dsMarbetes
			'Me.dg.DataValueField = "idAlmacen"
			'Me.dg.DataTextField = "idAlmacen"
			Me.dg.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub cboAlmacenActual_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboAlmacenActual.SelectedIndexChanged
		CargarMarbetes()
	End Sub
#End Region


	Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
		If Me.cboAlmacenNuevo.SelectedIndex = 0 Then
			CMensajes.MostrarMensaje("Es necesario seleccionar el almacén nuevo.", CMensajes.Tipo.GetError, wucMessageCambiarMarbAlmacen)
		ElseIf Me.cboAlmacenActual.SelectedValue = Me.cboAlmacenNuevo.SelectedValue Then
			CMensajes.MostrarMensaje("El marbete actual y el marbete nuevo no pueden ser iguales.", CMensajes.Tipo.GetError, wucMessageCambiarMarbAlmacen)
		Else
			Try
				Dim Sentencia As String = ""
				Dim SentenciaNueva As String = ""
				For i As Integer = 0 To Me.dg.Rows.Count - 1
					Dim ck As CheckBox = CType(dg.Rows(i).Cells(0).Controls(1), CheckBox)
					If ck.Checked = True Then
                        SentenciaNueva = " EXEC Marbetes_Cambiar_Almacen @idTienda='" & Session("IdTienda") &
                                         "',@Perfil='" & CStr(Session("Perfil")).Substring(0, 1) &
                                         "',@idAlmacenNuevo='" & Me.cboAlmacenNuevo.SelectedValue &
                                         "',@idMarbete='" & dg.Rows(i).Cells(1).Text & "'"

                        If Len(Sentencia & SentenciaNueva) > 8000 Then
							DatosSQL.procedimientoText(Sentencia)
							Sentencia = ""
						End If
						Sentencia = Sentencia + SentenciaNueva
					End If
				Next
				If Len(Sentencia) > 0 AndAlso Len(Sentencia) <= 8000 Then
					DatosSQL.procedimientoText(Sentencia)
					CargarMarbetes()
					CMensajes.MostrarMensaje("Los marbetes seleccionados fueron enviados al almacén " & Me.cboAlmacenNuevo.SelectedValue, CMensajes.Tipo.GetExito, wucMessageCambiarMarbAlmacen)
				Else
					CMensajes.MostrarMensaje("No se ha seleccionado ningún marbete.", CMensajes.Tipo.GetError, wucMessageCambiarMarbAlmacen)
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(dg.HeaderRow.Cells(0).FindControl("Checkbox3"), CheckBox)
		For Each row As GridViewRow In dg.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(0).FindControl("Checkbox4"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub
End Class
