﻿Imports System.Data.SqlClient

Public Class CorreccionesFinalesPorHoja
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageCorreccionesFinalesPorHoja.Visible = False

		If Not Page.IsPostBack Then
			Departamentos_Buscar()
			CargarAlmacen()
		End If
	End Sub

	Sub Departamentos_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("CatalogoGeneral_Deptos_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.cboDepto.DataSource = ds.Tables(0)
				Me.cboDepto.DataValueField = "CODE_DEPTO"
				Me.cboDepto.DataTextField = "NOM_DEPTO"
				Me.cboDepto.DataBind()
			Else
				CMensajes.MostrarMensaje("No existen datos para generar el reporte.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorHoja)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
		Try
			If Me.cboDepto.SelectedValue = "-1" Then
				CMensajes.MostrarMensaje("Seleccione el departamento.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorHoja)
			ElseIf Not IsNumeric(txtHoja.Text) Then
				CMensajes.MostrarMensaje("Hoja debe ser un valor númerico.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorHoja)
			Else
				Dim ds As New DataSet
				Dim Par(3) As SqlParameter
				Par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				Par(1) = New SqlParameter("@CODE_DEPTO", Me.cboDepto.SelectedValue)
				Par(2) = New SqlParameter("@Hoja", CInt(Me.txtHoja.Text.Trim))
				Par(3) = New SqlParameter("@Almacen", Me.cboAlmacenes.SelectedValue)
				ds = DatosSQL.funcioncp("CorreccionesFinales80_20DeptoHoja", Par)
				If ds.Tables(0).Rows.Count > 0 Then
					Me.dg.DataSource = ds.Tables(0)
					Me.dg.DataBind()
					Me.btnGuardar.Visible = True
				Else
					Me.dg.DataSource = Nothing
					Me.dg.DataBind()
					CMensajes.MostrarMensaje("No se encontraron datos.", CMensajes.Tipo.GetError, wucMessageCorreccionesFinalesPorHoja)
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
		Try
			Dim ModCount As Integer = 0
			Dim CantNoNumCount As Integer = 0
			Dim CantNegCount As Integer = 0
			For i As Integer = 0 To Me.dg.Rows.Count - 1
				Dim Cant As TextBox = CType(dg.Rows(i).Cells(4).Controls(1), TextBox)
				If Cant.Text.Trim.Length > 0 Then
					If IsNumeric(Cant.Text.Trim) = False Then
						CantNoNumCount = CantNoNumCount + 1
						Cant.BackColor = Drawing.Color.Red
					ElseIf CInt(Cant.Text.Trim) < 0 Then
						CantNegCount = CantNegCount + 1
						Cant.BackColor = Drawing.Color.Yellow
					ElseIf Me.ActualizarConteoFinal(dg.Rows(i).Cells(1).Text, Cant.Text) = True Then
						ModCount = ModCount + 1
						Cant.BackColor = Drawing.Color.Transparent
						Me.dg.Rows(i).Cells(3).Text = Microsoft.VisualBasic.FormatNumber(Cant.Text, 2)
						Cant.Text = ""
					End If
				End If
			Next
			If ModCount >= 0 AndAlso CantNoNumCount = 0 AndAlso CantNegCount = 0 Then
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas.", CMensajes.Tipo.GetInformativo, wucMessageCorreccionesFinalesPorHoja)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount > 0 AndAlso CantNegCount = 0 Then
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo.", CMensajes.Tipo.GetInformativo, wucMessageCorreccionesFinalesPorHoja)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount = 0 AndAlso CantNegCount > 0 Then
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en amarillo.", CMensajes.Tipo.GetInformativo, wucMessageCorreccionesFinalesPorHoja)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount > 0 AndAlso CantNegCount > 0 Then
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo y amarillo.", CMensajes.Tipo.GetInformativo, wucMessageCorreccionesFinalesPorHoja)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function ActualizarConteoFinal(ByVal EAN As String, ByVal ConteoFinal As Integer) As Boolean
		Try
			ActualizarConteoFinal = False

			Dim par(5) As SqlParameter
			par(0) = New SqlParameter("@Accion", 2) 'Modificar
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			par(3) = New SqlParameter("@EAN", EAN)
			par(4) = New SqlParameter("@ConteoFinal", ConteoFinal)
			par(5) = New SqlParameter("@Almacen", Me.cboAlmacenes.SelectedValue)


			DatosSQL.procedimientocp("ConteoConsolidado_Administracion", par)
			ActualizarConteoFinal = True
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub CargarAlmacen()
		Try
			Dim dtAlmacenes As New DataTable
			dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

			Me.cboAlmacenes.DataSource = dtAlmacenes
			Me.cboAlmacenes.DataValueField = "idAlmacen"
			Me.cboAlmacenes.DataTextField = "idAlmacen"
			Me.cboAlmacenes.DataBind()

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class