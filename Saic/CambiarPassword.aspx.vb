﻿Imports System.Data.SqlClient

Public Class CambiarPassword
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			If Session("IdUsuario") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If

			Me.wucMessageCambiarPassword.Visible = False
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnCambiar_Click(sender As Object, e As EventArgs) Handles btnCambiar.Click
		Try
			If Me.txtPassword.Text.Trim = Me.txtConPass.Text.Trim Then
				Dim par(3) As SqlParameter
				par(0) = New SqlParameter("@Accion", 4)
				par(1) = New SqlParameter("@Password", Me.txtPassword.Text.Trim)
				par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
				par(3) = New SqlParameter("@PasswordAnt", Me.txtPassAnterior.Text.Trim)
				DatosSQL.funcioncp("Usuarios_Administracion", par)
				CMensajes.MostrarMensaje("Se actualizó el password con éxito.", CMensajes.Tipo.GetExito, wucMessageCambiarPassword)
			Else
				CMensajes.MostrarMensaje("La confirmación de la nueva contraseña no coincide.", CMensajes.Tipo.GetError, wucMessageCambiarPassword)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class