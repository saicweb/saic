﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_ArticulosNoContadosFisicamente
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageRPT_ArticulosNoContadosFisicamente.Visible = False

		If Session("Sociedad").ToString = "3" Then
			Me.lblAlmacen.Visible = False
			Me.cboAlmacen.Visible = False
		End If

		If Not Page.IsPostBack Then
			If Session("Sociedad").ToString <> "3" Then Almacen_Buscar()
			Session.Remove("RPT_ArtNoContadosFisicamente")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim almacen As String
			If Session("Sociedad").ToString = "3" Then
				almacen = "A001"
			Else
				almacen = Me.cboAlmacen.SelectedValue
			End If
			CatalogoReportes_CRV13(almacen)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_ArticulosNoContados_Fisicamente.DataBinding
		If Session("RPT_ArtNoContadosFisicamente") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_ArticulosNoContados_Fisicamente.ReportSource = rpt
				Session("RPT_ArtNoContadosFisicamente") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_ArticulosNoContados_Fisicamente.ReportSource = Session("RPT_ArtNoContadosFisicamente")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = RPT_ArticulosNoContadosFisicamente(Almacen)

		Dim nomReporte As String = IIf(Session("Sociedad").ToString = "3", NombreReportes.RPT_ArticulosNoContadosFisicamente_RST, NombreReportes.RPT_ArticulosNoContadosFisicamente)
		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & nomReporte))
		txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "ARTICULOS NO CONTADOS FISICAMENTE"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_ArticulosNoContados_Fisicamente.ReportSource = rpt
			RPT_ArticulosNoContados_Fisicamente.DataBind()
		Else
			CMensajes.MostrarMensaje("No existen artículos no contados fisicamente.", CMensajes.Tipo.GetInformativo, wucMessageRPT_ArticulosNoContadosFisicamente)
		End If
	End Sub

	Public Function RPT_ArticulosNoContadosFisicamente(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Almacen", Almacen)
			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_ArticulosNoContadosFisicamente_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ArticulosNoContadosFisicamente", par)
			End If
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(1) As SqlParameter
				par2(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par2(1) = New SqlParameter("@Almacen", Almacen)

				If Session("Sociedad").ToString = "3" Then
					dsExcel = DatosSQL.funcioncp("RPT_ArticulosNoContadosFisicamenteEXCEL_RST", par2)
				Else
					dsExcel = DatosSQL.funcioncp("RPT_ArticulosNoContadosFisicamenteEXCEL", par2)
				End If
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					If Session("Sociedad").ToString = "3" Then
						Reportes.REPEXCEL(dsExcel, "Articulos no contados fisicamente", "ArticulosNoContadosFisicamente_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), False)
					Else
						Reportes.REPEXCEL(dsExcel, "Articulos no contados fisicamente", "ArticulosNoContadosFisicamente_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), True)
					End If
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@validar", 2)
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class