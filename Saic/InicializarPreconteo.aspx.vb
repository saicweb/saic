﻿Imports System.Data.SqlClient

Public Class InicializarPreconteo
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If
				If Session("Perfil") = "R" Then
					CMensajes.Show("Sólo el perfil 'Preconteo' tiene permiso de acceso a esta sección.")
					Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@Accion", 4)
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			DatosSQL.procedimientocp("Preconteos_Administracion", par)
			CMensajes.MostrarMensaje("Preconteo inicializado correctamente.", CMensajes.Tipo.GetExito, wucMessageInicializarPreconteo)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageInicializarPreconteo.Visible = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Protected Sub btnInicializar_Click(sender As Object, e As EventArgs) Handles btnInicializar.Click
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">Éste proceso eliminará cualquier información que esté relacionada con el preconteo. ¿Realmente desea inicializar el preconteo?</div>"
		winConfirm1.MuestraDialogo()
	End Sub
End Class