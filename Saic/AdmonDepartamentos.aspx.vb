﻿Imports System.Data.SqlClient
Public Class AdmonDepartamentos
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button
	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If

				ConsultarDepto()

			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
				Me.btnGuardar.Visible = False
				Me.btnModificar.Visible = False
				Me.btnEliminar.Visible = False
			End Try
		End If
	End Sub

	Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
		Deparatamentos_Administracion(1)
	End Sub
	Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
		Deparatamentos_Administracion(2)
	End Sub


#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub
#End Region

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Deparatamentos_Administracion(3)
	End Sub
	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.winConfirm1.OcultaDialogo()
		Response.Redirect("AdmonDepartamentos.aspx?SID=" & Session("SID"), False)
	End Sub

	Sub Deparatamentos_Administracion(ByVal Accion As Integer)
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Accion", Accion)
			par(1) = New SqlParameter("@IdDepartamento", Me.txtIdDepartamento.Text.ToUpper.Trim)
			par(2) = New SqlParameter("@DescDepto", Me.txtNombreDepartamento.Text.Trim.ToUpper)

			ds = DatosSQL.funcioncp("Departamentos_Administracion", par)

			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0), CMensajes.Tipo.GetError, wucMessageAdmonDepartamentos)
			Else
				ConsultarDepto()
				btnGuardar.Visible = True
				btnModificar.Visible = False
				btnEliminar.Visible = False
				txtIdDepartamento.Enabled = True
				LimpiarCambpos()

				Select Case Accion
					Case 1 '--NUEVO
						CMensajes.MostrarMensaje("Departamento Agregado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonDepartamentos)
					Case 2 '--MODIFICAR
						CMensajes.MostrarMensaje("Departamento Modificado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonDepartamentos)
					Case 3 '--ELIMINAR
						CMensajes.MostrarMensaje("Departamento Eliminado exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonDepartamentos)
				End Select
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub LimpiarCambpos()
		txtIdDepartamento.Text = ""
		txtNombreDepartamento.Text = ""
	End Sub

	Sub ConsultarDepto()
		Try
			'IdArea,NombreArea
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdDepartamento", -1)

			ds = DatosSQL.funcioncp("Departamentos_Buscar", par)
			gvDepartamento.DataSource = ds
			gvDepartamento.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub gvDepartamento_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvDepartamento.RowCommand
		Try
			If e.CommandName = "Modificar" Then
				Dim index As Integer = Convert.ToInt32(e.CommandArgument)
				Dim id, descripcion As String
				id = gvDepartamento.Rows(index).Cells(0).Text
				descripcion = gvDepartamento.Rows(index).Cells(1).Text

				txtIdDepartamento.Text = id
				txtNombreDepartamento.Text = descripcion
				txtIdDepartamento.Enabled = False
				btnModificar.Visible = True
				btnGuardar.Visible = False
			ElseIf (e.CommandName = "Eliminar") Then
				Dim index As Integer = Convert.ToInt32(e.CommandArgument)
				Dim id, descripcion As String
				id = gvDepartamento.Rows(index).Cells(0).Text
				descripcion = HttpUtility.HtmlDecode(gvDepartamento.Rows(index).Cells(1).Text)

				txtIdDepartamento.Text = id
				txtNombreDepartamento.Text = descripcion
				txtIdDepartamento.Enabled = False
				btnModificar.Visible = False
				btnGuardar.Visible = False
				btnEliminar.Visible = False
				Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
				lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar el departamento?</div>"
				winConfirm1.MuestraDialogo()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class