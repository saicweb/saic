﻿Imports System.Data.SqlClient

Public Class CambiarMarbeteUbicacion
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Session("Cedis") = 0 Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		Else
			Me.wucMessageCambiarMarbUbicacion.Visible = False

			Try
				If Not Page.IsPostBack Then
					CargarMarbetes()
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If

	End Sub

	Sub CargarMarbetes()
		Try
			Dim dsMarbetes As New DataSet
			Dim par(1) As SqlParameter

			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))

			dsMarbetes = DatosSQL.funcioncp("CambiarMarbeteUbicacion_MostrarMarbete", par)

			Me.dg.DataSource = dsMarbetes
			Me.dg.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
		Try
			Dim err As Integer = 0
			For i As Integer = 0 To Me.dg.Rows.Count - 1
				Dim ck As CheckBox = CType(dg.Rows(i).Cells(0).Controls(1), CheckBox)
				If ck.Checked = True Then
					err += 1
				End If
			Next

			If err = 0 Then
				CMensajes.MostrarMensaje("Seleccione por lo menos un marbete para el cambio de ubicación.", CMensajes.Tipo.GetError, wucMessageCambiarMarbUbicacion)
			ElseIf String.IsNullOrEmpty(Me.txtUbicacionNueva.Text) OrElse String.IsNullOrWhiteSpace(Me.txtUbicacionNueva.Text) Then
				CMensajes.MostrarMensaje("Especifique una ubicación nueva.", CMensajes.Tipo.GetError, wucMessageCambiarMarbUbicacion)
			Else
				Dim ubicacionNueva As String = Nothing
				ubicacionNueva = Me.txtUbicacionNueva.Text

				For i As Integer = 0 To Me.dg.Rows.Count - 1
					Dim ck As CheckBox = CType(dg.Rows(i).Cells(0).Controls(1), CheckBox)
					If ck.Checked = True Then
						Dim marbete As String = Nothing
						marbete = dg.Rows(i).Cells(1).Text

						'Agregar metodo para actualizar la ubicación
						Dim par(3) As SqlParameter

						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@Perfil", Session("Perfil"))
						par(2) = New SqlParameter("@Ubicacion", ubicacionNueva)
						par(3) = New SqlParameter("@IdMarbete", marbete)

						DatosSQL.procedimientocp("Actualizar_Ubicacion", par)
					End If
				Next
				CargarMarbetes()
				CMensajes.MostrarMensaje("Se actualizó la ubicación correctamente.", CMensajes.Tipo.GetExito, wucMessageCambiarMarbUbicacion)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(dg.HeaderRow.Cells(0).FindControl("Checkbox3"), CheckBox)
		For Each row As GridViewRow In dg.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(0).FindControl("Checkbox4"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub

End Class