﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Public Class RPT_TotalMarbete
    Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("UnidadNegocio") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Dim unidadNegocio As String = Session("UnidadNegocio")
		Dim idTienda As String = Session("IdTienda")

		If Not Page.IsPostBack Then
			Session.Remove("RPT_TotalMarbete")
		End If
	End Sub

    Protected Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
		CatalogoReportes_CRV13()
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Total_Marbete.DataBinding
		If Session("RPT_TotalMarbete") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Total_Marbete.ReportSource = rpt
				Session("RPT_TotalMarbete") = rpt
				Me.btnGenerarReporte.Visible = False
			End If
		Else
			RPT_Total_Marbete.ReportSource = Session("RPT_TotalMarbete")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13()
		ds = ConsultaTotalPorMarbete()

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_TotalMarbete))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "REPORTE RESUMEN POR MARBETE"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Total_Marbete.ReportSource = rpt
			RPT_Total_Marbete.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_TotalMarbete)
		End If
	End Sub

	Private Function ConsultaTotalPorMarbete() As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@idTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			ds = DatosSQL.funcioncp("RPT_MarbetesTotalPorMarbete", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(1) As SqlParameter
				par2(0) = New SqlParameter("@idTienda", Session("IdTienda"))
				par2(1) = New SqlParameter("@Perfil", Session("Perfil"))

				dsExcel = DatosSQL.funcioncp("RPT_MarbetesTotalPorMarbeteExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					Reportes.REPEXCEL(dsExcel, "Reporte Total por Marbete", "TotalXMarbete", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class