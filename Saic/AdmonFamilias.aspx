﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonFamilias.aspx.vb" Inherits="Saic.AdmonFamilias" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Familias" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageAdmonFamilias" runat="server" />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table runat="server" width="45%" align="center">
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Id familia: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtIdFamilia" runat="server" MaxLength="20" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtIdFamilia" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Descripción: "></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtdescripcion" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdescripcion" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" Text="Departamento: "></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="cboDepartamento" Width="250px" AutoPostBack="true" CssClass="dropdown"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr align="center">
                                <td colspan="2">
                                    <br />
                                    <span><asp:Button runat="server" Text="Agregar" ID="btnGuardar" CssClass="btn" Visible="true" /></span>
                                    <span><asp:Button runat="server" Text="Actualizar" CausesValidation="false"  ID="btnModificar" CssClass="btn" Visible="false" /></span>
                                    <span><asp:Button ID="btnEliminar" runat="server" CausesValidation="false" Visible="False" Text="Eliminar" CssClass="btn"/></span>               
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnGuardar" />
                        <asp:PostBackTrigger ControlID="btnModificar" />
                        <asp:PostBackTrigger ControlID="gvFamilias" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                    <asp:GridView ID="gvFamilias" runat="server" AutoGenerateColumns="false" SkinID="Grid3">
                        <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                        <Columns>
                            <asp:BoundField DataField="IdFamilia" HeaderText="IdFamilia" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="DescripcionFamilia" HeaderText="Descripción" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                            <asp:BoundField DataField="DescDepto" HeaderText="Departamento" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Middle" />
                            <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkmodificar" CausesValidation="false" CommandName="Modificar" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Modificar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEliminar" CausesValidation="false" CommandName="Eliminar" CommandArgument='<%# Container.DataItemIndex %>' runat="server"  >Eliminar</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
</asp:Content>
