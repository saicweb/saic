﻿Public Class CSesiones
	Inherits CRespuesta

#Region "PROPIEDADES"
	Public Property Cnn As String
	Public Property idSesion As String
	Public Property fechaSesion As String
	Public Property idUsuario As String
	Public Property contrasena As String
	Public Property correoElectronico As String
	Public Property idTipoUsuario As String
	Public Property TipoUsuario As String
	Public Property privilegio As String
	Public Property usuarioEspecial As String
	Public Property Accion As String
	Public Property Perfil As String
	Public Property URL As String
#End Region

#Region "METODOS"

	Public Function Ejecuta_sesiones() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@idSesion", idSesion)
		CnnSQL.AgregarParametros("@fechaSesion", fechaSesion)
		CnnSQL.AgregarParametros("@idUsuario", idUsuario)
		CnnSQL.AgregarParametros("@contrasena", contrasena)
		CnnSQL.AgregarParametros("@correoElectronico", correoElectronico)
		CnnSQL.AgregarParametros("@idTipoUsuario", idTipoUsuario)
		CnnSQL.AgregarParametros("@TipoUsuario", TipoUsuario)
		CnnSQL.AgregarParametros("@usuarioEspecial", usuarioEspecial)
		CnnSQL.AgregarParametros("@Accion", Accion)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procsesiones, SQLConexion.CnnSql.TipoDato.StoredProcedure)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CSesiones - " & ex.Message)
		End Try
		Return GeneraException()
	End Function


	Public Function DestruyeSesion() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@SID", idSesion)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procDestruyeSesion, SQLConexion.CnnSql.TipoDato.StoredProcedure)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CSesiones - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function consultaPermisosUsuario() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@SID", idSesion)
		CnnSQL.AgregarParametros("@Perfil", Perfil)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procPermisosSesion, SQLConexion.CnnSql.TipoDato.StoredProcedure)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CSesiones - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

#End Region

#Region "Constructores"
	Sub New(ConexionString As String)
		_Cnn = ConexionString
	End Sub
	Sub New()
		_Cnn = CReglasNegocio.CnnSeguridad
	End Sub
#End Region

#Region "TipoAccion"
	''' <summary>
	''' Accion que se ejecutara dentro del  SP
	''' </summary>
	''' <remarks></remarks>
	Public Enum TipoAccion
		''' <summary>
		''' Permite Obtener idusuario,noEmpleado,nombreUsuario con el parametro de idsesion
		''' </summary>
		GetInfoUsuario = 1
		''' <summary>
		''' permite validar si existe info. con el parametro IdSesion
		''' </summary>
		''' <remarks></remarks>
		GetExists = 2
	End Enum
#End Region
End Class
