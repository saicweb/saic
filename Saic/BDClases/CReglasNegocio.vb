﻿Public MustInherit Class CReglasNegocio
#Region "SEGURIDAD"
	Protected Const procusuarios As String = "procusuarios"
	Protected Const procLogin As String = "procLogin"
	Protected Const procusuariosession As String = "procusuariosession"
	Protected Const procUnidadNegocio As String = "procUnidadNegocio"
	Protected Const procsesiones As String = "procsesiones"
	Protected Const procPermisosSesion As String = "procPermisosSesion"
	Protected Const procDestruyeSesion As String = "procDestruyeSesion"
#End Region

#Region "SAIC"
	Public Const Sufijo As String = "Sufijo"
	Protected Const Materiales0304 As String = "Materiales0304"
	Protected Const procCheckList As String = "procCheckList"
	Protected Const procEvaluacion As String = "procEvaluacion"
	Protected Const procIncidencias As String = "procIncidencias"
	Protected Const procInv_externo As String = "procInv_externo"
	Protected Const procInv_oper As String = "procInv_oper"
#End Region

#Region "TABLAS BULKCOPY"
	Public Const MEAN As String = "MEAN"
	Public Const GRAN As String = "GRAN"
	Public Const GRANINV As String = "GRANINV"
	Public Const Conteos As String = "Conteos"
	Public Const Materiales As String = "Materiales"
	Public Const CargaConteosDLX As String = "CargaConteosDLX"
	Public Const CatalogoInventarioTeorico As String = "CatalogoInventarioTeorico"
#End Region

#Region "CONEXIONES"
	Public Shared CnnSeguridad As String = ConfigurationManager.AppSettings("cnnSeguridad")
	Public Shared CnnSAIC As String = ConfigurationManager.AppSettings("cnn")
#End Region
End Class
