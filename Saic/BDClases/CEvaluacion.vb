﻿Public Class CEvaluacion
	Inherits CRespuesta
#Region "VARIABLES"
	Private _Folio As String
	Private _Fecha As String
	Private _Sucursal As String
	Private _Calificación As String
	Private _Adi As String
	Private _Apoyo1 As String
	Private _Apoyo2 As String
	Private _Encargada As String
	Private _V1_1 As String
	Private _V1_2 As String
	Private _V1_3 As String
	Private _V1_4 As String
	Private _Obs1_1 As String
	Private _Obs1_2 As String
	Private _Obs1_3 As String
	Private _Obs1_4 As String
	Private _V2_1 As String
	Private _V2_2 As String
	Private _V2_3 As String
	Private _V2_4 As String
	Private _V2_5 As String
	Private _V2_6 As String
	Private _V2_7 As String
	Private _V2_8 As String
	Private _V2_9 As String
	Private _V2_10 As String
	Private _V2_11 As String
	Private _Obs2_1 As String
	Private _Obs2_2 As String
	Private _Obs2_3 As String
	Private _Obs2_4 As String
	Private _Obs2_5 As String
	Private _Obs2_6 As String
	Private _Obs2_7 As String
	Private _Obs2_8 As String
	Private _Obs2_9 As String
	Private _Obs2_10 As String
	Private _Obs2_11 As String
	Private _V3_1 As String
	Private _V3_2 As String
	Private _V3_3 As String
	Private _V3_4 As String
	Private _V3_5 As String
	Private _Obs3_1 As String
	Private _Obs3_2 As String
	Private _Obs3_3 As String
	Private _Obs3_4 As String
	Private _Obs3_5 As String
	Private _Accion As String
	Private _Cnn As String
#End Region

#Region "PROPIEDADES"
	Public Property Folio As String
		Get
			Return _Folio
		End Get
		Set(ByVal value As String)
			_Folio = value
		End Set
	End Property

	Public Property Fecha As String
		Get
			Return _Fecha
		End Get
		Set(ByVal value As String)
			_Fecha = value
		End Set
	End Property

	Public Property Sucursal As String
		Get
			Return _Sucursal
		End Get
		Set(ByVal value As String)
			_Sucursal = value
		End Set
	End Property

	Public Property Calificación As String
		Get
			Return _Calificación
		End Get
		Set(ByVal value As String)
			_Calificación = value
		End Set
	End Property

	Public Property Adi As String
		Get
			Return _Adi
		End Get
		Set(ByVal value As String)
			_Adi = value
		End Set
	End Property

	Public Property Apoyo1 As String
		Get
			Return _Apoyo1
		End Get
		Set(ByVal value As String)
			_Apoyo1 = value
		End Set
	End Property

	Public Property Apoyo2 As String
		Get
			Return _Apoyo2
		End Get
		Set(ByVal value As String)
			_Apoyo2 = value
		End Set
	End Property

	Public Property Encargada As String
		Get
			Return _Encargada
		End Get
		Set(ByVal value As String)
			_Encargada = value
		End Set
	End Property

	Public Property V1_1 As String
		Get
			Return _V1_1
		End Get
		Set(ByVal value As String)
			_V1_1 = value
		End Set
	End Property

	Public Property V1_2 As String
		Get
			Return _V1_2
		End Get
		Set(ByVal value As String)
			_V1_2 = value
		End Set
	End Property

	Public Property V1_3 As String
		Get
			Return _V1_3
		End Get
		Set(ByVal value As String)
			_V1_3 = value
		End Set
	End Property

	Public Property V1_4 As String
		Get
			Return _V1_4
		End Get
		Set(ByVal value As String)
			_V1_4 = value
		End Set
	End Property

	Public Property Obs1_1 As String
		Get
			Return _Obs1_1
		End Get
		Set(ByVal value As String)
			_Obs1_1 = value
		End Set
	End Property

	Public Property Obs1_2 As String
		Get
			Return _Obs1_2
		End Get
		Set(ByVal value As String)
			_Obs1_2 = value
		End Set
	End Property

	Public Property Obs1_3 As String
		Get
			Return _Obs1_3
		End Get
		Set(ByVal value As String)
			_Obs1_3 = value
		End Set
	End Property

	Public Property Obs1_4 As String
		Get
			Return _Obs1_4
		End Get
		Set(ByVal value As String)
			_Obs1_4 = value
		End Set
	End Property

	Public Property V2_1 As String
		Get
			Return _V2_1
		End Get
		Set(ByVal value As String)
			_V2_1 = value
		End Set
	End Property

	Public Property V2_2 As String
		Get
			Return _V2_2
		End Get
		Set(ByVal value As String)
			_V2_2 = value
		End Set
	End Property

	Public Property V2_3 As String
		Get
			Return _V2_3
		End Get
		Set(ByVal value As String)
			_V2_3 = value
		End Set
	End Property

	Public Property V2_4 As String
		Get
			Return _V2_4
		End Get
		Set(ByVal value As String)
			_V2_4 = value
		End Set
	End Property

	Public Property V2_5 As String
		Get
			Return _V2_5
		End Get
		Set(ByVal value As String)
			_V2_5 = value
		End Set
	End Property

	Public Property V2_6 As String
		Get
			Return _V2_6
		End Get
		Set(ByVal value As String)
			_V2_6 = value
		End Set
	End Property

	Public Property V2_7 As String
		Get
			Return _V2_7
		End Get
		Set(ByVal value As String)
			_V2_7 = value
		End Set
	End Property

	Public Property V2_8 As String
		Get
			Return _V2_8
		End Get
		Set(ByVal value As String)
			_V2_8 = value
		End Set
	End Property

	Public Property V2_9 As String
		Get
			Return _V2_9
		End Get
		Set(ByVal value As String)
			_V2_9 = value
		End Set
	End Property

	Public Property V2_10 As String
		Get
			Return _V2_10
		End Get
		Set(ByVal value As String)
			_V2_10 = value
		End Set
	End Property

	Public Property V2_11 As String
		Get
			Return _V2_11
		End Get
		Set(ByVal value As String)
			_V2_11 = value
		End Set
	End Property

	Public Property Obs2_1 As String
		Get
			Return _Obs2_1
		End Get
		Set(ByVal value As String)
			_Obs2_1 = value
		End Set
	End Property

	Public Property Obs2_2 As String
		Get
			Return _Obs2_2
		End Get
		Set(ByVal value As String)
			_Obs2_2 = value
		End Set
	End Property

	Public Property Obs2_3 As String
		Get
			Return _Obs2_3
		End Get
		Set(ByVal value As String)
			_Obs2_3 = value
		End Set
	End Property

	Public Property Obs2_4 As String
		Get
			Return _Obs2_4
		End Get
		Set(ByVal value As String)
			_Obs2_4 = value
		End Set
	End Property

	Public Property Obs2_5 As String
		Get
			Return _Obs2_5
		End Get
		Set(ByVal value As String)
			_Obs2_5 = value
		End Set
	End Property

	Public Property Obs2_6 As String
		Get
			Return _Obs2_6
		End Get
		Set(ByVal value As String)
			_Obs2_6 = value
		End Set
	End Property

	Public Property Obs2_7 As String
		Get
			Return _Obs2_7
		End Get
		Set(ByVal value As String)
			_Obs2_7 = value
		End Set
	End Property

	Public Property Obs2_8 As String
		Get
			Return _Obs2_8
		End Get
		Set(ByVal value As String)
			_Obs2_8 = value
		End Set
	End Property

	Public Property Obs2_9 As String
		Get
			Return _Obs2_9
		End Get
		Set(ByVal value As String)
			_Obs2_9 = value
		End Set
	End Property

	Public Property Obs2_10 As String
		Get
			Return _Obs2_10
		End Get
		Set(ByVal value As String)
			_Obs2_10 = value
		End Set
	End Property

	Public Property Obs2_11 As String
		Get
			Return _Obs2_11
		End Get
		Set(ByVal value As String)
			_Obs2_11 = value
		End Set
	End Property

	Public Property V3_1 As String
		Get
			Return _V3_1
		End Get
		Set(ByVal value As String)
			_V3_1 = value
		End Set
	End Property

	Public Property V3_2 As String
		Get
			Return _V3_2
		End Get
		Set(ByVal value As String)
			_V3_2 = value
		End Set
	End Property

	Public Property V3_3 As String
		Get
			Return _V3_3
		End Get
		Set(ByVal value As String)
			_V3_3 = value
		End Set
	End Property

	Public Property V3_4 As String
		Get
			Return _V3_4
		End Get
		Set(ByVal value As String)
			_V3_4 = value
		End Set
	End Property

	Public Property V3_5 As String
		Get
			Return _V3_5
		End Get
		Set(ByVal value As String)
			_V3_5 = value
		End Set
	End Property

	Public Property Obs3_1 As String
		Get
			Return _Obs3_1
		End Get
		Set(ByVal value As String)
			_Obs3_1 = value
		End Set
	End Property

	Public Property Obs3_2 As String
		Get
			Return _Obs3_2
		End Get
		Set(ByVal value As String)
			_Obs3_2 = value
		End Set
	End Property

	Public Property Obs3_3 As String
		Get
			Return _Obs3_3
		End Get
		Set(ByVal value As String)
			_Obs3_3 = value
		End Set
	End Property

	Public Property Obs3_4 As String
		Get
			Return _Obs3_4
		End Get
		Set(ByVal value As String)
			_Obs3_4 = value
		End Set
	End Property

	Public Property Obs3_5 As String
		Get
			Return _Obs3_5
		End Get
		Set(ByVal value As String)
			_Obs3_5 = value
		End Set
	End Property

	Public Property Accion As TipoAccion
		Get
			Return _Accion
		End Get
		Set(ByVal value As TipoAccion)
			_Accion = value
		End Set
	End Property

#End Region

#Region "METODOS"
	Public Function INSERTAR() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		CnnSQL.AgregarParametros("@Folio", Folio)
		CnnSQL.AgregarParametros("@Fecha", Fecha)
		CnnSQL.AgregarParametros("@Sucursal", Sucursal)
		CnnSQL.AgregarParametros("@Calificación", Calificación)
		CnnSQL.AgregarParametros("@Adi", Adi)
		CnnSQL.AgregarParametros("@Apoyo1", Apoyo1)
		CnnSQL.AgregarParametros("@Apoyo2", Apoyo2)
		CnnSQL.AgregarParametros("@Encargada", Encargada)
		CnnSQL.AgregarParametros("@V1_1", V1_1)
		CnnSQL.AgregarParametros("@V1_2", V1_2)
		CnnSQL.AgregarParametros("@V1_3", V1_3)
		CnnSQL.AgregarParametros("@V1_4", V1_4)
		CnnSQL.AgregarParametros("@Obs1_1", Obs1_1)
		CnnSQL.AgregarParametros("@Obs1_2", Obs1_2)
		CnnSQL.AgregarParametros("@Obs1_3", Obs1_3)
		CnnSQL.AgregarParametros("@Obs1_4", Obs1_4)
		CnnSQL.AgregarParametros("@V2_1", V2_1)
		CnnSQL.AgregarParametros("@V2_2", V2_2)
		CnnSQL.AgregarParametros("@V2_3", V2_3)
		CnnSQL.AgregarParametros("@V2_4", V2_4)
		CnnSQL.AgregarParametros("@V2_5", V2_5)
		CnnSQL.AgregarParametros("@V2_6", V2_6)
		CnnSQL.AgregarParametros("@V2_7", V2_7)
		CnnSQL.AgregarParametros("@V2_8", V2_8)
		CnnSQL.AgregarParametros("@V2_9", V2_9)
		CnnSQL.AgregarParametros("@V2_10", V2_10)
		CnnSQL.AgregarParametros("@V2_11", V2_11)
		CnnSQL.AgregarParametros("@Obs2_1", Obs2_1)
		CnnSQL.AgregarParametros("@Obs2_2", Obs2_2)
		CnnSQL.AgregarParametros("@Obs2_3", Obs2_3)
		CnnSQL.AgregarParametros("@Obs2_4", Obs2_4)
		CnnSQL.AgregarParametros("@Obs2_5", Obs2_5)
		CnnSQL.AgregarParametros("@Obs2_6", Obs2_6)
		CnnSQL.AgregarParametros("@Obs2_7", Obs2_7)
		CnnSQL.AgregarParametros("@Obs2_8", Obs2_8)
		CnnSQL.AgregarParametros("@Obs2_9", Obs2_9)
		CnnSQL.AgregarParametros("@Obs2_10", Obs2_10)
		CnnSQL.AgregarParametros("@Obs2_11", Obs2_11)
		CnnSQL.AgregarParametros("@V3_1", V3_1)
		CnnSQL.AgregarParametros("@V3_2", V3_2)
		CnnSQL.AgregarParametros("@V3_3", V3_3)
		CnnSQL.AgregarParametros("@V3_4", V3_4)
		CnnSQL.AgregarParametros("@V3_5", V3_5)
		CnnSQL.AgregarParametros("@Obs3_1", Obs3_1)
		CnnSQL.AgregarParametros("@Obs3_2", Obs3_2)
		CnnSQL.AgregarParametros("@Obs3_3", Obs3_3)
		CnnSQL.AgregarParametros("@Obs3_4", Obs3_4)
		CnnSQL.AgregarParametros("@Obs3_5", Obs3_5)
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetInsert)
		Try
			CnnSQL.ejecutaSentenciaNonQuery(procEvaluacion, SQLConexion.CnnSql.TipoDato.StoredProcedure, _Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function CONSULTA() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetSelect)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procEvaluacion)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "CONSTRUCTORES"
	Sub New(ConexionString As String)
		_Cnn = ConexionString
	End Sub
	Sub New()
		_Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		GetInsert = 1
		GetSelect = 2
	End Enum
#End Region
End Class
