﻿Public Class CMateriales
	Inherits CRespuesta

#Region "PROPIEDADES"
	Public Property Cnn As String
	Public Property Accion As String
	Public Property Material As String
	Public Property EAN As String
	Public Property ST As String
#End Region

#Region "METODOS"
	Public Function CantidadMateriales() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@Material", Material)
		CnnSQL.AgregarParametros("@EAN", EAN)
		CnnSQL.AgregarParametros("@ST", ST)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, Materiales0304)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CMateriales - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function EliminarMateriales() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@Material", Material)
		CnnSQL.AgregarParametros("@EAN", EAN)
		CnnSQL.AgregarParametros("@ST", ST)

		Try
			CnnSQL.ejecutaSentenciaNonQuery(Materiales0304, SQLConexion.CnnSql.TipoDato.StoredProcedure, Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CMateriales - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function InsertaMateriales() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@Material", Material)
		CnnSQL.AgregarParametros("@EAN", EAN)
		CnnSQL.AgregarParametros("@ST", ST)

		Try
			CnnSQL.ejecutaSentenciaNonQuery(Materiales0304, SQLConexion.CnnSql.TipoDato.StoredProcedure, Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CMateriales - " & ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "Constructores"
	Sub New(ConexionString As String)
		Cnn = ConexionString
	End Sub
	Sub New()
		Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

End Class
