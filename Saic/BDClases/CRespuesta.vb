﻿''' <summary>
''' Permite obtener acciones de respuesta 
''' </summary>
''' <remarks></remarks>
Public MustInherit Class CRespuesta
	Inherits CReglasNegocio

	Private _dsRespuesta As DataSet 'para guardar la Info.
	Private _ErrorMensaje As String 'Descripcion del Mensaje de Error
	Private _GeneraException As Boolean 'Bandera para generar Error

	''' <summary>
	''' evento para detonar la Excepcion
	''' </summary>
	''' <param name="sErrorMessage"></param>
	''' <remarks></remarks>
	Public Sub DisparaException(ByVal sErrorMessage As String)
		_GeneraException = True
		_ErrorMensaje = sErrorMessage
	End Sub

	''' <summary>
	''' permite obtener el valor de si genero una Excepcion 
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public ReadOnly Property GeneraException() As Boolean
		Get
			Return _GeneraException
		End Get
	End Property

	''' <summary>
	''' propiedad de solo lectura para obtener la descripcion del error
	''' </summary>
	''' <returns>Descripcion del mensaje</returns>
	Public ReadOnly Property ErrorMensaje() As String
		Get
			Return _ErrorMensaje
		End Get
	End Property

	''' <summary>
	''' Propiedad para asignar y obtener el resultado almacenado en un DataSet
	''' </summary>
	''' <value></value>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Property dsRespuesta As DataSet
		Get
			Return _dsRespuesta
		End Get
		Set(ByVal value As DataSet)
			_dsRespuesta = value
		End Set
	End Property

	''' <summary>
	''' Permite validar si _dsRespuesta contiene Info.
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function ValidarDatos() As Boolean
		If _dsRespuesta Is Nothing Then
			Return False
		ElseIf _dsRespuesta.Tables.Count = 0 Then
			Return False
		ElseIf _dsRespuesta.Tables(0).Rows.Count = 0 Then
			Return False
		Else
			Return True
		End If
	End Function

End Class
