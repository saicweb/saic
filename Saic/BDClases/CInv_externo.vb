﻿Public Class CInv_externo
	Inherits CRespuesta
#Region "VARIABLES"
	Private _Tienda As String
	Private _Location As String
	Private _Dist As String
	Private _Zona As String
	Private _Subdirección As String
	Private _FechaInventario As String
	Private _ResponsableInventario As String
	Private _Tipo As String
	Private _HoradeInicioprogramada As String
	Private _Inventarioenunidades As String
	Private _ValorInventario As String
	Private _ValorRGISvsSAIC As String
	Private _PisodeVenta1 As String
	Private _Bodega1 As String
	Private _inventarioPV1 As String
	Private _inventarioB1 As String
	Private _PisodeVenta2 As String
	Private _Bodega2 As String
	Private _inventarioPV2 As String
	Private _inventarioB2 As String
	Private _Programado As String
	Private _Finales As String
	Private _DifenContadores As String
	Private _TotalCambios As String
	Private _Confiabilidad As String
	Private _V1_Confiabilidad As String
	Private _V2_Adherenciaaprocedimientos As String
	Private _V3_Eficiencia As String
	Private _V4_Cortesiaycooperación As String
	Private _V5_Apariencia As String
	Private _V6_Calificaciónalsupervisor As String
	Private _V7_EvaluaciónglobalaRGIS As String
	Private _Observaciones As String
	Private _InicioConteo_RGIS As String
	Private _TerminodeConteo_RGIS As String
	Private _Estatus As String
	Private _CierredeInventario_RGIS As String
	Private _Total_HrasConteo As String
	Private _ValidacionPV As String
	Private _TotaltiempoRGIS As String
	Private _InicioprocesoNoCatalogados As String
	Private _Inicioprocesogeneracion8020 As String
	Private _Inicioprocesovalidación8020 As String
	Private _Inicioprocesocorreccionesfinales As String
	Private _TotaltiempoNoCatalogados As String
	Private _Totaltiempogeneracion8020 As String
	Private _Totaltiempovalidacion8020 As String
	Private _Totaltiempocorreccionesfinales As String
	Private _CierreInventarioGeneral As String
	Private _TiempoTotalInventario As String
	Private _Tarifa2015 As String
	Private _CostoHonorario As String
	Private _Subtotal As String
	Private _TasaIva As String
	Private _Iva As String
	Private _Total As String
	Private _AcumuladoMerma As String
	Private _ResultadoInv As String
	Private _TotalMerma As String
	Private _CostoVentas As String
	Private _Porcentaje As String
	Private _Accion As String
	Private _Cnn As String
#End Region

#Region "PROPIEDADES"
	Public Property Tienda As String
		Get
			Return _Tienda
		End Get
		Set(ByVal value As String)
			_Tienda = value
		End Set
	End Property

	Public Property Location As String
		Get
			Return _Location
		End Get
		Set(ByVal value As String)
			_Location = value
		End Set
	End Property

	Public Property Dist As String
		Get
			Return _Dist
		End Get
		Set(ByVal value As String)
			_Dist = value
		End Set
	End Property

	Public Property Zona As String
		Get
			Return _Zona
		End Get
		Set(ByVal value As String)
			_Zona = value
		End Set
	End Property

	Public Property Subdirección As String
		Get
			Return _Subdirección
		End Get
		Set(ByVal value As String)
			_Subdirección = value
		End Set
	End Property

	Public Property FechaInventario As String
		Get
			Return _FechaInventario
		End Get
		Set(ByVal value As String)
			_FechaInventario = value
		End Set
	End Property

	Public Property ResponsableInventario As String
		Get
			Return _ResponsableInventario
		End Get
		Set(ByVal value As String)
			_ResponsableInventario = value
		End Set
	End Property

	Public Property Tipo As String
		Get
			Return _Tipo
		End Get
		Set(ByVal value As String)
			_Tipo = value
		End Set
	End Property

	Public Property HoradeInicioprogramada As String
		Get
			Return _HoradeInicioprogramada
		End Get
		Set(ByVal value As String)
			_HoradeInicioprogramada = value
		End Set
	End Property

	Public Property Inventarioenunidades As String
		Get
			Return _Inventarioenunidades
		End Get
		Set(ByVal value As String)
			_Inventarioenunidades = value
		End Set
	End Property

	Public Property ValorInventario As String
		Get
			Return _ValorInventario
		End Get
		Set(ByVal value As String)
			_ValorInventario = value
		End Set
	End Property

	Public Property ValorRGISvsSAIC As String
		Get
			Return _ValorRGISvsSAIC
		End Get
		Set(ByVal value As String)
			_ValorRGISvsSAIC = value
		End Set
	End Property

	Public Property PisodeVenta1 As String
		Get
			Return _PisodeVenta1
		End Get
		Set(ByVal value As String)
			_PisodeVenta1 = value
		End Set
	End Property

	Public Property Bodega1 As String
		Get
			Return _Bodega1
		End Get
		Set(ByVal value As String)
			_Bodega1 = value
		End Set
	End Property

	Public Property inventarioPV1 As String
		Get
			Return _inventarioPV1
		End Get
		Set(ByVal value As String)
			_inventarioPV1 = value
		End Set
	End Property

	Public Property inventarioB1 As String
		Get
			Return _inventarioB1
		End Get
		Set(ByVal value As String)
			_inventarioB1 = value
		End Set
	End Property

	Public Property PisodeVenta2 As String
		Get
			Return _PisodeVenta2
		End Get
		Set(ByVal value As String)
			_PisodeVenta2 = value
		End Set
	End Property

	Public Property Bodega2 As String
		Get
			Return _Bodega2
		End Get
		Set(ByVal value As String)
			_Bodega2 = value
		End Set
	End Property

	Public Property inventarioPV2 As String
		Get
			Return _inventarioPV2
		End Get
		Set(ByVal value As String)
			_inventarioPV2 = value
		End Set
	End Property

	Public Property inventarioB2 As String
		Get
			Return _inventarioB2
		End Get
		Set(ByVal value As String)
			_inventarioB2 = value
		End Set
	End Property

	Public Property Programado As String
		Get
			Return _Programado
		End Get
		Set(ByVal value As String)
			_Programado = value
		End Set
	End Property

	Public Property Finales As String
		Get
			Return _Finales
		End Get
		Set(ByVal value As String)
			_Finales = value
		End Set
	End Property

	Public Property DifenContadores As String
		Get
			Return _DifenContadores
		End Get
		Set(ByVal value As String)
			_DifenContadores = value
		End Set
	End Property

	Public Property TotalCambios As String
		Get
			Return _TotalCambios
		End Get
		Set(ByVal value As String)
			_TotalCambios = value
		End Set
	End Property

	Public Property Confiabilidad As String
		Get
			Return _Confiabilidad
		End Get
		Set(ByVal value As String)
			_Confiabilidad = value
		End Set
	End Property

	Public Property V1_Confiabilidad As String
		Get
			Return _V1_Confiabilidad
		End Get
		Set(ByVal value As String)
			_V1_Confiabilidad = value
		End Set
	End Property

	Public Property V2_Adherenciaaprocedimientos As String
		Get
			Return _V2_Adherenciaaprocedimientos
		End Get
		Set(ByVal value As String)
			_V2_Adherenciaaprocedimientos = value
		End Set
	End Property

	Public Property V3_Eficiencia As String
		Get
			Return _V3_Eficiencia
		End Get
		Set(ByVal value As String)
			_V3_Eficiencia = value
		End Set
	End Property

	Public Property V4_Cortesiaycooperación As String
		Get
			Return _V4_Cortesiaycooperación
		End Get
		Set(ByVal value As String)
			_V4_Cortesiaycooperación = value
		End Set
	End Property

	Public Property V5_Apariencia As String
		Get
			Return _V5_Apariencia
		End Get
		Set(ByVal value As String)
			_V5_Apariencia = value
		End Set
	End Property

	Public Property V6_Calificaciónalsupervisor As String
		Get
			Return _V6_Calificaciónalsupervisor
		End Get
		Set(ByVal value As String)
			_V6_Calificaciónalsupervisor = value
		End Set
	End Property

	Public Property V7_EvaluaciónglobalaRGIS As String
		Get
			Return _V7_EvaluaciónglobalaRGIS
		End Get
		Set(ByVal value As String)
			_V7_EvaluaciónglobalaRGIS = value
		End Set
	End Property

	Public Property Observaciones As String
		Get
			Return _Observaciones
		End Get
		Set(ByVal value As String)
			_Observaciones = value
		End Set
	End Property

	Public Property InicioConteo_RGIS As String
		Get
			Return _InicioConteo_RGIS
		End Get
		Set(ByVal value As String)
			_InicioConteo_RGIS = value
		End Set
	End Property

	Public Property TerminodeConteo_RGIS As String
		Get
			Return _TerminodeConteo_RGIS
		End Get
		Set(ByVal value As String)
			_TerminodeConteo_RGIS = value
		End Set
	End Property

	Public Property Estatus As String
		Get
			Return _Estatus
		End Get
		Set(ByVal value As String)
			_Estatus = value
		End Set
	End Property

	Public Property CierredeInventario_RGIS As String
		Get
			Return _CierredeInventario_RGIS
		End Get
		Set(ByVal value As String)
			_CierredeInventario_RGIS = value
		End Set
	End Property

	Public Property Total_HrasConteo As String
		Get
			Return _Total_HrasConteo
		End Get
		Set(ByVal value As String)
			_Total_HrasConteo = value
		End Set
	End Property

	Public Property ValidacionPV As String
		Get
			Return _ValidacionPV
		End Get
		Set(ByVal value As String)
			_ValidacionPV = value
		End Set
	End Property

	Public Property TotaltiempoRGIS As String
		Get
			Return _TotaltiempoRGIS
		End Get
		Set(ByVal value As String)
			_TotaltiempoRGIS = value
		End Set
	End Property

	Public Property InicioprocesoNoCatalogados As String
		Get
			Return _InicioprocesoNoCatalogados
		End Get
		Set(ByVal value As String)
			_InicioprocesoNoCatalogados = value
		End Set
	End Property

	Public Property Inicioprocesogeneracion8020 As String
		Get
			Return _Inicioprocesogeneracion8020
		End Get
		Set(ByVal value As String)
			_Inicioprocesogeneracion8020 = value
		End Set
	End Property

	Public Property Inicioprocesovalidación8020 As String
		Get
			Return _Inicioprocesovalidación8020
		End Get
		Set(ByVal value As String)
			_Inicioprocesovalidación8020 = value
		End Set
	End Property

	Public Property Inicioprocesocorreccionesfinales As String
		Get
			Return _Inicioprocesocorreccionesfinales
		End Get
		Set(ByVal value As String)
			_Inicioprocesocorreccionesfinales = value
		End Set
	End Property

	Public Property TotaltiempoNoCatalogados As String
		Get
			Return _TotaltiempoNoCatalogados
		End Get
		Set(ByVal value As String)
			_TotaltiempoNoCatalogados = value
		End Set
	End Property

	Public Property Totaltiempogeneracion8020 As String
		Get
			Return _Totaltiempogeneracion8020
		End Get
		Set(ByVal value As String)
			_Totaltiempogeneracion8020 = value
		End Set
	End Property

	Public Property Totaltiempovalidacion8020 As String
		Get
			Return _Totaltiempovalidacion8020
		End Get
		Set(ByVal value As String)
			_Totaltiempovalidacion8020 = value
		End Set
	End Property

	Public Property Totaltiempocorreccionesfinales As String
		Get
			Return _Totaltiempocorreccionesfinales
		End Get
		Set(ByVal value As String)
			_Totaltiempocorreccionesfinales = value
		End Set
	End Property

	Public Property CierreInventarioGeneral As String
		Get
			Return _CierreInventarioGeneral
		End Get
		Set(ByVal value As String)
			_CierreInventarioGeneral = value
		End Set
	End Property

	Public Property TiempoTotalInventario As String
		Get
			Return _TiempoTotalInventario
		End Get
		Set(ByVal value As String)
			_TiempoTotalInventario = value
		End Set
	End Property

	Public Property Tarifa2015 As String
		Get
			Return _Tarifa2015
		End Get
		Set(ByVal value As String)
			_Tarifa2015 = value
		End Set
	End Property

	Public Property CostoHonorario As String
		Get
			Return _CostoHonorario
		End Get
		Set(ByVal value As String)
			_CostoHonorario = value
		End Set
	End Property

	Public Property Subtotal As String
		Get
			Return _Subtotal
		End Get
		Set(ByVal value As String)
			_Subtotal = value
		End Set
	End Property

	Public Property TasaIva As String
		Get
			Return _TasaIva
		End Get
		Set(ByVal value As String)
			_TasaIva = value
		End Set
	End Property

	Public Property Iva As String
		Get
			Return _Iva
		End Get
		Set(ByVal value As String)
			_Iva = value
		End Set
	End Property

	Public Property Total As String
		Get
			Return _Total
		End Get
		Set(ByVal value As String)
			_Total = value
		End Set
	End Property

	Public Property AcumuladoMerma As String
		Get
			Return _AcumuladoMerma
		End Get
		Set(ByVal value As String)
			_AcumuladoMerma = value
		End Set
	End Property

	Public Property ResultadoInv As String
		Get
			Return _ResultadoInv
		End Get
		Set(ByVal value As String)
			_ResultadoInv = value
		End Set
	End Property

	Public Property TotalMerma As String
		Get
			Return _TotalMerma
		End Get
		Set(ByVal value As String)
			_TotalMerma = value
		End Set
	End Property

	Public Property CostoVentas As String
		Get
			Return _CostoVentas
		End Get
		Set(ByVal value As String)
			_CostoVentas = value
		End Set
	End Property

	Public Property Porcentaje As String
		Get
			Return _Porcentaje
		End Get
		Set(ByVal value As String)
			_Porcentaje = value
		End Set
	End Property

	Public Property Accion As TipoAccion
		Get
			Return _Accion
		End Get
		Set(ByVal value As TipoAccion)
			_Accion = value
		End Set
	End Property

#End Region

#Region "METODOS"
	Public Function INSERTAR() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Tienda", Tienda)
		CnnSQL.AgregarParametros("@Location", Location)
		CnnSQL.AgregarParametros("@Dist", Dist)
		CnnSQL.AgregarParametros("@Zona", Zona)
		CnnSQL.AgregarParametros("@Subdirección", Subdirección)
		CnnSQL.AgregarParametros("@FechaInventario", FechaInventario)
		CnnSQL.AgregarParametros("@ResponsableInventario", ResponsableInventario)
		CnnSQL.AgregarParametros("@Tipo", Tipo)
		CnnSQL.AgregarParametros("@HoradeInicioprogramada", HoradeInicioprogramada)
		CnnSQL.AgregarParametros("@Inventarioenunidades", Inventarioenunidades)
		CnnSQL.AgregarParametros("@ValorInventario", ValorInventario)
		CnnSQL.AgregarParametros("@ValorRGISvsSAIC", ValorRGISvsSAIC)
		CnnSQL.AgregarParametros("@PisodeVenta", PisodeVenta1)
		CnnSQL.AgregarParametros("@Bodega", Bodega1)
		CnnSQL.AgregarParametros("@inventarioPV", inventarioPV1)
		CnnSQL.AgregarParametros("@inventarioB", inventarioB1)
		CnnSQL.AgregarParametros("@PisodeVenta2", PisodeVenta2)
		CnnSQL.AgregarParametros("@Bodega2", Bodega2)
		CnnSQL.AgregarParametros("@inventarioPV2", inventarioPV2)
		CnnSQL.AgregarParametros("@inventarioB2", inventarioB2)
		CnnSQL.AgregarParametros("@Programado", Programado)
		CnnSQL.AgregarParametros("@Finales", Finales)
		CnnSQL.AgregarParametros("@DifenContadores", DifenContadores)
		CnnSQL.AgregarParametros("@TotalCambios", TotalCambios)
		CnnSQL.AgregarParametros("@Confiabilidad", Confiabilidad)
		CnnSQL.AgregarParametros("@V1_Confiabilidad", V1_Confiabilidad)
		CnnSQL.AgregarParametros("@V2_Adherenciaaprocedimientos", V2_Adherenciaaprocedimientos)
		CnnSQL.AgregarParametros("@V3_Eficiencia", V3_Eficiencia)
		CnnSQL.AgregarParametros("@V4_Cortesiaycooperación", V4_Cortesiaycooperación)
		CnnSQL.AgregarParametros("@V5_Apariencia", V5_Apariencia)
		CnnSQL.AgregarParametros("@V6_Calificaciónalsupervisor", V6_Calificaciónalsupervisor)
		CnnSQL.AgregarParametros("@V7_EvaluaciónglobalaRGIS", V7_EvaluaciónglobalaRGIS)
		CnnSQL.AgregarParametros("@Observaciones", Observaciones)
		CnnSQL.AgregarParametros("@InicioConteo_RGIS", InicioConteo_RGIS)
		CnnSQL.AgregarParametros("@TerminodeConteo_RGIS", TerminodeConteo_RGIS)
		CnnSQL.AgregarParametros("@Estatus", Estatus)
		CnnSQL.AgregarParametros("@CierredeInventario_RGIS", CierredeInventario_RGIS)
		CnnSQL.AgregarParametros("@Total_HrasConteo", Total_HrasConteo)
		CnnSQL.AgregarParametros("@ValidacionPV", ValidacionPV)
		CnnSQL.AgregarParametros("@TotaltiempoRGIS", TotaltiempoRGIS)
		CnnSQL.AgregarParametros("@InicioprocesoNoCatalogados", InicioprocesoNoCatalogados)
		CnnSQL.AgregarParametros("@Inicioprocesogeneracion8020", Inicioprocesogeneracion8020)
		CnnSQL.AgregarParametros("@Inicioprocesovalidación8020", Inicioprocesovalidación8020)
		CnnSQL.AgregarParametros("@Inicioprocesocorreccionesfinales", Inicioprocesocorreccionesfinales)
		CnnSQL.AgregarParametros("@TotaltiempoNoCatalogados", TotaltiempoNoCatalogados)
		CnnSQL.AgregarParametros("@Totaltiempogeneracion8020", Totaltiempogeneracion8020)
		CnnSQL.AgregarParametros("@Totaltiempovalidacion8020", Totaltiempovalidacion8020)
		CnnSQL.AgregarParametros("@Totaltiempocorreccionesfinales", Totaltiempocorreccionesfinales)
		CnnSQL.AgregarParametros("@CierreInventarioGeneral", CierreInventarioGeneral)
		CnnSQL.AgregarParametros("@TiempoTotalInventario", TiempoTotalInventario)
		CnnSQL.AgregarParametros("@Tarifa2015", Tarifa2015)
		CnnSQL.AgregarParametros("@CostoHonorario", CostoHonorario)
		CnnSQL.AgregarParametros("@Subtotal", Subtotal)
		CnnSQL.AgregarParametros("@TasaIva", TasaIva)
		CnnSQL.AgregarParametros("@Iva", Iva)
		CnnSQL.AgregarParametros("@Total", Total)
		CnnSQL.AgregarParametros("@AcumuladoMerma", AcumuladoMerma)
		CnnSQL.AgregarParametros("@ResultadoInv", ResultadoInv)
		CnnSQL.AgregarParametros("@TotalMerma", TotalMerma)
		CnnSQL.AgregarParametros("@CostoVentas", CostoVentas)
		CnnSQL.AgregarParametros("@Porcentaje", Porcentaje)
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetInsert)
		Try
			CnnSQL.ejecutaSentenciaNonQuery(procInv_externo, SQLConexion.CnnSql.TipoDato.StoredProcedure, _Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function CONSULTA() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetSelect)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procInv_externo)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "CONSTRUCTORES"
	Sub New(ConexionString As String)
		_Cnn = ConexionString
	End Sub
	Sub New()
		_Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		GetInsert = 1
		GetSelect = 2
	End Enum
#End Region
End Class
