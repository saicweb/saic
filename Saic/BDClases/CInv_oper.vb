﻿Public Class CInv_oper
	Inherits CRespuesta
#Region "VARIABLES"
	Private _Tienda As String
	Private _Distrito As String
	Private _Zona As String
	Private _Subdirección As String
	Private _NombrePP As String
	Private _NombreADI As String
	Private _Diadeinventario As String
	Private _FechadeInventario As String
	Private _Contadores As String
	Private _Validadores As String
	Private _Mesadecontrol As String
	Private _Ctd_Marbetes As String
	Private _PisodeVenta1 As String
	Private _Bodega1 As String
	Private _inventarioPV1 As String
	Private _inventarioB1 As String
	Private _PisodeVenta2 As String
	Private _Bodega2 As String
	Private _inventarioPV2 As String
	Private _inventarioB2 As String
	Private _V1_PreparacionBodega As String
	Private _V2_ManejoSAIC As String
	Private _V3_PreparacionPV As String
	Private _V4_Organizacióndelconteo As String
	Private _V5_Controlyordenmesa As String
	Private _Observaciones As String
	Private _InicioConteo As String
	Private _Finconteo As String
	Private _InicioValidación As String
	Private _FinValidación As String
	Private _Iniciocorrecciones As String
	Private _FinCorrecciones As String
	Private _Totalconteo As String
	Private _Totalvalidaciones As String
	Private _Totalcorrecciones As String
	Private _TotalprocesoPV As String
	Private _Inicioprocesogeneracion8020 As String
	Private _Inicioprocesovalidación8020 As String
	Private _Inicioprocesocorreccionesfinales As String
	Private _Totaltiempogeneracion8020 As String
	Private _Totaltiempovalidacion8020 As String
	Private _Totaltiempocorreccionesfinales As String
	Private _CierreInventarioGeneral As String
	Private _TiempoTotalInventario As String
	Private _AcumuladoMerma As String
	Private _ResultadoInv As String
	Private _TotalMerma As String
	Private _CostoVentas As String
	Private _Porcentaje As String
	Private _Accion As String
	Private _Cnn As String
#End Region

#Region "PROPIEDADES"
	Public Property Tienda As String
		Get
			Return _Tienda
		End Get
		Set(ByVal value As String)
			_Tienda = value
		End Set
	End Property

	Public Property Distrito As String
		Get
			Return _Distrito
		End Get
		Set(ByVal value As String)
			_Distrito = value
		End Set
	End Property

	Public Property Zona As String
		Get
			Return _Zona
		End Get
		Set(ByVal value As String)
			_Zona = value
		End Set
	End Property

	Public Property Subdirección As String
		Get
			Return _Subdirección
		End Get
		Set(ByVal value As String)
			_Subdirección = value
		End Set
	End Property

	Public Property NombrePP As String
		Get
			Return _NombrePP
		End Get
		Set(ByVal value As String)
			_NombrePP = value
		End Set
	End Property

	Public Property NombreADI As String
		Get
			Return _NombreADI
		End Get
		Set(ByVal value As String)
			_NombreADI = value
		End Set
	End Property

	Public Property Diadeinventario As String
		Get
			Return _Diadeinventario
		End Get
		Set(ByVal value As String)
			_Diadeinventario = value
		End Set
	End Property

	Public Property FechadeInventario As String
		Get
			Return _FechadeInventario
		End Get
		Set(ByVal value As String)
			_FechadeInventario = value
		End Set
	End Property

	Public Property Contadores As String
		Get
			Return _Contadores
		End Get
		Set(ByVal value As String)
			_Contadores = value
		End Set
	End Property

	Public Property Validadores As String
		Get
			Return _Validadores
		End Get
		Set(ByVal value As String)
			_Validadores = value
		End Set
	End Property

	Public Property Mesadecontrol As String
		Get
			Return _Mesadecontrol
		End Get
		Set(ByVal value As String)
			_Mesadecontrol = value
		End Set
	End Property

	Public Property Ctd_Marbetes As String
		Get
			Return _Ctd_Marbetes
		End Get
		Set(ByVal value As String)
			_Ctd_Marbetes = value
		End Set
	End Property

	Public Property PisodeVenta1 As String
		Get
			Return _PisodeVenta1
		End Get
		Set(ByVal value As String)
			_PisodeVenta1 = value
		End Set
	End Property

	Public Property Bodega1 As String
		Get
			Return _Bodega1
		End Get
		Set(ByVal value As String)
			_Bodega1 = value
		End Set
	End Property

	Public Property inventarioPV1 As String
		Get
			Return _inventarioPV1
		End Get
		Set(ByVal value As String)
			_inventarioPV1 = value
		End Set
	End Property

	Public Property inventarioB1 As String
		Get
			Return _inventarioB1
		End Get
		Set(ByVal value As String)
			_inventarioB1 = value
		End Set
	End Property

	Public Property PisodeVenta2 As String
		Get
			Return _PisodeVenta2
		End Get
		Set(ByVal value As String)
			_PisodeVenta2 = value
		End Set
	End Property

	Public Property Bodega2 As String
		Get
			Return _Bodega2
		End Get
		Set(ByVal value As String)
			_Bodega2 = value
		End Set
	End Property

	Public Property inventarioPV2 As String
		Get
			Return _inventarioPV2
		End Get
		Set(ByVal value As String)
			_inventarioPV2 = value
		End Set
	End Property

	Public Property inventarioB2 As String
		Get
			Return _inventarioB2
		End Get
		Set(ByVal value As String)
			_inventarioB2 = value
		End Set
	End Property

	Public Property V1_PreparacionBodega As String
		Get
			Return _V1_PreparacionBodega
		End Get
		Set(ByVal value As String)
			_V1_PreparacionBodega = value
		End Set
	End Property

	Public Property V2_ManejoSAIC As String
		Get
			Return _V2_ManejoSAIC
		End Get
		Set(ByVal value As String)
			_V2_ManejoSAIC = value
		End Set
	End Property

	Public Property V3_PreparacionPV As String
		Get
			Return _V3_PreparacionPV
		End Get
		Set(ByVal value As String)
			_V3_PreparacionPV = value
		End Set
	End Property

	Public Property V4_Organizacióndelconteo As String
		Get
			Return _V4_Organizacióndelconteo
		End Get
		Set(ByVal value As String)
			_V4_Organizacióndelconteo = value
		End Set
	End Property

	Public Property V5_Controlyordenmesa As String
		Get
			Return _V5_Controlyordenmesa
		End Get
		Set(ByVal value As String)
			_V5_Controlyordenmesa = value
		End Set
	End Property

	Public Property Observaciones As String
		Get
			Return _Observaciones
		End Get
		Set(ByVal value As String)
			_Observaciones = value
		End Set
	End Property

	Public Property InicioConteo As String
		Get
			Return _InicioConteo
		End Get
		Set(ByVal value As String)
			_InicioConteo = value
		End Set
	End Property

	Public Property Finconteo As String
		Get
			Return _Finconteo
		End Get
		Set(ByVal value As String)
			_Finconteo = value
		End Set
	End Property

	Public Property InicioValidación As String
		Get
			Return _InicioValidación
		End Get
		Set(ByVal value As String)
			_InicioValidación = value
		End Set
	End Property

	Public Property FinValidación As String
		Get
			Return _FinValidación
		End Get
		Set(ByVal value As String)
			_FinValidación = value
		End Set
	End Property

	Public Property Iniciocorrecciones As String
		Get
			Return _Iniciocorrecciones
		End Get
		Set(ByVal value As String)
			_Iniciocorrecciones = value
		End Set
	End Property

	Public Property FinCorrecciones As String
		Get
			Return _FinCorrecciones
		End Get
		Set(ByVal value As String)
			_FinCorrecciones = value
		End Set
	End Property

	Public Property Totalconteo As String
		Get
			Return _Totalconteo
		End Get
		Set(ByVal value As String)
			_Totalconteo = value
		End Set
	End Property

	Public Property Totalvalidaciones As String
		Get
			Return _Totalvalidaciones
		End Get
		Set(ByVal value As String)
			_Totalvalidaciones = value
		End Set
	End Property

	Public Property Totalcorrecciones As String
		Get
			Return _Totalcorrecciones
		End Get
		Set(ByVal value As String)
			_Totalcorrecciones = value
		End Set
	End Property

	Public Property TotalprocesoPV As String
		Get
			Return _TotalprocesoPV
		End Get
		Set(ByVal value As String)
			_TotalprocesoPV = value
		End Set
	End Property

	Public Property Inicioprocesogeneracion8020 As String
		Get
			Return _Inicioprocesogeneracion8020
		End Get
		Set(ByVal value As String)
			_Inicioprocesogeneracion8020 = value
		End Set
	End Property

	Public Property Inicioprocesovalidación8020 As String
		Get
			Return _Inicioprocesovalidación8020
		End Get
		Set(ByVal value As String)
			_Inicioprocesovalidación8020 = value
		End Set
	End Property

	Public Property Inicioprocesocorreccionesfinales As String
		Get
			Return _Inicioprocesocorreccionesfinales
		End Get
		Set(ByVal value As String)
			_Inicioprocesocorreccionesfinales = value
		End Set
	End Property

	Public Property Totaltiempogeneracion8020 As String
		Get
			Return _Totaltiempogeneracion8020
		End Get
		Set(ByVal value As String)
			_Totaltiempogeneracion8020 = value
		End Set
	End Property

	Public Property Totaltiempovalidacion8020 As String
		Get
			Return _Totaltiempovalidacion8020
		End Get
		Set(ByVal value As String)
			_Totaltiempovalidacion8020 = value
		End Set
	End Property

	Public Property Totaltiempocorreccionesfinales As String
		Get
			Return _Totaltiempocorreccionesfinales
		End Get
		Set(ByVal value As String)
			_Totaltiempocorreccionesfinales = value
		End Set
	End Property

	Public Property CierreInventarioGeneral As String
		Get
			Return _CierreInventarioGeneral
		End Get
		Set(ByVal value As String)
			_CierreInventarioGeneral = value
		End Set
	End Property

	Public Property TiempoTotalInventario As String
		Get
			Return _TiempoTotalInventario
		End Get
		Set(ByVal value As String)
			_TiempoTotalInventario = value
		End Set
	End Property

	Public Property AcumuladoMerma As String
		Get
			Return _AcumuladoMerma
		End Get
		Set(ByVal value As String)
			_AcumuladoMerma = value
		End Set
	End Property

	Public Property ResultadoInv As String
		Get
			Return _ResultadoInv
		End Get
		Set(ByVal value As String)
			_ResultadoInv = value
		End Set
	End Property

	Public Property TotalMerma As String
		Get
			Return _TotalMerma
		End Get
		Set(ByVal value As String)
			_TotalMerma = value
		End Set
	End Property

	Public Property CostoVentas As String
		Get
			Return _CostoVentas
		End Get
		Set(ByVal value As String)
			_CostoVentas = value
		End Set
	End Property

	Public Property Porcentaje As String
		Get
			Return _Porcentaje
		End Get
		Set(ByVal value As String)
			_Porcentaje = value
		End Set
	End Property

	Public Property Accion As TipoAccion
		Get
			Return _Accion
		End Get
		Set(ByVal value As TipoAccion)
			_Accion = value
		End Set
	End Property

#End Region

#Region "METODOS"
	Public Function INSERTAR() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Tienda", Tienda)
		CnnSQL.AgregarParametros("@Distrito", Distrito)
		CnnSQL.AgregarParametros("@Zona", Zona)
		CnnSQL.AgregarParametros("@Subdirección", Subdirección)
		CnnSQL.AgregarParametros("@NombrePP", NombrePP)
		CnnSQL.AgregarParametros("@NombreADI", NombreADI)
		CnnSQL.AgregarParametros("@Diadeinventario", Diadeinventario)
		CnnSQL.AgregarParametros("@FechadeInventario", FechadeInventario)
		CnnSQL.AgregarParametros("@Contadores", Contadores)
		CnnSQL.AgregarParametros("@Validadores", Validadores)
		CnnSQL.AgregarParametros("@Mesadecontrol", Mesadecontrol)
		CnnSQL.AgregarParametros("@Ctd_Marbetes", Ctd_Marbetes)
		CnnSQL.AgregarParametros("@PisodeVenta", PisodeVenta1)
		CnnSQL.AgregarParametros("@Bodega", Bodega1)
		CnnSQL.AgregarParametros("@inventarioPV", inventarioPV1)
		CnnSQL.AgregarParametros("@inventarioB", inventarioB1)
		CnnSQL.AgregarParametros("@PisodeVenta2", PisodeVenta2)
		CnnSQL.AgregarParametros("@Bodega2", Bodega2)
		CnnSQL.AgregarParametros("@inventarioPV2", inventarioPV2)
		CnnSQL.AgregarParametros("@inventarioB2", inventarioB2)
		CnnSQL.AgregarParametros("@V1_PreparacionBodega", V1_PreparacionBodega)
		CnnSQL.AgregarParametros("@V2_ManejoSAIC", V2_ManejoSAIC)
		CnnSQL.AgregarParametros("@V3_PreparacionPV", V3_PreparacionPV)
		CnnSQL.AgregarParametros("@V4_Organizacióndelconteo", V4_Organizacióndelconteo)
		CnnSQL.AgregarParametros("@V5_Controlyordenmesa", V5_Controlyordenmesa)
		CnnSQL.AgregarParametros("@Observaciones", Observaciones)
		CnnSQL.AgregarParametros("@InicioConteo", InicioConteo)
		CnnSQL.AgregarParametros("@Finconteo", Finconteo)
		CnnSQL.AgregarParametros("@InicioValidación", InicioValidación)
		CnnSQL.AgregarParametros("@FinValidación", FinValidación)
		CnnSQL.AgregarParametros("@Iniciocorrecciones", Iniciocorrecciones)
		CnnSQL.AgregarParametros("@FinCorrecciones", FinCorrecciones)
		CnnSQL.AgregarParametros("@Totalconteo", Totalconteo)
		CnnSQL.AgregarParametros("@Totalvalidaciones", Totalvalidaciones)
		CnnSQL.AgregarParametros("@Totalcorrecciones", Totalcorrecciones)
		CnnSQL.AgregarParametros("@TotalprocesoPV", TotalprocesoPV)
		CnnSQL.AgregarParametros("@Inicioprocesogeneracion8020", Inicioprocesogeneracion8020)
		CnnSQL.AgregarParametros("@Inicioprocesovalidación8020", Inicioprocesovalidación8020)
		CnnSQL.AgregarParametros("@Inicioprocesocorreccionesfinales", Inicioprocesocorreccionesfinales)
		CnnSQL.AgregarParametros("@Totaltiempogeneracion8020", Totaltiempogeneracion8020)
		CnnSQL.AgregarParametros("@Totaltiempovalidacion8020", Totaltiempovalidacion8020)
		CnnSQL.AgregarParametros("@Totaltiempocorreccionesfinales", Totaltiempocorreccionesfinales)
		CnnSQL.AgregarParametros("@CierreInventarioGeneral", CierreInventarioGeneral)
		CnnSQL.AgregarParametros("@TiempoTotalInventario", TiempoTotalInventario)
		CnnSQL.AgregarParametros("@AcumuladoMerma", AcumuladoMerma)
		CnnSQL.AgregarParametros("@ResultadoInv", ResultadoInv)
		CnnSQL.AgregarParametros("@TotalMerma", TotalMerma)
		CnnSQL.AgregarParametros("@CostoVentas", CostoVentas)
		CnnSQL.AgregarParametros("@Porcentaje", Porcentaje)
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetInsert)
		Try
			CnnSQL.ejecutaSentenciaNonQuery(procInv_oper, SQLConexion.CnnSql.TipoDato.StoredProcedure, _Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function CONSULTA() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetSelect)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procInv_oper)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "CONSTRUCTORES"
	Sub New(ConexionString As String)
		_Cnn = ConexionString
	End Sub
	Sub New()
		_Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		GetInsert = 1
		GetSelect = 2
	End Enum
#End Region
End Class
