﻿Public Class CUsuarios
	Inherits CRespuesta

#Region "VARIABLES"
	Private _Accion As String
#End Region

#Region "PROPIEDADES"
	Public Property Accion As TipoAccion
		Get
			Return _Accion
		End Get
		Set(ByVal value As TipoAccion)
			_Accion = value
		End Set
	End Property

	Public Property Cnn As String

	Public Property IdUsuario As String

	Public Property NombreUsuario As String

	Public Property Password As String

	Public Property TipoUsuario As String

	Public Property IdTienda As String

	Public Property TransferirInformacionPreconteoExternoAlSAIC As String

	Public Property SubirArchivoAccessControl As String

	Public Property PermisoReversa As String

	Public Property Password2 As String

	Public Property Activo() As String
#End Region

#Region "METODOS"

	Public Function Ejecuta_usuarios() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", _Accion)
		CnnSQL.AgregarParametros("@IdUsuario", IdUsuario)
		CnnSQL.AgregarParametros("@NombreUsuario", NombreUsuario)
		CnnSQL.AgregarParametros("@Password", Password)
		CnnSQL.AgregarParametros("@TipoUsuario", TipoUsuario)
		CnnSQL.AgregarParametros("@IdTienda", IdTienda)
		CnnSQL.AgregarParametros("@TransferirInformacionPreconteoExternoAlSAIC", TransferirInformacionPreconteoExternoAlSAIC)
		CnnSQL.AgregarParametros("@SubirArchivoAccessControl", SubirArchivoAccessControl)
		CnnSQL.AgregarParametros("@PermisoReversa", PermisoReversa)
		CnnSQL.AgregarParametros("@Password2", Password2)
		CnnSQL.AgregarParametros("@Activo", Activo)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, procusuarios)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CUsuarios - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function loginUsuario() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@IdUsuario", IdUsuario)
		CnnSQL.AgregarParametros("@contrasena", Password)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, procLogin)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CUsuarios - " & ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "Constructores"
	Sub New(ConexionString As String)
		Cnn = ConexionString
	End Sub
	Sub New()
		Cnn = CReglasNegocio.CnnSeguridad
	End Sub
#End Region

#Region "TipoAccion"
	''' <summary>
	''' Accion que se ejecutara dentro del  SP
	''' </summary>
	''' <remarks></remarks>
	Public Enum TipoAccion
		''' <summary>
		''' Permite obtener la Informacion del usuario mediante el parametro idusuario se envia -1 para traer Todos
		''' </summary>
		GetSelect = 1
		''' <summary>
		''' Permite Insertar los nuevos usuarios
		''' </summary>
		GetInsert = 2
		''' <summary>
		''' Permite Modificar los registros del usuario
		''' </summary>
		''' <remarks></remarks>
		GetUpdate = 3
		''' <summary>
		''' Permite dar baja logica un registro mediante el parametro IdUsuario
		''' </summary>
		GetDelete = 4
		''' <summary>
		''' Permite Modificar la contraseña del usuario
		''' </summary>
		UpdateContrasena = 5
		''' <summary>
		''' Permite Cambiar el valor IngSesion de la tabla para reiniciar el cambio de Password
		''' </summary>
		UpdateIngSesion = 6
		''' <summary>
		''' Permite Obtener el correoElectronico,NombreUsuario,contrasena para enviar por correo
		''' </summary>
		GetCorreo = 7
		''' <summary>
		''' Permite Obtener los tipo de usuarios
		''' </summary>
		GetTipo = 8
		''' <summary>
		''' Permite Validar si existe el NoEmpleado o idusuario
		''' </summary>
		ExistEmpleado = 9

	End Enum
#End Region
End Class
