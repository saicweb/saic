﻿Public Class CNegocioTienda
	Inherits CRespuesta

#Region "PROPIEDADES"
	Public Property Cnn As String
	Public Property Accion As String
	Public Property UnidadNegocio As String
	Public Property Tienda As String
#End Region

#Region "METODOS"
	Public Function LlenaUnidadNegocio() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@UnidadNegocio", UnidadNegocio)
		CnnSQL.AgregarParametros("@Tienda", Tienda)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, procUnidadNegocio)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CNegocioTienda - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function LlenaTienda() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@UnidadNegocio", UnidadNegocio)
		CnnSQL.AgregarParametros("@Tienda", Tienda)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, procUnidadNegocio)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CNegocioTienda - " & ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function Cedis() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@Tienda", Tienda)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, procUnidadNegocio)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CNegocioTienda - " & ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "Constructores"
	Sub New(ConexionString As String)
		Cnn = ConexionString
	End Sub
	Sub New()
		Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		''' <summary>
		''' Permite obtener la información de la tabla UnidadNegocio
		''' </summary>
		GetUnidadNegocio = 1
		''' <summary>
		''' Permite obtener la información de la tabla de tiendas dependiendo de la unidad de negocio que se escogió
		''' </summary>
		GetTienda = 2
	End Enum
#End Region

End Class
