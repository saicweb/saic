﻿Public Class CIncidencias
	Inherits CRespuesta
#Region "VARIABLES"
	Private _Tienda As String
	Private _Fecha As String
	Private _Gerente As String
	Private _Especialista As String
	Private _TotalIncidencias As String
	Private _V1_1 As String
	Private _V1_2 As String
	Private _V1_3 As String
	Private _V1_4 As String
	Private _V1_5 As String
	Private _V1_6 As String
	Private _Observaciones_Area_1 As String
	Private _V2_1 As String
	Private _V2_2 As String
	Private _V2_3 As String
	Private _V2_4 As String
	Private _V2_5 As String
	Private _Observaciones_Area_2 As String
	Private _V3_1 As String
	Private _V3_2 As String
	Private _V3_3 As String
	Private _V3_4 As String
	Private _V3_5 As String
	Private _V3_6 As String
	Private _V3_7 As String
	Private _Observaciones_Area_3 As String
	Private _V4_1 As String
	Private _V4_2 As String
	Private _V4_3 As String
	Private _V4_4 As String
	Private _V4_5 As String
	Private _Accion As String
	Private _Cnn As String
#End Region

#Region "PROPIEDADES"
	Public Property Tienda As String
		Get
			Return _Tienda
		End Get
		Set(ByVal value As String)
			_Tienda = value
		End Set
	End Property

	Public Property Fecha As String
		Get
			Return _Fecha
		End Get
		Set(ByVal value As String)
			_Fecha = value
		End Set
	End Property

	Public Property Gerente As String
		Get
			Return _Gerente
		End Get
		Set(ByVal value As String)
			_Gerente = value
		End Set
	End Property

	Public Property Especialista As String
		Get
			Return _Especialista
		End Get
		Set(ByVal value As String)
			_Especialista = value
		End Set
	End Property

	Public Property TotalIncidencias As String
		Get
			Return _TotalIncidencias
		End Get
		Set(ByVal value As String)
			_TotalIncidencias = value
		End Set
	End Property

	Public Property V1_1 As String
		Get
			Return _V1_1
		End Get
		Set(ByVal value As String)
			_V1_1 = value
		End Set
	End Property

	Public Property V1_2 As String
		Get
			Return _V1_2
		End Get
		Set(ByVal value As String)
			_V1_2 = value
		End Set
	End Property

	Public Property V1_3 As String
		Get
			Return _V1_3
		End Get
		Set(ByVal value As String)
			_V1_3 = value
		End Set
	End Property

	Public Property V1_4 As String
		Get
			Return _V1_4
		End Get
		Set(ByVal value As String)
			_V1_4 = value
		End Set
	End Property

	Public Property V1_5 As String
		Get
			Return _V1_5
		End Get
		Set(ByVal value As String)
			_V1_5 = value
		End Set
	End Property

	Public Property V1_6 As String
		Get
			Return _V1_6
		End Get
		Set(ByVal value As String)
			_V1_6 = value
		End Set
	End Property

	Public Property Observaciones_Area_1 As String
		Get
			Return _Observaciones_Area_1
		End Get
		Set(ByVal value As String)
			_Observaciones_Area_1 = value
		End Set
	End Property

	Public Property V2_1 As String
		Get
			Return _V2_1
		End Get
		Set(ByVal value As String)
			_V2_1 = value
		End Set
	End Property

	Public Property V2_2 As String
		Get
			Return _V2_2
		End Get
		Set(ByVal value As String)
			_V2_2 = value
		End Set
	End Property

	Public Property V2_3 As String
		Get
			Return _V2_3
		End Get
		Set(ByVal value As String)
			_V2_3 = value
		End Set
	End Property

	Public Property V2_4 As String
		Get
			Return _V2_4
		End Get
		Set(ByVal value As String)
			_V2_4 = value
		End Set
	End Property

	Public Property V2_5 As String
		Get
			Return _V2_5
		End Get
		Set(ByVal value As String)
			_V2_5 = value
		End Set
	End Property

	Public Property Observaciones_Area_2 As String
		Get
			Return _Observaciones_Area_2
		End Get
		Set(ByVal value As String)
			_Observaciones_Area_2 = value
		End Set
	End Property

	Public Property V3_1 As String
		Get
			Return _V3_1
		End Get
		Set(ByVal value As String)
			_V3_1 = value
		End Set
	End Property

	Public Property V3_2 As String
		Get
			Return _V3_2
		End Get
		Set(ByVal value As String)
			_V3_2 = value
		End Set
	End Property

	Public Property V3_3 As String
		Get
			Return _V3_3
		End Get
		Set(ByVal value As String)
			_V3_3 = value
		End Set
	End Property

	Public Property V3_4 As String
		Get
			Return _V3_4
		End Get
		Set(ByVal value As String)
			_V3_4 = value
		End Set
	End Property

	Public Property V3_5 As String
		Get
			Return _V3_5
		End Get
		Set(ByVal value As String)
			_V3_5 = value
		End Set
	End Property

	Public Property V3_6 As String
		Get
			Return _V3_6
		End Get
		Set(ByVal value As String)
			_V3_6 = value
		End Set
	End Property

	Public Property V3_7 As String
		Get
			Return _V3_7
		End Get
		Set(ByVal value As String)
			_V3_7 = value
		End Set
	End Property

	Public Property Observaciones_Area_3 As String
		Get
			Return _Observaciones_Area_3
		End Get
		Set(ByVal value As String)
			_Observaciones_Area_3 = value
		End Set
	End Property

	Public Property V4_1 As String
		Get
			Return _V4_1
		End Get
		Set(ByVal value As String)
			_V4_1 = value
		End Set
	End Property

	Public Property V4_2 As String
		Get
			Return _V4_2
		End Get
		Set(ByVal value As String)
			_V4_2 = value
		End Set
	End Property

	Public Property V4_3 As String
		Get
			Return _V4_3
		End Get
		Set(ByVal value As String)
			_V4_3 = value
		End Set
	End Property

	Public Property V4_4 As String
		Get
			Return _V4_4
		End Get
		Set(ByVal value As String)
			_V4_4 = value
		End Set
	End Property

	Public Property V4_5 As String
		Get
			Return _V4_5
		End Get
		Set(ByVal value As String)
			_V4_5 = value
		End Set
	End Property

	Public Property Accion As TipoAccion
		Get
			Return _Accion
		End Get
		Set(ByVal value As TipoAccion)
			_Accion = value
		End Set
	End Property

#End Region

#Region "METODOS"
	Public Function INSERTAR() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		CnnSQL.AgregarParametros("@Tienda", Tienda)
		CnnSQL.AgregarParametros("@Fecha", Fecha)
		CnnSQL.AgregarParametros("@Gerente", Gerente)
		CnnSQL.AgregarParametros("@Especialista", Especialista)
		CnnSQL.AgregarParametros("@TotalIncidencias", TotalIncidencias)
		CnnSQL.AgregarParametros("@V1_1", V1_1)
		CnnSQL.AgregarParametros("@V1_2", V1_2)
		CnnSQL.AgregarParametros("@V1_3", V1_3)
		CnnSQL.AgregarParametros("@V1_4", V1_4)
		CnnSQL.AgregarParametros("@V1_5", V1_5)
		CnnSQL.AgregarParametros("@V1_6", V1_6)
		CnnSQL.AgregarParametros("@Observaciones_Area_1", Observaciones_Area_1)
		CnnSQL.AgregarParametros("@V2_1", V2_1)
		CnnSQL.AgregarParametros("@V2_2", V2_2)
		CnnSQL.AgregarParametros("@V2_3", V2_3)
		CnnSQL.AgregarParametros("@V2_4", V2_4)
		CnnSQL.AgregarParametros("@V2_5", V2_5)
		CnnSQL.AgregarParametros("@Observaciones_Area_2", Observaciones_Area_2)
		CnnSQL.AgregarParametros("@V3_1", V3_1)
		CnnSQL.AgregarParametros("@V3_2", V3_2)
		CnnSQL.AgregarParametros("@V3_3", V3_3)
		CnnSQL.AgregarParametros("@V3_4", V3_4)
		CnnSQL.AgregarParametros("@V3_5", V3_5)
		CnnSQL.AgregarParametros("@V3_6", V3_6)
		CnnSQL.AgregarParametros("@V3_7", V3_7)
		CnnSQL.AgregarParametros("@Observaciones_Area_3", Observaciones_Area_3)
		CnnSQL.AgregarParametros("@V4_1", V4_1)
		CnnSQL.AgregarParametros("@V4_2", V4_2)
		CnnSQL.AgregarParametros("@V4_3", V4_3)
		CnnSQL.AgregarParametros("@V4_4", V4_4)
		CnnSQL.AgregarParametros("@V4_5", V4_5)
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetInsert)
		Try
			CnnSQL.ejecutaSentenciaNonQuery(procIncidencias, SQLConexion.CnnSql.TipoDato.StoredProcedure, _Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function CONSULTA() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetSelect)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procIncidencias)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "CONSTRUCTORES"
	Sub New(ConexionString As String)
		_Cnn = ConexionString
	End Sub
	Sub New()
		_Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		GetInsert = 1
		GetSelect = 2
	End Enum
#End Region
End Class
