﻿Public Class CCheckList
	Inherits CRespuesta
#Region "VARIABLES"
	Private _Folio As String
	Private _Sucursal As String
	Private _FechaInv As String
	Private _FechaBod As String
	Private _Supervisor As String
	Private _Apoyo1 As String
	Private _Apoyo2 As String
	Private _Encargada As String
	Private _HoraInicio As String
	Private _HoraTermino As String
	Private _Totalhrs As String
	Private _TotalUN As String
	Private _TotalValor As String
	Private _Califgral As String
	Private _Prodgral As String
	Private _HradicionalB As String
	Private _ProdgralPV As String
	Private _ProdgralB As String
	Private _Conta1 As String
	Private _Conta2 As String
	Private _Tiempo_conteo_PV As String
	Private _Tiempo_conteo_B As String
	Private _TiempoC1PV As String
	Private _TiempoC1B As String
	Private _TiempogralC1 As String
	Private _TiempoC2PV As String
	Private _TiempoC2B As String
	Private _TiempogralC2 As String
	Private _Validaciondif As String
	Private _Correcciondif As String
	Private _TotMarbPV As String
	Private _TotUNPV As String
	Private _TotvalorPV As String
	Private _TotMarbB As String
	Private _TotUNB As String
	Private _TotvalorB As String
	Private _TotMarb As String
	Private _TotUN As String
	Private _Totvalor As String
	Private _UNRIPV As String
	Private _TMRIPV As String
	Private _PDRIPV As String
	Private _UNRIB As String
	Private _TMRIB As String
	Private _PDRIB As String
	Private _UNGralRI As String
	Private _TMGralRI As String
	Private _PDGralRI As String
	Private _UNAP1PV As String
	Private _TMAP1PV As String
	Private _PDAP1PV As String
	Private _UNAP1B As String
	Private _TMAP1B As String
	Private _PDAP1B As String
	Private _UNGralAP1 As String
	Private _TMGralAP1 As String
	Private _PDGralAP1 As String
	Private _UNAP2PV As String
	Private _TMAP2PV As String
	Private _PDAP2PV As String
	Private _UNAP2B As String
	Private _TMAP2B As String
	Private _PDAP2B As String
	Private _UNGralAP2 As String
	Private _TMGralAP2 As String
	Private _PDGralAP2 As String
	Private _Toterr As String
	Private _ToterrEnc As String
	Private _ToterrAdi As String
	Private _Pregunta1 As String
	Private _Pregunta2 As String
	Private _Pregunta3 As String
	Private _Pregunta4 As String
	Private _Pregunta5 As String
	Private _Pregunta6 As String
	Private _Pregunta7 As String
	Private _Pregunta8 As String
	Private _Pregunta9 As String
	Private _Pregunta10 As String
	Private _Pregunta11 As String
	Private _Pregunta12 As String
	Private _Pregunta13 As String
	Private _AREA1 As String
	Private _Obs1 As String
	Private _AREA2 As String
	Private _Obs2 As String
	Private _AREA3 As String
	Private _Obs3 As String
	Private _AREA4 As String
	Private _Obs4 As String
	Private _Obsgral1 As String
	Private _Accion As String
	Private _Cnn As String
#End Region

#Region "PROPIEDADES"
	Public Property Folio As String
		Get
			Return _Folio
		End Get
		Set(ByVal value As String)
			_Folio = value
		End Set
	End Property

	Public Property Sucursal As String
		Get
			Return _Sucursal
		End Get
		Set(ByVal value As String)
			_Sucursal = value
		End Set
	End Property

	Public Property FechaInv As String
		Get
			Return _FechaInv
		End Get
		Set(ByVal value As String)
			_FechaInv = value
		End Set
	End Property

	Public Property FechaBod As String
		Get
			Return _FechaBod
		End Get
		Set(ByVal value As String)
			_FechaBod = value
		End Set
	End Property

	Public Property Supervisor As String
		Get
			Return _Supervisor
		End Get
		Set(ByVal value As String)
			_Supervisor = value
		End Set
	End Property

	Public Property Apoyo1 As String
		Get
			Return _Apoyo1
		End Get
		Set(ByVal value As String)
			_Apoyo1 = value
		End Set
	End Property

	Public Property Apoyo2 As String
		Get
			Return _Apoyo2
		End Get
		Set(ByVal value As String)
			_Apoyo2 = value
		End Set
	End Property

	Public Property Encargada As String
		Get
			Return _Encargada
		End Get
		Set(ByVal value As String)
			_Encargada = value
		End Set
	End Property

	Public Property HoraInicio As String
		Get
			Return _HoraInicio
		End Get
		Set(ByVal value As String)
			_HoraInicio = value
		End Set
	End Property

	Public Property HoraTermino As String
		Get
			Return _HoraTermino
		End Get
		Set(ByVal value As String)
			_HoraTermino = value
		End Set
	End Property

	Public Property Totalhrs As String
		Get
			Return _Totalhrs
		End Get
		Set(ByVal value As String)
			_Totalhrs = value
		End Set
	End Property

	Public Property TotalUN As String
		Get
			Return _TotalUN
		End Get
		Set(ByVal value As String)
			_TotalUN = value
		End Set
	End Property

	Public Property TotalValor As String
		Get
			Return _TotalValor
		End Get
		Set(ByVal value As String)
			_TotalValor = value
		End Set
	End Property

	Public Property Califgral As String
		Get
			Return _Califgral
		End Get
		Set(ByVal value As String)
			_Califgral = value
		End Set
	End Property

	Public Property Prodgral As String
		Get
			Return _Prodgral
		End Get
		Set(ByVal value As String)
			_Prodgral = value
		End Set
	End Property

	Public Property HradicionalB As String
		Get
			Return _HradicionalB
		End Get
		Set(ByVal value As String)
			_HradicionalB = value
		End Set
	End Property

	Public Property ProdgralPV As String
		Get
			Return _ProdgralPV
		End Get
		Set(ByVal value As String)
			_ProdgralPV = value
		End Set
	End Property

	Public Property ProdgralB As String
		Get
			Return _ProdgralB
		End Get
		Set(ByVal value As String)
			_ProdgralB = value
		End Set
	End Property

	Public Property Conta1 As String
		Get
			Return _Conta1
		End Get
		Set(ByVal value As String)
			_Conta1 = value
		End Set
	End Property

	Public Property Conta2 As String
		Get
			Return _Conta2
		End Get
		Set(ByVal value As String)
			_Conta2 = value
		End Set
	End Property

	Public Property Tiempo_conteo_PV As String
		Get
			Return _Tiempo_conteo_PV
		End Get
		Set(ByVal value As String)
			_Tiempo_conteo_PV = value
		End Set
	End Property

	Public Property Tiempo_conteo_B As String
		Get
			Return _Tiempo_conteo_B
		End Get
		Set(ByVal value As String)
			_Tiempo_conteo_B = value
		End Set
	End Property

	Public Property TiempoC1PV As String
		Get
			Return _TiempoC1PV
		End Get
		Set(ByVal value As String)
			_TiempoC1PV = value
		End Set
	End Property

	Public Property TiempoC1B As String
		Get
			Return _TiempoC1B
		End Get
		Set(ByVal value As String)
			_TiempoC1B = value
		End Set
	End Property

	Public Property TiempogralC1 As String
		Get
			Return _TiempogralC1
		End Get
		Set(ByVal value As String)
			_TiempogralC1 = value
		End Set
	End Property

	Public Property TiempoC2PV As String
		Get
			Return _TiempoC2PV
		End Get
		Set(ByVal value As String)
			_TiempoC2PV = value
		End Set
	End Property

	Public Property TiempoC2B As String
		Get
			Return _TiempoC2B
		End Get
		Set(ByVal value As String)
			_TiempoC2B = value
		End Set
	End Property

	Public Property TiempogralC2 As String
		Get
			Return _TiempogralC2
		End Get
		Set(ByVal value As String)
			_TiempogralC2 = value
		End Set
	End Property

	Public Property Validaciondif As String
		Get
			Return _Validaciondif
		End Get
		Set(ByVal value As String)
			_Validaciondif = value
		End Set
	End Property

	Public Property Correcciondif As String
		Get
			Return _Correcciondif
		End Get
		Set(ByVal value As String)
			_Correcciondif = value
		End Set
	End Property

	Public Property TotMarbPV As String
		Get
			Return _TotMarbPV
		End Get
		Set(ByVal value As String)
			_TotMarbPV = value
		End Set
	End Property

	Public Property TotUNPV As String
		Get
			Return _TotUNPV
		End Get
		Set(ByVal value As String)
			_TotUNPV = value
		End Set
	End Property

	Public Property TotvalorPV As String
		Get
			Return _TotvalorPV
		End Get
		Set(ByVal value As String)
			_TotvalorPV = value
		End Set
	End Property

	Public Property TotMarbB As String
		Get
			Return _TotMarbB
		End Get
		Set(ByVal value As String)
			_TotMarbB = value
		End Set
	End Property

	Public Property TotUNB As String
		Get
			Return _TotUNB
		End Get
		Set(ByVal value As String)
			_TotUNB = value
		End Set
	End Property

	Public Property TotvalorB As String
		Get
			Return _TotvalorB
		End Get
		Set(ByVal value As String)
			_TotvalorB = value
		End Set
	End Property

	Public Property TotMarb As String
		Get
			Return _TotMarb
		End Get
		Set(ByVal value As String)
			_TotMarb = value
		End Set
	End Property

	Public Property TotUN As String
		Get
			Return _TotUN
		End Get
		Set(ByVal value As String)
			_TotUN = value
		End Set
	End Property

	Public Property Totvalor As String
		Get
			Return _Totvalor
		End Get
		Set(ByVal value As String)
			_Totvalor = value
		End Set
	End Property

	Public Property UNRIPV As String
		Get
			Return _UNRIPV
		End Get
		Set(ByVal value As String)
			_UNRIPV = value
		End Set
	End Property

	Public Property TMRIPV As String
		Get
			Return _TMRIPV
		End Get
		Set(ByVal value As String)
			_TMRIPV = value
		End Set
	End Property

	Public Property PDRIPV As String
		Get
			Return _PDRIPV
		End Get
		Set(ByVal value As String)
			_PDRIPV = value
		End Set
	End Property

	Public Property UNRIB As String
		Get
			Return _UNRIB
		End Get
		Set(ByVal value As String)
			_UNRIB = value
		End Set
	End Property

	Public Property TMRIB As String
		Get
			Return _TMRIB
		End Get
		Set(ByVal value As String)
			_TMRIB = value
		End Set
	End Property

	Public Property PDRIB As String
		Get
			Return _PDRIB
		End Get
		Set(ByVal value As String)
			_PDRIB = value
		End Set
	End Property

	Public Property UNGralRI As String
		Get
			Return _UNGralRI
		End Get
		Set(ByVal value As String)
			_UNGralRI = value
		End Set
	End Property

	Public Property TMGralRI As String
		Get
			Return _TMGralRI
		End Get
		Set(ByVal value As String)
			_TMGralRI = value
		End Set
	End Property

	Public Property PDGralRI As String
		Get
			Return _PDGralRI
		End Get
		Set(ByVal value As String)
			_PDGralRI = value
		End Set
	End Property

	Public Property UNAP1PV As String
		Get
			Return _UNAP1PV
		End Get
		Set(ByVal value As String)
			_UNAP1PV = value
		End Set
	End Property

	Public Property TMAP1PV As String
		Get
			Return _TMAP1PV
		End Get
		Set(ByVal value As String)
			_TMAP1PV = value
		End Set
	End Property

	Public Property PDAP1PV As String
		Get
			Return _PDAP1PV
		End Get
		Set(ByVal value As String)
			_PDAP1PV = value
		End Set
	End Property

	Public Property UNAP1B As String
		Get
			Return _UNAP1B
		End Get
		Set(ByVal value As String)
			_UNAP1B = value
		End Set
	End Property

	Public Property TMAP1B As String
		Get
			Return _TMAP1B
		End Get
		Set(ByVal value As String)
			_TMAP1B = value
		End Set
	End Property

	Public Property PDAP1B As String
		Get
			Return _PDAP1B
		End Get
		Set(ByVal value As String)
			_PDAP1B = value
		End Set
	End Property

	Public Property UNGralAP1 As String
		Get
			Return _UNGralAP1
		End Get
		Set(ByVal value As String)
			_UNGralAP1 = value
		End Set
	End Property

	Public Property TMGralAP1 As String
		Get
			Return _TMGralAP1
		End Get
		Set(ByVal value As String)
			_TMGralAP1 = value
		End Set
	End Property

	Public Property PDGralAP1 As String
		Get
			Return _PDGralAP1
		End Get
		Set(ByVal value As String)
			_PDGralAP1 = value
		End Set
	End Property

	Public Property UNAP2PV As String
		Get
			Return _UNAP2PV
		End Get
		Set(ByVal value As String)
			_UNAP2PV = value
		End Set
	End Property

	Public Property TMAP2PV As String
		Get
			Return _TMAP2PV
		End Get
		Set(ByVal value As String)
			_TMAP2PV = value
		End Set
	End Property

	Public Property PDAP2PV As String
		Get
			Return _PDAP2PV
		End Get
		Set(ByVal value As String)
			_PDAP2PV = value
		End Set
	End Property

	Public Property UNAP2B As String
		Get
			Return _UNAP2B
		End Get
		Set(ByVal value As String)
			_UNAP2B = value
		End Set
	End Property

	Public Property TMAP2B As String
		Get
			Return _TMAP2B
		End Get
		Set(ByVal value As String)
			_TMAP2B = value
		End Set
	End Property

	Public Property PDAP2B As String
		Get
			Return _PDAP2B
		End Get
		Set(ByVal value As String)
			_PDAP2B = value
		End Set
	End Property

	Public Property UNGralAP2 As String
		Get
			Return _UNGralAP2
		End Get
		Set(ByVal value As String)
			_UNGralAP2 = value
		End Set
	End Property

	Public Property TMGralAP2 As String
		Get
			Return _TMGralAP2
		End Get
		Set(ByVal value As String)
			_TMGralAP2 = value
		End Set
	End Property

	Public Property PDGralAP2 As String
		Get
			Return _PDGralAP2
		End Get
		Set(ByVal value As String)
			_PDGralAP2 = value
		End Set
	End Property

	Public Property Toterr As String
		Get
			Return _Toterr
		End Get
		Set(ByVal value As String)
			_Toterr = value
		End Set
	End Property

	Public Property ToterrEnc As String
		Get
			Return _ToterrEnc
		End Get
		Set(ByVal value As String)
			_ToterrEnc = value
		End Set
	End Property

	Public Property ToterrAdi As String
		Get
			Return _ToterrAdi
		End Get
		Set(ByVal value As String)
			_ToterrAdi = value
		End Set
	End Property

	Public Property Pregunta1 As String
		Get
			Return _Pregunta1
		End Get
		Set(ByVal value As String)
			_Pregunta1 = value
		End Set
	End Property

	Public Property Pregunta2 As String
		Get
			Return _Pregunta2
		End Get
		Set(ByVal value As String)
			_Pregunta2 = value
		End Set
	End Property

	Public Property Pregunta3 As String
		Get
			Return _Pregunta3
		End Get
		Set(ByVal value As String)
			_Pregunta3 = value
		End Set
	End Property

	Public Property Pregunta4 As String
		Get
			Return _Pregunta4
		End Get
		Set(ByVal value As String)
			_Pregunta4 = value
		End Set
	End Property

	Public Property Pregunta5 As String
		Get
			Return _Pregunta5
		End Get
		Set(ByVal value As String)
			_Pregunta5 = value
		End Set
	End Property

	Public Property Pregunta6 As String
		Get
			Return _Pregunta6
		End Get
		Set(ByVal value As String)
			_Pregunta6 = value
		End Set
	End Property

	Public Property Pregunta7 As String
		Get
			Return _Pregunta7
		End Get
		Set(ByVal value As String)
			_Pregunta7 = value
		End Set
	End Property

	Public Property Pregunta8 As String
		Get
			Return _Pregunta8
		End Get
		Set(ByVal value As String)
			_Pregunta8 = value
		End Set
	End Property

	Public Property Pregunta9 As String
		Get
			Return _Pregunta9
		End Get
		Set(ByVal value As String)
			_Pregunta9 = value
		End Set
	End Property

	Public Property Pregunta10 As String
		Get
			Return _Pregunta10
		End Get
		Set(ByVal value As String)
			_Pregunta10 = value
		End Set
	End Property

	Public Property Pregunta11 As String
		Get
			Return _Pregunta11
		End Get
		Set(ByVal value As String)
			_Pregunta11 = value
		End Set
	End Property

	Public Property Pregunta12 As String
		Get
			Return _Pregunta12
		End Get
		Set(ByVal value As String)
			_Pregunta12 = value
		End Set
	End Property

	Public Property Pregunta13 As String
		Get
			Return _Pregunta13
		End Get
		Set(ByVal value As String)
			_Pregunta13 = value
		End Set
	End Property

	Public Property AREA1 As String
		Get
			Return _AREA1
		End Get
		Set(ByVal value As String)
			_AREA1 = value
		End Set
	End Property

	Public Property Obs1 As String
		Get
			Return _Obs1
		End Get
		Set(ByVal value As String)
			_Obs1 = value
		End Set
	End Property

	Public Property AREA2 As String
		Get
			Return _AREA2
		End Get
		Set(ByVal value As String)
			_AREA2 = value
		End Set
	End Property

	Public Property Obs2 As String
		Get
			Return _Obs2
		End Get
		Set(ByVal value As String)
			_Obs2 = value
		End Set
	End Property

	Public Property AREA3 As String
		Get
			Return _AREA3
		End Get
		Set(ByVal value As String)
			_AREA3 = value
		End Set
	End Property

	Public Property Obs3 As String
		Get
			Return _Obs3
		End Get
		Set(ByVal value As String)
			_Obs3 = value
		End Set
	End Property

	Public Property AREA4 As String
		Get
			Return _AREA4
		End Get
		Set(ByVal value As String)
			_AREA4 = value
		End Set
	End Property

	Public Property Obs4 As String
		Get
			Return _Obs4
		End Get
		Set(ByVal value As String)
			_Obs4 = value
		End Set
	End Property

	Public Property Obsgral1 As String
		Get
			Return _Obsgral1
		End Get
		Set(ByVal value As String)
			_Obsgral1 = value
		End Set
	End Property

	Public Property Accion As TipoAccion
		Get
			Return _Accion
		End Get
		Set(ByVal value As TipoAccion)
			_Accion = value
		End Set
	End Property

#End Region

#Region "METODOS"
	Public Function INSERTAR() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		CnnSQL.AgregarParametros("@Folio", Folio)
		CnnSQL.AgregarParametros("@Sucursal", Sucursal)
		CnnSQL.AgregarParametros("@FechaInv", FechaInv)
		CnnSQL.AgregarParametros("@FechaBod", FechaBod)
		CnnSQL.AgregarParametros("@Supervisor", Supervisor)
		CnnSQL.AgregarParametros("@Apoyo1", Apoyo1)
		CnnSQL.AgregarParametros("@Apoyo2", Apoyo2)
		CnnSQL.AgregarParametros("@Encargada", Encargada)
		CnnSQL.AgregarParametros("@HoraInicio", HoraInicio)
		CnnSQL.AgregarParametros("@HoraTermino", HoraTermino)
		CnnSQL.AgregarParametros("@Totalhrs", Totalhrs)
		CnnSQL.AgregarParametros("@TotalUN", TotalUN)
		CnnSQL.AgregarParametros("@TotalValor", TotalValor)
		CnnSQL.AgregarParametros("@Califgral", Califgral)
		CnnSQL.AgregarParametros("@Prodgral", Prodgral)
		CnnSQL.AgregarParametros("@HradicionalB", HradicionalB)
		CnnSQL.AgregarParametros("@ProdgralPV", ProdgralPV)
		CnnSQL.AgregarParametros("@ProdgralB", ProdgralB)
		CnnSQL.AgregarParametros("@Conta1", Conta1)
		CnnSQL.AgregarParametros("@Conta2", Conta2)
		CnnSQL.AgregarParametros("@Tiempo_conteo_PV", Tiempo_conteo_PV)
		CnnSQL.AgregarParametros("@Tiempo_conteo_B", Tiempo_conteo_B)
		CnnSQL.AgregarParametros("@TiempoC1PV", TiempoC1PV)
		CnnSQL.AgregarParametros("@TiempoC1B", TiempoC1B)
		CnnSQL.AgregarParametros("@TiempogralC1", TiempogralC1)
		CnnSQL.AgregarParametros("@TiempoC2PV", TiempoC2PV)
		CnnSQL.AgregarParametros("@TiempoC2B", TiempoC2B)
		CnnSQL.AgregarParametros("@TiempogralC2", TiempogralC2)
		CnnSQL.AgregarParametros("@Validaciondif", Validaciondif)
		CnnSQL.AgregarParametros("@Correcciondif", Correcciondif)
		CnnSQL.AgregarParametros("@TotMarbPV", TotMarbPV)
		CnnSQL.AgregarParametros("@TotUNPV", TotUNPV)
		CnnSQL.AgregarParametros("@TotvalorPV", TotvalorPV)
		CnnSQL.AgregarParametros("@TotMarbB", TotMarbB)
		CnnSQL.AgregarParametros("@TotUNB", TotUNB)
		CnnSQL.AgregarParametros("@TotvalorB", TotvalorB)
		CnnSQL.AgregarParametros("@TotMarb", TotMarb)
		CnnSQL.AgregarParametros("@TotUN", TotUN)
		CnnSQL.AgregarParametros("@Totvalor", Totvalor)
		CnnSQL.AgregarParametros("@UNRIPV", UNRIPV)
		CnnSQL.AgregarParametros("@TMRIPV", TMRIPV)
		CnnSQL.AgregarParametros("@PDRIPV", PDRIPV)
		CnnSQL.AgregarParametros("@UNRIB", UNRIB)
		CnnSQL.AgregarParametros("@TMRIB", TMRIB)
		CnnSQL.AgregarParametros("@PDRIB", PDRIB)
		CnnSQL.AgregarParametros("@UNGralRI", UNGralRI)
		CnnSQL.AgregarParametros("@TMGralRI", TMGralRI)
		CnnSQL.AgregarParametros("@PDGralRI", PDGralRI)
		CnnSQL.AgregarParametros("@UNAP1PV", UNAP1PV)
		CnnSQL.AgregarParametros("@TMAP1PV", TMAP1PV)
		CnnSQL.AgregarParametros("@PDAP1PV", PDAP1PV)
		CnnSQL.AgregarParametros("@UNAP1B", UNAP1B)
		CnnSQL.AgregarParametros("@TMAP1B", TMAP1B)
		CnnSQL.AgregarParametros("@PDAP1B", PDAP1B)
		CnnSQL.AgregarParametros("@UNGralAP1", UNGralAP1)
		CnnSQL.AgregarParametros("@TMGralAP1", TMGralAP1)
		CnnSQL.AgregarParametros("@PDGralAP1", PDGralAP1)
		CnnSQL.AgregarParametros("@UNAP2PV", UNAP2PV)
		CnnSQL.AgregarParametros("@TMAP2PV", TMAP2PV)
		CnnSQL.AgregarParametros("@PDAP2PV", PDAP2PV)
		CnnSQL.AgregarParametros("@UNAP2B", UNAP2B)
		CnnSQL.AgregarParametros("@TMAP2B", TMAP2B)
		CnnSQL.AgregarParametros("@PDAP2B", PDAP2B)
		CnnSQL.AgregarParametros("@UNGralAP2", UNGralAP2)
		CnnSQL.AgregarParametros("@TMGralAP2", TMGralAP2)
		CnnSQL.AgregarParametros("@PDGralAP2", PDGralAP2)
		CnnSQL.AgregarParametros("@Toterr", Toterr)
		CnnSQL.AgregarParametros("@ToterrEnc", ToterrEnc)
		CnnSQL.AgregarParametros("@ToterrAdi", ToterrAdi)
		CnnSQL.AgregarParametros("@Pregunta1", Pregunta1)
		CnnSQL.AgregarParametros("@Pregunta2", Pregunta2)
		CnnSQL.AgregarParametros("@Pregunta3", Pregunta3)
		CnnSQL.AgregarParametros("@Pregunta4", Pregunta4)
		CnnSQL.AgregarParametros("@Pregunta5", Pregunta5)
		CnnSQL.AgregarParametros("@Pregunta6", Pregunta6)
		CnnSQL.AgregarParametros("@Pregunta7", Pregunta7)
		CnnSQL.AgregarParametros("@Pregunta8", Pregunta8)
		CnnSQL.AgregarParametros("@Pregunta9", Pregunta9)
		CnnSQL.AgregarParametros("@Pregunta10", Pregunta10)
		CnnSQL.AgregarParametros("@Pregunta11", Pregunta11)
		CnnSQL.AgregarParametros("@Pregunta12", Pregunta12)
		CnnSQL.AgregarParametros("@Pregunta13", Pregunta13)
		CnnSQL.AgregarParametros("@AREA1", AREA1)
		CnnSQL.AgregarParametros("@Obs1", Obs1)
		CnnSQL.AgregarParametros("@AREA2", AREA2)
		CnnSQL.AgregarParametros("@Obs2", Obs2)
		CnnSQL.AgregarParametros("@AREA3", AREA3)
		CnnSQL.AgregarParametros("@Obs3", Obs3)
		CnnSQL.AgregarParametros("@AREA4", AREA4)
		CnnSQL.AgregarParametros("@Obs4", Obs4)
		CnnSQL.AgregarParametros("@Obsgral1", Obsgral1)
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetInsert)
		Try
			CnnSQL.ejecutaSentenciaNonQuery(procCheckList, SQLConexion.CnnSql.TipoDato.StoredProcedure, _Cnn)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function

	Public Function CONSULTA() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet
		CnnSQL.AgregarParametros("@Accion", TipoAccion.GetSelect)
		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(_Cnn, procCheckList)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException(ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "CONSTRUCTORES"
	Sub New(ConexionString As String)
		_Cnn = ConexionString
	End Sub
	Sub New()
		_Cnn = CReglasNegocio.CnnSAIC
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		GetInsert = 1
		GetSelect = 2
	End Enum
#End Region
End Class
