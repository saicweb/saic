﻿Public Class CUsuarioSession
	Inherits CRespuesta

#Region "PROPIEDADES"
	Public Property Cnn() As String

	Public Property Accion As String

	Public Property IdUsuario As String

	Public Property IdStatus As String

	Public Property UltimaSession As String

	Public Property SesionActiva As String

	Public Property FechaCambio As String

	Public Property FechaBloqueado As String
#End Region

#Region "METODOS"
	Public Function Ejecuta_usuariosession() As Boolean
		Dim CnnSQL As New SQLConexion.CnnSql
		dsRespuesta = New DataSet

		CnnSQL.AgregarParametros("@Accion", Accion)
		CnnSQL.AgregarParametros("@IdUsuario", IdUsuario)
		CnnSQL.AgregarParametros("@Idstatus", IdStatus)
		CnnSQL.AgregarParametros("@ultimaSession", UltimaSession)
		CnnSQL.AgregarParametros("@sesionActiva", SesionActiva)
		CnnSQL.AgregarParametros("@FechaCambio", FechaCambio)
		CnnSQL.AgregarParametros("@FechaBloqueado", FechaBloqueado)

		Try
			dsRespuesta = CnnSQL.ejecutaSentenciaConsulta(Cnn, procusuariosession)
			If CnnSQL.GenerarError Then
				Throw New Exception(CnnSQL.ErrMensaje)
			End If
		Catch ex As Exception
			DisparaException("ERROR: CUsuarioSession - " & ex.Message)
		End Try
		Return GeneraException()
	End Function
#End Region

#Region "Constructores"
	Sub New(ConexionString As String)
		Cnn = ConexionString
	End Sub
	Sub New()
		Cnn = CReglasNegocio.CnnSeguridad
	End Sub
#End Region

#Region "TipoAccion"
	Public Enum TipoAccion
		''' <summary>
		''' Cierra la session del usuario con el parametro NoEmpleado
		''' </summary>
		CloseSession = 1
		''' <summary>
		''' Abre la session del usuario con el parametro NoEmpleado
		''' </summary>
		OpenSession = 2
		''' <summary>
		''' Permite obtener la info de la sesion ultimaSession,sesionActiva,FechaCambio,etc con el parametro IdEmpleado
		''' </summary>
		''' <remarks></remarks>
		GetInfoSession = 3
		''' <summary>
		''' Permite Bloquear la session del usuario con el NoEmpleado
		''' </summary>
		''' <remarks></remarks>
		Bloquear = 4
		''' <summary>
		''' Permite agregar los nuevos registros
		''' </summary>
		''' <remarks></remarks>
		GetInsert = 5
		''' <summary>
		''' Permite Desbloquear al usuario mediante el parametro de NoEmpleado
		''' </summary>
		''' <remarks></remarks>
		Desbloquear = 6
	End Enum
#End Region
End Class
