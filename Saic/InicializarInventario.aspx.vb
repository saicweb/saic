﻿Imports System.Data.SqlClient

Public Class InicializarInventario
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Not Page.IsPostBack Then
			If Session("IdUsuario") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If
			If Session("Perfil") = "P" Then
				Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
			End If

			dateFechaInventario.Text = Today.Date.ToString("yyyy-MM-dd")

			InformacionGeneralInventario()
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			If Me.cboTipoInventario.SelectedValue = "X" Then
				CMensajes.MostrarMensaje("Seleccione el tipo de inventario.", CMensajes.Tipo.GetError, wucMessageInicializarInventario)
			ElseIf Me.cboTipoInventario.SelectedValue = "G" And Me.txtreponsableAdmonInventario.Text.Trim.Length = 0 Then 'GENERAL
				CMensajes.MostrarMensaje("Ingrese el nombre del responsable de administración de inventario.", CMensajes.Tipo.GetError, wucMessageInicializarInventario)
			ElseIf Me.cboTipoInventario.SelectedValue = "P" And Me.txtJefeDepto.Text.Trim.Length = 0 Then 'PARCIAL
				CMensajes.MostrarMensaje("Ingrese el nombre del jefe de departamento.", CMensajes.Tipo.GetError, wucMessageInicializarInventario)
			Else
				If Session("IdTienda") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If

				Dim FechaInventario As DateTime
				'FechaInventario = New DateTime(Me.dateFechaInventario.SelectedDate.Year, Me.dateFechaInventario.SelectedDate.Month, Me.dateFechaInventario.SelectedDate.Day, 23, 59, 59)
				'FechaInventario = New DateTime(dateFechaInventario_CalendarExtender.SelectedDate.Value.Year, dateFechaInventario_CalendarExtender.SelectedDate.Value.Month, dateFechaInventario_CalendarExtender.SelectedDate.Value.Day, 23, 59, 59)
				FechaInventario = New DateTime(Convert.ToDateTime(dateFechaInventario.Text).Year, Convert.ToDateTime(dateFechaInventario.Text).Month, Convert.ToDateTime(dateFechaInventario.Text).Day, 23, 59, 59)

				Dim PAR(9) As SqlParameter
				PAR(0) = New SqlParameter("@Accion", 1)
				PAR(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				PAR(2) = New SqlParameter("@FechaInventario", FechaInventario)
				PAR(3) = New SqlParameter("@TipoInventario", Me.cboTipoInventario.SelectedValue)
				PAR(4) = New SqlParameter("@NombreGerente", Me.txtGerente.Text.Trim)
				PAR(5) = New SqlParameter("@NombreSubGerenteAdmvo", Me.txtSubGerenteAdmvo.Text.Trim)
				PAR(6) = New SqlParameter("@NombreSubGerenteComercial", Me.txtSubGerenteComercial.Text.Trim)
				PAR(7) = New SqlParameter("@NombreJefeDepto", Me.txtJefeDepto.Text.Trim)
				PAR(8) = New SqlParameter("@NombreJefeControlInventarios", Me.txtJefeControlInventarios.Text.Trim)
				PAR(9) = New SqlParameter("@NombreRespAdmonInventarios", Me.txtreponsableAdmonInventario.Text.Trim)
				DatosSQL.procedimientocp("InformacionGeneralInventario_Administracion", PAR)

				Me.btnInicializar.Enabled = False
				CMensajes.MostrarMensaje("Inventario inicializado.", CMensajes.Tipo.GetExito, wucMessageInicializarInventario)
				Session("TipoInventario") = Me.cboTipoInventario.SelectedValue.Trim
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageInicializarInventario.Visible = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Private Sub btnInicializar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInicializar.Click
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">¿Realmente desea inicializar el inventario?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Sub InformacionGeneralInventario()
		Try
			'Restaurantes
			If Session("Sociedad").ToString = "3" Then
				Me.lblSubGerenteAdmon.Visible = False
				Me.txtSubGerenteAdmvo.Visible = False
				Me.RequiredFieldValidator2.Enabled = False

				Me.lblSubGerenteComercial.Visible = False
				Me.txtSubGerenteComercial.Visible = False
				Me.RequiredFieldValidator3.Enabled = False

				Me.lblJefeDepto.Visible = False
				Me.txtJefeDepto.Visible = False
			End If

			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("InformacionGeneralInventario_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				If IsDBNull(ds.Tables(0).Rows(0).Item("TipoInventario")) = False Then
					Me.cboTipoInventario.SelectedValue = ds.Tables(0).Rows(0).Item("TipoInventario")
					If Me.cboTipoInventario.SelectedValue = "G" Then 'GENERAL
						Me.lblNomResAdmonInv.Enabled = True
						Me.txtreponsableAdmonInventario.Enabled = True
						Me.lblJefeDepto.Enabled = False
						Me.txtJefeDepto.Enabled = False
					ElseIf Me.cboTipoInventario.SelectedValue = "P" Then 'PARCIAL
						Me.lblNomResAdmonInv.Enabled = False
						Me.txtreponsableAdmonInventario.Enabled = False
						Me.lblJefeDepto.Enabled = True
						Me.txtJefeDepto.Enabled = True
					End If
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreGerente")) = False Then
					Me.txtGerente.Text = ds.Tables(0).Rows(0).Item("NombreGerente")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreSubGerenteAdmvo")) = False Then
					Me.txtSubGerenteAdmvo.Text = ds.Tables(0).Rows(0).Item("NombreSubGerenteAdmvo")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreSubGerenteComercial")) = False Then
					Me.txtSubGerenteComercial.Text = ds.Tables(0).Rows(0).Item("NombreSubGerenteComercial")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreJefeDepto")) = False Then
					Me.txtJefeDepto.Text = ds.Tables(0).Rows(0).Item("NombreJefeDepto")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreJefeControlInventarios")) = False Then
					Me.txtJefeControlInventarios.Text = ds.Tables(0).Rows(0).Item("NombreJefeControlInventarios")
				End If
				If IsDBNull(ds.Tables(0).Rows(0).Item("NombreRespAdmonInventarios")) = False Then
					Me.txtreponsableAdmonInventario.Text = ds.Tables(0).Rows(0).Item("NombreRespAdmonInventarios")
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub cboTipoInventario_SelectedIndexChanged1(sender As Object, e As System.EventArgs) Handles cboTipoInventario.SelectedIndexChanged
		If Me.cboTipoInventario.Items.Count > 0 AndAlso Me.cboTipoInventario.SelectedIndex <> -1 Then
			If Me.cboTipoInventario.SelectedValue = "X" Then
				Me.lblNomResAdmonInv.Enabled = False
				Me.txtreponsableAdmonInventario.Enabled = False
				Me.lblJefeDepto.Enabled = False
				Me.txtJefeDepto.Enabled = False
			ElseIf Me.cboTipoInventario.SelectedValue = "G" Then 'GENERAL
				Me.lblNomResAdmonInv.Enabled = True
				Me.txtreponsableAdmonInventario.Enabled = True
				Me.lblJefeDepto.Enabled = False
				Me.txtJefeDepto.Enabled = False
			ElseIf Me.cboTipoInventario.SelectedValue = "P" Then 'PARCIAL
				Me.lblNomResAdmonInv.Enabled = False
				Me.txtreponsableAdmonInventario.Enabled = False
				Me.lblJefeDepto.Enabled = True
				Me.txtJefeDepto.Enabled = True
			End If
		End If
	End Sub
End Class