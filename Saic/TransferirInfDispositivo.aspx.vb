﻿Imports System.IO
Imports System.Data.SqlClient
Imports dbAutoTrack.DataReports

Public Class TransferirInfDispositivo
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Dim Cont As Integer

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblCont.Text = ""
		Cont = 0
		If Not Page.IsPostBack OrElse Session("RecargarArtSet") = "1" Then
			If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If

			If Session("TipoUsuario") = "A" OrElse Session("SubirArchivoAccessControl") = "1" Then
				Me.uplXML.Visible = True
			End If

			If Session("Sociedad").ToString = "3" Then
				Me.lblAlmacen.Visible = False
				Me.cboAlmacen.Visible = False
				Me.lblConteos.Visible = False
				Me.rbtnNumConteo.Visible = False
				Me.chkImprimirMarbetes.Visible = False
				Me.Link.Visible = False
				Me.Link2.Visible = False
			Else
				Almacenes_Buscar()
			End If

			Terminales_Buscar()
			Session("RecargarArtSet") = ""
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			If Not Session("Proceso") Is Nothing AndAlso Session("Proceso") = "1" Then
				If Session("Sociedad").ToString = "3" Then
					Dim listaMarbetes As List(Of String) = ValidarRango(Me.cboTerminal.SelectedValue)

					If listaMarbetes IsNot Nothing AndAlso listaMarbetes.Count > 0 Then
						CMensajes.MostrarMensaje("Favor de capturar el rango de marbetes para: " & String.Join(",", listaMarbetes.ToArray()) & ".", CMensajes.Tipo.GetExito, wucMessageTransferirInfDispositivo)
					Else
						Transferir()
					End If
				Else
					Transferir()
				End If
			ElseIf Not Session("Proceso") Is Nothing AndAlso Session("Proceso") = "2" Then
				Me.ConteosDispositivo_Inicializar()

				Dim SR As StreamReader
				Dim StrRuta As String = Session("strRuta")
				SR = New StreamReader(StrRuta)

				Dim linea As String = SR.ReadLine

				If Session("Cedis") = "0" Then '---NO ES CEDIS
					Me.ProcesarAccess(SR, StrRuta, linea)
				Else '---ES CEDIS
					Me.ProcesarAccessCedis(SR, StrRuta, linea)
				End If

				Me.Terminales_Buscar()
				If Not Session("IdTerminal") Is Nothing Then
					Try
						Me.cboTerminal.SelectedValue = Session("IdTerminal")
						Almacenes_Buscar()
					Catch ex As Exception
						Throw New Exception(ex.Message, ex.InnerException)
					End Try
				End If
				Me.ConteosDispositivo_Buscar()

				File.Delete(StrRuta)

				Me.TblDispositivosXAdjuntar.Visible = True
				CMensajes.MostrarMensaje("Proceso terminado.", CMensajes.Tipo.GetExito, wucMessageTransferirInfDispositivo)
				Me.lblTiempoProceso.Text &= "  Hora de término: " & Now.ToShortTimeString
				Me.wucMessageMensaje.Visible = False

				Me.btnCargar.Enabled = True
				Me.btnCargar.Text = "Transferir información"
			End If
			Session("dsAccess") = Nothing
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageTransferirInfDispositivo.Visible = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Private Sub btnCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargar.Click
		Try
			Session("dsAccess") = Nothing
			Session("IdTerminal") = Nothing
			Session("Proceso") = "2"
			Me.lblTiempoProceso.Text = "Hora de Inicio: " & Now.ToShortTimeString
			CMensajes.MostrarMensaje("Procesando...", CMensajes.Tipo.GetInformativo, wucMessageMensaje)
			'--------------------------------------------
			Dim strRuta As String
			Dim IdTienda As String = CStr(Session("IdTienda"))
			Dim nomArchivo As String
			Dim singleResultExist As Boolean = singleUpload.HasFile 'singleUpload.UploadedFiles.Count > 0
			Dim Cedis As Integer = CInt(Session("Cedis"))

			Me.btnCargar.Enabled = False
			Me.btnCargar.Text = "Transfiriendo infromación..."

			If singleResultExist Then
				nomArchivo = "ConteosDispositivo_" & Now.Day & Now.Month & Now.Year & Now.Hour.ToString & Now.Minute.ToString & "_" & Session("IdUsuario") & ".txt"
				If singleUpload.FileName.ToUpper.EndsWith(".TXT") Then
					Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

					If Not Directory.Exists(DirectorioArchivos) Then
						Directory.CreateDirectory(DirectorioArchivos)
					End If

					strRuta = DirectorioArchivos
					strRuta &= nomArchivo
					Session("nomArchivo") = nomArchivo
					singleUpload.SaveAs(strRuta)

					Session("strRuta") = strRuta
					Dim Linea As String
					Dim Registro() As String
					Dim SR As StreamReader
					SR = New StreamReader(strRuta)

					Linea = SR.ReadLine()

					Dim borrar As Boolean = False
					If Not Linea Is Nothing Then
						Registro = Linea.Split("|")
						Dim NAmeDis As String = Registro(1).ToString.ToUpper
						Session("IdTerminal") = NAmeDis

						If Me.ConteosDispositivo_Existe(NAmeDis) = False Then
							If Cedis = "0" Then '---NO ES CEDIS
								Me.ProcesarAccess(SR, strRuta, Linea)
							Else '---ES CEDIS
								Me.ProcesarAccessCedis(SR, strRuta, Linea)
							End If

							Me.Terminales_Buscar()
							If Not Session("IdTerminal") Is Nothing Then
								Try
									Me.cboTerminal.SelectedValue = Session("IdTerminal")
									Almacenes_Buscar()
								Catch ex As Exception
									Throw New Exception(ex.Message, ex.InnerException)
								End Try
							End If
							Me.ConteosDispositivo_Buscar()

							Me.TblDispositivosXAdjuntar.Visible = True
							CMensajes.MostrarMensaje("Proceso terminado.", CMensajes.Tipo.GetExito, wucMessageTransferirInfDispositivo)
							Me.lblTiempoProceso.Text &= "  Hora de término: " & Now.ToShortTimeString
							Me.wucMessageMensaje.Visible = False

							Me.btnCargar.Enabled = True
							Me.btnCargar.Text = "Transferir información"
							borrar = True
						Else
							Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
							lblTexto.Text = "<div align=""center"">Existe información grabada del dispositivo y el usuario en turno. ¿Desea continuar?</div>"
							winConfirm1.MuestraDialogo()
						End If
					End If
					SR.Close()
					If borrar Then File.Delete(strRuta)
					Me.btnCargar.Enabled = False
					Me.btnCargar.Text = "Transferir información"
				Else
					CMensajes.MostrarMensaje("El archivo a cargar debe ser un archivo con extension .txt", CMensajes.Tipo.GetError, wucMessageTransferirInfDispositivo)
					Me.btnCargar.Enabled = True
					Me.btnCargar.Text = "Transferir información"
				End If
			Else
				CMensajes.MostrarMensaje("No se ha especificado el nombre del archivo a cargar.", CMensajes.Tipo.GetError, wucMessageTransferirInfDispositivo)
				Me.btnCargar.Enabled = True
				Me.btnCargar.Text = "Transferir información"
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function ProcesarAccess(ByVal SR As StreamReader, strRuta As String, Linea As String) As Integer
		Try
			Dim Sentencia As String = ""
			Dim SentenciaNueva As String = ""
			Dim cont As Integer = 0
			'@Accion INT, --1=Agregar 2=Modificar 3=Eliminar un registro 4=Eliminar registros para la tienda, usuario y perfil

			Dim IdRow, NameDis, Marbete, Cant, Cbarras, Almacen As String
			Dim Registro() As String

			Do
				If Linea Is Nothing OrElse Linea = "" Then
					Exit Do
				End If
				Registro = Linea.Split("|")

				IdRow = Registro(0).Trim
				NameDis = Registro(1).Trim.ToUpper
				Marbete = Registro(2).Trim
				Cant = Registro(3).Trim
				Cbarras = Registro(4).Trim

				If Session("Sociedad").ToString = "3" Then
					Almacen = "A001"
				Else
					Almacen = Registro(5).Trim
				End If


				Dim IdTerminal As String = NameDis
				Session("IdTerminal") = IdTerminal

				Dim EAN As String = Cbarras
				Dim Conteo As String = Cant

				SentenciaNueva = " EXEC ConteosDispositivo_Administracion 1" &
									  "," & Chr(39) & Session("IdTienda") & Chr(39) & "," & Chr(39) & Session("IdUsuario") & Chr(39) & "," &
									   Chr(39) & IdTerminal & Chr(39) & "," & Chr(39) & Marbete & Chr(39) & "," &
									 IdRow.ToString & "," & Chr(39) & EAN & Chr(39) & "," &
									Conteo.ToString & "," & Chr(39) & Session("Perfil") & Chr(39) &
									"," & Chr(39) & Almacen & Chr(39)

				If Len(Sentencia & SentenciaNueva) > 8000 Then
					DatosSQL.procedimientoText(Sentencia)
					Sentencia = ""
				End If
				Sentencia = Sentencia + SentenciaNueva
				cont = cont + 1

				Linea = SR.ReadLine()
			Loop Until Linea Is Nothing

			If Len(Sentencia) > 0 AndAlso Len(Sentencia) <= 8000 Then
				DatosSQL.procedimientoText(Sentencia)
			End If
			Return cont
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub ConteosDispositivo_Buscar()
		Try
			Dim DS As New DataSet
			Dim PAR(5) As SqlParameter
			PAR(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			PAR(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			PAR(2) = New SqlParameter("@Perfil", Session("Perfil"))
			PAR(3) = New SqlParameter("@IdTerminal", Me.cboTerminal.SelectedValue)
			PAR(4) = New SqlParameter("@Conteo", rbtnNumConteo.SelectedValue)
			PAR(5) = New SqlParameter("@idAlmacen", IIf(Session("Sociedad").ToString = "3", "A001", Me.cboAlmacen.SelectedValue))

			DS = DatosSQL.funcioncp("ConteosDispositivo_Buscar", PAR)
			If Not DS Is Nothing AndAlso DS.Tables.Count > 0 Then
				Me.dg.DataSource = DS.Tables(0)
				Me.dg.DataBind()
				'If Session("Sociedad").ToString = "3" Then
				'	Me.dg.Columns(4).Visible = True
				'	'Me.dg.Columns(5).Visible = True
				'End If
				Me.dg.Visible = True
				lblCont.Text = "Total de marbetes por adjuntar: " & DS.Tables(0).Rows.Count
				Me.btnAdjuntar.Enabled = True
				Me.btnEliminar.Enabled = True
				Me.btnGuardar.Enabled = True
			Else
				Me.dg.DataSource = Nothing
				Me.dg.DataBind()

				Me.btnAdjuntar.Enabled = False
				Me.btnEliminar.Enabled = False
				Me.btnGuardar.Enabled = False
			End If
			If Session("Cedis") <> 1 Then
				'Me.dg.Columns(14).Visible = False 'BULTOS
				'Me.dg.Columns(15).Visible = False 'BULTOV
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub ConteosDispositivo_Inicializar()
		Try
			'@Accion INT, --1=Agregar 2=Modificar 3=Eliminar un registro 4=Eliminar registros para la tienda, usuario y perfil
			Dim PAR(4) As SqlParameter
			PAR(0) = New SqlParameter("@Accion", 4)
			PAR(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			PAR(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			PAR(3) = New SqlParameter("@Perfil", Session("Perfil"))
			PAR(4) = New SqlParameter("@IdTerminal", Session("IdTerminal"))

			DatosSQL.funcioncp("ConteosDispositivo_Administracion", PAR)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
		Try
			Dim Sentencia As String = ""
			Dim SentenciaNueva As String = ""
			For i As Integer = 0 To Me.dg.Rows.Count - 1
				Dim ck As CheckBox = CType(dg.Rows(i).Cells(2).Controls(1), CheckBox)
				If ck.Checked = True Then
					SentenciaNueva = " EXEC ConteosDispositivo_Administracion @Accion=3,@IdTienda='" & Session("IdTienda") & "',@IdMarbete='" & dg.Rows(i).Cells(1).Text & "',@IdTerminal='" & Me.cboTerminal.SelectedValue & "'"

					If Len(Sentencia & SentenciaNueva) > 8000 Then
						DatosSQL.procedimientoText(Sentencia)
						Sentencia = ""
					End If
					Sentencia = Sentencia + SentenciaNueva
				End If
			Next
			If Len(Sentencia) > 0 AndAlso Len(Sentencia) <= 8000 Then
				DatosSQL.procedimientoText(Sentencia)
			End If
			Me.ConteosDispositivo_Buscar()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnAdjuntar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdjuntar.Click
		Try
			Session("Proceso") = "1"
			If Me.dg.Rows.Count = 0 Then
				Exit Sub
			End If

			If ExisteInfAdjuntadaEnAlmacen() = True Then
				CMensajes.MostrarMensaje("Existe información adjuntada del dispositivo para dos almacenes diferentes.", CMensajes.Tipo.GetError, wucMessageTransferirInfDispositivo)
			Else
				If ExisteInfAdjuntada() = True Then
					Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
					lblTexto.Text = "<div align=""center"">Existe información adjuntada del dispositivo y el usuario en turno. ¿Desea continuar?</div>"
					winConfirm1.MuestraDialogo()
				Else
					Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
					lblTexto.Text = "<div align=""center"">¿Seguro que el dispositivo es el correcto para adjuntar?</div>"
					winConfirm1.MuestraDialogo()
				End If
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	'Private Sub btnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
	'    Try
	'        Me.lblMensaje.Text = ""
	'        Dim Sentencia As String = ""
	'        Dim SentenciaNueva As String = ""
	'        For i As Integer = 0 To Me.dg.Rows.Count - 1
	'            Dim txtCant As TextBox = CType(dg.Rows(i).Cells(10).Controls(1), TextBox)
	'            If txtCant.Text.Trim.Length = 0 Then
	'            ElseIf IsNumeric(txtCant.Text) = False Then
	'                Me.dg.Rows(i).BackColor = Drawing.Color.Red
	'            ElseIf CDbl(txtCant.Text) < 0 Then
	'                Me.dg.Rows(i).BackColor = Drawing.Color.Yellow
	'            ElseIf CDbl(txtCant.Text) <> CDbl(dg.Rows(i).Cells(9).Text) Then
	'                Me.dg.Rows(i).BackColor = Drawing.Color.Transparent

	'                SentenciaNueva = " EXEC ConteosDispositivo_Administracion @Accion=2,@IdConteosDispositivo=" & _
	'                               dg.Rows(i).Cells(6).Text & ",@Conteo=" & txtCant.Text.Trim

	'                dg.Rows(i).Cells(9).Text = txtCant.Text
	'                If Len(Sentencia & SentenciaNueva) > 8000 Then
	'                    DatosSQL.procedimientoText(Sentencia)
	'                    Sentencia = ""
	'                End If
	'                Sentencia = Sentencia + SentenciaNueva
	'            End If
	'        Next
	'        If Len(Sentencia) > 0 AndAlso Len(Sentencia) <= 8000 Then
	'            DatosSQL.procedimientoText(Sentencia)
	'        End If
	'        Me.ConteosDispositivo_Buscar()
	'    Catch ex As Exception
	'        Me.lblMensaje.Text = "Error P.TID.4: " & ex.Message
	'    End Try
	'End Sub

	Sub RPT_MarbeteIndividual()
		Try
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@Tipo", "R")
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_MarbeteIndividual_Dispositivo", par)
			If Not ds Is Nothing Then
				If ds.Tables(0).Rows.Count > 0 Then
					REPORTE(ds)
					If Session("ImpMarbetes") Then
						Me.Link.Visible = True
						Me.Link2.Visible = True
					End If
				Else
					CMensajes.MostrarMensaje("No existen marbetes dentro del rango especificado.", CMensajes.Tipo.GetError, wucMessageTransferirInfDispositivo)
				End If
			Else
				CMensajes.MostrarMensaje("Ocurrió un error al conectar con la base de datos.", CMensajes.Tipo.GetError, wucMessageTransferirInfDispositivo)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPORTE(ByVal DATOS As DataSet, Optional ByVal Reporte As String = Nothing)
		Try
			Dim dt As DataTable
			dt = DATOS.Tables(0)

			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim settings As New PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Portrait
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			'MKSR se tiene que descomentarizar para hacer el reporte
			If Reporte = "Articulos SET" Then
				report1 = New RPT_ArticulosSetrpt
			Else
				report1 = New RPT_MarbeteIndividualrpt
			End If
			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim pathPdf As String

			If Reporte = "Articulos SET" Then
				Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("ArticulosSet")

				If Not Directory.Exists(DirectorioArchivos) Then
					Directory.CreateDirectory(DirectorioArchivos)
				End If
				pathPdf = DirectorioArchivos & "ArticulosSet_" & Session("IdUsuario") & ".pdf"
			Else
				Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbeteIndividualDispositivo")

				If Not Directory.Exists(DirectorioArchivos) Then
					Directory.CreateDirectory(DirectorioArchivos)
				End If
				pathPdf = DirectorioArchivos & "MarbeteIndividualDispositivo_" & Session("IdUsuario") & ".pdf"
			End If
			If Reporte = "Articulos SET" Then
				report1.DataSource = DATOS.Tables(1)
			Else
				report1.DataSource = DATOS
			End If


			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = Format(DateTime.Now, "dd/MM/yyyy")
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = Format(DateTime.Now, "hh:mm tt")
            CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = Session("IdTienda")
            CType(report1.Sections.Item(0).Controls.Item("txtAlmacen"), dbATTextBox).Value = cboAlmacen.SelectedItem.Text

            document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)

			Dim encripta As New Encryption
			If Reporte = "Articulos SET" Then
				Me.linkArtSet.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
				Session("RecargarArtSet") = "1"
			Else
				Me.Link.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
			End If

			'If Session("ImpMarbetes") Then
			'	Me.Link.Visible = True
			'End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	'Private Sub dg_PageIndexChanged(ByVal source As System.Object, ByVal e As Telerik.WebControls.GridPageChangedEventArgs) Handles dg.PageIndexChanged
	'    ' SE REDIRECCIONA EL NUMERO DE PAGINA
	'    dg.CurrentPageIndex = e.NewPageIndex
	'    ' SE LLENAN LOS RESULTADOS
	'    Me.ConteosDispositivo_Buscar()
	'End Sub

	Sub Terminales_Buscar()
		Try
			Me.cboTerminal.Items.Clear()
			Dim ds As New DataSet
			Dim par(3) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			par(2) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
			par(3) = New SqlParameter("@TipoUsuario", Session("TipoUsuario"))
			ds = DatosSQL.funcioncp("ConteosDispositivo_Terminales", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.cboTerminal.DataSource = ds.Tables(0)
				Me.cboTerminal.DataTextField = "IDTERMINAL"
				Me.cboTerminal.DataValueField = "IDTERMINAL"
				Me.cboTerminal.DataBind()
			Else
				CMensajes.MostrarMensaje("No existen terminales por adjuntar.", CMensajes.Tipo.GetError, wucMessageTransferirInfDispositivo)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Function ExisteInfAdjuntada() As Boolean
		Try
			ExisteInfAdjuntada = False
			Dim dt As DataSet
			Dim par(6) As SqlParameter
			par(0) = New SqlParameter("@Tipo", 4)
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			par(3) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
			par(4) = New SqlParameter("@IdTerminal", Me.cboTerminal.SelectedValue)
			par(5) = New SqlParameter("@Conteo", Me.rbtnNumConteo.SelectedValue) '0)
			par(6) = New SqlParameter("@idAlmacen", IIf(Me.cboAlmacen.SelectedValue = "", "A001", Me.cboAlmacen.SelectedValue))

			dt = DatosSQL.funcioncp("Existe_Administracion", par)
			If Not dt Is Nothing AndAlso dt.Tables.Count > 0 AndAlso dt.Tables(0).Rows.Count > 0 AndAlso dt.Tables(0).Rows(0).Item("EXISTE") = "S" Then
				Return True
			Else
				Return False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Function ConteosDispositivo_Existe(ByVal IdTerminal As String) As Boolean
		Try
			ConteosDispositivo_Existe = False
			Dim DS As New DataSet

			If Session("Perfil") IsNot Nothing AndAlso Session("Perfil") = "R" Then
				Dim PAR(4) As SqlParameter
				PAR(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				PAR(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
				PAR(2) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
				PAR(3) = New SqlParameter("@IdTerminal", IdTerminal)
				PAR(4) = New SqlParameter("@Conteo", rbtnNumConteo.SelectedValue)

				DS = DatosSQL.funcioncp("ConteosDispositivo_Buscar", PAR)
			Else
				Dim PAR(5) As SqlParameter
				PAR(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				PAR(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
				PAR(2) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
				PAR(3) = New SqlParameter("@IdTerminal", IdTerminal)
				PAR(4) = New SqlParameter("@Conteo", rbtnNumConteo.SelectedValue)
				PAR(5) = New SqlParameter("@idAlmacen", cboAlmacen.SelectedValue)

				DS = DatosSQL.funcioncp("ConteosDispositivo_Buscar", PAR)
			End If

			If Not DS Is Nothing AndAlso DS.Tables.Count > 0 AndAlso DS.Tables(0).Rows.Count > 0 Then
				Return True
			Else
				Return False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Private Sub rbtnNumConteo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnNumConteo.SelectedIndexChanged
		Try
			If Me.cboTerminal.Items.Count > 0 AndAlso Not Me.cboTerminal.SelectedIndex = -1 AndAlso Me.cboTerminal.SelectedValue <> "[Seleccione]" Then
				Me.ConteosDispositivo_Buscar()
			Else
				Me.dg.DataSource = Nothing
				Me.dg.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub RPT_MarbetesAdjuntados()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			'  If Session("Sociedad") = 1 Then 'C - Control
			ds = DatosSQL.funcioncp("RPT_MarbetesAdjuntados", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim report1 As dbATReport
				Dim settings As New dbAutoTrack.DataReports.PageSettings
				settings.PaperKind = Drawing.Printing.PaperKind.Letter
				settings.Orientation = PageOrientation.Portrait
				settings.Margins.MarginLeft = 0
				settings.Margins.MarginRight = 0
				settings.Margins.MarginTop = 0
				settings.Margins.MarginBottom = 0

				report1 = New RPT_MarbetesAdjuntadosrpt
				report1.PageSetting = settings
				Dim document1 As New PDFExport.PDFDocument

				Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbetesAdjuntados")

				If Not Directory.Exists(DirectorioArchivos) Then
					Directory.CreateDirectory(DirectorioArchivos)
				End If

				Dim pathPdf As String = DirectorioArchivos & Session("Perfil") & "_" & Session("IdUsuario") & ".pdf"
				report1.DataSource = ds.Tables(0)
				CType(report1.Sections.Item(1).Controls.Item("txtFecha"), dbATTextBox).Value = Today.ToShortDateString
				CType(report1.Sections.Item(1).Controls.Item("txtHora"), dbATTextBox).Value = Now.ToShortTimeString
				CType(report1.Sections.Item(1).Controls.Item("txtTienda"), dbATTextBox).Value = Session("IdTienda")
				CType(report1.Sections.Item(1).Controls.Item("TxtIdUsuario"), dbATTextBox).Value = Session("IdUsuario")

				CType(report1.Sections.Item(4).Controls.Item("txtTotalUnidades"), dbATTextBox).Value = ds.Tables(0).Rows(0).Item("TotalUnidades")
				CType(report1.Sections.Item(4).Controls.Item("txtTotalMarbetes"), dbATTextBox).Value = ds.Tables(0).Rows(0).Item("TotalMarbetes")

				document1.EmbedFont = True
				document1.Compress = True
				report1.Generate()
				document1.Export(report1.Document, pathPdf)

				Dim encripta As New Encryption
				Me.Link.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
				'If Session("ImpMarbetes") Then
				'	Me.Link.Visible = True
				'End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Excel_MarbetesAdjuntados()
		Dim ds As New DataSet
		Dim par(1) As SqlParameter
		par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
		par(1) = New SqlParameter("@Tipo", 1)
		ds = DatosSQL.funcioncp("RPT_MarbetesAdjuntados", par)
		If Not ds Is Nothing Then
			If ds.Tables(0).Rows.Count > 0 Then
				REPEXCEL(ds.Tables(0), Today.ToShortDateString, Today.ToShortTimeString, Session("IdTienda"))
				If Session("ImpMarbetes") Then
					Me.Link.Visible = True
					Me.Link2.Visible = True
				End If
			End If
		End If
	End Sub

	Sub REPEXCEL(ByVal DATOS As DataTable, ByVal Fecha As String, ByVal Hora As String, ByVal Tienda As String)
		Try
			'Crear el directorio si no existe

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbetesAdjuntados")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim Archivo As String = DirectorioArchivos & "MarbeteIndividualDispositivo_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"
			Dim TitulosArchivo As String = "Marbetes Adjuntados"
			'Guardar la información en el archivo txt
			Dim Subtitulo As String = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda & "  Row;" & DATOS.Rows.Count
			Reportes.Exportar_ExcelDinamico(DATOS, Archivo, TitulosArchivo, Subtitulo)

			Dim encripta As New Encryption
			Me.Link2.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
			'If Me.chkImprimirMarbetes.Checked Then
			'	Me.Link2.Visible = True
			'End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function ProcesarAccessCedis(ByVal SR As StreamReader, strRuta As String, Linea As String) As Integer
		Try
			Dim Sentencia As String = ""
			Dim SentenciaNueva As String = ""
			Dim cont As Integer = 0
			'@Accion INT, --1=Agregar 2=Modificar 3=Eliminar un registro 4=Eliminar registros para la tienda, usuario y perfil
			'Dim proceso As Double = (100 / (Datos.Rows.Count - 1))

			Dim IdRow, NameDis, Ubicacion, Marbete, Cant, Cbarras, Almacen, Bulto, BultoV As String
			Dim Registro() As String

			Do
				If Linea Is Nothing Then
					Exit Do
				End If
				Registro = Linea.Split("|")

				IdRow = Registro(0).Trim
				NameDis = Registro(1).Trim.ToUpper
				Ubicacion = Registro(2).Trim
				Marbete = Registro(3).Trim
				Cant = Registro(4).Trim
				Cbarras = Registro(5).Trim
				Almacen = Registro(6).Trim
				Bulto = Registro(7).Trim
				BultoV = Registro(8).Trim

				Dim IdTerminal As String = NameDis
				Dim EAN As String = Cbarras
				Dim Conteo As String = Cant

				SentenciaNueva = " EXEC ConteosDispositivo_Administracion 1" &
									 "," & Chr(39) & Session("IdTienda") & Chr(39) & "," & Chr(39) & Session("IdUsuario") & Chr(39) & "," &
								  Chr(39) & IdTerminal & Chr(39) & "," & Chr(39) & Marbete & Chr(39) & "," &
								IdRow.ToString & "," & Chr(39) & EAN & Chr(39) & "," &
							   Conteo.ToString & "," & Chr(39) & CStr(Session("Perfil")).Substring(0, 1) & Chr(39) &
							   "," & Chr(39) & Almacen & Chr(39) &
							   "," & Chr(39) & Ubicacion & Chr(39) &
							   "," & Chr(39) & Bulto & Chr(39) &
							   "," & Chr(39) & BultoV & Chr(39)

				If Len(Sentencia & SentenciaNueva) > 8000 Then
					DatosSQL.procedimientoText(Sentencia)
					Sentencia = ""
				End If
				Sentencia = Sentencia + SentenciaNueva
				cont = cont + 1

				Linea = SR.ReadLine()
			Loop Until Linea Is Nothing

			'For i As Integer = 0 To Datos.Rows.Count - 1
			'    Dim IdTerminal As String = Datos.Rows(i).Item("NameDIS")
			'    Dim IdMarbete As String = Datos.Rows(i).Item("Marbete")
			'    Dim IdRow As String
			'    If IsDBNull(Datos.Rows(i).Item("IdRow")) Then
			'        IdRow = "''"
			'    Else
			'        IdRow = Datos.Rows(i).Item("IdRow")
			'    End If
			'    Dim EAN As String = Datos.Rows(i).Item("CBARRAS")
			'    Dim Conteo As String = Datos.Rows(i).Item("Cant")
			'    Dim Bultos As String = Datos.Rows(i).Item("Bultos")
			'    Dim BultoV As String = CInt(Datos.Rows(i).Item("BultoV"))
			'    SentenciaNueva = " EXEC ConteosDispositivo_Administracion 1" & _
			'                         "," & Chr(39) & Session("IdTienda") & Chr(39) & "," & Chr(39) & Session("IdUsuario") & Chr(39) & "," & _
			'                      Chr(39) & IdTerminal & Chr(39) & "," & Chr(39) & IdMarbete & Chr(39) & "," & _
			'                    IdRow.ToString & "," & Chr(39) & EAN & Chr(39) & "," & _
			'                   Conteo.ToString & "," & Chr(39) & CStr(Session("Perfil")).Substring(0, 1) & Chr(39) & "," & _
			'                    Bultos & "," & BultoV

			'    If Len(Sentencia & SentenciaNueva) > 8000 Then
			'        DatosSQL.procedimientoText(Sentencia)
			'        Sentencia = ""
			'    End If
			'    Sentencia = Sentencia + SentenciaNueva
			'    cont = cont + 1
			'Next
			If Len(Sentencia) > 0 AndAlso Len(Sentencia) <= 8000 Then
				DatosSQL.procedimientoText(Sentencia)
			End If
			Return cont
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Almacenes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
			par(2) = New SqlParameter("@IdTerminal", Me.cboTerminal.SelectedValue)
			ds = DatosSQL.funcioncp("ConteosDispositivo_Terminales_Almacenes", par)

			Me.cboAlmacen.Items.Clear()
			Dim Row As DataRow = ds.Tables(0).NewRow
			Row("Almacen") = "0"
			Row("Almacen") = "[Seleccionar]"
			ds.Tables(0).Rows.InsertAt(Row, 0)
			ds.AcceptChanges()

			Me.cboAlmacen.DataSource = ds.Tables(0)
			Me.cboAlmacen.DataValueField = "Almacen"
			Me.cboAlmacen.DataTextField = "Almacen"
			Me.cboAlmacen.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Function ExisteInfAdjuntadaEnAlmacen() As Boolean
		Try
			Dim dt As DataSet
			Dim par(4) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			par(2) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
			par(3) = New SqlParameter("@IdTerminal", Me.cboTerminal.SelectedValue)
			par(4) = New SqlParameter("@idAlmacen", Me.cboAlmacen.SelectedValue)

			dt = DatosSQL.funcioncp("Existe_Administracion_Almacen", par)
			If Not dt Is Nothing AndAlso dt.Tables.Count > 0 AndAlso dt.Tables(0).Rows.Count > 0 AndAlso dt.Tables(0).Rows(0).Item("EXISTE") = "S" Then
				Return True
			Else
				Return False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Private Sub cboTerminal_SelectedIndexChanged1(sender As Object, e As System.EventArgs) Handles cboTerminal.SelectedIndexChanged
		Try
			Me.dg.Visible = False

			If Session("Sociedad").ToString = "3" Then
				ConteosDispositivo_Buscar()
			Else
				Almacenes_Buscar()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub dg_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dg.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			'Dim Rep As String = e.Row.Cells(14).Text
			'If Rep = "D" Then
			'    e.Row.BackColor = Drawing.Color.Yellow
			'ElseIf Rep = "M" Then
			'    e.Row.BackColor = Drawing.Color.LightGreen
			'End If
			Cont += 1
			e.Row.Cells(0).Text = Cont

			'dg.HeaderRow.Cells(0).Visible = False 'IdRow
			'e.Row.Cells(0).Visible = False
			'dg.HeaderRow.Cells(1).Visible = False 'Terminal
			'e.Row.Cells(1).Visible = False
			'dg.HeaderRow.Cells(3).Visible = False 'EAN
			'e.Row.Cells(3).Visible = False
			'dg.HeaderRow.Cells(4).Visible = False 'Num. Articulo
			'e.Row.Cells(4).Visible = False
			'dg.HeaderRow.Cells(5).Visible = False 'Descripcion
			'e.Row.Cells(5).Visible = False
			'dg.HeaderRow.Cells(7).Visible = False 'Costo
			'e.Row.Cells(7).Visible = False
			'dg.HeaderRow.Cells(8).Visible = False 'Precio
			'e.Row.Cells(8).Visible = False
			'dg.HeaderRow.Cells(10).Visible = False 'Cantidad
			'e.Row.Cells(10).Visible = False

			'dg.HeaderRow.Cells(6).Visible = False 'IdConteosDispositivo
			'e.Row.Cells(6).Visible = False
			'dg.HeaderRow.Cells(9).Visible = False 'Cantidad
			'e.Row.Cells(9).Visible = False
			'dg.HeaderRow.Cells(13).Visible = False 'Marcar
			'e.Row.Cells(13).Visible = False
			'dg.HeaderRow.Cells(16).Visible = False 'BultosV
			'e.Row.Cells(16).Visible = False
			'dg.HeaderRow.Cells(17).Visible = False 'BultosV
			'e.Row.Cells(17).Visible = False


		End If
	End Sub

	Private Sub cboAlmacen_SelectedIndexChanged1(sender As Object, e As System.EventArgs) Handles cboAlmacen.SelectedIndexChanged
		If Me.cboAlmacen.SelectedIndex = 0 AndAlso Session("Sociedad").ToString <> "3" Then
			Me.dg.Visible = False
			Me.btnAdjuntar.Enabled = False
			Me.btnEliminar.Enabled = False
			Me.btnGuardar.Enabled = False
		Else
			Me.dg.Visible = True
			ConteosDispositivo_Buscar()
		End If
	End Sub

	Protected Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chkHeader As CheckBox = CType(Me.dg.Controls(0).Controls(0).FindControl("Checkbox3"), CheckBox)
		'If chkHeader.Checked = True Then
		For x As Integer = 0 To Me.dg.Rows.Count - 1
			Dim chkRow As New CheckBox
			chkRow = CType(Me.dg.Rows(x).FindControl("Checkbox4"), CheckBox)
			chkRow.Checked = chkHeader.Checked
		Next
		'End If
	End Sub

    Protected Sub Checkbox3_CheckedChanged(sender As Object, e As EventArgs)
        For Each fila As GridViewRow In dg.Rows
            If fila.RowType = DataControlRowType.DataRow Then
                Dim checkbox As CheckBox = fila.FindControl("Checkbox4")

                Dim ChBoxEncabezado As CheckBox = dg.HeaderRow.FindControl("Checkbox3")
                checkbox.Checked = ChBoxEncabezado.Checked
            End If
        Next
    End Sub

    Protected Sub chkETDgArt_CheckedChanged(sender As Object, e As EventArgs)
        For Each fila As GridViewRow In dgArticulosSet.Rows
            If fila.RowType = DataControlRowType.DataRow Then
                Dim checkbox As CheckBox = fila.FindControl("chkEUnoDgArt")

                Dim ChBoxEncabezado As CheckBox = dgArticulosSet.HeaderRow.FindControl("chkETDgArt")
                checkbox.Checked = ChBoxEncabezado.Checked
            End If
        Next
    End Sub

    Protected Sub chkImprimirMarbetes_CheckedChanged(sender As Object, e As EventArgs) Handles chkImprimirMarbetes.CheckedChanged
		Session("ImpMarbetes") = chkImprimirMarbetes.Checked
	End Sub

	Private Sub Transferir()
		'TRANSFERIR INFORMACION DE CONTEODISPOSITIVO A CONTEOS
		Dim PAR(5) As SqlParameter
		PAR(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
		PAR(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
		PAR(2) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
		PAR(3) = New SqlParameter("@IdTerminal", Me.cboTerminal.SelectedValue)
		PAR(4) = New SqlParameter("@NumConteo", Me.rbtnNumConteo.SelectedValue)
		PAR(5) = New SqlParameter("@idAlmacen", IIf(Session("Sociedad").ToString = "3", "A001", Me.cboAlmacen.SelectedValue))
		Dim ds As DataSet

		'cedis si  lleva ubicación

		ds = DatosSQL.funcioncp("ConteosDispositivo_Transferir", PAR)

		Dim marbete As String
		Dim almacen As String
		Dim mensaje As String

		If ds.Tables(0).Rows.Count > 0 Then
			For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
				marbete = ds.Tables(0).Rows(i).Item("marbete")
				almacen = ds.Tables(0).Rows(i).Item("almacen")
				mensaje &= "El Marbete " & marbete & " ya se encuentra en el almacén " & almacen & "," & vbCrLf
			Next
			'CMensajes.Show(mensaje & vbCrLf)
			CMensajes.MostrarMensaje(mensaje, CMensajes.Tipo.GetError, wucMessageMensaje)
		Else
			If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 AndAlso Session("Cedis") <> 1 Then
				Me.dg.Visible = False
				Me.btnEliminar.Visible = False
				Me.btnGuardar.Visible = False
				Me.btnAdjuntar.Visible = False
				Me.dgArticulosSet.DataSource = ds.Tables(1)
				Me.dgArticulosSet.DataBind()
				Me.dgArticulosSet.Columns(4).Visible = False
				Me.dgArticulosSet.Columns(5).Visible = False
				REPORTE(ds, "Articulos SET")
				Me.linkArtSet.Visible = True
				Me.dgArticulosSet.Visible = True
				Me.btnEliminarArticulosSet.Visible = True
				CMensajes.MostrarMensaje("Existe materiales Set en los registros que requiere adjuntar, favor de corregir y tomar los materiales individualmente.", CMensajes.Tipo.GetError, wucMessageMensaje)
			End If
			'CMensajes.Show("Información Adjuntada correctamente.")
			Almacenes_Buscar()
			Me.dg.DataSource = Nothing
			Me.dg.DataBind()
			Me.lblTiempoProceso.Text = ""
			Me.btnAdjuntar.Enabled = False
			Me.btnEliminar.Enabled = False
			Me.btnGuardar.Enabled = False
			Me.Terminales_Buscar()
			If Session("ImpMarbetes") Then
				RPT_MarbeteIndividual() 'Nuevo llamado a generación de reporte RUTM 
				Excel_MarbetesAdjuntados()
				Me.Link.Visible = True
				Me.Link2.Visible = True
			End If
			CMensajes.MostrarMensaje("Información adjuntada correctamente.", CMensajes.Tipo.GetExito, wucMessageTransferirInfDispositivo)
		End If
	End Sub

	''' <summary>
	''' Valida que exista el marbete en un rango
	''' </summary>
	''' <param name="IdTerminal"></param>
	''' <returns></returns>
	Private Function ValidarRango(ByVal IdTerminal As String) As List(Of String)
		Try
			Dim par1(1) As SqlParameter
			par1(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par1(1) = New SqlParameter("@IdTerminal", IdTerminal)

			Dim ds1 As DataSet
			ds1 = DatosSQL.funcioncp("ConteosDispositivo_Validar", par1)

			If ds1 IsNot Nothing AndAlso ds1.Tables.Count > 0 AndAlso ds1.Tables(0).Rows.Count Then
				Dim marbetes As New List(Of String)

				For Each marbete As DataRow In ds1.Tables(0).Rows
					Dim PAR(2) As SqlParameter
					PAR(0) = New SqlParameter("@Accion", 4)
					PAR(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
					PAR(2) = New SqlParameter("@RangoInicial", marbete(0))

					Dim ds As DataSet

					If Session("Perfil") = "R" Then
						ds = DatosSQL.funcioncp("CatalogoMarbetesRango_Administracion", PAR)
					Else
						ds = DatosSQL.funcioncp("CatalogoMarbetesRango_Administracion2", PAR)
					End If

					If Not (ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0) Then
						marbetes.Add(marbete(0))
					End If
				Next

				Return marbetes
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Protected Sub btnEliminarArticulosSet_Click(sender As Object, e As EventArgs) Handles btnEliminarArticulosSet.Click
		EliminarArticulosSet()
	End Sub

	Sub EliminarArticulosSet()
		Try
			Dim Sentencia As String = ""
			Dim SentenciaNueva As String = ""
			For i As Integer = 0 To Me.dgArticulosSet.Rows.Count - 1
				Dim ck As CheckBox = CType(dgArticulosSet.Rows(i).Cells(3).Controls(1), CheckBox)
				If ck.Checked = True Then
                    SentenciaNueva = " EXEC EliminarArticulosSet @IdTienda='" & Session("IdTienda") & "',@IdUsuario='" & Session("IdUsuario") & "',@Perfil='" & Session("Perfil") & "',@IdTerminal='" & dgArticulosSet.Rows(i).Cells(4).Text & "',@IdAlmacen='" & dgArticulosSet.Rows(i).Cells(5).Text & "',@IdMarbete='" & dgArticulosSet.Rows(i).Cells(0).Text & "',@EAN='" & dgArticulosSet.Rows(i).Cells(1).Text & "'"

                    If Len(Sentencia & SentenciaNueva) > 8000 Then
						DatosSQL.procedimientoText(Sentencia)
						Sentencia = ""
					End If
					Sentencia = Sentencia + SentenciaNueva
				End If
			Next
			If Len(Sentencia) > 0 AndAlso Len(Sentencia) <= 8000 Then
				DatosSQL.procedimientoText(Sentencia)
			End If
			Response.Redirect("TransferirInfDispositivo.aspx?P=" & Session("Perfil") & "&SID=" & Session("SID"), False)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class