﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.IO

Public Class SiteMaster
	Inherits MasterPage
	Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
		Try
			If Not Directory.Exists("C:\SAIC\") Then
				Directory.CreateDirectory("C:\SAIC\")
			End If

			Me.lblTienda.Text = ""
			Me.lblTienda.Text = "Tienda: " & Session("IdTienda").ToString.Trim

			Dim cNegocioTienda As New CNegocioTienda With {.Accion = 3,
														   .Tienda = Session("IdTienda")}

			If cNegocioTienda.Cedis() Then
				Throw New Exception(cNegocioTienda.ErrorMensaje)
			End If
			If cNegocioTienda.dsRespuesta IsNot Nothing AndAlso cNegocioTienda.dsRespuesta.Tables.Count > 0 AndAlso cNegocioTienda.dsRespuesta.Tables(0).Rows.Count > 0 Then
				Session("Cedis") = cNegocioTienda.dsRespuesta.Tables(0).Rows(0)(0)
				Session("Sociedad") = cNegocioTienda.dsRespuesta.Tables(0).Rows(0)(1)
			End If

			Dim ds As New DataSet
			Dim param(0) As SqlParameter
			param(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("Encabezado_MostrarMenu", param)
			If ds.Tables(1).Rows.Count > 0 Then
				'Fecha inventario y tipo inventario
				Me.lblFechaInv.Text = ds.Tables(1).Rows(0).Item(0)
				Session("lblFechaInv") = ds.Tables(1).Rows(0).Item(0)
				Session("TipoInventario") = ds.Tables(1).Rows(0).Item(1)
			End If

			If Not Page.IsPostBack Then
				If Session("URL") Is Nothing OrElse Session("URL") = "" Then
					Dim url(0) As String
					url = Request.Url.ToString.Split("?")
					url(0) = url(0).Replace("Inicio.aspx", "")
					Session("URL") = url(0)
				End If

				Me.cmbPerfil.SelectedValue = Session("Perfil")
				If ValidaSesion() Then
					Me.cmbPerfil.SelectedValue = Session("Perfil")
					GenerarMenu()
					lblUsuario.Text = Session("NombreUsuario")
					LlenaPerfil()
					If Me.cmbPerfil.Items.Count > 0 Then
						If Not Session("Entrar") = True Then
							ObtenerPerfil()
							GenerarMenu()
							CambiaPaginaPerfil()
							Session("Entrar") = True
						End If
					End If
				End If
			Else
				If Me.cmbPerfil.SelectedValue <> Session("Perfil") Then
				Else
					Me.cmbPerfil.SelectedValue = Session("Perfil")
					Session("Perfil") = Me.cmbPerfil.SelectedValue.ToString
					Session("TextPerfil") = Me.cmbPerfil.SelectedItem.ToString
				End If
			End If
		Catch ex As Exception

		End Try

		lnkCerrar.Attributes.Add("onclick", "window.close();")
	End Sub

	Protected Sub lnkCerrar_Click(sender As Object, e As EventArgs) Handles lnkCerrar.Click
		cerrarSession()
		Session("Entrar") = Nothing
	End Sub

	Private Sub cmbPerfil_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPerfil.SelectedIndexChanged
		ObtenerPerfil()
		GenerarMenu()
		CambiaPaginaPerfil()
		Mostrar_Mensaje("Cambio de perfil correcto.")
	End Sub

	''' <summary>
	''' Valida que la sesión exista
	''' </summary>
	''' <returns></returns>
	Function ValidaSesion() As Boolean
		' valida que exista la sesion
		Dim idSesion As String = Session("SID") ' SID (identificador de sesion del usuario)
		If idSesion Is Nothing OrElse idSesion.Trim = String.Empty Then
			Return False
		End If

		Dim CSesiones As New CSesiones With {.Accion = CSesiones.TipoAccion.GetExists,
									 .idSesion = idSesion}
		If CSesiones.Ejecuta_sesiones Then
			Throw New Exception(CSesiones.ErrorMensaje)
		End If

		Try
			' el metodo regresa el campo "existe" con valor "1" cuando la sesion existe
			If CSesiones.ValidarDatos Then
				Return True
			Else
				' el metodo regresa el campo "existe" con valor "0" cuando la sesion no existe
				Return False
			End If
		Catch ex As Exception
			'MsgBox("Error al determinar si existe la sesion " & ex.Message)
		End Try
	End Function

	''' <summary>
	''' Genera el menú de opciones por medio del control
	''' </summary>
	Sub GenerarMenu()
		Me.menuSAIC.idSesion = Session("SID") ' define propiedadSID (identificador de sesion del usuario)
		Me.menuSAIC.consultaPermisos()  ' ejecuta metodo para formar el menu del usuario
	End Sub

	''' <summary>
	''' Llena combo de perfil de inventario
	''' </summary>
	Private Sub LlenaPerfil()
		Dim dt As DataTable = New DataTable("Tabla")

		dt.Columns.Add("Perfil")
		dt.Columns.Add("Valor")

		Dim dr As DataRow

		If Not Session("Cedis") = 1 Then
			dr = dt.NewRow()
			dr("Perfil") = "Preconteo"
			dr("Valor") = "P"
			dt.Rows.Add(dr)
		End If

		dr = dt.NewRow()
		dr("Perfil") = "Rotativo"
		dr("Valor") = "R"
		dt.Rows.Add(dr)

		Me.cmbPerfil.DataSource = dt
		Me.cmbPerfil.DataTextField = "Perfil"
		Me.cmbPerfil.DataValueField = "Valor"
		Me.cmbPerfil.DataBind()
	End Sub

	''' <summary>
	''' Obtiene perfil de inventario
	''' </summary>
	Private Sub ObtenerPerfil()
		Session("Perfil") = Me.cmbPerfil.SelectedValue
	End Sub

	''' <summary>
	''' Cambia perfil de inventario
	''' </summary>
	Sub CambiaPaginaPerfil()
		'Session("Perfil") = Me.cmbPerfil.SelectedValue.ToString
		Me.cmbPerfil.SelectedValue = Session("Perfil")
		Response.Redirect("Inicio.aspx?SID=" & Session("SID"), False)
	End Sub

	''' <summary>
	''' Cierra sesión
	''' </summary>
	Sub cerrarSession()
		Try
			Dim Csesiones As New CSesiones With {.idSesion = Request.QueryString("SID")}
			If Csesiones.DestruyeSesion() Then
				Throw New Exception(Csesiones.ErrorMensaje)
			Else
				HttpContext.Current.Session.Clear()
				Response.Redirect("~/Default.aspx", False)
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Function ExpandPath(ByVal sender As Object, ByVal e As SiteMapResolveEventArgs) As SiteMapNode
		Dim dtUrls As New DataTable
		Dim SID As String = getSID()

		dtUrls = CType(Session("dtUrlsSiteMap" & SID), DataTable)
		Dim currentNode As SiteMapNode = Nothing

		If dtUrls IsNot Nothing AndAlso dtUrls.Rows.Count > 0 AndAlso Not IsNothing(SiteMap.CurrentNode) Then
			currentNode = SiteMap.CurrentNode.Clone(True)
		ElseIf dtUrls IsNot Nothing AndAlso dtUrls.Rows.Count > 0 AndAlso SiteMap.CurrentNode Is Nothing Then
			Dim s As String = HttpContext.Current.Request.RawUrl
			If s.Contains("&SID") Then
				s = s.Remove(s.IndexOf("&SID"))
				currentNode = SiteMap.Provider.FindSiteMapNode(s).Clone(True)
			End If
		End If

		If dtUrls IsNot Nothing AndAlso dtUrls.Rows.Count > 0 AndAlso currentNode IsNot Nothing Then
			Dim tempNode As SiteMapNode = currentNode
			'Revisar si el parent node tiene parametros y coincide con uno existente en sitemap

			Dim i As Integer

			i = dtUrls.Rows.IndexOf(dtUrls.Rows.Find(tempNode.Url))
			If i > -1 Then tempNode.Url = tempNode.Url & dtUrls.Rows(i).Item("urlQuery").ToString

			tempNode = tempNode.ParentNode

			Do While (tempNode IsNot Nothing AndAlso tempNode.ToString.Trim IsNot String.Empty)
				i = dtUrls.Rows.IndexOf(dtUrls.Rows.Find(tempNode.Url))
				If i > -1 Then tempNode.Url = tempNode.Url & dtUrls.Rows(i).Item("urlQuery").ToString

				If Not tempNode.Url.Contains("SID=") AndAlso Not tempNode.Url.Contains("?") Then
					tempNode.Url = String.Format(tempNode.Url & "?SID={0}", SID)
				ElseIf Not tempNode.Url.Contains("SID=") AndAlso tempNode.Url.Contains("?") Then
					tempNode.Url = String.Format(tempNode.Url & "&SID={0}", SID)
				End If

				tempNode = tempNode.ParentNode
			Loop
		End If

		Return currentNode
	End Function

	''' <summary>
	''' Obtiene SID
	''' </summary>
	''' <returns></returns>
	Function getSID() As String
		Try
			Return HttpContext.Current.Request.QueryString("SID").ToString
		Catch ex As Exception
			Return ""
		End Try
	End Function

	Private Sub ObtenerURL(path As String, query As String)
		Dim r As DataRow
		Dim dtUrls As New DataTable

		dtUrls = CType(Session("dtUrlsSiteMap" & Request.QueryString("SID")), DataTable)

		If dtUrls Is Nothing Then
			Dim dt As New DataTable
			dt.TableName = Request.QueryString("SID").ToString
			dt.Columns.Add("urlPath")
			dt.Columns.Add("urlQuery")

			Dim pk(0) As DataColumn
			pk(0) = dt.Columns("urlPath")

			dt.PrimaryKey = pk

			r = dt.NewRow
			r.Item(0) = path
			r.Item(1) = query
			dt.Rows.InsertAt(r, 0)

			dtUrls = dt
		ElseIf dtUrls.Rows.Count > 0 Then
index:
			Dim i As Integer = dtUrls.Rows.IndexOf(dtUrls.Rows.Find(path))
			If i < 0 Then
				'No existe
				r = dtUrls.NewRow
				r.Item(0) = path
				r.Item(1) = query
				Try
					dtUrls.Rows.InsertAt(r, 0)
				Catch ex As Data.ConstraintException
					GoTo index
				End Try
			Else
				r = dtUrls.Rows(i)
				r.Item(1) = query
				Try
					dtUrls.AcceptChanges()
				Catch ex As Data.ConstraintException
					GoTo index
				End Try
			End If
		End If

		Session("dtUrlsSiteMap" & Request.QueryString("SID")) = dtUrls
	End Sub

	Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
		Try
			' define la cultura a español mexico
			Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")
			' Define el UI culture a español mexico
			Thread.CurrentThread.CurrentUICulture = New CultureInfo("es-MX")
			AddHandler SiteMap.SiteMapResolve, AddressOf ExpandPath
			Session("SID") = Request.QueryString("SID")

			Session("PopUpError") = False
			Dim cRequest As HttpRequest = HttpContext.Current.Request
			If cRequest.Path.ToLowerInvariant().IndexOf(".aspx") > -1 Then
				ObtenerURL(cRequest.Url.AbsolutePath, cRequest.Url.Query)
			End If
		Catch ex As Exception

		End Try
	End Sub

	''' <summary>
	''' Muestra mensaje
	''' </summary>
	''' <param name="mensaje"></param>
	Sub Mostrar_Mensaje(ByVal mensaje As String)
		mensaje = "alert('" & mensaje & "');"
		ScriptManager.RegisterClientScriptBlock(Me.Page, Me.GetType, Guid.NewGuid.ToString, mensaje, True)
	End Sub


End Class