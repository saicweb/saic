﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading

Public Class DescargaArchivoCierre
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		Me.wucMessageDecargaArchivoCierre.Visible = False

		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
		End If

		If Not Page.IsPostBack Then
			Me.txtfechainicio.Text = CDate(Today.Date).ToString("yyy-MM-dd")
			Me.txtfechafin.Text = CDate(Today.Date).ToString("yyy-MM-dd")
		End If
	End Sub

	Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
		Try
			Thread.CurrentThread.CurrentCulture = New CultureInfo("es-MX")
			Dim FechaInicio As DateTime
			Dim FechaFin As DateTime
			FechaInicio = New DateTime(Convert.ToDateTime(txtfechainicio.Text).Year, Convert.ToDateTime(txtfechainicio.Text).Month, Convert.ToDateTime(txtfechainicio.Text).Day, 0, 0, 0)
			FechaFin = New DateTime(Convert.ToDateTime(txtfechafin.Text).Year, Convert.ToDateTime(txtfechafin.Text).Month, Convert.ToDateTime(txtfechafin.Text).Day, 23, 59, 59)
			If FechaInicio <= FechaFin Then
				Dim ds As New DataSet
				Dim par(2) As SqlParameter
				par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(1) = New SqlParameter("@FechaInicio", FechaInicio)
				par(2) = New SqlParameter("@FechaFin", FechaFin)
				ds = DatosSQL.funcioncp("CI_DescargaArchivoCierre", par)
				If ds.Tables(0).Rows.Count > 0 Then
					Me.grid.DataSource = ds.Tables(0)
					Me.grid.DataBind()
				Else
					Me.grid.DataSource = Nothing
					Me.grid.DataBind()
					CMensajes.MostrarMensaje("No existe información disponible.", CMensajes.Tipo.GetError, wucMessageDecargaArchivoCierre)
				End If
			Else
				CMensajes.MostrarMensaje("La fecha inicio debe ser menor a la fecha fin.", CMensajes.Tipo.GetError, wucMessageDecargaArchivoCierre)
			End If
		Catch ex As Exception
			CMensajes.Show(ex.Message)
		End Try

	End Sub

	Private Sub grid_DataBound(sender As Object, e As System.EventArgs) Handles grid.DataBound
		Try
			For x As Integer = 0 To Me.grid.Rows.Count - 1
				Dim link As New HtmlAnchor
				Dim Arch As String = Me.grid.Rows(x).Cells(4).Text

				Dim encripta As New Encryption
				link.HRef = "FileDownload.aspx?file=" & encripta.encryptString(Me.grid.Rows(x).Cells(5).Text)
				link.InnerHtml = Arch

				If Me.grid.Rows(x).Cells(4).Controls.Count = 0 Then
					Me.grid.Rows(x).Cells(4).Controls.Add(link)
				End If

			Next
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Sub

	Private Sub grid_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grid.RowDataBound
		If e.Row.RowType = DataControlRowType.DataRow Then
			grid.HeaderRow.Cells(5).Visible = False
			e.Row.Cells(5).Visible = False
		End If
	End Sub
End Class