﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_MarbeteIndividual.aspx.vb" Inherits="Saic.RPT_MarbeteIndividual" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Impresión de marbetes por rango" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageMarbeteIndividual" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" cellpadding="10" id="TblCabecero">
                    <tr>
                        <td colspan="4" align="center">
                            <asp:radiobuttonlist id="rbtnConteos" runat="server"  AutoPostBack="true" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
							    <asp:ListItem Value="1" Selected="true">Conteo 1</asp:ListItem>
							    <asp:ListItem Value="2">Conteo 2</asp:ListItem>
                                <asp:ListItem Value="3">Conteo 3</asp:ListItem>
						    </asp:radiobuttonlist>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Rango inicial: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboInicioRango" runat="server" Width="150px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="Rango final: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboFinalRango" runat="server" Width="150px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4"> <br />
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnVerReporte" CssClass="btn" CausesValidation="false"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>
