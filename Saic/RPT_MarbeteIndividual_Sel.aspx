﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RPT_MarbeteIndividual_Sel.aspx.vb" Inherits="Saic.RPT_MarbeteIndividual_Sel" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Impresión de marbetes por selección" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageMarbeteIndividual_Sel" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server">
                    <tr>
                        <td colspan="4" align="center">
                            <asp:radiobuttonlist id="rbtnConteos" runat="server"  AutoPostBack="true" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
							    <asp:ListItem Value="1" Selected="true">Conteo 1</asp:ListItem>
							    <asp:ListItem Value="2">Conteo 2</asp:ListItem>
                                <asp:ListItem Value="3">Conteo 3</asp:ListItem>
						    </asp:radiobuttonlist>
                        </td>
                    </tr>
                    <tr>
                        <td><br />
                            <asp:Panel runat="server" Height="300px" Width="300px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="Grid" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:TemplateField HeaderText="Todos" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:CheckBox id="Checkbox3" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" runat="server" Text="Todos&lt;br /&gt;" TextAlign="Left"></asp:CheckBox>
                                            </HeaderTemplate>
						                    <ItemTemplate>
                                                <asp:CheckBox id="Checkbox4" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
					                    </asp:TemplateField>
								        <asp:BoundField HeaderText="Marbete" DataField="idMarbete"  ItemStyle-Width="200px" HeaderStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="1" align="center"><br />
                            <asp:RadioButtonList runat="server" ID="rbtnRPTFormatos" RepeatDirection="Horizontal" CssClass="radio inline" TextAlign="Right" CellPadding="15">
                                <asp:ListItem Value="PDF" Selected="True">PDF</asp:ListItem>
								<asp:ListItem Value="EXCEL">EXCEL</asp:ListItem>
                            </asp:RadioButtonList><br /><br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnVerReporte" CssClass="btn"/></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" align="center"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="False">Descargar reporte</asp:hyperlink></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
