﻿Imports System.Data.SqlClient
Imports System.IO
Imports dbAutoTrack.DataReports

Public Class RPT_DiferenciasDeptoEAN_PrevioCierre
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageDiferenciasPorDepartamento.Visible = False

		If Not Page.IsPostBack Then
			Almacen_Buscar()
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			If Me.txtDeptoInicial.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese el departamento inicial.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
			ElseIf Me.txtDeptoFinal.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese el departamento final.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
			ElseIf IsNumeric(Me.txtDeptoInicial.Text.Trim) = False Then
				CMensajes.MostrarMensaje("El departamento inicial debe ser un dato numérico.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
			ElseIf IsNumeric(Me.txtDeptoFinal.Text.Trim) = False Then
				CMensajes.MostrarMensaje("El departamento final debe ser un dato numérico.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
			ElseIf CInt(Me.txtDeptoInicial.Text.Trim) > CInt(Me.txtDeptoFinal.Text.Trim) Then
				CMensajes.MostrarMensaje("El rango de departamentos es incorrecto.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
			Else
				Dim ds As New DataSet
				Dim par(5) As SqlParameter
				par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(1) = New SqlParameter("@DeptoInicial", CInt(Me.txtDeptoInicial.Text.Trim))
				par(2) = New SqlParameter("@DeptoFinal", CInt(Me.txtDeptoFinal.Text.Trim))
				par(3) = New SqlParameter("@IdSociedad", Session("Sociedad"))
				par(4) = New SqlParameter("@TipoUsuario", Session("TipoUsuario"))
				par(5) = New SqlParameter("@Almacen", Me.cboAlmacen.SelectedValue)

				If Me.rbtnRPTFormatos.Items(0).Selected = True Then
					ds = DatosSQL.funcioncp("RPT_DifDeptoPrevioCierre", par)
				Else
					ds = DatosSQL.funcioncp("RPT_DifDeptoPrevioCierreExcel", par)
				End If

				If Not ds Is Nothing Then
					If ds.Tables(0).Rows.Count > 0 Then
						Me.HyperLink1.Visible = True
						If Me.rbtnRPTFormatos.Items(0).Selected = True Then
							REPORTE(ds)
						Else
							REPEXCEL(ds.Tables(0), Today.ToShortDateString, Now.ToShortTimeString, Session("IdTienda"), Me.cboAlmacen.SelectedValue)
						End If
					Else
						CMensajes.MostrarMensaje("No existen artículos por mostrar.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
					End If
				Else
					CMensajes.MostrarMensaje("No existen artículos por mostrar.", CMensajes.Tipo.GetError, wucMessageDiferenciasPorDepartamento)
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPEXCEL(ByVal DATOS As DataTable, ByVal Fecha As String, ByVal Hora As String, ByVal Tienda As String, ByVal Almacen As String)
		Try
			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("DiferenciasPorDepartamentoPrevioAlCierre")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim Archivo As String = DirectorioArchivos & "DiferenciasPorDepartamentoPrevioAlCierre_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"
			Dim TitulosArchivo As String = "Diferencias Por Departamento Antes Del Cierre De Inventario"
			'Guardar la información en el archivo txt
			Dim Subtitulo As String = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda & "       Almacen: " & Almacen
			Reportes.Exportar_ExcelDinamico(DATOS, Archivo, TitulosArchivo, Subtitulo)
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPORTE(ByVal DATOS As DataSet)
		Try
			'//////////////////////////////////////////////////////////////////////
			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim settings As New dbAutoTrack.DataReports.PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Landscape
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			report1 = New RPT_DiferenciasPorDepartamento2rpt
			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("DiferenciasPorDepartamentoPrevioAlCierre")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim pathPdf As String = DirectorioArchivos & "DiferenciasPorDepartamentoPrevioAlCierre_" & Session("Perfil") & "_" & Session("IdUsuario") & ".pdf"
			report1.DataSource = DATOS

			CType(report1.Sections.Item(0).Controls.Item("DbATLabel1"), dbATLabel).Text = "REPORTE DE DIFERENCIAS POR DEPARTAMENTO ANTES DEL CIERRE DE INVENTARIO"
			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
			CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")
			CType(report1.Sections.Item(0).Controls.Item("txtAlmacen"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Almacen")

			document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", "R")
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@validar", 2)
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class