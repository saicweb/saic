﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="CambiarPassword.aspx.vb" Inherits="Saic.CambiarPassword" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%"  >
        <tr>
            <td class="titulos">
                Cambiar password
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageCambiarPassword" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="40%" align="center">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Password anterior: "></asp:Label>
                        </td>
                        <td>         
                            <asp:TextBox ID="txtPassAnterior" runat="server" MaxLength="10" Width="100px" TextMode="Password"></asp:TextBox> 
                            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server"
                                ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9].{7}.*)$"
                                ControlToValidate="txtPassAnterior" ErrorMessage="*"
                                ForeColor="Red" Font-Size="Medium">
                            </asp:RegularExpressionValidator>
                            <asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" ControlToValidate="txtPassAnterior" Display="Dynamic"
								ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:requiredfieldvalidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Password nuevo: "></asp:Label>
                        </td>
                        <td>           
                            <asp:TextBox ID="txtPassword" runat="server" MaxLength="10" Width="100px" TextMode="Password"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9].{7}.*)$"
                                ControlToValidate="txtPassword" ErrorMessage="*"
                                ForeColor="Red" Font-Size="Medium">
                            </asp:RegularExpressionValidator>
                            <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" Display="Dynamic"
								ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:requiredfieldvalidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Confirme password: "></asp:Label>
                        </td>
                        <td>           
                            <asp:TextBox ID="txtConPass" runat="server" MaxLength="10" Width="100px" TextMode="Password"></asp:TextBox> 
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9].{7}.*)$"
                                ControlToValidate="txtConPass" ErrorMessage="*"
                                ForeColor="Red" Font-Size="Medium">
                            </asp:RegularExpressionValidator>
                            <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtConPass" Display="Dynamic"
								ErrorMessage="*" ForeColor="Red" Font-Size="Medium"></asp:requiredfieldvalidator>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2" align="center"><br />
                            <span><asp:Button runat="server" Text="Actualizar" ID="btnCambiar" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
