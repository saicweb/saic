﻿Imports System.IO
Imports System.Data.SqlClient

Public Class SubirArchivoProvExterno
	Inherits System.Web.UI.Page

	Dim dtDatos As DataTable
	Dim dtErrores As DataTable

	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
		Me.wucMessageMensaje.Visible = False
		Me.wucMessageSubirArchivoProvExterno.Visible = False

		If Not Page.IsPostBack Then
			If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Cedis") Is Nothing OrElse Session("Perfil") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If
			If Session("Perfil") = "P" Then
				Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"), False)
			End If
			CargarAlmacenActual()
			Me.btnCargar.Attributes.Add("onclick", "this.disabled=true;" + GetPostBackEventReference(btnCargar).ToString())
		End If
	End Sub

	Sub dtErrores_Configurar()
		Try
			dtErrores = New DataTable

			' Estructura de la tabla
			dtErrores.Columns.Add("Renglon", GetType(String))
			dtErrores.Columns.Add("Mensaje", GetType(String))

			Session("dtErrores") = dtErrores
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub dtDatos_Configurar()
		Try
			dtDatos = New DataTable
			' Estructura de la tabla

			dtDatos.Columns.Add("Usuario", GetType(String))
			dtDatos.Columns.Add("IdMarbete", GetType(String))
			dtDatos.Columns.Add("Cantidad", GetType(Double))
			dtDatos.Columns.Add("EAN", GetType(String))
			dtDatos.Columns.Add("IdRow", GetType(Integer))

			Session("dtDatos") = dtDatos
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub dtErrores_Agregar(ByVal Renglon As String, ByVal Mensaje As String)
		Try
			dtErrores = CType(Session("dterrores"), DataTable)
			Dim drErrores As DataRow
			drErrores = dtErrores.NewRow

			drErrores("Renglon") = Renglon
			drErrores("Mensaje") = Mensaje

			dtErrores.Rows.Add(drErrores)
			Session("dtErrores") = dtErrores
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Function Tienda_Existe(ByVal IdTienda As String) As Boolean
		Tienda_Existe = False
		Try
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", IdTienda)
			Dim ds As New DataSet
			ds = DatosSQL.funcioncp("Tiendas_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Return True
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Private Sub btnCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargar.Click
		Try
			Me.btnCargar.Enabled = False
			Me.btnCargar.Text = "Subiendo archivo..."

			Session("Almacen") = Me.cboAlmacenActual.SelectedValue
			Me.lblTiempoProceso.Text = "Hora de inicio: " & Now.ToShortTimeString
			Dim procede As Boolean
			CMensajes.MostrarMensaje("Procesando...", CMensajes.Tipo.GetInformativo, wucMessageMensaje)
			procede = CargarArchivo(5)
			Me.lblTiempoProceso.Text &= "  Hora de término: " & Now.ToShortTimeString
			Me.wucMessageMensaje.Visible = False

			Me.btnCargar.Enabled = True
			Me.btnCargar.Text = "Cargar archivo"
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	''' <summary>
	''' Cargar archivo
	''' </summary>
	''' <param name="columnas"></param>
	''' <returns></returns>
	Private Function CargarArchivo(ByVal columnas As Integer)
		Try
			Dim strRuta As String
			Dim nomArchivo As String
			Dim singleResultExist As Boolean = singleUpload.HasFile
			If singleResultExist Then
				nomArchivo = singleUpload.FileName
				If nomArchivo.ToUpper.Substring(0, 4) <> "GRAN" Then
					CMensajes.MostrarMensaje("El nombre del archivo debe inciar con el prefijo 'GRAN'", CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
					Return False
				ElseIf singleUpload.FileName.ToUpper.EndsWith(".TXT") Then
					'Crear el directorio si no existe
					Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Archivos")

					If Not Directory.Exists(DirectorioArchivos) Then
						Directory.CreateDirectory(DirectorioArchivos)
					End If

					strRuta = DirectorioArchivos
					'nomArchivo = singleUpload.UploadedFiles(0).GetName
					strRuta &= nomArchivo
					Session("nomArchivo") = nomArchivo

					singleUpload.SaveAs(strRuta)
					'Revisarlo para ver si aplica como archivo correcto
					Dim Archivo As New FileInfo(strRuta)

					Me.DTSDatos(Archivo.FullName, columnas)
					'Me.GuardarArchivo(Archivo, columnas)
					Return True
				Else
					CMensajes.MostrarMensaje("El archivo a cargar debe ser un archivo con extensión .txt", CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
					Return False
				End If
			Else
				CMensajes.MostrarMensaje("No se ha especificado el nombre del archivo a cargar.", CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
				Return False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Private Function GuardarArchivo(ByVal Archivo As FileInfo, ByVal Columnas As Integer)
		Dim LECTOR As New StreamReader(Archivo.FullName)
		Try
			Me.dtErrores_Configurar()
			Me.dtDatos_Configurar()
			dtDatos = CType(Session("dtDatos"), DataTable)
			Dim IdTienda As String = ""
			Dim TiendaOK As Boolean = False
			'VARIABLES DE LOS DATOS
			'Dim DocumentoInventario, CampoFijo1, CampoFijo2, CodigoMaterial, IdTienda, IdAlmacen, UnidadMedidaInventario As String
			'Dim Consecutivo, Ejercicio As Integer
			'Dim InventarioTeorico As Decimal

			Dim LINEA, RenCorrectos As String
			Dim i As Integer = 0
			Do
				LINEA = LECTOR.ReadLine()
				'si ya llego al fin de los rows
				If LINEA Is Nothing Then
					Exit Do
				End If
				i = i + 1
				'leer datos 
				Dim datos() As String
				datos = LINEA.Split(",")
				If datos.Length <= 1 Then
					Exit Do
				ElseIf datos.Length > Columnas Then
					Me.dtErrores_Agregar(i, "El numero de columnas del archivo es mayor a " & CStr(Columnas))
					Exit Do
				Else
					If datos(0) = "" Then 'Usuario
						Me.dtErrores_Agregar(i, "No se encontro el Usuario.")
					ElseIf datos(1) = "" Then 'IdMarbete
						Me.dtErrores_Agregar(i, "No se encontro el campo IdMarbete.")
					ElseIf datos(2) = "" Then 'Cantidad
						Me.dtErrores_Agregar(i, "No se encontro el campo Cantidad.")
					ElseIf IsNumeric(datos(2)) = False Then
						Me.dtErrores_Agregar(i, "El campo Cantidad debe ser un dato numerico.")
					ElseIf datos(3) = "" Then 'EAN
						Me.dtErrores_Agregar(i, "No se encontro el campo EAN.")
					ElseIf datos(4) = "" Then 'IdRow
						Me.dtErrores_Agregar(i, "No se encontro el campo IdRow.")
					ElseIf IsNumeric(datos(4)) = False Then
						Me.dtErrores_Agregar(i, "El campo IdRow debe ser un dato numerico.")
					Else
						If TiendaOK = False Then
							IdTienda = Left(datos(1), 3)
							If Session("Cedis") = "1" Then 'ES UN CEDIS, ID TIENDA PRIMEROS 4 CARACTERES
								IdTienda = Left(datos(1), 4)
							End If
							If (Session("Cedis") = "0" AndAlso IdTienda <> Right(Session("IdTienda"), 3)) _
							OrElse (Session("Cedis") = "1" AndAlso IdTienda <> Session("IdTienda")) Then
								Me.dtErrores_Agregar(i, "La Tienda no corresponde a la tienda con la cual se esta trabajando: " & Session("IdTienda"))
								'ElseIf Me.Tienda_Existe(Session("IdTienda")) = False Then
								'    Me.dtErrores_Agregar(i, "La Tienda no existe en la base de datos.")
							Else
								TiendaOK = True
							End If
						End If
						Dim Cant As Double = (CInt(datos(2)) / 100) 'CInt(Left(datos(2), datos(2).Length - 2)) + (CInt(Right(datos(2), 2)) / 100)
						Dim drDatos As DataRow
						drDatos = dtDatos.NewRow
						drDatos("Usuario") = datos(0)
						drDatos("IdMarbete") = datos(1)
						drDatos("Cantidad") = Cant
						drDatos("EAN") = datos(3)
						drDatos("IdRow") = datos(4)
						dtDatos.Rows.Add(drDatos)
					End If
				End If
			Loop

			'VERIFICAR SI NO HUBO ERRORES
			dtErrores = CType(Session("dterrores"), DataTable)

			'Dim param(5) As SqlParameter 
			Dim param(6) As SqlParameter 'JLGF 29-05-2013
			If dtErrores.Rows.Count > 0 Then
				Me.dgErrores.DataSource = dtErrores
				Me.dgErrores.DataBind()

				CMensajes.MostrarMensaje("No fue posible subir el archivo, verifique los errores presentados.", CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
			ElseIf dtDatos.Rows.Count <= 0 Then
				CMensajes.MostrarMensaje("El archivo no tiene datos.", CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
				Return False
			Else
				RenCorrectos = ""
				' Mandar los valores a la base de datos para revisar otros errores
				For ren As Integer = 0 To dtDatos.Rows.Count - 1
					Try
						'GUARDAR DATOS
						param(0) = New SqlParameter("@Usuario", dtDatos.Rows(ren).Item("Usuario"))  'varchar(20)
						param(1) = New SqlParameter("@IdMarbete", dtDatos.Rows(ren).Item("IdMarbete"))  'int
						param(2) = New SqlParameter("@Cantidad", dtDatos.Rows(ren).Item("Cantidad")) 'int
						param(3) = New SqlParameter("@EAN", dtDatos.Rows(ren).Item("EAN"))  'varchar(13)
						param(4) = New SqlParameter("@IdRow", dtDatos.Rows(ren).Item("IdRow"))    'varchar(10)
						param(5) = New SqlParameter("@IdTienda", Session("IdTienda"))    'varchar(10)
						param(6) = New SqlParameter("@IdAlmacen", Session("Almacen"))   'JLGF 29-05-2013 'varchar(4)

						DatosSQL.procedimientocp("GRANINV_Administracion", param)

						RenCorrectos = RenCorrectos + CStr(dtDatos.Rows(ren).Item("IdRow")) + "   "
					Catch ex As Exception
						Me.dtErrores_Agregar(ren + 1, "Error al guardar: " & ex.Message)
					End Try
				Next

				dtErrores = CType(Session("dterrores"), DataTable)
				Me.dgErrores.DataSource = dtErrores
				Me.dgErrores.DataBind()

				'If RenCorrectos.Length > 0 Then
				'    txtRegistrosCorrectos.Text = RenCorrectos
				'End If
			End If
			Session("dterrores") = Nothing
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		Finally
			LECTOR.Close()
			Archivo.Delete()
		End Try
	End Function

	''' <summary>
	''' DTS archivo
	''' </summary>
	''' <param name="FileName"></param>
	''' <param name="columnas"></param>
	Sub DTSDatos(ByVal FileName As String, ByVal columnas As Integer)
		Try
			Dim Continuar As Boolean = False
			'**-----VALIDAR ARCHIVO-----**
			Dim LECTOR As New StreamReader(FileName)
			Dim LINEA As String
			Dim msj As String = ""
			Do
				Try
					LINEA = LECTOR.ReadLine()
					'si ya llego al fin de los rows
					If LINEA Is Nothing Then
						Exit Do
					End If
					'leer datos 
					Dim datos() As String
					datos = LINEA.Split(",")
					If datos.Length = 0 Then
						Exit Do
					ElseIf datos.Length < columnas Then
						CMensajes.MostrarMensaje("El archivo debe contar con " & columnas.ToString & " columnas.", CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
						Continuar = False
					Else
						Dim IdTienda As String = Left(datos(1), 3)

						If Session("Cedis") = "1" Then 'ES UN CEDIS, ID TIENDA PRIMEROS 4 CARACTERES
							IdTienda = Left(datos(1), 4)
						End If
						Dim pruebasesion As String = Session("IdTienda")
						Dim prueba As String = IdTienda
						Dim pruebacedis As String = Session("Cedis")

						If (Session("Cedis") = "0" AndAlso IdTienda <> Right(Session("IdTienda"), 3)) OrElse (Session("Cedis") = "1" AndAlso IdTienda <> Session("IdTienda")) Then
							CMensajes.MostrarMensaje("El número de tienda no corresponde a la tienda con la cual se está trabajando: " & Chr(39) & CStr(Session("IdTienda")) & Chr(39), CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
							Continuar = False
						Else
							Continuar = True
						End If
					End If

				Catch ex As Exception
					CMensajes.MostrarMensaje("Ocurrió un error durante la carga del archivo: " & ex.Message, CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
					Continuar = False
				End Try
				Exit Do
			Loop
			LECTOR.Close()

			If Continuar = True Then
				Try
					'	Limpia tablas temporales gran y granerrores
					Dim dt As New DataTable()

					Dim par(2) As SqlParameter
					par(0) = New SqlParameter("@Archivo", "GRANINV")
					par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
					par(2) = New SqlParameter("@idAlmacen", Me.cboAlmacenActual.SelectedValue)
					DatosSQL.procedimientocp("CatalogoGeneral_DTSLimpiarTemporales", par)

					'	Subir archivo a la GRAN
					LECTOR = Nothing
					LECTOR = New StreamReader(FileName)
					LINEA = Nothing
					Dim x() As String = {"IdTienda", "Usuario", "Marbete", "Cantidad", "EAN", "IdRow", "IdAlmacen"}

					For Each value As String In x
						dt.Columns.Add(New DataColumn(value))
					Next

					Do
						Try
							LINEA = LECTOR.ReadLine()
							'si ya llego al fin de los rows
							If LINEA Is Nothing Then
								Exit Do
							End If

							'leer datos 
							Dim datos() As String
							datos = LINEA.Split(",")

							If Not (String.IsNullOrEmpty(datos(1).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(1).ToString.Trim) OrElse String.IsNullOrEmpty(datos(2).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(2).ToString.Trim) OrElse String.IsNullOrEmpty(datos(3).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(3).ToString.Trim) OrElse String.IsNullOrEmpty(datos(4).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(4).ToString.Trim)) Then
								Dim idtienda As String = Session("IdTienda")
								Dim usuario As String = Session("Usuario")
								Dim idalmacen = Me.cboAlmacenActual.SelectedValue
								Dim marbete As String = datos(1).ToString.Trim
								Dim w = datos(2).ToString.Trim.Substring(0, 6) & "." & datos(2).ToString.Trim.Substring(6, 2)
								Dim cantidad As String = Convert.ToDecimal(w)
								'Dim cantidad As String = datos(2).ToString.Trim
								Dim ean As String = datos(3).ToString.Trim
								Dim idrow As String = datos(4).ToString.Trim

								Dim row As DataRow = dt.NewRow()
								row.Item("IdTienda") = idtienda
								row.Item("Usuario") = usuario
								row.Item("Marbete") = marbete
								row.Item("Cantidad") = cantidad
								row.Item("EAN") = ean
								row.Item("IdRow") = idrow
								row.Item("IdAlmacen") = idalmacen

								dt.Rows.Add(row)
							End If
						Catch ex As Exception
							CMensajes.MostrarMensaje("Ocurrió un error durante la carga del archivo: " & ex.Message, CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
							Exit Do
							Exit Sub
						End Try
					Loop
					LECTOR.Close()

					Dim distinctDT As DataTable = dt.DefaultView.ToTable(True, "Marbete")

					Dim marbetes As New ArrayList()

					For Each dr In distinctDT.Rows
						Dim dsConteo As New DataSet
						ReDim par(2)
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@IdMarbete", dr("Marbete"))
						par(2) = New SqlParameter("@IdAlmacen", Me.cboAlmacenActual.SelectedValue)
						dsConteo = DatosSQL.funcioncp("Valida_Conteo_GranInv", par)

						Dim count As Integer = dsConteo.Tables(0).Rows(0)(0)

						If count > 0 Then
							If Not marbetes.Contains(dr("Marbete")) Then
								marbetes.Add(dr("Marbete"))
							End If
						End If
					Next

					If Not marbetes.Count > 0 Then
						Dim dsSufijo As DataSet = Nothing
						Dim tabla As String = Nothing
						Dim sufijo As String = Nothing
						ReDim par(0)
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						dsSufijo = DatosSQL.funcioncp(CReglasNegocio.Sufijo, par)

						If dsSufijo IsNot Nothing AndAlso dsSufijo.Tables.Count > 0 AndAlso dsSufijo.Tables(0).Rows.Count > 0 Then
							sufijo = dsSufijo.Tables(0).Rows(0)("Sufijo")
							tabla = CReglasNegocio.GRAN & sufijo
							BulkCopy.Insertar(dt, tabla)
						End If

						'	Agrega tienda y almacen a tabla GRAN
						ReDim par(2)
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@Tipo", 2)
						par(2) = New SqlParameter("@idAlmacen", Me.cboAlmacenActual.SelectedValue)
						DatosSQL.procedimientocp("GRAN_Administracion", par)

						'	Busca errores en la gran y si hay borra la tabla
						ReDim par(1)
						par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						par(1) = New SqlParameter("@idAlmacen", Me.cboAlmacenActual.SelectedValue)
						DatosSQL.procedimientocp("GRAN_Validar", par)

						''	Elimina GRANINV
						'ReDim par(2)
						'par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						'par(1) = New SqlParameter("@idAlmacen", Me.cboAlmacenActual.SelectedValue)
						'par(2) = New SqlParameter("@Accion", 2)
						'DatosSQL.procedimientocp("GRANINV_Administracion", par)

						''	 Inserta GRAN y Conteos
						'ReDim par(2)
						'par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
						'par(1) = New SqlParameter("@Tipo", 3)
						'par(2) = New SqlParameter("@idAlmacen", Me.cboAlmacenActual.SelectedValue)
						'ds1 = DatosSQL.funcioncp("GRAN_Administracion", par)

						'	Subir archivo a la GRANINV
						tabla = ""
						tabla = CReglasNegocio.GRANINV & sufijo
						BulkCopy.Insertar(dt, tabla)

						'	Subir archivo a la Conteos
						LECTOR = Nothing
						LECTOR = New StreamReader(FileName)
						LINEA = Nothing
						Do
							Try
								LINEA = LECTOR.ReadLine()
								'si ya llego al fin de los rows
								If LINEA Is Nothing Then
									Exit Do
								End If
								'leer datos 
								Dim datos() As String
								datos = LINEA.Split(",")

								If Not (String.IsNullOrEmpty(datos(1).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(1).ToString.Trim) OrElse String.IsNullOrEmpty(datos(2).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(2).ToString.Trim) OrElse String.IsNullOrEmpty(datos(3).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(3).ToString.Trim) OrElse String.IsNullOrEmpty(datos(4).ToString.Trim) OrElse String.IsNullOrWhiteSpace(datos(4).ToString.Trim)) Then
									ReDim par(8)
									par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
									par(1) = New SqlParameter("@Usuario", Session("IdUsuario"))
									par(2) = New SqlParameter("@IdMarbete", datos(1).ToString.Trim)
									Dim w = datos(2).ToString.Trim.Substring(0, 6) & "." & datos(2).ToString.Trim.Substring(6, 2)
									Dim cantidad As String = Convert.ToDecimal(w)
									par(3) = New SqlParameter("@Cantidad", cantidad)
									par(4) = New SqlParameter("@EAN", datos(3).ToString.Trim)
									par(5) = New SqlParameter("@IdRow", datos(4).ToString.Trim)
									par(6) = New SqlParameter("@IdAlmacen", Me.cboAlmacenActual.SelectedValue)
									par(7) = New SqlParameter("@Accion", 3)
									par(8) = New SqlParameter("@Terminal", datos(0).ToString.TrimStart("0"))
									DatosSQL.procedimientocp("Inserta_Archivo_GRANINV", par)
								End If
							Catch ex As Exception
								CMensajes.MostrarMensaje("Ocurrió un error durante la carga del archivo: " & ex.Message, CMensajes.Tipo.GetError, wucMessageSubirArchivoProvExterno)
								Exit Do
								Exit Sub
							End Try
						Loop
						LECTOR.Close()
						CMensajes.MostrarMensaje("Proceso terminado.", CMensajes.Tipo.GetExito, wucMessageSubirArchivoProvExterno)
					Else
						Dim listaMarbetes As String = String.Join(",", marbetes.ToArray())
						CMensajes.MostrarMensaje("Ya existe información de conteos.", CMensajes.Tipo.GetInformativo, wucMessageSubirArchivoProvExterno)
					End If
				Catch ex As Exception
					Throw New Exception(ex.Message, ex.InnerException)
				End Try
				ValidacionBodega_Errores()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		Finally
			If FileName <> "" And File.Exists(FileName) = True Then
				File.Delete(FileName)
			End If
		End Try
	End Sub

	Private Sub ValidacionBodega_Errores()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("GRAN_Administracion", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				'Me.lblMensaje.Text &= Chr(13) & " Archivo con errores, favor de verificar información y volver a cargar el archivo. "
				'Me.lblMensaje.Text &= Chr(13) & "Articulos cargados: " & ds.Tables(0).Rows(0).Item("Conteo")
				CMensajes.MostrarMensaje("Archivo con errores, favor de verificar información y volver a cargar el archivo." & vbNewLine & "Artículos cargados: " & ds.Tables(0).Rows(0).Item("Conteo"), CMensajes.Tipo.GetError, wucMessageMensaje)
				Me.lblErroresPresentados.Visible = True
				Me.dgErrores.Visible = True
				Me.dgErrores.DataSource = ds.Tables(0)
				Me.dgErrores.DataBind()
			Else
				Me.dgErrores.Visible = False
				Me.lblErroresPresentados.Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarAlmacenActual()
		Try
			Dim dtAlmacenes As New DataTable
			dtAlmacenes = DatosSQL.FuncionSPDataTable("Buscar_Almacenes")

			If dtAlmacenes IsNot Nothing AndAlso dtAlmacenes.Rows.Count > 0 Then
				Me.cboAlmacenActual.DataSource = dtAlmacenes
				Me.cboAlmacenActual.DataValueField = "idAlmacen"
				Me.cboAlmacenActual.DataTextField = "idAlmacen"
				Me.cboAlmacenActual.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

End Class