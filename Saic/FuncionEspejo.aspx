﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FuncionEspejo.aspx.vb" Inherits="Saic.FuncionEspejo" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                Función espejo
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageFuncionEspejo" runat="server" /><br />
            </td>
        </tr>
        <tr align="center">
            <td>
                <span><asp:Button runat="server" Text="Generar archivo" ID="btnCambiar" CssClass="btn" /></span>
            </td>
        </tr>
    </table>
</asp:Content>
