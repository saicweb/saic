﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SurtirPisoVenta.aspx.vb" Inherits="Saic.SurtirPisoVenta" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
    <script language="javascript" type="text/javascript">
        function disableEnterKey(e) {
            var key;
            if (window.event)
                key = window.event.keyCode;
            else
                key = e.which;

            return (key != 13);
        }
		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%" >
        <tr>
            <td class="titulos">
                <asp:Label runat="server" ID="lblTitulo" Text="Surtir piso venta" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageSurtirPisoVenta" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="100%">
                    <tr>
                        <td align="center">
                            <table runat="server" id="TblSurtirPisoVenta" width="50%" cellpadding="10">
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text="Marbete: "></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" id="cboMarbetes" Width="120px" AutoPostBack="true" MarkFirstMatch="true" AppendDataBoundItems="true" CssClass="dropdown"></asp:DropDownList>
                                        <asp:Label runat="server" Text="" ID="lblAlmacen"></asp:Label>
                                    </td>
                                    <td align="center">
                                        <span><asp:Button runat="server" Text="Surtir" ID="btnsurtir" CssClass="btn" CausesValidation="false" /></span>
                                    </td>
                                    <td align="center">
                                        <span><asp:Button runat="server" Text="Eliminar" ID="btneliminar" CssClass="btn" CausesValidation="false" /></span>
                                    </td>
                                    <td align="center">
                                        <span><asp:Button runat="server" Text="Agregar" ID="btnAgregar" CssClass="btn" CausesValidation="false" /></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:Panel runat="server" Width="100%" Height="350px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="grid" DataKeyNames="IdTienda" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="Terminal" DataField="Terminal" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:BoundField HeaderText="Marbete" DataField="Marbete" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px" />
                                        <asp:BoundField HeaderText="EAN" DataField="EAN" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px" />
                                        <asp:BoundField HeaderText="Descripci&#243;n" DataField="Descripcion" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="200px" ItemStyle-Width="200px" />
                                        <asp:BoundField HeaderText="Cantidad Actual" DataField="Cant_Actual" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px" />
                                        <asp:TemplateField HeaderText="Cantidad a Surtir" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtcant" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Cant_Surtir") %>'></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Cantidad Final" DataField="Cant_Final" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" ItemStyle-Width="100px"  />
                                        <asp:TemplateField HeaderText="Acción" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkmodificar" runat="server" CommandName="Modificar" Text="Modificar" CommandArgument="<%#Container.DataItemIndex%>"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Seleccionar" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:Label ID="lbltitulochk" runat="server" Text="Seleccionar"></asp:Label><br />
                                                <asp:CheckBox ID="chkheader"  runat="server" AutoPostBack="true" OnCheckedChanged="CheckUncheckAll" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox id="CheckBox5" runat="server" />
                                            </ItemTemplate>      
                                        </asp:TemplateField>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>  
    
    <table align="center">
        <tr>
            <td>
                <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="HddnAccion" />
    <asp:HiddenField runat="server" ID="HddnIdRow" />
</asp:Content>
