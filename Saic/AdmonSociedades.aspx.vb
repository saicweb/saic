﻿Imports System.Data.SqlClient
Public Class AdmonSociedades
    Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As Button
    Protected WithEvents btnConfirmNO As Button
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If

				ConsultarModelo()
			Catch ex As Exception
				Me.btnGuardar.Visible = False
				Me.btnModificar.Visible = False
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
        End If
    End Sub

    Sub Sociedad_Datos()
        Try
            Dim ds As New DataSet
            Dim PAR(0) As SqlParameter
            PAR(0) = New SqlParameter("@IdSociedad", Me.txtIdSociedad.Text.Trim)
            ds = DatosSQL.funcioncp("Sociedades_Buscar", PAR)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'DescripcionSociedad,DescripcionCorta, Direccion, Municipio, CodigoPostal, RFC
                If IsDBNull(ds.Tables(0).Rows(0).Item("DescripcionSociedad")) = False Then
                    Me.txtdescripcion.Text = ds.Tables(0).Rows(0).Item("DescripcionSociedad")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("DescripcionCorta")) = False Then
                    Me.txtdescripcionCorta.Text = ds.Tables(0).Rows(0).Item("DescripcionCorta")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("Direccion")) = False Then
                    Me.txtdireccion.Text = ds.Tables(0).Rows(0).Item("Direccion")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("Municipio")) = False Then
                    Me.txtMunicipio.Text = ds.Tables(0).Rows(0).Item("Municipio")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("CodigoPostal")) = False Then
                    Me.txtCodigoPostal.Text = ds.Tables(0).Rows(0).Item("CodigoPostal")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("RFC")) = False Then
                    Me.txtrFC.Text = ds.Tables(0).Rows(0).Item("RFC")
                End If
            Else
                Me.btnModificar.Visible = False
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Sociedades_Administracion(1)
    End Sub
    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Sociedades_Administracion(2)
    End Sub

#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

    Sub cargaWinConfirm()
        winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
        Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
        txtTitutlo.Text = "Confirmar"
        Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
        txtCuerpo.Text = ""


        btnConfirmNO = winConfirm1.FindControl("BtnNO")
        btnConfirmNO.Text = "No"
        AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

        btnConfirmSI = winConfirm1.FindControl("btnSI")
        btnConfirmSI.Text = "Sí"
        AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

        phConfirm.Controls.Add(winConfirm1)
    End Sub
#End Region

    Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
        Sociedades_Administracion(3)
    End Sub
    Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.winConfirm1.OcultaDialogo()
        Response.Redirect("AdmonSociedades.aspx?SID=" & Session("SID"), False)
    End Sub
	Sub Sociedades_Administracion(ByVal Accion As Integer)
		Try
			If String.IsNullOrEmpty(Me.txtIdSociedad.Text) OrElse String.IsNullOrWhiteSpace(Me.txtIdSociedad.Text) Then
				CMensajes.MostrarMensaje("Ingrese el Id de sociedad.", CMensajes.Tipo.GetError, wucMessageAdmonSociedades)
			ElseIf String.IsNullOrEmpty(Me.txtdescripcion.Text) OrElse String.IsNullOrWhiteSpace(Me.txtdescripcion.Text) Then
				CMensajes.MostrarMensaje("Ingrese la descripcion de la sociedad.", CMensajes.Tipo.GetError, wucMessageAdmonSociedades)
			Else
				Dim par(7) As SqlParameter
				Dim ds As New DataSet
				par(0) = New SqlParameter("@Accion", Accion)
				par(1) = New SqlParameter("@IdSociedad", Me.txtIdSociedad.Text.Trim)
				par(2) = New SqlParameter("@DescripcionSociedad", Me.txtdescripcion.Text.Trim)
				par(3) = New SqlParameter("@DescripcionCorta", Me.txtDescripcionCorta.Text.Trim)
				par(4) = New SqlParameter("@Direccion", Me.txtDireccion.Text.Trim)
				par(5) = New SqlParameter("@Municipio", Me.txtMunicipio.Text.Trim)
				par(6) = New SqlParameter("@CodigoPostal", CInt(Me.txtCodigoPostal.Text.Trim))
				par(7) = New SqlParameter("@RFC", Me.txtRFC.Text.Trim)

				ds = DatosSQL.funcioncp("Sociedades_Administracion", par)

				If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0), CMensajes.Tipo.GetError, wucMessageAdmonSociedades)
				Else
					ConsultarModelo()
					btnGuardar.Visible = True
					btnModificar.Visible = False
					txtIdSociedad.Enabled = True
					limpiarDatos(Accion)

					Select Case Accion
						Case 1 '--NUEVO
							CMensajes.MostrarMensaje("Sociedad Agregada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSociedades)
						Case 2 '--MODIFICAR
							CMensajes.MostrarMensaje("Sociedad Modificada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSociedades)
						Case 3 '--ELIMINAR
							CMensajes.MostrarMensaje("Sociedad Eliminada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonSociedades)
					End Select
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub limpiarDatos(ByVal Accion As String)

		If (Accion = 1) Then
			txtCodigoPostal.Text = ""
			txtdescripcion.Text = ""
			txtDescripcionCorta.Text = ""
			txtDireccion.Text = ""
			txtIdSociedad.Text = ""
			txtMunicipio.Text = ""
			txtRFC.Text = ""
		ElseIf (Accion = 2) Then
			txtCodigoPostal.Text = ""
			txtdescripcion.Text = ""
			txtDescripcionCorta.Text = ""
			txtDireccion.Text = ""
			txtIdSociedad.Text = ""
			txtMunicipio.Text = ""
			txtRFC.Text = ""
		ElseIf (Accion = 3) Then
			txtCodigoPostal.Text = ""
			txtdescripcion.Text = ""
			txtDescripcionCorta.Text = ""
			txtDireccion.Text = ""
			txtIdSociedad.Text = ""
			txtMunicipio.Text = ""
			txtRFC.Text = ""
		End If

	End Sub

	Sub ConsultarModelo()
        Try
            'IdArea,NombreArea
            Dim ds As New DataSet
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@IdSociedad", -1)

            ds = DatosSQL.funcioncp("Sociedades_Buscar", par)
            gvsociedad.DataSource = ds
            gvsociedad.DataBind()
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

    Protected Sub gvsociedad_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvsociedad.RowCommand
        Try
            If e.CommandName = "Modificar" Then
                Me.lblTitulo.Text = "Modificar datos de sociedad"
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim IdSociedad, DescripcionSociedad, DescripcionCorta, Direccion, Municipio, CodigoPostal, RFC As String
                IdSociedad = gvsociedad.Rows(index).Cells(0).Text
                DescripcionSociedad = gvsociedad.Rows(index).Cells(1).Text
                DescripcionCorta = gvsociedad.Rows(index).Cells(2).Text
                Direccion = gvsociedad.Rows(index).Cells(3).Text
                Municipio = gvsociedad.Rows(index).Cells(4).Text
                CodigoPostal = gvsociedad.Rows(index).Cells(5).Text
                RFC = gvsociedad.Rows(index).Cells(6).Text

                txtIdSociedad.Text = IdSociedad
                txtDescripcionCorta.Text = DescripcionCorta
                txtCodigoPostal.Text = CodigoPostal
                txtdescripcion.Text = DescripcionSociedad
                txtDireccion.Text = Direccion
                txtMunicipio.Text = Municipio
                txtRFC.Text = RFC
                txtIdSociedad.Enabled = False
                btnModificar.Visible = True
                btnGuardar.Visible = False
            ElseIf e.CommandName = "Eliminar" Then
                Me.lblTitulo.Text = "Eliminar datos de sociedad"
                Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim IdSociedad, DescripcionSociedad, DescripcionCorta, Direccion, Municipio, CodigoPostal, RFC As String
                IdSociedad = gvsociedad.Rows(index).Cells(0).Text
                DescripcionSociedad = gvsociedad.Rows(index).Cells(1).Text
                DescripcionCorta = gvsociedad.Rows(index).Cells(2).Text
                Direccion = gvsociedad.Rows(index).Cells(3).Text
                Municipio = gvsociedad.Rows(index).Cells(4).Text
                CodigoPostal = gvsociedad.Rows(index).Cells(5).Text
                RFC = gvsociedad.Rows(index).Cells(6).Text

                txtIdSociedad.Text = IdSociedad
                txtDescripcionCorta.Text = DescripcionCorta
                txtCodigoPostal.Text = CodigoPostal
                txtdescripcion.Text = DescripcionSociedad
                txtDireccion.Text = Direccion
                txtMunicipio.Text = Municipio
                txtRFC.Text = RFC
                txtIdSociedad.Enabled = False
                btnModificar.Visible = False
                btnGuardar.Visible = False
                Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
                lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar la sociedad?</div>"
                winConfirm1.MuestraDialogo()
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
End Class