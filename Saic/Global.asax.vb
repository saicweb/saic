﻿Imports System.Web.SessionState
Imports System.Net.Mail

Public Class Global_asax
	Inherits System.Web.HttpApplication

	Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
		' Se desencadena al iniciar la aplicación
	End Sub

	Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
		' Se desencadena al iniciar la sesión
	End Sub

	Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
		' Se desencadena al comienzo de cada solicitud
	End Sub

	Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
		' Se desencadena al intentar autenticar el uso
	End Sub

	Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
		Try

			Session("Err") = ""
			' Se desencadena cuando se produce un error
			Dim lastErrorWrapper As HttpException = CType(Server.GetLastError(), HttpException)

			Dim lastError As Exception = lastErrorWrapper
			If lastErrorWrapper.InnerException IsNot Nothing Then
				lastError = lastErrorWrapper.InnerException
			End If

			Dim lastErrorTypeName As String = lastError.GetType().ToString()
			Dim lastErrorMessage As String = lastError.Message
			Dim lastErrorStackTrace As String = lastError.StackTrace

			Session("Err") = String.Format("{0}|{1}|{2}|{3}", Request.RawUrl, lastErrorMessage, lastError.InnerException, lastErrorStackTrace)
			'Response.Redirect("evento.aspx")
			Response.Redirect("~/evento.aspx?P=" & Request.RawUrl & "&error=" & lastErrorMessage)
		Catch ex As Exception

		End Try
	End Sub

	Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
		' Se desencadena cuando finaliza la sesión
	End Sub

	Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
		' Se desencadena cuando finaliza la aplicación
	End Sub
End Class