﻿Imports System.Data.SqlClient
Imports System.IO
Imports dbAutoTrack.DataReports

Public Class CierreInventario
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Dim ds As New DataSet

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageCierreInventario.Visible = False
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageCierreInventario.Visible = False
		Me.btnGenerarReporte.Enabled = False
		Me.CerrarINV()
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
	End Sub

#End Region

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">¿Está seguro que desea cerrar el inventario de la tienda " & Session("IdTienda") & "?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Sub CerrarINV()
		Me.btnGenerarReporte.Visible = False
		Try
			Dim dsMarFal As New DataSet
			Dim parMarFal(1) As SqlParameter
			parMarFal(0) = New SqlParameter("@Tipo", Session("Perfil"))
			parMarFal(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			If Session("Sociedad").ToString = "3" Then
				dsMarFal = DatosSQL.funcioncp("RPT_MarbetesFaltantes_RST", parMarFal)
			Else
				dsMarFal = DatosSQL.funcioncp("RPT_MarbetesFaltantes", parMarFal)
			End If

			If Not dsMarFal Is Nothing AndAlso dsMarFal.Tables.Count > 0 AndAlso dsMarFal.Tables(0).Rows.Count > 0 Then
				CMensajes.MostrarMensaje("Existen marbetes faltantes.", CMensajes.Tipo.GetError, wucMessageCierreInventario)
				Me.HyperLink1.Text = "Descargar reporte de marbetes faltantes"
				RPT_MarFal(dsMarFal)
			Else
				'Quitar validación del reporte 80-20 para inventario parcil, ya que se ejecutará directamente en el cierre de inventario - BFSG 06/12/2010
				If Session("Sociedad") = 1 Then 'C - Control
					If Not Session("IdTienda").ToString.StartsWith("CC") Then
						If Session("TipoInventario") <> "P" Then
							Dim dSRPT2080 As New DataSet
							Dim parrpt2080(0) As SqlParameter
							parrpt2080(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
							dSRPT2080 = DatosSQL.funcioncp("ConteoConsolidadoBuscar", parrpt2080)
							If Not dSRPT2080 Is Nothing AndAlso dSRPT2080.Tables.Count > 0 AndAlso dSRPT2080.Tables(0).Rows.Count > 0 Then
								'Cambia el status
								Dim ds As New DataSet
								Dim par(0) As SqlParameter
								par(0) = New SqlParameter("@Tienda", Session("IdTienda"))
								ds = DatosSQL.funcioncp("CI_BuscaPendientesTienda", par)
								If ds.Tables(0).Rows.Count > 0 Then
									If ds.Tables(0).Rows(0).Item("CierreInventario") = "PENDIENTE" Then
										CMensajes.MostrarMensaje("Inventario ya fue marcado como pendiente para realizar cierre.", CMensajes.Tipo.GetInformativo, wucMessageCierreInventario)
									Else
										If ds.Tables(0).Rows(0).Item("CierreInventario") = "REALIZADO" Then
											CMensajes.MostrarMensaje("El cierre de inventario ha sido realizado con anterioridad.", CMensajes.Tipo.GetInformativo, wucMessageCierreInventario)
										Else
											'Actualizar el estatus
											Dim para(2) As SqlParameter
											para(0) = New SqlParameter("@Tienda", Session("IdTienda"))
											para(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
											para(2) = New SqlParameter("@Tipo", "CIERRE")
											DatosSQL.procedimientocp("CI_ActualizaEstatus", para)
											CMensajes.MostrarMensaje("Se ha realizado con éxito el cierre de inventario.", CMensajes.Tipo.GetExito, wucMessageCierreInventario)
										End If
									End If
								Else
									CMensajes.MostrarMensaje("No existe nigún inventario en proceso para la tienda " & Session("IdTienda") & ".", CMensajes.Tipo.GetError, wucMessageCierreInventario)
								End If
								Me.btnGenerarReporte.Visible = False
							Else
								CMensajes.MostrarMensaje("Se debe realizar primero el Reporte 80/20.", CMensajes.Tipo.GetError, wucMessageCierreInventario)
							End If
						Else
							GoTo cedis
						End If
					Else
						GoTo cedis
					End If
				Else
					'Cambia el status
cedis:
					Dim ds As New DataSet
					Dim par(0) As SqlParameter
					par(0) = New SqlParameter("@Tienda", Session("IdTienda"))
					ds = DatosSQL.funcioncp("CI_BuscaPendientesTienda", par)
					If ds.Tables(0).Rows.Count > 0 Then
						If ds.Tables(0).Rows(0).Item("CierreInventario") = "PENDIENTE" Then
							CMensajes.MostrarMensaje("Inventario ya fue marcado como pendiente para realizar cierre.", CMensajes.Tipo.GetInformativo, wucMessageCierreInventario)
						Else
							If ds.Tables(0).Rows(0).Item("CierreInventario") = "REALIZADO" Then
								CMensajes.MostrarMensaje("El cierre de inventario ha sido realizado con anterioridad.", CMensajes.Tipo.GetInformativo, wucMessageCierreInventario)
							Else
								'Actualizar el estatus
								Dim para(2) As SqlParameter
								para(0) = New SqlParameter("@Tienda", Session("IdTienda"))
								para(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
								para(2) = New SqlParameter("@Tipo", "CIERRE")
								DatosSQL.procedimientocp("CI_ActualizaEstatus", para)
								CMensajes.MostrarMensaje("Se ha realizado con éxito el cierre de inventario.", CMensajes.Tipo.GetExito, wucMessageCierreInventario)
							End If
						End If
					Else
						CMensajes.MostrarMensaje("No existe nigún inventario en proceso para la tienda " & Session("IdTienda") & ".", CMensajes.Tipo.GetError, wucMessageCierreInventario)
					End If
					Me.btnGenerarReporte.Visible = False
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub RPT_MarFal(ByVal DATOS As DataSet)
		Try
			'//////////////////////////////////////////////////////////////////////
			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim settings As New dbAutoTrack.DataReports.PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Portrait
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			If Session("Sociedad").ToString = "3" Then
				report1 = New RPT_MarbetesFaltantes_RSTrpt
			ElseIf Session("Cedis") = 1 Then
				report1 = New RPT_MarbetesFaltantes_CDS
			Else
				report1 = New RPT_MarbetesFaltantesrpt
			End If
			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbetesFaltantes")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim pathPdf As String = DirectorioArchivos & "MarbetesFaltantes_" & Session("Perfil") & "_" & Session("IdUsuario") & ".pdf"
			report1.DataSource = DATOS

			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
			CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")

			document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
			Me.HyperLink1.Visible = True
			' Response.Redirect("FileDownload.aspx?file=" & pathPdf)

			'//////////////////////////////////////////////////
			'termina el reporte
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub EstatusINV()
		Try
			Dim ds As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("InformacionGeneralInventario_Buscar", par)
			If ds Is Nothing AndAlso ds.Tables.Count <= 0 AndAlso ds.Tables(0).Rows.Count <= 0 Then
				Me.btnGenerarReporte.Visible = False
				CMensajes.MostrarMensaje("No se encontró un inventario en proceso para la tienda " & Session("IdTienda") & ".", CMensajes.Tipo.GetError, wucMessageCierreInventario)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

End Class