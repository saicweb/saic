﻿Imports System.Data.SqlClient

Public Class Reversa80_20
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		txtPassword.Attributes.Add("value", txtPassword.Text)

		Me.wucMessageReversa8020.Visible = False
		If Not Page.IsPostBack Then
			Validacion()
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageReversa8020.Visible = False
		'Validacion()
		If Me.HddAcceso.Value <> 1 Then
			Exit Sub
		End If
		Dim Pass, CPass As String
		Dim ds As New DataSet
		Dim PAR(1) As SqlParameter
		Try
			If Me.txtPassword.Text.Trim.Length = 0 Then
				CMensajes.MostrarMensaje("Ingrese la contraseña de reversa antes de continuar.", CMensajes.Tipo.GetError, wucMessageReversa8020)
			Else
				Pass = Security.getMD5Hash(Me.txtPassword.Text.Trim)

				PAR(0) = New SqlParameter("@idAccion", 3)
				PAR(1) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
				ds = DatosSQL.funcioncpSeguridad("Usuarios_Buscar", PAR)

				If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					If IsDBNull(ds.Tables(0).Rows(0).Item("Password2")) = False Then
						CPass = ds.Tables(0).Rows(0).Item("Password2")
						If CPass = Pass Then
							Reversa()
						Else
							CMensajes.MostrarMensaje("La contraseña de permiso de reversa es incorrecta, favor de verificar.", CMensajes.Tipo.GetError, wucMessageReversa8020)
						End If
					End If
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageReversa8020.Visible = False
		Me.txtTienda.Text = ""
		Me.txtPassword.Text = ""
		Me.btnEjecutar.Enabled = False
		Me.winConfirm1.OcultaDialogo()
	End Sub

#End Region

	Sub Validacion()
		Try
			If Session("Perfil") = "P" Then
				Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
			End If

			Me.HddAcceso.Value = 0

			If Session("PermisoReversa") = 0 Then
				CMensajes.MostrarMensaje("No cuenta con permisos para acceder a esta operación.", CMensajes.Tipo.GetError, wucMessageReversa8020)
			Else
				Me.btnEjecutar.Enabled = True
				Me.txtTienda.Text = Session("IdTienda")
				Me.HddAcceso.Value = 1
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">Ésta acción dará reversa al reporte 80/20 de la tienda indicada, ¿Está seguro de continuar?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Sub Reversa()
		Try
			'Acción para reversa de reporte 80/20
			Dim ds As New DataSet
			Dim PAR(1) As SqlParameter
			PAR(0) = New SqlParameter("@Accion", 3)
			PAR(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("ConteoConsolidado_Administracion", PAR)

			ReDim PAR(2)
			PAR(0) = New SqlParameter("@Tipo", "80-20")
			PAR(1) = New SqlParameter("@Usuario", Session("IdUsuario"))
			PAR(2) = New SqlParameter("@Motivo", DBNull.Value)
			'Acción para almacenamiento de bitácora
			DatosSQL.procedimientocp("Reversa_Bitacora", PAR)
			CMensajes.MostrarMensaje("Reversa de reporte 80/20 completada con éxito.", CMensajes.Tipo.GetExito, wucMessageReversa8020)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
		Me.txtTienda.Text = ""
		Me.txtPassword.Text = ""
		Me.btnEjecutar.Enabled = False
	End Sub
End Class