﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonTiendas.aspx.vb" Inherits="Saic.AdmonTiendas" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Tiendas" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageAdmonTiendas" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="90%" align="center" id="TblGridTienda">
                    <tr>
                        <td align="center">
                            <p>
								<asp:Image id="Image1" runat="server" ImageUrl="img/plusNested.png"></asp:Image>&nbsp
								<asp:LinkButton id="lnkAgregar" runat="server" ForeColor="Navy">Agregar Tienda</asp:LinkButton>
                            </p><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                <asp:GridView runat="server" ID="GridTiendas" DataKeyNames="IdTienda" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id de tienda" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
						                    <ItemTemplate>
                                                <asp:LinkButton ID="lnkIdTienda" CommandName="IdTienda" CommandArgument='<%#Container.DataItemIndex%>' Text='<%# DataBinder.Eval(Container.DataItem, "IdTienda") %>' runat="server"></asp:LinkButton>
						                    </ItemTemplate>
					                    </asp:TemplateField>
								        <asp:BoundField DataField="NombreTienda" HeaderText="Nombre de tienda" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
                                        <asp:BoundField DataField="UnidadNegocio" HeaderText="Unidad de negocio" HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
								        <asp:BoundField DataField="Ubicacion" HeaderText="Ubicaci&#243;n" HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
								        <asp:BoundField DataField="DescripcionSociedad" HeaderText="Sociedad" HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"><ItemStyle Width="95px" />
								        </asp:BoundField>
								        <asp:BoundField DataField="Cedis" HeaderText="CEDIS" HeaderStyle-Width="85px" ItemStyle-Width="85px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
								        <asp:BoundField DataField="IP" HeaderText="IP" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" width="50%" align="center" id="TblAgregar_Actualizar" visible="false">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Id de tienda: "></asp:Label>
                        </td>
                        <td>          
                            <asp:TextBox ID="txtIdTienda" runat="server" MaxLength="4" Width="60px" ToolTip="Ingrese el Numero de Tienda sin colocar como prefijo un letra. Ingrese solo 3 digitos."></asp:TextBox> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtIdTienda" Display="Dynamic" ErrorMessage="Campo Obligatorio" ForeColor="Red" Font-Size="X-Small">* Campo Obligatorio
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Nombre de tienda: "></asp:Label>
                        </td>
                        <td>          
                            <asp:TextBox ID="txtNombreTienda" runat="server" MaxLength="50" Width="180px"></asp:TextBox> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNombreTienda" Display="Dynamic" ErrorMessage="Campo Obligatorio" ForeColor="Red" Font-Size="X-Small">* Campo Obligatorio
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Unidad de negocio: "></asp:Label>
                        </td>
                        <td>         
                            <asp:DropDownList runat="server" id="cmbUnidadNegocio" Width="250px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Ubicación: "></asp:Label>
                        </td>
                        <td>          
                            <asp:TextBox ID="txtUbicacion" runat="server" MaxLength="50" Width="180px"></asp:TextBox> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUbicacion" Display="Dynamic" ErrorMessage="Campo Obligatorio" ForeColor="Red" Font-Size="X-Small">* Campo Obligatorio
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Sociedad: "></asp:Label>
                        </td>
                        <td>          
                            <asp:DropDownList runat="server" id="cboSociedad" Width="250px" CssClass="dropdown"></asp:DropDownList>&nbsp&nbsp&nbsp&nbsp
                            <asp:CheckBox runat="server" ID="chkCedis" CssClass="checkbox inline" Text="Cedis" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Dirección IP: "></asp:Label>
                        </td>
                        <td>          
                            <asp:TextBox ID="txtIP" runat="server" MaxLength="15" Width="130px" ToolTip="Ingrese aquí la dirección IP del servidor de tienda."></asp:TextBox> 
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><br />
                            <span><asp:Button runat="server" Text="Agregar" ID="btnAgregar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp
                            <span><asp:Button runat="server" Text="Actualizar" ID="btnActualizar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp
                            <span><asp:Button runat="server" Text="Baja" ID="btnBaja" CssClass="btn" Visible="false" CausesValidation="false" /></span>&nbsp&nbsp&nbsp
                            <span><asp:Button runat="server" Text="Cancelar" ID="btnCancelar" CssClass="btn" Visible="false" CausesValidation="false" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
       <table>
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
