﻿Imports System.Data.SqlClient

Public Class MenuOpciones
	Inherits System.Web.UI.UserControl

	Private _idSesion As String
	Public Property idSesion As String
		Get
			Return _idSesion
		End Get
		Set(ByVal value As String)
			_idSesion = value
		End Set
	End Property

	Private _status As String
	Public ReadOnly Property status As String
		Get
			Return _status
		End Get
	End Property

	''' <summary>
	''' Consulta los permisos del usuario por medio del identificador de sesion.
	''' </summary>
	Public Sub consultaPermisos()
		Dim CSesiones As New CSesiones With {.idSesion = Me.idSesion, .Perfil = Session("Perfil")}
		If CSesiones.consultaPermisosUsuario Then
			_status = CSesiones.ErrorMensaje
		Else
			Dim dsUsuario As DataSet = CSesiones.dsRespuesta

			If dsUsuario IsNot Nothing AndAlso dsUsuario.Tables.Count > 0 Then
				If dsUsuario.Tables(0).Rows.Count > 0 Then
					Session("Usuario") = dsUsuario.Tables(0).Rows(0).Item("idUsuario").ToString
					Session("TipoUsuario") = dsUsuario.Tables(0).Rows(0).Item("idTipoUsuario").ToString
				End If
				Call formaMenu(dsUsuario.Tables(1))
			End If
		End If
	End Sub

	''' <summary>
	''' Forma el menú de usuario
	''' </summary>
	''' <param name="dt"></param>
	Private Sub formaMenu(ByVal dt As DataTable)
		Dim rutaIconos As String = ConfigurationManager.AppSettings("rutaIconos")
		Dim items As MenuItem
		Dim itemsChild As MenuItem
		Dim urlIngresada As String = Session("URL")
		Dim cierre As Boolean = RevisarConteoConsolidado()

		If dt.Rows.Count > 0 Then
			' recorre la consulta de las opciones del usuario
			dt.Columns.Add("ValuePath")

			For Each rowURL As DataRow In dt.Rows
				rowURL("URL") = rowURL("URL").ToString.Replace(ConfigurationManager.AppSettings("urlSeguridad"), urlIngresada)
				dt.AcceptChanges()
			Next

			Try
				For w As Integer = dt.Rows.Count - 1 To 0 Step -1
					If cierre AndAlso Session("Perfil").ToString = "R" Then
						If dt.Rows(w)("URL").ToString.ToUpper.Contains("ADMONMARBETES.ASPX") Then
							dt.Rows.Remove(dt.Rows(w))
						ElseIf dt.Rows(w)("URL").ToString.ToUpper.Contains("AGREGARARTICULOMARBETE.ASPX") Then
							dt.Rows.Remove(dt.Rows(w))
						ElseIf dt.Rows(w)("URL").ToString.ToUpper.Contains("ADMONARTICULOMARBETE.ASPX") Then
							dt.Rows.Remove(dt.Rows(w))
						ElseIf dt.Rows(w)("URL").ToString.ToUpper.Contains("CAMBIARMARBETEUBICACION.ASPX") Then
							dt.Rows.Remove(dt.Rows(w))
						End If
						dt.AcceptChanges()
					End If
				Next
			Catch ex As Exception
				Throw New Exception(ex.Message)
			End Try

			Dim vp As String
			Dim padre As String
			Dim hijo As String
			Dim expression As String

			For i As Integer = 0 To dt.Rows.Count - 1
				vp = ""
				If DBNull.Value.Equals(dt.Rows(i).Item("idModuloPadre")) OrElse dt.Rows(i).Item("idModuloPadre").ToString.Trim = String.Empty Then
					vp = dt.Rows(i).Item("value")
				Else
					padre = dt.Rows(i).Item("idModuloPadre")
					hijo = dt.Rows(i).Item("idModulo")
					'find:
					If padre <> hijo Then
						expression = "idModulo = '" & padre & "' and idSistema='" & dt.Rows(i).Item("idSistema") & "'"
						Dim foundRows() As DataRow
						foundRows = dt.Select(expression)
						If foundRows.Count > 0 Then
							vp = foundRows(0).Item("ValuePath") & "/" & dt.Rows(i).Item("value")
							'hijo = vp
							'padre = IIf(DBNull.Value.Equals(foundRows(0).Item("idModuloPadre")) OrElse dt.Rows(i).Item("idModuloPadre").ToString.Trim = String.Empty, Nothing, foundRows(0).Item("idModuloPadre"))

							'If padre Is Nothing Then
							'    vp = dt.Rows(i).Item("idSistema") & vp
							'End If

							'GoTo find
						End If
					End If
				End If
				dt.Rows(i).Item("ValuePath") = vp
				dt.AcceptChanges()
			Next

			'Dim dtv As DataView
			'dtv = New DataView(dt)
			'dtv.Sort = "ValuePath"
			'dt = dtv.ToTable

			For i As Integer = 0 To dt.Rows.Count - 1
				If DBNull.Value.Equals(dt.Rows(i).Item("idModuloPadre")) OrElse dt.Rows(i).Item("idModuloPadre").ToString.Trim = String.Empty Then
					items = New MenuItem
					items.Text = dt.Rows(i).Item("descripcionModulo")
					items.Value = dt.Rows(i).Item("value")
					If Not DBNull.Value.Equals(dt.Rows(i).Item("URL")) Then items.NavigateUrl = dt.Rows(i).Item("URL").ToString.Replace("&?", "&")
					If Not DBNull.Value.Equals(dt.Rows(i).Item("imagenIcono")) Then items.ImageUrl = rutaIconos & dt.Rows(i).Item("imagenIcono")
					Me.MenuUsuario.Items.Add(items)
				Else
					items = MenuUsuario.FindItem(dt.Rows(i).Item("ValuePath"))
				End If
				If items IsNot Nothing Then
					Dim expression2 As String = "idModuloPadre = '" & dt.Rows(i).Item("idModulo") & "' and idSistema='" & dt.Rows(i).Item("idSistema") & "'"
					Dim foundRows() As DataRow

					foundRows = dt.Select(expression2)

					If foundRows.Count > 0 Then
						Dim r As Integer
						' Print column 0 of each returned row.
						For r = 0 To foundRows.Count - 1
							' determina que es itemChild por tener modulo padre
							itemsChild = New MenuItem
							itemsChild.Text = foundRows(r).Item("descripcionModulo")
							itemsChild.Value = foundRows(r).Item("value")
							If Not DBNull.Value.Equals(foundRows(r).Item("URL")) Then itemsChild.NavigateUrl = foundRows(r).Item("URL").ToString.Replace("&?", "&")
							If Not DBNull.Value.Equals(foundRows(r).Item("imagenIcono")) Then itemsChild.ImageUrl = rutaIconos & foundRows(r).Item("imagenIcono")
							items.ChildItems.Add(itemsChild)
						Next
					End If
				End If
			Next

		End If
	End Sub

	''' <summary>
	''' Valida que la tabla conteoconsolidado_cierre tenga datos para bloquear opciones del menú
	''' </summary>
	''' <returns></returns>
	Private Function RevisarConteoConsolidado() As Boolean
		Dim regreso As Boolean = False
		Dim ds As New DataSet
		Dim par(0) As SqlParameter
		par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))

		ds = DatosSQL.funcioncp("RevisarConteoConsolidado", par)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			regreso = True
		End If

		Return regreso
	End Function
End Class