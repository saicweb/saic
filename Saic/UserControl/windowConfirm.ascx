﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="windowConfirm.ascx.vb" Inherits="Saic.windowConfirm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .popover2
    {
        padding: 1px;
        background-color: #ffffff;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, 0.2);
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding;
        background-clip: padding-box;
    }
    
    .popover2.top
    {
        margin-bottom: 10px;
    }
    
    .popover2.right
    {
        margin-left: 10px;
    }
    
    .popover2.bottom
    {
        margin-top: 10px;
    }
    
    .popover2.left
    {
        margin-right: 10px;
    }
    
    .popover2 .arrow, .popover2 .arrow:after
    {
        position: absolute;
        display: inline-block;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid;
    }
    
    .popover2 .arrow:after
    {
        z-index: -1;
        content: "";
    }
    
    .popover2.top .arrow
    {
        bottom: 1px;
        left: 50%;
        margin-left: -10px;
        border-top-color: #ffffff;
        border-width: 10px 10px 0;
    }
    
    .popover2.top .arrow:after
    {
        bottom: -1px;
        left: -11px;
        border-top-color: rgba(0, 0, 0, 0.25);
        border-width: 11px 11px 0;
    }
    
    .popover2.right .arrow
    {
        top: 45px;
        left: 1px;
        margin-top: -10px;
        border-right-color: #ffffff;
        border-width: 10px 10px 10px 0;
    }
    
    .popover2.right .arrow:after
    {
        bottom: -11px;
        left: -1px;
        border-right-color: rgba(0, 0, 0, 0.25);
        border-width: 11px 11px 11px 0;
    }
    
    .popover2.bottom .arrow
    {
        top: 1px;
        left: 50%;
        margin-left: -10px;
        border-bottom-color: #ffffff;
        border-width: 0 10px 10px;
    }
    
    .popover2.bottom .arrow:after
    {
        top: -1px;
        left: -11px;
        border-bottom-color: rgba(0, 0, 0, 0.25);
        border-width: 0 11px 11px;
    }
    
    .popover2.left .arrow
    {
        top: 45px;
        right: 1px;
        margin-top: -10px;
        border-left-color: #ffffff;
        border-width: 10px 0 10px 10px;
    }
    
    .popover2.left .arrow:after
    {
        right: -1px;
        bottom: -11px;
        border-left-color: rgba(0, 0, 0, 0.25);
        border-width: 11px 0 11px 11px;
    }
</style>
<div>
    <asp:Panel ID="PanelWindowConfirm" runat="server" class="popover2" Style="display: none">
        <table class="style1">
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="LblTextoTitulo" runat="server" Font-Bold="True"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="style2">
                    <asp:Label ID="LblTextoConfirmacion" runat="server"></asp:Label>
                </td>
                
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="BtnSI" runat="server" ClientIDMode="Static" Text="Si" Width="50px"
                                            Height="30px" CausesValidation="false" ViewStateMode="Enabled" />
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;
                                    </td>
                                    <td>
                                        <asp:Button ID="BtnNO" runat="server" ClientIDMode="Static" Text="No" Width="50px"
                                            Height="30px" CausesValidation="false" ViewStateMode="Enabled" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpdatePanel1">
                        <ProgressTemplate>
                            <img src="Img/ajax-loader.gif" alt="Cargando" />
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btnOculto" runat="server" Style="display: none" />
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalPopupWindowConfirm" runat="server" PopupControlID="PanelWindowConfirm"
        CancelControlID="btnOculto" TargetControlID="btnOculto">
    </asp:ModalPopupExtender>


</div>
