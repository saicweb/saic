﻿Public Class wucMessage
	Inherits System.Web.UI.UserControl

	' Metodos publicos
	Public Sub showMessage(ByVal message As String, ByVal tipo As TipoMensaje)
		ltMessage.Text = message
		pnMessage.Visible = False
		If message.Length > 0 Then

			pnMessage.Visible = True
			'Vemos como lo vamos a desplegar
			Select Case tipo
				Case TipoMensaje.Error
					pnMessage.CssClass = "alert alert-block alert-danger"
				Case TipoMensaje.Exito
					pnMessage.CssClass = "alert alert-block alert-success"
				Case TipoMensaje.Informativo
					pnMessage.CssClass = "alert alert-block alert-info"
				Case Else

			End Select
		End If
	End Sub


	Enum TipoMensaje
		Informativo = 1
		[Error] = 2
		Exito = 3
	End Enum

End Class