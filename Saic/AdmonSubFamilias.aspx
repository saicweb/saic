﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonSubFamilias.aspx.vb" Inherits="Saic.AdmonSubFamilias" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Subfamilias" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageAdmonSubFamilias" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="55%" align="center" id="TblGridSubfamilias">
                    <tr>
                        <td align="center" colspan="2" class="auto-style1">
                            <p>
								<asp:Image id="Image1" runat="server" ImageUrl="img/plusNested.png"></asp:Image>&nbsp
								<asp:LinkButton id="lnkAgregar" runat="server" ForeColor="Navy">Agregar Subfamilia</asp:LinkButton>
                            </p><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Departamento: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboDepartamentoGrid" runat="server" AutoPostBack="true"  Width="220px" CssClass="dropdown"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="cboDepartamentoGrid" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                        </asp:RequiredFieldValidator>
                            </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Familia: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboFamiliaGrid" runat="server" AutoPostBack="true"  Width="220px" CssClass="dropdown"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cboFamiliaGrid" Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="X-Small">
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center"><br />
                <asp:Panel runat="server" Height="350px" ScrollBars="Auto" ID="gridSubfamilia">
                    <asp:GridView runat="server" ID="GridSubfamilias" DataKeyNames="IdSubFamilia" AutoGenerateColumns="False" SkinID="Grid3">
                        <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                        <Columns>
                            <asp:TemplateField HeaderText="Id de subfamilia" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
						        <ItemTemplate>
                                    <asp:LinkButton ID="lnkIdSubFamilia" CommandName="IdSubFamilia" CommandArgument='<%#Container.DataItemIndex%>' Text='<%# DataBinder.Eval(Container.DataItem, "IdSubFamilia") %>' runat="server"></asp:LinkButton>
						        </ItemTemplate>
					        </asp:TemplateField>
						    <asp:BoundField DataField="DescripcionSubFamilia" HeaderText="Descripci&#243;n" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
						    <asp:BoundField DataField="DescripcionFamilia" HeaderText="Familia" HeaderStyle-Width="200px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
						    <asp:BoundField DataField="DescDepto" HeaderText="Departamento" HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
					    </Columns>
                    </asp:GridView>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td><br />
                <table runat="server" width="50%" align="center" id="TblAgregarSubFamilia" visible="false">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Id de Subfamilia: " ></asp:Label>
                        </td>
                        <td>        
                            <asp:TextBox ID="txtIdSubFamilia" runat="server" MaxLength="20" Width="150px"></asp:TextBox> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtIdSubFamilia" Display="Dynamic" ErrorMessage="Campo Obligatorio" ForeColor="Red" Font-Size="X-Small">* Campo Obligatorio
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Descripción: "></asp:Label>
                        </td>
                        <td>          
                            <asp:TextBox ID="txtdescripcion" runat="server" MaxLength="100" Width="250px"></asp:TextBox> 
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtdescripcion" Display="Dynamic" ErrorMessage="Campo Obligatorio" ForeColor="Red" Font-Size="X-Small">* Campo Obligatorio
                            </asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Departamento: "></asp:Label>
                        </td>
                        <td>          
                            <asp:DropDownList runat="server" id="cboDepartamento" Width="250px" AutoPostBack="true" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Familia: "></asp:Label>
                        </td>
                        <td>           
                            <asp:DropDownList runat="server" id="cboFamilia" Width="250px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><br />
                            <span><asp:Button runat="server" Text="Agregar" ID="btnAgregar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp 
                            <span><asp:Button runat="server" Text="Actualizar" ID="btnActualizar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp
                            <span><asp:Button runat="server" Text="Baja" ID="btnBaja" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp
                            <span><asp:Button runat="server" Text="Cancelar" ID="btnCancelar" CssClass="btn" Visible="false" CausesValidation="false" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
