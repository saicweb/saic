﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AdmonMarbetes.aspx.vb" Inherits="Saic.AdmonMarbetes" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes" CssClass="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="" ID="lblTitulo"  Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageAdmonMarbete" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="45%" id="TblAdmonMarbetes">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:radiobuttonlist id="rbtnConteos" runat="server"  AutoPostBack="true" RepeatDirection="Horizontal" Visible="false" CssClass="radio inline" CellPadding="15" TextAlign="Right">
							    <asp:ListItem Value="1" Selected="true">Conteo 1</asp:ListItem>
							    <asp:ListItem Value="2">Conteo 2</asp:ListItem>
                                <asp:ListItem Value="3">Conteo 3</asp:ListItem>
						    </asp:radiobuttonlist>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Marbete actual: " ID="lblRangoInicial" Visible="false" ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboRangoInicial" runat="server" AutoPostBack="true" Visible="false"  Width="150px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Id marbete: " ID="lblIdMarbete" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtIdMarbete" runat="server" MaxLength="17" Width="150px"></asp:TextBox> 
                            <asp:DropDownList id="cboMarbetes" runat="server" AutoPostBack="true" Visible="false" Width="150px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Ubicación física: " ID="lblUbicacionFisica" Visible="false" Width="130px"></asp:Label>
                            <asp:Label runat="server" Text="Almacén: " ID="lblAlmacen" Visible="false" ></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtUbicacionFisica" runat="server" MaxLength="80" Visible="false" Width="280px"></asp:TextBox>
                            <asp:Label runat="server" Text="" ID="lblAlmacen1" Visible="false" ></asp:Label>
                            <asp:DropDownList id="cmbAlmacen" runat="server" Visible="false"  Width="130px" AutoPostBack="true" CssClass="dropdown"></asp:DropDownList>
                            <asp:DropDownList id="cmbArea" runat="server" Visible="false"  Width="130px" AutoPostBack="true" CssClass="dropdown"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Área: " ID="lblArea" Visible="false" ></asp:Label>
                            <asp:Label runat="server" Text="Id marbete: " ID="lblMarbeteEliminar" Visible="false" ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboArea" runat="server" Visible="false"  Width="250px" CssClass="dropdown"></asp:DropDownList>
                            <asp:Panel runat="server" ID="PnlMarbeteEliminar" Width="195px" Height="200px" ScrollBars="Auto" Visible="false">
                                <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3" EmptyDataText="No se encontraron  registros.">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:TemplateField HeaderText="Todos" HeaderStyle-Width="60px" ItemStyle-Width="60px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <HeaderTemplate>
                                                <asp:CheckBox id="Checkbox3" OnCheckedChanged="CheckUncheckAll" AutoPostBack="true" runat="server" Text="Todos&lt;br /&gt;" TextAlign="Left"></asp:CheckBox>
                                            </HeaderTemplate>
						                    <ItemTemplate>
                                                <asp:CheckBox id="Checkbox4" runat="server"></asp:CheckBox>
                                            </ItemTemplate>
					                    </asp:TemplateField>
								        <asp:BoundField HeaderText="Marbete" DataField="idMarbete" ItemStyle-Width="120px" ControlStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"/>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr align="center">
                        <td colspan="2"><br />
                            <span><asp:Button runat="server" Text="Agregar marbete" ID="btnAgregar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp 
                            <span><asp:Button runat="server" Text="Actualizar" ID="btnActualizar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp
                            <span><asp:Button runat="server" Text="Eliminar" ID="btnEliminar" CssClass="btn" Visible="false" /></span>&nbsp&nbsp&nbsp
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table> 
    <table align="center">
        <tr>
            <td>
                <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="HddnAccion" />
    <asp:HiddenField runat="server" ID="HddnMarbete" />
</asp:Content>
