﻿Imports System.Data.SqlClient

Public Class SurtirMarbete
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Try
			If Not IsPostBack Then
				CatalogoMarbetes_Buscar()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			par(2) = New SqlParameter("@Conteo", 1)

			ds = DatosSQL.funcioncp("Marbetes_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdMarbete") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Me.cboMarbetes.DataSource = ds.Tables(0)
				Me.cboMarbetes.DataValueField = "IdMarbete"
				Me.cboMarbetes.DataTextField = "IdMarbete"
				Me.cboMarbetes.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarAlmacenEliminar()
		Try
			Dim dsAlmacenes As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@Perfil", Session("Perfil"))
			par(1) = New SqlParameter("@idMarbete", Me.cboMarbetes.SelectedValue)
			par(2) = New SqlParameter("@IdTienda", Session("IdTienda"))

			dsAlmacenes = DatosSQL.funcioncp("Buscar_Almacen_Marbete", par)

			If dsAlmacenes.Tables(0).Rows.Count = 0 Then
				Me.lblAlmacen1.Text = ""
			Else
				Me.lblAlmacen1.Text = dsAlmacenes.Tables(0).Rows(0).Item(0)
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
		Session("SurtirMarbete_Marbete") = Nothing
		Session("SurtirMarbete_Almacen") = Nothing
		Session("SurtirMarbete_Marbete") = cboMarbetes.SelectedValue
		Session("SurtirMarbete_Almacen") = lblAlmacen1.Text
		Response.Redirect("SurtirArticuloMarbete.aspx?SID=" & Request.QueryString("SID"), False)
	End Sub

	Protected Sub cboMarbetes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboMarbetes.SelectedIndexChanged
		CargarAlmacenEliminar()
	End Sub
End Class