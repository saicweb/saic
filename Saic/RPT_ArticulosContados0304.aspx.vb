﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Public Class RPT_ArticulosContados0304
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject
	Dim txtAlmacen As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Not Page.IsPostBack Then
            If Session("Sociedad").ToString <> "3" Then Almacen_Buscar()
            Session.Remove("RPT_Art0304")
        End If
	End Sub

	Sub Almacen_Buscar()
		Try
			Dim par(1) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim almacen As String = Me.cboAlmacen.SelectedValue

			CatalogoReportes_CRV13(almacen)

			Me.lblAlmacen.Visible = False
			Me.cboAlmacen.Visible = False
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_ArticulosContados_0304.DataBinding
		If Session("RPT_Art0304") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_ArticulosContados_0304.ReportSource = rpt
				Session("RPT_Art0304") = rpt
			End If
		Else
			RPT_ArticulosContados_0304.ReportSource = Session("RPT_Art0304")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = RPT_ArticulosContados0304(Almacen)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_ArticulosContados0304))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtAlmacen = rpt.ReportDefinition.Sections("Section1").ReportObjects("TxtAlmacen")
			txtNombreReporte.Text = "REPORTE DE ARTICULOS CONTADOS EN ESTATUS 03 Y 04"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			txtAlmacen.Text = IIf(Almacen = "0", "Todos", Almacen)
			rpt.SetDataSource(ds.Tables(0))
			RPT_ArticulosContados_0304.ReportSource = rpt
			RPT_ArticulosContados_0304.DataBind()
		Else
            CMensajes.MostrarMensaje("No existe articulos contados en estatus 03 y 04.", CMensajes.Tipo.GetInformativo, wucMessageArticulosContados0304)
        End If
	End Sub

	Public Function RPT_ArticulosContados0304(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
            Dim par(2) As SqlParameter
            par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
            par(1) = New SqlParameter("@Almacen", Almacen)
            par(2) = New SqlParameter("@Perfil", CStr(Session("Perfil")).Substring(0, 1))
            ds = DatosSQL.funcioncp("RPT_Articulos0304", par)
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class