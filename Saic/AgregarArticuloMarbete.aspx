﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="AgregarArticuloMarbete.aspx.vb" Inherits="Saic.AgregarArticuloMarbete" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
    <script language="javascript" type="text/javascript">
    		    function disableEnterKey(e) {
    		        var key;
    		        if (window.event)
    		            key = window.event.keyCode;
    		        else
    		            key = e.which;

    		        return (key != 13);
    		    }
    		    function nomenclaturaUbicacion(control) { /*GISP 2017-10-20*/
                    var prefijo = '<%=Session("Prefijoalmacen")%>';
    		        var ubicacion = '';
    		        var tipoAlmacen = '';
    		        var almacen = '';
    		        ubicacion = control.value.toUpperCase();

    		        if (ubicacion.length >= 4) {
    		            tipoAlmacen = ubicacion.substring(0, 4) + ' ';

    		            if (tipoAlmacen != prefijo) {
    		                tipoAlmacen = prefijo;
    		                ubicacion = tipoAlmacen; 
                        }

    		            if (ubicacion.length >= 5) {
    		                almacen = ubicacion.substring(5, ubicacion.length);
    		            }

    		            ubicacion = tipoAlmacen + almacen;
                    }		        

                    control.value = ubicacion;
                }
                function asignarPrefijoAlmacen(control) {   /*GISP 2017-10-26*/
                    var prefijo = '<%=Session("Prefijoalmacen")%>';

                    if (control.value == '') {
                        control.value = prefijo;
                    }
                }
		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Administración de marbetes"  class="titulos" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Agregar artículo a marbete / EAN" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageAgregarArtMarbete" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="75%">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Marbete: "></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList id="cboMarbetes" runat="server" Width="170px" CssClass="dropdown" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:DropDownList id="cboAlmacenes" runat="server" Width="170px" CssClass="dropdown"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="" ID="lblAlmacen"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Ubicación: " ID="lblUbicacion" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <%--<asp:TextBox ID="txtUbicacion" runat="server" Visible="false" Width="170px" onfocus="asignarPrefijoAlmacen(this);" onkeyup="nomenclaturaUbicacion(this);"></asp:TextBox> --%>
                            <asp:TextBox ID="txtUbicacion" runat="server" Visible="false" Width="170px"></asp:TextBox> 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:UpdatePanel ID="upl" runat="server">
                                <ContentTemplate>
                                    <table runat="server" cellpadding="10">
                                        <tr>
                                            <td>
                                                <asp:FileUpload ID="RadUpload1" runat="server" CssClass="upload" /><br />
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" CssClass="inline radio" CellPadding="15">
                                                    <asp:ListItem Value="Excel" Selected="true">Excel&nbsp&nbsp&nbsp&nbsp&nbsp</asp:ListItem>
                                                    <asp:ListItem Value="TXT">TXT&nbsp&nbsp</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td>
                                                <span><asp:Button runat="server" Text="Cargar archivo" ID="btnAgregar" CssClass="btn" /></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnAgregar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><br />
                            <asp:Panel runat="server" Width="475px" Height="350px" ScrollBars="Auto">
                                <asp:GridView id="RadGrid1" runat="server" AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="ID" DataField="ID" HeaderStyle-Width="75px" ItemStyle-Width="75px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"></asp:BoundField>
                                        <asp:TemplateField HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:TextBox id="TextBox1" runat="server" MaxLength="16" Width="130px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Descripci&#243;n" HeaderStyle-Width="170px" ItemStyle-Width="170px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" >
                                            <ItemTemplate>
                                                <asp:TextBox id="TextBox2" runat="server" MaxLength="100" Width="150px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cantidad" HeaderStyle-Width="65px" ItemStyle-Width="65px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                            <ItemTemplate>
                                                <asp:TextBox id="TextBox3" runat="server" MaxLength="6" Width="50px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
						        </asp:GridView>
                            </asp:Panel>   
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><br />
                            <asp:button runat="server" id="btnGuardar" Text="Agregar artículos" CssClass="btn"></asp:button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><br />
                            <asp:label id="lblOrigen" runat="server" Visible="False"></asp:label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField runat="server" ID="ErrInsertar" Value="0" />
</asp:Content>
