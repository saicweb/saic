﻿Public Class Inicio
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Request.RawUrl.ToString.Contains("P=P") OrElse Request.RawUrl.ToString.Contains("P=R") Then
			Select Case Request.QueryString("P")
				Case "P"
					Me.lblTitulo.Text = "Proceso Preconteo"
				Case "R"
					Me.lblTitulo.Text = "Proceso Rotativo"
			End Select
		Else
			Me.lblTitulo.Text = "Bienvenidos a SAIC"
		End If
	End Sub
End Class