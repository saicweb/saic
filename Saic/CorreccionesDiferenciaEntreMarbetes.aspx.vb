﻿Imports System.Data
Imports System.Data.SqlClient
Public Class CorreccionesDiferenciaEntreMarbetes
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
	End Sub

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			Guardar()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
	End Sub

#End Region

	Sub consultar()
		Try
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			ds = DatosSQL.funcioncp("RPT_DiferenciaEntreMarbetes_Correccion", par)
			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Me.dg.DataSource = ds.Tables(0)
				Me.dg.DataBind()

				If Me.Session("Cedis") <> 1 Then
					Me.dg.Columns(2).Visible = False
				End If

				Me.lblaviso.Visible = True
				Me.btnGuardar.Visible = True
				Me.btnGuardar2.Visible = True
			Else
				CMensajes.MostrarMensaje("No existe mercancía con diferencias en conteos.", CMensajes.Tipo.GetInformativo, wucMessageCorreccionesDiferenciasEntreMarbetes)
				Me.dg.DataSource = Nothing
				Me.dg.DataBind()
			End If

		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
		consultar()
	End Sub

	Function ActualizarConteoFinal(ByVal IdMarbete As String, ByVal EAN As String, ByVal Conteo1 As Integer, ByVal Conteo2 As Integer) As Boolean
		Try
			ActualizarConteoFinal = False
			Dim par(6) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@IdMarbete", IdMarbete)
			par(2) = New SqlParameter("@EAN", EAN)
			par(3) = New SqlParameter("@Conteo", Conteo1)
			par(4) = New SqlParameter("@IdUsuario", Session("IdUsuario"))
			par(5) = New SqlParameter("@Perfil", Session("Perfil"))
			par(6) = New SqlParameter("@Conteo2", Conteo2)
			DatosSQL.procedimientocp("Conteos_Administracion_Correcciones", par)
			ActualizarConteoFinal = True
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Guardar()
		Try
			Dim ModCount As Integer = 0
			Dim CantNoNumCount As Integer = 0
			Dim CantNegCount As Integer = 0
			Dim CantDup As Integer = 0
			Dim Val As Boolean
			For i As Integer = 0 To Me.dg.Rows.Count - 1
				Val = True

				Dim chk As CheckBox
				Dim Cant As TextBox
				If Session("Cedis") = 1 Then
					chk = CType(dg.Rows(i).Cells(8).Controls(1), CheckBox)
					Cant = CType(dg.Rows(i).Cells(9).Controls(1), TextBox)
				Else
					chk = CType(dg.Rows(i).Cells(7).Controls(1), CheckBox)
					Cant = CType(dg.Rows(i).Cells(8).Controls(1), TextBox)
				End If

				If Cant.Text.Trim.Length > 0 And chk.Checked = True Then
					CantDup = CantDup + 1
					Cant.BackColor = Drawing.Color.RoyalBlue
					Val = False
				ElseIf Cant.Text.Trim.Length > 0 Then
					If IsNumeric(Cant.Text.Trim) = False Then
						CantNoNumCount = CantNoNumCount + 1
						Cant.BackColor = Drawing.Color.Red
						Val = False
					ElseIf CInt(Cant.Text.Trim) < 0 Then
						CantNegCount = CantNegCount + 1
						Cant.BackColor = Drawing.Color.Yellow
						Val = False
					End If
				ElseIf chk.Checked = False Then
					Val = False
				End If
				If Val = True Then
					Dim idmarbete, ean, conteo2 As String
					idmarbete = dg.Rows(i).Cells(1).Text
					If Session("Cedis") = 1 Then
						ean = dg.Rows(i).Cells(3).Text
						conteo2 = Me.dg.Rows(i).Cells(6).Text
					Else
						ean = dg.Rows(i).Cells(2).Text
						conteo2 = Me.dg.Rows(i).Cells(5).Text
					End If

					If Cant.Text.Trim.Length > 0 Then
						If Me.ActualizarConteoFinal(idmarbete, ean, Cant.Text, conteo2) = True Then
							ModCount = ModCount + 1
							Cant.BackColor = Drawing.Color.Transparent
							'Conteo1
							Me.dg.Rows(i).Cells(4).Text = Microsoft.VisualBasic.FormatNumber(Cant.Text, 2)
							'Diferencia
							Me.dg.Rows(i).Cells(6).Text = Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(5).Text, 2) - Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(4).Text, 2)
							Cant.Text = ""
						End If
					ElseIf chk.Checked = True Then
						If Me.ActualizarConteoFinal(idmarbete, ean, conteo2, conteo2) = True Then
							ModCount = ModCount + 1
							Cant.BackColor = Drawing.Color.Transparent

							If Session("Cedis") = 1 Then
								'Conteo1
								Me.dg.Rows(i).Cells(5).Text = Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(6).Text, 2)
								'Diferencia
								Me.dg.Rows(i).Cells(7).Text = Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(6).Text, 2) - Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(5).Text, 2)
							Else
								'Conteo1
								Me.dg.Rows(i).Cells(4).Text = Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(5).Text, 2)
								'Diferencia
								Me.dg.Rows(i).Cells(6).Text = Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(5).Text, 2) - Microsoft.VisualBasic.FormatNumber(Me.dg.Rows(i).Cells(4).Text, 2)
							End If

							chk.Checked = False
						End If
					End If
				End If
			Next
			If ModCount >= 0 AndAlso CantNoNumCount = 0 AndAlso CantNegCount = 0 AndAlso CantDup = 0 Then
				'CMensajes.Show(ModCount.ToString & " - Cantidades modificadas.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount > 0 AndAlso CantNegCount = 0 AndAlso CantDup = 0 Then
				'CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount = 0 AndAlso CantNegCount > 0 AndAlso CantDup = 0 Then
				' CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en amarillo.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en amarillo.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount = 0 AndAlso CantNegCount = 0 AndAlso CantDup > 0 Then
				'CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en azul.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en azul.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount > 0 AndAlso CantNegCount > 0 AndAlso CantDup = 0 Then
				' CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo y amarillo.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo y amarillo.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount > 0 AndAlso CantNegCount = 0 AndAlso CantDup > 0 Then
				'CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo y azul.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo y azul.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount = 0 AndAlso CantNegCount > 0 AndAlso CantDup > 0 Then
				'CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en amarillo y azul.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en amarillo y azul.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			ElseIf ModCount >= 0 AndAlso CantNoNumCount > 0 AndAlso CantNegCount > 0 AndAlso CantDup > 0 Then
				'CMensajes.Show(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo, amarillo y azul.")
				CMensajes.MostrarMensaje(ModCount.ToString & " - Cantidades modificadas. Favor de verificar los campos marcados en rojo, amarillo y azul.", CMensajes.Tipo.GetExito, wucMessageCorreccionesDiferenciasEntreMarbetes)
			End If

			consultar()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click, btnGuardar2.Click
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">¿Ya son todas las correcciones que realizará?</div>"
		winConfirm1.MuestraDialogo()
	End Sub
End Class