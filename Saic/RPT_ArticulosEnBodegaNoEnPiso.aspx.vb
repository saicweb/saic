﻿Imports System.Data.SqlClient
Imports CrystalDecisions.CrystalReports.Engine

Public Class RPT_ArticulosEnBodegaNoEnPiso
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject
	Dim txtAlmacen As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageRPT_ArticulosEnBodegaNoEnPiso.Visible = False

		If Not Page.IsPostBack Then
			Almacen_Buscar()
			Session.Remove("RPT_ArtBodegaNoEnPiso")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Try
			Dim almacen As String = Me.cboAlmacen.SelectedValue
			CatalogoReportes_CRV13(almacen)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_ArticulosEn_Bodega_NoEnPiso.DataBinding
		If Session("RPT_ArtBodegaNoEnPiso") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_ArticulosEn_Bodega_NoEnPiso.ReportSource = rpt
				Session("RPT_ArtBodegaNoEnPiso") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_ArticulosEn_Bodega_NoEnPiso.ReportSource = Session("RPT_ArtBodegaNoEnPiso")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = RPT_ArticulosEnBodegaNoEnPiso(Almacen)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_ArticulosEnBodegaNoEnPiso))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtAlmacen = rpt.ReportDefinition.Sections("Section1").ReportObjects("TxtAlmacen")
			txtNombreReporte.Text = "MERCANCIA CONTADA EN BODEGA Y NO EN PISO"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			txtAlmacen.Text = IIf(Almacen = "0", "Todos", Almacen)
			rpt.SetDataSource(ds.Tables(0))

			RPT_ArticulosEn_Bodega_NoEnPiso.ReportSource = rpt
			RPT_ArticulosEn_Bodega_NoEnPiso.DataBind()
		Else
			CMensajes.MostrarMensaje("No existen artículos en bodega que no estén en piso.", CMensajes.Tipo.GetInformativo, wucMessageRPT_ArticulosEnBodegaNoEnPiso)
		End If
	End Sub

	Public Function RPT_ArticulosEnBodegaNoEnPiso(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Almacen", Almacen)
			ds = DatosSQL.funcioncp("RPT_ArticulosEnBodegaNoEnPiso", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(1) As SqlParameter
				par2(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par2(1) = New SqlParameter("@Almacen", Almacen)

				dsExcel = DatosSQL.funcioncp("RPT_ArticulosEnBodegaNoEnPisoExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					Reportes.REPEXCEL(dsExcel, "Mercancia contada en bodega y no en piso", "ArticulosEnBodegaNoEnPiso_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), True)
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Almacen_Buscar()
		Try
			Dim par(1) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing Then
				If DS.Tables(0).Rows.Count > 0 Then
					Me.cboAlmacen.DataSource = Nothing
					Me.cboAlmacen.Items.Clear()
					Me.cboAlmacen.DataSource = DS
					Me.cboAlmacen.DataTextField = "almacen"
					Me.cboAlmacen.DataValueField = "idalmacen"
					Me.cboAlmacen.DataBind()
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class