﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TransferirConteo_DLX.aspx.vb" Inherits="Saic.TransferirConteo_DLX" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%"  >
        <tr>
            <td class="titulos">
                Transferir conteo 2
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageTransferirConteo_DLX" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <table runat="server" align="center">
                            <tr>
                                <td align="center" >
                                    <asp:FileUpload ID="RadUpload1" runat="server" CssClass="upload" Width="300px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><br />
                                    <asp:RadioButtonList ID="rdformato" runat="server" RepeatDirection="Horizontal" CssClass="radio inline" CellPadding="15">
                                        <asp:ListItem Value="1" Selected="True">Excel</asp:ListItem>
							            <asp:ListItem Value="2">TXT</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><br /><br />
                                    <asp:Label runat="server" Text="Opción de múltiples conteos" ></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><br />
                                    <asp:radiobuttonlist id="rbtnNumConteo" runat="server" AutoPostBack="True" CssClass="radio inline" RepeatDirection="Horizontal" CellPadding="15">
								        <asp:ListItem Value="1">Conteo 1</asp:ListItem>
								        <asp:ListItem Value="2" Selected="True">Conteo 2</asp:ListItem>
								        <asp:ListItem Value="3">Conteo 3</asp:ListItem>
							        </asp:radiobuttonlist>
                                </td>
                            </tr>
                            <tr align="center">
                                <td colspan="2"><br />
                                    <span><asp:Button runat="server" Text="Cargar archivo" ID="btncargar" CssClass="btn" /></span>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                         <asp:PostBackTrigger ControlID="btncargar" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
