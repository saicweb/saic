﻿Imports System.Data.SqlClient
Imports System.IO
Imports dbAutoTrack.DataReports

Public Class RPT_MarbeteIndividual_Sel
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageMarbeteIndividual_Sel.Visible = False

		If Session("Sociedad").ToString = "3" Then
			Me.rbtnConteos.Visible = False
		End If

		If Not Page.IsPostBack Then
			Me.CatalogoMarbetes_Buscar()
		End If
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			par(2) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)

			ds = DatosSQL.funcioncp("Marbetes_Buscar", par)
			If Not ds Is Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.Grid.DataSource = ds.Tables(0)
				Me.Grid.DataBind()
				Me.btnVerReporte.Enabled = True
				Me.Grid.Visible = True
			Else
				Me.btnVerReporte.Enabled = False
				Me.Grid.Visible = False
				CMensajes.MostrarMensaje("No se encontró ningún marbete.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual_Sel)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
		Dim seleccionados As Integer = 0

		For i As Integer = 0 To Me.Grid.Rows.Count - 1
			Dim chkRow As New CheckBox
			chkRow = CType(Me.Grid.Rows(i).FindControl("Checkbox4"), CheckBox)
			If chkRow.Checked = True Then
				seleccionados += 1
			End If
		Next

		If seleccionados = 0 Then
			CMensajes.MostrarMensaje("Debe seleccionar por lo menos un marbete.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual_Sel)
		Else
			Try
				Dim marbete As String

				For i As Integer = 0 To Me.Grid.Rows.Count - 1
					Dim chkRow As New CheckBox
					chkRow = CType(Me.Grid.Rows(i).FindControl("Checkbox4"), CheckBox)
					If chkRow.Checked = True Then
						marbete = Me.Grid.Rows(i).Cells(1).Text
						Try
							DatosSQL.procedimientoText("Insert into MarbetesTemp values('" & marbete & "')")
						Catch ex As Exception
							Throw New Exception(ex.Message, ex.InnerException)
						End Try
					End If
				Next

				Dim ds As New DataSet, ds2 As New DataSet, almacen As String
				Dim par(2) As SqlParameter, param(2) As SqlParameter
				par(0) = New SqlParameter("@Tipo", Session("Perfil"))
				param(0) = New SqlParameter("@Tipo", Session("Perfil"))
				par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				param(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(2) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)
				param(2) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)

				If Me.rbtnRPTFormatos.Items(0).Selected = False Then
					If Session("Sociedad").ToString <> "3" Then
						ds2 = DatosSQL.funcioncp("RPT_MarbeteIndividual_Sel_Excel_Almacen", param)
						almacen = ds2.Tables(0).Rows(0).Item("Almacen")
					Else
						almacen = ""
					End If
				End If
				If Me.rbtnRPTFormatos.Items(0).Selected = True Then
					If Session("Sociedad").ToString = "3" Then
						ds = DatosSQL.funcioncp("RPT_MarbeteIndividual_RST_Sel", par)
					Else
						ds = DatosSQL.funcioncp("RPT_MarbeteIndividual_Sel", par)
					End If
				Else
					If Session("Sociedad").ToString = "3" Then
						ds = DatosSQL.funcioncp("RPT_MarbeteIndividual_RST_Sel_Excel", par)
					Else
						ds = DatosSQL.funcioncp("RPT_MarbeteIndividual_Sel_Excel", par)
					End If
				End If

				If Not ds Is Nothing OrElse ds.Tables.Count > 0 Then
					If ds.Tables(0).Rows.Count > 0 Then
						Me.HyperLink1.Visible = True
						If Me.rbtnRPTFormatos.Items(0).Selected = True Then
							REPORTE(ds)
						Else
							REPEXCEL(ds.Tables(0), Today.ToShortDateString, Now.ToShortTimeString, Session("IdTienda"), Me.rbtnConteos.SelectedValue, almacen)
						End If
						Me.btnVerReporte.Enabled = True
					Else
						CMensajes.MostrarMensaje("No se encontraron los Marbetes especificados.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual_Sel)
					End If
				Else
					CMensajes.MostrarMensaje("No se encontraron los Marbetes especificados.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual_Sel)
				End If

			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Private Sub rbtnConteos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnConteos.SelectedIndexChanged
		Me.CatalogoMarbetes_Buscar()
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(Grid.HeaderRow.Cells(0).FindControl("Checkbox3"), CheckBox)
		For Each row As GridViewRow In Grid.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(0).FindControl("Checkbox4"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub

	Sub REPEXCEL(ByVal DATOS As DataTable, ByVal Fecha As String, ByVal Hora As String, ByVal Tienda As String, ByVal Conteo As String, Optional ByVal Almacen As String = Nothing)
		Try
			'Crear el directorio si no existe
			If DATOS.Rows.Count = 0 Then
				CMensajes.MostrarMensaje("Sin datos.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual_Sel)
			Else
				Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbeteIndividualSeleccionAleatoria")

				If Not Directory.Exists(DirectorioArchivos) Then
					Directory.CreateDirectory(DirectorioArchivos)
				End If

				Dim Archivo As String = DirectorioArchivos & "MarbeteIndividualSeleccionAleatoria_" & Session("Perfil") & "_" & Session("IdUsuario") & ".xls"
				Dim TitulosArchivo As String = "Reporte De Marbetes Por Selección"
				'Guardar la información en el archivo txt
				Dim Subtitulo As String = Nothing
				If Session("Sociedad").ToString = "3" Then
					Subtitulo = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda
				Else
					Subtitulo = "Fecha: " & Fecha & "    Hora: " & Hora & "       Tienda: " & Tienda & "    Conteo " & Conteo & "    Almacen " & Almacen
				End If
				Reportes.Exportar_ExcelDinamico(DATOS, Archivo, TitulosArchivo, Subtitulo)
				Dim encripta As New Encryption
				Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(Archivo.Trim)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub REPORTE(ByVal DATOS As DataSet)
		Try
			'//////////////////////////////////////////////////////////////////////
			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim settings As New dbAutoTrack.DataReports.PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Portrait
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			If Session("Sociedad").ToString = "3" Then
				report1 = New RPT_MarbeteIndividual_RSTrpt
			Else
				report1 = New RPT_MarbeteIndividualrpt
			End If

			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbeteIndividualSeleccionAleatoria")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim pathPdf As String = DirectorioArchivos & "MarbeteIndividualSeleccionAleatoria_" & Session("IdUsuario") & ".pdf"
			report1.DataSource = DATOS

			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
			CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")

			document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)

			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
			'//////////////////////////////////////////////////
			'termina el reporte
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class