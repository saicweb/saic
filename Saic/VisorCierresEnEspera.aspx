﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="VisorCierresEnEspera.aspx.vb" Inherits="Saic.VisorCierresEnEspera" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                <asp:Label runat="server" Text="Visor de cierre en espera" ID="lblTitulo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageVisorCierres" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center">
                    <tr>
                        <td>
                            <asp:Timer runat="server" id="UpdateTimer" interval="60000" />
                            <asp:UpdatePanel runat="server" id="TimedPanel" updatemode="Conditional">
                                <ContentTemplate>
                                    <asp:Panel runat="server" Height="350px" ScrollBars="Auto">
                                        <asp:GridView runat="server" ID="dg" AutoGenerateColumns="False" SkinID="Grid3">
                                            <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                            <Columns>
                                                <asp:BoundField HeaderText="ID" DataField="ID" HeaderStyle-Width="70px" ItemStyle-Width="70px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                <asp:BoundField HeaderText="Tienda" DataField="Tienda" HeaderStyle-Width="130px" ItemStyle-Width="130px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                                <asp:BoundField HeaderText="Estatus" DataField="Estatus" HeaderStyle-Width="100px" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"  />
                                                <asp:BoundField HeaderText="Hora" DataField="Hora" HeaderStyle-Width="130px" ItemStyle-Width="130px" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
					                        </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </ContentTemplate>
                                <Triggers>
                                        <asp:AsyncPostBackTrigger controlid="UpdateTimer"/>
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:Label id="lblhora" Runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
