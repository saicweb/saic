﻿Imports System.Data.SqlClient
Public Class AdmonFamilias
    Inherits System.Web.UI.Page

    Protected WithEvents winConfirm1 As windowConfirm
    Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Not Page.IsPostBack Then
			Try
				If Session("IdUsuario") Is Nothing Then
					Response.Redirect("Default.aspx")
				End If

				Me.Departamentos_Buscar()
				Me.gvFamilias.Visible = True
				ConsultaFam()
			Catch ex As Exception
				Me.btnGuardar.Visible = False
				Me.btnModificar.Visible = False
				Me.btnEliminar.Visible = False
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Private Sub Departamentos_Buscar()
		Try
			Dim ds As New DataSet
			ds = DatosSQL.FuncionSP("Departamentos_Buscar")
			If Not ds Is Nothing Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdDepartamento") = "-1"
				dr("DescDepto") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Me.cboDepartamento.DataSource = ds.Tables(0)
				Me.cboDepartamento.DataTextField = "DescDepto"
				Me.cboDepartamento.DataValueField = "IdDepartamento"
				Me.cboDepartamento.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub ConsultaFam()
		Try
			'IdArea,NombreArea
			Dim ds As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdDepartamento", -1)
			par(1) = New SqlParameter("@IdFamilia", "")

			ds = DatosSQL.funcioncp("Familias_Buscar", par)
			gvFamilias.DataSource = ds
			gvFamilias.DataBind()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Familia_Datos()
        Try
            Dim ds As New DataSet
            Dim PAR(0) As SqlParameter
            PAR(0) = New SqlParameter("@IdFamilia", Me.txtIdFamilia.Text.Trim)
            ds = DatosSQL.funcioncp("Familias_Buscar", PAR)
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'F.IdDepartamento,F.IdFamilia,F.DescripcionFamilia
                If IsDBNull(ds.Tables(0).Rows(0).Item("IdFamilia")) = False Then
                    Me.txtIdFamilia.Text = ds.Tables(0).Rows(0).Item("IdFamilia")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("DescripcionFamilia")) = False Then
                    Me.txtdescripcion.Text = ds.Tables(0).Rows(0).Item("DescripcionFamilia")
                End If
                If IsDBNull(ds.Tables(0).Rows(0).Item("IdDepartamento")) = False Then
                    Me.cboDepartamento.SelectedValue = ds.Tables(0).Rows(0).Item("IdDepartamento")
                End If
            Else
                Me.btnModificar.Visible = False
            End If
        Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub
    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click
        Familias_Administracion(1)
    End Sub
    Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        Familias_Administracion(2)
    End Sub


#Region "PopUp confirm"
    Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        cargaWinConfirm()
    End Sub

    Sub cargaWinConfirm()
        winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
        Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
        txtTitutlo.Text = "Confirmar"
        Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
        txtCuerpo.Text = ""


        btnConfirmNO = winConfirm1.FindControl("BtnNO")
        btnConfirmNO.Text = "No"
        AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

        btnConfirmSI = winConfirm1.FindControl("btnSI")
        btnConfirmSI.Text = "Sí"
        AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

        phConfirm.Controls.Add(winConfirm1)
    End Sub
#End Region

    Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
        Familias_Administracion(3)
    End Sub
    Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.winConfirm1.OcultaDialogo()
        Response.Redirect("AdmonFamilias.aspx?SID=" & Session("SID"), False)
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click

    End Sub
    Sub Familias_Administracion(ByVal Accion As Integer)
		Try
			Dim ds As New DataSet
			Dim Tienda As String = ""
			If String.IsNullOrEmpty(Me.txtIdFamilia.Text) OrElse String.IsNullOrWhiteSpace(Me.txtIdFamilia.Text) Then
				CMensajes.MostrarMensaje("Ingrese el ID de la familia.", CMensajes.Tipo.GetError, wucMessageAdmonFamilias)
			ElseIf String.IsNullOrEmpty(Me.txtdescripcion.Text) OrElse String.IsNullOrWhiteSpace(Me.txtdescripcion.Text) Then
				CMensajes.MostrarMensaje("Ingrese la descripción de la familia.", CMensajes.Tipo.GetError, wucMessageAdmonFamilias)
			ElseIf Me.cboDepartamento.SelectedValue = "-1" Then
				CMensajes.MostrarMensaje("Selecione el departamento.", CMensajes.Tipo.GetError, wucMessageAdmonFamilias)
			Else
				Dim par(3) As SqlParameter
				par(0) = New SqlParameter("@Accion", Accion)
				par(1) = New SqlParameter("@IdFamilia", Me.txtIdFamilia.Text.ToUpper.Trim)
				par(2) = New SqlParameter("@DescripcionFamilia", Me.txtdescripcion.Text.Trim.ToUpper)
				par(3) = New SqlParameter("@IdDepartamento", Me.cboDepartamento.SelectedValue)

				ds = DatosSQL.funcioncp("Familias_Administracion", par)

				If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
					CMensajes.MostrarMensaje(ds.Tables(0).Rows(0)(0), CMensajes.Tipo.GetError, wucMessageAdmonFamilias)
				Else
					btnGuardar.Visible = True
					btnModificar.Visible = False
					btnEliminar.Visible = False
					txtIdFamilia.Enabled = True
					Me.gvFamilias.Visible = True
					ConsultaFam()
					limpiarDatos()

					Select Case Accion
						Case 1 '--NUEVO
							'CMensajes.Show("Familia Agregada exitosamente.")
							CMensajes.MostrarMensaje("Familia Agregada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonFamilias)
						Case 2 '--MODIFICAR
							'CMensajes.Show("Familia Modificada exitosamente.")
							CMensajes.MostrarMensaje("Familia Modificada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonFamilias)
						Case 3 '--ELIMINAR
							'CMensajes.Show("Familia Eliminada exitosamente.")
							CMensajes.MostrarMensaje("Familia Eliminada exitosamente.", CMensajes.Tipo.GetExito, wucMessageAdmonFamilias)
					End Select
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
    End Sub

	Protected Sub gvFamilias_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvFamilias.RowCommand

		Try
			If e.CommandName = "Modificar" Then
				Dim index As Integer = Convert.ToInt32(e.CommandArgument)
				Dim id, descripcion, descdepto As String
				id = gvFamilias.Rows(index).Cells(0).Text
				descripcion = HttpUtility.HtmlDecode(gvFamilias.Rows(index).Cells(1).Text)
				descdepto = HttpUtility.HtmlDecode(gvFamilias.Rows(index).Cells(2).Text)

				For i As Integer = 0 To cboDepartamento.Items.Count - 1
					Dim departamento As String = cboDepartamento.Items(i).Text
					If departamento = descdepto Then
						cboDepartamento.SelectedIndex = i
					End If
				Next

				txtIdFamilia.Text = id
				txtdescripcion.Text = descripcion
				txtIdFamilia.Enabled = False
				btnModificar.Visible = True
				btnGuardar.Visible = False
			ElseIf (e.CommandName = "Eliminar") Then
				Dim index As Integer = Convert.ToInt32(e.CommandArgument)
				Dim id, descripcion, descdepto As String
				id = gvFamilias.Rows(index).Cells(0).Text
				descripcion = HttpUtility.HtmlDecode(gvFamilias.Rows(index).Cells(1).Text)
				descdepto = HttpUtility.HtmlDecode(gvFamilias.Rows(index).Cells(2).Text)

				For i As Integer = 0 To cboDepartamento.Items.Count - 1
					Dim departamento As String = cboDepartamento.Items(i).Text
					If departamento = descdepto Then
						cboDepartamento.SelectedIndex = i
					End If
				Next

				txtIdFamilia.Text = id
				txtdescripcion.Text = descripcion
				txtIdFamilia.Enabled = False
				btnModificar.Visible = False
				btnEliminar.Visible = False
				btnGuardar.Visible = False
				Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
				lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar la familia?</div>"
				winConfirm1.MuestraDialogo()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub limpiarDatos()
        txtIdFamilia.Text = ""
        txtdescripcion.Text = ""
        cboDepartamento.SelectedIndex = 0
    End Sub

End Class