﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine

Public Class RPT_ContadoDetallado
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("UnidadNegocio") Is Nothing OrElse Session("IdTienda") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Session("Cedis") = 0 Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		Else
			Dim unidadNegocio As String = Session("UnidadNegocio")
			Dim idTienda As String = Session("IdTienda")
			Dim scriptManager2 As ScriptManager = ScriptManager.GetCurrent(Me.Page)
			scriptManager2.RegisterPostBackControl(Me.btnGenerarReporte)
		End If

		If Not Page.IsPostBack Then
			Session.Remove("RPT_ContadoDetCedis")
		End If
	End Sub

	Protected Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
		CatalogoReportes_CRV13()
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Contado_Detallado.DataBinding
		If Session("RPT_ContadoDetCedis") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Contado_Detallado.ReportSource = rpt
				Session("RPT_ContadoDetCedis") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_Contado_Detallado.ReportSource = Session("RPT_ContadoDetCedis")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13()
		ds = ConsultaContadoDetalladoCEDIS()

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & NombreReportes.RPT_ContadoDetallado))
			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			txtNombreReporte.Text = "REPORTE RESUMEN CONTADO DETALLADO"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			rpt.SetDataSource(ds.Tables(0))
			RPT_Contado_Detallado.ReportSource = rpt
			RPT_Contado_Detallado.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_ContadoDetallado)
		End If
	End Sub

	Public Function ConsultaContadoDetalladoCEDIS() As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(0) As SqlParameter
			par(0) = New SqlParameter("@idTienda", Session("IdTienda"))
			ds = DatosSQL.funcioncp("RPT_ContadoDetalladoCEDIS", par)
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(0) As SqlParameter
				par2(0) = New SqlParameter("@idTienda", Session("IdTienda"))

				dsExcel = DatosSQL.funcioncp("RPT_ContadoDetalladoCEDISExcel", par2)
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					Reportes.REPEXCEL(dsExcel, "Reporte Resumen Contado Detallado", "ContadoDetallado_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"))
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function
End Class