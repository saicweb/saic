﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="SurtirArticuloMarbete.aspx.vb" Inherits="Saic.SurtirArticuloMarbete" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td class="titulos">
                Extracción artículos en marbete
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageSurtirArtMarbete" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server">
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Marbete: " ></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="" id="lblMarbete" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label runat="server" Text="Almacén: " ></asp:Label>
                        </td>
                        <td>
                            <asp:Label runat="server" Text="" id="lblAlmacen" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <table runat="server" align="center">
                                        <tr>
                                            <td align="center" ><br />
                                                <asp:FileUpload ID="RadUpload1" runat="server" CssClass="upload" Width="300px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center"><br />
                                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" CssClass="radio inline" CellPadding="15">
                                                    <asp:ListItem Value="1" Selected="True">Excel</asp:ListItem>
							                        <asp:ListItem Value="2">TXT</asp:ListItem>
                                                </asp:RadioButtonList><br />
                                            </td>
                                        </tr>
                                        <tr align="center">
                                            <td colspan="2"><br />
                                                <span><asp:Button runat="server" Text="Cargar archivo" ID="btnAgregar" CssClass="btn" /></span>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                     <asp:PostBackTrigger ControlID="btnAgregar" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <asp:Panel runat="server" Height="250px" ScrollBars="Auto" Width="310px">
                                <asp:GridView runat="server" ID="RadGrid1"  AutoGenerateColumns="False" SkinID="Grid3">
                                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"></HeaderStyle>                                
                                    <Columns>
                                        <asp:BoundField HeaderText="ID" DataField="ID" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="75px" ItemStyle-Width="75px" />
                                        <asp:TemplateField HeaderText="EAN" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="120px" ItemStyle-Width="120px">
                                            <ItemTemplate>
                                                <asp:TextBox id="TextBox1" runat="server" Width="110px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cantidad" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:TextBox id="TextBox3" runat="server" Width="89px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
							        </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><br />
                            <span><asp:Button runat="server" Text="Agregar artículos" ID="btnGuardar" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
