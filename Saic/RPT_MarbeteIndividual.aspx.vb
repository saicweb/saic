﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_MarbeteIndividual
	Inherits System.Web.UI.Page

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		Me.wucMessageMarbeteIndividual.Visible = False

		If Session("Sociedad").ToString = "3" Then Me.rbtnConteos.Visible = False

		If Not Page.IsPostBack Then
			Me.CatalogoMarbetes_Buscar()
		End If
	End Sub

	Sub CatalogoMarbetes_Buscar()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Perfil", Session("Perfil"))
			par(2) = New SqlParameter("@Conteo", Me.rbtnConteos.SelectedValue)

			ds = DatosSQL.funcioncp("Marbetes_Buscar", par)

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Me.cboInicioRango.Items.Clear()
				Me.cboFinalRango.Items.Clear()
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("IdMarbete") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				Me.cboInicioRango.DataSource = ds.Tables(0)
				Me.cboInicioRango.DataValueField = "IdMarbete"
				Me.cboInicioRango.DataTextField = "IdMarbete"
				Me.cboInicioRango.DataBind()

				Me.cboFinalRango.DataSource = ds.Tables(0)
				Me.cboFinalRango.DataValueField = "IdMarbete"
				Me.cboFinalRango.DataTextField = "IdMarbete"
				Me.cboFinalRango.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnVerReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerReporte.Click
		If Me.cboInicioRango.SelectedValue = "[Seleccione]" Then
			CMensajes.MostrarMensaje("Seleccione el rango inicial.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual)
		ElseIf Me.cboFinalRango.SelectedValue = "[Seleccione]" Then
			CMensajes.MostrarMensaje("Seleccione el rango final.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual)
		ElseIf CDbl(Right(Me.cboInicioRango.SelectedValue, 15)) > CDbl(Right(Me.cboFinalRango.SelectedValue, 15)) Then
			CMensajes.MostrarMensaje("El rango es incorrecto.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual)
		Else
			Try
				Dim ds As New DataSet
				Dim rangoInicial = Me.cboInicioRango.SelectedValue
				Dim rangoFinal = Me.cboFinalRango.SelectedValue
				Dim conteo = Me.rbtnConteos.SelectedValue

				ds = RPT_MarbeteIndividual(rangoInicial, rangoFinal, conteo)

				If Not ds Is Nothing Then
					If ds.Tables(0).Rows.Count > 0 Then
						Me.HyperLink1.Visible = True
						REPORTE(ds)
					Else
						CMensajes.MostrarMensaje("No existen marbetes dentro del rango especificado.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual)
					End If
				Else
					CMensajes.MostrarMensaje("No existen marbetes dentro del rango especificado.", CMensajes.Tipo.GetError, wucMessageMarbeteIndividual)
				End If
			Catch ex As Exception
				Throw New Exception(ex.Message, ex.InnerException)
			End Try
		End If
	End Sub

	Public Function RPT_MarbeteIndividual(ByVal rangoInicial As String, ByVal rangoFinal As String, Optional ByVal conteo As String = Nothing) As DataSet
		Try
			Dim ds As New DataSet
			Dim par(3) As SqlParameter

			If Session("Sociedad").ToString = 3 Then
				par(0) = New SqlParameter("@Tipo", Session("Perfil"))
				par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(2) = New SqlParameter("@RangoInicial", rangoInicial)
				par(3) = New SqlParameter("@RangoFinal", rangoFinal)

				ds = DatosSQL.funcioncp("RPT_MarbeteIndividual_RST", par)
			Else
				ReDim par(4)
				par(0) = New SqlParameter("@Tipo", Session("Perfil"))
				par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
				par(2) = New SqlParameter("@RangoInicial", rangoInicial)
				par(3) = New SqlParameter("@RangoFinal", rangoFinal)
				par(4) = New SqlParameter("@Conteo", conteo)

				ds = DatosSQL.funcioncp("RPT_MarbeteIndividual", par)
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub REPORTE(ByVal DATOS As DataSet)
		Try
			'//////////////////////////////////////////////////////////////////////
			'generacion de  reporte en pdf
			Dim report1 As dbATReport
			Dim settings As New dbAutoTrack.DataReports.PageSettings
			settings.PaperKind = Drawing.Printing.PaperKind.Letter
			settings.Orientation = PageOrientation.Portrait
			settings.Margins.MarginLeft = 0
			settings.Margins.MarginRight = 0
			settings.Margins.MarginTop = 0
			settings.Margins.MarginBottom = 0

			If Session("Sociedad").ToString = "3" Then
				report1 = New RPT_MarbeteIndividual_RSTrpt
			Else
				report1 = New RPT_MarbeteIndividualrpt
			End If

			report1.PageSetting = settings
			Dim document1 As New PDFExport.PDFDocument

			Dim DirectorioArchivos As String = ConfigurationManager.AppSettings("SAIC") & ConfigurationManager.AppSettings("Reportes") & ConfigurationManager.AppSettings("MarbetesIndividualRango")

			If Not Directory.Exists(DirectorioArchivos) Then
				Directory.CreateDirectory(DirectorioArchivos)
			End If

			Dim pathPdf As String = DirectorioArchivos & "MarbetesIndividualRango_" & Me.cboInicioRango.SelectedValue & "A" & Me.cboFinalRango.SelectedValue & "_" & Session("IdUsuario") & ".pdf"
			report1.DataSource = DATOS

			CType(report1.Sections.Item(0).Controls.Item("txtFecha"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Fecha")
			CType(report1.Sections.Item(0).Controls.Item("txtHora"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Hora")
			CType(report1.Sections.Item(0).Controls.Item("txtTienda"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Tienda")
			'CType(report1.Sections.Item(0).Controls.Item("txtAlmacen"), dbATTextBox).Value = DATOS.Tables(1).Rows(0).Item("Almacen")

			document1.EmbedFont = True
			document1.Compress = True
			report1.Generate()
			document1.Export(report1.Document, pathPdf)
			Dim encripta As New Encryption
			Me.HyperLink1.NavigateUrl = "FileDownload.aspx?file=" & encripta.encryptString(pathPdf.Trim)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub rbtnConteos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtnConteos.SelectedIndexChanged
		Me.CatalogoMarbetes_Buscar()
	End Sub
End Class