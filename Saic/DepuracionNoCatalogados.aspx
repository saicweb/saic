﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="DepuracionNoCatalogados.aspx.vb" Inherits="Saic.DepuracionNoCatalogados" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
               <asp:Label runat="server" Text="Procesos especiales" class="titulos"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Depuración de no catalogados" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageDepuracionNoCatalogados" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="100%">
                    <tr>
                        <td align="center">
                            <asp:button id="btnGenerarReporte" runat="server" Text="Depurar" CssClass="btn"></asp:button>
                        </td>
                    </tr>
                    <tr>
                        <td><br />
                            <asp:label id="lbltiempo" runat="server" ForeColor="Red"></asp:label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
