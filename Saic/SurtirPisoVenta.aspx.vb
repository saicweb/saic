﻿Imports System.Data.SqlClient
Imports System.IO

Public Class SurtirPisoVenta
	Inherits System.Web.UI.Page

	Protected WithEvents winConfirm1 As windowConfirm
	Protected WithEvents btnConfirmSI As Button
	Protected WithEvents btnConfirmNO As Button

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		Try
			If Session("IdUsuario") Is Nothing OrElse Session("Perfil") Is Nothing OrElse Session("IdTienda") Is Nothing Then
				Response.Redirect("Default.aspx")
			End If

			Me.wucMessageSurtirPisoVenta.Visible = False

			If Not IsPostBack Then
				If Session("Msg") IsNot Nothing Then
					CMensajes.MostrarMensaje(Session("Msg"), CMensajes.Tipo.GetInformativo, wucMessageSurtirPisoVenta)
				End If
				CargarMarbetes()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnsurtir_Click(sender As Object, e As System.EventArgs) Handles btnsurtir.Click
		Try
			Surtir()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
		Response.Redirect("SurtirMarbete.aspx?SID=" & Request.QueryString("SID"), False)
	End Sub

	Private Sub btneliminar_Click(sender As Object, e As System.EventArgs) Handles btneliminar.Click
		Me.HddnAccion.Value = Nothing
		Me.HddnAccion.Value = "Eliminar"
		Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		lblTexto.Text = "<div align=""center"">¿Realmente desea eliminar el/los elemento(s) seleccionado(s)?</div>"
		winConfirm1.MuestraDialogo()
	End Sub

	Private Sub cboMarbetes_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboMarbetes.SelectedIndexChanged
		Try
			Dim Ds As New DataSet
			Ds = Ejecutar(cboMarbetes.SelectedValue, Nothing, Nothing, Nothing, Accion.ObtenerAlmacen_Marbete, Nothing, Nothing)
			If Not Ds Is Nothing AndAlso Ds.Tables.Count > 0 AndAlso Ds.Tables(0).Rows.Count > 0 Then
				Session("Almacen") = Ds.Tables(0).Rows(0)(0)
			Else
				Session("Almacen") = Nothing
			End If

			lblAlmacen.Text = "Almacén: " & Session("Almacen")
			Ejecutar(cboMarbetes.SelectedValue, Nothing, Nothing, Nothing, Accion.ActualizarConteo, Nothing, Session("Almacen"))
			CargarGrid()
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarMarbetes()
		Try
			Dim ds As New DataSet
			ds = Ejecutar(Nothing, Nothing, Nothing, Nothing, Accion.ConsultaMarbete, Nothing, Nothing)

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				Dim dr As DataRow
				dr = ds.Tables(0).NewRow
				dr("Marbete") = "[Seleccione]"
				ds.Tables(0).Rows.InsertAt(dr, 0)

				cboMarbetes.Items.Clear()
				Me.cboMarbetes.DataSource = ds.Tables(0)
				Me.cboMarbetes.DataValueField = "Marbete"
				Me.cboMarbetes.DataTextField = "Marbete"
				Me.cboMarbetes.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub CargarGrid()
		Try
			Dim ds As New DataSet
			ds = Ejecutar(cboMarbetes.SelectedValue, Nothing, Nothing, Nothing, Accion.ConsultaGrid, Nothing, Nothing)

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
				grid.DataSource = ds
				grid.DataBind()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Function Ejecutar(Marbete As String, EAN As String, Cant_Surtir As String, st_surtido As String, Accion As Accion, Cant_Final As String, Almacen As String) As DataSet
		Try
			Dim ds As New DataSet
			Dim par(8) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Marbete", Marbete)
			par(2) = New SqlParameter("@EAN", EAN)
			par(3) = New SqlParameter("@Cant_Surtir", Cant_Surtir)
			par(4) = New SqlParameter("@st_surtido", st_surtido)
			par(5) = New SqlParameter("@Perfil", Session("Perfil"))
			par(6) = New SqlParameter("@Accion", Accion)
			par(7) = New SqlParameter("@Cant_Final", Cant_Final)
			par(8) = New SqlParameter("@Almacen", Almacen)

			ds = DatosSQL.funcioncp("ConteosDispositivo_Surtir", par)
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Surtir()
		Try
			Dim Msg As String = Nothing
			Dim cont As Integer = 0
			For i As Integer = 0 To grid.Rows.Count - 1
				Dim Check As CheckBox = TryCast(grid.Rows(i).Cells(8).FindControl("CheckBox5"), CheckBox)
				If Check.Checked Then
					cont += 1
					Dim Marbete As String = grid.Rows(i).Cells(1).Text
					Dim EAN As String = grid.Rows(i).Cells(2).Text
					Dim Cant_Final As String = grid.Rows(i).Cells(6).Text
					Cant_Final = Cant_Final.Replace("(", "")
					Cant_Final = Cant_Final.Replace(")", "")
					If Cant_Final < 0 Then
						Msg &= " Marbete: " & Marbete & " EAN: " & EAN & vbLf
						Continue For
					End If
					Ejecutar(Marbete, EAN, Nothing, Nothing, Accion.AgregarSurtido, Cant_Final, Session("Almacen"))
				End If
			Next
			If cont = 0 Then
				CMensajes.MostrarMensaje("Seleccione por lo menos un registro.", CMensajes.Tipo.GetError, wucMessageSurtirPisoVenta)
			Else
				CargarGrid()
				If Msg Is Nothing Then
					Session("Msg") = "Información surtida correctamente."
				Else
					Session("Msg") = " Imposible surtir cantidades Negativas. " & vbLf & Msg
				End If
				Response.Redirect("SurtirPisoVenta.aspx?SID=" & Request.QueryString("SID"), False)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Sub Eliminar()
		Dim cont As Integer = 0
		For i As Integer = 0 To grid.Rows.Count - 1
			Dim Check As CheckBox = TryCast(grid.Rows(i).Cells(8).FindControl("CheckBox5"), CheckBox)
			If Check.Checked Then
				cont += 1
				Dim Marbete As String = grid.Rows(i).Cells(1).Text
				Dim EAN As String = grid.Rows(i).Cells(2).Text
				Ejecutar(Marbete, EAN, Nothing, Nothing, Accion.EliminarSurtido, Nothing, Session("Almacen"))
			End If
		Next
		If cont = 0 Then
			CMensajes.MostrarMensaje("Seleccione por lo menos un registro.", CMensajes.Tipo.GetError, wucMessageSurtirPisoVenta)
		Else
			CargarGrid()
			Session("Msg") = "Información eliminada correctamente."
			Response.Redirect("SurtirPisoVenta.aspx?SID=" & Request.QueryString("SID"), False)
		End If
	End Sub

	Sub Modificar(ByVal irow As Integer)
		Dim Cant As TextBox = CType(grid.Rows(irow).Cells(5).Controls(1), TextBox)
		If Cant.Text.Trim = "" Then
			CMensajes.MostrarMensaje("Ingrese la cantidad antes de continuar.", CMensajes.Tipo.GetError, wucMessageSurtirPisoVenta)
		ElseIf IsNumeric(Cant.Text.Trim) = False Then
			CMensajes.MostrarMensaje("La cantidad debe ser un dato numérico.", CMensajes.Tipo.GetError, wucMessageSurtirPisoVenta)
		ElseIf CDbl(Cant.Text.Trim) < 0 Then
			CMensajes.MostrarMensaje("La cantidad debe ser un dato numérico mayor que cero.", CMensajes.Tipo.GetError, wucMessageSurtirPisoVenta)
		Else
			Dim Cant_Surtir As String = Cant.Text
			Dim Marbete As String = grid.Rows(irow).Cells(1).Text
			Dim EAN As String = grid.Rows(irow).Cells(2).Text
			Ejecutar(Marbete, EAN, Cant_Surtir, Nothing, Accion.ModificarCantidad, Nothing, Nothing)
			CargarGrid()
			CMensajes.MostrarMensaje("Registro actualizado correctamente.", CMensajes.Tipo.GetExito, wucMessageSurtirPisoVenta)
		End If
	End Sub

	Private Sub grid_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grid.RowCommand
		Try
			Dim irow As Integer = Integer.Parse(e.CommandArgument.ToString)

			If e.CommandName = "Modificar" Then
				Me.HddnAccion.Value = Nothing
				Me.HddnAccion.Value = "Modificar"
				Me.HddnIdRow.Value = Nothing
				Me.HddnIdRow.Value = irow
				Dim lblTexto As Label = winConfirm1.FindControl("LblTextoConfirmacion")
				lblTexto.Text = "<div align=""center"">¿Esta seguro que desea modificar el registro seleccionado?</div>"
				winConfirm1.MuestraDialogo()
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub grid_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grid.RowDataBound
		Try
			If e.Row.RowType = DataControlRowType.DataRow Then
				Dim lnkmodificar As LinkButton = CType(e.Row.Cells(7).Controls(1), LinkButton)

				Dim Cant_Final As String = e.Row.Cells(6).Text
				If Cant_Final < 0 Then
					e.Row.Cells(6).Text = "(" & Cant_Final & ")"
					e.Row.Cells(6).ForeColor = Drawing.Color.Red
				End If
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(grid.HeaderRow.Cells(0).FindControl("chkheader"), CheckBox)
		For Each row As GridViewRow In grid.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(0).FindControl("CheckBox5"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub

	Public Enum Accion
		Insert = 1
		ConsultaGrid = 2
		ConsultaMarbete = 3
		ModificarCantidad = 4
		AgregarSurtido = 5
		ObtenerAlmacen_Marbete = 6
		EliminarSurtido = 7
		ActualizarConteo = 9
	End Enum

#Region "PopUp confirm"
	Private Sub Principal_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
		cargaWinConfirm()
	End Sub

	Sub cargaWinConfirm()
		winConfirm1 = CType(LoadControl("~\UserControl\windowConfirm.ascx"), windowConfirm)
		Dim txtTitutlo As Label = winConfirm1.FindControl("LblTextoTitulo")
		txtTitutlo.Text = "Confirmar"
		Dim txtCuerpo As Label = winConfirm1.FindControl("LblTextoConfirmacion")
		txtCuerpo.Text = ""


		btnConfirmNO = winConfirm1.FindControl("BtnNO")
		btnConfirmNO.Text = "No"
		AddHandler btnConfirmNO.Click, AddressOf Me.btnConfirmNO_Click

		btnConfirmSI = winConfirm1.FindControl("btnSI")
		btnConfirmSI.Text = "Sí"
		AddHandler btnConfirmSI.Click, AddressOf Me.btnConfirmSI_Click

		phConfirm.Controls.Add(winConfirm1)
	End Sub

	Protected Sub btnConfirmSI_Click(ByVal sender As Object, ByVal e As EventArgs)
		Try
			Select Case Me.HddnAccion.Value.ToString
				Case "Modificar"
					Dim irow As Integer = Me.HddnIdRow.Value
					Modificar(irow)
				Case "Eliminar"
					Eliminar()
			End Select
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Protected Sub btnConfirmNO_Click(ByVal sender As Object, ByVal e As EventArgs)
		Me.wucMessageSurtirPisoVenta.Visible = False
		Me.winConfirm1.OcultaDialogo()
	End Sub
#End Region
End Class