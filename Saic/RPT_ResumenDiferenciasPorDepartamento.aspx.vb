﻿Imports System.Data.SqlClient
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine
Imports dbAutoTrack.DataReports

Public Class RPT_ResumenDiferenciasPorDepartamento
	Inherits System.Web.UI.Page

	Public Shared rpt As New ReportDocument
	Dim txtNombreReporte As TextObject
	Dim txtFechaReporte As TextObject
	Dim txtHoraReporte As TextObject
	Dim txtTiendaReporte As TextObject
	Dim txtAlmacen As TextObject
	Dim txtUnidadesPiso As TextObject
	Dim txtUnidadesBodega As TextObject
	Dim txtUnidadesTotal As TextObject
	Dim txtCostoPiso As TextObject
	Dim txtCostoBodega As TextObject
	Dim txtCostoTotal As TextObject
	Dim txtVentasPiso As TextObject
	Dim txtVentasBodega As TextObject
	Dim txtVentasTotal As TextObject

	Dim ds As DataSet
	Dim RutaCR As String = NombreReportes.RutaCR

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If
		If Session("Perfil") = "P" Then
			Response.Redirect("Inicio.aspx?SID=" & Request.QueryString("SID"))
		End If

		Me.wucMessageRPT_ResumenDiferenciasPorDepartamento.Visible = False

		If Session("Sociedad").ToString = "3" Then
			Me.lblAlmacen.Visible = False
			Me.cboAlmacen.Visible = False
		End If

		If Not Page.IsPostBack Then
			If Session("Sociedad").ToString <> "3" Then Almacen_Buscar()
			Session.Remove("RPT_ResumenDifDepto")
		End If
	End Sub

	Private Sub btnGenerarReporte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarReporte.Click
		Dim almacen As String = Nothing
		If Session("Sociedad").ToString = "3" Then
			almacen = "A001"
		Else
			almacen = Me.cboAlmacen.SelectedValue
		End If
		CatalogoReportes_CRV13(almacen)
	End Sub

	Protected Sub _reportViewer_DataBinding(sender As Object, e As EventArgs) Handles RPT_Resumen_Diferencias_PorDepartamento.DataBinding
		If Session("RPT_ResumenDifDepto") Is Nothing Then
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				rpt.SetDataSource(ds.Tables(0))
				RPT_Resumen_Diferencias_PorDepartamento.ReportSource = rpt
				Session("RPT_ResumenDifDepto") = rpt
				Me.TblCabecero.Visible = False
			End If
		Else
			RPT_Resumen_Diferencias_PorDepartamento.ReportSource = Session("RPT_ResumenDifDepto")
		End If
	End Sub

	Public Sub CatalogoReportes_CRV13(ByVal Almacen As String)
		ds = RPT_ResumenDiferenciasPorDepartamento(Almacen)

		Dim nomReporte As String
		nomReporte = IIf(Session("Sociedad").ToString = "3", NombreReportes.RPT_ResumenDiferenciasPorDepartamento_RST, NombreReportes.RPT_ResumenDiferenciasPorDepartamento)

		If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
			rpt.Load(Server.MapPath(RutaCR & nomReporte))

			txtNombreReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtNombreReporte")
			txtFechaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtFechaReporte")
			txtHoraReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtHoraReporte")
			txtTiendaReporte = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtTiendaReporte")
			If Session("Sociedad").ToString <> "3" Then
				txtAlmacen = rpt.ReportDefinition.Sections("Section1").ReportObjects("TxtAlmacen")
				txtUnidadesBodega = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtUnidadesBodega")
				txtCostoBodega = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtCostoBodega")
				txtVentasBodega = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtVentasBodega")
				txtVentasPiso = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtVentasPiso")
				txtVentasTotal = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtVentasTotal")
				txtUnidadesPiso = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtUnidadesPiso")
				txtUnidadesTotal = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtUnidadesTotal")
				txtCostoPiso = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtCostoPiso")
				txtCostoTotal = rpt.ReportDefinition.Sections("Section1").ReportObjects("txtCostoTotal")
			End If

			txtNombreReporte.Text = "RESUMEN DE DIFERENCIAS POR DEPARTAMENTO"
			txtFechaReporte.Text = Today
			txtHoraReporte.Text = TimeString
			txtTiendaReporte.Text = Reportes.GetTienda(Session("IdTienda"))
			If Session("Sociedad").ToString <> "3" Then
				txtUnidadesPiso.Text = IIf(ds.Tables(1).Rows(0)("UnidadesPiso") Is DBNull.Value, "", ds.Tables(1).Rows(0)("UnidadesPiso"))
				txtUnidadesTotal.Text = IIf(ds.Tables(1).Rows(0)("UnidadesTotal") Is DBNull.Value, "", ds.Tables(1).Rows(0)("UnidadesTotal"))
				txtCostoPiso.Text = IIf(ds.Tables(1).Rows(0)("CostoPiso") Is DBNull.Value, "", ds.Tables(1).Rows(0)("CostoPiso"))
				txtCostoTotal.Text = IIf(ds.Tables(1).Rows(0)("CostoTotal") Is DBNull.Value, "", ds.Tables(1).Rows(0)("CostoTotal"))
				txtAlmacen.Text = IIf(Almacen = "0", "Todos", Almacen)
				txtVentasPiso.Text = IIf(ds.Tables(1).Rows(0)("VentaPiso") Is DBNull.Value, "", ds.Tables(1).Rows(0)("VentaPiso"))
				txtVentasTotal.Text = IIf(ds.Tables(1).Rows(0)("VentaTotal") Is DBNull.Value, "", ds.Tables(1).Rows(0)("VentaTotal"))
				txtUnidadesBodega.Text = IIf(ds.Tables(1).Rows(0)("UnidadesBodega") Is DBNull.Value, "", ds.Tables(1).Rows(0)("UnidadesBodega"))
				txtCostoBodega.Text = IIf(ds.Tables(1).Rows(0)("CostoBodega") Is DBNull.Value, "", ds.Tables(1).Rows(0)("CostoBodega"))
				txtVentasBodega.Text = IIf(ds.Tables(1).Rows(0)("VentaBodega") Is DBNull.Value, "", ds.Tables(1).Rows(0)("VentaBodega"))
			End If

			rpt.SetDataSource(ds.Tables(0))
			RPT_Resumen_Diferencias_PorDepartamento.ReportSource = rpt
			RPT_Resumen_Diferencias_PorDepartamento.DataBind()
		Else
			CMensajes.MostrarMensaje("No se encontró información.", CMensajes.Tipo.GetError, wucMessageRPT_ResumenDiferenciasPorDepartamento)
		End If
	End Sub

	Public Function RPT_ResumenDiferenciasPorDepartamento(ByVal Almacen As String) As DataSet
		Try
			Dim dsExcel As New DataSet
			Dim par(1) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Almacen", Almacen)
			If Session("Sociedad").ToString = "3" Then
				ds = DatosSQL.funcioncp("RPT_ResumenDiferenciasPorDepartamento_RST", par)
			Else
				ds = DatosSQL.funcioncp("RPT_ResumenDiferenciasPorDepartamento", par)
			End If
			If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Dim par2(1) As SqlParameter
				par2(0) = New SqlParameter("@idTienda", Session("IdTienda"))
				par2(1) = New SqlParameter("@Almacen", Almacen)

				If Session("Sociedad").ToString = "3" Then
					dsExcel = DatosSQL.funcioncp("RPT_ResumenDiferenciasPorDepartamentoExcel_RST", par2)
				Else
					dsExcel = DatosSQL.funcioncp("RPT_ResumenDiferenciasPorDepartamentoExcel", par2)
				End If
				If dsExcel IsNot Nothing AndAlso dsExcel.Tables.Count > 0 AndAlso dsExcel.Tables(0).Rows.Count > 0 Then
					If Session("Sociedad").ToString = "3" Then
						Reportes.REPEXCEL(dsExcel, "Resumen Diferencias Por Departamento", "ResumenDiferenciasPorDepartamento_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), False)
					Else
						Reportes.REPEXCEL(dsExcel, "Resumen Diferencias Por Departamento", "ResumenDiferenciasPorDepartamento_", Me.HyperLink1, Session("Perfil"), Session("IdUsuario"), True)
					End If
				End If
			End If
			Return ds
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Sub Almacen_Buscar()
		Try
			Dim par(2) As SqlParameter
			Dim DS As New DataSet
			par(0) = New SqlParameter("@Tipo", Session("Perfil"))
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@validar", 2)
			DS = DatosSQL.funcioncp("Almacen_Buscar", par)

			If Not DS Is Nothing AndAlso DS.Tables.Count > 0 AndAlso DS.Tables(0).Rows.Count > 0 Then
				Me.cboAlmacen.DataSource = DS
				Me.cboAlmacen.DataTextField = "almacen"
				Me.cboAlmacen.DataValueField = "idalmacen"
				Me.cboAlmacen.DataBind()
			Else
				CMensajes.MostrarMensaje("No existe información para mostrar.", CMensajes.Tipo.GetError, wucMessageRPT_ResumenDiferenciasPorDepartamento)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub
End Class