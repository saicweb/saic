﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="Saic._Default" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SAIC</title>
    <link href="~/styles/estilos.css" rel="stylesheet" type="text/css" />
    <link href="~/styles/bootstrap.css" rel="stylesheet" type="text/css" />

</head>

<body style="background-color: Black; background-image: none;">
    <form id="form1" runat="server" submitdisabledcontrols="true" defaultbutton="btnIngresar">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table id="login">
                <tr>
                    <td>
                        <table align="center" style="margin-top: -11%; margin-left: 105px" width="43%">
                            <tr>
                                <td align="center">
                                    <asp:TextBox ID="txtUsuario" runat="server" MaxLength="10" Width="170px"
                                        Height="18px" ToolTip="Usuario" TabIndex="1" placeholder="Usuario">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsuario" Display="Dynamic"
								        ErrorMessage="*" ForeColor="Red" Font-Size="small"></asp:requiredfieldvalidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><br />
                                    <asp:TextBox ID="TxtContrasena" runat="server" MaxLength="100" Width="170px"
                                        ToolTip="Contraseña" placeholder="Contaseña" Height="18px" TabIndex="2"
                                        TextMode="Password" EnableViewState="true">
                                    </asp:TextBox>
                                </td>
                                <td>
                                    <asp:RegularExpressionValidator ID="regexEmailValid" runat="server"
                                        ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9].{7}.*)$"
                                        ControlToValidate="TxtContrasena" ErrorMessage="*"
                                        ForeColor="Red" Font-Size="Small">
                                    </asp:RegularExpressionValidator>
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ControlToValidate="TxtContrasena" Display="Dynamic"
								        ErrorMessage="*" ForeColor="Red" Font-Size="small"></asp:requiredfieldvalidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"><br />
                                    <asp:UpdatePanel ID="upl" runat="server">
                                        <ContentTemplate>
                                            <asp:DropDownList runat="server" Width="183px" ID="cmbUnidadNegocio" AppendDataBoundItems="True" AutoPostBack="true" TabIndex="3">
                                            </asp:DropDownList><br /><br />
                                            <asp:DropDownList runat="server" Width="183px" ID="cmbTienda" AppendDataBoundItems="True" Enabled="false" TabIndex="4">
                                            </asp:DropDownList><br />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="cmbUnidadNegocio" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr align="center">
                                <td align="center" style="margin-left: -20px"><br />
                                    <asp:radiobuttonlist id="rbtnperfilInv" runat="server" tabIndex="5" RepeatDirection="Horizontal" ToolTip="Perfil de inventario" CssClass="radio inline" TextAlign="Right" CellPadding="15">
										<asp:ListItem Value="1">Preconteo</asp:ListItem>
										<asp:ListItem Value="2">Rotativo</asp:ListItem>
									</asp:radiobuttonlist>
                                </td>
                                <td><br />
                                    <asp:requiredfieldvalidator id="RequiredFieldValidator3" CssClass="inline" runat="server" ControlToValidate="rbtnperfilInv" Display="Dynamic"
								        ErrorMessage="*" ForeColor="Red" Font-Size="small"></asp:requiredfieldvalidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2"><br />
                                    <asp:Button ID="btnIngresar" runat="server" Text="Entrar" SkinID="warning"
                                        ForeColor="White"
                                        style="margin-top: -10px; margin-left: 0px" TabIndex="6"
                                        onclick="btnIngresar_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>
    </form>
</body>
</html>

