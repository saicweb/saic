﻿Imports System.Data
Imports System.Data.SqlClient
Public Class MarcarMarbeteVacio
	Inherits System.Web.UI.Page

	Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		If Session("IdUsuario") Is Nothing OrElse Session("IdTienda") Is Nothing OrElse Session("Perfil") Is Nothing Then
			Response.Redirect("Default.aspx")
		End If

		If Not IsPostBack Then
			MarbetesFaltantes()
		End If
	End Sub

	Sub MarbetesFaltantes()
		Try
			Dim ds As New DataSet
			Dim par(2) As SqlParameter
			par(0) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(1) = New SqlParameter("@Tipo", 2)
			par(2) = New SqlParameter("@Perfil", Session("Perfil"))

			ds = DatosSQL.funcioncp("CatalogoMarbetes_Buscar", par)

			If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
				Me.grid.DataSource = ds.Tables(0)
				Me.grid.DataBind()
			Else
				Me.grid.DataSource = Nothing
				Me.grid.DataBind()
				CMensajes.MostrarMensaje("No existen marbetes faltantes.", CMensajes.Tipo.GetError, wucMessageMarcarMarbeteVacio)
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Sub btnModificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModificar.Click
		Try
			Dim MensajeErrores As String = ""
			Dim IdMarbete As String
			Dim CkVacio As CheckBox
			Dim Vacio As String
			For Each oRow In Me.grid.Rows
				CkVacio = oRow.FindControl("chkvacio")
				IdMarbete = grid.Rows(oRow.RowIndex).Cells(0).Text.Replace("*", "").TrimEnd 'grid.Columns.Items(i).Cells(2).Text
				Vacio = grid.Rows(oRow.RowIndex).Cells(2).Text     'Dim Vacio As String = grid.Items(i).Cells(4).Text

				If CkVacio.Checked And Vacio = "N" Then
					CatalogoMarbetes_Administracion(IdMarbete, "S", MensajeErrores)
				ElseIf CkVacio.Checked = False And Vacio = "S" Then
					CatalogoMarbetes_Administracion(IdMarbete, "N", MensajeErrores)
				End If
			Next
			Me.MarbetesFaltantes()
			CMensajes.MostrarMensaje("Proceso completado con exito.", CMensajes.Tipo.GetExito, wucMessageMarcarMarbeteVacio)
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Sub

	Private Function CatalogoMarbetes_Administracion(ByVal IdMarbete As String, ByVal Vacio As String, ByVal Mensaje As String) As Boolean
		Try
			CatalogoMarbetes_Administracion = False
			Dim par(4) As SqlParameter
			If Session("Perfil") = "P" Then
				par(0) = New SqlParameter("@Accion", 6) '5=Marcar o Desmarcar como vacio
			Else
				par(0) = New SqlParameter("@Accion", 5) '5=Marcar o Desmarcar como vacio
			End If
			par(1) = New SqlParameter("@IdTienda", Session("IdTienda"))
			par(2) = New SqlParameter("@IdMarbete", IdMarbete)
			par(3) = New SqlParameter("@Vacio", Vacio)
			par(4) = New SqlParameter("@Perfil", Session("Perfil"))
			DatosSQL.procedimientocp("CatalogoMarbetes_Administracion", par)
			Return CatalogoMarbetes_Administracion = True
		Catch ex As Exception
			Throw New Exception(ex.Message, ex.InnerException)
		End Try
	End Function

	Private Sub grid_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grid.RowDataBound
		Try
			If e.Row.RowType = DataControlRowType.DataRow Then
				Dim Vacio As String = e.Row.Cells(2).Text
				If Vacio = "S" Then
					Dim CkVacio As CheckBox = CType(e.Row.Cells(1).Controls(1), CheckBox)
					CkVacio.Checked = True
				End If
				e.Row.Cells(2).Visible = False 'vacio
				grid.HeaderRow.Cells(2).Visible = False
			End If
		Catch ex As Exception
			Throw New Exception(ex.Message)
		End Try
	End Sub

	Protected Sub CheckUncheckAll(ByVal sender As Object, ByVal e As System.EventArgs)
		Dim chk1 As CheckBox
		chk1 = DirectCast(grid.HeaderRow.Cells(1).FindControl("chkheader"), CheckBox)
		For Each row As GridViewRow In grid.Rows
			Dim chk As CheckBox
			chk = DirectCast(row.Cells(1).FindControl("chkvacio"), CheckBox)
			chk.Checked = chk1.Checked
		Next
	End Sub
End Class

