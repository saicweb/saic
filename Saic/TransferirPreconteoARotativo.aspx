﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="TransferirPreconteoARotativo.aspx.vb" Inherits="Saic.TransferirPreconteoARotativo" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>
    <link href="Styles/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/bootstrap-multiselect.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            CargaListas();
        });


        function CargaListas() {

            var firstConfigurationSet = {
                includeSelectAllOption: false,
                enableFiltering: false,

            };
            var secondConfigurationSet = {
                includeSelectAllOption: false,
                enableFiltering: true,
                maxHeight: 350
            };

            var set = 1;
            $('[id*=lstDepartamentos]').multiselect(secondConfigurationSet);
            function rebuildMultiselect(options) {
                $('[id*=lstDepartamentos]').multiselect('setOptions', options);
                $('[id*=lstDepartamentos]').multiselect('rebuild');
            }

            $('[id*=lstPrueba]').multiselect(secondConfigurationSet);
            function rebuildMultiselect(options) {
                $('[id*=lstPrueba]').multiselect('setOptions', options);
                $('[id*=lstPrueba]').multiselect('rebuild');
            }
        }
        function recargar() {
            window.location.href = window.location.href;
        }



    </script>

    <table width="100%">
        <tr>
            <td class="titulos">
                <asp:Label ID="lblTitulo" runat="server" Text="Transferir de preconteo a rotativo"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessage" runat="server" />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="false" SkinID="Grid3" PageSize="20" AllowPaging="true" DataKeyNames="Almacen">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <p align="center">Marcar p/Transferir</p>
                                            <p align="center">
                                                <asp:CheckBox ID="CheckBox1" OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" runat="server" Text="Todos"></asp:CheckBox>
                                            </p>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chbxSeleccionar" OnCheckedChanged="chbxSeleccionar_CheckedChanged" AutoPostBack="true" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Almacen">
                                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkAlmacen" CommandName="Almacen" CommandArgument="<%#Container.DataItemIndex%>"
                                                Text='<%# DataBinder.Eval(Container.DataItem, "Almacen") %>' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="150px"></HeaderStyle>
                                        <HeaderTemplate></HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ListBox ID="lstDepartamentos" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No se encontro informacion en este catálogo.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <asp:Button ID="btnTransferir" runat="server" Text="Transferir" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:HiddenField ID="hdnTienda" runat="server" />
                 <asp:PlaceHolder ID="phConfirm" runat="server"></asp:PlaceHolder>
                
            </td>
        </tr>
    </table>
</asp:Content>


