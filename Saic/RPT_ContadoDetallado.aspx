﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master"   CodeBehind="RPT_ContadoDetallado.aspx.vb" Inherits="Saic.RPT_ContadoDetallado" %>

<%@ Register Src="~/UserControl/wucMessage.ascx" TagName="wucMessage" TagPrefix="wum" %>
<%@ Register assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPages" runat="server">
    <table runat="server" width="100%">
        <tr>
            <td>
                <asp:Label runat="server" Text="Cierre de inventario" class="titulos" ></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label runat="server" Text="Reporte contado detallado" ID="Label2" Font-Size="Medium" Font-Bold="true"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <wum:wucMessage ID="wucMessageRPT_ContadoDetallado" runat="server" /><br />
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" width="45%" align="center" id="TblCabecero"> 
                    <tr align="center">
                        <td colspan="2">
                            <span><asp:Button runat="server" Text="Generar reporte" ID="btnGenerarReporte" CssClass="btn" /></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table runat="server" width="90%">
                    <tr>
                        <td align="center" width="700px"><br />
                            <span><asp:hyperlink id="HyperLink1" runat="server" ForeColor="Blue" Visible="false">Descargar Excel</asp:hyperlink></span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"><br />
                            <div style="z-index:-1">
                                <cr:crystalreportviewer Id="RPT_Contado_Detallado" runat="server"  HasCrystalLogo="False"
                                AutoDataBind="True" OnDataBinding="_reportViewer_DataBinding" Height="50px"  EnableParameterPrompt="false" EnableDatabaseLogonPrompt="false" ToolPanelWidth="200px" 
                                Width="500px" ToolPanelView="None" HasRefreshButton="True" ShowAllPageIds="True" HasToggleGroupTreeButton="False" HasToggleParameterPanelButton="False" HasDrilldownTabs="False"></cr:crystalreportviewer>  
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
